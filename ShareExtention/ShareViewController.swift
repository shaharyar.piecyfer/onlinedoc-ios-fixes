//
//  ShareViewController.swift
//  ShareExtention
//
//  Created by Piecyfer on 01/01/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit
import Social
import CoreServices

class ShareViewController: UIViewController {

//    private let typeText = String(kUTTypeText)
    private let typeURL = String(kUTTypeURL)
    private let typeImage = String(kUTTypeImage)
    private let typeVideo = String(kUTTypeVideo)
    private let typeData = String(kUTTypeData)
    private let typePropertyList = String(kUTTypePropertyList)
//    private let typePDF = String(kUTTypePDF)
    
    private let appURL = "onlineDoc://"
    private let groupName = "group.com.piecyfer.onlinedoc"
    private let urlDefaultName = "incomingURL"
    private var attachmentData: [ShareFileModel] = []
    private let KeyShareAttachment = "KeyShareAttachment"
//    private let KeySharePDF = "KeySharePDF"

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // 1
        guard let extensionItem = extensionContext?.inputItems.first as? NSExtensionItem,
            let itemProvider = extensionItem.attachments?.first else {
                self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
                return
        }

        if itemProvider.hasItemConformingToTypeIdentifier(typeData) {
            manageFiles()
        }
//        else if itemProvider.hasItemConformingToTypeIdentifier(typeImage) {
//            manageFiles()
//        }
        else {
            print("Error: not a right file type selected")
            self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
        }
    }
    
    private func manageFiles() {
        let content = extensionContext!.inputItems[0] as! NSExtensionItem
        for (index, attachment) in (content.attachments!).enumerated() {
            if attachment.hasItemConformingToTypeIdentifier(typeURL) {
                attachment.loadItem(forTypeIdentifier: typeURL, options: nil, completionHandler: { (data, error) in
                    guard error == nil else {
                        self.showErrorPopup(error)
                        return
                    }
                    
                    if let url = data as? NSURL, let urlString = url.absoluteString {
                        if !urlString.starts(with: "file:"){
                            let file = ShareFileModel()
                            file.fileType = FileTypes.WEB_URL.rawValue
                            file.name = urlString
                            self.attachmentData.append(file)
                            
                            if index == (content.attachments?.count)! - 1 {
                                DispatchQueue.main.async {
                                    let userDefaults = UserDefaults(suiteName: self.groupName)
                                    userDefaults?.set(try? PropertyListEncoder().encode(self.attachmentData), forKey: self.KeyShareAttachment)
                                    userDefaults?.synchronize()
                                    self.openMainApp()
                                }
                            }
                        }
                        else {
                            do {
                                let rawData = try Data(contentsOf: URL(string: urlString)!)
                                    
                                let name = url.lastPathComponent
                                let file = ShareFileModel()
                                file.name = name ?? UUID().uuidString + "\(url.pathExtension ?? ".jpeg")"
                                file.fileExt = url.pathExtension ?? String(name!.split(separator: ".").last!)
                                
                                let documentDirectory = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: self.groupName)
                                guard let tmpURL = documentDirectory?.appendingPathComponent(name ?? file.name) else {return}
                                do {
                                    try rawData.write(to: tmpURL, options: [])
                                    file.fileURL = tmpURL
                                } catch {
                                    print("ShareViewController->writeFile->error  ===> \(error)")
                                }
                                
                                if attachment.hasItemConformingToTypeIdentifier(self.typeImage) {
                                    file.fileType = FileTypes.IMAGE.rawValue
                                    file.mimeType = "image/" + file.fileExt
                                } else if urlString.isImageType() {
                                    file.fileType = FileTypes.IMAGE.rawValue
                                    file.mimeType = "image/" + file.fileExt
                                }else if attachment.hasItemConformingToTypeIdentifier(self.typeVideo) {
                                    file.fileType = FileTypes.VIDEO.rawValue
                                    file.mimeType = "video/" + file.fileExt
                                } else if urlString.isVideoType() {
                                    file.fileType = FileTypes.VIDEO.rawValue
                                    file.mimeType = "video/" + file.fileExt
                                } else {
                                    file.fileType = FileTypes.APPLICATION.rawValue
                                    file.mimeType = "application/" + file.fileExt
                                }
                                self.attachmentData.append(file)
                                
                                if index == (content.attachments?.count)! - 1 {
                                    DispatchQueue.main.async {
                                        let userDefaults = UserDefaults(suiteName: self.groupName)
                                        userDefaults?.set(try? PropertyListEncoder().encode(self.attachmentData), forKey: self.KeyShareAttachment)
                                        userDefaults?.synchronize()
                                        self.openMainApp()
                                    }
                                }
                                
//                                DispatchQueue.main.async {
//                                    let alert = UIAlertController(title: "Alert", message: "-> \(file.name) ->  \(file.base64String) ->  \(file.fileExt) -> \(file.mimeType)", preferredStyle: .alert)
//
//                                    let action = UIAlertAction(title: "OK", style: .cancel) { _ in
//                                        alert.dismiss(animated: true, completion: {
//                                            self.extensionContext!.completeRequest(returningItems: nil, completionHandler: nil)
//                                        })
//                                    }
//
//                                    alert.addAction(action)
//                                    self.present(alert, animated: true, completion: nil)
//                                }
                            }
                            catch let exp {
                                print("GETTING EXCEPTION \(exp.localizedDescription)")
                                self.showErrorPopup(exp)
                            }
                        }
                    }
                })
            }
            else if attachment.hasItemConformingToTypeIdentifier(typePropertyList) {
                attachment.loadItem(forTypeIdentifier: typePropertyList, options: nil, completionHandler: { (decoder, error) in
                    guard error == nil else {
                        self.showErrorPopup(error)
                        return
                    }
                    guard let dictionary = decoder as? NSDictionary else {
                        self.showErrorPopup(error)
                        return
                    }
                    guard let results = dictionary.value(forKey: NSExtensionJavaScriptPreprocessingResultsKey) as? NSDictionary else {
                        self.showErrorPopup(error)
                        return
                    }
                    let properties = [
                        "url": results.value(forKey: "URL") as? String,
                        "title": results.value(forKey: "title") as? String,
                        "quote": results.value(forKey: "selectedText") as? String
                    ] as? [String: String]
                    
                    let file = ShareFileModel()
                    file.fileType = FileTypes.WEB_URL.rawValue
                    file.name = properties!["url"] ?? ""
                    self.attachmentData.append(file)

                    if index == (content.attachments?.count)! - 1 {
                        DispatchQueue.main.async {
                            let userDefaults = UserDefaults(suiteName: self.groupName)
                            userDefaults?.set(try? PropertyListEncoder().encode(self.attachmentData), forKey: self.KeyShareAttachment)
                            userDefaults?.synchronize()
                            self.openMainApp()
                        }
                    }
                })
            }
            else if attachment.hasItemConformingToTypeIdentifier(typeData) {
                attachment.loadItem(forTypeIdentifier: typeData, options: nil) { [weak self] data, error in

                    if error == nil, let url = data as? URL, let this = self {
                        do {
                            let rawData = try Data(contentsOf: url)
                            let name = url.lastPathComponent
                            let file = ShareFileModel()
                            file.name = name
//                            file.base64String = rawData
                            file.fileExt = String(name.split(separator: ".").last!)
                            
                            let documentDirectory = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: this.groupName)
                            guard let tmpURL = documentDirectory?.appendingPathComponent(name) else {return}
                            do {
                                try rawData.write(to: tmpURL, options: [])
                                file.fileURL = tmpURL
                            } catch {
                                print("ShareViewController->writeFile->error  ===> \(error)")
                            }
                            
                            if attachment.hasItemConformingToTypeIdentifier(this.typeImage) {
                                file.fileType = FileTypes.IMAGE.rawValue
                                file.mimeType = "image/" + String(name.split(separator: ".").last!)
                            } else if attachment.hasItemConformingToTypeIdentifier(this.typeVideo) {
                                file.fileType = FileTypes.VIDEO.rawValue
                                file.mimeType = "video/" + String(name.split(separator: ".").last!)
                            } else {
                                file.fileType = FileTypes.APPLICATION.rawValue
                                file.mimeType = "application/" + String(name.split(separator: ".").last!)
                            }

                            this.attachmentData.append(file)

                            if index == (content.attachments?.count)! - 1 {
                                DispatchQueue.main.async {
                                    let userDefaults = UserDefaults(suiteName: this.groupName)
                                    userDefaults?.set(try? PropertyListEncoder().encode(this.attachmentData), forKey: this.KeyShareAttachment)
                                    userDefaults?.synchronize()
                                    this.openMainApp()
                                }
                            }
                        }
                        catch let exp {
                            print("GETTING EXCEPTION \(exp.localizedDescription)")
                            this.extensionContext!.completeRequest(returningItems: nil, completionHandler: nil)
                        }
                    }
                    else if error == nil, let image = data as? UIImage, let this = self {
                        print("Share internal image")
                        var newImage = image
                        if image.size.width > 500 {
                            newImage = this.resizeImage(image: image, targetSize: CGSize(width: 500.0, height: 500.0))
                        }

                        let rawData = newImage.jpegData(compressionQuality: 0.5)
                        let file = ShareFileModel()
                        file.name = UUID().uuidString + ".jpeg"
                        file.base64String = rawData
                        file.fileExt = "jpeg"
                        file.mimeType = "image/jpeg"
                        file.fileType = FileTypes.IMAGE.rawValue
                        self?.attachmentData.append(file)

                        if index == (content.attachments?.count)! - 1 {
                            DispatchQueue.main.async {
                                let userDefaults = UserDefaults(suiteName: this.groupName)
                                userDefaults?.set(try? PropertyListEncoder().encode(this.attachmentData), forKey: this.KeyShareAttachment)
                                userDefaults?.synchronize()
                                this.openMainApp()
                            }
                        }
                    }
                    else {
                        print("GETTING ERROR")
                        print("errorerror -> \(error) data -> \(type(of: data))")
                        self?.showErrorPopup(error)
                    }
                }
            }
        }
    }
    
    private func showErrorPopup(_ error: Error?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: error?.localizedDescription ?? "Can't share this file.", preferredStyle: .alert)

            let action = UIAlertAction(title: "OK", style: .cancel) { _ in
                alert.dismiss(animated: true, completion: {
                    self.extensionContext!.completeRequest(returningItems: nil, completionHandler: nil)
                })
            }

            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }

    private func openMainApp() {
        self.extensionContext?.completeRequest(returningItems: nil, completionHandler: { _ in
            guard let url = URL(string: self.appURL) else { return }
            _ = self.openURL(url)
        })
    }

    @objc private func openURL(_ url: URL) -> Bool {
        var responder: UIResponder? = self
        while responder != nil {
            if let application = responder as? UIApplication {
                return application.perform(#selector(openURL(_:)), with: url) != nil
            }
            responder = responder?.next
        }
        return false
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
}

open class ShareFileModel: Codable {
    var name : String = ""
    var base64String : Data!
    var fileExt : String = ""
    var mimeType : String = ""
    var fileType : String = ""
    var fileURL : URL?
}

public enum FileTypes: String {
    case IMAGE = "image"
    case VIDEO = "video"
    case APPLICATION = "application"
    case WEB_URL = "web_url"
}

extension String {
    public func isImageType() -> Bool {
        let imageFormats = ["jpg", "png", "gif", "tiff", "ico", "svg", "webp"]

        if URL(string: self) != nil  {

            let extensi = (self as NSString).pathExtension

            return imageFormats.contains(extensi)
        }
        return false
    }
    
    public func isVideoType() -> Bool {
        let imageFormats = ["mp4", "mov", "avi", "ogg", "wmv", "webm"]

        if URL(string: self) != nil  {

            let extensi = (self as NSString).pathExtension

            return imageFormats.contains(extensi)
        }
        return false
    }
}
