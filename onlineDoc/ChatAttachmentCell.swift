//
//  ChatViewCell.swift
//  onlineDoc
//
//  Created by Saqlain on 22/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation


class ChatAttachmentCell: UITableViewCell {
    
    var memberId = 0
    var controller = AVPlayerViewController()
    let supportedVideoFormats = ["mp4","mov", "avi","mkv"]
    @IBOutlet weak var personImageView: UIImageView!{
        didSet{
            personImageView.layer.cornerRadius = personImageView.frame.size.height / 2
            personImageView.clipsToBounds = true
            personImageView.layer.borderColor = UIColor.white.cgColor
            personImageView.layer.borderWidth = 0.5
        }
    }
    @IBOutlet weak var personActiveStatus: UIView!{
        didSet{
            personActiveStatus.layer.cornerRadius = personActiveStatus.frame.size.height / 2
            personActiveStatus.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var attachmentImageView: UIImageView!{
        didSet{
            attachmentImageView.layer.cornerRadius = 10
            attachmentImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var commentView: UIView!{
        didSet{
            commentView.layer.cornerRadius = 6.0
            commentView.clipsToBounds = true
        }
    }
    @IBOutlet weak var commentAtLabel: UILabel!
    @IBOutlet weak var videoView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setAttachment(_ indexPath: IndexPath, data : Message){
            
           
            
            if let  moment = data.created_at{
                self.commentAtLabel.text = AppConfig.shared.convertToDate(from: moment.UTCToLocal).getMoment() + " ago"
            }
            if data.isServerFile{
                //TODO server file
                if data.file_ext != nil{
                    if data.file_ext == "jpg"{
                        ///TODO Show Image from Url
                        self.attachmentImageView.isHidden = false
                        self.videoView.isHidden = true
                        if let image = data.file_name, let path = data.file_path,image != "" && path != ""{
                            let imagePath = image + path
                            if let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(imagePath){
                                self.attachmentImageView.imageFromServer(imageUrl: url)
                            }else{
                                self.attachmentImageView.image = nil
                            }
                        }else{
                            self.attachmentImageView.image = nil
                        }
                        
                    }else if self.supportedVideoFormats.contains((data.file_ext?.lowercased())!){
                        /// Hanlde the video thing here
                        self.attachmentImageView.isHidden = true
                        self.videoView.isHidden = false
                        self.attachmentImageView.isHidden = true
                        self.videoView.isHidden = false
                        if let video = data.file_name, let path = data.file_path,video != "" && path != ""{
                            let videoPath = video + path
                            if let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(videoPath){
                                let player = AVPlayer(url: url)
                                controller.player = player
                                controller.view.frame = self.attachmentImageView.frame
                                self.attachmentImageView.addSubview(controller.view)
                                player.play()
                            }else{
                                self.attachmentImageView.image = nil
                            }
                        }
                    }else{
                        /// Hanlde the other attachment here like documents
                        self.attachmentImageView.image = #imageLiteral(resourceName: "attached")
                    }
                }else{
                    self.attachmentImageView.image = nil
                }
            }else{
                /// show local attached  file uploaded by user
                self.attachmentImageView.image = data.image
                
            }
            
            
            if let image = data.file_name,let path = data.file_path{
                let img = path + image
                if let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(img){
    //                self.myAttachmentImageView.imageFromServer(imageUrl: url)
                }else{
    //                self.myAttachmentImageView.image = nil
                }
            }else{
    //            self.myAttachmentImageView.image = nil
            }
        }
    
    /*
    func setCellData(indexPath: IndexPath, data : Message, member: GroupMember){
        
        if let message = data.message{
            self.commentTextLabel.text = message
        }else{
            self.commentTextLabel.text = nil
        }
        
        if let  moment = data.created_at{
            self.commentAtLabel.text = AppConfig.shared.convertToDate(from: moment.UTCToLocal).getMoment() + " ago"
        }
        
        
        
        if data.isServerFile{
            //TODO server file
            if data.file_ext != nil{
                if data.file_ext == "jpg"{
                    ///TODO Show Image from Url
                    if let image = data.file_name, let path = data.file_path,image != "" && path != ""{
                        let imagePath = image + path
                        if let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(imagePath){
                            self.attachmentImageView.imageFromServer(imageUrl: url)
                        }else{
                            self.attachmentImageView.image = nil
                        }
                    }else{
                        self.attachmentImageView.image = nil
                    }
                    
                }else if self.supportedVideoFormats.contains((data.file_ext?.lowercased())!){
                    /// Hanlde the video thing here
//
//                    if let video = data.file_name, let path = data.file_path,video != "" && path != ""{
//                        let imagePath = video + path
//                        if let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(imagePath){
//                            //self.attachmentImageView.imageFromServer(imageUrl: url)
//                            let player = AVPlayer(url: url)
//                            let controller=AVPlayerViewController()
//                            controller.player=player
//                            controller.view.frame = self.attachmentImageView.frame
//                            self.attachmentImageView.addSubview(controller.view)
////                            self.attachmentImageView.addChildViewController(controller)
//                            player.play()
//                        }else{
//                            self.attachmentImageView.image = nil
//                        }
//                    }
                }else{
                    /// Hanlde the other attachment here like documents
                    self.attachmentImageView.image = #imageLiteral(resourceName: "attached")
                }
            }else{
                self.attachmentImageView.image = nil
            }
        }else{
            /// show local attached  file uploaded by user
            self.attachmentImageView.image = data.image
            
        }
        
        
        
        
//
//
//        if let name = AppConfig.shared.user.name{
//            self.myNameLabel.text = name.capitalized
//        }
//
//        if let message = data.message{
//            self.myMessageTextLabel.text = message
//        }
        
        
        
        if let image = data.file_name,let path = data.file_path{
            let img = path + image
            if let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(img){
//                self.myAttachmentImageView.imageFromServer(imageUrl: url)
            }else{
//                self.myAttachmentImageView.image = nil
            }
        }else{
//            self.myAttachmentImageView.image = nil
        }
    }
    */
    func setAttachmentData(at indexPath: IndexPath, with data: ThreadComment){
        
        self.videoView.isHidden = true
        
        self.attachmentImageView.image = nil
        self.attachmentImageView.isHidden = false
        self.controller = AVPlayerViewController()
        
        
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.backgroundColor = UIColor.red
        nameLabel.numberOfLines = 0
        nameLabel.sizeToFit()
        self.attachmentImageView.translatesAutoresizingMaskIntoConstraints = false
        
        
        if let commentByName = data.commentBy?.name.capitalized{
            nameLabel.text  = commentByName
        }
        
        if let commentBy = data.commentBy?.profilePicture, commentBy != ""{
            if let imageUrl = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(commentBy){
                self.personImageView.imageFromServer(imageUrl: imageUrl, placeholder: #imageLiteral(resourceName: "userDefaultIcon"))
            }else{
                self.personImageView.image = #imageLiteral(resourceName: "userDefaultIcon")
            }
        }else{
            self.personImageView.image = #imageLiteral(resourceName: "userDefaultIcon")
        }
        if let moment = data.added_date{
            self.commentAtLabel.text = AppConfig.shared.convertToDate(from: moment.UTCToLocal).getMoment() + " ago"
        }
        
        if data.file_main_type == "image"{
            ///TODO Show Image from Url
            self.videoView.isHidden = true
            self.attachmentImageView.isHidden = false
            self.attachmentImageView.image = nil
            if let image = data.file_name, let path = data.file_path,image != "" && path != ""{
                let imagePath = path + image
                if let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(imagePath){
                    self.attachmentImageView.imageFromServer(imageUrl: url)
                }else{
                    self.attachmentImageView.image = nil
                }
            }else{
                self.attachmentImageView.image = nil
            }
        }else if data.file_main_type == "video"{
            //TODO Show image from Server
            self.attachmentImageView.image = nil
            self.attachmentImageView.isHidden = true
            self.videoView.isHidden = false
            if let name = data.file_name, let path = data.file_path, name != "" && path != ""{
                let videoPath = path + name
                if let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(videoPath){
                    let player = AVPlayer(url: url)
                    controller.player = player
                    controller.view.frame = self.attachmentImageView.frame
                    self.attachmentImageView.addSubview(controller.view)
                    player.play()
                }else{
                    self.attachmentImageView.image = nil
                }
            }
        }else if data.file_main_type == "application"{
            //TODO Show attachment icon for the time being
            self.attachmentImageView.image = #imageLiteral(resourceName: "attached")
            self.attachmentImageView.contentMode = .scaleAspectFit
        }else{
            self.controller.removeFromParent()
            self.attachmentImageView.image = nil
        }
        
        self.commentView.addSubview(nameLabel)
        nameLabel.leadingAnchor.constraint(equalTo: commentView.leadingAnchor, constant: 20).isActive = true
        nameLabel.topAnchor.constraint(equalTo: commentView.topAnchor, constant: 20).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: commentView.trailingAnchor, constant: 20).isActive = true
        self.attachmentImageView.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 10).isActive = true
        self.attachmentImageView.leadingAnchor.constraint(equalTo: commentView.leadingAnchor, constant: 20).isActive = true
        self.attachmentImageView.trailingAnchor.constraint(equalTo: commentView.trailingAnchor, constant: 20).isActive = true
        self.attachmentImageView.bottomAnchor.constraint(equalTo: commentView.bottomAnchor, constant: 8).isActive = true
        self.attachmentImageView.widthAnchor.constraint(equalToConstant: 317).isActive = true
        self.attachmentImageView.heightAnchor.constraint(equalToConstant: 209).isActive = true
        self.attachmentImageView.contentMode = .scaleAspectFill
    }
}

