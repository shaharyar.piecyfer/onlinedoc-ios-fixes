//
//  InviteAlertVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 09/11/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField
import FlagPhoneNumber

class InviteAlertVC: BaseVC {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var _title: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var uiContact: UIView!
    @IBOutlet weak var tfContact: FPNTextField!
    @IBOutlet weak var uiEmail: UIView!
    @IBOutlet weak var tfEmail: RAGTextField!{
        didSet{
            tfEmail.setUnderlineField()
            tfEmail.textPadding = UIEdgeInsets(top: 8.0, left: 0.0, bottom: 8.0, right: 0.0)
            tfEmail.placeholder = "Email"
        }
    }
    @IBOutlet weak var lblContactTitle: UILabel!
    @IBOutlet weak var popupViewYConstraint: NSLayoutConstraint!
    
    var phoneNumber: String = ""
    var isValidPhoneNumber: Bool = false
    var inviteBy: NewChatSelections = .EMAIL
    var invitedUserPhoneEmail = ""
    var searchedEmail: String = ""
    var searchedPhone: String = ""
    
    var onClickDone:((String, NewChatSelections) -> ())?
    var onClickCancel:(() -> ())?
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.setupTheme()
        self.keyboardSetting()
    }
    
    private func setupView() {
        self.view.backgroundColor = AppColors.blackColor.withAlphaComponent(0.2)
        _title.text = inviteBy == .EMAIL ? "Invite by email".localized() : "Invite by SMS".localized()
        uiContact.isHidden = inviteBy == .EMAIL
        uiEmail.isHidden = inviteBy == .PHONE
        btnDone.setTitle(DoneText.localized(), for: .normal)
        btnCancel.setTitle(cancelText.localized(), for: .normal)
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.onTapBackground))
        self.mainView.addGestureRecognizer(gesture)
        self.setupPhoneNumber()
        self.lblContactTitle.text = ContactNumberText.localized()
        
        if !searchedEmail.isEmpty {
            self.tfEmail.text = searchedEmail
        }
        else if !searchedPhone.isEmpty {
            self.tfContact.text = searchedPhone
        }
        
        inviteBy == .EMAIL ? tfEmail.becomeFirstResponder() : tfContact.becomeFirstResponder()
        
        self.tfEmail.delegate = self
        self.tfContact.delegate = self
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: InviteAlertVC.setTheme)
    }
    
    @objc func onTapBackground(sender : UITapGestureRecognizer) {
//        self.dismissVC()
    }
    
    func keyboardSetting(){
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillShowNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillHideNotification,object: nil)
    }
    
    @objc func keyboardNotification(_ notification: NSNotification) {
        let isShowing = notification.name == UIResponder.keyboardWillShowNotification
        
        if let userInfo = notification.userInfo {
//            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.intValue ?? Int(UIView.AnimationOptions.curveEaseInOut.rawValue)
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: UInt(animationCurveRaw))
            self.popupViewYConstraint?.constant = isShowing ? -50 : 0
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    @IBAction func onClickDone(_ sender: UIButton) {
        let (isValid, message) = self.validateField()
        if isValid {
            self.dismiss(animated: true, completion: {
                self.onClickDone?(self.invitedUserPhoneEmail, self.inviteBy)
            })
        }
        else {
            self.showAlert(To: "Alert".localized(), for: message) {_ in}
        }
    }
    
    @IBAction func onClickCancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.onClickCancel?()
        })
    }
    
    private func setupPhoneNumber() {
//        tfContact.setCountries(including: [.DK])
        tfContact.setFlag(countryCode: .DK)
        tfContact.displayMode = .list
        tfContact.delegate = self
        tfContact.attributedPlaceholder = NSAttributedString(string: tfContact.placeholder ?? "2015550123", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        tfContact.phoneCodeRightPadding = 8.0
        
        listController.setup(repository: tfContact.countryRepository)
        listController.didSelect = { [weak self] country in
            self?.tfContact.setFlag(countryCode: country.code)
        }
    }
    
    private func validateField() -> (Bool, String) {
        var message = ""
        if inviteBy == .EMAIL {
            let email = self.tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
            if email.isEmpty {
                message = "Valid Email Required".localized()
                return (false, message)
            }else if !AppConfig.shared.isValidEmail(testStr: email){
                message = "\(email) \(InvalidEmail.localized())"
                return (false, message)
            }else{
                self.invitedUserPhoneEmail = email
                return (true, message)
            }
        }
        else if inviteBy == .PHONE {
            let contact = self.phoneNumber
            if contact.isEmpty {
                message = ValidConactNumberRequired.localized()
                return (false, message)
            }else if !self.isValidPhoneNumber {
                message = InvalidConactNumber.localized
                return (false, message)
            }else{
                self.invitedUserPhoneEmail = contact
                return (true, message)
            }
        }
        else {
            return (false, message)
        }
    }
}

//MARK: - UITextFieldDelegate
extension InviteAlertVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension InviteAlertVC: FPNTextFieldDelegate {
    
    /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        listController.title = "txt_select_country".localized()
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))
        self.present(navigationViewController, animated: true, completion: nil)
    }
    
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
    }
    
    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        self.isValidPhoneNumber = isValid
        self.phoneNumber = textField.getFormattedPhoneNumber(format: .E164) ?? ""
    }
}

extension InviteAlertVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        tfContact.textColor = theme.settings.titleColor
    }
}
