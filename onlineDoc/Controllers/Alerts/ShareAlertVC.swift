//
//  ShareAlertVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 04/01/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

class ShareAlertVC: BaseVC {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var _title: UILabel!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnNewThread: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var onClickThread:(() -> Void)?
    var onClickMessage:(() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = AppColors.blackColor.withAlphaComponent(0.2)
        _title.text = createIn.localized()
        btnNewThread.setTitle(group.localized(), for: .normal)
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.onClickCancel))
        self.mainView.addGestureRecognizer(gesture)
    }
    
    @objc func onClickCancel(sender : UITapGestureRecognizer) {
        appDelegate.attachements.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickMessage(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.onClickMessage?()
        })
    }
    
    @IBAction func onClickThread(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.onClickThread?()
        })
    }
}
