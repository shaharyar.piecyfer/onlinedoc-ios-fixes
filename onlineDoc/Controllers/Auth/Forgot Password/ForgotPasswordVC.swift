//
//  ForgotPasswordVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 08/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField

class ForgotPasswordVC: BaseVC {

    @IBOutlet weak var forgotPasswordTextField: RAGTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppConfig.shared.setunderLine(for: self.forgotPasswordTextField)
        AppConfig.shared.setTextfieldIcon(for: self.forgotPasswordTextField, addIcon: UIImage(named: "email")!)
        
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: ForgotPasswordVC.setTheme)
    }
    
    @IBAction func resetPassword(){
        if self.forgotPasswordTextField.text!.isEmpty{
            self.showAlert(To: ForgotEmailTitle.localized(), for: ForgotEmailMessage.localized()) {_ in}
        }else if !AppConfig.shared.isValidEmail(testStr: self.forgotPasswordTextField.text!.localized()){
            self.showAlert(To: ValidationAlertTitle.localized(), for: self.forgotPasswordTextField.text! + InvalidEmail.localized()) {_ in}
            
        }else {
            self.showLoader()
            RequestManager.shared.resetPassword(email: self.forgotPasswordTextField.text!){ (status,response) in
                if status{
                    if let _ = response as? ResetPasswordSuccessResponse{
                        let message = ResetPasswordResponseMessage.localized().replacingOccurrences(of: "#MAIL#", with: self.forgotPasswordTextField.text!)
                        self.showAlert(To: ForgetPaswortAlertTitle.localized(), for: message) { [weak self] _ in
                            guard let `self` = self else {return}
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }else{
                    
                }
            }
        }
    }
}


extension ForgotPasswordVC: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
}

extension ForgotPasswordVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        forgotPasswordTextField.textColor = theme.settings.titleColor
    }
}
