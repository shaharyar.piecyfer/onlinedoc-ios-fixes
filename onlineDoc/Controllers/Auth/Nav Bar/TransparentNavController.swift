//
//  TransparentNavController.swift
//  onlineDoc
//
//  Created by Piecyfer on 02/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class TransparentNavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
    }
}
