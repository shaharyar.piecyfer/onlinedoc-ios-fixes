//
//  ResetPasswordVC.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 01/07/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField

class ResetPasswordVC: BaseVC {
    @IBOutlet weak var newPasswordField : RAGTextField!{
        didSet {
            AppConfig.shared.setunderLine(for: newPasswordField)
        }
    }
    @IBOutlet weak var confirmPasswordField : RAGTextField!{
        didSet {
            AppConfig.shared.setunderLine(for: confirmPasswordField)
        }
    }
    @IBOutlet weak var lblDetailRestPass : UILabel!
    
    var queryToken = ""{
        didSet{
            self.userData.token = queryToken
        }
    }
    var email = ""{
        didSet{
            self.userData.email = email
        }
    }
    var tempPass = ""
    
    var userData = NewPassword()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: ResetPasswordVC.setTheme)
    }
    
    @IBAction func resetPassword(){
        let (isValid, message) = self.validationStatus()
        if isValid {
            if tempPass.isEmpty {
                RequestManager.shared.resetPassword(self.userData) { (status, jsonData) in
                    if status{
                        if let response = jsonData as? GeneralSuccessResult {
                            if let _ = response.message {
                                self.showAlert(To: PasswordUpdateHeader.localized(), for: PasswordUpdateMessage.localized()) { [weak self] _ in
                                    guard let `self` = self else {return}
                                    self.performSegue(withIdentifier: "SegueToLogin", sender: self)
                                }
                            }
                        }
                    }
                }
            }
            else {
                var passwordCreds = ResetPassword()
                passwordCreds.oldPassword = tempPass
                passwordCreds.newPassword = self.userData.newPassword
                passwordCreds.newConfirmPassword = self.userData.confirmPassword
                RequestManager.shared.updatePassword(passwordCreds: passwordCreds) { (status,responseData) in
                    if status {
                        if let data = responseData as? GeneralSuccessResult {
                            var credentialsDic = UserDefaults.standard.object(forKey: CREDENTIALS_DICT) as? [[String:Any]] ?? [[String:Any]]()
                            if let index = (credentialsDic.firstIndex(where: { $0["userName"] as? String == AppConfig.shared.user.email })){
                                credentialsDic[index]["passCode"] = passwordCreds.newPassword
                                UserDefaults.standard.set(credentialsDic, forKey: CREDENTIALS_DICT)
                            }
                            self.view.endEditing(true)
                            self.showAlert(To: "", for: PasswordChangeMessage.localized()) { [weak self] _ in
                                guard let `self` = self else {return}
                                let vc = self.getVC(storyboard: .HOME_TAB, vcIdentifier: "TabsHomeScene") as! TabBarVC
                                UIApplication.shared.windows.first?.rootViewController = vc
                            }
                        }else{
                            self.view.endEditing(true)
                        }
                    }
                }
            }
        }else{
            self.showAlert(To: ValidationAlertTitle.localized(), for: message) {_ in}
        }
    }
    
    private func validationStatus() -> (Bool, String) {
        var status = true
        var message = ""
        var count = 0
        
        if self.newPasswordField.text!.isEmpty{
            message += PasswordText.localized() + ", "
            count = count + 1
            status = false
        }else {
            self.userData.newPassword = self.newPasswordField.text!
        }
        
        if self.confirmPasswordField.text!.isEmpty{
            message += ConfirmPasswordText.localized() + ", "
            count = count + 1
            status = false
        }else {
            self.userData.confirmPassword = self.confirmPasswordField.text!
        }
        
        if let password = self.newPasswordField.text, let confirmPassword = self.confirmPasswordField.text{
            if password != confirmPassword{
                message += PasswordMissmatch.localized() + ", "
                count += 1
                status = false
            }
        }
        
        if self.newPasswordField.text!.count  < 6{
            message += "\n\(PaswordLength.localized())"
            status = false
        }
        if self.confirmPasswordField.text!.count < 6 {
            message += "\n\(ConfirmPasswordLength.localized())"
            status = false
        }
        
        if count == 1{
            message = String(message.dropLast(2))
            message += IsRequiredString.localized()
            count = 0
        }else if count > 1{
            message = String(message.dropLast(2))
            message += AreRequiredString.localized()
            count   = 0
        }
        
        return (status, message)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueToLogin"{
            if let dvc = segue.destination as? LoginVC{
                dvc.email = self.userData.email
            }
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    /**/
    
}

extension ResetPasswordVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.newPasswordField{
            _ = self.confirmPasswordField.becomeFirstResponder()
        }else if textField == confirmPasswordField{
            _ = self.confirmPasswordField.resignFirstResponder()
            self.resetPassword()
        }
        return true
    }
    
}

extension ResetPasswordVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        newPasswordField.textColor = theme.settings.titleColor
        newPasswordField.tintColor = theme.settings.titleColor
        confirmPasswordField.textColor = theme.settings.titleColor
        confirmPasswordField.tintColor = theme.settings.titleColor
        lblDetailRestPass.textColor = theme.settings.subTitleColor
    }
}


struct NewPassword{
    var token = ""
    var email = ""
    var newPassword = ""
    var confirmPassword = ""
}
