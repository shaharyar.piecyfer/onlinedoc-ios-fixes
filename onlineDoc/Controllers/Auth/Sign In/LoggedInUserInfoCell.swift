//
//  LoggedInUserInfoCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/11/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class LoggedInUserInfoCell: UITableViewCell {
    
//    MARK: - Variables and Instances
    var deleteActionCompletion: ((Int) -> ())?
    var cellIndex: Int?
    //    MARK: - Outlets
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton! {
        didSet {
            let image = UIImage(named: "del")?.withRenderingMode(.alwaysTemplate)
            deleteButton.setImage(image, for: .normal)
            deleteButton.tintColor = AppColors.redColor
        }
    }
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView! {
        didSet {
            userProfileImageView.contentMode = .scaleAspectFill
            userProfileImageView.layer.cornerRadius = userProfileImageView.frame.size.width/2
            userProfileImageView.clipsToBounds = true
            userProfileImageView.layer.borderWidth = 0.5
            userProfileImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var clippingView: UIView!
    @IBOutlet weak var uiCountView: UIView!
    @IBOutlet weak var uiChat: UIView!
    @IBOutlet weak var uiNotification: UIView!
    @IBOutlet weak var lblChatCount: UILabel!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var ivBellIcon: UIImageView!
    
//    MARK: - Actions
    @IBAction func didTapDeleteIcon(_ sender: UIButton) {
        print("Delete Icon tapped!!")
        self.deleteActionCompletion?(cellIndex!)
    }
    //    MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Make it card-like
        containerView.layer.cornerRadius = 10
        
        ivBellIcon.image = ivBellIcon.image?.withRenderingMode(.alwaysTemplate)
        ivBellIcon.tintColor = .white
        uiCountView.isHidden = true
        
        //Generic theme
        Themer.shared.register(
            target: self,
            action: LoggedInUserInfoCell.applyTheme)
        let labels: [UILabel] = AppConfig.shared.getSubviewsOf(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        Themer.shared.register(
            target: self,
            action: LoggedInUserInfoCell.setTheme)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setCellData(indexPath: IndexPath, data:GroupMember){
        if let name = data.name{
            self.userNameLabel.isHidden = false
            self.userNameLabel.text = name
        }
        else {
            self.userNameLabel.isHidden = true
        }
        
        if let email = data.email {
            self.userEmailLabel.isHidden = false
            self.userEmailLabel.text = email
        }
        else if let phoneNo = data.phone {
            self.userEmailLabel.isHidden = false
            self.userEmailLabel.text = phoneNo
        }
        else {
            self.userEmailLabel.isHidden = true
        }
        
        if (data.name == data.phone) {
            userNameLabel.isHidden = true
        }
        else {
            userNameLabel.isHidden = false
        }
        
        self.cellIndex = indexPath.row
        if let imageUrl = data.profilePicture, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.userProfileImageView.imageFromServer(imageUrl: url!)
        }else{
            self.userProfileImageView.image = UIImage(named: "ic_user")
        }
    }
    
    func setCellData(indexPath: IndexPath, data:User){
        if let name = data.full_name {
            self.userNameLabel.isHidden = false
            self.userNameLabel.text = name
        }
        else if let name = data.name {
            self.userNameLabel.isHidden = false
            self.userNameLabel.text = name
        }
        else {
            self.userNameLabel.isHidden = true
        }
        
        if let email = data.email {
            self.userEmailLabel.isHidden = false
            self.userEmailLabel.text = email
        }
        else if let phoneNo = data.phone {
            self.userEmailLabel.isHidden = false
            self.userEmailLabel.text = phoneNo
        }
        else {
            self.userEmailLabel.isHidden = true
        }
        
        if (data.name == data.phone) {
            userNameLabel.isHidden = true
        }
        else {
            userNameLabel.isHidden = false
        }
        
        self.cellIndex = indexPath.row
        if let imageUrl = data.file?.src, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.userProfileImageView.imageFromServer(imageUrl: url!)
        }else{
            self.userProfileImageView.image = UIImage(named: "ic_user")
        }
    }
    
    func setOnlyOneMemberData(userName: String, email: String, profileImageStr: String) {
        self.userNameLabel.text = userName
        self.userEmailLabel.text = email
        if profileImageStr != ""{
            let url = URL(string: profileImageStr)
            self.userProfileImageView.imageFromServer(imageUrl: url!)
        }else{
            self.userProfileImageView.image = UIImage(named: "ic_user")
        }
        self.deleteButton.isHidden = true
    }
}

extension LoggedInUserInfoCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        userEmailLabel.textColor = theme.settings.subTitleColor
        lblChatCount.textColor = theme.settings.whiteColor
        lblNotificationCount.textColor = theme.settings.whiteColor
    }
}
