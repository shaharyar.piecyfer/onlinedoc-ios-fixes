//
//  LoggedInUsersVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/11/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
class LoggedInUsersVC: BaseVC {
    //    MARK: - Variables and Instantiations
    fileprivate var userCreds = LogInCredentials()
    fileprivate var defaults = UserDefaults.standard
    var appUsers: [[String:Any]]?
    var didDismissCompletion: ((Bool) -> ())?
    //    MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabel2: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var isResetView = true
    
    //    MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        let credentials = self.defaults.object(forKey: CREDENTIALS_DICT) as? [[String:Any]] ?? [[String:Any]]()
        appUsers = credentials
        let hasUserId = credentials.allSatisfy({$0.keys.contains(where: {$0 == "userId"})})
        if hasUserId {
            getLoggedInUsers()
        } else {
            tableView.reloadData()
        }
    }// End Function
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
        titleLabel.text = selectProfile.localized()
        titleLabel2.text = multipleAuthorizedLoginsOnThisDevice.localized()
    }// End Function
    
    override func viewWillDisappear(_ animated: Bool) {
        print("check for disappear")
        if isResetView {
            self.didDismissCompletion?(true)
        }
    }
    
    func getLoggedInUsers(){
        let ids = appUsers!.map({ (item) -> Int in
            Int(item["userId"] as! String)!
        })
        RequestManager.shared.getLoggedInUsersCount(data: ids) { (status, response) in
            if status {
                if let data = response as? LoggedInUserCountResultModel, let users = data.users, !users.isEmpty {
                    var updatedAppUsers = [[String:Any]]()
                    users.forEach({ (user) in
                         _ = self.appUsers!.first { (item) -> Bool in
                            if item["userId"] as! String == String(user.id!) {
                                var dic = item
                                dic.updateValue(user.message_count ?? 0, forKey: "chatUnreadCount")
                                dic.updateValue(user.notification_count ?? 0, forKey: "threadNotificationUnreadCount")
                                updatedAppUsers.append(dic)
                                return true
                            }
                            return false
                         }
                    })
                    self.appUsers = updatedAppUsers
                    self.tableView.reloadData()
                    if self.appUsers?.count == 1 {
                        if let user = self.appUsers?.first {
                            self.defaults.set(self.appUsers, forKey: CREDENTIALS_DICT)
                            self.defaults.synchronize()
                            let email = user["userName"]
                            let password = user["passCode"]
                            self.userCreds.userName = email as? String
                            self.userCreds.passCode = password as? String
                            self.logInUser()
                        }
                    }
                }
                else if let data = response as? LoggedInUserCountResultModel, let users = data.users, users.isEmpty {
                    self.appUsers = [[String:Any]]()
                    self.tableView.reloadData()
                    self.defaults.set(self.appUsers, forKey: CREDENTIALS_DICT)
                    self.defaults.synchronize()
                    self.isResetView = false
                    self.dismiss(animated: true) {
                        self.didDismissCompletion?(true)
                    }
                }
            }
        }
    }
    
    func logInUser() {
        RequestManager.shared.login(credentials: self.userCreds)  { (status, response)  in
            if status{
                if let data = response as? LoginSuccessResponse, status == true && data.result != nil{
                    if let token = data.result?.token, let user = data.result {
                        AppConfig.shared.authToken = "Bearer " + token
                        if let deviceToken = user.device_token {
                            AppConfig.shared.deviceToken = deviceToken
                        }
                        AppConfig.shared.user = user
                        if let userId = user.id{
                            AppConfig.shared.userId = String(userId)
                        }
                        if !(user.is_confirmed ?? false) {
                            let vc = CodeVerificationVC()
                            vc.email = self.userCreds.userName
                            vc.phone = user.phone ?? ""
                            vc.onVerifiedCallBack = {
                                AppConfig.shared.user.is_confirmed = true
                                let vc = self.getVC(storyboard: .HOME_TAB, vcIdentifier: "TabsHomeScene") as! TabBarVC
                                UIApplication.shared.windows.first?.rootViewController = vc
                            }
                            self.present(vc, animated: true, completion: {
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                        else if let preName = user.pre_name, preName != ""{
                            self.isResetView = false
                            let vc = self.getVC(storyboard: .HOME_TAB, vcIdentifier: "TabsHomeScene") as! TabBarVC
                            UIApplication.shared.windows.first?.rootViewController = vc
                        }
                    }
                    
                    //TODO: Implement FCM token API
//                    RequestManager.shared.setNoficationToken(AppConfig.shared.pushToken) { (status, res) in
//                        if status{
//                            if let resp = res as? GeneralSuccessResponse{
//                                print(resp.result?.message as Any)
//                            }
//                        }
//                    }
                    //TODO: Implement setOnlineStatus API
//                    RequestManager.shared.setOnlineStatus(.online) { (status, data) in
//                        if status{}
//                    }
                }else if let data = response as? Login401Response, status == true && data.errors != nil{
                    self.hideLoader()
                    guard let error = data.errors?.first else {return}
                    if error.contains("deactivate") {
                        self.showAlert(To: "Account confirmation".localized(), for: error.localized(), firstBtnStyle: .default, secondBtnStyle: .default, firstBtnTitle: reactivate.localized(), secondBtnTitle: OKText.localized()) { [weak self] (status) in
                            guard let `self` = self else {return}
                            if status {
                                self.callApiReactivate()
                            }
                        }
                    }
                    else {
                        if (error.contains("This account is not confirmed.")){
                            self.showAlert(To: "Invalid Credentials".localized(), for: error.localized()) {_ in}
                        }
                        else {
                            self.showAlert(To: "Invalid Credentials".localized(), for: error.localized()) {_ in}
                        }
                    }
                }
            }else {
                self.showAlert(To: ValidationAlertTitle.localized(), for: "") {_ in}
            }
        }
    } // End Function
    
    func callApiReactivate() {
        guard let email = self.userCreds.userName else {
            return
        }
        RequestManager.shared.reactivateUser(email: email) { (status, response) in
            if status{
                if let data = response as? GeneralSuccessResult {
                    if let message = data.message {
//                    if (message!.contains("If there is an account associated with")) {
//                        message = "If there is an account associated with this".localized() + " \(email) " + "you will receive an email with a confirmation link.".localized()
//                    }
                        self.showAlert(To: "", for: message) {_ in}
                    }
                }
            }else{
                print("Got Response with nil to complete waiting")
            }
        }
    }
    
    /*
    func sendReconfirmationLink() {
        guard let email = self.userNameTextField.text else {
            return
        }
        RequestManager.shared.resendConfirmationLink(email: email) { (status, response) in
            if status{
                if let data = response as? GeneralSuccessResponse{
                    var message = data.result?.message
                    if (message!.contains("If there is an account associated with")) {
                        message = "If there is an account associated with this".localized() + " \(email) " + "you will receive an email with a confirmation link.".localized()
                    }
                    AppConfig.shared.popAlert(To: "Confirmation Link".localized(), for: message!, callingView: self)
                }
            }else{
                print("Got Response with nil to complete waiting")
            }
        }
    }
    */
}
// MARK: - Extensions UITableView
extension LoggedInUsersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appUsers!.count
    }// End Function
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoggedInUserInfoCell.self), for: indexPath)  as? LoggedInUserInfoCell else {preconditionFailure()}
        if let name = self.appUsers![indexPath.row]["name"] as? String {
            cell.userNameLabel.isHidden = false
            cell.userNameLabel.text = name
        } else {
            cell.userNameLabel.isHidden = true
        }
        cell.userEmailLabel.text = self.appUsers![indexPath.row]["userName"] as? String
        let img = self.appUsers![indexPath.row]["image"] as? String
        if let image = img, image != ""{
            let imagePath = URL(string: image)
            cell.userProfileImageView.imageFromServer(imageUrl: imagePath!)
        }
        else {
            cell.userProfileImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        cell.cellIndex = indexPath.row
        let chatUnreadCount = self.appUsers![indexPath.row]["chatUnreadCount"] as? Int ?? 0
        let threadNotificationUnreadCount = self.appUsers![indexPath.row]["threadNotificationUnreadCount"] as? Int ?? 0
        cell.uiCountView.isHidden = (chatUnreadCount+threadNotificationUnreadCount) > 0 ? false : true
        cell.uiChat.isHidden = chatUnreadCount > 0 ? false : true
        cell.uiNotification.isHidden = threadNotificationUnreadCount > 0 ? false : true
        cell.lblChatCount.text = "\(chatUnreadCount)"
        cell.lblNotificationCount.text = "\(threadNotificationUnreadCount)"
        cell.deleteActionCompletion = ({ index in
            self.appUsers!.remove(at: index)
            self.defaults.set(self.appUsers, forKey: CREDENTIALS_DICT)
            self.defaults.synchronize()
            if (self.appUsers!.count == 0) {
                self.isResetView = false
                self.dismiss(animated: true) {
                    self.didDismissCompletion?(true)
                }
            }
            self.tableView.reloadData()
        })
        return cell
    }// End Function
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.layer.masksToBounds = true
        let radius = cell.contentView.layer.cornerRadius
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: radius).cgPath
    }// End Function
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = self.appUsers![indexPath.row]
        let email = user["userName"]
        let password = user["passCode"]
        self.userCreds.userName = email as? String
        self.userCreds.passCode = password as? String
        self.logInUser()
    }// End Function
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: LoggedInUserInfoCell.self), bundle: nil), forCellReuseIdentifier: String(describing: LoggedInUserInfoCell.self))
        
        self.tableView.separatorStyle = .none
    }
    
}
