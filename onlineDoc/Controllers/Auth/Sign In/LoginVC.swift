//
//  ViewController.swift
//  onlineDoc
//
//  Created by Piecyfer  on 10/10/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox
import RAGTextField
import LocalAuthentication

class LoginVC: BaseVC, UIPopoverPresentationControllerDelegate {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var parentView : UIView!
    @IBOutlet weak var forgotPasswordButton:UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnSignin: UIButton!
    @IBOutlet weak var checkMarkLabel : UILabel!{
        didSet{
            checkMarkLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.markCheck)))
        }
    }
    @IBOutlet weak var userNameTextField: RAGTextField!{
        didSet {
            print("hereInEmailField")
            userNameTextField.delegate = self
            AppConfig.shared.setunderLine(for: userNameTextField)
        }
    }
    @IBOutlet weak var userPasswordTextField: RAGTextField!{
        didSet{
            userPasswordTextField.delegate = self
            AppConfig.shared.setunderLine(for: userPasswordTextField)
        }
    }
    @IBOutlet weak var checkMarkView: UIView!
    @IBOutlet weak var checkBoxView: BEMCheckBox!
    @IBOutlet weak var changeLanguageButton: UIButton!
    @IBOutlet weak var BiometricsSymbolsStackView: UIStackView!
    @IBOutlet weak var biometricButton: UIButton!
    @IBOutlet weak var lblVersion : UILabel!
    
    //MARK: - Local Properties
    fileprivate var userCreds = LogInCredentials()
    fileprivate var defaults = UserDefaults.standard
    fileprivate var appUsers: [[String:Any]]!
    var email = ""
    let context = LAContext()
    var strAlertMessage = String()
    var error: NSError?
    var didAutoShowFaceTouchLogin = false
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutSubviewFields()
        self.layoutCheckBox()
        self.forgotPasswordButton.isEnabled = true
        self.forgotPasswordButton.isUserInteractionEnabled = true
        
        self.setupTheme()
    }// End Fucntion
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: LoginVC.setTheme)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if email != ""{
            self.userNameTextField.text = email
        }
    }// End Fucntion
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
        self.LoadViewWithData()
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        switch AppConfig.shared.currentLanguage() {
        case .DANISH:
            changeLanguageButton.setTitle("DK", for: .normal)
            AppConfig.shared.setLanguage(lang: .DANISH)
        default:
            changeLanguageButton.setTitle("ENG", for: .normal)
            AppConfig.shared.setLanguage(lang: .ENGLISH)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }// End Fucntion
    
    //MARK: - Local Functions
    func LoadViewWithData() {
        self.loadUsers()
    }
    
    func loadUsers() {
        let credentials = self.defaults.object(forKey: CREDENTIALS_DICT) as? [[String:Any]] ?? [[String:Any]]()
        if credentials.count > 0 {
            if credentials.allSatisfy({$0.keys.contains(where: {$0 == "userId"})}) {
                self.appUsers = credentials
                print("Found Credentials \(self.appUsers.count)")
            } else {
                print("Delete all Credentials")
                self.defaults.set(self.appUsers, forKey: CREDENTIALS_DICT)
            }
        }else{
            print("No Credentials Yet")
            self.appUsers = credentials
        }
        self.checkBiometricsAvailabilityForDevice()
    }// End Fucntion
    
    func layoutSubviewFields() {
        AppConfig.shared.setTextfieldIcon(for: self.userNameTextField, addIcon: UIImage(named: "user")!, location: .left)
        if let passwordIcon = UIImage(named: "secureEntry"){
            AppConfig.shared.setTextfieldIcon(for: self.userPasswordTextField, addIcon: passwordIcon, location: .left)
        }
        if let version = AppConfig.shared.appVersion() {
            lblVersion.text = "v\(version)"
        }
    }// End Fucntion
    
    func layoutCheckBox() {
        self.checkBoxView.delegate = self
        self.checkBoxView.boxType = .square
        self.checkBoxView.onCheckColor = .black
        self.checkBoxView.tintColor = UIColor(red: 0/255, green: 138/255, blue: 236/255, alpha: 1)
        self.checkBoxView.onAnimationType = .flat
    }// End Fucntion
    
    func validationStatus() -> (Bool, String) {
        
        var message = ""
        if self.userNameTextField.text!.isEmpty{
            message = "\(RequiredEmailString.localized())"
            return(false, message)
        }else if  !AppConfig.shared.isValidEmail(testStr: self.userNameTextField.text!){
            message = "\(self.userNameTextField.text!) \(InvalidEmail.localized())"
            return(false, message)
        }else{
            self.userCreds.userName = self.userNameTextField.text!
        }
        
        if self.userPasswordTextField.text!.isEmpty{
            message = "\(RequiredPasswordString.localized())"
            return(false, message)
        }else{
            self.userCreds.passCode = self.userPasswordTextField.text!
            return(true, message)
        }
    }// End Fucntion
    
    func checkBiometricsAvailabilityForDevice() {
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthentication,
            error: &error) {
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                switch context.biometryType {
                case .faceID:
                    self.strAlertMessage = setYourFaceToAuthenticate.localized()
                    if (appUsers == nil) || (appUsers.count == 0) {
                        self.BiometricsSymbolsStackView.isHidden = true
                        self.didAutoShowFaceTouchLogin = true
                    }
                    self.biometricButton.isHidden = false // Use FaceID Image
                    self.biometricButton.setImage(#imageLiteral(resourceName: "FaceID"), for: .normal)
                    if !didAutoShowFaceTouchLogin {
                        didAutoShowFaceTouchLogin = true
                        if appDelegate.isUniversalLinkClick == false {
                            self.didVerifyBiometrically(UIButton())
                        }
                    }
                    break
                case .touchID:
                    self.strAlertMessage = setYourFingerToAuthenticate.localized()
                    if (appUsers == nil) || (appUsers.count == 0) {
                        self.BiometricsSymbolsStackView.isHidden = true
                        self.didAutoShowFaceTouchLogin = true
                    }
                    self.biometricButton.isHidden = false // Use TouchID Image
                    self.biometricButton.setImage(#imageLiteral(resourceName: "TouchID"), for: .normal)
                    if !didAutoShowFaceTouchLogin {
                        didAutoShowFaceTouchLogin = true
                        if appDelegate.isUniversalLinkClick == false {
                            self.didVerifyBiometrically(UIButton())
                        }
                    }
                    break
                case .none:
                    print("none")
                    //description = "none"
                    self.BiometricsSymbolsStackView.isHidden = true
                    break
                @unknown default:
                    break
                }
            }else {
                // Device cannot use biometric authentication
                if let err = error {
                    let strMessage = self.errorMessage(errorCode: err._code)
                    self.showAlert(To: "Error".localized(), for: strMessage) {_ in}
                }
            }
        }else{
            self.BiometricsSymbolsStackView.isHidden = true
            if let err = error {
                let strMessage = self.errorMessage(errorCode: err._code)
                self.showAlert(To: "Error".localized(), for: strMessage) {_ in}
            }
        }
    }// End Fucntion
    
//    func notifyUser(_ msg: String, err: String?) {
//        let alert = UIAlertController(title: msg,
//                                      message: err,
//                                      preferredStyle: .alert)
//        let cancelAction = UIAlertAction(title: "OK",
//                                         style: .cancel, handler: nil)
//        alert.addAction(cancelAction)
//        self.present(alert, animated: true,
//                     completion: nil)
//    } // End Fucntion
    
    func errorMessage(errorCode:Int) -> String{
        var strMessage = ""
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            strMessage = authenticationFailed.localized()
            
        case LAError.userCancel.rawValue:
            strMessage = userCancel.localized()
            
        case LAError.userFallback.rawValue:
            strMessage = userFallback.localized()
            
        case LAError.systemCancel.rawValue:
            strMessage = systemCancel.localized()
            
        case LAError.passcodeNotSet.rawValue:
            strMessage = passcodeNotSet.localized()
            
        case LAError.biometryNotAvailable.rawValue:
            strMessage = touchIDNotAvailable.localized()
            
        case LAError.biometryNotEnrolled.rawValue:
            strMessage = touchIDNotEnrolled.localized()
            
        case LAError.biometryLockout.rawValue:
            strMessage = touchIDLockOut.localized()
            
        case LAError.appCancel.rawValue:
            strMessage = appCancel.localized()
            
        case LAError.invalidContext.rawValue:
            strMessage = invalidContext.localized()
            
        default:
            strMessage = "error"
        }
        return strMessage
    } // End Fucntion
    
    //MARK: - Action Methods
    @objc func markCheck(){
        self.checkBoxView.on = !self.checkBoxView.on
        AppConfig.shared.rememberMe = !AppConfig.shared.rememberMe
//        print(AppConfig.shared.rememberMe)
    }
    
    @IBAction func didVerifyBiometrically(_ sender: Any) {
        // Device can use biometric authentication
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthentication,
            error: &error) {
            context.evaluatePolicy(
                .deviceOwnerAuthentication,
                localizedReason: self.strAlertMessage,
                reply: { [weak self] (success, error) -> Void in
                    guard let `self` = self else {return}
                    DispatchQueue.main.async {
                        if success {
                            //Fingerprint recognized
                            if self.appUsers.count == 1 {
                                self.selectUserForLogin(index: 0)
                            }else{
                                if let vc = self.getVC(storyboard: .MAIN, vcIdentifier: String(describing: LoggedInUsersVC.self)) as? LoggedInUsersVC {
                                    vc.modalPresentationStyle = .popover
                                    vc.didDismissCompletion = ({ state in
                                        if (state) {
                                            self.LoadViewWithData()
                                        }
                                    })
                                    self.presentVC(vc)
                                }
                            }
                        }
                    }
                })
        }
    }
    
    @IBAction func logIn(autoLogin: Bool = false){
        let (isValid, message) = self.validationStatus()
        if isValid{
            RequestManager.shared.login(credentials: self.userCreds)  { (status, response)  in
                if status{
                    AppConfig.shared.saveResetUserLogin()
                    if let data = response as? LoginSuccessResponse, status == true && data.result != nil{
                        if let user = data.result, !(user.is_confirmed ?? true), !(user.reset_temp_password ?? true) {
                            let vc = CodeVerificationVC()
                            vc.email = self.userCreds.userName
                            vc.phone = user.phone ?? ""
                            vc.onVerifiedCallBack = {
                                self.logIn()
                            }
                            self.navigateVC(vc)
                        }
                        else {
                            if let token = data.result?.token, let user = data.result {
                                var found = false
                                if self.appUsers != nil{
                                    if self.appUsers.count > 0 {
                                        if (self.appUsers.firstIndex(where: { $0["userName"] as? String == self.userCreds.userName }) != nil){
                                            found = true
                                            // Getting Index where Credentials for logged User is present
                                            if let index = self.appUsers.firstIndex(where: { $0["userName"] as? String == self.userCreds.userName }) {
                                                let passField = self.userPasswordTextField.text!
                                                // Updating the PassCode for logged In User in AppUSers
                                                self.appUsers[index].updateValue((passField), forKey: "passCode")
                                                if self.appUsers[index].keys.contains(where: { $0 == "userId" }) {
                                                    self.appUsers[index].updateValue(user.stringId!, forKey: "userId")
                                                } else {
                                                    self.appUsers[index]["userId"] = user.stringId
                                                }
                                                if self.appUsers[index].keys.contains(where: { $0 == "name" }) {
                                                    self.appUsers[index].updateValue((user.first_name ?? "") + " " + (user.last_name ?? ""), forKey: "name")
                                                } else {
                                                    self.appUsers[index]["name"] = (user.first_name ?? "") + " " + (user.last_name ?? "")
                                                }
                                                if self.appUsers[index].keys.contains(where: { $0 == "image" }) {
                                                    self.appUsers[index].updateValue(user.file?.src ?? "", forKey: "image")
                                                } else {
                                                    self.appUsers[index]["image"] = user.file?.src ?? ""
                                                }
                                                self.defaults.set(self.appUsers, forKey: CREDENTIALS_DICT)
                                                self.defaults.synchronize()
                                            }
                                        }
                                        if !found{
                                            var currentUser = self.userCreds.dictionary
                                            currentUser["userId"] = user.stringId
                                            currentUser["name"] = (user.first_name ?? "") + " " + (user.last_name ?? "")
                                            currentUser["image"] = user.file?.src ?? ""
                                            self.appUsers.append(currentUser)
                                            self.defaults.set(self.appUsers, forKey: CREDENTIALS_DICT)
                                            self.defaults.synchronize()
                                        }
                                    }
                                    else {
                                        var currentUser = self.userCreds.dictionary
                                        if self.appUsers == nil{
                                            self.appUsers = [[String:String]]()
                                        }
                                        currentUser["userId"] = user.stringId
                                        currentUser["name"] = (user.first_name ?? "") + " " + (user.last_name ?? "")
                                        currentUser["image"] = user.file?.src ?? ""
                                        self.appUsers.append(currentUser)
                                        self.defaults.set(self.appUsers, forKey: CREDENTIALS_DICT)
                                        self.defaults.synchronize()
                                        print("No Credentials Yet")
                                    }
                                } else{
                                    
                                    var currentUser = self.userCreds.dictionary
                                    if self.appUsers == nil{
                                        self.appUsers = [[String:String]]()
                                    }
                                    currentUser["userId"] = user.stringId
                                    currentUser["name"] = (user.first_name ?? "") + " " + (user.last_name ?? "")
                                    currentUser["image"] = user.file?.src ?? ""
                                    self.appUsers.append(currentUser)
                                    self.defaults.set(self.appUsers, forKey: CREDENTIALS_DICT)
                                    self.defaults.synchronize()
                                    print("No Credentials Yet")
                                }
                                AppConfig.shared.authToken = "Bearer " + token
                                if let deviceToken = user.device_token {
                                    AppConfig.shared.deviceToken = deviceToken
                                }
                                AppConfig.shared.user = user
                                if let userId = user.id{
                                    AppConfig.shared.userId = String(userId)
                                }
                                if let preName = user.pre_name, preName != ""{
                                    if (user.reset_temp_password ?? false) {
                                        if let vc = self.getVC(storyboard: .MAIN, vcIdentifier: "ResetPaswordScene") as? ResetPasswordVC {
                                            vc.queryToken = token
                                            vc.tempPass = self.userCreds.passCode
                                            UIApplication.shared.windows.first?.rootViewController = vc
                                        }
                                    }
                                    else {
                                        let vc = self.getVC(storyboard: .HOME_TAB, vcIdentifier: "TabsHomeScene") as! TabBarVC
                                        UIApplication.shared.windows.first?.rootViewController = vc
                                    }
                                }
                            }
                        }
                    }else if let data = response as? Login401Response, status == true && data.errors != nil{
                        if let errorText = data.errors?.first, errorText.contains("deactivate") {
                            self.showAlert(To: "Account confirmation".localized(), for: data.errors!.first!.localized(), firstBtnStyle: .default, secondBtnStyle: .default, firstBtnTitle: reactivate.localized(), secondBtnTitle: OKText.localized()) { [weak self] (status) in
                                guard let `self` = self else {return}
                                if status {
                                    self.callApiReactivate()
                                }
                            }
                        }else{
                            self.hideLoader()
                            if (data.errors!.first!.contains("This account is not confirmed.")){
                                let vc = CodeVerificationVC()
                                vc.email = self.userCreds.userName
                                vc.onVerifiedCallBack = {
                                    self.logIn()
                                }
                                self.navigateVC(vc)
                            }
                            else {
                                if let errorText = data.errors?.first {
                                    self.showAlert(To: ValidationAlertTitle.localized(), for: errorText.localized()) {_ in}
                                    if autoLogin {
                                        self.appUsers = [[String:Any]]()
                                        self.defaults.set(self.appUsers, forKey: CREDENTIALS_DICT)
                                        self.defaults.synchronize()
                                        self.loadUsers()
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    self.showAlert(To: ValidationAlertTitle.localized(), for: message) {_ in}
                }
            }
        }else{
            self.showAlert(To: ValidationAlertTitle.localized(), for: message) {_ in}
        }
    }
    
    func callApiReactivate() {
        guard let email = self.userNameTextField.text else {
            return
        }
        RequestManager.shared.reactivateUser(email: email) { (status, response) in
            if status{
                if let data = response as? GeneralSuccessResult {
                    if let message = data.message {
//                    if (message!.contains("If there is an account associated with")) {
//                        message = "If there is an account associated with this".localized() + " \(email) " + "you will receive an email with a confirmation link.".localized()
//                    }
                        self.showAlert(To: "", for: message) {_ in}
                    }
                }
            }else{
                print("Got Response with nil to complete waiting")
            }
        }
    }
    
    func sendReconfirmationLink() {
        guard let email = self.userNameTextField.text else {
            return
        }
        RequestManager.shared.resendConfirmationLink(email: email) { (status, response) in
            if status{
                if let data = response as? GeneralSuccessResult {
                    var message = data.message
                    if (message!.contains("If there is an account associated with")) {
                        message = "If there is an account associated with this".localized() + " \(email) " + "you will receive an email with a confirmation link.".localized()
                    }
                    self.showAlert(To: "Confirmation Link".localized(), for: message!) {_ in}
                }
            }else{
                print("Got Response with nil to complete waiting")
            }
        }
    }
    
    @IBAction func signUp(){
        //TODO:- Implement user SignUp Functionality Here
        self.performSegue(withIdentifier: "RegistrationSceneSegue", sender: self)
    }
    
    @IBAction func resetPassword(){
        print("Reset password is Tapped")
        self.performSegue(withIdentifier: "forgotPasswordSceneSegue", sender: self)
    }
    
    @IBAction func didTapChangeLanguage(_ sender: UIButton) {
        let lang = AppConfig.shared.currentLanguage()
        print("currentLanguage -> \(lang)")
        switch lang {
        case .DANISH:
            AppConfig.shared.setLanguage(lang: .ENGLISH)
        default:
            AppConfig.shared.setLanguage(lang: .DANISH)
        }
        AppConfig.shared.changeRootVCToLoginVC()
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func selectUserForLogin(index:Int) {
        let user = self.appUsers[index]
        let email = user["userName"]
        self.userNameTextField.text = email as? String
        let password = user["passCode"]
        self.userPasswordTextField.text = password as? String
        self.logIn(autoLogin: true)
    }
}

//MARK: - UITextFieldDelegate
extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userNameTextField{
            let _ = self.userPasswordTextField.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
            self.logIn()
        }
        return true
    }
}

extension LoginVC: BEMCheckBoxDelegate {
    func didTap(_ checkBox: BEMCheckBox) {
        AppConfig.shared.rememberMe = !AppConfig.shared.rememberMe
        print("LoginVC->didTap->rememberMe", AppConfig.shared.rememberMe)
    }
}

extension LoginVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        forgotPasswordButton.setTitleColor(theme.settings.titleColor, for: .normal)
        userNameTextField.textColor = theme.settings.titleColor
        userPasswordTextField.textColor = theme.settings.titleColor
        btnSignup.setTitleColor(theme.settings.titleColor, for: .normal)
        btnSignin.setTitleColor(theme.settings.titleColor, for: .normal)
        btnSignin.backgroundColor = theme.settings.highlightedBgColor
        lblVersion.textColor = theme.settings.subTitleColor
        checkMarkLabel.textColor = theme.settings.titleColor
        changeLanguageButton.setTitleColor(theme.settings.titleColor, for: .normal)
    }
}
