//
//  SignUpVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 06/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField
import FlagPhoneNumber

class SignUpVC: BaseVC {
    //MARK: - Outlets
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var genderButtons: [UIButton]!
    
    //MARK:- Computed Textfield Outlets
    @IBOutlet weak var firstNameTextField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: firstNameTextField)
        }
    }
    @IBOutlet weak var lastNameTextField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: lastNameTextField)
        }
    }
    @IBOutlet weak var dobTextField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: dobTextField)
        }
    }
    @IBOutlet weak var contactTextField: FPNTextField!
    @IBOutlet weak var emailTextField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: emailTextField)
        }
    }
    @IBOutlet weak var passwordTextField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: passwordTextField)
        }
    }
    @IBOutlet weak var confirmPasswordTextField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: confirmPasswordTextField)
        }
    }
    
    //MARK: Properties
    
    var gender = "M"
    var userData = RegisterUserModel()
   
    var datePicker : UIDatePicker!
    var phoneNumber: String = ""
    var isValidPhoneNumber: Bool = false
    
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layOutGenderButtons()
        self.layoutSubViewTextFieldIcons()
        self.scrollView.shouldIgnoreScrollingAdjustment = true
        self.setupPhoneNumber()
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: SignUpVC.setTheme)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //MARK: - layOutMethods
    func layoutSubViewTextFieldIcons() {
        AppConfig.shared.setTextfieldIcon(for: self.dobTextField, addIcon: UIImage(named: "calender")!)
        AppConfig.shared.setTextfieldIcon(for: self.emailTextField, addIcon: UIImage(named: "email")!)
//        AppConfig.shared.setTextfieldIcon(for: self.contactTextField, addIcon: UIImage(named: "phone")!)
        AppConfig.shared.setTextfieldIcon(for: self.lastNameTextField, addIcon: UIImage(named: "user")!)
        AppConfig.shared.setTextfieldIcon(for: self.firstNameTextField, addIcon: UIImage(named: "user")!)
        AppConfig.shared.setTextfieldIcon(for: self.passwordTextField, addIcon: UIImage(named: "secureEntry")!)
        AppConfig.shared.setTextfieldIcon(for: self.confirmPasswordTextField, addIcon: UIImage(named: "secureEntry")!)
    }
    
    private func setupPhoneNumber() {
//        contactTextField.setFlag(key: .US)
        contactTextField.setCountries(including: [.DK])
        contactTextField.delegate = self
        contactTextField.attributedPlaceholder = NSAttributedString(string: contactTextField.placeholder ?? "2015550123", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        contactTextField.phoneCodeRightPadding = 8.0
    }
    
    func pickUpDate(_ textField : UITextField){
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
        }
        self.datePicker.calendar = Calendar(identifier: .gregorian)
        self.datePicker.locale = Locale(identifier: "en_US")
        self.datePicker!.datePickerMode = .date
        self.datePicker!.maximumDate = Date()
        if let txt = textField.text, txt != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            self.datePicker.setDate(dateFormatter.date(from: txt)!, animated: true)
        }
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .white
//            UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button to the ToolBar
        let cancelButton = UIBarButtonItem(title: cancelText.localized, style: .plain, target: self, action: #selector(self.cancelClick))
        cancelButton.setTitleTextAttributes([
            NSAttributedString.Key.font :UIFont.systemFont(ofSize: 19),
            NSAttributedString.Key.foregroundColor : AppColors.destructive,
            ], for: .normal)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: SelectText.localized(), style: .plain, target: self, action: #selector(self.doneClick))
        doneButton.setTitleTextAttributes([
            NSAttributedString.Key.font :UIFont.systemFont(ofSize: 20),
            NSAttributedString.Key.foregroundColor : AppColors.choice,
            ], for: .normal)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        textField.inputAccessoryView = toolBar
        textField.inputView = self.datePicker

    }
    
    @objc func showDatePicker(_ sender: UITapGestureRecognizer) {
        let _ = dobTextField.becomeFirstResponder()
        pickUpDate(dobTextField)
    }
    
    @objc func doneClick(sender:UIButton) {
        dobTextField.text = AppConfig.shared.formatPickerDate(date: self.datePicker.date)
        let _ = dobTextField.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        let _ = dobTextField.resignFirstResponder()
    }
    
    func layOutGenderButtons()  {
        self.maleButton.roundCorners(corners: [.topLeft, .bottomLeft], radius: 6.0)
        self.maleButton.backgroundColor = AppColors.setActive
        
        self.femaleButton.roundCorners(corners: [.topRight, .bottomRight], radius: 6.0)
        self.femaleButton.backgroundColor = AppColors.setInActive
        self.setGenderSelection(maleButton)
    }
    
    func setGenderSelection(_ sender:UIButton) {
        for button in genderButtons{
            if button == sender{
                guard let title = button.currentTitle else { return }
                button.backgroundColor = AppColors.setActive
                button.alpha = 1
                button.setImage(UIImage(named: "\(title)Active"), for: .normal)
            }else{
                guard let title = button.currentTitle else { return }
                button.backgroundColor = AppColors.setInActive
                button.alpha = 0.5
                button.setImage(UIImage(named: title), for: .normal)
            }
        }
    }
    
    func validationStatus() -> (Bool, String) {
        
        var status = true
        var message = ""
        var count = 0
        
        if self.firstNameTextField.text!.isEmpty{
            message += FirstNameText.localized() + ", "
            count = count + 1
            status = false
        }else{
            self.userData.first_name = self.firstNameTextField.text!
        }
        
        if self.lastNameTextField.text!.isEmpty{
            message += LastNameText.localized() + ", "
            count = count + 1
            status = false
        }else{
            self.userData.last_name = self.lastNameTextField.text!
        }
        
//        if self.dobTextField.text!.isEmpty{
//            message += DateOfBirthText.localized() + ", "
//            count = count + 1
//            status = false
//        }else{
//            self.userData.dob = self.dobTextField.text!.dbFormat
//        }
        
        if self.gender == ""{
            message += GenderText.localized() + ", "
            count = count + 1
            status = false
        }else{
            self.userData.gender = self.gender
        }
        
        if self.contactTextField.text!.isEmpty{
            message += ContactNumberText.localized() + ", "
            count = count + 1
            status = false
        }else if !self.isValidPhoneNumber {
            return (false, "Please enter a valid contact number.")
        }else {
            self.userData.phone = self.phoneNumber
        }
        
        if self.emailTextField.text!.isEmpty{
            message += "Email, "
            count = count + 1
            status = false
        }else{
            self.userData.email = self.emailTextField.text!
        }
        
        if let email = self.emailTextField.text, !AppConfig.shared.isValidEmail(testStr: email){
            message += "\(email) \(InvalidEmail.localized()), "
            count += 1
            status = false
        }
        
        if self.passwordTextField.text!.isEmpty{
            message += PasswordText.localized() + ", "
            count = count + 1
            status = false
        }else {
            self.userData.password = self.passwordTextField.text!
        }
        
        if self.confirmPasswordTextField.text!.isEmpty{
            message += ConfirmPasswordText.localized() + ", "
            count = count + 1
            status = false
        }else {
            self.userData.password_confirmation = self.confirmPasswordTextField.text!
        }
        
        if let password = self.passwordTextField.text, let confirmPassword = self.confirmPasswordTextField.text{
            if password != confirmPassword{
                message += PasswordMissmatch.localized() + ", "
                count += 1
                status = false
            }
        }
        
        if self.passwordTextField.text!.count  < 6{
            message += "\n\(PaswordLength.localized())"
            status = false
        }
        if self.confirmPasswordTextField.text!.count < 6 {
            message += "\n\(ConfirmPasswordLength.localized())"
            status = false
        }
        
        if count == 1{
            message = String(message.dropLast(2))
            message += IsRequiredString.localized()
            count = 0
        }else if count > 1{
            message = String(message.dropLast(2))
            message += AreRequiredString.localized()
            count   = 0
        }
        
        return (status, message)
    }
    
    //MARK: - Action Methods
    @IBAction func selectorGender(_ sender:UIButton){
        guard let title = sender.currentTitle else {
            return
        }
        self.gender = title == MaleGender.localized() ? "M" : "F"
        print(self.gender)
        self.setGenderSelection(sender)
    }
    
    @IBAction func registerButton(){
        let (isValid, message) = self.validationStatus()
        if isValid{
            RequestManager.shared.register(data: self.userData) { (status, response) in
                if status{
                    if let _ = response as? GeneralSuccessResult{
                        let vc = CodeVerificationVC()
                        vc.email = self.userData.email!
                        vc.phone = self.userData.phone!
                        vc.onVerifiedCallBack = { [weak self] in
                            guard let `self` = self else {return}
                            self.navigationController?.popViewController(animated: true)
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                }
            }
        }else{
            self.showAlert(To: ValidationAlertTitle.localized(), for: message) {_ in}
        }
    }
}

//MARK: - TextField Delegation Extension

extension SignUpVC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dobTextField{
            pickUpDate(dobTextField)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameTextField{
            let _ = lastNameTextField.becomeFirstResponder()
        }else if textField == lastNameTextField{
            let _ = contactTextField.becomeFirstResponder()
        }
//        else if textField == dobTextField {
//            let _ = contactTextField.becomeFirstResponder()
//        }
        else if textField == contactTextField{
            let _ = emailTextField.becomeFirstResponder()
        }else if textField == emailTextField{
            let _ = passwordTextField.becomeFirstResponder()
        }else if textField == passwordTextField{
            let _ = confirmPasswordTextField.becomeFirstResponder()
        }else {
            self.view.endEditing(true)
        }
        return true
    }
}

extension SignUpVC: FPNTextFieldDelegate {
    
    /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
//      let navigationViewController = UINavigationController(rootViewController: listController)
//      listController.title = "Countries"
//      self.present(navigationViewController, animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
    }
    
    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        self.isValidPhoneNumber = isValid
//        self.phoneNumber = textField.getFormattedPhoneNumber(format: .E164) ?? ""
        self.phoneNumber = textField.getRawPhoneNumber() ?? ""
    }
}

extension SignUpVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        firstNameTextField.textColor = theme.settings.titleColor
        lastNameTextField.textColor = theme.settings.titleColor
        dobTextField.textColor = theme.settings.titleColor
        contactTextField.textColor = theme.settings.titleColor
        emailTextField.textColor = theme.settings.titleColor
        passwordTextField.textColor = theme.settings.titleColor
        confirmPasswordTextField.textColor = theme.settings.titleColor
        btnSignup.setTitleColor(theme.settings.titleColor, for: .normal)
        btnSignup.backgroundColor = theme.settings.highlightedBgColor
        lblDescription.textColor = theme.settings.subTitleColor
    }
}
