//
//  SplashVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 13/11/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class SplashVC: BaseVC {
    
    @IBOutlet weak var ivLogo: UIImageView! {
        didSet{
//            let image = UIImage(named: "logoHome")?.withRenderingMode(.alwaysTemplate)
//            ivLogo.image = image
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkForForceUpdate()
        
//        self.setupTheme()
    }
    
//    private func setupTheme() {
//        Themer.shared.register(
//            target: self,
//            action: SplashVC.setTheme)
//    }
    
    func checkForForceUpdate() {
        RequestManager.shared.forceUpdate { (status, response) in
            if status{
                if let data = response as? ForceUpdateModelResult, status == true && data.result != nil{
                    if let data = data.result {
                        let iosVersion = data.ios_version
                        let appVersion = AppConfig.shared.appVersion() //Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String

                        if let appVersion = appVersion, iosVersion! > appVersion {
                            if (data.force ?? false) {
                                self.showAlert(To: AppUpdate.localized(), for: AppUpdateMessage.localized()) { [weak self] _ in
                                    guard let `self` = self else {return}
                                    self.openWebLink()
                                }
                            }else {
                                self.showAlert(To: AppUpdate.localized(), for: AppUpdateMessage.localized()) { [weak self] _ in
                                    guard let `self` = self else {return}
                                    self.openWebLink()
                                    self.proceedWithApp()
                                }
                            }
                        }else{
                            self.proceedWithApp()
                        }
                    }else{
                        print("Error")
                        self.proceedWithApp()
                    }
                }else {
                    print("Error")
                    self.proceedWithApp()
                }
            }else{
                print("Error")
                self.proceedWithApp()
            }
        }
    }
    
    func proceedWithApp() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.isPushNotificationClicked {return}
        if appDelegate.isUniversalLinkClick {return}
        if let token = AppConfig.shared.authToken, token != ""{
            let vc = self.getVC(storyboard: .HOME_TAB, vcIdentifier: "TabsHomeScene") as! TabBarVC
            appDelegate.window?.rootViewController = vc
            appDelegate.window?.makeKeyAndVisible()
        }
        else{
            let vc = self.getVC(storyboard: .MAIN, vcIdentifier: "MainNavView")
            appDelegate.window?.rootViewController = vc
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    func openWebLink() {
        if let url = URL(string: "https://apps.apple.com/us/app/onlinedoc/id1524247859"),
           UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (opened) in
                if(opened){
                    print("Website opened!!!")
                }
            }
        } else {
            print("Can't Open URL on Simulator")
        }
    }
}
