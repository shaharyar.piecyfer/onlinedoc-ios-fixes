//
//  UserTypeVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 25/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class UserTypeVC: BaseVC {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var optionsTableView: UITableView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var btnUpdate: UIButton!
    
    var userType = ""
    var data = [Profession(imageName: "doctor", title: "Doctor".localized()),
                Profession(imageName: "healthStaff", title: "Health Care Personal".localized()),
                Profession(imageName: "citizen", title: "Standard User".localized())
    ]
    var underlyingViewController: UIViewController? // Reference to the View Controller on which this View Controller is presented
    var onCompletion: ((Bool, String) -> ())?
   
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.navigationBar.tintColor = .white
        lblHeader.textColor = .white
        optionsTableView.register(UINib(nibName: "UserTypeViewCell", bundle: Bundle.main), forCellReuseIdentifier: "UserTypeCell")
        optionsTableView.rowHeight = UITableView.automaticDimension
        optionsTableView.estimatedRowHeight = 100
        optionsTableView.alwaysBounceVertical = false
        
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: UserTypeVC.setTheme)
    }
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layOutSceneBorders()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if (self.userType == "Doctor".localized()) {
            let indexpath = IndexPath(row: 0, section: 0)
            optionsTableView.selectRow(at: indexpath, animated: true, scrollPosition:.top )
        }
        else if (self.userType == "Health Care Personal".localized()) {
            let indexpath = IndexPath(row: 1, section: 0)
            optionsTableView.selectRow(at: indexpath, animated: true, scrollPosition:.middle )
        }
        else if (self.userType == "Standard User".localized()) {
            let indexpath = IndexPath(row: 2, section: 0)
            optionsTableView.selectRow(at: indexpath, animated: true, scrollPosition:.bottom )
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func layOutSceneBorders(){
        self.headerView.roundCorners(corners: [.topLeft, .topRight], radius: 6.0)
        self.footerView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 6.0)
        self.view.layer.cornerRadius = 6.0
        self.view.clipsToBounds = true
        self.view.layer.borderColor = AppColors.rowActive.cgColor
        self.headerView.layer.borderColor = AppColors.setActive.cgColor
    }
    
    @IBAction func UserTypeTapped(_ sender: Any) {
        RequestManager.shared.userType(preName: self.userType) { (status, response) in
            if status{
                if let data = response as? LoginSuccessResponse, status == true && data.result != nil
                {
                    if let user = data.result {
                        AppConfig.shared.user = user
                    }
                    if let _ = self.underlyingViewController {
                        if (self.underlyingViewController?.restorationIdentifier == "ProfileVC") {
                            self.onCompletion?(true, self.userType.localized())
                            self.dismissVC()
                        }
                    }
                    else {
                        let storyboard: UIStoryboard = UIStoryboard(name: "HomeTab", bundle: nil)
                        let tabController = storyboard.instantiateViewController(withIdentifier: "TabsHomeScene" ) as! UITabBarController
                        UIApplication.shared.keyWindow?.rootViewController = tabController
                    }
                }
            }
        }
    }
}

extension UserTypeVC: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTypeCell") as? UserTypeViewCell
            else {
            return self.emptyTblCell()
        }
        cell.professionImageView.image = UIImage(named: data[indexPath.row].imageName)?.withRenderingMode(.alwaysTemplate)
        cell.prefessionTitleLabel.text = data[indexPath.row].title
//        let view = UIView()
//        view.backgroundColor = AppColors.choice
//        cell.selectedBackgroundView = view
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 1:
            self.userType = UserType.HealthStaff.rawValue
            break
        case 2:
                self.userType = UserType.StandardUser.rawValue
            break
        default:
            self.userType = UserType.Doctor.rawValue
        }
        print(self.userType)
    }
    
}

struct Profession {
    var imageName: String
    var title: String
}

extension UserTypeVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        headerView.backgroundColor = theme.settings.highlightedBgColor
        lblHeader.textColor = theme.settings.titleColor
        footerView.backgroundColor = theme.settings.highlightedBgColor
        btnUpdate.setTitleColor(theme.settings.titleColor, for: .normal)
        btnUpdate.backgroundColor = theme.settings.highlightedBgColor
    }
}
