//
//  BaseVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 25/06/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MobileCoreServices
import WeScan
import VisionKit
import OpalImagePicker
//import PDFKit
import Photos
//import PhotosUI

var baseVC: BaseVC?
class BaseVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIGestureRecognizerDelegate, UIDocumentPickerDelegate, VNDocumentCameraViewControllerDelegate, OpalImagePickerControllerDelegate, UIDocumentBrowserViewControllerDelegate {
    
    typealias boolCallBack = (_ status:Bool)-> Void
    typealias boolWithMsgCallBack = (_ status:Bool, _ msg:String?)-> Void
    typealias dataCallBack = (_ data:Any)-> Void
    
    var refreshControl: UIRefreshControl = UIRefreshControl()
    let imagePicker = UIImagePickerController()
    let docBrowser = UIDocumentBrowserViewController(forOpeningFilesWithContentTypes: [String("public.data")])
    var localTimeZoneIdentifier: String {
        return TimeZone.current.identifier
    }
    
    var isNeedToDark = false
    func setupBettryIconBlackOrWhite(isBlack:Bool = true){
        self.isNeedToDark = isBlack
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func enableSwipeToDismissVC() {
        if(self.navigationController != nil){
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        }
    }
    
    func disableSwipeToDismissVC() {
        self.delay(1.0) { [weak self] in
            guard let `self` = self else {return}
            if(self.navigationController != nil){
                self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = AppConfig.shared.isDarkMode ? .dark : .light
        }
        if AppConfig.shared.isDarkMode {
            return .lightContent
        }else{
            return .darkContent
        }
    }
    
    func showLoader() {
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        }
    }
    
    func hideLoader(){
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *){
            overrideUserInterfaceStyle = AppConfig.shared.isDarkMode ? .dark : .light
        }
        navigationController?.navigationBar.barStyle = AppConfig.shared.isDarkMode ? .black : .default
        if let nv = self.navigationController {
            nv.interactivePopGestureRecognizer?.delegate = self
            Themer.shared.register(
                target: nv,
                action: UINavigationController.applyTheme)
        }
        Themer.shared.register(
            target: self,
            action: BaseVC.applyTheme)

        let labels = AppConfig.shared.getLabelsInView(view: self.view)
        for item in labels {
            Themer.shared.register(
                target: item,
                action: UILabel.applyTheme)
        }
        
        Themer.shared.register(
            target: self,
            action: BaseVC.setTheme)
        
        self.listenNetworkConnectivity()
    }
    
    private func listenNetworkConnectivity() {
        NotificationCenter.default.addObserver(forName: .nertworkFlagsChanged, object: nil, queue: .main) { [weak self] _ in
            guard let `self` = self else { return }
            switch Network.reachability.status {
            case .unreachable:
//                view.backgroundColor = .red
                self.showAlert(To: NoInternetHeader.localized(), for: NoInternetMessage.localized()) {_ in}
            case .wwan: break
            case .wifi: break
            }
            print("Reachability Summary")
            print("Status:", Network.reachability.status)
            print("HostName:", Network.reachability.hostname ?? "nil")
            print("Reachable:", Network.reachability.isReachable)
            print("Wifi:", Network.reachability.isReachableViaWiFi)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNeedsStatusBarAppearanceUpdate()
        if(self.navigationController != nil){
            baseVC = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickCross(sender: UIButton){
        dismissVC()
    }
    
    func dismissVC(){
        if let nev = self.navigationController{
            nev.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func popTo(from fromVC: UIViewController, to toVC: AnyClass){
        guard fromVC.navigationController != nil else {return}
        for controller in fromVC.navigationController!.viewControllers as Array {
            if controller.isKind(of: toVC) {
                fromVC.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    //MARK: Delay
    func delay(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    //MARK: ATTRIBUTED TEXT
    func setAttributedTextForLabel(mainString: String , attributedStringsArray: [String] , lbl : UILabel , color : UIColor, attFont:UIFont) {
        let attributedString1 = NSMutableAttributedString(string: mainString)
        for objStr in attributedStringsArray {
            let range1 = (mainString as NSString).range(of: objStr)
            let attribute_font = [NSAttributedString.Key.font: attFont]
            attributedString1.addAttributes(attribute_font, range:  range1)
            
            attributedString1.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range1)
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range1)
            attributedString1.addAttribute(NSAttributedString.Key.strokeColor, value: UIColor.white, range: range1)
            attributedString1.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range1)
        }
        lbl.attributedText = attributedString1
    }
    
    func showAlert(To title:String, for msg:String, completion: @escaping boolCallBack){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.view.tintColor = AppColors.choice
        let btn = UIAlertAction(title: OKText.localized(), style: .default, handler: { (action: UIAlertAction!) in
            completion(true)
            alert.dismiss(animated: true)
        })
        btn.setValue(UIColor.whiteOrBlack, forKey: "titleTextColor")
        alert.addAction(btn)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(To title:String, for msg:String, firstBtnStyle: UIAlertAction.Style = .default,
                   secondBtnStyle: UIAlertAction.Style = .destructive, firstBtnTitle: String = yesText, secondBtnTitle: String = noText, completion: @escaping boolCallBack){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.view.tintColor = AppColors.choice
        let firstBtn = UIAlertAction(title: firstBtnTitle.localized(), style: firstBtnStyle, handler: { (action: UIAlertAction!) in
            completion(true)
            alert.dismiss(animated: true)
        })
        let secondBtn = UIAlertAction(title: secondBtnTitle.localized(), style: secondBtnStyle, handler: { (action: UIAlertAction!) in
            completion(false)
            alert.dismiss(animated: true)
        })
        if firstBtnStyle == .default {
            firstBtn.setValue(UIColor.whiteOrBlack, forKey: "titleTextColor")
        }
        secondBtn.setValue(UIColor.whiteOrBlack, forKey: "titleTextColor")
        
        alert.addAction(firstBtn)
        alert.addAction(secondBtn)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showMultiBtnAlert(model: AlertModel, completion: @escaping dataCallBack){
        let alert = UIAlertController(title: model.title, message: model.description, preferredStyle: model.style)
        if let firstSubview = alert.view.subviews.first, let alertContentView = firstSubview.subviews.first {
            for view in alertContentView.subviews {
                view.backgroundColor = model.backgroundColor
            }
        }
        alert.modalPresentationStyle = model.presentationStyle
        alert.view.tintColor = model.tintColor
        
        for btn in model.btns {
            let alertBtn = UIAlertAction(title: btn.title?.localized(), style: btn.style, handler: { (action: UIAlertAction!) in
                completion(btn.title!)
                alert.dismiss(animated: true)
            })
            alertBtn.setValue(btn.color, forKey: "titleTextColor")
            alert.addAction(alertBtn)
        }

        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(To title:String, for msg:String, tfPlaceholder: String = "", isSecure: Bool = false, buttons: Int = 1,
                   firstBtnStyle: UIAlertAction.Style = .default, secondBtnStyle: UIAlertAction.Style = .destructive,
                   firstBtnTitle: String = yesText, secondBtnTitle: String = noText,
                   completion: @escaping boolWithMsgCallBack){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.view.tintColor = AppColors.choice
        alert.addTextField { (textField) in
            textField.placeholder = tfPlaceholder
            textField.autocapitalizationType = .sentences
            textField.tintColor = UIColor.whiteOrBlack
            textField.isSecureTextEntry = isSecure
        }
        
        if buttons == 1 {
            let btn = UIAlertAction(title: OKText.localized(), style: .default, handler: { (action: UIAlertAction!) in
                completion(true, alert.textFields?[0].text)
                alert.dismiss(animated: true)
            })
            btn.setValue(UIColor.whiteOrBlack, forKey: "titleTextColor")
            alert.addAction(btn)
        } else {
            let firstBtn = UIAlertAction(title: firstBtnTitle.localized(), style: firstBtnStyle, handler: { (action: UIAlertAction!) in
                completion(true, alert.textFields?[0].text)
                alert.dismiss(animated: true)
            })
            firstBtn.setValue(UIColor.whiteOrBlack, forKey: "titleTextColor")
            let secondBtn = UIAlertAction(title: secondBtnTitle.localized(), style: secondBtnStyle, handler: { (action: UIAlertAction!) in
                completion(false, nil)
                alert.dismiss(animated: true)
            })
            secondBtn.setValue(UIColor.whiteOrBlack, forKey: "titleTextColor")
            
            alert.addAction(firstBtn)
            alert.addAction(secondBtn)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDialogAlert(options: AttachmentOption..., singleSelection: Bool = false) {
        UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).effect = UIBlurEffect(style: .dark)
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if let firstSubview = alert.view.subviews.first, let alertContentView = firstSubview.subviews.first {
            for view in alertContentView.subviews {
                view.backgroundColor = .white
            }
        }
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        }
        UINavigationBar.appearance().tintColor = UIColor.whiteOrBlack
        alert.modalPresentationStyle = .popover
        alert.view.tintColor = AppColors.blackColor
        imagePicker.navigationController?.navigationBar.tintColor = AppColors.destructive
        
        for option in options {
            switch option {
            case .camera:
                let image = UIImage(named: "homeCam")
                let imageView = UIImageView()
                imageView.image = image
                imageView.frame =  CGRect(x: 20, y: 18, width: 30, height: 24)
                alert.view.addSubview(imageView)
                
                let camera = UIAlertAction(title: NSLocalizedString(CameraText.localized(), comment: ""), style: .default) { [weak self] action in
                    guard let `self` = self else {return}
                    alert.dismiss(animated: true, completion: nil)
                    self.didTapSelection(option: .camera)
                }
                alert.addAction(camera)
                
            case .library:
                let image1 = UIImage(named: "homePhotos")
                let imageView1 = UIImageView()
                imageView1.image = image1
                imageView1.frame = CGRect(x: 20, y: 75, width: 30, height: 24)
                alert.view.addSubview(imageView1)
                
                let photos = UIAlertAction(title: NSLocalizedString(PhotosText.localized(), comment: ""), style: .default) { [weak self] action in
                    guard let `self` = self else {return}
                    alert.dismiss(animated: true, completion: nil)
                    self.didTapSelection(option: .library, singleSelection)
                }
                alert.addAction(photos)
                
            case .video:
                let movieIcon = UIImage(named: "homeVideo")
                let movieImageView = UIImageView()
                movieImageView.image = movieIcon
                movieImageView.frame =  CGRect(x: 20, y: 128, width: 30, height: 24)
                alert.view.addSubview(movieImageView)

                let movie = UIAlertAction(title: NSLocalizedString(videoText.localized(), comment: ""), style: .default) { [weak self] action in
                    guard let `self` = self else {return}
                    alert.dismiss(animated: true, completion: nil)
                    self.didTapSelection(option: .video)
                }
                alert.addAction(movie)

            case .docs:
                let docIcon = UIImage(named: "homeDoc")
                let docIconView = UIImageView()
                docIconView.image = docIcon
                docIconView.frame = CGRect(x: 20, y: 180, width: 24, height: 30)
                alert.view.addSubview(docIconView)

                let documents = UIAlertAction(title: NSLocalizedString("Documents", comment: ""), style: .default) { [weak self] action in
                    guard let `self` = self else {return}
                    alert.dismiss(animated: true, completion: nil)
                    self.didTapSelection(option: .docs)
                }
                alert.addAction(documents)

            case .scan:
                let scanIcon = UIImage(named: "scanner-24")
                let scanIconView = UIImageView()
                scanIconView.image = scanIcon
                scanIconView.frame = CGRect(x: 20, y: 240, width: 30, height: 30)
                alert.view.addSubview(scanIconView)

                let scanAction = UIAlertAction(title: "Scan", style: .default) { [weak self] _ in
                    guard let `self` = self else {return}
                    alert.dismiss(animated: true, completion: nil)
                    self.didTapSelection(option: .scan)
                }
                alert.addAction(scanAction)
            case .none:
                return
            }
        }
        
        alert.addAction(UIAlertAction(title: "", style: .default, handler: nil))
        
        let cancelButtonVC = getVC(storyboard: .MAIN, vcIdentifier: "CancelButtonViewController")
        let cancel = UIAlertAction(title: "", style: .cancel, handler: nil)
        cancel.setValue(cancelButtonVC, forKey: "contentViewController")
        
        alert.addAction(cancel)
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        present(alert, animated: true, completion: nil)
    }
    
    func didTapSelection(option: AttachmentOption, _ singleSelection: Bool = false) {
        switch option {
        case .camera:
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                self.showAlert(To: "", for: "Device has no camera.".localized()) {_ in}
                return
            }
            self.imagePicker.sourceType = .camera
            self.imagePicker.mediaTypes = [String(kUTTypeImage)]
            self.imagePicker.delegate = self
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
                self.presentVC(self.imagePicker)
            })
        case .library:
            if singleSelection {
                self.imagePicker.sourceType = .photoLibrary
//                self.imagePicker.mediaTypes = [String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo)]
                self.imagePicker.mediaTypes = [String(kUTTypeImage)]
                self.imagePicker.delegate = self
                self.imagePicker.navigationBar.tintColor = UIColor.whiteOrBlack
                UINavigationBar.appearance().tintColor = UIColor.whiteOrBlack
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
                    self.presentVC(self.imagePicker)
                })
            }
            else {
                let imagePicker = OpalImagePickerController()
                imagePicker.imagePickerDelegate = self
                imagePicker.maximumSelectionsAllowed = 5
                imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image, PHAssetMediaType.video])
                imagePicker.navigationBar.tintColor = UIColor.whiteOrBlack
                UINavigationBar.appearance().tintColor = UIColor.whiteOrBlack
                
                let configuration = OpalImagePickerConfiguration()
                configuration.doneButtonTitle = SelectText.localized()
                imagePicker.configuration = configuration
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
                    self.presentVC(imagePicker)
                })
            }
        case .video:
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                self.showAlert(To: "", for: "Device has no camera.".localized()) {_ in}
                return
            }
            self.imagePicker.sourceType = .camera
            self.imagePicker.delegate = self
            self.imagePicker.mediaTypes = [String(kUTTypeMovie), String(kUTTypeVideo)]
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
                self.presentVC(self.imagePicker)
            })
        case .docs:
            self.docBrowser.delegate = self
            self.docBrowser.modalPresentationStyle = .fullScreen
            self.docBrowser.allowsDocumentCreation = false
            let cancelbutton = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(onClickDocPickerCancel))
            self.docBrowser.additionalTrailingNavigationBarButtonItems = [cancelbutton]
            UINavigationBar.appearance().tintColor = UIColor.whiteOrBlack
            if #available(iOS 11.0, *) {
                self.docBrowser.allowsPickingMultipleItems = true
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: { [weak self] in
                guard let `self` = self else {return}
                self.presentVC(self.docBrowser)
            })
//            let importMenu = UIDocumentPickerViewController(documentTypes: [String("public.data")], in: .import)
//
//            importMenu.delegate = self
//            importMenu.modalPresentationStyle = .fullScreen
//            UINavigationBar.appearance().tintColor = UIColor.whiteOrBlack
//            if #available(iOS 11.0, *) {
//                importMenu.allowsMultipleSelection = true
//            }
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
//                self.presentVC(importMenu)
//            })
        case .scan:
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                self.showAlert(To: "", for: "Device has no camera.".localized()) {_ in}
                return
            }
            let vc = VNDocumentCameraViewController()
            vc.overrideUserInterfaceStyle = .light
            vc.delegate = self
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
                self.presentVC(vc)
            })
        case .none:
            return
        }
    }
    
    @objc private func onClickDocPickerCancel() {
        docBrowser.dismiss(animated: true, completion: nil)
    }
    
    func imageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImage.Orientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2))
            break
        case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi / 2))
            break
        case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
            break
        @unknown default:
            break
        }
        
        switch src.imageOrientation {
        case UIImage.Orientation.upMirrored, UIImage.Orientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImage.Orientation.leftMirrored, UIImage.Orientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImage.Orientation.up, UIImage.Orientation.down, UIImage.Orientation.left, UIImage.Orientation.right:
            break
        @unknown default:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored, UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    //MARK: Get VC From Storyborad
    func getVC(storyboard: Storyboard, vcIdentifier : String) -> UIViewController {
        if #available(iOS 13.0, *) {
            return UIStoryboard(name: storyboard.board(), bundle: nil).instantiateViewController(identifier: vcIdentifier)
        } else {
            return UIStoryboard(name: storyboard.board(), bundle: nil).instantiateViewController(withIdentifier: vcIdentifier)
        }
    }
    
    func previewUser(id: Int, fromChat: Bool = false) {
        if let vc = getVC(storyboard: .PROFILE, vcIdentifier: "MemberProfileScene") as? MemberProfileVC {
            vc.memberId = id
            vc.fromChat = fromChat
            self.navigateVC(vc)
        }
    }
    
    func loadUserGroup(completion: @escaping boolCallBack){
        RequestManager.shared.userGroupsWithCategpries{ (status, response) in
            if status{
                if let data = response as? GroupsWithCategoriesResult {
                    if let groupsList = data.groups {
                        AppConfig.shared.myGroupsWithCategories = groupsList
                    }
                }
            }
            completion(true)
        }
    }
    
    func systemMembers(page: Int, searchText: String = "", completion: @escaping (_ status:Bool, _ users:[User]?)-> Void){
        RequestManager.shared.getSystemMembers(page: page, searchText: searchText) { (status, response) in
            if status {
                if let data = response as? UserListModel{
                    if let members = data.users
                    {
                        completion(true, members)
                    }
                }
            } else {
                completion(false, nil)
            }
        }
    }
    
    func friendRequest(params: FriendRequestModel, showAlert: Bool = true, completion: @escaping boolWithMsgCallBack) {
        RequestManager.shared.friendRequest(params: params) { (status, response) in
            if status {
                if let res = response as? GeneralSuccessResult, res.message != nil {
                    self.showAlert(To: "", for: res.message!.localized()) {_ in}
                    completion(true, res.message)
                }
                else {
                    completion(true, nil)
                }
            } else {
                completion(false, nil)
            }
        }
    }
    
    func emptyTblCell() -> UITableViewCell {
        let emptyCell = UITableViewCell()
        emptyCell.backgroundColor = .clear
        emptyCell.contentView.backgroundColor = .clear
        return emptyCell
    }
    
    func emptyCVCell() -> UICollectionViewCell {
        let emptyCell = UICollectionViewCell()
        emptyCell.backgroundColor = .clear
        emptyCell.contentView.backgroundColor = .clear
        return emptyCell
    }
    
    func postNotifyToGroupUnread(groupId: Int, notifications: Int) {
        //to remove red dot at home groups shortcuts, when visiting thread
        let group = GroupModel()
        group.id = groupId
        group.group_notification_count = notifications
        NotificationCenter.default.post(name: .notifyGroup, object: nil, userInfo: ["Group": group, "Notify": NotifyCRUD.read])
    }
}

extension BaseVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        refreshControl.tintColor = theme.settings.titleColor
    }
}
