//
//  AttachmentCollectionViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 13/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

protocol AttachmentCellDelegate:class {
    func didSelectToRemove(index: IndexPath)
}

class AttachmentCell: UICollectionViewCell {
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    weak var delegate: AttachmentCellDelegate!
    var index: IndexPath?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func removeItem(){
        delegate?.didSelectToRemove(index: self.index!)
    }
}
