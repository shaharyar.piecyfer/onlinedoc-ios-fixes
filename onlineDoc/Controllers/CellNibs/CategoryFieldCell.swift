//
//  CategoryFieldCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 16/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField
import BEMCheckBox

protocol CategoryFieldCellDelegate : class {
    func didWriteNewCategory(index: IndexPath, isChecked: Bool , category: String?, sortOrder: Int?)
    func didTapCheckMark(index: IndexPath, isChecked:Bool)
}

class CategoryFieldCell: UITableViewCell {
    
    @IBOutlet weak var newCategoryField : RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: newCategoryField)
            newCategoryField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var newcategoryOrderField : UITextField!
    
    @IBOutlet weak var checkBox : BEMCheckBox!{
        didSet{
            checkBox.boxType = .square
            checkBox.isUserInteractionEnabled = false
        }
    }
    
    weak var delegate: CategoryFieldCellDelegate?
    var index: IndexPath?
    var lastEnteredText = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        UITextField.appearance().tintColor = .black
        //Generic theme
        Themer.shared.register(
            target: self,
            action: CategoryFieldCell.applyTheme)
        let labels: [UILabel] = AppConfig.shared.getSubviewsOf(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: CategoryFieldCell.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == self.newCategoryField{
            if let text = textField.text{
                if text.count > 0{
                    self.checkBox.on = true
                    self.checkBox.isUserInteractionEnabled = true
                }else{
                    self.checkBox.on = false
                    self.checkBox.isUserInteractionEnabled = false
                }
            }
        }
    }
    
    func configCell(indexPath: IndexPath, data: GroupCategoryModel) {
        self.index = indexPath
        self.checkBox.tag = indexPath.row
        
        if let title = data.name{
            self.newCategoryField.text = title
        } else {
            self.newCategoryField.text = nil
        }
        if let sort = data.sort_order {
            self.newcategoryOrderField.text = String(sort)
        } else {
            self.newcategoryOrderField.text = nil
        }
        
        self.checkBox.on = data.isChecked
        
//        if indexPath.row % 2 == 0{
//            self.contentView.backgroundColor = AppColors.evenRow
//        }else{
//            self.contentView.backgroundColor = AppColors.oddRow
//        }
        
//        self.contentView.isUserInteractionEnabled = true
//        self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didSelectRow(!data.isChecked))))
    }
    
    @objc func didSelectRow(_ checked: Bool){
        self.delegate?.didTapCheckMark(index: self.index!, isChecked: checked)
    }
    
    func setData(indexPath: IndexPath, data: Category){
        
        self.index = indexPath
        
        self.checkBox.tag = indexPath.row
        
        if let title = data.name{
            self.newCategoryField.text = title
        }else{
            self.newCategoryField.text = nil
        }
        if let sort = data.sortOrder{
            self.newcategoryOrderField.text = String(sort)
        }else if data.order > 0{
            self.newcategoryOrderField.text = String(data.order)
        }else{
            self.newcategoryOrderField.text = nil
        }
        
        self.checkBox.on = data.isChecked
      
        if indexPath.row % 2 == 0{
            self.contentView.backgroundColor = AppColors.evenRow
        }else{
            self.contentView.backgroundColor = AppColors.oddRow
        }
    }
}

extension CategoryFieldCell: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == newCategoryField{
            if let text = textField.text, text != ""{
                self.delegate?.didWriteNewCategory(index: self.index!, isChecked: true, category: text, sortOrder: nil)
            }else{
                self.delegate?.didWriteNewCategory(index: self.index!, isChecked: false, category: nil, sortOrder: nil)
            }
        }
   
        if textField == newcategoryOrderField{
            if let txt = textField.text, txt != "", self.checkBox.on == true{
                if let order = Int(txt){
                    self.delegate?.didWriteNewCategory(index: self.index!, isChecked: true, category: nil, sortOrder: order)
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == newCategoryField{
            textField.resignFirstResponder()
        }
        return  true
        
    }
}

extension CategoryFieldCell: BEMCheckBoxDelegate{
    func didTap(_ checkBox: BEMCheckBox) {
        if checkBox.on{
            self.delegate?.didTapCheckMark(index: index!, isChecked: true)
        }else{
            self.delegate?.didTapCheckMark(index: index!, isChecked: false)
        }
    }
}

extension CategoryFieldCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        newCategoryField.textColor = theme.settings.titleColor
    }
}
