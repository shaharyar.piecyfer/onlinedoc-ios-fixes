//
//  CategoryRowCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 16/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox
import RAGTextField

protocol CategoryRowCellDelegate: class {
    func didTapCheckMark(index: IndexPath, order:Int?)
    func didEditCategory(at index: IndexPath, with Text : String)
}

class CategoryRowCell: UITableViewCell {
    
    @IBOutlet weak var iconHeight: NSLayoutConstraint!
    @IBOutlet weak var iconLeading: NSLayoutConstraint!
    @IBOutlet weak var CategoryTitleField:UITextField!
    @IBOutlet weak var categoryImageView : UIImageView!
    @IBOutlet weak var categoryTitleLabel : UILabel!
    @IBOutlet weak var categorySortOrderField : UITextField!{
        didSet{
            categorySortOrderField.delegate = self
        }
    }
    @IBOutlet weak var categoryCheckBoxView : BEMCheckBox!{
        didSet{
            categoryCheckBoxView.boxType = .square
            categoryCheckBoxView.isUserInteractionEnabled = false
        }
    }
    weak var delegate : CategoryRowCellDelegate!
    var title = ""
    var index: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        UITextField.appearance().tintColor = .black
        //Generic theme
        Themer.shared.register(
            target: self,
            action: CategoryRowCell.applyTheme)
        let labels: [UILabel] = AppConfig.shared.getSubviewsOf(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: CategoryRowCell.setTheme)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(indexPath: IndexPath, data: GroupCategoryModel) {
        self.categoryTitleLabel.text = nil
        if let title = data.name{
            self.categoryTitleLabel.text = title
        }else{
            self.categoryTitleLabel.text = nil
        }
        
        if data.isSelectedToEdit{
            self.categoryTitleLabel.isHidden = true
            self.CategoryTitleField.text = self.categoryTitleLabel.text
            self.CategoryTitleField.isHidden = false
            self.CategoryTitleField.becomeFirstResponder()
        }else{
            self.CategoryTitleField.isHidden = true
            self.categoryTitleLabel.isHidden = false
        }
        
        if let order = data.sort_order {
            self.categorySortOrderField.text = String(order)
        }else{
            self.categorySortOrderField.text = nil
        }
        
//        if indexPath.row % 2 == 0{
//            self.contentView.backgroundColor = AppColors.evenRow
//        }else{
//            self.contentView.backgroundColor = AppColors.oddRow
//        }
        self.index = indexPath
        self.categoryCheckBoxView.on = data.isChecked //data.id == nil ? (data.isChecked ? true : false) : (data.isUpdated ? data.isChecked : data.is_available)
        if indexPath.row == 0  {
            categorySortOrderField.isEnabled = false
        }
        if indexPath.row != 0 {
            self.contentView.isUserInteractionEnabled = true
            self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didSelectRow)))
        }
    }
    
    func setCell(indexPath: IndexPath, data: Category, isFromHomeScene: Bool = false){
        
        self.categoryTitleLabel.text = nil
        
        if isFromHomeScene{
            if let title = data.folderName{
                self.categoryTitleLabel.text = title
                self.CategoryTitleField.text = title
                
            }else{
                self.categoryTitleLabel.text = nil
                self.CategoryTitleField.text = nil
            }
        }else{
            if let title = data.name{
                self.categoryTitleLabel.text = title
            }else{
                self.categoryTitleLabel.text = nil
            }
        }
        
        if data.isSelectedToEdit{
            self.categoryTitleLabel.isHidden = true
            self.CategoryTitleField.isHidden = false
            self.CategoryTitleField.becomeFirstResponder()
        }else{
            self.CategoryTitleField.isHidden = true
            self.categoryTitleLabel.isHidden = false
        }
        
        if isFromHomeScene{
            
            if data.order != 0{
                self.categorySortOrderField.text = String(data.order)
            }else if let order = data.sortOrder, order != 0{
                self.categorySortOrderField.text = String(order)
            }else{
                self.categorySortOrderField.text = nil
            }
        }else{
            if data.order != 0{
                self.categorySortOrderField.text = String(data.order)
            }else if let order = data.sortOrder , order != 0{
                self.categorySortOrderField.text = String(order)
            }else{
                self.categorySortOrderField.text = nil
            }
            
        }

        if indexPath.row % 2 == 0{
            self.contentView.backgroundColor = AppColors.evenRow
        }else{
            self.contentView.backgroundColor = AppColors.oddRow
        }
        self.index = indexPath
        self.categoryCheckBoxView.on = data.isChecked ? true : false
        if indexPath.row != 0 {
            self.contentView.isUserInteractionEnabled = true
            self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didSelectRow)))
        }
    }
    
    func setData(indexPath: IndexPath, data: GroupModel){
        if let title = data.name {
            self.categoryTitleLabel.text = title
        }
        
        self.index = indexPath
        self.categoryCheckBoxView.tag = indexPath.row
        self.categorySortOrderField.isHidden = true
        self.categoryCheckBoxView.on = data.isSelected
        
        let imgUrl = data.file?.medium_url ?? data.file?.src
        if let imageUrl = imgUrl, imageUrl != "" {
            let url = URL(string: imageUrl)
            categoryImageView.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "ic_group"))
        }else {
            categoryImageView.image = #imageLiteral(resourceName: "ic_group")
        }
        
//        if indexPath.row % 2 == 0{
//            self.contentView.backgroundColor = AppColors.evenRow
//        }else{
//            self.contentView.backgroundColor = AppColors.oddRow
//        }
    }
    
    func setCellData(indexPath: IndexPath, data: FoldersList){
        if let title = data.folderName{
            self.categoryTitleLabel.text = title
        }
        
        self.categoryCheckBoxView.on = data.isSelected
        self.categorySortOrderField.isHidden = true
        if indexPath.row % 2 == 0{
            self.contentView.backgroundColor = AppColors.evenRow
        }else{
            self.contentView.backgroundColor = AppColors.oddRow
        }
        
        self.contentView.isUserInteractionEnabled = true
        self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didSelectRow)))
    }
    
    @objc func didSelectRow(){
        self.delegate?.didTapCheckMark(index: self.index!, order: nil)
    }
}

extension CategoryRowCell: BEMCheckBoxDelegate{
    
    func didTap(_ checkBox: BEMCheckBox) {
//        self.delegate?.didTapCheckMark(index: self.index!, order: nil)
    }
}

extension CategoryRowCell: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == categorySortOrderField{
            if let text = textField.text{
                if let sorting = Int(text){
                    self.delegate?.didTapCheckMark(index: self.index!, order: sorting)
                }
            }
        }else if textField == CategoryTitleField{
            if let txt = textField.text{
                self.delegate?.didEditCategory(at: self.index!, with: txt)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension CategoryRowCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        CategoryTitleField.textColor = theme.settings.titleColor
        categorySortOrderField.textColor = theme.settings.titleColor
    }
}
