//
//  ChatCellMultiFiles.swift
//  onlineDoc
//
//  Created by Zeeshan Ahmad Butt on 25/11/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import AVKit
import TRMosaicLayout

protocol ChatCellMultiFilesDelegate:class {
    func didTapImageViewMultiple(View: UIImageView)
    func didTapDeleteCommentMultiple(at index: Int)
    func didTapEditCommentMultiple(at index: Int)
    func didTapViewFileMultiple(file: String, name:String)
}


class ChatCellMultiFiles: UITableViewCell {
    
    var viewType = "" // mine, other
    
    @IBOutlet weak var backView:UIView! {
        didSet{
            backView.layer.cornerRadius = 10
            backView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet{
            profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewProfile)))
            profileImageView.layer.cornerRadius = profileImageView.layer.frame.height / 2
            profileImageView.clipsToBounds = true
            profileImageView.layer.borderWidth = 0.5
            profileImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    
    @IBOutlet weak var userOnlineStatus: UIView! {
        didSet{
            userOnlineStatus.layer.cornerRadius = userOnlineStatus.layer.frame.height / 2
            userOnlineStatus.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var moreButton:UIButton!
            
    var index = 0
    
    public typealias UserProfileCallBack = (_ index:Int) -> Void
    var callbacks : UserProfileCallBack?
    weak var delegate: ChatCellMultiFilesDelegate?
    
    // Zeeshan Added outlets and properties
    @IBOutlet weak var containerStack: UIStackView!
    @IBOutlet weak var attchementView :UIView!
    @IBOutlet weak var docListStackView:UIStackView!
    @IBOutlet weak var filesCollectionView: UICollectionView!
    @IBOutlet weak var filesCountView :UIView!
    @IBOutlet weak var filesCountLabel :UILabel!
    @IBOutlet weak var ivDoubleTick: UIImageView!
    
    var commentAttachments = [Message]()
    var attachedFiles = [Message](){
        didSet{
            if attachedFiles.count > 3{
                //Show view and count
                let remainingFiles = attachedFiles.count - 3
                self.filesCountView.isHidden = false
                self.filesCountLabel.text = "+\(remainingFiles)"
            }else{
                //Hide view and count
                self.filesCountView.isHidden = true
                self.filesCountLabel.text = ""
            }
        }
    }
    
    var attachedList = [Message]()
    var controller = AVPlayerViewController()
    var player = AVPlayer()
    var controller1 = AVPlayerViewController()
    var player1 = AVPlayer()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        filesCollectionView.register(UINib(nibName: "MultiFileAttachment", bundle: nil), forCellWithReuseIdentifier: "MultiFileAttachmentCell")
        let mosaicLayout = TRMosaicLayout()
        self.filesCollectionView?.collectionViewLayout = mosaicLayout
        mosaicLayout.delegate = self
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func feedMessagesMultiple(at indexPath:Int, with message: Message, of type: String) {
        self.viewType = type
        self.index = indexPath
        let data = (message.multiFiles?.first)!
        
        if let commentByImage = data.commentBy?.profilePicture, commentByImage != ""{
            if let url = URL(string: commentByImage){
                self.profileImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let moment = data.created_at{
            self.timeStampLabel.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
        }else{
            self.timeStampLabel.text = nil
        }

        self.userNameLabel.isHidden = true
        self.userOnlineStatus.isHidden = true
        
        // Now implement multifiles images/vdeo/pdf/etc function
        self.commentAttachments.removeAll()
        self.attachedFiles.removeAll()
        self.attachedList.removeAll()
        for subview in self.attchementView.subviews{
            if subview is UIStackView{
                subview.removeFromSuperview()
            }
        }
        
        if self.docListStackView != nil{
            self.docListStackView.subviews.forEach{ $0.removeFromSuperview()}
        }
        
        self.filesCollectionView.reloadData()
        
        // populate the data
        self.commentAttachments = message.multiFiles!
        let imgVidFiles = self.commentAttachments.filter{$0.file_main_type != "application"}
        
        if imgVidFiles.count == 0 {
            print("Do nothing because we don't have images or videos")
            self.filesCollectionView.isHidden = true
        }else if imgVidFiles.count > 2 {
            self.filesCollectionView.isHidden = false
            self.attachedFiles = imgVidFiles
            self.filesCollectionView.reloadData()
        }else{
            self.filesCollectionView.isHidden = true
            self.attachedFiles = imgVidFiles
            let stackView = UIStackView()
            stackView.alignment = .fill
            stackView.axis = .horizontal
            stackView.distribution = .fillEqually
            stackView.frame = self.attchementView.frame
            self.layoutAttachments(_stackView: stackView, data: imgVidFiles)
        }
        
        if imgVidFiles.count > 0 {
            self.attchementView.isHidden = false
        }else{
            self.attchementView.isHidden = true
        }
        
        let docFiles = self.commentAttachments.filter{$0.file_main_type == "application"}
        if docFiles.count > 0 {
            self.containerStack.spacing = 8
            self.docListStackView.isHidden = false
            self.attachedList = docFiles
            self.loadFilesList(docFiles)
        }else{
            self.docListStackView.isHidden = true
        }
        if self.viewType == "mine" {
            self.ivDoubleTick.image = self.ivDoubleTick.image?.withRenderingMode(.alwaysTemplate)
            if message.isRead {
                self.ivDoubleTick.tintColor = .systemBlue
            } else {
                self.ivDoubleTick.tintColor = .gray
            }
        }
        self.layoutIfNeeded()
    }
    
    func layoutAttachments(_stackView: UIStackView, data:[Message]){
        //TODO Handle the atachments here without using moasic layout for 1 or 2 files
        for file in data{
            if file.file_main_type == "image"{
                let imageView = UIImageView()
                if let image = file.file_name, let path = file.file_path,image != "" && path != ""{
                    let imagePath = path + image.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    if let url = URL(string: imagePath){
                        imageView.imageFromServer(imageUrl: url, placeholder: UIImage())
                        imageView.isUserInteractionEnabled = true
                        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
                        imageView.contentMode = .scaleAspectFill
                        imageView.clipsToBounds = true
                        _stackView.addArrangedSubview(imageView)
                        imageView.translatesAutoresizingMaskIntoConstraints = false
                        imageView.heightAnchor.constraint(equalToConstant: self.attchementView.frame.size.height).isActive = true
                    }
                }
            }else if file.file_main_type == "video"{
                if let name = file.file_name, let path = file.file_path, name != "" && path != ""{
                    let videoPath = path + name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    if let url = URL(string: videoPath){
                        if data.count > 1 && data[0].file_main_type == "video" &&  data[1].file_main_type == "video" && _stackView.subviews.count == 1{
                            player1 = AVPlayer(url: url)
                            controller1.player = player1
                            player1.rate = 0
                            self.player1.pause()
                            _stackView.addArrangedSubview(controller1.view)
                        }else{
                            player = AVPlayer(url: url)
                            controller.player = player
                            player.rate = 0
                            self.player.pause()
                            _stackView.addArrangedSubview(controller.view)
                        }
                    }
                }else{
                    let imageView = UIImageView()
                    imageView.image = #imageLiteral(resourceName: "attached")
                    imageView.contentMode = .scaleAspectFit
                    imageView.isUserInteractionEnabled = true
                    imageView.contentMode = .scaleToFill
                    _stackView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                }
            }else{
                let imageView = UIImageView()
                imageView.frame = _stackView.frame
                imageView.image = #imageLiteral(resourceName: "attached")
                imageView.contentMode = .scaleAspectFit
                imageView.isUserInteractionEnabled = true
                _stackView.addSubview(imageView)
                imageView.translatesAutoresizingMaskIntoConstraints = false
                imageView.trailingAnchor.constraint(equalTo: _stackView.trailingAnchor).isActive = true
                imageView.bottomAnchor.constraint(equalTo: _stackView.bottomAnchor).isActive = true
                imageView.leadingAnchor.constraint(equalTo: _stackView.leadingAnchor).isActive = true
                imageView.topAnchor.constraint(equalTo: _stackView.topAnchor).isActive = true
            }
        }
        self.attchementView.addSubview(_stackView)
        _stackView.translatesAutoresizingMaskIntoConstraints = false
        _stackView.trailingAnchor.constraint(equalTo: self.attchementView.trailingAnchor).isActive = true
        _stackView.bottomAnchor.constraint(equalTo: self.attchementView.bottomAnchor).isActive = true
        _stackView.leadingAnchor.constraint(equalTo: self.attchementView.leadingAnchor).isActive = true
        _stackView.topAnchor.constraint(equalTo: self.attchementView.topAnchor).isActive = true
        _stackView.heightAnchor.constraint(equalToConstant: self.attchementView.frame.size.height).isActive = true
    }
    
    func loadFilesList(_ files:[Message]){
        for file in files{
            if let fileView  = Bundle.main.loadNibNamed("DocumentListView", owner: self, options: nil)?.first as? DocumentListView{
                
                var fileRealName = ""
                
                if let title = file.real_name, let ext = file.file_ext{
                    fileView.documentTitle.text = title + "." + ext
                    fileRealName = title + "." + ext
                }
                
                if let path = file.file_path, let name = file.file_name{
                    let url = path + name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    fileView.fileUrl = url
                }
                
                fileView.file { (url) in
                    self.delegate?.didTapViewFileMultiple(file: url, name: fileRealName)
                }
                
                if let ext = file.file_ext{
                    switch ext {
                    case "pdf":
                        fileView.iconImageView.image = #imageLiteral(resourceName: "pdfIcon")
                        break
                    default:
                        fileView.iconImageView.image = #imageLiteral(resourceName: "attached")
                    }
                }
                fileView.tapButton.isEnabled = true
                self.docListStackView.addArrangedSubview(fileView)
            }
        }
    }
}

extension ChatCellMultiFiles : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.attachedFiles.count > 0 {
            return self.attachedFiles.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultiFileAttachmentCell", for: indexPath) as? MultiFileAttachmentCell else {
            print("Unable to read a collection view ThreadFilesCell")
            return UICollectionViewCell()
        }
        //TODO populate Cell here
        cell.controller.view.removeFromSuperview()
        cell.attachedImageView.image = UIImage()
        cell.messageDataToFeed(attachedFiles[indexPath.item], at: indexPath)
        return cell
    }
}

extension ChatCellMultiFiles : TRMosaicLayoutDelegate{
    
    func collectionView(_ collectionView: UICollectionView, mosaicCellSizeTypeAtIndexPath indexPath: IndexPath) -> TRMosaicCellType {
        if attachedFiles.count > 1{
            return indexPath.item % 3 == 0 ? .big : .small
        }else{
            return .big
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: TRMosaicLayout, insetAtSection: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func heightForSmallMosaicCell() -> CGFloat {
        if attachedFiles.count > 1{
            return 110
        }else{
            return 150
        }
    }
}

extension ChatCellMultiFiles {
    
    func profile(callback : @escaping UserProfileCallBack) {
        self.callbacks = callback
    }
    
    @objc func viewProfile(){
        self.callbacks!(self.index)
    }
    
    @objc func imageViewTap(_ sender : UITapGestureRecognizer){
        if let imageView = sender.view as? UIImageView{
            self.delegate?.didTapImageViewMultiple(View: imageView)
        }
    }
    
    @IBAction func showMore(_ sender:UIButton){
        self.delegate?.didTapDeleteCommentMultiple(at: self.index)
    }
    
    @IBAction func showMoreMine(_ sender:UIButton){
        self.delegate?.didTapEditCommentMultiple(at: self.index)
    }
}

