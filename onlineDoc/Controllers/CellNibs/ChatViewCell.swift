//
//  ChatViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 22/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ChatViewCell: UITableViewCell {
    
    var memberId = 0
    let supportedVideoFormats = ["mp4","mov", "avi","mkv","MOV"]
    let supportedImageFormats = ["jpeg", "gif", "bmp", "tiff", "png","JPG","jpg","HEIC"]
    @IBOutlet weak var personImageView: UIImageView!{
        didSet{
            personImageView.layer.cornerRadius = personImageView.frame.size.height / 2
            personImageView.clipsToBounds = true
            personImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
            personImageView.layer.borderWidth = 0.5
        }
    }
    @IBOutlet weak var personActiveStatus: UIView!{
        didSet{
            personActiveStatus.layer.cornerRadius = personActiveStatus.frame.size.height / 2
            personActiveStatus.clipsToBounds = true
        }
    }
    @IBOutlet weak var commentTextLabel: UILabel!
    @IBOutlet weak var attachmentImageView: UIImageView!{
        didSet{
            attachmentImageView.layer.cornerRadius = attachmentImageView.frame.size.height / 2
            attachmentImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var commentView: UIView!{
        didSet{
            commentView.layer.cornerRadius = 6.0
            commentView.clipsToBounds = true
        }
    }
    @IBOutlet weak var commentAtLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(indexPath: IndexPath, data : Message, member: GroupMember){
        
        if let message = data.message{
            self.commentTextLabel.text = message
        }else{
            self.commentTextLabel.text = nil
        }
        
        if let  moment = data.created_at{
            self.commentAtLabel.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
        }
        
        
        
        if data.isServerFile{
            //TODO server file
            if data.file_ext != nil{
                if data.file_ext == "jpg"{
                    ///TODO Show Image from Url
                    if let image = data.file_name, let path = data.file_path,image != "" && path != ""{
                        let imagePath = image + path
                        if let url = URL(string: imagePath){
                            self.attachmentImageView.imageFromServer(imageUrl: url)
                        }else{
                            self.attachmentImageView.image = nil
                        }
                    }else{
                        self.attachmentImageView.image = nil
                    }
                    
                }else if self.supportedVideoFormats.contains((data.file_ext?.lowercased())!){
                    /// Hanlde the video thing here
//
//                    if let video = data.file_name, let path = data.file_path,video != "" && path != ""{
//                        let imagePath = video + path
//                        if let url = URL(string: imagePath){
//                            //self.attachmentImageView.imageFromServer(imageUrl: url)
//                            let player = AVPlayer(url: url)
//                            let controller=AVPlayerViewController()
//                            controller.player=player
//                            controller.view.frame = self.attachmentImageView.frame
//                            self.attachmentImageView.addSubview(controller.view)
////                            self.attachmentImageView.addChildViewController(controller)
//                            player.play()
//                        }else{
//                            self.attachmentImageView.image = nil
//                        }
//                    }
                }else{
                    /// Hanlde the other attachment here like documents
                    self.attachmentImageView.image = #imageLiteral(resourceName: "attached")
                }
            }else{
                self.attachmentImageView.image = nil
            }
        }else{
            /// show local attached  file uploaded by user
            self.attachmentImageView.image = data.image
            
        }
        
        
        
        
        
        
//
//
//        if let name = AppConfig.shared.user.name{
//            self.myNameLabel.text = name.capitalized
//        }
//
//        if let message = data.message{
//            self.myMessageTextLabel.text = message
//        }
        if let image = data.file_name,let path = data.file_path{
            let img = path + image
            if let url = URL(string: img){
//                self.myAttachmentImageView.imageFromServer(imageUrl: url)
            }else{
//                self.myAttachmentImageView.image = nil
            }
        }else{
//            self.myAttachmentImageView.image = nil
        }
    }
    
    func setThreadComment(indexPath: IndexPath, data : ThreadComment){
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.backgroundColor = UIColor.red
        nameLabel.numberOfLines = 0

        nameLabel.sizeToFit()
        self.commentTextLabel.translatesAutoresizingMaskIntoConstraints = false
        
        if let name = data.commentBy?.name.capitalized{
            nameLabel.text = name
        }
        if let comment = data.comment{
            self.commentTextLabel.text = comment.capitalized
        }else{
            self.commentTextLabel.text = nil
        }
        
        self.commentView.addSubview(nameLabel)
        
        nameLabel.leadingAnchor.constraint(equalTo: commentView.leadingAnchor, constant: 20).isActive = true
        nameLabel.topAnchor.constraint(equalTo: commentView.topAnchor, constant: 20).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: commentView.trailingAnchor, constant: 20).isActive = true
        self.commentTextLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 10).isActive = true
        self.commentTextLabel.leadingAnchor.constraint(equalTo: commentView.leadingAnchor, constant: 20).isActive = true
        self.commentTextLabel.trailingAnchor.constraint(equalTo: commentView.trailingAnchor, constant: 20).isActive = true
        self.commentTextLabel.bottomAnchor.constraint(equalTo: commentView.bottomAnchor, constant: 8).isActive = true
        
        if let commentByImage = data.commentBy?.profilePicture, commentByImage != ""{
            if let url = URL(string: commentByImage){
                self.personImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                self.personImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.personImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        
        
//        let constraints = [
//
//
//            view.centerXAnchor.constraint(equalTo: superview.centerXAnchor),
//            view.centerYAnchor.constraint(equalTo: superview.centerYAnchor),
//            view.widthAnchor.constraint(equalToConstant: 100),
//            view.heightAnchor.constraint(equalTo: view.widthAnchor)
//        ]
//        NSLayoutConstraint.activate(constraints)
        
        
        
        
    }
    
    
   /* {
            
        if let message = data.comment{
                self.commentTextLabel.text = message
            }else{
                self.commentTextLabel.text = nil
            }
            
        if let  moment = data.added_date{
                self.commentAtLabel.text = AppConfig.shared.convertToDate(from: moment.UTCToLocal).getMoment() + " ago"
            }
                        if data.file_ext != nil{
                            if data.file_ext == "jpg"{
                                ///TODO Show Image from Url
                                if let image = data.file_name, let path = data.file_path,image != "" && path != ""{
                                    let imagePath = image + path
                                    if let url = URL(string: imagePath){
                                        self.attachmentImageView.imageFromServer(imageUrl: url)
                                    }else{
                                        self.attachmentImageView.image = nil
                                    }
                                }else{
                                    self.attachmentImageView.image = nil
                                }
                                
                            }else if self.supportedVideoFormats.contains((data.file_ext?.lowercased())!){
                                /// Hanlde the video thing here
            //
            //                    if let video = data.file_name, let path = data.file_path,video != "" && path != ""{
            //                        let imagePath = video + path
            //                        if let url = URL(string: imagePath){
            //                            //self.attachmentImageView.imageFromServer(imageUrl: url)
            //                            let player = AVPlayer(url: url)
            //                            let controller=AVPlayerViewController()
            //                            controller.player=player
            //                            controller.view.frame = self.attachmentImageView.frame
            //                            self.attachmentImageView.addSubview(controller.view)
            ////                            self.attachmentImageView.addChildViewController(controller)
            //                            player.play()
            //                        }else{
            //                            self.attachmentImageView.image = nil
            //                        }
            //                    }
                            }else{
                                /// Hanlde the other attachment here like documents
                                self.attachmentImageView.image = #imageLiteral(resourceName: "attached")
                            }
                        }else{
                            self.attachmentImageView.image = nil
                        }

            
            
            
            
            
    //
    //
    //        if let name = AppConfig.shared.user.name{
    //            self.myNameLabel.text = name.capitalized
    //        }
    //
    //        if let message = data.message{
    //            self.myMessageTextLabel.text = message
    //        }
            
            
            
            if let image = data.file_name,let path = data.file_path{
                let img = path + image
                if let url = URL(string: img){
    //                self.myAttachmentImageView.imageFromServer(imageUrl: url)
                }else{
    //                self.myAttachmentImageView.image = nil
                }
            }else{
    //            self.myAttachmentImageView.image = nil
            }
        }
    */
    
}

