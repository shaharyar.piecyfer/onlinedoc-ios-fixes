//
//  CommentDetailViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 26/02/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class CommentDetailViewCell: UITableViewCell {
    
    //MARK: - IBOUTLETs
    
 
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var momentsLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!{
         didSet{
             profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
             profileImageView.clipsToBounds = true
         }
     }
    @IBOutlet weak var attachmentImageView: UIImageView!{
        didSet{
            attachmentImageView.layer.cornerRadius = 6.0
            attachmentImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var profileImageCommentLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var profileImageCommentTrailingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCommentDate(_ indexPath : IndexPath, data: ThreadComment){
        //TODO Check if its my comment
        if let personId = data.user_id{
//            if personId == AppConfig.shared.user.id{
//                //TODO : - its my comment and set the view accordingly
//            }
            if let commentTxt = data.comment{
                self.commentLabel.textAlignment = .right
                self.commentLabel.text = commentTxt
            }
            if let filePath = data.file_path, let fileName = data.real_name, (filePath != "" && fileName != ""){
                let fileUrlComponent = filePath + fileName
                if let url = URL(string: fileUrlComponent){
                    self.attachmentImageView.imageFromServer(imageUrl: url)
                }
            }else{
                self.attachmentImageView.image = nil
                self.attachmentImageView.isHidden = true
            }
        
        }else{
            //TODO check if its other persons comment
            if let firstName = data.profile?.f_name, let lastName = data.profile?.l_name{
                self.nameLabel.text = firstName + " " + lastName
            }
            if let commentTxt = data.comment{
                self.commentLabel.textAlignment = .left
                self.commentLabel.text = commentTxt
            }
            if let fileExtension = data.file_ext, fileExtension == "jpg"{
                if let filePath = data.file_path, let fileName = data.real_name, (filePath != "" && fileName != ""){
                    let fileUrlComponent = filePath + fileName
                    if let url = URL(string: fileUrlComponent){
                        self.attachmentImageView.imageFromServer(imageUrl: url)
                    }
                }
            }else if let fileExtension = data.file_ext, fileExtension.lowercased() == "mov" {
                //TODO Handle the video thing here
                
            }else{
                self.attachmentImageView.image = nil
                self.attachmentImageView.isHidden = true
            }
            
        }
    }
}
