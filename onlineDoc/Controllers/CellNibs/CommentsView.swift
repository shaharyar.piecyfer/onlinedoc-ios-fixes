//
//  CommentsView.swift
//  onlineDoc
//
//  Created by Piecyfer on 06/04/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

protocol CommentsViewDelegate: class {
    func didTapProfileImage(with id: Int)
}

class CommentsView: UIView {
    
    @IBOutlet weak var contentView:UIView!{
        didSet{
            contentView.layer.cornerRadius = 20
            contentView.clipsToBounds = true
        }
    }
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.imageViewTap)))
            profileImageView.layer.cornerRadius = profileImageView.layer.frame.height / 2
            profileImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileCommentLabel: UILabel!
    @IBOutlet weak var profilemomentLabel: UILabel!
    
    weak var delegate : CommentsViewDelegate?
    var memberID = 0
    @objc func imageViewTap(){
        self.delegate?.didTapProfileImage(with: self.memberID)
    }
    
    func setTheme() {
        Themer.shared.register(
            target: self,
            action: CommentsView.setTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
    }
}

extension CommentsView {
    fileprivate func setTheme(_ theme: MyTheme) {
        contentView.backgroundColor = theme.settings.highlightedBgColor
    }
}
