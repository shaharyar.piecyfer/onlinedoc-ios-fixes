//
//  ConnectionViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 22/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class ConnectionViewCell: UITableViewCell {
    
    public typealias UserProfileCallBack = (_ index:IndexPath, _ profile:Bool, _ accept: Bool) -> Void
    var callbacks : UserProfileCallBack?
    
    var index: IndexPath?
    
    @IBOutlet weak var connectionImageView: UIImageView!{
        didSet{
            connectionImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewProfile)))
            connectionImageView.layer.cornerRadius = connectionImageView.frame.size.height / 2
            connectionImageView.clipsToBounds = true
            connectionImageView.contentMode = .scaleAspectFill
            connectionImageView.layer.borderWidth = 0.5
            connectionImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var connectionNameLabel: UILabel!
    @IBOutlet weak var lblLastChatTime: UILabel!
    @IBOutlet weak var connectionLastMessageLabel: UILabel!
    @IBOutlet weak var connectionOnlineStatusView: UIView!
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var acceptRequestButton : UIButton!
    @IBOutlet weak var cancelRequestButton : UIButton!
    @IBOutlet weak var plusIconImage : UIImageView!{
        didSet{
            plusIconImage.layer.cornerRadius = plusIconImage.frame.size.height / 2
            plusIconImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var iconView : UIView!{
        didSet{
            iconView.layer.cornerRadius = iconView.frame.size.height / 2
            iconView.clipsToBounds = true
        }
    }
    @IBOutlet weak var badgeView:UIView!{
        didSet{
            badgeView.layer.cornerRadius = badgeView.frame.size.height / 2
            badgeView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var countLabel: UILabel!{
        didSet{
            countLabel.layer.cornerRadius = countLabel.frame.size.height / 2
            countLabel.clipsToBounds = true
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.acceptRequestButton.isHidden = true
        self.cancelRequestButton.isHidden = true
        self.badgeView.isHidden = true
        self.connectionOnlineStatusView.isHidden = true
        self.iconView.isHidden = true
        self.lblLastChatTime.isHidden = true
        self.connectionOnlineStatusView.backgroundColor = .gray
        //Generic theme
        Themer.shared.register(
            target: self,
            action: ConnectionViewCell.applyTheme)

        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: ConnectionViewCell.setTheme)
        
        self.connectionOnlineStatusView.backgroundColor = AppColors.lightGrayColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(_ indexPath: IndexPath, data: ChatRoomModel) {
        self.index = indexPath
        if data.user?.virtual_user ?? false {
            if let name = data.user?.full_name {
                self.connectionNameLabel.text = name.lowercased().contains("virtual") ? (data.user?.email ?? data.user?.phone) : data.user?.full_name?.capitalized
            }
            else {
                self.connectionNameLabel.text = data.user?.email ?? data.user?.phone
            }
        }else if let name = data.user?.full_name{
            self.connectionNameLabel.text = name.capitalized
        }else{
            self.connectionNameLabel.text = ""
        }
        
        if let lastMessage = data.lastMessage, !lastMessage.isEmpty{
            self.connectionLastMessageLabel.attributedText = lastMessage.attributedText(fontSize: 12, allStyleFont: AppFont.kMuliLight(12).font())?.attributedString
        }else if let lastMessage = data.lastMessage, lastMessage.isEmpty {
            self.connectionLastMessageLabel.text = ""
        }else {
            self.connectionLastMessageLabel.text = ""
        }
        
        self.acceptRequestButton.isHidden = true
        self.cancelRequestButton.isHidden = true
        self.badgeView.isHidden = false
        self.connectionOnlineStatusView.isHidden = false
        if let count = data.chat?.message_count {
            if count > 0 {
                self.badgeView.isHidden = false
                self.countLabel.text = count > 9 ? "9+" : "\(count)"
            }else{
                self.badgeView.isHidden = true
            }
        }else{
            self.badgeView.isHidden = true
        }
        
        //Online status
        if let status = data.user?.online_status {
            switch status {
            case OnlineMode.ONLINE.rawValue:
                self.connectionOnlineStatusView.backgroundColor = .green
            case OnlineMode.AWAY.rawValue:
                self.connectionOnlineStatusView.backgroundColor = .orange
            default:
                self.connectionOnlineStatusView.backgroundColor = .gray
            }
        }else {
            self.connectionOnlineStatusView.backgroundColor = .gray
        }
        
        if data.friend_status?.requestStatus == FriendStatus.ACCEPT.rawValue {
            self.iconView.isHidden = true
            let imgUrl = data.user?.file?.medium_url ?? data.user?.file?.src
            if let imagePath = imgUrl, imagePath != ""{
                if let url = URL(string: imagePath){
                    self.connectionImageView.imageFromServer(imageUrl: url)
                }else{
                    self.connectionImageView.image = #imageLiteral(resourceName: "ic_user")
                }
            }else{
                self.connectionImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }
        else {
            self.iconView.isHidden = false
            self.connectionImageView.image = #imageLiteral(resourceName: "ic_user")
            self.connectionImageView.layer.borderWidth = 1.5
        }
        
        self.connectionOnlineStatusView.layer.cornerRadius   = self.connectionOnlineStatusView.bounds.height / 2
        self.connectionOnlineStatusView.layer.borderWidth = 1.0
        self.connectionOnlineStatusView.layer.borderColor = UIColor.white.cgColor
        self.backView?.layer.cornerRadius = 6.0
        self.backView?.clipsToBounds = true
        
        self.lblLastChatTime.isHidden = true
        let updatedTime = data.last_message_created_at ?? data.chat?.updated_at
        if let moment = updatedTime {
            self.lblLastChatTime.isHidden = false
            self.lblLastChatTime.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
        }
    }
    
    func setupRequests(_ indexPath: IndexPath, data: MemberModel) {
        if let  name = data.full_name{
            self.connectionNameLabel.text = name.capitalized
        }else{
            self.connectionNameLabel.text = ""
        }
        if let title = data.clinic_name{
            self.connectionLastMessageLabel.text = title
        }else{
            self.connectionLastMessageLabel.text = ""
        }
        
        self.acceptRequestButton.isHidden = false
        self.cancelRequestButton.isHidden = false
        self.lblLastChatTime.isHidden = true
//        if data.isAdminRecord {
//            self.cancelRequestButton.isHidden = false
//        }else{
//            self.cancelRequestButton.isHidden = true
//        }
        self.badgeView.isHidden = true
        self.connectionOnlineStatusView.isHidden = true
        
        self.iconView.isHidden = false
        let imgUrl = data.file?.medium_url ?? data.file?.src
        if let imagePath = imgUrl, imagePath != ""{
            if let url = URL(string: imagePath){
                self.connectionImageView.imageFromServer(imageUrl: url)
            }else{
                self.connectionImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.connectionImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        self.connectionImageView.layer.borderWidth = 1.5
    }

    func action(callback: @escaping UserProfileCallBack) {
        self.callbacks = callback
    }

    @objc func viewProfile() {
        self.callbacks!(self.index!, true, false)
    }
    @IBAction func acceptGropupRequest(){
        self.callbacks!(self.index!, false, true)
    }
    @IBAction func cancelGroupRequest(){
        self.callbacks!(self.index!, false, false)
    }
}

extension ConnectionViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        connectionLastMessageLabel.textColor = theme.settings.subTitleColor
        lblLastChatTime.textColor = theme.settings.subTitleColor
        countLabel.textColor = theme.settings.whiteColor
    }
}
