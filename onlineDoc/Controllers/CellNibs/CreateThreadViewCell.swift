//
//  CreateThreadViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 18/02/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

protocol CreateThreadViewCellDelegate: class {
    func createThreadWith(type:Int)
}

class CreateThreadViewCell: UITableViewCell {
    @IBOutlet weak var userView: UIView!{
        didSet{
            userView.layer.cornerRadius = 20
        }
    }
    
    @IBOutlet weak var myProfileImageView: UIImageView!{
        didSet{
            myProfileImageView.layer.cornerRadius = myProfileImageView.frame.size.height / 2
            myProfileImageView.clipsToBounds = true
            
            myProfileImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
            myProfileImageView.layer.borderWidth = 0.5
        }
    }
    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var ivCamera: UIImageView!
    @IBOutlet weak var ivDocument: UIImageView!
    @IBOutlet weak var ivVideo: UIImageView!
    @IBOutlet weak var ivScan: UIImageView!
    
    weak var delegate: CreateThreadViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: CreateThreadViewCell.applyTheme)

        let subviews = AppConfig.shared.getLabelsInView(view: self.contentView)
        for item in subviews {
            Themer.shared.register(
                target: item,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: CreateThreadViewCell.setTheme)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        contentView.frame = contentView.frame.inset(by: margins)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func createThreadWithLibrary(){
        self.delegate?.createThreadWith(type: 1)
    }
    
    @IBAction func createThreadWithCamera(){
        self.delegate?.createThreadWith(type: 2)
    }
    
    @IBAction func createThreadWithDocuments(){
        self.delegate?.createThreadWith(type: 3)
    }
    
    @IBAction func createThreadWithVideo() {
        self.delegate?.createThreadWith(type: 4)
    }
    
    @IBAction func createThreadWithScan() {
        self.delegate?.createThreadWith(type: 5)
    }
}

extension CreateThreadViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        userView.backgroundColor = theme.settings.highlightedBgColor
        ivPhoto.image = ivPhoto.image?.withRenderingMode(.alwaysTemplate)
        ivPhoto.tintColor = theme.settings.titleColor
        ivCamera.image = ivCamera.image?.withRenderingMode(.alwaysTemplate)
        ivCamera.tintColor = theme.settings.titleColor
        ivDocument.image = ivDocument.image?.withRenderingMode(.alwaysTemplate)
        ivDocument.tintColor = theme.settings.titleColor
        ivScan.image = ivScan.image?.withRenderingMode(.alwaysTemplate)
        ivScan.tintColor = theme.settings.titleColor
        ivVideo.image = ivVideo.image?.withRenderingMode(.alwaysTemplate)
        ivVideo.tintColor = theme.settings.titleColor
    }
}
