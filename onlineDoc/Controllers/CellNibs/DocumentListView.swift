//
//  DocumentListView.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 24/06/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class DocumentListView: UIView {
    public typealias FileCallBack = (_ url:String) -> Void
    var fileCallBacks : FileCallBack?
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var documentTitle: UILabel!
    @IBOutlet weak var documentDec: UILabel!
    @IBOutlet weak var tapButton: UIButton!
    var fileUrl = ""
    
    func file(callback : @escaping FileCallBack) {
        self.fileCallBacks = callback
    }
    
    @IBAction func fileTapped(){
       self.fileCallBacks!(self.fileUrl)
    }
    
    func setTheme() {
        Themer.shared.register(
            target: self,
            action: DocumentListView.setTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
    }
}

extension DocumentListView {
    fileprivate func setTheme(_ theme: MyTheme) {
        self.backgroundColor = theme.settings.highlightedBgColor
    }
}
