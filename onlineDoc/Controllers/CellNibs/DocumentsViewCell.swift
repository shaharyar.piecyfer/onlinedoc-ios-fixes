//
//  DocumentsViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 02/06/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import AVKit

protocol DocumentsViewCellDelegate: class {
    func download(image: UIImage?, url: String?, fileName: String?)
    func deleteFile(at index: IndexPath)
}

class DocumentsViewCell: UITableViewCell {
    @IBOutlet weak var documentIconImageView: UIImageView!{
        didSet{
            documentIconImageView.layer.cornerRadius = 6
            documentIconImageView.clipsToBounds = true
            documentIconImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
            documentIconImageView.layer.borderWidth = 1
        }
    }
    @IBOutlet weak var documentTitleLabel: UILabel!
    @IBOutlet weak var documentUploadedByLabel: UILabel!
    @IBOutlet weak var documenteUploadTimeStampLabel: UILabel!

    @IBOutlet weak var downloadFileImageView: UIImageView!{
        didSet{
            let image = UIImage(named: "docDownload")?.withRenderingMode(.alwaysTemplate)
            downloadFileImageView.image = image
            downloadFileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(downlaodAt)))
        }
    }
    
    @IBOutlet weak var deleteFileImageView: UIImageView!{
        didSet{
            let image = UIImage(named: "delete")?.withRenderingMode(.alwaysTemplate)
            deleteFileImageView.image = image
            deleteFileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(deleteAt)))
        }
    }
    @IBOutlet weak var fileContainerView: UIView!{
        didSet{
            fileContainerView.layer.cornerRadius = 6
            fileContainerView.clipsToBounds = true
           // videoView.layer.borderColor = UIColor.white.cgColor
           // videoView.layer.borderWidth = 1
        }
    }
    var index: IndexPath?
    var file: FileModel?
//    var fileExt = ""
//    var fileUrl = ""
//    var fileName = ""
//    var fileMainType = ""
//    var adminId = 0
//    var ThreadCreatorId = 0
    let contrlr = AVPlayerViewController()
    weak var delegate : DocumentsViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: DocumentsViewCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: DocumentsViewCell.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setDocument(with document: ThreadCommentModel, at indexpath: IndexPath) {
        self.index = indexpath
        self.file = document.files?.first
        self.fileContainerView.subviews.forEach{ $0.removeFromSuperview() }
//        if let ext = document.file_main_type {
//            self.fileMainType = ext
//        }
        if let file = document.files?.first {
            if let ext = file.content_type {
                if ext.contains("image") {
                    self.showImage(file)
                }
                else if ext.contains("video") {
                    self.showVideo(file)
                }
                else {
                    self.showDoc(file)
                }
            }
            
            if let name = document.comment_creator?.full_name {
                self.documentUploadedByLabel.text = name.capitalized
            }else{
                self.documentUploadedByLabel.text = nil
            }
            
            if let title = file.name {
                self.documentTitleLabel.text = title.capitalized
            }else{
                self.documentTitleLabel.text = nil
            }
            
            if let createdAt = document.created_at {
                self.documenteUploadTimeStampLabel.text = AppConfig.shared.convertToDate(from: createdAt).getMoment() + AgoText.localized()
            }else{
                self.documenteUploadTimeStampLabel.text = nil
            }
            
            //        if let fileName = document.name{
            //            self.fileName = fileName
            //        }
        }
    }
    
    func setRights(group:Int){
        
    }
    
    func showImage(_ document: FileModel) {
        contrlr.view.removeFromSuperview()
        let imageView = UIImageView()
        imageView.frame = self.fileContainerView.frame
        if let path = document.src, path != "" {
            if let url = URL(string: path){
                imageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "defaultImage"))
                self.fileContainerView.addSubview(imageView)
                imageView.translatesAutoresizingMaskIntoConstraints = false
                imageView.trailingAnchor.constraint(equalTo: self.fileContainerView.trailingAnchor).isActive = true
                imageView.bottomAnchor.constraint(equalTo: self.fileContainerView.bottomAnchor).isActive = true
                imageView.leadingAnchor.constraint(equalTo: self.fileContainerView.leadingAnchor).isActive = true
                imageView.topAnchor.constraint(equalTo: self.fileContainerView.topAnchor).isActive = true
                imageView.heightAnchor.constraint(equalToConstant: self.fileContainerView.frame.size.height).isActive = true
            }
        }
    }
    
    func showVideo(_ document: FileModel) {
        if let path = document.src, path != "" {
            if let url = URL(string: path){
                let player = AVPlayer(url: url)
                let ctrl = AVPlayerViewController()
                ctrl.player = player
                ctrl.view.frame = self.fileContainerView.frame
                self.fileContainerView.addSubview(ctrl.view)
                player.pause()
                ctrl.view.translatesAutoresizingMaskIntoConstraints = false
                ctrl.view.trailingAnchor.constraint(equalTo: self.fileContainerView.trailingAnchor).isActive = true
                ctrl.view.bottomAnchor.constraint(equalTo: self.fileContainerView.bottomAnchor).isActive = true
                ctrl.view.leadingAnchor.constraint(equalTo: self.fileContainerView.leadingAnchor).isActive = true
                ctrl.view.topAnchor.constraint(equalTo: self.fileContainerView.topAnchor).isActive = true
            }
        }else{
//             self.documentIconImageView.image = #imageLiteral(resourceName: "attached")
        }
    }
    
    func showDoc(_ document: FileModel) {
        contrlr.view.removeFromSuperview()
        let imageView = UIImageView()
        imageView.frame = self.fileContainerView.frame
        if document.content_type?.contains("pdf") ?? false {
            imageView.image = UIImage(named: "pdfIcon")
        }
        else if document.content_type?.contains("word") ?? false {
            imageView.image = UIImage(named: "ic_word")
        }
        else if (document.content_type?.contains("sheet") ?? false) || (document.content_type?.contains("excel") ?? false) {
            imageView.image = UIImage(named: "ic_excel")
        }
        else if document.content_type?.contains("text") ?? false {
            imageView.image = UIImage(named: "ic_text")
        }
        else {
            imageView.image = #imageLiteral(resourceName: "attached")
        }
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleToFill
        self.fileContainerView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.trailingAnchor.constraint(equalTo: self.fileContainerView.trailingAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.fileContainerView.bottomAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: self.fileContainerView.leadingAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: self.fileContainerView.topAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: self.fileContainerView.frame.size.height).isActive = true
    }
    
    @objc func downlaodAt() {
        if let file = self.file, let type = file.content_type {
            if type.contains("image") {
                var image = UIImage()
                for subview in self.fileContainerView.subviews {
                    if subview is UIImageView {
                        if let view = subview as? UIImageView {
                            if let img = view.image {
                                image = img
                            }
                        }
                    }
                }
                self.delegate?.download(image: image, url: nil, fileName: nil)
            }
            else {
                self.delegate?.download(image: nil, url: file.src, fileName: file.name)
            }
        }
    }

    @objc func deleteAt() {
        self.delegate?.deleteFile(at: self.index!)
    }
}


extension DocumentsViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        downloadFileImageView.tintColor = theme.settings.subTitleColor
        deleteFileImageView.tintColor = theme.settings.subTitleColor
    }
}
