//
//  FolderThreadCell.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 06/07/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class FolderThreadCell: UITableViewCell {
    
    @IBOutlet weak var threadTitleLabel : UILabel!
    @IBOutlet weak var threadDescLabel : UILabel!
    @IBOutlet weak var counterView: UIView!{
        didSet{
            counterView.layer.cornerRadius = 2
            counterView.clipsToBounds = true
        }
    }
    @IBOutlet weak var counterLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: FolderThreadCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: FolderThreadCell.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(_ indxPath: IndexPath, data: ThreadModel) {
        if let title = data.title{
            threadTitleLabel.text = title.isEmpty ? data.description : title
        }else{
            threadTitleLabel.text = data.description ?? ""
        }
        if let desc = data.clinic_category_name {
            threadDescLabel.text = desc
        }else{
            threadDescLabel.text = ""
        }
//        print(category.params.page)
//        print("pageNo: \(category.params.page)")
//        print("Total Count: \(category.threadUnreadCount)")
//        let arryofCounts = self.subArray(category.params.page, source: category.threadUnreadCount)
//        print(arryofCounts)
//
        if data.doc_notification_count == 0 {
            self.counterLabel.text = ""
            self.counterView.isHidden = true
        }else{
            self.counterLabel.text = "\(data.doc_notification_count ?? 0)"
            self.counterView.isHidden = false
        }
    }
    
    func setThreadData(_ indxPath: IndexPath,of category: Category)  {
        if let data = category.threads?[indxPath.row]{
            if let title = data.title{
                threadTitleLabel.text = title
                
            }else{
                threadTitleLabel.text = ""
            }
            if let desc = data.journal{
                threadDescLabel.text = desc
            }else{
                threadDescLabel.text = ""
            }
            print(category.threadIds?[indxPath.row])
            print(category.threadUnreadCount[indxPath.row])
        }
//        print(category.params.page)
        print("pageNo: \(category.params.page)")
        print("Total Count: \(category.threadUnreadCount)")
        let arryofCounts = self.subArray(category.params.page, source: category.threadUnreadCount)
        print(arryofCounts)
        
        if arryofCounts[indxPath.row] == 0 {
            self.counterLabel.text = ""
            self.counterView.isHidden = true
        }else{
            self.counterLabel.text = "\(arryofCounts[indxPath.row])"
            self.counterView.isHidden = false
        }
        
        
    }
    
    func subArray(_ page: Int, source: [Int])-> [Int]{
        
        let start = page*5
        var end = start+4
        
        var respArray = [Int]()
        
        if end > source.count-1{
            end = source.count-1
            if start == end {
                respArray = [source[start]]
            }else{
                respArray = source[start ... end].map { Int($0) }
            }
        }else{
            respArray = source[start ... end].map { Int($0) }
        }
        return respArray
    }
}

extension FolderThreadCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        threadDescLabel.textColor = theme.settings.subTitleColor
        counterLabel.textColor = theme.settings.whiteColor
    }
}
