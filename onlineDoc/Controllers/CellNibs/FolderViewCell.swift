//
//  HeaderViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 24/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox


protocol FolderViewCellDelegate: AnyObject {
    func createNew()
}

class FolderViewCell: UITableViewCell {
    
    weak var delegate: FolderViewCellDelegate?

    @IBOutlet weak var iconImageView:UIImageView! {
        didSet {
            let image = UIImage(named: "carrot")?.withRenderingMode(.alwaysTemplate)
            iconImageView.image = image
        }
    }
    @IBOutlet weak var groupImageView:UIImageView! {
        didSet {
            let image = UIImage(named: "folder")?.withRenderingMode(.alwaysTemplate)
            groupImageView.image = image
        }
    }
    @IBOutlet weak var createButton:UIButton!
       /* {
        didSet{
            groupImageView.layer.cornerRadius = groupImageView.bounds.size.height / 2
            groupImageView.clipsToBounds = true
        }
    }*/
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var threadsCountLabel:UILabel!
    @IBOutlet  weak var counterView:UIView!{
        didSet{
            counterView.layer.cornerRadius = 2
            counterView.clipsToBounds = true
        }
    }
    @IBOutlet  weak var counterLabel:UILabel!
    @IBOutlet weak var checkBoxView: BEMCheckBox! {
        didSet{
            checkBoxView.boxType = .square
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: FolderViewCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: FolderViewCell.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func createTapped(_ sender:UIButton){
        self.delegate?.createNew()
    }
    
    /*
    func setHeader(section: Int, data: GroupsList){
        
    }*/
    
}

extension FolderViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        contentView.backgroundColor = theme.settings.highlightedBgColor
        threadsCountLabel.textColor = theme.settings.subTitleColor
        iconImageView.tintColor = theme.settings.subTitleColor
        groupImageView.tintColor = theme.settings.subTitleColor
        counterLabel.textColor = theme.settings.whiteColor
    }
}
