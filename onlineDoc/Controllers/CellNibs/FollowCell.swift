//
//  FollowCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 11/01/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

class FollowCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!{
        didSet{
            imgView.layer.borderWidth = 0.5
            imgView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var viewUnreadCount: UIView!
    @IBOutlet weak var lblUnreadCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: FollowCell.applyTheme)

        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: FollowCell.setTheme)

//        let subviews = AppConfig.shared.getSubviewsInView(view: self.contentView)
//        for item in subviews {
//            Themer.shared.register(
//                target: item,
//                action: UITableViewCell.applyTheme)
//        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension FollowCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        lblDetail.textColor = theme.settings.subTitleColor
        lblUnreadCount.textColor = theme.settings.whiteColor
    }
}
