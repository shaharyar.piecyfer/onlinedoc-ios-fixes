//
//  FooterButtonCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/06/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

protocol FooterViewCellDelegate:class {
    func didTapShowMore(status:Bool)
}

class FooterViewCell: UITableViewCell {
    
    @IBOutlet weak var showMoreButton: UIButton!
    weak var delegate: FooterButtonCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func showMore(){
        self.delegate?.didTapShowMore(status: true)
    }
}
