//
//  GeneralViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 10/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox


protocol GeneralViewCellDelegate: class {
    func didTapRow(at index : Int)
}

class GeneralViewCell: UITableViewCell {
    
    @IBOutlet weak var cellBackground: UIView!
    @IBOutlet weak var cellImageView: UIImageView!{
        didSet{
            cellImageView.layer.cornerRadius = cellImageView.frame.size.height / 2
            cellImageView.clipsToBounds =  true
            cellImageView.layer.borderWidth = 0.5
            cellImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    
    @IBOutlet weak var labelTailingToCheckBox: NSLayoutConstraint!
    @IBOutlet weak var labelTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellCheckBox: BEMCheckBox!
    weak var delegate: GeneralViewCellDelegate!
    private var task: URLSessionDataTask?
//    var index = -1
//    var section = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellCheckBox.boxType = .circle
        
        //Generic theme
        Themer.shared.register(
            target: self,
            action: GeneralViewCell.applyTheme)

        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        task?.cancel()
        task = nil
        self.cellImageView.image = #imageLiteral(resourceName: "ic_user")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @objc func rowTap(_ sender:UITapGestureRecognizer){
        if let tag = sender.view?.tag{
            delegate?.didTapRow(at: tag)
        }
    }
    
    func setCellData(indexPath: IndexPath, data: GroupCategoryModel){
        if let title = data.name {
            self.cellLabel.text = title
        }
        self.cellImageView.image = #imageLiteral(resourceName: "folder")
        self.cellCheckBox.on = data.isSelected
        self.cellCheckBox.boxType = .circle
        self.cellCheckBox.inputView?.cornerRadius = 2
        self.cellCheckBox.isUserInteractionEnabled = false
    }
    
    func setUserData(indexPath: IndexPath, data: GroupMember){
        if let firstName = data.firstName, let lastName = data.lastName{
            self.cellLabel.text = firstName + " " +  lastName
        }else{
            self.cellLabel.text = ""
        }
        self.cellImageView.contentMode = .scaleAspectFill
        if let imageUrl = data.profilePicture, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.cellImageView.imageFromServer(imageUrl: url!)
        }else{
            self.cellImageView.image = UIImage(named: "ic_user")
        }
        self.cellCheckBox.on = data.isSelected
        self.contentView.alpha = 1.0
        self.contentView.tag = indexPath.row
        self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rowTap)))
    }
    
    func setupData(indexPath: IndexPath, data: User){
        if let name = data.full_name {
            self.cellLabel.text = name
        }else if let name = data._name {
            self.cellLabel.text = name
        }else{
            self.cellLabel.text = ""
        }
        self.cellImageView.contentMode = .scaleAspectFill
        let imgUrl = data.file?.medium_url ?? data.file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.cellImageView.imageFromServer(imageUrl: url!)
        }else{
            self.cellImageView.image = UIImage(named: "ic_user")
        }
        self.cellCheckBox.on = data.isSelected
        self.contentView.alpha = 1.0
        self.contentView.tag = indexPath.row
        self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rowTap)))
    }
    
    func setupData(indexPath: IndexPath, data: MemberModel){
        if let name = data.full_name {
            self.cellLabel.text = name
        }else{
            self.cellLabel.text = ""
        }
        self.cellImageView.contentMode = .scaleAspectFill
        let imgUrl = data.file?.medium_url ?? data.file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.cellImageView.imageFromServer(imageUrl: url!)
        }else{
            self.cellImageView.image = UIImage(named: "ic_user")
        }
        self.cellCheckBox.on = data.isSelected
        self.contentView.alpha = 1.0
        self.contentView.tag = indexPath.row
        self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rowTap)))
    }
    
    func setCategoriesData(_ indexPath : IndexPath, data : Category, isFromHomeScene : Bool = false){
        self.cellCheckBox.isHidden = true
        
        self.cellLabel.text = nil
        if isFromHomeScene{
            if let catTitle = data.folderName{
                self.labelTrailingConstraint.constant = 8
                if self.labelTailingToCheckBox != nil{
                    self.labelTailingToCheckBox.isActive = false
                }
                self.cellLabel.text = catTitle
            }
        }
        else{
            if let catTitle = data.name{
                self.labelTrailingConstraint.constant = 8
                if self.labelTailingToCheckBox != nil{
                    self.labelTailingToCheckBox.isActive = false
                }
                self.cellLabel.text = catTitle
            }
        }
        
        self.cellImageView.image = #imageLiteral(resourceName: "folder")
        self.cellImageView.contentMode = .center
//        if indexPath.row % 2 == 0{
//            self.cellBackground.backgroundColor = AppColors.evenChoice
//        }else{
//            self.cellBackground.backgroundColor = AppColors.oddChoice
//        }
    }
    
    //for chat
    func configCell(indexPath: IndexPath, data: MemberModel) {
        self.cellLabel.text = data.full_name
        self.cellImageView.contentMode = .scaleAspectFill
        let imgUrl = data.file?.medium_url ?? data.file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            self.cellImageView.image = #imageLiteral(resourceName: "ic_user")
            if task == nil {
                task = cellImageView.downloadImage(from: imageUrl)
            }
        }else{
            self.cellImageView.image = UIImage(named: "ic_user")
        }
        
        self.cellCheckBox.on = data.isSelected
        self.contentView.alpha = 1.0
    }
    
    //for new chat
    func configCell(indexPath: IndexPath, data: ContactModel) {
        self.cellLabel.text = (data.contact_name?.isEmpty ?? false) ? data.mobile_no : (data.contact_name ?? data.mobile_no)
        self.cellImageView.contentMode = .scaleAspectFill
        let imgUrl = data.file?.medium_url ?? data.file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            self.cellImageView.image = #imageLiteral(resourceName: "ic_user")
            if task == nil {
                task = cellImageView.downloadImage(from: imageUrl)
            }
        }else{
            self.cellImageView.image = UIImage(named: "ic_user")
        }
        
        self.cellCheckBox.on = data.isSelected
    }
}
