////
////  GeneralViewCellAllUsers.swift
////  onlineDoc
////
////  Created by Piecyfer on 02/09/2020.
////  Copyright © 2020 onlineDoc. All rights reserved.
////
//
import UIKit
import BEMCheckBox

protocol GeneralViewCellAllUsersDelegate: class {
    func didTapRow(at index : Int )
    func didTapShowProfile(at index: Int, of section: Int)
}

class GeneralViewCellAllUsers: UITableViewCell {

//    MARK: - Outlets
    @IBOutlet weak var cellImage: UIImageView!{
        didSet{
            cellImage.layer.cornerRadius = cellImage.frame.size.height / 2
            cellImage.clipsToBounds =  true
        }
    }
    
    @IBOutlet weak var cellLbl: UILabel!
    @IBOutlet weak var cellCheckBox: BEMCheckBox!
    @IBOutlet weak var cellBackground: UIView!
    @IBOutlet weak var showUserProfileButton: UIButton!
    @IBOutlet weak var cellCheckBoxButton: UIButton!
    //    MARK: - UIActions
    
   
    @IBAction func didTapUserProfileImage(_ sender: UIButton) {
        self.delegate.didTapShowProfile(at: sender.tag, of: 0) // Section will be zero in this case
    }
    
    // This Action is cancelled for now for tapping on checkBox
    @IBAction func didTapCellCheckBoxButton(_ sender: UIButton) {
//        self.delegate.didTapRow(at: sender.tag)
    }
    
//    MARK: - Variables and Instances
    weak var delegate: GeneralViewCellAllUsersDelegate!
//    var index: IndexPath?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellCheckBox.boxType = .circle
        cellCheckBox.isHidden = true
//        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: -8, left: -8, bottom: -8, right: -8))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(indexPath: IndexPath, data: MemberModel) {
        self.cellLbl.text = data.full_name
        self.cellImage.contentMode = .scaleAspectFill
        if let imageUrl = data.file?.src, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.cellImage.imageFromServer(imageUrl: url!)
        }else{
            self.cellImage.image = UIImage(named: "ic_user")
        }
//        self.cellCheckBox.on = data.isChecked
        self.contentView.alpha = 1.0
        self.showUserProfileButton.tag = indexPath.row
        self.cellCheckBoxButton.tag = indexPath.row
    }
}
