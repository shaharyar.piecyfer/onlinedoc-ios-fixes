//
//  GroupViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 18/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

protocol GroupViewCellDelegate: class {
    func didTapCancel(id: Int, index:IndexPath)
    func didTapGroupInfo(id: Int, index:IndexPath)
    func didTapRequestMembership(id: Int, index:IndexPath)
}

class GroupViewCell: UITableViewCell {
    @IBOutlet weak var uiContainer: UIView!
    @IBOutlet weak var groupImageView: UIImageView!
    @IBOutlet weak var groupTitleLabel: UILabel!
    @IBOutlet weak var groupMembersCountLabel: UILabel!
    @IBOutlet weak var groupDescriptionLabel: UILabel!
    @IBOutlet weak var groupRequestMembershipButton: UIButton!
    weak var delegate : GroupViewCellDelegate?
    var index: IndexPath?
    var groupId : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        groupImageView.image = UIImage(named: "groupDefault")
        //Generic theme
        Themer.shared.register(
            target: self,
            action: GroupViewCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: GroupViewCell.setTheme)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func performAction(_ sender: UIButton){
        
        if let senderTitle = sender.currentTitle{
            switch senderTitle {
            case GoToGroup.localized():
                delegate?.didTapGroupInfo(id: self.groupId!, index: self.index!)
                break
            case CancelRequest.localized():
                delegate?.didTapCancel(id: self.groupId!, index: self.index!)
                break
            case RequestMembership.localized():
                delegate?.didTapRequestMembership(id: self.groupId!, index: self.index!)
                break
            default:
                break
            }
        }
    }
    
    func setCell(_ data: GroupModel) {
        let img = data.file?.medium_url ?? data.file?.src
        if let imageUrl = img, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.groupImageView.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "groupDefault"))
            self.groupImageView.contentMode = .scaleAspectFill
        }else{
            self.groupImageView.image = UIImage(named: "groupDefault")
            self.groupImageView.contentMode = .scaleAspectFill
        }
        
        if let membersCount = data.member_count, membersCount == 1 {
            self.groupMembersCountLabel.text = "\(membersCount) Member".localized()
        }else if let membersCount = data.member_count {
            self.groupMembersCountLabel.text = "\(membersCount) Members".localized()
        }
        
        if let title  = data.name{
            self.groupTitleLabel.text = title
        }
        if let description = data.description, description != ""{
            self.groupDescriptionLabel.text =  description
        }else{
            self.groupDescriptionLabel.text  = nil
        }
        
        switch data.request_status {
        case GroupStatus.APPVOVED.rawValue:
            self.groupRequestMembershipButton.setTitle(GoToGroup.localized(), for: .normal)
            break
        case GroupStatus.REQUESTED.rawValue:
            self.groupRequestMembershipButton.setTitle(CancelRequest.localized(), for: .normal)
            break
        default :
            self.groupRequestMembershipButton.setTitle(RequestMembership.localized(), for: .normal)
            break
        }
    }
    
    func setData(_ data: MyGroups) {
        
        if let imageUrl = data.groupImage, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.groupImageView.imageFromServer(imageUrl: url!, placeholder: UIImage())
        }else{
            self.groupImageView.image = UIImage(named: "defaultImage")
        }
        
        if let title  = data.clinicName{
            self.groupTitleLabel.text = title
        }
    }
}

extension GroupViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        uiContainer.backgroundColor = theme.settings.cellColor
        groupRequestMembershipButton.backgroundColor = theme.settings.highlightedBgColor
        groupMembersCountLabel.textColor = theme.settings.subTitleColor
    }
}
