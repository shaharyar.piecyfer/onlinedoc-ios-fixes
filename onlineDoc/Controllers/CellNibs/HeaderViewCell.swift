//
//  HeaderViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 24/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox

class HeaderViewCell: UITableViewCell {
    
    @IBOutlet weak var hideCarrotConstriant:NSLayoutConstraint!
    @IBOutlet weak var showCarrotConstriant:NSLayoutConstraint!
    @IBOutlet weak var hideCheckBoxConstriant:NSLayoutConstraint!
    @IBOutlet weak var showCheckBoxConstriant:NSLayoutConstraint!
    
    @IBOutlet weak var iconImageView:UIImageView!{
        didSet {
            let image = UIImage(named: "carrot")?.withRenderingMode(.alwaysTemplate)
            iconImageView.image = image
        }
    }
    @IBOutlet weak var groupImageView:UIImageView!{
        didSet{
            groupImageView.layer.cornerRadius = groupImageView.bounds.size.height / 2
            groupImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var checkBoxView:BEMCheckBox!{
        didSet{
            checkBoxView.boxType = .square
        }
    }
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: HeaderViewCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: HeaderViewCell.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    /*
    func setHeader(section: Int, data: GroupsList){
        
    }*/
    
}

extension HeaderViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        contentView.backgroundColor = theme.settings.highlightedBgColor
        iconImageView.tintColor = theme.settings.subTitleColor
//        groupImageView.tintColor = theme.settings.subTitleColor
//        counterLabel.textColor = theme.settings.whiteColor
    }
}
