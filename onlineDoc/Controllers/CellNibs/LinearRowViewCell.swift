//
//  LinearRowViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 15/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class LinearRowViewCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellTextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
