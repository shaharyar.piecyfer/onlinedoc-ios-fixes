//
//  LoadMoreCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 27/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

class LoadMoreCell: UITableViewCell {
    
    @IBOutlet weak var btnLoadMore: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    var onClickLoadMore: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnLoadMore.setTitle(loadMoreText.localized(), for: .normal)
        self.loadingIndicator.isHidden = true
        self.loadingIndicator.stopAnimating()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: LoadMoreCell.applyTheme)
        
        Themer.shared.register(
            target: self,
            action: LoadMoreCell.setTheme)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func onClickLoadMore(_ sender: UIButton) {
        self.loadingIndicator.isHidden = false
        self.loadingIndicator.startAnimating()
        self.btnLoadMore.setTitle("", for: .normal)
        onClickLoadMore?()
    }
}

extension LoadMoreCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        btnLoadMore.backgroundColor = theme.settings.whiteColor
        btnLoadMore.tintColor = theme.settings.titleColor
    }
}
