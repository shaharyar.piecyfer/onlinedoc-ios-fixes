//
//  MemberInfoTableViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 14/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
protocol MemberInfoTableViewCellDelegate : class {
    func sendRequest(index: IndexPath)
    func didRequestInvitation(index: IndexPath)
    func didTapShowProfile(index: IndexPath)
    func didTapSendMessage(index: IndexPath)
}

class MemberInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var trailingConstraintWhenHidden: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraintWithInviteButton: NSLayoutConstraint!
    @IBOutlet weak var cellBackgroundView: UIView!{
        didSet{
            cellBackgroundView.layer.cornerRadius = 12
            cellBackgroundView.clipsToBounds = true
        }
    }
    @IBOutlet weak var memberProfileImageView: UIImageView!{
        didSet{
            memberProfileImageView.layer.cornerRadius = memberProfileImageView.bounds.size.height / 2
            memberProfileImageView.clipsToBounds = true
            memberProfileImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
            memberProfileImageView.layer.borderWidth = 0.5
            memberProfileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showMemberProfile)))
        }
    }
    @IBOutlet weak var memberNameLabel: UILabel!{
        didSet{
            memberNameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showMemberProfile)))
        }
    }
    @IBOutlet weak var memberProfessionLabel: UILabel!
    @IBOutlet weak var requestFriendshipButton: UIButton!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var invitationButton: UIButton!
    weak var delegate: MemberInfoTableViewCellDelegate!
    var memberId = 0
    var index: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: MemberInfoTableViewCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: MemberInfoTableViewCell.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func sendMessage(){
        self.delegate?.didTapSendMessage(index: self.index!)
    }
    
    @IBAction func sendRequest(sender:UIButton) {
        self.delegate?.sendRequest(index: self.index!)
    }
    
    @IBAction func groupInvitation(_ sender : UIButton){
        if let txt = sender.currentTitle{
            switch txt {
            case inviteButtonTitle.localized():
                self.delegate?.didRequestInvitation(index: self.index!)
                break
            default:
                break
            }
        }
    }
    
    func setCell(_ data: MemberModel, indexPath: IndexPath, isGroupInfo: Bool = false, isAdmin:Bool = false) {
        self.index = indexPath
        
        let imgUrl = data.file?.medium_url ?? data.file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.memberProfileImageView.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "ic_user"))
        }else{
            self.memberProfileImageView.image = UIImage(named: "ic_user")
        }
        
        if let name  = data.full_name {
            self.memberNameLabel.text = name
        }
        
        if let profession = data.pre_name{
            self.memberProfessionLabel.text = profession.replacingOccurrences(of: "_", with: " ").capitalized.localized()
        }
        
        if let memberId = data.user_id {
            self.memberId = memberId
        }
        
        if let isFriend = data.request_status {
            self.requestFriendshipButton.tag = indexPath.row
            switch isFriend {
            case FriendStatus.ACCEPT.rawValue:
                self.requestFriendshipButton.isHidden = true
                break
            case FriendStatus.REQUEST.rawValue:
                self.requestFriendshipButton.isHidden = false
                self.requestFriendshipButton.isEnabled = true
                self.requestFriendshipButton.setImage(UIImage(named: "addUserInactive"), for: .normal)
                break
            default:
                break
            }
            
            if indexPath.section == 1 && isFriend == FriendStatus.REQUEST.rawValue {
                self.requestFriendshipButton.isHidden = true
            }
        } else {
            self.requestFriendshipButton.isHidden = false
            self.requestFriendshipButton.setImage(UIImage(named: "addUser"), for: .normal)
            self.requestFriendshipButton.isEnabled = true
        }
    }
    
    @objc func showMemberProfile(){
        self.delegate?.didTapShowProfile(index: self.index!)
    }
}

extension MemberInfoTableViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        sendMessageButton.setTitleColor(theme.settings.titleColor, for: .normal)
        invitationButton.setTitleColor(theme.settings.titleColor, for: .normal)
        memberProfessionLabel.textColor = theme.settings.subTitleColor
    }
}
