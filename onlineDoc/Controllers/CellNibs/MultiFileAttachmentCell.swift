//
//  MultiFileAttachmentCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 23/10/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
import UIKit
import AVKit


class MultiFileAttachmentCell: UICollectionViewCell {
    
    @IBOutlet weak var attachedImageView : UIImageView!
    
    var player = AVPlayer()
    var controller = AVPlayerViewController()
    
    func dataToFeed(_ data: ThreadComment, at indexPath: IndexPath){
        //TODO : set cell image or video
        //        self.attachedImageView = nil
        self.attachedImageView.image = nil
        self.attachedImageView.image = UIImage()
        if data.file_ext != nil{
            if data.file_main_type?.lowercased() == "image" {
                //TODO show an Image here
                //                self.controller.view.removeFromSuperview()
                //                self.controller.removeFromParent()
                self.attachedImageView.isHidden = false
                if let imgPath = data.file_path, let imgName = data.file_name, imgPath != "" && imgName != ""{
                    let path = imgPath + imgName
                    if let url = URL(string: path){
                        //                        self.attachedImageView
                        //                        self.attachedImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "groupDefault"))
                        self.attachedImageView.imageFromServer(imageUrl: url, placeholder: UIImage()) { (status, image) in
                            if status{
                                self.attachedImageView.image = image
                            }else{
                                self.attachedImageView.contentMode = .scaleAspectFit
                                self.attachedImageView.image = #imageLiteral(resourceName: "notFound")
                            }
                        }
                    }else{
                        attachedImageView.image = nil
                        self.attachedImageView.isHidden = true
                    }
                }else{
                    attachedImageView.image = nil
                    self.attachedImageView.isHidden = true
                }
            }else if data.file_main_type?.lowercased() == "video"{
                //TODO Handle Video here
                self.attachedImageView.isHidden = true
                if let video = data.file_name, let path = data.file_path,video != "" && path != ""{
                    let videoPath = path + video
                    print("Video Path : \(videoPath)")
                    if let url = URL(string: videoPath){
                        //                        var controller = AVPlayerViewController()
                        player = AVPlayer(url: url)
                        self.controller.player = player
                        //                        self.controller.view.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.width / 2, height: self.contentView.frame.height)
                        self.player.pause()
                        self.controller.view.frame = self.contentView.frame
                        self.contentView.addSubview(self.controller.view)
                    }else{
                        //                        controller.removeFromParent()
                        self.attachedImageView.isHidden = false
                        self.attachedImageView.image = #imageLiteral(resourceName: "attached")
                    }
                }
            }
            
            else if data.file_main_type?.lowercased() == "application"{
                //               controller.removeFromParent()
                self.attachedImageView.image = #imageLiteral(resourceName: "attached")
                self.attachedImageView.contentMode = .scaleAspectFill
                self.attachedImageView.isHidden = false
            }else{
                //                controller.removeFromParent()
                self.attachedImageView.image = nil
                self.attachedImageView.isHidden = true
            }
        }
    }
    
    func messageDataToFeed(_ data: Message, at indexPath: IndexPath){
        //TODO : set cell image or video
        //        self.attachedImageView = nil
        self.attachedImageView.image = nil
        self.attachedImageView.image = UIImage()
        if data.file_ext != nil{
            if data.file_main_type?.lowercased() == "image" {
                //TODO show an Image here
                //                self.controller.view.removeFromSuperview()
                //                self.controller.removeFromParent()
                self.attachedImageView.isHidden = false
                if let imgPath = data.file_path, let imgName = data.file_name, imgPath != "" && imgName != ""{
                    let path = imgPath + imgName
                    if let url = URL(string: path){
                        //                        self.attachedImageView
                        //                        self.attachedImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "groupDefault"))
                        self.attachedImageView.imageFromServer(imageUrl: url, placeholder: UIImage()) { (status, image) in
                            if status{
                                self.attachedImageView.image = image
                            }else{
                                self.attachedImageView.contentMode = .scaleAspectFit
                                self.attachedImageView.image = #imageLiteral(resourceName: "notFound")
                            }
                        }
                    }else{
                        attachedImageView.image = nil
                        self.attachedImageView.isHidden = true
                    }
                }else{
                    attachedImageView.image = nil
                    self.attachedImageView.isHidden = true
                }
            }else if data.file_main_type?.lowercased() == "video"{
                //TODO Handle Video here
                self.attachedImageView.isHidden = true
                if let video = data.file_name, let path = data.file_path,video != "" && path != ""{
                    let videoPath = path + video
                    print("Video Path : \(videoPath)")
                    if let url = URL(string: videoPath){
                        //                        var controller = AVPlayerViewController()
                        player = AVPlayer(url: url)
                        self.controller.player = player
                        //                        self.controller.view.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.width / 2, height: self.contentView.frame.height)
                        self.player.pause()
                        self.controller.view.frame = self.contentView.frame
                        self.contentView.addSubview(self.controller.view)
                    }else{
                        //                        controller.removeFromParent()
                        self.attachedImageView.isHidden = false
                        self.attachedImageView.image = #imageLiteral(resourceName: "attached")
                    }
                }
            }
            else if data.file_main_type?.lowercased() == "application"{
                //               controller.removeFromParent()
                self.attachedImageView.image = #imageLiteral(resourceName: "attached")
                self.attachedImageView.contentMode = .scaleAspectFill
                self.attachedImageView.isHidden = false
            }else{
                //                controller.removeFromParent()
                self.attachedImageView.image = nil
                self.attachedImageView.isHidden = true
            }
        }
    }
    
    func dummyDataToFeed(_ data: Attachment, at indexPath: IndexPath){
        //TODO : set cell image or video
        //        self.attachedImageView = nil
        self.attachedImageView.image = nil
        self.attachedImageView.image = UIImage()
        if data.FileType != nil{
            if data.file_main_type.lowercased() == "image" {
                //                self.controller.view.removeFromSuperview()
                self.attachedImageView.isHidden = false
                if let img = data.image{
                    self.attachedImageView.image = img
                }else{
                    attachedImageView.image = nil
                }
            }else if data.file_main_type.lowercased() == "video"{
                //TODO show an Image here
                //                self.controller.view.removeFromSuperview()
                self.attachedImageView.isHidden = false
                if let img = data.image{
                    self.attachedImageView.image = img
                }else{
                    attachedImageView.image = nil
                }
            }else if data.file_main_type.lowercased() == "application"{
                //                self.controller.removeFromParent()
                self.attachedImageView.image = #imageLiteral(resourceName: "attached")
                self.attachedImageView.contentMode = .scaleAspectFill
            }else{
                //                `self.controller.removeFromParent()
                self.attachedImageView.image = nil
            }
        }
    }
}
