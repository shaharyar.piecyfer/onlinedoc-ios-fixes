//
//  MyActivityCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 15/03/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import AVKit

protocol MyActivityCellDelegate: class {
    func didTapMoreButton(for messageAtIndex:Int)
    func didTapImageView(imageView: UIImageView)
    func didTapVideo(with url: String)
    func didViewFile(with url: String, name: String)
}

class MyActivityCell: UITableViewCell {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var labelContainer: UIView!
    @IBOutlet weak var myMessageTextLabel: UILabel!
    @IBOutlet weak var mediaContentView: UIView!
    @IBOutlet weak var momentLabel: UILabel!
    @IBOutlet weak var messageLabelTextView: UITextView!
    @IBOutlet weak var ivDoubleTick: UIImageView!
    
    @IBOutlet weak var myImageView: UIImageView! {
        didSet {
            myImageView.layer.cornerRadius = myImageView.frame.size.height / 2
            myImageView.clipsToBounds = true
            myImageView.layer.borderWidth = 0.5
            myImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.layer.cornerRadius = 6.0
            containerView.clipsToBounds = true
        }
    }

    @IBOutlet weak var onlineStatus: UIView!{
        didSet{
            onlineStatus.layer.cornerRadius = onlineStatus.frame.size.height / 2
            onlineStatus.clipsToBounds = true
        }
    }
    
    weak var delegate : MyActivityCellDelegate!
    var messageAtIndex = 0
    var urlString = ""
    var urlName = ""
    var isDeleteOnly = false
    var controller = AVPlayerViewController()
    var player = AVPlayer()
    let supportedVideoFormats = ["mp4","mov", "avi","mkv","MOV"]
    let supportedImageFormats = ["jpeg", "gif", "bmp", "tiff", "png","JPG","jpg","HEIC"]
    
    public typealias UserProfileCallBack = (_ index:Int) -> Void
    var callbacks : UserProfileCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        UITextView.appearance().tintColor = .white
        let fixedWidth = messageLabelTextView.frame.size.width
        let newSize = messageLabelTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        messageLabelTextView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        messageLabelTextView.linkTextAttributes = [.foregroundColor: UIColor.systemBlue]
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func moreOptionsTapped(){
        delegate.didTapMoreButton(for: self.messageAtIndex)
    }
    
    /// function use to sert data in user chat section
    /// - Parameters:
    ///   - indexPath: to save the index of the cell to peroform operation later on
    ///   - data: it wiil be chat message retrieved from firbase in chat section
    func setUserMessage(indexPath: IndexPath, data : Message){
        
        self.messageAtIndex = indexPath.row
        self.userNameLabel.isHidden = true
        self.labelContainer.isHidden = true
        if let image = AppConfig.shared.user.file?.src{
            if let  url = URL(string: image){
                self.myImageView.imageFromServer(imageUrl: url)
            }else{
                self.myImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.myImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let message = data.message{
            self.messageLabelTextView.text = message.localized()
            //            self.myMessageTextLabel.text = message
        }else{
            //            self.myMessageTextLabel.text = nil
            self.messageLabelTextView.text = nil
            
        }
        if let  moment = data.created_at{
            //            self.momentLabel.text = moment.UTCToLocal
            
            let d =  AppConfig.shared.convertToDate(from: moment)
            let dateValue = d.getMoment() + AgoText.localized()
            self.momentLabel.text = dateValue
        }
        
        if data.file_ext != nil{
            self.isDeleteOnly = true
            self.setFileContents(with: data)
        }else{
            self.mediaContentView.isHidden = true
        }
        self.ivDoubleTick.image = self.ivDoubleTick.image?.withRenderingMode(.alwaysTemplate)
        if data.isRead {
            self.ivDoubleTick.tintColor = .systemBlue
        } else {
            self.ivDoubleTick.tintColor = .gray
        }
        
    }
    func profile(callback : @escaping UserProfileCallBack) {
        self.callbacks = callback
    }
    @objc func viewProfile(){
        self.callbacks!(self.messageAtIndex)
    }
    
    /// Method to display a comment mdae by the user on particuler thread
    /// - Parameters:
    ///   - data: varibale will get the comment data at particuler instance
    ///   - indexPath: indexPath description to save the index to perform operation later
    func feedUserComment(with data: ThreadComment, at indexPath: IndexPath){
        
        self.onlineStatus.isHidden = true
        if data.comment_type != nil{
            print(data.comment_type)
        }
        if data.multiFiles != nil {
            print(data.multiFiles?.count)
        }
        self.messageAtIndex = indexPath.row - 1
        if let image = AppConfig.shared.user.file?.src {
            if let  url = URL(string: image){
                self.myImageView.imageFromServer(imageUrl: url)
            }else{
                self.myImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.myImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let message = data.comment{
            //            self.myMessageTextLabel.text = message
            self.messageLabelTextView.text = message
        }else{
            //            self.myMessageTextLabel.text = nil
            self.messageLabelTextView.text = nil
        }
        if let  moment = data.added_date{
            self.momentLabel.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
            
            //            let saveDate = AppConfig.shared.convertToDate(from: moment.UTCToLocal)
            //                     self.momentLabel.text = Date.timeAgoSinceDate(saveDate, currentDate: Date(), numericDates: true)
        }
        
        if let userName = AppConfig.shared.user.name?.capitalized  {
            self.userNameLabel.text = userName
        }else{
            self.userNameLabel.text = nil
        }
        
        if data.file_name != nil{
            self.setCommentContents(with: data)
        }else{
            self.mediaContentView.isHidden = true
        }
    }
    
    func setCommentContents(with data: ThreadComment){
        self.controller = AVPlayerViewController()
        if data.file_main_type == "image"{
            let imageView = UIImageView()
            imageView.frame = self.mediaContentView.frame
            self.mediaContentView.isHidden = false
            if let image = data.file_name, let realFileName = data.real_name, let fileExt = data.file_ext, let path = data.file_path,image != "" && path != ""{
                let imagePath = path + image
                //                self.myMessageTextLabel.text = realFileName + "." + fileExt
                if let url = URL(string: imagePath){
                    imageView.imageFromServer(imageUrl: url, placeholder: UIImage())
                    imageView.isUserInteractionEnabled = true
                    imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
                    imageView.backgroundColor = .clear
                    
                    imageView.contentMode = .scaleAspectFit
                    self.mediaContentView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                    imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if data.file_main_type == "video"{
            if let name = data.file_name, let path = data.file_path, name != "" && path != ""{
                let videoPath = path + name
                self.urlString = videoPath
                var fileRealName = ""
                if let title = data.real_name{
                    fileRealName = title + "." + (data.file_ext ?? "")
                }
                self.urlName = fileRealName

                if let url = URL(string: videoPath){
                    let player = AVPlayer(url: url)
                    controller.player = player
                    player.pause()
                    controller.view.frame = self.mediaContentView.frame
                    self.mediaContentView.addSubview(controller.view)
                    self.mediaContentView.isHidden = false
                    controller.view.translatesAutoresizingMaskIntoConstraints = false
                    controller.view.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    controller.view.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    controller.view.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    controller.view.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if data.file_main_type == "application"{
            if let name = data.file_name, let path = data.file_path, let realFileName = data.real_name, let ext = data.file_ext, name != "" && path != ""{
                let filePath = path + name
                //                self.myMessageTextLabel.text = realFileName + "." + ext
                //                self.myMessageTextLabel.isHidden = false
                
                self.messageLabelTextView.text = realFileName + "." + ext
                self.messageLabelTextView.isHidden = false
                
                self.mediaContentView.isHidden = true
                var fileRealName = ""
                if let title = data.real_name{
                    fileRealName = title + "." + (data.file_ext ?? "")
                }
                self.urlName = fileRealName

                self.urlString = filePath
            }
            self.containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewFile(_:))))
            self.mediaContentView.isHidden = false
            let imageView = UIImageView()
            imageView.image = #imageLiteral(resourceName: "attached")
            imageView.contentMode = .scaleAspectFit
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewFile(_:))))
            imageView.backgroundColor = .clear
            imageView.contentMode = .scaleAspectFit
            self.mediaContentView.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
            imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
            imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
            imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
        }else{
            self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
            self.mediaContentView.isHidden = true
        }
    }
    
    
    func setFileContents(with data: Message){
        self.controller = AVPlayerViewController()
        if self.supportedImageFormats.contains(data.file_ext!){
            let imageView = UIImageView()
            imageView.frame = self.mediaContentView.frame
            self.mediaContentView.isHidden = false
            if let image = data.file_name, let realName = data.real_name, let path = data.file_path, let fileext = data.file_ext, image != "" && path != ""{
                //                self.myMessageTextLabel.text = realName + "." + fileext
                let imagePath = path + image
                if let url = URL(string: imagePath){
                    imageView.imageFromServer(imageUrl: url, placeholder: UIImage())
                    imageView.isUserInteractionEnabled = true
                    imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
                    imageView.backgroundColor = .clear
                    imageView.contentMode = .scaleAspectFit
                    self.mediaContentView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                    imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
                    
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if self.supportedVideoFormats.contains(data.file_ext!.lowercased()) {
            //            self.controller.removeFromParent()
            if let name = data.file_name, let realName = data.real_name, let fileExt = data.file_ext, let path = data.file_path, name != "" && path != ""{
                //                self.myMessageTextLabel.text = realName + "." + fileExt
                let videoPath = path + name
                if let url = URL(string: videoPath){
                    player = AVPlayer(url: url)
                    controller.player = player
                    self.mediaContentView.isHidden = false
                    controller.view.frame = self.mediaContentView.frame
                    self.mediaContentView.addSubview(controller.view)
                    player.pause()
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else {
            if let name = data.file_name, let realName = data.real_name, let fileext = data.file_ext, let path = data.file_path, name != "" && path != ""{
                let filePath = path + name
                //                self.myMessageTextLabel.text = realName + "." + fileext
                //                self.myMessageTextLabel.isHidden = false
                self.messageLabelTextView.text = realName + "." + fileext
                self.messageLabelTextView.isHidden = false
                self.mediaContentView.isHidden = true
                self.urlString = filePath
                var fileRealName = ""
                if let title = data.real_name{
                    fileRealName = title + "." + (data.file_ext ?? "")
                }
                self.urlName = fileRealName
            }
            self.containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewFile(_:))))
            self.mediaContentView.isHidden = false
            let imageView = UIImageView()
            imageView.image = #imageLiteral(resourceName: "attached")
            imageView.contentMode = .scaleAspectFit
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewFile(_:))))
            imageView.backgroundColor = .clear
            imageView.contentMode = .scaleAspectFit
            self.mediaContentView.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
            imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
            imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
            imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
        }
        //        else{
        //            self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
        //            self.mediaContentView.isHidden = true
        //        }
    }
    
    
    @objc func imageViewTap(_ sender : UITapGestureRecognizer){
        if let imageView = sender.view as? UIImageView{
            self.delegate?.didTapImageView(imageView: imageView)
        }
    }
    
    @objc func playVideo(from url:String){
        self.delegate?.didTapVideo(with: self.urlString)
    }
    
    @objc func viewFile(_ sender : UITapGestureRecognizer){
        self.delegate?.didViewFile(with: self.urlString,name: self.urlName)
    }
    
}

extension MyActivityCell: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
}
