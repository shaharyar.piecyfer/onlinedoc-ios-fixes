//
//  MyGroupCollectionCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 03/03/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

class MyGroupCollectionCell: UICollectionViewCell {

    @IBOutlet weak var ivGroup: UIImageView!
    @IBOutlet weak var uiUnread: UIView!
    @IBOutlet weak var ivAdmin: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var uiGradient: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ivAdmin.isHidden = true
        self.uiUnread.isHidden = true
        self.ivGroup.image = #imageLiteral(resourceName: "groupDefault")
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = uiGradient.bounds
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.darkGray.cgColor]
        self.uiGradient.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func configCell(group: GroupModel){
        let imgUrl = group.file?.medium_url ?? group.file?.src
        if let imageUrl = imgUrl {
            let url = URL(string: imageUrl)
            ivGroup.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "groupDefault"))
        }else{
            ivGroup.image = #imageLiteral(resourceName: "groupDefault")
        }
        
        if let groupTitle = group.name {
            self.lblTitle.text = groupTitle
        }
        
        self.ivAdmin.isHidden = true
        if let admins = group.clinic_admins, let id = AppConfig.shared.user.id, admins.contains(where: { $0.id == id }) {
            self.ivAdmin.isHidden = false
        }
        if let count = group.group_notification_count {
            self.uiUnread.isHidden = count == 0
        }
        else {
            self.uiUnread.isHidden = true
        }
    }
}
