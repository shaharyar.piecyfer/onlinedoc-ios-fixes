//
//  MyGroupCustomCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 28/02/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit

enum MyGroupCustomCellType {
    case CREATE_GROUP
    case FIND_GROUP
}

class MyGroupCustomCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCell(type: MyGroupCustomCellType){
        switch type {
        case .CREATE_GROUP:
            self.lblTitle.text = "txt_create_new_group".localized().capitalized
            self.imgView.image = UIImage(systemName: "plus")
        case .FIND_GROUP:
            self.lblTitle.text = "txt_find_group".localized().capitalized
            self.imgView.image = UIImage(systemName: "magnifyingglass")
        }
    }
}
