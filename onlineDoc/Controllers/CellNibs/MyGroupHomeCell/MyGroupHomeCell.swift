//
//  MyGroupHomeCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 03/03/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

class MyGroupHomeCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var userGroups = [GroupModel]()
    var onSelect:((GroupModel)->(Void))?
    var onSelectCustomBtn:((MyGroupCustomCellType)->(Void))?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Themer.shared.register(
            target: self,
            action: MyGroupHomeCell.applyTheme)
        
        setupCV()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(){
        
    }
}

extension MyGroupHomeCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? self.userGroups.count : 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MyGroupCollectionCell.self), for: indexPath) as! MyGroupCollectionCell
            cell.configCell(group: self.userGroups[indexPath.item])
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MyGroupCustomCell.self), for: indexPath) as! MyGroupCustomCell
            cell.configCell(type: indexPath.row == 0 ? .CREATE_GROUP : .FIND_GROUP)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.onSelect?(self.userGroups[indexPath.item])
        }
        else {
            self.onSelectCustomBtn?(indexPath.row == 0 ? .CREATE_GROUP : .FIND_GROUP)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 0.0)
        }
        return UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
    }
    
    func setupCV() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsSelection = true
        collectionView.register(UINib(nibName: String(describing: MyGroupCollectionCell.self), bundle:nil), forCellWithReuseIdentifier: String(describing: MyGroupCollectionCell.self))
        collectionView.register(UINib(nibName: String(describing: MyGroupCustomCell.self), bundle:nil), forCellWithReuseIdentifier: String(describing: MyGroupCustomCell.self))
    }
    
}
