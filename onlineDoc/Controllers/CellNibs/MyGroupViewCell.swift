//
//  UserSelectionViewself.swift
//  onlineDoc
//
//  Created by Piecyfer on 15/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox

class MyGroupViewCell: UITableViewCell {
    @IBOutlet weak var mainImageView: UIImageView!{
        didSet{
            mainImageView.layer.cornerRadius = mainImageView.frame.size.height / 2
            mainImageView.clipsToBounds = true
            mainImageView.layer.borderWidth = 0.5
            mainImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subNameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    var index: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: MyGroupViewCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: MyGroupViewCell.setTheme)
    }
    
    func setThreadData(indexPath:IndexPath, data: ThreadModel){
        self.index = indexPath
        let imgUrl = data.clinicImage?.medium_url ?? data.clinicImage?.src
        if let url = imgUrl {
            let imageUrl = URL(string: url)
            self.mainImageView.imageFromServer(imageUrl: imageUrl!)
        }else{
            self.mainImageView.image = UIImage(named: "ic_group")
        }
        
        if let title = data.title?.capitalized, !title.isEmpty {
            self.nameLabel.attributedText = title.attributedText(fontSize: 14, allStyleFont: AppFont.kMuliSemiBold(14).font())?.attributedString
        }else{
            self.nameLabel.attributedText = data.description?.capitalized.attributedText(fontSize: 12, allStyleFont: AppFont.kMuliLight(12).font())?.attributedString
        }
        if let groupTitle = data.clinic_name{
            self.subNameLabel.text = groupTitle
        }else{
            self.subNameLabel.text = ""
        }
        if let category = data.clinic_category_name{
            self.categoryLabel.text = category
        }else{
            self.categoryLabel.text = ""
        }
    }
}

extension MyGroupViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        subNameLabel.textColor = theme.settings.subTitleColor
        categoryLabel.textColor = theme.settings.subTitleColor
    }
}
