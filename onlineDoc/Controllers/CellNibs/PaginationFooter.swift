//
//  PaginationFooter.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 03/07/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

protocol PaginationFooterDelegate: class {
    func didTap(page number: Int, section sect: Int)
}

class PaginationFooter: UITableViewCell {
    var numberOfPagesToShow = 0{
        didSet{
            for i in 1...numberOfPagesToShow{
                let xVal = i != 1 ? (i+1)*25 : 0
                let btn = UIButton(frame: CGRect(x: xVal, y: 7, width: 25 , height: 25))
                btn.cornerRadius = 6
                btn.setTitle("\(i)", for: .normal)
                btn.tag = (i)
                btn.addTarget(self, action: #selector(self.buttonTapped(_:)), for: .touchUpInside)
                if i == self.pageNumber {
                    self.setButtonActive(btn)
                }
                btn.setTitleColor(AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor, for: .normal)
                self.paginationStack.addArrangedSubview(btn)
                self.layoutIfNeeded()
            }
        }
    }
    
    var totalThreads = 0 {
        didSet{
            var pages = totalThreads / blockSize
            let modulus = totalThreads % blockSize
            if modulus > 0 {
                pages += 1
            }
            numberOfPagesToShow = pages
        }
    }
    var arr = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    var pageNumber = 1
    let blockSize = 5
//
    var section = 0

    weak var delegate: PaginationFooterDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: PaginationFooter.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: PaginationFooter.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBOutlet weak var firstPageButton: UIButton!
    @IBOutlet weak var preButton:  UIButton! {
        didSet {
            let image = UIImage(named: "carrotPrevious")?.withRenderingMode(.alwaysTemplate)
            preButton.setImage(image, for: .normal)
        }
    }
    @IBOutlet weak var nxtButton:  UIButton! {
        didSet {
            let image = UIImage(named: "carrotNext")?.withRenderingMode(.alwaysTemplate)
            nxtButton.setImage(image, for: .normal)
        }
    }
    @IBOutlet weak var preDotsButton: UIButton!
    @IBOutlet weak var nxtDotsButton: UIButton!
    @IBOutlet weak var lastPageButton: UIButton!
    @IBOutlet var buttonsCollection: [UIButton]!
    @IBOutlet weak var paginationStack: UIStackView!
    @IBOutlet weak var paginationView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @objc func buttonTapped(_ sender:UIButton){
        self.pageNumber = sender.tag
        self.applyPagination()
        
    }
    
    @IBAction func nextButtonTapped(){
        if self.pageNumber < (self.numberOfPagesToShow - 1){
            self.pageNumber += 1
            self.applyPagination()
        }
    }
    
    @IBAction func previousButtonTapped(){
        if self.pageNumber > 0{
            self.pageNumber -= 1
            self.applyPagination()
        }else{
            self.pageNumber = 0
        }
    }
    
    func applyPagination() {
        self.delegate?.didTap(page: pageNumber, section: self.section)
    }
    
    func setButtonActive(_ button: UIButton) {
        button.backgroundColor = AppConfig.shared.isDarkMode ? AppColors.setActive : AppColors.extraLightGrayColor
        let scrollWidth = scrollView.frame.width
        let scrollHeight = scrollView.frame.height
        let desiredXCoor = button.frame.origin.x - ((scrollWidth / 2) - (button.frame.width / 2))
        
        let rect = CGRect(x: desiredXCoor, y: 0, width: scrollWidth, height: scrollHeight)
        scrollView.scrollRectToVisible(rect, animated: true)
    }
}

extension PaginationFooter {
    fileprivate func setTheme(_ theme: MyTheme) {
        contentView.backgroundColor = theme.settings.highlightedBgColor
        preButton.tintColor = theme.settings.titleColor
        nxtButton.tintColor = theme.settings.titleColor
    }
}
