//
//  ProfileGroupCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 19/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class ProfileGroupCell: UITableViewCell {
    @IBOutlet weak var groupImageView: UIImageView!{
        didSet{
            groupImageView.layer.cornerRadius = groupImageView.bounds.size.height / 2
            groupImageView.clipsToBounds = true
            groupImageView.layer.borderWidth = 0.5
            groupImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var groupTitle: UILabel!
    @IBOutlet weak var viewUnread: UIView!
    @IBOutlet weak var lblUnread: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: ProfileGroupCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: ProfileGroupCell.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension ProfileGroupCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        lblUnread.textColor = theme.settings.whiteColor
        groupTitle.textColor = theme.settings.titleColor
    }
}
