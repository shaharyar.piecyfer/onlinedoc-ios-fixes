//
//  ConnectionViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 22/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import ObjectMapper
import FirebaseDatabase

protocol RequestViewCellDelegate: class {
    func didTapRequestButton(index: Int, isFriendThing: Bool, isCancelRequest: Bool)
    func didTapRequestFriendship(index: Int)
}


class RequestViewCell: UITableViewCell {
    
    public typealias UserProfileCallBack = (_ index:Int) -> Void
    var callbacks : UserProfileCallBack?
    
    @IBOutlet weak var connectionImageView: UIImageView!{
        didSet{
            connectionImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewProfile)))
            self.connectionImageView.layer.cornerRadius = connectionImageView.frame.size.height / 2
            self.connectionImageView.clipsToBounds = true
            self.connectionImageView.contentMode = .scaleAspectFill
            self.connectionImageView.layer.borderWidth = 1.0
            self.connectionImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var iconImageView: UIImageView!{
        didSet{
            iconImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewProfile)))
            self.iconImageView.layer.cornerRadius = iconImageView.frame.size.height / 2
            self.iconImageView.clipsToBounds = true
            self.iconImageView.contentMode = .scaleAspectFit
//            self.iconImageView.layer.borderWidth = 1.0
//            self.iconImageView.layer.borderColor = UIColor.white.cgColor
        }
    }
    @IBOutlet weak var connectionNameLabel: UILabel!
    @IBOutlet weak var connectionLastMessageLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var acceptRequestButton: UIButton!
    @IBOutlet weak var cancelRequestButton: UIButton!
    @IBOutlet weak var sendRequestButton: UIButton!
    
    
    @IBOutlet weak var badgeView:UIView!{
        didSet{
            badgeView.layer.cornerRadius = badgeView.frame.size.height / 2
            badgeView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var countLabel: UILabel!{
        didSet{
            
            countLabel.layer.cornerRadius = countLabel.frame.size.height / 2
            countLabel.clipsToBounds = true
            
        }
    }
    
    var RecordId = 0
    var isFriend = false
    var index = 0
    
    weak var delegate : RequestViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setGroupRequestData(_ indexPath : IndexPath, data : GroupJoinRequest){
        self.contentView.backgroundColor = .clear
        self.backgroundView?.backgroundColor = .clear
        self.iconImageView.image = #imageLiteral(resourceName: "ic_user")
        self.index = indexPath.row
        if let requesterName = data.name{
            self.connectionNameLabel.text = requesterName
        }else{
            self.connectionNameLabel.text = nil
        }
        if let img = data.profilePicture, img != ""{
            if let url = URL(string: img){
                self.connectionImageView.imageFromServer(imageUrl: url)
            }else{
                self.connectionImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.connectionImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        if let groupTitle = data.clinicName{
            self.connectionLastMessageLabel.text = groupTitle
        }else{
            self.connectionLastMessageLabel.text = nil
        }
        self.backView?.layer.cornerRadius = 6.0
        self.backView?.clipsToBounds = true
        self.sendRequestButton.isHidden = true
        self.acceptRequestButton.isHidden = false
        self.cancelRequestButton.isHidden = false
    }
    
    @IBAction func cancelRequestTapped(sender: UIButton){
        delegate?.didTapRequestButton(index: self.index, isFriendThing: self.isFriend, isCancelRequest: true)
    }
    
    @IBAction func acceptRequestTapped(sender: UIButton){
        delegate?.didTapRequestButton(index: self.index, isFriendThing: self.isFriend, isCancelRequest: false)
    }
    
    @IBAction func sendRequestTapped(sender: UIButton){
        delegate?.didTapRequestFriendship(index: self.index)
    }
    
    func profile(callback : @escaping UserProfileCallBack) {
        self.callbacks = callback
    }
    
    @objc func viewProfile(){
        self.callbacks!(index)
    }
}
