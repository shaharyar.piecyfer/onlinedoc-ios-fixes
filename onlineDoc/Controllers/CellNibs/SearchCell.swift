//
//  SearchCell.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 14/07/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

protocol SearchCellDelegate:class {
    func didTapSearch(text: String)
    func didEndEditing()
//    func didChangeText(text: String)
}

class SearchCell: UITableViewCell {

    @IBOutlet weak var searchBar: UISearchBar!
    weak var delegate: SearchCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        searchBar.returnKeyType = UIReturnKeyType.done
        self.searchBar.placeholder = SearchText.localized
        self.searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension SearchCell: UISearchBarDelegate{
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        self.delegate?.didChangeText(text: searchText)
//        self.searchBar.becomeFirstResponder()
//    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let txt = searchBar.text{
            self.delegate?.didTapSearch(text: txt)
        }else{
            self.delegate?.didTapSearch(text: "")
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.delegate?.didEndEditing()
    }
}

