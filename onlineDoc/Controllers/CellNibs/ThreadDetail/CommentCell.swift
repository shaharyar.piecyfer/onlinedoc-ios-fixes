//
//  CommentCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 14/07/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit
import AVKit
import TRMosaicLayout
import Atributika

protocol CommentCellDelegate: AnyObject {
    func didTapMoreOptions(index: IndexPath)
    func previewComment(fileWith url: String, name:String, contentType: String)
}

class CommentCell: UITableViewCell {
    @IBOutlet weak var uiTitle: UIView!
    @IBOutlet weak var uiDescription: UIView!
    @IBOutlet weak var backView: UIStackView! {
        didSet{
            backView.layer.cornerRadius = 5
            backView.clipsToBounds = true
            backView.layer.borderWidth = 0.5
            backView.layer.borderColor = AppColors.borderColor.cgColor
        }
    }
    
    @IBOutlet weak var ivProfile: UIImageView! {
        didSet{
            ivProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewProfile)))
            ivProfile.layer.cornerRadius = ivProfile.layer.frame.height / 2
            ivProfile.clipsToBounds = true
            ivProfile.layer.borderWidth = 0.5
            ivProfile.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    
    @IBOutlet weak var uiOnlineStatus: UIView! {
        didSet{
            uiOnlineStatus.layer.cornerRadius = uiOnlineStatus.layer.frame.height / 2
            uiOnlineStatus.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTimeStamp: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var ivMoreBtn: UIImageView! {
        didSet{
            let image = UIImage(named: "more")?.withRenderingMode(.alwaysTemplate)
            ivMoreBtn.image = image
        }
    }
    @IBOutlet weak var ivDoubleTick: UIImageView!
    
    @IBOutlet weak var iv1: UIImageView! //if one image
    @IBOutlet weak var ui22: UIView! //if two images
    @IBOutlet weak var iv21: UIImageView!
    @IBOutlet weak var iv22: UIImageView!
    
    @IBOutlet weak var ui33: UIView! //if three images
    @IBOutlet weak var iv31: UIImageView!
    @IBOutlet weak var iv32: UIImageView!
    @IBOutlet weak var iv33: UIImageView!
    
    @IBOutlet weak var ui44: UIView! //if three images
    @IBOutlet weak var iv41: UIImageView!
    @IBOutlet weak var iv42: UIImageView!
    @IBOutlet weak var iv43: UIImageView!
    @IBOutlet weak var iv44: UIImageView!
    
    @IBOutlet weak var containerStack: UIStackView!
    @IBOutlet weak var imagesVideosView :UIView!
    @IBOutlet weak var docListStackView: UIStackView!
    
    @IBOutlet weak var filesCountView: UIView!
    @IBOutlet weak var filesCountLabel: UILabel!
    
    var index: IndexPath?
    var onClickProfile: ((_ index: IndexPath) -> Void)?
    
    weak var delegate: CommentCellDelegate?
    private var task: URLSessionDataTask?
    
    private let lbl = AttributedLabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ivProfile.image = #imageLiteral(resourceName: "ic_user")
        self.lblMessage.text = nil
        self.lbl.attributedText = nil
        lbl.onClick = { label, detection in
            switch detection.type {
            case .link(let url):
                var urlString = url.absoluteString
                if urlString.hasPrefix("https://") || urlString.hasPrefix("http://") {
                    UIApplication.shared.open(url)
                }
                else {
                    urlString = "https://\(urlString)"
                    if let _url = URL(string: urlString) {
                        UIApplication.shared.open(_url)
                    }
                }
            default:
                break
            }
        }
        self.lblMessage.addSubview(lbl)
        self.lblMessage.isUserInteractionEnabled = true
        let marginGuide = self.lblMessage.layoutMarginsGuide
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lbl.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        lbl.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        lbl.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        lbl.numberOfLines = 0
        //Generic theme
        Themer.shared.register(
            target: self,
            action: CommentCell.applyTheme)

        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: CommentCell.setTheme)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        task?.cancel()
        task = nil
        self.ivProfile.image = #imageLiteral(resourceName: "ic_user")
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(index: IndexPath, data: ThreadCommentModel, isForMine: Bool = false, isAdmin: Bool = false){
//        self.uiTitle.isHidden = true
//        self.uiDescription.isHidden = true
        self.index = index
        if isForMine || isAdmin {
            self.btnMore.isHidden = false
            self.ivMoreBtn.isHidden = false
        }
        
        var imgUrl = data.comment_creator?.file?.thumb_url ?? data.comment_creator?.file?.medium_url ?? data.comment_creator?.file?.src
        if imgUrl == nil {
            imgUrl = data.comment_creator?.file?.thumb_url ?? data.comment_creator?.comment_user_file?.medium_url ?? data.comment_creator?.comment_user_file?.src
        }
        if let imgPath = imgUrl, imgPath != "" {
            ivProfile.image = #imageLiteral(resourceName: "ic_user")
            if task == nil {
                task = ivProfile.downloadImage(from: imgPath)
            }
//            if let imgUrl = URL(string: imgPath){
//                ivProfile.imageFromServer(imageUrl: imgUrl, placeholder: #imageLiteral(resourceName: "ic_user"))
//            }else{
//                ivProfile.image = #imageLiteral(resourceName: "ic_user")
//            }
        }else {
            ivProfile.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let name = data.comment_creator?.full_name {
            self.lblUserName.text = name
        }else{
            self.lblUserName.text = nil
        }
        
        if let desc = data.comment {
            self.uiDescription.isHidden = false
            self.lbl.attributedText = desc.attributedText(fontSize: 14, allStyleFont: AppFont.kMuliLight(14).font())
//            let desc = desc.htmlToString
//            let attributedString = NSMutableAttributedString.init(string: desc)
//            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColors.blackColor, range: NSRange(location: 0, length: desc.count))
//            self.lblMessage.attributedText = attributedString
////            self.threadDescriptionLabel.text = desc
////            self.threadDescriptionLabel.tintColor = .white
//
//            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//            let matches = detector.matches(in: desc, options: [], range: NSRange(location: 0, length: desc.utf16.count))
//
//            for match in matches {
//                guard let range = Range(match.range, in: desc) else { continue }
//                let url = desc[range]
//
//                let _range = (desc as NSString).range(of: String(url))
//                let mutableAttributedString = NSMutableAttributedString.init(string: desc)
//                mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.systemBlue, range: _range)
//                self.lblMessage.attributedText = mutableAttributedString
//
//                lblMessage.isUserInteractionEnabled = true
//                lblMessage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
//            }
        }else{
            self.lblMessage.text = nil
            self.lbl.attributedText = nil
            self.uiDescription.isHidden = true
        }
        
        if let timeStamp  = data.created_at {
            self.lblTimeStamp.text = (AppConfig.shared.convertToDate(from: timeStamp).getMoment() + AgoText.localized())
        }else{
            self.lblTimeStamp.text = ""
        }
        
        for subview in imagesVideosView.subviews{
            if subview is UIStackView{
                subview.removeFromSuperview()
            }
        }
        if self.docListStackView != nil{
            self.docListStackView.subviews.forEach{ $0.removeFromSuperview()}
        }
        
        if let files = data.files {
            let imagesVideos = files.filter({$0.content_type!.contains("image") || $0.content_type!.contains("video")})
            self.setupImagesVideos(data: imagesVideos)
            
            let otherFiles = files.filter({!$0.content_type!.contains("image") && !$0.content_type!.contains("video")})
            self.loadFilesList(files: otherFiles)
        }
        else {
            self.imagesVideosView.isHidden = true
        }
    }
    
    //MARK:- for chat messgaes
    func configCell(index: IndexPath, data: ChatModel, isForMine: Bool = false, receiver: User?) {
        self.index = index
        self.uiTitle.isHidden = false
        self.uiDescription.isHidden = true
        var profileImage = ""
        if isForMine {
            self.backView.layer.borderWidth = 0
            self.btnMore.isHidden = false
            self.ivMoreBtn.isHidden = false
            self.ivDoubleTick.isHidden = false
            self.ivDoubleTick.image = self.ivDoubleTick.image?.withRenderingMode(.alwaysTemplate)
            if data.receiver_is_read ?? true {
                self.ivDoubleTick.tintColor = .systemBlue
            } else {
                self.ivDoubleTick.tintColor = AppColors.lightGrayColor
            }
            profileImage = AppConfig.shared.user.file?.medium_url ?? AppConfig.shared.user.file?.src ?? ""
        }
        else {
            self.btnMore.isHidden = true
            self.ivMoreBtn.isHidden = true
            profileImage = receiver?.file?.medium_url ?? receiver?.file?.src ?? ""
        }
        self.lblUserName.isHidden = true
        
        if profileImage != "" {
            if let imgUrl = URL(string: profileImage) {
                ivProfile.imageFromServer(imageUrl: imgUrl, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                ivProfile.image = #imageLiteral(resourceName: "ic_user")
            }
        }else {
            ivProfile.image = #imageLiteral(resourceName: "ic_user")
        }
        
//        if let name = data.comment_creator?.full_name {
//            self.lblUserName.text = name
//        }else{
//            self.lblUserName.text = nil
//        }
        
        if let desc = data.message {
            self.uiDescription.isHidden = false
            self.lbl.attributedText = desc.attributedText(fontSize: 14, allStyleFont: AppFont.kMuliLight(14).font())
//            let attributedString = NSMutableAttributedString.init(string: desc)
//            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColors.blackColor, range: NSRange(location: 0, length: desc.count))
//            self.lblMessage.attributedText = attributedString
////            self.threadDescriptionLabel.text = desc
////            self.threadDescriptionLabel.tintColor = .white
//
//            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//            let matches = detector.matches(in: desc, options: [], range: NSRange(location: 0, length: desc.utf16.count))
//
//            for match in matches {
//                guard let range = Range(match.range, in: desc) else { continue }
//                let url = desc[range]
//
//                let _range = (desc as NSString).range(of: String(url))
//                let mutableAttributedString = NSMutableAttributedString.init(string: desc)
//                mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.systemBlue, range: _range)
//                self.lblMessage.attributedText = mutableAttributedString
//
//                lblMessage.isUserInteractionEnabled = true
//                lblMessage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
//            }
        }else{
            self.lblMessage.text = nil
            self.lbl.attributedText = nil
            self.uiDescription.isHidden = true
        }
        
        if let timeStamp  = data.created_at {
            self.lblTimeStamp.text = (AppConfig.shared.convertToDate(from: timeStamp).getMoment() + AgoText.localized())
        }else{
            self.lblTimeStamp.text = ""
        }
        
        for subview in imagesVideosView.subviews{
            if subview is UIStackView{
                subview.removeFromSuperview()
            }
        }
        if self.docListStackView != nil{
            self.docListStackView.subviews.forEach{ $0.removeFromSuperview()}
        }
        
        if let files = data.files {
            self.uiTitle.isHidden = true
            let imagesVideos = files.filter({$0.content_type!.contains("image") || $0.content_type!.contains("video")})
            self.setupImagesVideos(data: imagesVideos)
            
            let otherFiles = files.filter({!$0.content_type!.contains("image") && !$0.content_type!.contains("video")})
            self.loadFilesList(files: otherFiles)
        }
        else {
            self.imagesVideosView.isHidden = true
        }
    }
    
    private func setupImagesVideos(data: [FileModel]) {
        if !data.isEmpty {
            //files.filter({$0.content_type!.contains("image") || $0.content_type!.contains("video")})
            self.imagesVideosView.isHidden = false
            if data.count == 1 {
                let isVideo = data.first?.content_type?.contains("video") ?? false
                if isVideo {
                    self.iv1.isHidden = false
                    self.iv1.image = UIImage(named: "ic_video")
                    self.iv1.contentMode = .center
                    self.iv1.backgroundColor = .black
//                    if let path = data.first?.image, let url = URL(string: path) {
//                        let view = UIView()
//                        view.frame = iv1!.frame
//                        let controller = AVPlayerViewController()
//                        let player = AVPlayer(url: url)
//                        controller.player = player
//                        player.rate = 0
//                        player.pause()
//                        controller.view.translatesAutoresizingMaskIntoConstraints = false
//                        controller.view.heightAnchor.constraint(equalToConstant: iv1!.frame.size.height).isActive = true
//                        controller.view.widthAnchor.constraint(equalToConstant: iv1!.frame.size.width).isActive = true
//                        view.clipsToBounds = true
//                        view.addSubview(controller.view)
//                        self.iv1.addSubview(view)
//                    }
                }
                else {
                    self.iv1.isHidden = false
                    if let img = data.first?.src, img != "" {
                        if let url = URL(string: img){
                            self.iv1.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "defaultImage"))
                        }else{
                            self.iv1.image = #imageLiteral(resourceName: "defaultImage")
                        }
                    }else{
                        self.iv1.image = #imageLiteral(resourceName: "defaultImage")
                    }
                }
                
                self.ui22.isHidden = true
                self.ui33.isHidden = true
                self.ui44.isHidden = true
            }
            else if data.count == 2 {
                self.ui22.isHidden = false
                for (index, _img) in data.enumerated() {
                    let image = index == 0 ? self.iv21 : self.iv22
                    let isVideo = _img.content_type?.contains("video") ?? false
                    if isVideo {
                        image?.image = UIImage(named: "ic_video")
                        image?.contentMode = .center
                        image?.backgroundColor = .black
//                        if let path = _img.image, let url = URL(string: path) {
//                            let view = UIView()
//                            view.frame = image!.frame
//                            let controller = AVPlayerViewController()
//                            let player = AVPlayer(url: url)
//                            controller.player = player
//                            player.rate = 0
//                            player.pause()
//                            controller.view.translatesAutoresizingMaskIntoConstraints = false
//                            controller.view.heightAnchor.constraint(equalToConstant: image!.frame.size.height).isActive = true
//                            controller.view.widthAnchor.constraint(equalToConstant: image!.frame.size.width).isActive = true
//                            view.clipsToBounds = true
//                            view.addSubview(controller.view)
//                            image!.addSubview(view)
//                        }
                    }
                    else {
                        if let img = _img.src, img != "" {
                            if let url = URL(string: img){
                                image?.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "defaultImage"))
                            }else{
                                image?.image = #imageLiteral(resourceName: "defaultImage")
                            }
                        }else{
                            image?.image = #imageLiteral(resourceName: "defaultImage")
                        }
                    }
                }
                self.iv1.isHidden = true
                self.ui33.isHidden = true
                self.ui44.isHidden = true
            }
            else if data.count == 3 {
                self.ui33.isHidden = false
                for (index, _img) in data.enumerated() {
                    let image = index == 0 ? self.iv31 : (index == 1 ? self.iv32 : self.iv33)
                    let isVideo = _img.content_type?.contains("video") ?? false
                    if isVideo {
                        image?.image = UIImage(named: "ic_video")
                        image?.contentMode = .center
                        image?.backgroundColor = .black
//                        if let path = _img.image, let url = URL(string: path) {
//                            let view = UIView()
//                            view.frame = image!.frame
//                            let controller = AVPlayerViewController()
//                            let player = AVPlayer(url: url)
//                            controller.player = player
//                            player.rate = 0
//                            player.pause()
//                            controller.view.translatesAutoresizingMaskIntoConstraints = false
//                            controller.view.heightAnchor.constraint(equalToConstant: image!.frame.size.height).isActive = true
//                            controller.view.widthAnchor.constraint(equalToConstant: image!.frame.size.width).isActive = true
//                            view.clipsToBounds = true
//                            view.addSubview(controller.view)
//                            image!.addSubview(view)
//                        }
                    }
                    else {
                        if let img = _img.src, img != "" {
                            if let url = URL(string: img){
                                image?.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "defaultImage"))
                            }else{
                                image?.image = #imageLiteral(resourceName: "defaultImage")
                            }
                        }else{
                            image?.image = #imageLiteral(resourceName: "defaultImage")
                        }
                    }
                }
                self.iv1.isHidden = true
                self.ui22.isHidden = true
                self.ui44.isHidden = true
            }
            else {
                self.ui44.isHidden = false
                for (index, _img) in data.enumerated() {
                    let image = index == 0 ? self.iv41 : (index == 1 ? self.iv42 : (index == 2 ? self.iv43 : self.iv44))
                    let isVideo = _img.content_type?.contains("video") ?? false
                    if isVideo {
                        image?.image = UIImage(named: "ic_video")
                        image?.contentMode = .center
                        image?.backgroundColor = .black
//                        if let path = _img.image, let url = URL(string: path) {
//                            let view = UIView()
//                            view.frame = image!.frame
//                            let controller = AVPlayerViewController()
//                            let player = AVPlayer(url: url)
//                            controller.player = player
//                            player.rate = 0
//                            player.pause()
//                            controller.view.translatesAutoresizingMaskIntoConstraints = false
//                            controller.view.heightAnchor.constraint(equalToConstant: image!.frame.size.height).isActive = true
//                            controller.view.widthAnchor.constraint(equalToConstant: image!.frame.size.width).isActive = true
//                            view.clipsToBounds = true
//                            view.addSubview(controller.view)
//                            image!.addSubview(view)
//                        }
                    }
                    else {
                        if let img = _img.src, img != "" {
                            if let url = URL(string: img){
                                image?.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "defaultImage"))
                            }else{
                                image?.image = #imageLiteral(resourceName: "defaultImage")
                            }
                        }else{
                            image?.image = #imageLiteral(resourceName: "defaultImage")
                        }
                    }
                }
                self.iv1.isHidden = true
                self.ui22.isHidden = true
                self.ui33.isHidden = true
                if data.count > 4 {
                    self.filesCountView.isHidden = false
                    self.filesCountLabel.text = "+\(data.count - 4)"
                }
                else {
                    self.filesCountView.isHidden = true
                    self.filesCountLabel.text = ""
                }
            }
        }
        else {
            self.imagesVideosView.isHidden = true
        }
    }
    
    private func loadFilesList(files: [FileModel]) {
        for file in files{
            if let fileView  = Bundle.main.loadNibNamed("DocumentListView", owner: self, options: nil)?.first as? DocumentListView{
                fileView.setTheme()
                if let title = file.name {
                    fileView.documentTitle.text = title
                }
                if file.content_type?.contains("pdf") ?? false {
                    fileView.iconImageView.image = UIImage(named: "pdfIcon")
                }
                else if file.content_type?.contains("word") ?? false {
                    fileView.iconImageView.image = UIImage(named: "ic_word")
                }
                else if (file.content_type?.contains("sheet") ?? false) || (file.content_type?.contains("excel") ?? false) {
                    fileView.iconImageView.image = UIImage(named: "ic_excel")
                }
                else if file.content_type?.contains("text") ?? false {
                    fileView.iconImageView.image = UIImage(named: "ic_text")
                }
                else {
                    fileView.iconImageView.image = #imageLiteral(resourceName: "attached")
                }
//                fileView.tapButton.isEnabled = false
                if let img = file.src {
                    fileView.fileUrl = img
                    fileView.file { (url) in
                        self.delegate?.previewComment(fileWith: url, name: file.name ?? "", contentType: file.content_type!)
                    }
                }
                self.docListStackView.addArrangedSubview(fileView)
                self.docListStackView.isHidden = false
            }
        }
    }
    
    @objc func viewProfile(){
        self.onClickProfile?(self.index!)
    }
    
    @IBAction func moreOptions(){
        self.delegate?.didTapMoreOptions(index: self.index!)
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = lblMessage.attributedText?.string else {
            return
        }
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count))

        for match in matches {
            guard let range = Range(match.range, in: text) else { continue }
            let url = text[range]
            
            if let url = URL(string: String(url)) {
                UIApplication.shared.open(url)
            }
        }
    }
}

extension CommentCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        contentView.backgroundColor = .clear
        lblTimeStamp.textColor = theme.settings.subTitleColor
        ivMoreBtn.tintColor = theme.settings.subTitleColor
        filesCountLabel.textColor = theme.settings.whiteColor
    }
}
