//
//  ThreadCommentCell.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 03/04/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import AVKit

protocol ThreadCommentCellDelegate:class {
    func didTapImageView(View: UIImageView)
    func didTapPlay(video:String, at index:Int)
    func didTapDeleteComment(at index: Int)
    func didTapViewFile(file: String, name:String)
}


class ThreadCommentCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewProfile)))
            profileImageView.layer.cornerRadius = profileImageView.layer.frame.height / 2
            profileImageView.clipsToBounds = true
            profileImageView.layer.borderWidth = 0.5
            profileImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var userOnlineStatus: UIView!{
        didSet{
            userOnlineStatus.layer.cornerRadius = userOnlineStatus.layer.frame.height / 2
            userOnlineStatus.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var backView:UIView!{
        didSet{
            backView.layer.cornerRadius = 10
            backView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var threadCommentLabel: UILabel!
    @IBOutlet weak var mediaContentView: UIView!
    @IBOutlet weak var moreButton:UIButton!
    @IBOutlet weak var threadCommentTextView: UITextView!
    var urlString = ""
    var urlName = ""
    var index = -1
    var controller = AVPlayerViewController()
    let supportedVideoFormats = ["mp4","mov", "avi","mkv","MOV"]
    let supportedImageFormats = ["jpeg", "gif", "bmp", "tiff", "png","JPG","jpg","HEIC"]
    
    public typealias UserProfileCallBack = (_ index:Int) -> Void
    var callbacks : UserProfileCallBack?
    weak var delegate: ThreadCommentCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        threadCommentTextView.linkTextAttributes = [.foregroundColor: UIColor.systemBlue]
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func feedComment(at indexPath:IndexPath, with data: ThreadComment)  {
        if data.comment_type != nil{
            print(data.comment_type)
        }
        if data.multiFiles != nil {
            print(data.multiFiles?.count)
        }
        //        self.messageAtindex = indexPath.row
        self.index = indexPath.row - 1
        if let commentByImage = data.commentBy?.profilePicture, commentByImage != ""{
            if let url = URL(string: commentByImage){
                self.profileImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let commnetByName = data.commentBy?.name.capitalized{
            self.userNameLabel.text = commnetByName
//            self.userNameLabel.text = "   " + commnetByName
        }else{
            self.userNameLabel.text = nil
        }
        
        self.userOnlineStatus.isHidden = true
        /*
       if let status = data.onlineStatus?.status{
            switch status {
            case "online":
                self.userOnlineStatus.backgroundColor = .green
            case "away":
                self.userOnlineStatus.backgroundColor = .orange
            default:
                self.userOnlineStatus.backgroundColor = .gray
            }
        }else{
            self.userOnlineStatus.backgroundColor = .gray
        }
        */
        if let moment = data.added_date{
            self.timeStampLabel.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
        }else{
            self.timeStampLabel.text = nil
        }
        
        if let comment = data.comment{
//            self.threadCommentLabel.text = comment
            self.threadCommentTextView.text = comment

        }else{
//            self.threadCommentLabel.text =  nil
            self.threadCommentTextView.text = nil
        }
        
        if data.file_main_type != nil{
            self.mediaContentView.isHidden = false
            self.setCommentContents(with: data)
        }else{
            self.mediaContentView.isHidden = true
        }
    }
    
    func setCommentContents(with data: ThreadComment){
        self.controller = AVPlayerViewController()
        if data.file_main_type == "image"{
            let imageView = UIImageView()
            imageView.frame = self.mediaContentView.frame
            self.mediaContentView.isHidden = false
            if let image = data.file_name, let path = data.file_path,image != "" && path != ""{
                let imagePath = path + image
//                self.threadCommentLabel.text = image
                if let url = URL(string: imagePath){
                    imageView.imageFromServer(imageUrl: url, placeholder: UIImage())
                    imageView.isUserInteractionEnabled = true
                    imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
                    imageView.backgroundColor = .clear
                    imageView.contentMode = .scaleAspectFit
                    self.mediaContentView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                    imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
                    
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if data.file_main_type == "video"{
            if let name = data.file_name, let path = data.file_path, name != "" && path != ""{
                let videoPath = path + name
//                self.urlString = ApiConfig.imageStagingUrl
                var fileRealName = ""
                if let title = data.real_name{
                    fileRealName = title + "." + (data.file_ext ?? "")
                }
                self.urlName = fileRealName
                if let url = URL(string: videoPath){
                    let player = AVPlayer(url: url)
                    controller.player = player
                    player.pause()
                    controller.view.frame = self.mediaContentView.frame
                    self.mediaContentView.addSubview(controller.view)
                    self.mediaContentView.isUserInteractionEnabled = true
                    self.mediaContentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.playVideo)))
//                    player.play()
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if data.file_main_type == "application"{
            if let name = data.file_name, let path = data.file_path, name != "" && path != ""{
                let filePath = path + name
//                self.threadCommentLabel.text = name
//                self.threadCommentLabel.isHidden = false
                self.threadCommentTextView.isHidden = false
                self.mediaContentView.isHidden = true
                var fileRealName = ""
                if let title = data.real_name{
                    fileRealName = title + "." + (data.file_ext ?? "")
                }
                self.urlName = fileRealName
                self.urlString = filePath
            }
            
            self.contentView.isUserInteractionEnabled = true
            self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showFile)))
            self.mediaContentView.isHidden = false
            let imageView = UIImageView()
            imageView.image = #imageLiteral(resourceName: "attached")
            imageView.contentMode = .scaleAspectFit
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showFile)))
            imageView.backgroundColor = .clear
            imageView.contentMode = .scaleAspectFit
            self.mediaContentView.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
            imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
            imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
            imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
        }else{
            self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
            self.mediaContentView.isHidden = true
        }
    }
    
    func setMessage(data:Message) {
        
        self.userNameLabel.isHidden = true
        
        if let commentByImage = data.commentBy?.profilePicture, commentByImage != ""{

            if let url = URL(string: commentByImage){
                self.profileImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let moment = data.created_at{
            self.timeStampLabel.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
//            let saveDate = AppConfig.shared.convertToDate(from: moment.UTCToLocal)
//            self.timeStampLabel.text = Date.timeAgoSinceDate(saveDate, currentDate: Date(), numericDates: true)
        }else{
            self.timeStampLabel.text = nil
        }
        
        
        if let msg = data.message{
//            self.threadCommentLabel.text = msg
            self.threadCommentTextView.text = msg.localized()
//            self.threadCommentLabel.text = "    " + msg
        }else{
//            self.threadCommentLabel.text =  nil
            self.threadCommentTextView.text =  nil
        }
        
        if data.file_name != nil{
            self.setMessageContents(with: data)
        }else{
            self.mediaContentView.isHidden = true
        }
    }
    
    func profile(callback : @escaping UserProfileCallBack) {
        self.callbacks = callback
    }
    @objc func viewProfile(){
        self.callbacks!(self.index)
    }
    
    
    func setMessageContents(with data: Message){
        self.controller = AVPlayerViewController()
        if self.supportedImageFormats.contains(data.file_ext!){
            let imageView = UIImageView()
            imageView.frame = self.mediaContentView.frame
            self.mediaContentView.isHidden = false
            if let image = data.file_name, let realName = data.real_name, let fileExt = data.file_ext, let path = data.file_path,image != "" && path != ""{
                let imagePath = path + image
//                self.threadCommentLabel.text = realName + "." + fileExt
                if let url = URL(string: imagePath){
                    imageView.imageFromServer(imageUrl: url, placeholder: UIImage())
                    imageView.isUserInteractionEnabled = true
                    imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
                    imageView.contentMode = .scaleAspectFit
                    self.mediaContentView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                    imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
                    
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if self.supportedVideoFormats.contains(data.file_ext!) {
            if let name = data.file_name, let path = data.file_path, name != "" && path != ""{
                let videoPath = path + name
                if let url = URL(string: videoPath){
                    let player = AVPlayer(url: url)
                    controller.player = player
                    controller.view.frame = self.mediaContentView.frame
                    self.mediaContentView.addSubview(controller.view)
                    self.mediaContentView.isHidden = false
                    player.pause()
                    controller.view.translatesAutoresizingMaskIntoConstraints = false
                    controller.view.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    controller.view.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    controller.view.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    controller.view.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                    controller.view.heightAnchor.constraint(lessThanOrEqualTo: self.mediaContentView.heightAnchor).isActive = true
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else{
            if let name = data.file_name, let realName = data.real_name, let fileExt = data.file_ext, let path = data.file_path, name != "" && path != ""{
                let filePath = path + name
//                self.threadCommentLabel.text = realName + "." + fileExt
                self.threadCommentTextView.text = realName + "." + fileExt
                self.mediaContentView.isHidden = true
//                self.threadCommentLabel.isHidden = false
                self.threadCommentTextView.isHidden = false
                self.urlString = filePath
                var fileRealName = ""
                if let title = data.real_name{
                    fileRealName = title + "." + (data.file_ext ?? "")
                }
                self.urlName = fileRealName
            }
            
            self.contentView.isUserInteractionEnabled = true
            self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showFile)))
            self.mediaContentView.isHidden = false
            let imageView = UIImageView()
            imageView.image = #imageLiteral(resourceName: "attached")
            imageView.contentMode = .scaleAspectFit
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showFile)))
            imageView.backgroundColor = .clear
            self.mediaContentView.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
            imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
            imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
            imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
        }
    }
    
    @objc func imageViewTap(_ sender : UITapGestureRecognizer){
        if let imageView = sender.view as? UIImageView{
            self.delegate?.didTapImageView(View: imageView)
        }
    }
    
    @objc func playVideo(){
        self.delegate?.didTapPlay(video: self.urlString, at: self.index)
    }
    
    @objc func showFile(){
        self.delegate?.didTapViewFile(file: self.urlString, name: self.urlName)
    }
    
    @IBAction func showMore(){
        self.delegate?.didTapDeleteComment(at: self.index)
    }
}
