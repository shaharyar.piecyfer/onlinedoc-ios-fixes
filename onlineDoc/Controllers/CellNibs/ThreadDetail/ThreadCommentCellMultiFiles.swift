//
//  ThreadCommentCellMultiFiles.swift
//  onlineDoc
//
//  Created by Zeeshan Ahmad Butt on 16/11/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import AVKit
import TRMosaicLayout

protocol ThreadCommentCellMultiFilesDelegate:class {
    func didTapImageViewMultiple(View: UIImageView)
    func didTapPlayMultiple(video:String, at index:Int)
    func didTapDeleteCommentMultiple(at index: Int)
    func didTapEditCommentMultiple(at index: Int)
    func didTapViewFileMultiple(file: String, name:String)
}


class ThreadCommentCellMultiFiles: UITableViewCell {
    
    var viewType = "" // mine, other
    
    @IBOutlet weak var backView:UIView! {
        didSet{
            backView.layer.cornerRadius = 10
            backView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet{
            profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewProfile)))
            profileImageView.layer.cornerRadius = profileImageView.layer.frame.height / 2
            profileImageView.clipsToBounds = true
            profileImageView.layer.borderWidth = 0.5
            profileImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    
    @IBOutlet weak var userOnlineStatus: UIView! {
        didSet{
            userOnlineStatus.layer.cornerRadius = userOnlineStatus.layer.frame.height / 2
            userOnlineStatus.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var moreButton:UIButton!
    
    @IBOutlet weak var mediaContentView: UIView!
        
    var urlString = ""
    var urlName = ""
    var index = -1
    var controller = AVPlayerViewController()
    let supportedVideoFormats = ["mp4","mov", "avi","mkv","MOV"]
    let supportedImageFormats = ["jpeg", "gif", "bmp", "tiff", "png","JPG","jpg","HEIC"]
    
    public typealias UserProfileCallBack = (_ index:Int) -> Void
    var callbacks : UserProfileCallBack?
    weak var delegate: ThreadCommentCellMultiFilesDelegate?
    
    // Zeeshan Added outlets and properties
    @IBOutlet weak var containerStack: UIStackView!
    @IBOutlet weak var attchementView :UIView!
    @IBOutlet weak var docListStackView:UIStackView!
    @IBOutlet weak var filesCollectionView: UICollectionView!
    @IBOutlet weak var filesCountView :UIView!
    @IBOutlet weak var filesCountLabel :UILabel!
    
    var commentAttachments = [ThreadComment]()
    var attachedFiles = [ThreadComment](){
        didSet{
            if attachedFiles.count > 3{
                //Show view and count
                let remainingFiles = attachedFiles.count - 3
                self.filesCountView.isHidden = false
                self.filesCountLabel.text = "+\(remainingFiles)"
            }else{
                //Hide view and count
                self.filesCountView.isHidden = true
                self.filesCountLabel.text = ""
            }
        }
    }
    var attachedList = [ThreadComment]()
    var player = AVPlayer()
    var controller1 = AVPlayerViewController()
    var player1 = AVPlayer()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        filesCollectionView.register(UINib(nibName: "MultiFileAttachment", bundle: nil), forCellWithReuseIdentifier: "MultiFileAttachmentCell")
        let mosaicLayout = TRMosaicLayout()
        self.filesCollectionView?.collectionViewLayout = mosaicLayout
        mosaicLayout.delegate = self
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func feedCommentMultiple(at indexPath:Int, with comments: ThreadComment, of type: String) {
        self.viewType = type
        self.index = indexPath
        let data = (comments.multiFiles?.first)!
        
        if type == "mine"{
//            if let image = AppConfig.shared.user.profilePicture{
//                if let  url = URL(string: image){
//                    self.profileImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
//                }else{
//                    self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
//                }
//            }else{
//                self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
//            }
            
            if let userName = AppConfig.shared.user.name?.capitalized  {
                self.userNameLabel.text = userName
            }else{
                self.userNameLabel.text = nil
            }
        }else{
            if let commentByImage = data.commentBy?.profilePicture, commentByImage != ""{
                if let url = URL(string: commentByImage){
                    self.profileImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
                }else{
                    self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
                }
            }else{
                self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
            }
            
            if let commnetByName = data.commentBy?.name.capitalized{
                self.userNameLabel.text = commnetByName
            }else{
                self.userNameLabel.text = nil
            }
        }
        
        self.userOnlineStatus.isHidden = true
        /*
        if let status = data.onlineStatus?.status{
            switch status {
            case "online":
                self.userOnlineStatus.backgroundColor = .green
            case "away":
                self.userOnlineStatus.backgroundColor = .orange
            default:
                self.userOnlineStatus.backgroundColor = .gray
            }
        }else{
            self.userOnlineStatus.backgroundColor = .gray
        }
        */
        
        if let moment = data.added_date{
            self.timeStampLabel.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
        }else{
            self.timeStampLabel.text = nil
        }
        
        // Now implement multifiles images/vdeo/pdf/etc function
        self.commentAttachments.removeAll()
        self.attachedFiles.removeAll()
        self.attachedList.removeAll()
        for subview in self.attchementView.subviews{
            if subview is UIStackView{
                subview.removeFromSuperview()
            }
        }
        
        if self.docListStackView != nil{
            self.docListStackView.subviews.forEach{ $0.removeFromSuperview()}
        }
        
        self.filesCollectionView.reloadData()
        
        // populate the data
        self.commentAttachments = comments.multiFiles!
        let imgVidFiles = self.commentAttachments.filter{$0.file_main_type != "application"}
        
        if imgVidFiles.count == 0 {
            print("Do nothing because we don't have images or videos")
            self.filesCollectionView.isHidden = true
        }else if imgVidFiles.count > 2 {
            self.filesCollectionView.isHidden = false
            self.attachedFiles = imgVidFiles
            self.filesCollectionView.reloadData()
        }else{
            self.filesCollectionView.isHidden = true
            self.attachedFiles = imgVidFiles
            let stackView = UIStackView()
            stackView.alignment = .fill
            stackView.axis = .horizontal
            stackView.distribution = .fillEqually
            stackView.frame = self.attchementView.frame
            self.layoutAttachments(_stackView: stackView, data: imgVidFiles)
        }
        
        if imgVidFiles.count > 0 {
            self.attchementView.isHidden = false
        }else{
            self.attchementView.isHidden = true
        }
        
        let docFiles = self.commentAttachments.filter{$0.file_main_type == "application"}
        if docFiles.count > 0 {
            self.containerStack.spacing = 8
            self.docListStackView.isHidden = false
            self.attachedList = docFiles
            self.loadFilesList(docFiles)
        }else{
            self.docListStackView.isHidden = true
        }
        self.layoutIfNeeded()
    }
    
    func layoutAttachments(_stackView: UIStackView, data:[ThreadComment]){
        //TODO Handle the atachments here without using moasic layout for 1 or 2 files
        for file in data{
            if file.file_main_type == "image"{
                let imageView = UIImageView()
                if let image = file.file_name, let path = file.file_path,image != "" && path != ""{
                    let imagePath = path + image.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    if let url = URL(string: imagePath){
                        imageView.imageFromServer(imageUrl: url, placeholder: UIImage())
                        imageView.isUserInteractionEnabled = true
                        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
                        imageView.contentMode = .scaleAspectFill
                        imageView.clipsToBounds = true
                        _stackView.addArrangedSubview(imageView)
                        imageView.translatesAutoresizingMaskIntoConstraints = false
                        imageView.heightAnchor.constraint(equalToConstant: self.attchementView.frame.size.height).isActive = true
                    }
                }
            }else if file.file_main_type == "video"{
                if let name = file.file_name, let path = file.file_path, name != "" && path != ""{
                    let videoPath = path + name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    if let url = URL(string: videoPath){
                        if data.count > 1 && data[0].file_main_type == "video" &&  data[1].file_main_type == "video" && _stackView.subviews.count == 1{
                            player1 = AVPlayer(url: url)
                            controller1.player = player1
                            player1.rate = 0
                            self.player1.pause()
                            _stackView.addArrangedSubview(controller1.view)
                        }else{
                            player = AVPlayer(url: url)
                            controller.player = player
                            player.rate = 0
                            self.player.pause()
                            _stackView.addArrangedSubview(controller.view)
                        }
                    }
                }else{
                    let imageView = UIImageView()
                    imageView.image = #imageLiteral(resourceName: "attached")
                    imageView.contentMode = .scaleAspectFit
                    imageView.isUserInteractionEnabled = true
                    imageView.contentMode = .scaleToFill
                    _stackView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                }
            }else{
                let imageView = UIImageView()
                imageView.frame = _stackView.frame
                imageView.image = #imageLiteral(resourceName: "attached")
                imageView.contentMode = .scaleAspectFit
                imageView.isUserInteractionEnabled = true
                _stackView.addSubview(imageView)
                imageView.translatesAutoresizingMaskIntoConstraints = false
                imageView.trailingAnchor.constraint(equalTo: _stackView.trailingAnchor).isActive = true
                imageView.bottomAnchor.constraint(equalTo: _stackView.bottomAnchor).isActive = true
                imageView.leadingAnchor.constraint(equalTo: _stackView.leadingAnchor).isActive = true
                imageView.topAnchor.constraint(equalTo: _stackView.topAnchor).isActive = true
            }
        }
        self.attchementView.addSubview(_stackView)
        _stackView.translatesAutoresizingMaskIntoConstraints = false
        _stackView.trailingAnchor.constraint(equalTo: self.attchementView.trailingAnchor).isActive = true
        _stackView.bottomAnchor.constraint(equalTo: self.attchementView.bottomAnchor).isActive = true
        _stackView.leadingAnchor.constraint(equalTo: self.attchementView.leadingAnchor).isActive = true
        _stackView.topAnchor.constraint(equalTo: self.attchementView.topAnchor).isActive = true
        _stackView.heightAnchor.constraint(equalToConstant: self.attchementView.frame.size.height).isActive = true
    }
    
    func loadFilesList(_ files:[ThreadComment]){
        for file in files{
            if let fileView  = Bundle.main.loadNibNamed("DocumentListView", owner: self, options: nil)?.first as? DocumentListView{
                
                var fileRealName = ""
                
                if let title = file.real_name, let ext = file.file_ext{
                    fileView.documentTitle.text = title + "." + ext
                    fileRealName = title + "." + ext
                }
                
                if let path = file.file_path, let name = file.file_name{
                    let url = path + name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                    fileView.fileUrl = url
                }
                
                fileView.file { (url) in
                    self.delegate?.didTapViewFileMultiple(file: url, name: fileRealName)
                }
                
                if let ext = file.file_ext{
                    switch ext {
                    case "pdf":
                        fileView.iconImageView.image = #imageLiteral(resourceName: "pdfIcon")
                        break
                    default:
                        fileView.iconImageView.image = #imageLiteral(resourceName: "attached")
                    }
                }
                fileView.tapButton.isEnabled = true
                self.docListStackView.addArrangedSubview(fileView)
            }
        }
    }
}

extension ThreadCommentCellMultiFiles : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.attachedFiles.count > 0 {
            return self.attachedFiles.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultiFileAttachmentCell", for: indexPath) as? MultiFileAttachmentCell else {
            print("Unable to read a collection view ThreadFilesCell")
            return UICollectionViewCell()
        }
        //TODO populate Cell here
        cell.controller.view.removeFromSuperview()
        cell.attachedImageView.image = UIImage()
        cell.dataToFeed(attachedFiles[indexPath.item], at: indexPath)
        return cell
    }
}

extension ThreadCommentCellMultiFiles : TRMosaicLayoutDelegate{
    
    func collectionView(_ collectionView: UICollectionView, mosaicCellSizeTypeAtIndexPath indexPath: IndexPath) -> TRMosaicCellType {
        if attachedFiles.count > 1{
            return indexPath.item % 3 == 0 ? .big : .small
        }else{
            return .big
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: TRMosaicLayout, insetAtSection: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func heightForSmallMosaicCell() -> CGFloat {
        if attachedFiles.count > 1{
            return 110
        }else{
            return 150
        }
    }
}

extension ThreadCommentCellMultiFiles {
    func setMessage(data:Message) {
        
        self.userNameLabel.isHidden = true
        
        if let commentByImage = data.commentBy?.profilePicture, commentByImage != ""{

            if let url = URL(string: commentByImage){
                self.profileImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let moment = data.created_at{
            self.timeStampLabel.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
//            let saveDate = AppConfig.shared.convertToDate(from: moment.UTCToLocal)
//            self.timeStampLabel.text = Date.timeAgoSinceDate(saveDate, currentDate: Date(), numericDates: true)
        }else{
            self.timeStampLabel.text = nil
        }
        
        
        if let msg = data.message{
            print("have single chat message")
        }else{
            print("Don't have single chat message")
        }
        
        if data.file_name != nil{
            self.setMessageContents(with: data)
        }else{
            self.mediaContentView.isHidden = true
        }
    }
    
    func setMessageContents(with data: Message){
        self.controller = AVPlayerViewController()
        if self.supportedImageFormats.contains(data.file_ext!){
            let imageView = UIImageView()
            imageView.frame = self.mediaContentView.frame
            self.mediaContentView.isHidden = false
            if let image = data.file_name, let realName = data.real_name, let fileExt = data.file_ext, let path = data.file_path,image != "" && path != ""{
                let imagePath = path + image
                if let url = URL(string: imagePath){
                    imageView.imageFromServer(imageUrl: url, placeholder: UIImage())
                    imageView.isUserInteractionEnabled = true
                    imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
                    imageView.contentMode = .scaleAspectFit
                    self.mediaContentView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                    imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
                    
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if self.supportedVideoFormats.contains(data.file_ext!) {
            if let name = data.file_name, let path = data.file_path, name != "" && path != ""{
                let videoPath = path + name
                if let url = URL(string: videoPath){
                    let player = AVPlayer(url: url)
                    controller.player = player
                    controller.view.frame = self.mediaContentView.frame
                    self.mediaContentView.addSubview(controller.view)
                    self.mediaContentView.isHidden = false
                    player.pause()
                    controller.view.translatesAutoresizingMaskIntoConstraints = false
                    controller.view.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    controller.view.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    controller.view.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    controller.view.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                    controller.view.heightAnchor.constraint(lessThanOrEqualTo: self.mediaContentView.heightAnchor).isActive = true
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else{
            if let name = data.file_name, let realName = data.real_name, let fileExt = data.file_ext, let path = data.file_path, name != "" && path != ""{
                let filePath = path + name
                self.mediaContentView.isHidden = true
                self.urlString = filePath
                var fileRealName = ""
                if let title = data.real_name{
                    fileRealName = title + "." + (data.file_ext ?? "")
                }
                self.urlName = fileRealName
            }
            
            self.contentView.isUserInteractionEnabled = true
            self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showFile)))
            self.mediaContentView.isHidden = false
            let imageView = UIImageView()
            imageView.image = #imageLiteral(resourceName: "attached")
            imageView.contentMode = .scaleAspectFit
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showFile)))
            imageView.backgroundColor = .clear
            self.mediaContentView.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
            imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
            imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
            imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
        }
    }
}

extension ThreadCommentCellMultiFiles {
    
    func profile(callback : @escaping UserProfileCallBack) {
        self.callbacks = callback
    }
    
    @objc func viewProfile(){
        self.callbacks!(self.index)
    }
    
    @objc func imageViewTap(_ sender : UITapGestureRecognizer){
        if let imageView = sender.view as? UIImageView{
            self.delegate?.didTapImageViewMultiple(View: imageView)
        }
    }
    
    @IBAction func showMore(_ sender:UIButton){
        self.delegate?.didTapDeleteCommentMultiple(at: self.index)
    }
    
    @IBAction func showMoreMine(_ sender:UIButton){
        self.delegate?.didTapEditCommentMultiple(at: self.index)
    }
    
    @objc func playVideo(){
        self.delegate?.didTapPlayMultiple(video: self.urlString, at: self.index)
    }
    
    @objc func showFile(){
        self.delegate?.didTapViewFileMultiple(file: self.urlString, name: self.urlName)
    }
}
