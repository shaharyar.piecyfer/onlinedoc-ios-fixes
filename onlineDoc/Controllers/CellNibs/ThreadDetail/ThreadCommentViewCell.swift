//
//  ThreadCommentViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 20/02/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class ThreadCommentViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileCommentLabel: UILabel!
    @IBOutlet weak var profilemomentLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
