//
//  ThreadCommentCell.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 03/04/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import AVKit

protocol UserActivityCellDelegate:class {
    func didTapImageView(View: UIImageView)
}


class UserActivityCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.layer.cornerRadius = profileImageView.layer.frame.height / 2
            profileImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var userOnlineStatus: UIView!{
        didSet{
            userOnlineStatus.layer.cornerRadius = userOnlineStatus.layer.frame.height / 2
            userOnlineStatus.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var backView:UIView!{
        didSet{
            backView.layer.cornerRadius = 10
            backView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var threadCommentLabel: UILabel!
    @IBOutlet weak var mediaContentView: UIView!
    
    var controller = AVPlayerViewController()
    let supportedVideoFormats = ["mp4","mov", "avi","mkv","MOV"]
    let supportedImageFormats = ["jpeg", "gif", "bmp", "tiff", "png","JPG","jpg","HEIC"]
    weak var delegate: ThreadCommentCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func feedComment(at indexPath:IndexPath, with data: ThreadComment)  {
        //        self.messageAtindex = indexPath.row
        if let commentByImage = data.commentBy?.profilePicture, commentByImage != ""{
            if let url = URL(string: commentByImage){
                self.profileImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let commnetByName = data.commentBy?.name.capitalized{
            self.userNameLabel.text = commnetByName
        }else{
            self.userNameLabel.text = nil
        }
        
        if let moment = data.added_date{
            self.timeStampLabel.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
        }else{
            self.timeStampLabel.text = nil
        }
        
        if let comment = data.comment?.capitalized{
            self.threadCommentLabel.text = comment
        }else{
            self.threadCommentLabel.text =  nil
        }
        
        if data.file_main_type != nil{
            self.mediaContentView.isHidden = false
            self.setCommentContents(with: data)
        }else{
            self.mediaContentView.isHidden = true
        }
    }
    
    func setCommentContents(with data: ThreadComment){
        self.controller = AVPlayerViewController()
        if data.file_main_type == "image"{
            let imageView = UIImageView()
            imageView.frame = self.mediaContentView.frame
            self.mediaContentView.isHidden = false
            if let image = data.file_name, let path = data.file_path,image != "" && path != ""{
                let imagePath = path + image
                if let url = URL(string: imagePath){
                    imageView.imageFromServer(imageUrl: url, placeholder: UIImage())
                    imageView.isUserInteractionEnabled = true
                    imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
                    imageView.contentMode = .scaleToFill
                    self.mediaContentView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    //                    imageView.centerYAnchor.constraint(equalTo: self.mediaContentView.centerYAnchor)
                    //                    imageView.centerXAnchor.constraint(equalTo: self.mediaContentView.centerXAnchor)
                    imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                    imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
                    
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if data.file_main_type == "video"{
            //TODO Show image from Server
            if let name = data.file_name, let path = data.file_path, name != "" && path != ""{
                let videoPath = path + name
                if let url = URL(string: videoPath){
                    let player = AVPlayer(url: url)
                    controller.player = player
                    controller.view.frame = self.mediaContentView.frame
                    self.mediaContentView.addSubview(controller.view)
                    player.play()
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if data.file_main_type == "application"{
            let imageView = UIImageView()
            imageView.image = #imageLiteral(resourceName: "attached")
            imageView.contentMode = .scaleAspectFit
        }else{
            self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
            self.mediaContentView.isHidden = true
        }
    }
    
    func setMessage(data:Message) {
        
        self.userNameLabel.isHidden = true
        
        if let moment = data.created_at{
            self.timeStampLabel.text = AppConfig.shared.convertToDate(from: moment).getMoment() + AgoText.localized()
        }else{
            self.timeStampLabel.text = nil
        }
        
        if let comment = data.message?.capitalized{
            self.threadCommentLabel.text = comment
        }else{
            self.threadCommentLabel.text =  nil
        }
        
        if data.file_name != nil{
            self.setMessageContents(with: data)
        }else{
            self.mediaContentView.isHidden = true
        }
    }
    
    func setMessageContents(with data: Message){
        self.controller = AVPlayerViewController()
        if self.supportedImageFormats.contains(data.file_ext!){
            let imageView = UIImageView()
            imageView.frame = self.mediaContentView.frame
            self.mediaContentView.isHidden = false
            if let image = data.file_name, let path = data.file_path,image != "" && path != ""{
                let imagePath = path + image
                if let url = URL(string: imagePath){
                    imageView.imageFromServer(imageUrl: url, placeholder: UIImage())
                    imageView.isUserInteractionEnabled = true
                    imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
                    imageView.contentMode = .scaleToFill
                    self.mediaContentView.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
                    imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
                    imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
                    imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
                    imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
                    
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if self.supportedVideoFormats.contains(data.file_ext!) {
            if let name = data.file_name, let path = data.file_path, name != "" && path != ""{
                let videoPath = path + name
                if let url = URL(string: videoPath){
                    let player = AVPlayer(url: url)
                    controller.player = player
                    controller.view.frame = self.mediaContentView.frame
                    self.mediaContentView.addSubview(controller.view)
                    player.play()
                }else{
                    self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                    self.mediaContentView.isHidden = true
                }
            }else{
                self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
                self.mediaContentView.isHidden = true
            }
        }else if data.file_ext == "application"{
            let imageView = UIImageView()
            imageView.image = #imageLiteral(resourceName: "attached")
            imageView.contentMode = .scaleAspectFit
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTap(_:))))
            imageView.contentMode = .scaleToFill
            self.mediaContentView.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.trailingAnchor.constraint(equalTo: self.mediaContentView.trailingAnchor).isActive = true
            imageView.bottomAnchor.constraint(equalTo: self.mediaContentView.bottomAnchor).isActive = true
            imageView.leadingAnchor.constraint(equalTo: self.mediaContentView.leadingAnchor).isActive = true
            imageView.topAnchor.constraint(equalTo: self.mediaContentView.topAnchor).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: self.mediaContentView.frame.size.height).isActive = true
        }else{
            self.mediaContentView.subviews.forEach{ $0.removeFromSuperview()}
            self.mediaContentView.isHidden = true
        }
    }
    
    
    
    
    @objc func imageViewTap(_ sender : UITapGestureRecognizer){
        if let imageView = sender.view as? UIImageView{
            self.delegate?.didTapImageView(View: imageView)
        }
    }
}
