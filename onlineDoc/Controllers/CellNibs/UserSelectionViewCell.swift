//
//  UserSelectionViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 15/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox

class UserSelectionViewCell: UITableViewCell {
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userSelectionView: BEMCheckBox!
}
