//
//  UserTypeViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 25/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class UserTypeViewCell: UITableViewCell {
    @IBOutlet weak var professionImageView: UIImageView!
    @IBOutlet weak var prefessionTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: UserTypeViewCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: UserTypeViewCell.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            contentView.backgroundColor = AppConfig.shared.isDarkMode ? AppColors.choice : AppColors.mediumGrayColor
        } else {
            contentView.backgroundColor = AppConfig.shared.isDarkMode ? AppColors.rowInactive : AppColors.extraLightGrayColor
        }
    }
}

extension UserTypeViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        prefessionTitleLabel.textColor = theme.settings.titleColor
        professionImageView.tintColor = theme.settings.titleColor
    }
}
