//
//  WebLinksTableViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 25/09/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class WebLinksTableViewCell: UITableViewCell {
    
    @IBOutlet weak var linksTextView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        linksTextView.linkTextAttributes = [.foregroundColor: UIColor.systemBlue]
        linksTextView.centerVertically()
        
        //Generic theme
        Themer.shared.register(
            target: self,
            action: WebLinksTableViewCell.applyTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
