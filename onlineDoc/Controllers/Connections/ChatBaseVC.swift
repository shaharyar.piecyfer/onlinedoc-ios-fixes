//
//  ChatBaseVC.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 01/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class ChatBaseVC: BaseVC {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let socketManager = SocketManager.sharedInstance
    
    //MARK: - IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar:UISearchBar!
    
    var searchedText = ""
    var pageNumber = 1
    var loadMore = true
    
    var chatRooms = [ChatRoomModel]()
    var selectedChat = ChatRoomModel()
    
    @objc private func refreshData(_ sender: Any) {
        refreshControl.beginRefreshing()
        self.pageNumber = 1
        self.loadMore = true
        self.showChatRooms(self.searchedText)
        self.apiCallForChatCount()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.layOutSearchbar()
        self.showChatRooms()
        AppConfig.shared.lastVisitedTabIndex = 2

        self.setupTheme()
        self.setupNotificationObservers()
        self.setSocket()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: ChatBaseVC.applyTheme)

        Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    private func setSocket() {
        if socketManager.client?.isConnected == false {
            print("ChatBase -> connectToSocket")
            socketManager.connectToSocket()
//            socketManager.delegate = self
        }
    }
    
    private func setupNotificationObservers() {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifySocket, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            if let notify = notification.userInfo?["Notify"] as? NotifySocket {
                switch notify {
                case .appearance:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .APPEARANCE {
                        if let data = notification.userInfo?["Data"] as? SocketAppearanceModel {
                            if let user = data.data?.user {
                                if let index = self.chatRooms.firstIndex(where: { $0.user?.id == user.id }) {
                                    self.chatRooms[index].user?.online_status = user.online_status
                                    DispatchQueue.main.async {
                                        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                                    }
                                }
                            }
                        }
                    }
                    
                case .chatRoomNotification:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let data = notification.userInfo?["Data"] as? ChatRoomModel {
                            if let index = self.chatRooms.firstIndex(where: { $0.chat?.chat_room_id == data.chat?.chat_room_id }) {
//                                if (self.chatRooms[index].chat?.message_count ?? 0) == 0 {
//                                    self.apiCallForChatCount()
//                                }
                                self.chatRooms[index] = data
                                DispatchQueue.main.async {
                                    self.sortChatRooms()
                                }
                            }
                            else {
                                self.chatRooms.insert(data, at: 0)
//                                self.apiCallForChatCount()
                                DispatchQueue.main.async {
                                    self.sortChatRooms()
                                }
                            }
                        }
                    }
                    
                case .readFromOtherDevice:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let data = notification.userInfo?["Data"] as? SocketReadFromOtherDeviceModel {
                            if let index = self.chatRooms.firstIndex(where: { $0.chat?.chat_room_id == data.chat_room_id }) {
                                var room = self.chatRooms[index]
                                room.chat?.message_count = 0
                                self.chatRooms[index] = room
                                DispatchQueue.main.async {
                                    self.sortChatRooms()
                                }
                            }
                            self.apiCallForChatCount()
                        }
                    }
                    
                default:
                    break
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
        setUpRightButton()
        if appDelegate.attachements.count > 0 {
            self.tapRightBarButton()
        }
    }
    
    deinit {
        print("ChatBaseVC->deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setUpRightButton() {
        let rightButton = UIBarButtonItem(image: UIImage(systemName: "plus"),
                                          style: .plain,
                                          target: self,
                                          action: #selector(self.tapRightBarButton))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    func layOutSearchbar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = SearchText.localized()
    }
    
    @objc func tapRightBarButton() {
        performSegue(withIdentifier: String(describing: NewChatVC.self), sender: nil)
    }

    private func showChatRooms(_ search: String = "") {
        if self.pageNumber == 1 {
            self.chatRooms.removeAll()
        }
        RequestManager.shared.getChatRooms(page: pageNumber, searchText: search) { (status, response) in
            self.refreshControl.endRefreshing()
            if status{
                if let result = response as? ChatRoomResult, let data = result.result {
                    if data.count < pageLimit50 {
                        self.loadMore = false
                    }
                    let chatRooms = data.filter({$0.chat != nil}).filter({$0.user != nil})
                    self.chatRooms.append(contentsOf: chatRooms)
                    self.sortChatRooms()
//                    self.tableView.reloadSections([1], with: .automatic)
                }
            }
        }
    }
    
    func sortChatRooms() {
        var unreadChatList = chatRooms.filter({($0.chat?.message_count ?? 0) > 0})
        unreadChatList.sort{ ($0.last_message_created_at ?? "") > ($1.last_message_created_at ?? "") }
        var otherChatList = chatRooms.filter({($0.chat?.message_count ?? 0) == 0})
        otherChatList.sort{ ($0.last_message_created_at ?? "") > ($1.last_message_created_at ?? "") }
        
        self.chatRooms = unreadChatList + otherChatList
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ChatVC, segue.identifier == "SegueToChat" {
            vc.chatId = self.selectedChat.chat?.chat_room_id
            vc.onDismiss = { [weak self] in
                guard let `self` = self else {return}
//                self.refreshData(UIButton())
                self.apiCallForChatCount()
                if let index = self.chatRooms.firstIndex(where: { $0.chat?.chat_room_id == self.selectedChat.chat?.chat_room_id }) {
                    self.chatRooms[index].chat?.message_count = 0
                    DispatchQueue.main.async {
                        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    }
                }
                self.selectedChat = ChatRoomModel()
            }
            vc.onDeleteChat = { [weak self] chatId in
                guard let `self` = self else {return}
                if let index = self.chatRooms.firstIndex(where: { $0.chat?.chat_room_id == chatId }) {
                    self.chatRooms.remove(at: index)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                self.selectedChat = ChatRoomModel()
            }
        }
        else if segue.identifier == "ChatPickRecipientsVC" {
            if let vc = segue.destination as? ChatPickRecipientsVC {
                vc.didStartNewChat = { [weak self] in
                    guard let `self` = self else {return}
                    self.refreshData(UIButton())
                }
            }
        }
        else if segue.identifier == "NewChatVC" {
            if let vc = segue.destination as? NewChatVC {
                vc.onDeleteChat = { [weak self] chatId in
                    guard let `self` = self else {return}
                    if let index = self.chatRooms.firstIndex(where: { $0.chat?.chat_room_id == chatId }) {
                        self.chatRooms.remove(at: index)
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                    self.selectedChat = ChatRoomModel()
                }
            }
        }
    }
    
    func gotoUserProfile(_ index: IndexPath, isChat: Bool = true) {
        let userId = self.chatRooms[index.row].chat?.user_id
        if let id = userId, id != AppConfig.shared.user.id {
            self.previewUser(id: id)
        }
    }
    
    private func deleteChat(at indexPath: IndexPath) {
        if let chatId = self.chatRooms[indexPath.row].chat?.chat_room_id {
            RequestManager.shared.deleteChatRoom(id: chatId) { (status, response) in
                if status{
                    if let _ = response as? GeneralSuccessResult {
                        self.chatRooms.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        self.apiCallForChatCount()
//                        self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    }
                }
            }
        }
    }
}

//MARK : - EXTENSIONS
extension ChatBaseVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchedText = ""
            self.searchBar.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.pageNumber = 1
            self.loadMore = true
            self.showChatRooms()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.pageNumber = 1
        self.loadMore = true
        self.searchedText = searchBar.text ?? ""
        self.showChatRooms(searchedText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchBar.resignFirstResponder()
    }
}

extension ChatBaseVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatRooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ConnectionViewCell.self), for: indexPath) as? ConnectionViewCell else {
            return self.emptyTblCell()
        }
        guard indexPath.row < self.chatRooms.count else { return self.emptyTblCell() }
        cell.index = indexPath
        cell.configCell(indexPath, data: self.chatRooms[indexPath.row])
        cell.action {  [weak self] (index, isProfile, requestType) in
            guard let strongSelf = self else { return }
            if isProfile{
                strongSelf.gotoUserProfile(index)
            }
        }
        if self.loadMore && indexPath.row == (self.chatRooms.count - 1) {
            self.pageNumber += 1
            self.showChatRooms(self.searchedText)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedChat = self.chatRooms[indexPath.row]
        if (self.selectedChat.chat?.message_count ?? 0) > 0 {
            self.chatRooms[indexPath.row].chat?.message_count = 0
            self.sortChatRooms()
//                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        self.performSegue(withIdentifier: "SegueToChat", sender: self)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: DeleteTitle.localized()) { _, _, _ in
            self.showAlert(To: "", for: deleteChatMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
                guard let `self` = self else {return}
                if status {
                    self.deleteChat(at: indexPath)
                }
            }
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: String(describing: ConnectionViewCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: ConnectionViewCell.self))
        
        self.tableView.separatorStyle = .none
        
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }
}
