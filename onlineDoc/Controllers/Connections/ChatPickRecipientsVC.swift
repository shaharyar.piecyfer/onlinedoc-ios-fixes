//
//  PickRecipientsViewController.swift
//  onlineDoc
//
//  Created by Piecyfer on 31/08/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class ChatPickRecipientsVC: BaseVC {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    // MARK: - Variables and Instances
    var searchText = ""
    var searchPage = 0
//    var isSearch = false
//    var usersAll = [Member]()
//    var selectedMember = Member()
    var personName = ""
    var selectedIndex = -1
    
    // MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var friends = [MemberModel]()
    var otherUsers = [MemberModel]()
    var pageNumber = 1
    var loadMore = true
    var searchedText = ""
    
    var didStartNewChat: (()->())?
    
//    lazy var refreshControl: UIRefreshControl = {
//        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action:
//            #selector(self.refreshUserList(_:)),
//                                 for: .valueChanged)
//        refreshControl.backgroundColor = .clear
//        refreshControl.tintColor = .white
//        return refreshControl
//    }()
    
    @objc private func refreshData(_ sender: Any) {
        self.pageNumber = 1
        self.searchBar.text = ""
        self.loadMore = true
        self.loadUsersAndFriendList(searchText: self.searchedText)
        self.searchBar.resignFirstResponder()
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.loadUsersAndFriendList()
        self.layOutSearchbar()
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationItem.title = "Pick a Recipient".localized()
        
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: ChatPickRecipientsVC.applyTheme)

        Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if appDelegate.attachements.count > 0 {
            appDelegate.attachements.removeAll()
        }
    }
    
    func layOutSearchbar(){
        self.searchBar.placeholder = SearchText.localized
        self.searchBar.delegate = self
//        self.searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
//        if let searchTextField = self.searchBar.textField{
//            searchTextField.textColor = .white
//            searchTextField.keyboardAppearance = .dark
//        }
    }
    
    // MARK: - Selector Methods
    @objc func oKButtonTapped(){
//        didTapSendMessage(toUser: selectedIndex)
    }
    
    // MARK: - Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    //MARK: - APIs
    func loadUsersAndFriendList(searchText: String = ""){
        if self.pageNumber == 1 {
            self.friends.removeAll()
            self.otherUsers.removeAll()
        }
        self.tableView.setEmptyMessage("")
        RequestManager.shared.getfriendsWithOtherUsers(page: self.pageNumber, searchText: searchText) { (status, response) in
            if status {
                if let res = response as? FriendListResponse {
                    if res.friends.isEmpty && res.other_users.isEmpty && self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                    }
                    if self.pageNumber == 1 {
                        if !searchText.isEmpty {
                            self.friends = res.friends.filter {
                                guard $0.phone != nil else {
                                    return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email)!.lowercased().contains(searchText.lowercased())
                                }
                                return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email)!.lowercased().contains(searchText.lowercased()) || ($0.phone)!.lowercased().contains(searchText.lowercased())
                                
                            }
                        } else {
                            self.friends = res.friends
                        }
                    }
                    self.otherUsers.append(contentsOf: res.other_users)
                    if res.other_users.count == 0 {
                        self.loadMore = false
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func navigateToChat(user: MemberModel){
        if let vc = getVC(storyboard: .CONNECTIOS, vcIdentifier: "ChatScene") as? ChatVC {
            if let roomId = user.chatRoomId {
                vc.chatId = roomId
            }
            else {
                var receiver = User()
                receiver.id = user.user_id
                receiver.full_name = user.full_name
                vc.receiver = receiver
            }
            if appDelegate.attachements.count > 0 {
                vc.attachements = appDelegate.attachements
            }
            vc.onDismiss = { [weak self] in
                guard let `self` = self else {return}
                self.didStartNewChat?()
                self.dismissVC()
            }
            self.navigateVC(vc)
        }
    }
}

//MARK: - EXTENSIONS
extension ChatPickRecipientsVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.resignFirstResponder()
            self.searchBar.textField?.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.pageNumber = 1
            self.loadMore = true
            self.searchedText = ""
            self.loadUsersAndFriendList()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
        let txt = searchBar.text?.trim ?? ""
        self.searchedText = txt
        self.loadUsersAndFriendList(searchText: txt)
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
        self.searchedText = ""
    }
}

extension ChatPickRecipientsVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.friends.count
        case 1:
            return self.searchedText.isEmpty ? 0 : self.otherUsers.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else { return self.emptyTblCell() }
//        cell.index = indexPath
        if indexPath.section == 0 {
            if indexPath.row < friends.count {
                cell.configCell(indexPath: indexPath, data: friends[indexPath.row])
            }
        }
        else {
            if indexPath.row < otherUsers.count {
                cell.configCell(indexPath: indexPath, data: otherUsers[indexPath.row])
            }
            if self.loadMore && indexPath.row == (self.otherUsers.count - 2){
                self.pageNumber += 1
                self.loadUsersAndFriendList(searchText: searchBar.text?.trim ?? "")
            }
        }
        cell.cellCheckBox.boxType = .square
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = indexPath.section == 0 ? friends[indexPath.row] : otherUsers[indexPath.row]
        self.navigateToChat(user: user)
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: GeneralViewCell.self), bundle: nil), forCellReuseIdentifier: "GeneralCell")
        
        self.tableView.separatorStyle = .none
        
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }
}
