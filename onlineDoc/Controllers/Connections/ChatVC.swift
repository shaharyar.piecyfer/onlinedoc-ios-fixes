//
//  ChatVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 14/03/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import AVKit
import SKPhotoBrowser
import MobileCoreServices
import WeScan
import VisionKit
import PDFKit
import OpalImagePicker
import Photos
import Lightbox

class ChatVC: BaseVC {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            let image = UIImage(named: "carrotPrevious")?.withRenderingMode(.alwaysTemplate)
            btnBack.setImage(image, for: .normal)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var composerView: UIView!{
        didSet{
            composerView.layer.cornerRadius = 6
            composerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var uiAcceptRequest : UIView!
    @IBOutlet weak var uiCancelRequest : UIView!
    @IBOutlet weak var uiSendRequest : UIView!
    
    let socketManager = SocketManager.sharedInstance
    var messageType : MessageType = .none
//    var connectionStatus : FriendRequestType = .none
    let videoFileName = "/od_video.mov"
    
    var messagePayLoad : AnyObject!
    var fromNotification = false
    var recallsForSocket = 0
    var lastTypingAt: DispatchTime?
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var newMessageTextView: UITextView!{
        didSet{
            newMessageTextView.text = ChatPlaceholder.localized()
        }
    }
    @IBOutlet weak var keyboardHeightLayoutConstraint:NSLayoutConstraint!
    @IBOutlet weak var composerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var composerViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var requestView: UIView!
    @IBOutlet weak var heightRequestView: NSLayoutConstraint!
//    @IBOutlet weak var userImageView: UIImageView!{
//        didSet{
//            userImageView.layer.cornerRadius = userImageView.frame.size.height / 2
//            userImageView.clipsToBounds = true
//        }
//    }
//    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var attachFileButton: UIButton!{
        didSet{
            let image = UIImage(named: "attached-1")?.withRenderingMode(.alwaysTemplate)
            attachFileButton.setImage(image, for: .normal)
        }
    }
    @IBOutlet weak var postCommentButton: UIButton!{
        didSet{
            let image = UIImage(named: "send")?.withRenderingMode(.alwaysTemplate)
            postCommentButton.setImage(image, for: .normal)
        }
    }
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var virtualUserPopup: UIButton!
    
    var chatRoom: ChatRoomModel?
    var chatId: Int?
    
    var chatMessages = [ChatModel]()
    var loadMore = true
    var pageNumber = 1
    
    var isAutoScroll = false
    
    var isMessageEdit = false
    var messageToEdit = ChatModel()
    var indexToEdit: IndexPath?
    
    var receiver: User?
    
    var keyBoardHeight:CGFloat = 0
    var documentInteractionController: UIDocumentInteractionController!
    var isFromPickRecipientsScreen = false
    
    //share files from outside the app
    var attachements = [Attachment]()
    
    var onDismiss: (()->())?
    var onDeleteChat: ((_ chatId: Int)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        if self.chatRoom == nil {
            self.getChatRoom()
        }
        else {
            self.setupUI()
            self.loadChat()
        }
        
        self.composerView.layer.cornerRadius = composerView.frame.size.height / 2
        self.composerView.clipsToBounds = true
        self.composerView.layer.borderColor = UIColor.darkGray.cgColor
        self.composerView.layer.borderWidth = 0.5
        
//        if (self.fromNotification) {
//            self.messageFromNotification()
//            self.fromNotification = false
//        }
        self.setupNotificationObservers()
        self.keyboardSetting()
        self.setupTheme()
        self.setSocket()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: ChatVC.applyTheme)
        
        Themer.shared.register(
            target: self,
            action: ChatVC.setTheme)
    }
    
    private func setSocket() {
        print("ChatVC->setSocket")
        if socketManager.client?.isConnected ?? false {
            if self.chatId != nil || self.chatRoom != nil {
                let params = ["room_id": String(self.chatId ?? self.chatRoom!.chat!.chat_room_id!)]
                if (self.socketManager.channelChat?.isSubscribed ?? false) == false {
                    print("ChatVC->isSubscribed->true")
                    self.delay(0.5) { [weak self] in
                        guard let `self` = self else {return}
                        print("ChatVC->goForSubscribe")
                        self.socketManager.subscribeChannel(channel: .CHAT, params: params)
                    }
//                    socketManager.delegate = self
                }
                else {
                    print("ChatVC->isSubscribed->false")
                }
            }
        }
        
        if self.fromNotification && socketManager.client.isConnected == false && self.recallsForSocket < 5 {
            self.recallsForSocket += 1
            self.delay(0.5) { [weak self] in
                guard let `self` = self else {return}
                self.setSocket()
            }
        }
    }
    
    private func setupNotificationObservers() {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifySocket, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            if let notify = notification.userInfo?["Notify"] as? NotifySocket {
                switch notify {
                case .appearance:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .APPEARANCE {
                        if let data = notification.userInfo?["Data"] as? SocketAppearanceModel {
                            if let user = data.data?.user, self.chatRoom?.user?.id == user.id {
                                DispatchQueue.main.async {
                                    self.lblStatus.text = user.online_status!
                                    self.updateTitleView(title: user.full_name ?? "Chating", subtitle: user.online_status!, baseColor: AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor)
                                }
                            }
                        }
                    }
                    
                case .message:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .CHAT {
                        if let data = notification.userInfo?["Data"] as? SocketMessageModel {
                            if data.data?.chat_room_id == self.chatId {
                                if data.type == "new", let _data = data.data {
                                    if self.chatMessages.isEmpty {
                                        DispatchQueue.main.async {
                                            self.tableView.setEmptyMessage("")
                                        }
                                    }
                                    self.chatMessages.append(_data)
                                    DispatchQueue.main.async {
                                        self.tableView.reloadData()
                                        self.tableView.scrollToBottom()
                                        if _data.sender_id != AppConfig.shared.user.id {
//                                            self.delay(1.0) { [weak self] in
//                                            guard let `self` = self else {return}
                                                self.readChat()
//                                            }
                                        }
                                    }
                                }
                                else if data.type == "update", let index = self.chatMessages.firstIndex(where: { $0.id == data.data?.id }) {
                                    if let _data = data.data {
                                        self.chatMessages[index] = _data
                                        DispatchQueue.main.async {
                                            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                                        }
                                    }
                                }
                                else if data.type == "delete", let _data = data.data, let senderId = _data.sender_id, senderId != AppConfig.shared.user.id, let index = self.chatMessages.firstIndex(where: { $0.id == _data.id }) {
                                    self.chatMessages.remove(at: index)
                                    DispatchQueue.main.async {
                                        self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .none)
                                    }
                                }
                            }
                        }
                    }
                    
                case .typing:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .CHAT {
                        DispatchQueue.main.async {
                            self.lblStatus.text = "typing..."
                            let title = self.chatRoom?.user?.full_name ?? self.receiver?.full_name
                            self.updateTitleView(title: title ?? "Chating", subtitle: "typing...", baseColor: AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor)
                            self.lastTypingAt = .now()
                            self.delay(2.0) { [weak self] in
                                guard let `self` = self else {return}
                                let subTitle = self.chatRoom?.user?.online_status ?? "offline"
                                self.lblStatus.text = subTitle
                                self.updateTitleView(title: title ?? "Chating", subtitle: subTitle, baseColor: AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor)
                            }
                        }
                    }
                    
                case .readMessage:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .CHAT {
                        if let userId = notification.userInfo?["Data"] as? Int {
                            guard userId != AppConfig.shared.user.id else {return}
                            DispatchQueue.main.async {
                                self.chatMessages.indices.forEach {
                                    self.chatMessages[$0].receiver_is_read = true
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                    
                default:
                    break
                }
            }
        }
    }
    
    private func keyboardSetting(){
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillShowNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillHideNotification,object: nil)
    }
    
    @objc func keyboardNotification(_ notification: NSNotification) {
        let isShowing = notification.name == UIResponder.keyboardWillShowNotification

        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.intValue ?? Int(UIView.AnimationOptions.curveEaseInOut.rawValue)
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: UInt(animationCurveRaw))
            self.keyboardHeightLayoutConstraint?.constant = isShowing ? -(endFrame!.size.height) : 0
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
            UIView.animate(withDuration: 0,
                           delay: 0,
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() }) { (completed) in
                if self.chatMessages.count > 2 {
                    let indexPath = IndexPath(row: self.chatMessages.count - 1, section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
                    
                }
            }
        }
    }
    
    func readChat() {
        if socketManager.channelChat != nil && self.socketManager.client?.isConnected == true && (self.socketManager.channelAppearance?.isSubscribed ?? false) == true && AppConfig.shared.user.id != nil {
            let params: [String : Any] = [
                "chat_room_id": self.chatId!,
                "user_id" : AppConfig.shared.user.id!
            ]
            socketManager.emit(channel: .APPEARANCE, action: .MESSAGE_READ, params: params)
        }
    }

    @IBAction func onClickBack(){
        if socketManager.channelChat != nil {
            socketManager.channelChat.unsubscribe()
            socketManager.channelChat = nil
        }
        self.dismissVC()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (self.fromNotification) {
            navigationController?.setNavigationBarHidden(true, animated: animated)
        }
        
        if self.isMovingFromParent {
            self.onDismiss?()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.attachements.count > 0 {
            self.checkShareAttachment()
        }
    }
    
    private func getChatRoom() {
        guard let chatId = self.chatId else {
            self.setupUI()
            return
        }
        RequestManager.shared.getChatRoom(chatId: chatId) { (status, response) in
            if status, let data = response as? ChatRoomModel {
                self.chatRoom = data
                self.setupUI()
                self.loadChat()
            }
        }
    }
    
    private func setupUI() {
//        if (isFromPickRecipientsScreen) {
//            self.requestView.isHidden = true
//            isFromPickRecipientsScreen = false
//        }
//        guard let chatRoom = self.chatRoom else {
//            if let receiver = receiver {
//                self.lblName.text = receiver.full_name
//            }
//            return
//        }
        self.chatId = self.chatRoom?.chat?.chat_room_id
        self.navigationItem.title = self.chatRoom?.user?.full_name ?? receiver?.full_name
        self.lblName.text = self.chatRoom?.user?.full_name ?? receiver?.full_name
        self.lblStatus.text = self.chatRoom?.user?.online_status ?? "offline"
        self.virtualUserPopup.isHidden = true
        self.virtualUserPopup.setTitle(virtualUserPopupMessage.localized(), for: .normal)
        self.virtualUserPopup.titleLabel?.font = AppFont.kMuliLight(14).font()
        self.virtualUserPopup.titleLabel?.lineBreakMode = .byWordWrapping
        
        var title = self.chatRoom?.user?.full_name ?? receiver?.full_name
        if self.chatRoom?.user?.virtual_user ?? false {
            title = self.chatRoom?.user?.email ?? self.chatRoom?.user?.phone
            self.virtualUserPopup.isHidden = false
            let name = self.chatRoom?.user?.full_name ?? receiver?.full_name
            if let name = name {
                title = name.lowercased().contains("virtual") ? (self.chatRoom?.user?.email ?? self.chatRoom?.user?.phone) : name.capitalized
            }
        }
        let subTitle = self.chatRoom?.user?.online_status ?? "offline"
        updateTitleView(title: title ?? "Chating", subtitle: subTitle, baseColor: AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor)
//        updateTitleView(title: "MessageKit", subtitle: isHidden ? "2 Online" : "Typing...")
        self.newMessageTextView.delegate = self
        self.messageLabel.text = connectionMessage.localized()
        
        self.requestView.isHidden = true
        self.heightRequestView.constant = 0
        if let user = self.chatRoom?.user, let friendStatus = self.chatRoom?.friend_status?.requestStatus {
            if friendStatus == FriendStatus.ACCEPT.rawValue {
                self.requestView.isHidden = true
                self.heightRequestView.constant = 0
                self.uiCancelRequest.isHidden = true
                self.uiAcceptRequest.isHidden = true
                self.uiSendRequest.isHidden = true
            }
            else if friendStatus == FriendStatus.REQUEST.rawValue {
                self.requestView.isHidden = false
                self.heightRequestView.constant = 50
                if self.chatRoom?.friend_status?.senderId == AppConfig.shared.user.id {
                    self.uiAcceptRequest.isHidden = true
                    self.messageLabel.text = connectionRejectMessage.localized()
                }
                else {
                    self.uiAcceptRequest.isHidden = false
                    self.messageLabel.text = connectionAcceptMessage.localized()
                }
                self.uiCancelRequest.isHidden = false
                self.uiSendRequest.isHidden = true
            }
            else { //cancel: then show request pane
                self.messageLabel.text = connectionMessage.localized()
                self.requestView.isHidden = false
                self.heightRequestView.constant = 50
                self.uiCancelRequest.isHidden = true
                self.uiAcceptRequest.isHidden = true
                self.uiSendRequest.isHidden = false
            }
            
            if user.virtual_user ?? false {
                self.requestView.isHidden = true
                self.heightRequestView.constant = 0
                self.uiCancelRequest.isHidden = true
                self.uiAcceptRequest.isHidden = true
                self.uiSendRequest.isHidden = true
            }
            
        }
    }
    
    private func checkShareAttachment() {
        if attachements.count > 0 {
            if attachements.count == 1 && attachements.first!.file_main_type == "web_url" {
                self.newMessageTextView.text = attachements.first!.FileName
                self.newMessageTextView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
                attachements.removeAll()
            }
            else {
                var multiFilesParams = [UploadMultiFileParams]()
                for item in attachements {
                    var directUploadParams = DirectUploadParams()
                    directUploadParams.filename = item.FileName
                    directUploadParams.content_type = MimeType(ext: item.FileName.fileExtension())
                    directUploadParams.byte_size = item.base64String.count
                    
                    let param = UploadMultiFileParams(directUploadParams: directUploadParams, data: item.base64String, image: nil)
                    multiFilesParams.append(param)
                    if multiFilesParams.count == attachements.count {
                        self.uploadForSignedId(multiFilesParams)
                        multiFilesParams.removeAll()
                        attachements.removeAll()
                    }
                }
            }
        }
    }
    
    deinit {
        print("ChatVC->deinit")
        if socketManager.channelChat?.isSubscribed ?? false {
            socketManager.channelChat.unsubscribe()
            socketManager.channelChat = nil
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func loadMoreMessages(_ sender: Any) {
        if loadMore {
            self.refreshControl.beginRefreshing()
            self.pageNumber += 1
            self.loadChat()
        }
        else {
            self.refreshControl.endRefreshing()
        }
    }
    
    private func loadChat() {
        guard let chatId = self.chatId else { return }
        if self.pageNumber == 1 {
            self.chatMessages.removeAll()
        }
        self.tableView.setEmptyMessage("")
        RequestManager.shared.getChat(chatId: chatId, page: self.pageNumber) { [weak self] (status, response) in
            guard let `self` = self else {return}
            self.refreshControl.endRefreshing()
            self.delay(1.0) { [weak self] in
                guard let `self` = self else {return}
                self.appDelegate.notificationPayload = nil
            }
            if status, let result = response as? ChatMessagesResult, let data = result.data {
                if !data.isEmpty {
                    if data.count < pageLimit10 {
                        self.loadMore = false
                    }
                    var _data = data
                    _data.removeAll { (item) -> Bool in
                        if item.message_type == MessageTypes.message.rawValue {
                            return (item.message == nil || (item.message?.isEmpty ?? false)) ? true : false
                        }
                        else if item.message_type == MessageTypes.file.rawValue {
                            return (item.files == nil || (item.files?.isEmpty ?? false)) ? true : false
                        }
                        return false
                    }
                    self.chatMessages.insert(contentsOf: _data.reversed(), at: 0)
                    self.tableView.reloadData()
//                    var rows = [IndexPath]()
//                    for (index, _) in data.enumerated() {
//                        rows.append(IndexPath(row: index, section: 0))
//                    }
//                    self.tableView.insertRows(at: rows, with: .none)
                    if self.pageNumber == 1 {
                        self.tableView.scrollToBottom()
                        self.apiCallForChatCount()
                        self.readChat()
                    }
                    else {
                        let index = IndexPath(row: data.count, section: 0)
                        self.tableView.scrollToRow(at: index, at: .bottom, animated: false)
                    }
                } else {
                    self.loadMore = false
                    if self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                    }
                }
            }
        }
    }
    
    func preview(file:URL, fileName : String = "") {
        self.documentInteractionController = UIDocumentInteractionController.init(url: file)
        self.documentInteractionController?.delegate = self
//        UITextView.appearance().tintColor = UIColor.whiteOrBlack
//        UINavigationBar.appearance().tintColor = UIColor.whiteOrBlack
        if fileName != "" {
            self.documentInteractionController.name = fileName
        }
        documentInteractionController.presentPreview(animated: true)
    }
    
    func storeAndPreview(withURLString: String, name : String = "", contentType: String) {
        guard let url = URL(string: withURLString) else { return }
        self.showLoader()
        URLSession.shared.dataTask(with: url) { data, response, error in
            self.hideLoader()
            guard let data = data, error == nil else { return }
            var fileName = name != "" ? name : response?.suggestedFilename ?? "upload.pdf"
            let ext = mimeTypes.first { (_key, _value) -> Bool in
                if _value == contentType {
                    fileName.append("."+_key)
                    return true
                }
                return false
            }
            if ext == nil {
                fileName.append(".pdf")
            }
            
            let tmpURL = FileManager.default.temporaryDirectory.appendingPathComponent(fileName)
            do {
                try data.write(to: tmpURL)
            } catch {
                print("storeAndPreview->error", error)
            }
            DispatchQueue.main.async {
                self.preview(file: tmpURL, fileName: name)
            }
        }.resume()
    }
    
    func handlePreview(index: IndexPath, isComments: Bool = true) {
        guard let file = self.chatMessages[index.row].files?.first else {
            return
        }
        if let type = file.content_type {
            if type.contains("image") {
                let cell = self.tableView.cellForRow(at: index) as? CommentCell
                if let imageView = cell?.iv1 {
                    self.preview(imageOf: imageView)
                }
            }
            else if type.contains("video") {
                if let path = file.src {
                    self.play(video: path)
                }
            }
            else {
                if let path = file.src, let name = file.name, let contentType = file.content_type {
                    self.showLoader()
                    self.storeAndPreview(withURLString: path, name: name, contentType: contentType)
                }
            }
        }
    }
    
    func showScannedFileRenamePopUp(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: "Rename Scanned File".localized(), for: "Do you want to rename scanned file?".localized(), firstBtnStyle: .default, secondBtnStyle: .default, firstBtnTitle: yesText.localized(), secondBtnTitle: noText.localized()) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                self.showSaveDialog(params, data, thumbNail)
            } else {
                self.uploadForSignedId(params, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(params, data, thumbNail)
//                }
            }
        }
    }
    
    func showSaveDialog(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: saveDocuments.localized(), for: "Enter document name".localized(), tfPlaceholder: String(Date().getTime())) { [weak self] (status, text) in
            guard let `self` = self else {return}
            if (text == nil || text!.isEmpty) {
                self.uploadForSignedId(params, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(file)
//                }
            }
            else {
                var info = params
                info.filename = text!+".pdf"
                self.uploadForSignedId(info, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(file)
//                }
            }
        }
    }
    
    @IBAction func attachFile(){
    }
    
    func play(video:String){
        if let url = URL(string: video){
            let player = AVPlayer(url: url)
            let controller = AVPlayerViewController()
            controller.player = player
            self.present(controller, animated: true, completion: nil)
            player.play()
        }
    }
    
    func showSlider(with images : [SKPhoto], at index:Int?){
        SKPhotoBrowserOptions.displayBackAndForwardButton = true
        SKPhotoBrowserOptions.backgroundColor = .black
        SKPhotoBrowserOptions.enableSingleTapDismiss = true
        
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        //            SKPhotoBrowser(photos: images)
        //            SKPhotoBrowser(photos: img, initialPageIndex: index)
        //        if let i = index{
        //            browser.initializePageIndex(i)
        //        }
        browser.delegate = self
        present(browser, animated: true, completion: nil)
    }
    
    func preview(imageOf view:UIImageView){
        var image = [SKPhoto]()
        if let img = view.image{
            image.append(SKPhoto.photoWithImage(img))
        }
        self.showSlider(with: image, at: nil)
    }
    
    private func uploadForSignedId(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?){
        RequestManager.shared.directUpload(_params: params, data: data) { (status, result) in
            if status {
                if let info = result as? DirectUploadModel {
                    let upload = Uploads(directUploadInfo: info, data: data, image: thumbNail)
                    var progressView: ProgressView?
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else {return}
                        progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
                        progressView!.lblTitle.text = "txt_progress_title".localized()
                        progressView!.lblDetail.text = "txt_progress_detail".localized()
                        progressView!.uiProgress.progress = 0
                        progressView!.tag = 100
                        progressView!.lblCount.text = "1/1"
                        self.addView(view: progressView!)
                    }
                    RequestManager.shared.directUploadImage(data: upload.data, params: upload.directUploadInfo) { [weak self] (percent) in
                        guard let _ = self else {return}
                        progressView?.uiProgress.progress = Float(percent)
                    } completion: { [weak self] (status, response) in
                        guard let `self` = self else {return}
                        if let progressView = progressView {
                            self.removeView(tag: progressView.tag)
                        }
                        if status {
                            var newMessage = PostMessageParams()
                            if let id = self.chatRoom?.user?.id {
                                newMessage.receiver_id = id
                            }
                            newMessage.message_type = .file
                            newMessage.files = [upload.directUploadInfo.signed_id!]
                            self.handleSendNewMessage(with: newMessage)
                        }
                        else {
                            self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                        }
                    }
                }
            }
        }
    }
    
    private func uploadForSignedId(_ params: [UploadMultiFileParams]) {
        var newMessage = PostMessageParams()
        let id = self.chatRoom?.user?.id ?? self.receiver?.id
        if let id = id {
            newMessage.receiver_id = id
        }
        newMessage.message_type = .file
        newMessage.files = []

        var progressView: ProgressView?
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else {return}
            progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
            progressView!.lblTitle.text = "txt_progress_title".localized()
            progressView!.lblDetail.text = "txt_progress_detail".localized()
            progressView!.uiProgress.progress = 0
            progressView!.tag = 100
            progressView!.lblCount.text = "1/\(params.count)"
            self.addView(view: progressView!)
        }

        let dispatchQueue = DispatchQueue(label: "FileUploadingQueue", qos: .background)
        let semaphore = DispatchSemaphore(value: 0)
        dispatchQueue.async {
            for (index, item) in params.enumerated() {
                RequestManager.shared.directUpload(_params: item.directUploadParams, data: item.data, isShowLoader: false) { [weak self] (status, result) in
                    guard let `self` = self else {return}
                    if status {
                        if let info = result as? DirectUploadModel {
                            let upload = Uploads(directUploadInfo: info, data: item.data, image: item.image)
                            RequestManager.shared.directUploadImage(data: upload.data, params: upload.directUploadInfo) { [weak self] (percent) in
                                guard let _ = self else {return}
                                progressView?.lblCount.text = "\(index+1)/\(params.count)"
                                progressView?.uiProgress.progress = Float(percent)
                            } completion: { [weak self]  (status, response) in
                                guard let `self` = self else {return}
                                if status {
                                    newMessage.files!.append(upload.directUploadInfo.signed_id!)
                                    if newMessage.files?.count == params.count {
                                        if let progressView = progressView {
                                            self.removeView(tag: progressView.tag)
                                        }
                                        self.handleSendNewMessage(with: newMessage)
                                    }
                                }
                                else {
                                    if let progressView = progressView {
                                        self.removeView(tag: progressView.tag)
                                    }
                                    self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                                }
                                semaphore.signal()
                            }
                        }
                    }
                    else {
                        semaphore.signal()
                    }
                }
                semaphore.wait()
            }
        }
    }
    
    private func showMoreOptions(isEdit:Bool, isDelete: Bool, indexPath: IndexPath? = nil) {
        if isEdit || isDelete {
            var alert = AlertModel()
            if isEdit {
                var editBtn = AlertBtnModel()
                editBtn.title = "Edit"
                alert.btns.append(editBtn)
            }
            if isDelete {
                var deleteBtn = AlertBtnModel()
                deleteBtn.title = "Delete"
                alert.btns.append(deleteBtn)
            }
            var cancelBtn = AlertBtnModel()
            cancelBtn.title = "Cancel"
            cancelBtn.color = .red
            alert.btns.append(cancelBtn)
            self.showMultiBtnAlert(model: alert) { [weak self] (data) in
                guard let `self` = self else {return}
                if let btn = data as? String {
                    if btn == "Edit" {
                        self.editOptionTapped(index: indexPath!)
                    }
                    else if btn == "Delete" {
                        self.onClickDelete(indexPath: indexPath)
                    }
                }
            }
        }
    }

    func editOptionTapped(index: IndexPath) {
        let message = self.chatMessages[index.row]
        self.messageToEdit = message
        self.indexToEdit = index
        if let type = message.message_type, type == MessageTypes.message.rawValue {
            guard let text = message.message else { return }
            self.attachFileButton.isHidden = true
            self.isMessageEdit = true
            self.newMessageTextView.attributedText = text.attributedText(fontSize: 14, allStyleFont: AppFont.kMuliLight(14).font())?.attributedString
            self.newMessageTextView.becomeFirstResponder()
        }else {
            self.performSegue(withIdentifier: "gotoEditChat", sender: self)
        }
    }
    
    private func onClickDelete(indexPath: IndexPath? = nil) {
        let message = deleteAlertMessage.localized()
        self.showAlert(To: "", for: message, firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                if let index = indexPath, let id = self.chatMessages[index.row].id {
                    self.deleteMessage(id: id, index: index)
                }
            }
        }
    }
    
    private func deleteMessage(id: Int, index: IndexPath) {
        RequestManager.shared.deleteMessage(id: id) { (status, response) in
            if status {
                self.chatMessages.remove(at: index.row)
                self.tableView.deleteRows(at: [index], with: .automatic)
                self.showAlert(To: DeleteMessageTitle.localized(), for: DeleteMessageSuccess.localized()) { _ in
//                    guard let `self` = self else {return}
//                    self.delegate?.didDeleteThread()
//                    self.dismissVC()
                }
            }
        }
    }
    
    @IBAction func selectAttachemnt(){
        self.view.endEditing(true)
        self.showDialogAlert(options: .camera, .library, .video, .docs, .scan)
    }
    
    @IBAction func sendMessage() {
        self.newMessageTextView.resignFirstResponder()
        self.isAutoScroll = true
        if let messageText = self.newMessageTextView.text, messageText.trimmingCharacters(in: .whitespacesAndNewlines) != "" && messageText != ChatPlaceholder.localized() {
            if !isMessageEdit {
                var newMessage = PostMessageParams()
                newMessage.receiver_id = self.chatRoom?.user?.id ?? self.receiver?.id
//                if let id = self.chatRoom?.user?.id {
//                    newMessage.receiver_id = id
//                }
//                else {
//                    newMessage.receiver_id = reciever?.id
//                }
                newMessage.message = messageText.trimmingCharacters(in: .whitespacesAndNewlines)
                newMessage.message_type = .message
                self.handleSendNewMessage(with: newMessage)
            }
            else {
                self.handleEditMessage(with: messageText)
            }
        }else{
            return
        }
    }
    
    func handleSendNewMessage(with params : PostMessageParams) {
        self.newMessageTextView.text = ChatPlaceholder.localized()
        self.newMessageTextView.textColor = .lightGray
        self.newMessageTextView.resignFirstResponder()
        
        RequestManager.shared.sendMessage(_params: params) { (status, response) in
            if status, let response = response as? ChatMessageResult {
                if let comment = response.data, (self.chatId == nil || (self.fromNotification && self.socketManager.client?.isConnected == false) || (self.fromNotification == false && self.socketManager.client?.isConnected == false) || (self.socketManager.channelChat?.isSubscribed ?? false) == false) {
                    if self.chatMessages.isEmpty {
                        self.tableView.setEmptyMessage("")
                    }
                    self.chatId = comment.chat_room_id
                    self.setSocket()
                    var updatedComment = comment
                    updatedComment.sender_id = AppConfig.shared.user.id
                    self.chatMessages.append(updatedComment)
                    self.tableView.reloadData()
                    self.tableView.scrollToBottom()
                }
            }
        }
    }
    
    func handleEditMessage(with text: String) {
        self.isMessageEdit = false
        guard let id = self.messageToEdit.id else { return }
        var params = PostMessageParams()
        params.messageId = id
        params.message = text
        params.message_type = .message

        RequestManager.shared.updateMessage(_params: params) { (status, response) in
            if status, let response = response as? ChatMessageResult {
                self.view.endEditing(true)
                self.attachFileButton.isHidden = false
                self.newMessageTextView.resignFirstResponder()
                self.newMessageTextView.text = ChatPlaceholder.localized()
                self.newMessageTextView.textColor = .lightGray

                if let message = response.data {
                    self.chatMessages[self.indexToEdit!.row].message = message.message
                    self.tableView.reloadRows(at: [self.indexToEdit!], with: .automatic)
                }
                self.messageToEdit = ChatModel()
                self.indexToEdit = nil
                self.showAlert(To: UpdateMessageTitle.localized(), for: UpdateMessageSuccess.localized()) {_ in}
            }
        }
    }
    
    @IBAction func sendRequest() {
        if let id = self.chatRoom?.user?.id {
            var params = FriendRequestModel()
            params.receiver_id = id
            params.request_status = FriendStatus.REQUEST
            self.handleFirendRequest(params: params)
        }
    }
    
    @IBAction func accept(){
        if let id = self.chatRoom?.user?.id {
            var params = FriendRequestModel()
            params.receiver_id = id
            params.request_status = FriendStatus.ACCEPT
            self.handleFirendRequest(params: params)
        }
    }
    
    @IBAction func cancel() {
        if let id = self.chatRoom?.user?.id {
            var params = FriendRequestModel()
            params.receiver_id = id
            params.request_status = FriendStatus.CANCEL
            self.handleFirendRequest(params: params)
        }
    }
    
    func handleFirendRequest(params: FriendRequestModel){
        self.friendRequest(params: params) { [unowned self] (status,_) in
            if status {
                var friend = FriendModel()
                friend.id = self.chatRoom?.friend_status?.id
                friend.senderId = AppConfig.shared.user.id
                friend.receiverId = params.receiver_id
                friend.requestStatus = params.request_status.rawValue
                self.chatRoom?.friend_status = friend
                
                self.setupUI()
            }
        }
    }
    
    func gotoUserProfile(_ index: Int) {
        let message = self.chatMessages[index]
        if let id = message.receiver_id, id != AppConfig.shared.user.id {
            self.previewUser(id: id, fromChat: true)
        }
        else if let id = message.sender_id {
            self.previewUser(id: id, fromChat: true)
        }
    }
    
    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
        if let theError = error {
            print("error saving the video = \(theError)")
        } else {
            DispatchQueue.main.async(execute: { () -> Void in })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoEditChat" {
            if let vc = segue.destination as? EditFileCommentVC {
                vc.isChatEdit = true
                vc.messageToEdit = self.messageToEdit
                vc.updatedMessage = { [weak self] data in
                    guard let strongSelf = self else { return }
                    if let data = data {
                        var updatedData = data
                        updatedData.sender_id = AppConfig.shared.user.id
                        strongSelf.chatMessages[strongSelf.indexToEdit!.row] = updatedData
                        strongSelf.tableView.reloadRows(at: [strongSelf.indexToEdit!], with: .automatic)
                    }
                    else {
//                        strongSelf.chatMessages.remove(at: strongSelf.indexToEdit!.row)
//                        strongSelf.tableView.reloadData()
                        if let id = strongSelf.chatMessages[strongSelf.indexToEdit!.row].id {
                            strongSelf.deleteMessage(id: id, index: strongSelf.indexToEdit!)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onClickVirtualUserPopup() {
        self.showVirtualUserOptions()
    }
    
    private func showVirtualUserOptions() {
        var alert = AlertModel()
        
        var editBtn = AlertBtnModel()
        editBtn.title = "Resend invitation".localized()
        alert.btns.append(editBtn)
        
        var deleteBtn = AlertBtnModel()
        deleteBtn.title = "Delete chat".localized()
        alert.btns.append(deleteBtn)
        
        var cancelBtn = AlertBtnModel()
        cancelBtn.title = "Cancel"
        cancelBtn.color = .red
        alert.btns.append(cancelBtn)
        self.showMultiBtnAlert(model: alert) { [weak self] (data) in
            guard let `self` = self else {return}
            if let btn = data as? String {
                if btn == "Resend invitation".localized() {
                    self.callAPIResendInvitation()
                }
                else if btn == "Delete chat".localized() {
                    self.showAlert(To: "", for: deleteChatMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
                        guard let `self` = self else {return}
                        if status {
                            self.callAPIDeleteChat()
                        }
                    }
                }
            }
        }
    }
    
    private func callAPIDeleteChat() {
        if let chatId = self.chatId {
            RequestManager.shared.deleteChatRoom(id: chatId) { (status, response) in
                if status{
                    if let _ = response as? GeneralSuccessResult {
                        self.onDeleteChat?(chatId)
                        self.dismissVC()
                    }
                }
            }
        }
    }
    
    private func callAPIResendInvitation() {
        let user = self.chatRoom?.user?.email ?? self.chatRoom?.user?.phone ?? ""
        let invitationBy: NewChatSelections = self.chatRoom?.user?.email != nil ? .EMAIL : .PHONE
        if !user.isEmpty {
            var name = ""
            if self.chatRoom?.user?.virtual_user ?? false {
                name = self.chatRoom?.user?.full_name ?? ""
            }
            RequestManager.shared.createVirtualUser(by: user, name: name, isEmail: invitationBy == .EMAIL) {_,_  in}
        }
        
    }
}

extension ChatVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < self.chatMessages.count else {
            return self.emptyTblCell()
        }
        let message = self.chatMessages[indexPath.row]
//        if indexPath.row == self.chatMessages.count - 1 && self.pageNumber == 1 {
//            self.loadMore = true
//        }
        if AppConfig.shared.user.id == message.sender_id {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell2", for: indexPath) as? CommentCell else {
                print("Unable to read the CommentCell2")
                return self.emptyTblCell()
            }
            cell.configCell(index: indexPath, data: message, isForMine: true, receiver: nil)
            cell.onClickProfile = { _ in }
//            if self.loadMore && indexPath.row == 1 {
//                self.pageNumber += 1
//                self.loadChat()
//            }
            cell.delegate = self
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CommentCell.self), for: indexPath) as? CommentCell else {
                print("Unable to read the CommentCell")
                return self.emptyTblCell()
            }
            cell.configCell(index: indexPath, data: message, isForMine: false, receiver: self.chatRoom?.user)
            cell.onClickProfile = { [weak self] (index) in
                guard let strongSelf = self else { return }
                strongSelf.gotoUserProfile(index.row)
            }
//            if self.loadMore && indexPath.row == 1 {
//                self.pageNumber += 1
//                self.loadChat()
//            }
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if self.chat[indexPath.row].multiFiles != nil {
//            self.getDataFromIndexToDisplay(index: indexPath.row)
//        }
        if let files = self.chatMessages[indexPath.row].files {
            if files.count > 1 {
                self.displayImagesVideos(index: indexPath.row)
            }
            else {
                self.handlePreview(index: indexPath)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.row == self.chat.count - 1{
//            if isAutoScroll{
//                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
//                self.isAutoScroll = false
//            }
//        }
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: String(describing: CommentCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CommentCell.self))
        self.tableView.register(UINib(nibName: "CommentCell2", bundle: nil), forCellReuseIdentifier: "CommentCell2")
        
        self.tableView.separatorStyle = .none
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        
        self.refreshControl.addTarget(self, action: #selector(self.loadMoreMessages(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }
}

extension ChatVC: LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func displayImagesVideos(index: Int) {
        guard let files = self.chatMessages[index].files else { return }
        let imagesVides = files.filter{ $0.content_type!.contains("image") || $0.content_type!.contains("video") }
        var combineData = [LightboxImage]()
        for file in imagesVides {
            if file.content_type!.contains("image") {
                if let path = file.src, path != "" {
                    if let url = URL(string: path) {
                        let res = LightboxImage(imageURL: url)
                        combineData.append(res)
                    }
                }
            }else{
                if let path = file.src, path != "" {
                    if let url = URL(string: path) {
                        let res = LightboxImage(image: UIImage(named: "vidThumb")!, text: "", videoURL: url)
                        combineData.append(res)
                    }
                }
            }
        }
        
        // Configure the LightBox
        LightboxConfig.CloseButton.text = closeText.localized()
        // Create an instance of LightboxController.
        let controller = LightboxController(images: combineData)
        
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
        
    }
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print("lightboxController->page", page)
    }
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        print("lightboxController->dissmiss view")
    }
}

extension ChatVC: UITextViewDelegate {
    
    //    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    //        if textView == self.newMessageTextView{
    //            KeyboardAvoiding.padding = 20
    //            KeyboardAvoiding.avoidingView = textView
    //            KeyboardAvoiding.padding = 0
    //        }
    //        return true
    //    }
    
    func textViewDidChange(_ textView: UITextView) {
        guard let _ = textView.text, textView == self.newMessageTextView else {
            return
        }
//        self.callUpdateTypeStatus(value: true)
        if self.chatId != nil {
            let params: [String : Any] = ["chat_room_id": self.chatId!]
            socketManager.emit(channel: .APPEARANCE, action: .TYPING, params: params)
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if let txt = textView.text, txt == ChatPlaceholder.localized(){
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
            textView.text = nil
        }else{
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let text = textView.text, textView == self.newMessageTextView else {
            return
        }
        if text.isEmpty {
            textView.textColor = .lightGray
            textView.text = ChatPlaceholder.localized()
        }
//        else if text != ChatPlaceholder.localized() {
//            self.callUpdateTypeStatus(value: false)
//        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(getHintsFromTextField),
            object: textView)
        self.perform(
            #selector(getHintsFromTextField),
            with: textView,
            afterDelay: 2.0)
        return true
    }

    @objc func getHintsFromTextField(textView: UITextView) {
//        print("getHintsFromTextField")
//        self.callUpdateTypeStatus(value: false)
    }
}

extension ChatVC {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let _capturedImage = info[.originalImage] as? UIImage {
            picker.dismiss(animated: true, completion: nil)
            let capturedImage = self.imageOrientation(_capturedImage)
//            self.profileImageView.image = capturedImage
            
            var fileName = UUID().uuidString + ".jpeg"
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                fileName = url.lastPathComponent
    //          fileType = url.pathExtension
            }
            
            let _imageData = capturedImage.jpegData(compressionQuality: capturedImage.size.width > 500 ? 0.5 : 1.0) ?? Data(capturedImage.sd_imageData()!)
            let imageSize: Int = _imageData.count
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //_imageData.fileExtension
            directUploadParams.byte_size = imageSize
            
            self.uploadForSignedId(directUploadParams, _imageData, capturedImage)
        }
        else if let selectedVideo:URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) {
            // Save video to the main photo album
            let selectorToCall = #selector(self.videoSaved(_:didFinishSavingWithError:context:))
            
            UISaveVideoAtPathToSavedPhotosAlbum(selectedVideo.relativePath, self, selectorToCall, nil)
            // Save the video to the app directory
            var _videoData = Data()
            let videoData = try? Data(contentsOf: selectedVideo)
            let paths = NSSearchPathForDirectoriesInDomains(
                FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
            let dataPath = documentsDirectory.appendingPathComponent(videoFileName)
            
            try! videoData?.write(to: dataPath, options: [])
            
            do {
                let data = try Data(contentsOf: dataPath, options: .mappedIfSafe)
                _videoData = data
            } catch  {
                
            }
            let thumbNail = dataPath.generateThumbnail()
            let fileName = selectedVideo.lastPathComponent
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = _videoData.count
            
            self.uploadForSignedId(directUploadParams, _videoData, thumbNail)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

//UIDocumentBrowserViewController
extension ChatVC {
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentsAt documentURLs: [URL]) {
        controller.dismiss(animated: true, completion: nil)
        guard documentURLs.count < maxAllowedAttachments else {
            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
            return
        }
        var multiFilesParams = [UploadMultiFileParams]()
        for url in documentURLs {
            let fileName = url.lastPathComponent
            var fileData = Data()
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                fileData = data
            } catch  {
                print("Unable to get data of the file")
            }
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = fileData.count
            
            let param = UploadMultiFileParams(directUploadParams: directUploadParams, data: fileData, image: nil)
            multiFilesParams.append(param)
        }
        if !multiFilesParams.isEmpty {
            self.uploadForSignedId(multiFilesParams)
        }
    }
    
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
//        guard urls.count < maxAllowedAttachments else {
//            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
//            return
//        }
//        var multiFilesParams = [UploadMultiFileParams]()
//        for url in urls {
//            let fileName = url.lastPathComponent
//            var fileData = Data()
//            do {
//                let data = try Data(contentsOf: url, options: .mappedIfSafe)
//                fileData = data
//            } catch  {
//                print("Unable to get data of the file")
//            }
//            var directUploadParams = DirectUploadParams()
//            directUploadParams.filename = fileName
//            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//            directUploadParams.byte_size = fileData.count
//
//            let param = UploadMultiFileParams(directUploadParams: directUploadParams, data: fileData, image: nil)
//            multiFilesParams.append(param)
//        }
//        if !multiFilesParams.isEmpty {
//            self.uploadForSignedId(multiFilesParams)
//        }
//    }
//
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
//        let fileName = url.lastPathComponent
//        var fileData = Data()
//        do {
//            let data = try Data(contentsOf: url, options: .mappedIfSafe)
//            fileData = data
//        } catch  {
//            print("Unable to get data of the file")
//        }
//        var directUploadParams = DirectUploadParams()
//        directUploadParams.filename = fileName
//        directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//        directUploadParams.byte_size = fileData.count
//
//        self.uploadForSignedId(directUploadParams, fileData, nil)
//    }
//
//    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//        // Picker was cancelled! Duh 🤷🏻‍♀️
//        self.dismiss(animated: true, completion: nil)
//    }
}

//MARK: - CommentCellDelegate
extension ChatVC: CommentCellDelegate {
    func didTapMoreOptions(index: IndexPath) {
        self.showMoreOptions(isEdit: true, isDelete: true, indexPath: index)
    }
    
    func previewComment(fileWith url: String, name: String, contentType: String) {
        self.storeAndPreview(withURLString: url, name: name, contentType: contentType)
    }
}

extension ChatVC: UIDocumentInteractionControllerDelegate {
    private func documentInteractionControllerViewControllerForPreview(controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
//        UITextView.appearance().tintColor = .white
//        self.newCommentTextView.tintColor = .white
    }
}

//VNDocumentCameraViewControllerDelegate
extension ChatVC {
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        print(error)
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        guard scan.pageCount >= 1 else {
            controller.dismiss(animated: true)
            return
        }
        DispatchQueue.main.async {
            let pdfDocument = PDFDocument()
            for i in 0 ..< scan.pageCount {
                if  let image = scan.imageOfPage(at: i).resize(toWidth: 700) {
                    print(image)
                    // Create a PDF page instance from your image
                    let pdfPage = PDFPage(image: image)
                    // Insert the PDF page into your document
                    pdfDocument.insert(pdfPage!, at: i)
                }
            }
            // Get the raw data of your PDF document
            let data = pdfDocument.dataRepresentation()
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let docURL = documentDirectory.appendingPathComponent(Date().getTime()+".pdf")
            do{
                try data?.write(to: docURL)
                
                let fileName = docURL.lastPathComponent
                var directUploadParams = DirectUploadParams()
                directUploadParams.filename = fileName
                directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                directUploadParams.byte_size = data!.count
                
//                print("CreateThreadVC->directUploadParams", directUploadParams)
//                self.uploadForSignedId(directUploadParams, data!, nil)
                self.showScannedFileRenamePopUp(directUploadParams, data!, nil)
            }catch(let error)
            {
                print("error is \(error.localizedDescription)")
            }
        }
        controller.dismiss(animated: true)
    }
}

//OpalImagePickerControllerDelegate
extension ChatVC {
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        print("ChatVC->imagePickerDidCancel")
    }
    
    internal func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        var multiFilesParams = [UploadMultiFileParams]()
        for assetItem in assets {
            AppConfig.shared.getURLFromAsset(mPhasset: assetItem) { (url) in
                if url != nil {
                    var fileName = url!.lastPathComponent
                    var fileData = Data()
                    do {
                        let data = try Data(contentsOf: url!, options: .mappedIfSafe)
                        fileData = data
                        if fileName.fileExtension().uppercased() == "HEIC" {
                            if let dataToImage = UIImage(data: data) {
                                fileName = String(fileName.split(separator: ".").first!).appending(".jpeg")
                                fileData = dataToImage.jpegData(compressionQuality: dataToImage.size.width > 500 ? 0.5 : 1.0) ?? data
                            }
                        }
                    } catch  {
                        print("Unable to get data of the file")
                    }
                    var directUploadParams = DirectUploadParams()
                    directUploadParams.filename = fileName
                    directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                    directUploadParams.byte_size = fileData.count
                    let param = UploadMultiFileParams(directUploadParams: directUploadParams, data: fileData, image: nil)
                    multiFilesParams.append(param)
                    if multiFilesParams.count == assets.count {
                        self.uploadForSignedId(multiFilesParams)
                        multiFilesParams.removeAll()
                    }
                }
                else {
                    if let img = AppConfig.shared.getAssetThumbnail(asset: assetItem) {
                        let fileName = UUID().uuidString + ".jpeg"
                        let fileData = img.jpegData(compressionQuality: img.size.width > 500 ? 0.5 : 1.0)
                        if let fileData = fileData {
                            var directUploadParams = DirectUploadParams()
                            directUploadParams.filename = fileName
                            directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                            directUploadParams.byte_size = fileData.count
                            let param = UploadMultiFileParams(directUploadParams: directUploadParams, data: fileData, image: nil)
                            multiFilesParams.append(param)
                            if multiFilesParams.count == assets.count {
                                self.uploadForSignedId(multiFilesParams)
                                multiFilesParams.removeAll()
                            }
                        }
                    }
                }
            }
        }
        //Dismiss Controller
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 0
    }
    
    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
        return URL(string: "https://placeimg.com/500/500/nature")
    }
}

extension ChatVC: SKPhotoBrowserDelegate {
    func willShowActionSheet(_ photoIndex: Int) {
//        UITextView.appearance().tintColor = UIColor.whiteOrBlack
    }
    
    func didDismissAtPageIndex(_ index: Int) {
//        UITextView.appearance().tintColor = .white
//        self.newMessageTextView.tintColor = .white
    }
}

extension ChatVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        topView.backgroundColor = theme.settings.nvBarColor
        attachFileButton.tintColor = theme.settings.titleColor
        postCommentButton.tintColor = theme.settings.titleColor
        btnBack.tintColor = theme.settings.titleColor
        lblStatus.textColor = theme.settings.subTitleColor
        requestView.backgroundColor = theme.settings.cellColor
    }
}

//extension ChatVC: SocketManagerDelegate {
//    func receiveMessage(data: SocketMessageModel) {
//        if data.data?.chat_room_id == self.chatId {
//            if data.type == "new", let _data = data.data {
//                if self.chatMessages.isEmpty {
//                    DispatchQueue.main.async {
//                        self.tableView.setEmptyMessage("")
//                    }
//                }
//                self.chatMessages.append(_data)
//                DispatchQueue.main.async {
//                    self.tableView.insertRows(at: [IndexPath(row: self.chatMessages.count-1, section: 0)], with: .none)
//                    self.tableView.scrollToBottom()
//                    if _data.sender_id != AppConfig.shared.user.id {
//                        self.readChat()
//                    }
//                }
//            }
//            else if data.type == "update", let index = self.chatMessages.firstIndex(where: { $0.id == data.data?.id }) {
//                if let _data = data.data {
//                    self.chatMessages[index] = _data
//                    DispatchQueue.main.async {
//                        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
//                    }
//                }
//            }
//        }
//    }
//
//    func userAppearance(data: SocketAppearanceModel) {
//        if let user = data.data?.user, self.chatRoom?.user?.id == user.id {
//            DispatchQueue.main.async {
//                self.lblStatus.text = user.online_status!
//                self.updateTitleView(title: user.full_name ?? "Chating", subtitle: user.online_status!, baseColor: AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor)
//            }
//        }
//    }
//
//    func typing() {
//        DispatchQueue.main.async {
//            self.lblStatus.text = "typing..."
//            let title = self.chatRoom?.user?.full_name ?? self.receiver?.full_name
//            self.updateTitleView(title: title ?? "Chating", subtitle: "typing...", baseColor: AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor)
//            self.lastTypingAt = .now()
//            self.delay(2.0) {
//                let subTitle = self.chatRoom?.user?.online_status ?? "offline"
//                self.lblStatus.text = subTitle
//                self.updateTitleView(title: title ?? "Chating", subtitle: subTitle, baseColor: AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor)
//            }
//        }
//    }
//
//    func readChatMessages(userId: Int) {
//        guard userId != AppConfig.shared.user.id else {return}
//        DispatchQueue.main.async {
//            self.chatMessages.indices.forEach {
//                self.chatMessages[$0].receiver_is_read = true
//            }
//            self.tableView.reloadData()
//        }
//    }
//}

//MARK: -   CUSTOM TYPE
struct SendMessage{
    var receiver_id : Int?
    var conversation_id: Int?
    var friend_id : Int?
    var message : String?
    var msgId : Int?
    var onlineStatus : String?
    var tab : String?
}

/// Custom type to pass attachment message
struct MessageMultiAttachment{
    
    var receiver_id : Int?
    var msgId : Int?
    var tab: String?
    var conversation_id : Int?
    var friend_id : Int?
    var firebaseChatCollName : String?
    var from : String?
    
    var fileData: Data?
    var mimeType: String?
    var fileName : String?
    var edit_message: Bool = false
    
}

struct MessageAttachment{
    
    var receiver_id : Int?
    var msgId : Int?
    var tab: String?
    var conversation_id : Int?
    var friend_id : Int?
    var fileData: Data?
    var mimeType: String?
    var fileName : String?
    
}

struct UpdateMessage {
    
    var message_id : String?
    var conversationI_id: String?
    var message : String?
    var messageFirebaseId: String?
    var user_id : Int?
    var tab: String?
    
}

struct DeleteMessage{
    
    var message_id : String?
    var conversationI_id : String?
    var messageFirebaseId : String?
    var tab : String?
}

struct DeleteMessageMultiple{
    var message_id = [String]()
    var conversationI_id : String!
    var messageFirebaseId : String!
    var user_id : String!
    var attachment_id = [String]()
    var tab : String!
}

struct DirectMessageParams{
    
    var sender_id : Int?
    var receiver_id : Int?
    var message : String?
    var msgId : Int?
    
}
struct SetActiveChat{
    var threadId: Int?
    var conversationID: String?
    var tab : String?
    var friendId : Any?
}
