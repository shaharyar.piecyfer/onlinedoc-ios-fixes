//
//  AddAttachmentsViewController.swift
//  onlineDoc
//
//  Created by Piecyfer on 13/10/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos
import SKPhotoBrowser
import MobileCoreServices
import WeScan
import VisionKit
import PDFKit

class EditFileCommentVC: BaseVC {
    //    MARK: - Outlets
    @IBOutlet weak var attachmentCollectionView: UICollectionView!
    @IBOutlet weak var addMoreAttachmentsButton: UIButton!
    
    //    MARK: - Vairables and Instances
    let videoFileName = "/od_video.mov"
    var commentToEdit = ThreadCommentModel()
//    var deleteAttachmentsIds = [Int]()
//    var attachmentsOnAdd = [Attachment]()
    var attachements = [FileModel]() {
        didSet{
            DispatchQueue.main.async {
                self.attachmentCollectionView.reloadData()
            }
        }
    }
    var isChatEdit = false
    var messageToEdit = ChatModel()
    var conversationId = ""
    var deleteMessageIds = [Int]()
    var receiverId = 0
    var friendId = 0
    var tab = ""
    var messgaeUpdated:(()->())?
    var didAnyUpdate = false //like, delete
    
    var directUploads = [Uploads](){
        didSet{
            DispatchQueue.main.async {
                self.attachmentCollectionView.reloadData()
            }
        }
    }
    var updatedComment: ((ThreadCommentModel?)->Void)?
    var updatedMessage: ((ChatModel?)->Void)?
    
    //MARK: - IBActions
    @IBAction func didTapAddMoreAttachments(_ sender: UIButton) {
        self.showDocumentAlert()
    }
    
    //MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(title: DoneText.localized(), style: .plain, target: self, action: #selector(DoneTapped))
        self.setupCommentFiles()
    }
    
    deinit {
        print("EditFileCommentVC->deinit")
    }
    
    private func setupCommentFiles() {
        if self.isChatEdit {
            guard let files = self.messageToEdit.files else {
                return
            }
            self.attachements = files
        }
        else {
            guard let files = self.commentToEdit?.files else {
                return
            }
            self.attachements = files
        }
    }
    
    func showDocumentAlert(){
        self.showDialogAlert(options: .camera, .library, .video, .docs, .scan)
    }
    
    //MARK: - Selector Methods
    @objc func DoneTapped() {
        if !directUploads.isEmpty {
            var progressView: ProgressView?
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else {return}
                progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
                progressView!.lblTitle.text = "txt_progress_title".localized()
                progressView!.lblDetail.text = "txt_progress_detail".localized()
                progressView!.uiProgress.progress = 0
                progressView!.tag = 100
                progressView!.lblCount.text = "1/\(self.directUploads.count)"
                self.addView(view: progressView!)
            }

            let dispatchQueue = DispatchQueue(label: "FileUploadingQueue", qos: .background)
            let semaphore = DispatchSemaphore(value: 0)
            
            dispatchQueue.async {
                var files = [String]()
                for (index, item) in self.directUploads.enumerated() {
                    RequestManager.shared.directUploadImage(data: item.data, params: item.directUploadInfo) { [weak self] (percent) in
                        guard let `self` = self else {return}
                        progressView?.lblCount.text = "\(index+1)/\(self.directUploads.count)"
                        progressView?.uiProgress.progress = Float(percent)
                    } completion: { [weak self] (status, response) in
                        guard let `self` = self else {return}
                        if status {
                            files.append(item.directUploadInfo.signed_id!)
                            if files.count == self.directUploads.count {
                                if let progressView = progressView {
                                    self.removeView(tag: progressView.tag)
                                }
                                self.updateComment(files)
                            }
                        }
                        else {
                            if let progressView = progressView {
                                self.removeView(tag: progressView.tag)
                            }
                            self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                        }
                        semaphore.signal()
                    }
                    semaphore.wait()
                }
            }
        }
        else if didAnyUpdate {
            if !self.attachements.isEmpty {
                self.updateComment([])
            }
            else {
                self.updatedComment?(nil)
                self.dismissVC()
            }
        }
    }
    
    func updateComment(_ files: [String]) {
        if self.isChatEdit {
            guard let _ = self.messageToEdit.id else { return }
            var params = PostMessageParams()
            params.message_type = .file
            params.messageId = self.messageToEdit.id
            if !self.attachements.isEmpty {
                let alreadyAddedSignedIds = self.attachements.compactMap({$0.signed_id})
                params.files = alreadyAddedSignedIds + files
            }
            else {
                params.files = files
            }
            
            RequestManager.shared.updateMessage(_params: params) { (status, response) in
                if status, let response = response as? ChatMessageResult {
                    if let message = response.data {
                        self.updatedMessage?(message)
                    }
                    self.showAlert(To: UpdateMessageTitle.localized(), for: UpdateMessageSuccess.localized()) { [weak self] _ in
                        guard let `self` = self else {return}
                        self.dismissVC()
                    }
                }
            }
        }
        else {
            guard let _ = self.commentToEdit?.id else { return }
            var params = PostCommentParams()
            params.comment_type = .file
            params.commentId = self.commentToEdit!.id
            if !self.attachements.isEmpty {
                let alreadyAddedSignedIds = self.attachements.compactMap({$0.signed_id})
                params.files = alreadyAddedSignedIds + files
            }
            else {
                params.files = files
            }
            
            RequestManager.shared.updateComment(_params: params) { (status, response) in
                if status, let response = response as? ThreadCommentResponse {
                    if let comment = response.data {
                        self.updatedComment?(comment)
                    }
                    self.showAlert(To: UpdateMessageTitle.localized(), for: UpdateMessageSuccess.localized()) { [weak self] _ in
                        guard let `self` = self else {return}
                        self.dismissVC()
                    }
                }
            }
        }
    }
    
    private func uploadForSignedId(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?){
        RequestManager.shared.directUpload(_params: params, data: data) { (status, result) in
            if status {
                if let info = result as? DirectUploadModel {
                    let upload = Uploads(directUploadInfo: info, data: data, image: thumbNail)
                    self.directUploads.append(upload)
                }
            }
        }
    }
    
    private func showScannedFileRenamePopUp(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: "Rename Scanned File".localized(), for: "Do you want to rename scanned file?".localized(), firstBtnStyle: .default, secondBtnStyle: .default, firstBtnTitle: yesText.localized(), secondBtnTitle: noText.localized()) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                self.showSaveDialog(params, data, thumbNail)
            } else {
                self.uploadForSignedId(params, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(params, data, thumbNail)
//                }
            }
        }
    }
    
    private func showSaveDialog(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: saveDocuments.localized(), for: "Enter document name".localized(), tfPlaceholder: String(Date().getTime())) { [weak self] (status, text) in
            guard let `self` = self else {return}
            if (text == nil || text!.isEmpty) {
                self.uploadForSignedId(params, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(file)
//                }
            }
            else {
                var info = params
                info.filename = text!+".pdf"
                self.uploadForSignedId(info, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(file)
//                }
            }
        }
    }
    
    private func deleteFile(index: IndexPath) {
        self.showAlert(To: "", for: deleteAlertMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                let signedId = self.attachements[index.row].signed_id
                guard let id = signedId else { return }
                RequestManager.shared.deleteFile(signedId: id, isChatMessageFile: self.isChatEdit) { (status, response) in
                    if status {
                        if let _ = response as? GeneralSuccessResult {
                            self.attachements.remove(at: index.row)
//                            self.attachmentCollectionView.deleteItems(at: [index])
                            self.attachmentCollectionView.reloadData()
                            self.didAnyUpdate = true
                            self.navigationItem.setHidesBackButton(true, animated: false)
                            self.showAlert(To: "", for: successfullyDeleted.localized()) { [weak self] _ in
                                guard let `self` = self else {return}
                                self.view.endEditing(true)
                                if self.attachements.count == 0 && self.directUploads.count == 0 {
                                    self.updatedComment?(nil)
                                    self.updatedMessage?(nil)
                                    self.dismissVC()
                                }
                            }
                        }
                        else {
                            print("Error in Deleting File")
                        }
                    }
                }
            }
        }
    }
}

// MARK: - Extension CollectionView
extension EditFileCommentVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.attachements.count + self.directUploads.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? AttachmentCell else{return AttachmentCell()}
        cell.index = indexPath
        if indexPath.row < self.attachements.count {
            let item = self.attachements[indexPath.item]
            if item.content_type!.contains("image"), let imgPath = item.src, imgPath != "" {
                if let imgUrl = URL(string: imgPath){
                    cell.cellImageView.imageFromServer(imageUrl: imgUrl, placeholder: #imageLiteral(resourceName: "attached"))
                }else {
                    cell.cellImageView.image = #imageLiteral(resourceName: "attached")
                }
            }else {
                cell.cellImageView.image = #imageLiteral(resourceName: "attached")
            }
        }
        else {
            let attachment = directUploads[indexPath.item - self.attachements.count]
            if attachment.directUploadInfo.content_type!.contains("image") || attachment.directUploadInfo.content_type!.contains("video") {
                cell.cellImageView.image = attachment.image
            }else{
                cell.cellImageView.image = #imageLiteral(resourceName: "attached")
            }
        }
        cell.delegate = self
        return cell
    }
}

//MARK: - Extension AttachmentCellDelegate
extension EditFileCommentVC : AttachmentCellDelegate {
    func didSelectToRemove(index: IndexPath) {
        if index.row < self.attachements.count {
            self.deleteFile(index: index)
        }
        else {
            let row = index.item - self.attachements.count
            if row < directUploads.count {
                self.directUploads.remove(at: row)
//                self.attachmentCollectionView.deleteItems(at: [index])
                self.attachmentCollectionView.reloadData()
                if self.attachements.count == 0 && self.directUploads.count == 0 {
                    self.updatedComment?(nil)
                    self.updatedMessage?(nil)
                    self.dismissVC()
                }
            }
        }
    }
}

extension EditFileCommentVC {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let _capturedImage = info[.originalImage] as? UIImage {
            picker.dismiss(animated: true, completion: nil)
            let capturedImage = self.imageOrientation(_capturedImage)
//            self.profileImageView.image = capturedImage
            
            var fileName = UUID().uuidString + ".jpeg"
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                fileName = url.lastPathComponent
    //          fileType = url.pathExtension
            }
            
            let _imageData = capturedImage.jpegData(compressionQuality: capturedImage.size.width > 500 ? 0.5 : 1.0) ?? Data(capturedImage.sd_imageData()!)
            let imageSize: Int = _imageData.count
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //_imageData.fileExtension
            directUploadParams.byte_size = imageSize
            
//            print("CreateThreadVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, _imageData, capturedImage)
        }
        else if let selectedVideo:URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) {
            // Save video to the main photo album
            let selectorToCall = #selector(self.videoSaved(_:didFinishSavingWithError:context:))
            
            UISaveVideoAtPathToSavedPhotosAlbum(selectedVideo.relativePath, self, selectorToCall, nil)
            // Save the video to the app directory
            var _videoData = Data()
            let videoData = try? Data(contentsOf: selectedVideo)
            let paths = NSSearchPathForDirectoriesInDomains(
                FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
            let dataPath = documentsDirectory.appendingPathComponent(videoFileName)
            
            try! videoData?.write(to: dataPath, options: [])
            
            do {
                let data = try Data(contentsOf: dataPath, options: .mappedIfSafe)
                _videoData = data
            } catch  {
                
            }
            let thumbNail = dataPath.generateThumbnail()
            let fileName = selectedVideo.lastPathComponent
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = _videoData.count
            
//            print("CreateThreadVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, _videoData, thumbNail)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
        if let theError = error {
            print("error saving the video = \(theError)")
        } else {
            DispatchQueue.main.async(execute: { () -> Void in})
        }
    }
}

//MARK: - Extension OpalImagePickerControllerDelegate
extension EditFileCommentVC {
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        print("EditFileCommentVC->imagePickerDidCancel")
    }
    
    internal func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        for assetItem in assets {
            AppConfig.shared.getURLFromAsset(mPhasset: assetItem) { (url) in
                if url != nil {
                    var fileName = url!.lastPathComponent
                    var fileData = Data()
                    do {
                        let data = try Data(contentsOf: url!, options: .mappedIfSafe)
                        fileData = data
                        if fileName.fileExtension().uppercased() == "HEIC" {
                            if let dataToImage = UIImage(data: data) {
                                fileName = String(fileName.split(separator: ".").first!).appending(".jpeg")
                                fileData = dataToImage.jpegData(compressionQuality: dataToImage.size.width > 500 ? 0.5 : 1.0) ?? data
                            }
                        }
                    } catch  {
                        print("Unable to get data of the file")
                    }
                    var thumbNail: UIImage?
                    if assetItem.mediaType == PHAssetMediaType.image {
                        if let image = AppConfig.shared.getImageFromAsset(asset: assetItem) {
                            thumbNail = image
                        }
                    }
                    else {
                        thumbNail = url!.generateThumbnail()
                    }
                    var directUploadParams = DirectUploadParams()
                    directUploadParams.filename = fileName
                    directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                    directUploadParams.byte_size = fileData.count
                    
                    self.uploadForSignedId(directUploadParams, fileData, thumbNail)
                }
                else {
                    if let img = AppConfig.shared.getAssetThumbnail(asset: assetItem) {
                        let fileName = UUID().uuidString + ".jpeg"
                        let fileData = img.jpegData(compressionQuality: img.size.width > 500 ? 0.5 : 1.0)
                        if let fileData = fileData {
                            var directUploadParams = DirectUploadParams()
                            directUploadParams.filename = fileName
                            directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                            directUploadParams.byte_size = fileData.count
                            
                            self.uploadForSignedId(directUploadParams, fileData, img)
                        }
                    }
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 0
    }
    
    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
        return URL(string: "https://placeimg.com/500/500/nature")
    }
}

//MARK: - Extension UIDocumentBrowserViewController
extension EditFileCommentVC {
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentsAt documentURLs: [URL]) {
        controller.dismiss(animated: true, completion: nil)
        guard documentURLs.count < maxAllowedAttachments else {
            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
            return
        }
        for url in documentURLs {
            let fileName = url.lastPathComponent
            var fileData = Data()
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                fileData = data
            } catch  {
                print("Unable to get data of the file")
            }
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = fileData.count
            
//            print("CreateThreadVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, fileData, nil)
        }
    }
    
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
//        guard urls.count < maxAllowedAttachments else {
//            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
//            return
//        }
//        for url in urls {
//            let fileName = url.lastPathComponent
//            var fileData = Data()
//            do {
//                let data = try Data(contentsOf: url, options: .mappedIfSafe)
//                fileData = data
//            } catch  {
//                print("Unable to get data of the file")
//            }
//            var directUploadParams = DirectUploadParams()
//            directUploadParams.filename = fileName
//            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//            directUploadParams.byte_size = fileData.count
//
////            print("CreateThreadVC->directUploadParams", directUploadParams)
//            self.uploadForSignedId(directUploadParams, fileData, nil)
//        }
//    }
//
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
//        let fileName = url.lastPathComponent
//        var fileData = Data()
//        do {
//            let data = try Data(contentsOf: url, options: .mappedIfSafe)
//            fileData = data
//        } catch  {
//            print("Unable to get data of the file")
//        }
//        var directUploadParams = DirectUploadParams()
//        directUploadParams.filename = fileName
//        directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//        directUploadParams.byte_size = fileData.count
//
////        print("CreateThreadVC->directUploadParams", directUploadParams)
//        self.uploadForSignedId(directUploadParams, fileData, nil)
//    }
//
//    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//        // Picker was cancelled! Duh 🤷🏻‍♀️
//        self.dismiss(animated: true, completion: nil)
//    }
}

// MARK: -  extension VNDocumentCameraViewControllerDelegate
extension EditFileCommentVC {
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        print(error)
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        guard scan.pageCount >= 1 else {
            controller.dismiss(animated: true)
            return
        }
        DispatchQueue.main.async {
            let pdfDocument = PDFDocument()
            for i in 0 ..< scan.pageCount {
                if  let image = scan.imageOfPage(at: i).resize(toWidth: 700) {
                    print(image)
                    // Create a PDF page instance from your image
                    let pdfPage = PDFPage(image: image)
                    // Insert the PDF page into your document
                    pdfDocument.insert(pdfPage!, at: i)
                }
            }
            // Get the raw data of your PDF document
            let data = pdfDocument.dataRepresentation()
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let docURL = documentDirectory.appendingPathComponent(Date().getTime()+".pdf")
            do{
                try data?.write(to: docURL)
                
                let fileName = docURL.lastPathComponent
                var directUploadParams = DirectUploadParams()
                directUploadParams.filename = fileName
                directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                directUploadParams.byte_size = data!.count
                
//                print("CreateThreadVC->directUploadParams", directUploadParams)
//                self.uploadForSignedId(directUploadParams, data!, nil)
                self.showScannedFileRenamePopUp(directUploadParams, data!, nil)
            }catch(let error)
            {
                print("error is \(error.localizedDescription)")
            }
        }
        controller.dismiss(animated: true)
    }
}


struct editMultiFileChatParams {
    var conversationId : String!
    var messageFirebaseId: String!
    var attachmentIds = [Int]()
    var messageIds = [Int]()
    var user_id = AppConfig.shared.userId
}
