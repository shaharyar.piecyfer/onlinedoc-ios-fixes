//
//  NewChatSelectionCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 04/11/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

protocol NewChatSelectionCellDelegate: AnyObject {
    func didTapRow(at selection: NewChatSelections)
}

class NewChatSelectionCell: UITableViewCell {
    @IBOutlet weak var uiContactSelection: UIView!
    @IBOutlet weak var uiPhoneSelection: UIView!
    @IBOutlet weak var uiEmailSelection: UIView!
    @IBOutlet weak var ivContactSelection: UIImageView!
    @IBOutlet weak var lblContactSelection: UILabel!
    @IBOutlet weak var ivPhoneSelection: UIImageView!
    @IBOutlet weak var lblPhoneSelection: UILabel!
    @IBOutlet weak var ivEmailSelection: UIImageView!
    @IBOutlet weak var lblEmailSelection: UILabel!
    @IBOutlet weak var btnContactSelection: UIButton!
    @IBOutlet weak var btnPhoneSelection: UIButton!
    @IBOutlet weak var btnEmailSelection: UIButton!
    @IBOutlet weak var uiSync: UIView!
    @IBOutlet weak var lblUnimportedCount: UILabel!
    @IBOutlet weak var imgSync: UIImageView!
    
    weak var delegate: NewChatSelectionCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnContactSelection.setTitle("", for: .normal)
        btnPhoneSelection.setTitle("", for: .normal)
        btnEmailSelection.setTitle("", for: .normal)
        lblContactSelection.text = "Import contacts".localized
        lblPhoneSelection.text = "Invite by SMS".localized
        lblEmailSelection.text = "Invite by email".localized
        uiSync.isHidden = true
        //Generic theme
        Themer.shared.register(
            target: self,
            action: NewChatSelectionCell.applyTheme)

        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        self.lblUnimportedCount.textColor = AppColors.whiteColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(selection: NewChatSelections?, count: Int) {
        uiSync.isHidden = true
        if count > 0 {
            self.uiSync.isHidden = false
            self.lblUnimportedCount.text = "\(count)"
        }
        guard let selection = selection else {
            ivContactSelection.image = UIImage(systemName: "plus.circle.fill")
            ivPhoneSelection.image = UIImage(systemName: "plus.circle.fill")
            ivEmailSelection.image = UIImage(systemName: "plus.circle.fill")
            return
        }
        switch selection {
        case .CONTACTS:
            ivContactSelection.image = UIImage(systemName: "plus.circle")
            ivPhoneSelection.image = UIImage(systemName: "plus.circle.fill")
            ivEmailSelection.image = UIImage(systemName: "plus.circle.fill")
            break
        case .PHONE:
            ivContactSelection.image = UIImage(systemName: "plus.circle.fill")
            ivPhoneSelection.image = UIImage(systemName: "plus.circle")
            ivEmailSelection.image = UIImage(systemName: "plus.circle.fill")
            break
        case .EMAIL:
            ivContactSelection.image = UIImage(systemName: "plus.circle.fill")
            ivPhoneSelection.image = UIImage(systemName: "plus.circle.fill")
            ivEmailSelection.image = UIImage(systemName: "plus.circle")
            break
        }
    }
    
    @IBAction func onClickContact(_ sender: UIButton) {
        if self.imgSync.isHidden {
            self.delegate?.didTapRow(at: .CONTACTS)
        }
        else {
            UIView.animate(withDuration:1.0, animations: {
                self.imgSync.rotate()
            }, completion: { _ in
                self.delegate?.didTapRow(at: .CONTACTS)
            })
        }
    }
    
    @IBAction func onClickPhone(_ sender: UIButton) {
        self.delegate?.didTapRow(at: .PHONE)
    }
    
    @IBAction func onClickEmail(_ sender: UIButton) {
        self.delegate?.didTapRow(at: .EMAIL)
    }
}
