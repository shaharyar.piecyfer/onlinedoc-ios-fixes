//
//  NewChatVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 04/11/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit
import Contacts

class NewChatVC: BaseVC {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    // MARK: - Variables and Instances
    var searchText = ""
    var searchPage = 0
    
    // MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //for empty result
    @IBOutlet weak var uiEmptyView: UIView!
    @IBOutlet weak var lblNoResult: UILabel!
    @IBOutlet weak var svInviteBySMS: UIView!
    @IBOutlet weak var btnInviteBySMS: UIButton!
    @IBOutlet weak var lblSMSDetail: UILabel!
    @IBOutlet weak var svInviteByEmail: UIView!
    @IBOutlet weak var btnInviteByEmail: UIButton!
    @IBOutlet weak var lblEmailDetail: UILabel!
    @IBOutlet weak var svImportContact: UIView!
    @IBOutlet weak var btnImportContact: UIButton!
    @IBOutlet weak var lblImportContactDetail: UILabel!
    
    var friends = [MemberModel]()
    var contacts = [ContactModel]()
    var otherUsers = [MemberModel]()
    var contactImported: Bool = false
    var pageNumber = 1
    var loadMore = true
    var searchedText = ""
    var localContactList = [ContactModel]()
    var selectedInviteSelection: NewChatSelections?
    var unimportedContactCount = 0
    
    var didStartNewChat: (()->())?
    var onDeleteChat: ((_ chatId: Int)->())?
    
    @objc private func refreshData(_ sender: Any) {
        self.pageNumber = 1
        self.loadMore = true
        if !self.searchedText.isEmpty {
            self.searchUsersForNewChat(searchText: self.searchedText)
        }
        else {
            self.getContacts()
        }
        self.searchBar.resignFirstResponder()
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.uiEmptyView.isHidden = true
        self.tableView.isHidden = false
        self.layOutSearchbar()
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationItem.title = "Pick a Recipient".localized()
        
        self.setupTheme()
        self.setupNotificationObservers()
    }
    
    private func setupNotificationObservers() {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            if self.searchedText.isEmpty {
                self.getContacts()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
        if self.searchedText.isEmpty {
            self.getContacts()
        }
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: NewChatVC.applyTheme)

        Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if appDelegate.attachements.count > 0 {
            appDelegate.attachements.removeAll()
        }
    }
    
    deinit {
        print("NewChatVC->deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    func layOutSearchbar(){
        self.searchBar.placeholder = "Search name, phonenumber or email".localized
        self.searchBar.delegate = self
        self.searchBar.becomeFirstResponder()
    }
    
    // MARK: - Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    //MARK: - APIs
    func getContacts() {
        if self.pageNumber == 1 {
            self.contacts.removeAll()
        }
        self.tableView.setEmptyMessage("")
        self.uiEmptyView.isHidden = true
        self.tableView.isHidden = false
        RequestManager.shared.getContacts(page: self.pageNumber) { (status, response) in
            if status {
                if let res = response as? ContactListResponse {
                    self.contacts.append(contentsOf: res.contacts)
                    if res.contacts.count == 0 {
                        self.loadMore = false
                    }
                    if !res.contacts.isEmpty && self.pageNumber == 1 && AppConfig.shared.getIsContactPermissionShows() == false {
                        self.checkContactPermission(onlyForDenied: true)
                    }
                    self.selectedInviteSelection = self.contacts.isEmpty ? nil : .CONTACTS
                    self.tableView.reloadData()
                    DispatchQueue.main.async {
                        self.fetchContacts()
                    }
                }
            }
        }
    }
    
    func searchUsersForNewChat(searchText: String){
        if self.pageNumber == 1 {
            self.friends.removeAll()
            self.contacts.removeAll()
            self.otherUsers.removeAll()
        }
        self.tableView.setEmptyMessage("")
        self.uiEmptyView.isHidden = true
        self.tableView.isHidden = false
        RequestManager.shared.searchNewChatUsers(page: self.pageNumber, searchText: searchText) { (status, response) in
            if status {
                if let res = response as? NewChatSearchResponse {
                    self.contactImported = res.contacts_imported ?? false
                    if res.friends.isEmpty && res.other_users.isEmpty && res.contacts.isEmpty && self.pageNumber == 1 && self.contactImported {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                    }
                    if self.pageNumber == 1 {
                        self.contacts = res.contacts
                        self.friends = res.friends
//                        if !searchText.isEmpty {
//                            self.contacts = res.contacts.filter({ (contact) -> Bool in
//                                return (contact.contact_name?.lowercased().contains(searchText.lowercased()) ?? false) || (contact.contact_email?.lowercased().contains(searchText.lowercased()) ?? false) || (contact.mobile_no?.contains(searchText) ?? false)
//                            })
//                            self.friends = res.friends.filter {
//                                guard $0.phone != nil else {
//                                    return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email)!.lowercased().contains(searchText.lowercased())
//                                }
//                                return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email)!.lowercased().contains(searchText.lowercased()) || ($0.phone)!.lowercased().contains(searchText.lowercased())
//
//                            }
//                        } else {
//                            self.contacts = res.contacts
//                            self.friends = res.friends
//                        }
                    }
                    self.otherUsers.append(contentsOf: res.other_users)
                    if res.other_users.count == 0 {
                        self.loadMore = false
                    }
                    if !searchText.isEmpty && self.contacts.isEmpty && self.friends.isEmpty && self.otherUsers.isEmpty {
                        self.setupEmptyView()
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func navigateToChat(user: MemberModel){
        if let vc = getVC(storyboard: .CONNECTIOS, vcIdentifier: "ChatScene") as? ChatVC {
            if let roomId = user.chatRoomId {
                vc.chatId = roomId
            }
            else {
                var receiver = User()
                receiver.id = user.user_id
                receiver.full_name = user.full_name
                vc.receiver = receiver
            }
            if appDelegate.attachements.count > 0 {
                vc.attachements = appDelegate.attachements
            }
            vc.onDismiss = { [weak self] in
                guard let `self` = self else {return}
                self.didStartNewChat?()
                self.dismissVC()
            }
            vc.onDeleteChat = { [weak self] chatId in
                guard let `self` = self else {return}
                self.onDeleteChat?(chatId)
            }
            self.navigateVC(vc)
        }
    }
    
    private func fetchContacts(importContact: Bool = false) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let `self` = self else { return }
            self.localContactList = PhoneBookContactManager.shared.getAllPhoneContactList()
            if !self.contacts.isEmpty && !self.localContactList.isEmpty {
                let result = self.localContactList.filter { lContact in
                    !self.contacts.contains { imported in
                        imported.mobile_no == lContact.mobile_no
                    }
                }
                self.unimportedContactCount = result.count
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    self.tableView.reloadSections([0], with: .none)
                    if importContact && result.count > 0 {
                        self.importContacts(contacts: result)
                    }
                }
            }
            else if importContact && self.localContactList.count > 0 {
                self.importContacts(contacts: self.localContactList)
            }
            else {
                if !self.contacts.isEmpty {
                    self.unimportedContactCount = self.localContactList.count
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else { return }
                        self.tableView.reloadSections([0], with: .none)
                        if importContact && self.localContactList.count > 0 {
                            self.importContacts(contacts: self.localContactList)
                        }
                    }
                }
            }
        }
    }
    
    private func importContacts(contacts: [ContactModel]) {
        RequestManager.shared.importContacts(contacts: contacts) { [weak self] (status, response) in
            guard let `self` = self else {return}
            if status {
                if let _ = response as? GeneralSuccessResult {
                    if self.searchedText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                        self.showAlert(To: "", for: "Contact Imported") {_ in}
                        self.getContacts()
                    }
                    else {
                        self.pageNumber = 1
                        self.loadMore = true
                        self.searchUsersForNewChat(searchText: self.searchedText)
                    }
                }
            }
        }
    }
    
    @IBAction func onClickNoResultBtn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            self.checkContactPermission()
        case 1:
            self.showInvitePopup(inviteBy: .PHONE)
        case 2:
            self.showInvitePopup(inviteBy: .EMAIL)
        default:
            break
        }
    }
    
    private func setupEmptyView() {
        self.uiEmptyView.isHidden = false
        self.tableView.isHidden = true
        self.lblNoResult.text = NoResultNewChat.localized()
        self.lblNoResult.font = AppFont.kMuliLight(14).font()
        self.lblImportContactDetail.text = "txt_import_contact_detail".localized()
        self.btnImportContact.setTitle("Import Contacts".localized(), for: .normal)
        self.btnImportContact.titleLabel?.font = AppFont.kMuliBold(14).font()
        self.lblSMSDetail.text = InviteBySMS.localized()
        self.btnInviteBySMS.setTitle("Invite by SMS".localized(), for: .normal)
        self.btnInviteBySMS.titleLabel?.font = AppFont.kMuliBold(14).font()
        self.lblEmailDetail.text = InviteByEmail.localized()
        self.btnInviteByEmail.setTitle("Invite by email".localized(), for: .normal)
        self.btnInviteByEmail.titleLabel?.font = AppFont.kMuliBold(14).font()
        
        if searchedText.starts(with: "+") {
            searchedText = String(searchedText.dropFirst(3))
        }
        if self.searchedText.isDigits {
            self.svImportContact.isHidden = true
            self.svInviteBySMS.isHidden = false
            self.svInviteByEmail.isHidden = true
        }
        else if self.searchedText.isValidEmail {
            self.svImportContact.isHidden = true
            self.svInviteBySMS.isHidden = true
            self.svInviteByEmail.isHidden = false
        }
        else {
            self.svImportContact.isHidden = false
            self.svInviteBySMS.isHidden = false
            self.svInviteByEmail.isHidden = false
        }
    }
    
    private func showInvitePopup(inviteBy: NewChatSelections) {
        if let vc = self.getVC(storyboard: .ALERT, vcIdentifier: String(describing: InviteAlertVC.self)) as? InviteAlertVC {
            vc.inviteBy = inviteBy
            if !self.uiEmptyView.isHidden {
                if inviteBy == .EMAIL {
                    vc.searchedEmail = self.searchedText.isValidEmail ? self.searchedText : ""
                }
                else if inviteBy == .PHONE {
                    vc.searchedPhone = self.searchedText.isDigits ? self.searchedText : ""
                }
            }
            vc.onClickDone = { [weak self] (data, inviteBy) in
                guard let `self` = self else {return}
                if data.isEmpty {return}
                self.callAPICheckChatRoom(by: data, name: "", inviteBy: inviteBy)
                
            }
            vc.onClickCancel = { [weak self] in
                guard let `self` = self else {return}
                self.selectedInviteSelection = self.contacts.isEmpty ? nil : .CONTACTS
                self.tableView.reloadSections([0], with: .automatic)
            }
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    private func callAPICheckChatRoom(by: String, name: String, inviteBy: NewChatSelections) {
        RequestManager.shared.checkChatRoom(by: by) { [weak self] (status, response) in
            guard let `self` = self else {return}
            if status {
                if let data = response as? CheckChatRoomResult {
                    if (data.chat_room_exists == true && data.system_user == true) {
                        self.callAPICreateVirtualUser(by: by, name: name, inviteBy: inviteBy, do_not_send_message: true)
                    } else if (data.chat_room_exists == false && data.system_user == false) {
                        self.showAlert(To: "Alert".localized(), for: NewChatAlert.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
                            guard let `self` = self else {return}
                            if status {
                                self.callAPICreateVirtualUser(by: by, name: name, inviteBy: inviteBy)
                            }
                        }
                    } else if (data.system_user == true && data.chat_room_exists == false) {
                        var receiver = MemberModel()
                        receiver.user_id = data.user_id
                        receiver.full_name = data.user_name
                        self.navigateToChat(user: receiver)
                    } else if (data.chat_room_exists == true && data.system_user == false) {
                        self.callAPICreateVirtualUser(by: by, name: name, inviteBy: inviteBy, do_not_send_message: true)
                    }
                }
            }
        }
    }
    
    private func callAPICreateVirtualUser(by: String, name: String, inviteBy: NewChatSelections, do_not_send_message: Bool = false) {
        RequestManager.shared.createVirtualUser(by: by, name: name, isEmail: inviteBy == .EMAIL, do_not_send_message: do_not_send_message) { [weak self] (status, response) in
            guard let `self` = self else {return}
            if status {
                if let data = response as? InvitedChatRoomResult, let chatRoom = data.result {
                    var user = MemberModel()
                    user.user_id = chatRoom.user_id
                    user.chatRoomId = chatRoom.chat_room_id
                    user.full_name = chatRoom.email ?? chatRoom.phone
                    
                    self.navigateToChat(user: user)
                }
            }
        }
    }
}

//MARK: - EXTENSIONS
extension NewChatVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.resignFirstResponder()
            self.searchBar.textField?.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.pageNumber = 1
            self.loadMore = true
            self.searchedText = ""
            self.uiEmptyView.isHidden = true
            self.tableView.isHidden = false
            self.getContacts()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
        let txt = searchBar.text?.trim ?? ""
        self.searchedText = txt
        self.searchUsersForNewChat(searchText: txt)
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
        self.searchedText = ""
    }
}

extension NewChatVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.searchedText.isEmpty ? 1 : 0
        case 1:
            return self.searchedText.isEmpty ? 0 : self.friends.count
        case 2:
            return self.searchedText.isEmpty ? self.contacts.count : ((!self.contactImported && !self.searchedText.isEmpty) ? 1 : self.contacts.count)
        case 3:
            return self.searchedText.isEmpty ? 0 : self.otherUsers.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewChatSelectionCell.self)) as? NewChatSelectionCell else { return self.emptyTblCell() }
            cell.uiEmailSelection.isHidden = false
            cell.uiPhoneSelection.isHidden = false
            cell.configCell(selection: self.selectedInviteSelection, count: self.unimportedContactCount)
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else { return self.emptyTblCell() }
            
            if indexPath.row < friends.count {
                cell.configCell(indexPath: indexPath, data: friends[indexPath.row])
            }
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.section == 2 {
            if !self.contactImported && !self.searchedText.isEmpty {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewChatSelectionCell.self)) as? NewChatSelectionCell else { return self.emptyTblCell() }
                cell.uiEmailSelection.isHidden = true
                cell.uiPhoneSelection.isHidden = true
                cell.configCell(selection: self.selectedInviteSelection, count: 0)
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }
            else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else { return self.emptyTblCell() }
                if indexPath.row < contacts.count {
                    cell.configCell(indexPath: indexPath, data: contacts[indexPath.row])
                }
                cell.selectionStyle = .none
                return cell
            }
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else { return self.emptyTblCell() }
            
            if indexPath.row < otherUsers.count {
                cell.configCell(indexPath: indexPath, data: otherUsers[indexPath.row])
            }
            if self.loadMore && indexPath.row == (self.otherUsers.count - 2){
                self.pageNumber += 1
                self.searchUsersForNewChat(searchText: searchBar.text?.trim ?? "")
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:
            let contact = self.contacts[indexPath.row]
            if let phone = contact.mobile_no {
                var desiredNumber = ""
                if phone.starts(with: "+") {
                    desiredNumber = "+" + phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
                }
                else {
                    desiredNumber = "+45" + phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression).suffix(8)
                }
                
                self.callAPICheckChatRoom(by: desiredNumber, name: contact.contact_name ?? "", inviteBy: .PHONE)
            }
        case 1,3:
            let user = indexPath.section == 1 ? friends[indexPath.row] : otherUsers[indexPath.row]
            self.navigateToChat(user: user)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        case 1:
            return self.searchedText.isEmpty ? 0 : (self.friends.isEmpty ? 0 : 50)
        case 2:
            return self.searchedText.isEmpty ? (self.contacts.isEmpty ? 0 : 50) : (self.contacts.isEmpty ? (self.contactImported ? 0 : 50) : 50)
        case 3:
            return self.searchedText.isEmpty ? 0 : (self.otherUsers.isEmpty ? 0 : 50)
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !self.contactImported && !self.searchedText.isEmpty && indexPath.section == 2 {
            return 50
        }
        else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = AppConfig.shared.isDarkMode ? AppColors.darkGrayColor : AppColors.mediumGrayColor
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = section == 1 ? "Friends".localized().uppercased() : (section == 2 ? "Contacts".localized().uppercased() : (section == 3 ? "Other Users".localized().uppercased() : ""))
        label.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
        headerView.addSubview(label)
        return headerView
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: NewChatSelectionCell.self), bundle: nil), forCellReuseIdentifier: String(describing: NewChatSelectionCell.self))
        self.tableView.register(UINib.init(nibName: String(describing: GeneralViewCell.self), bundle: nil), forCellReuseIdentifier: "GeneralCell")
        
        self.tableView.separatorStyle = .none
        
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
        if #available(iOS 15.0, *) {
            self.tableView.sectionHeaderTopPadding = 0.0
        }
    }
}

extension NewChatVC: NewChatSelectionCellDelegate {
    func didTapRow(at selection: NewChatSelections) {
        self.selectedInviteSelection = selection
        switch selection {
        case .CONTACTS:
            if !self.contactImported {
                self.checkContactPermission()
            }
            break
        case .PHONE:
            self.showInvitePopup(inviteBy: .PHONE)
            self.tableView.reloadSections([0], with: .automatic)
            break
        case .EMAIL:
            self.showInvitePopup(inviteBy: .EMAIL)
            self.tableView.reloadSections([0], with: .automatic)
            break
        }
    }
    
    func checkContactPermission(onlyForDenied: Bool = false) {
        let cab = CNContactStore.init()
        cab.requestAccess(for: CNEntityType.contacts) { (granted, error) in
            if(granted){
                if onlyForDenied {return}
                DispatchQueue.main.async {
                    self.fetchContacts(importContact: true)
                }
            }else{
                DispatchQueue.main.async {
                    if onlyForDenied {
                        AppConfig.shared.saveContactPermissionShow()
                    }
                    self.showAlert(To: "permission_required".localized(), for: "contact_permission".localized(), firstBtnStyle: .default, secondBtnStyle: .default, firstBtnTitle: "settings".localized(), secondBtnTitle: cancelText.localized()) { status in
                        if status {
                            if let url = URL(string: UIApplication.openSettingsURLString) {
                                UIApplication.shared.open(url, options: [:], completionHandler: { _ in })
                            }
                        }
                    }
                }
            }
        }
    }
}
