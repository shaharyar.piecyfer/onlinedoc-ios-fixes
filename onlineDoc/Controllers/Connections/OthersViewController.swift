//
//  OthersViewController.swift
//  onlineDoc
//
//  Created by Piecyfer on 20/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//
/*
import UIKit

//protocol OthersViewControllerDelegate: class {
//    func didChangeChat(count :Int)
//}

class OthersViewController: BaseVC {
    @IBOutlet weak var friendRequestsTableView: UITableView!{
        didSet{
            friendRequestsTableView.register(UINib(nibName: "RequestViewCell", bundle: Bundle.main), forCellReuseIdentifier: "RequestCell")
        }
    }

    @IBOutlet weak var friendSearchBar:UISearchBar!{
        didSet{
            friendSearchBar.placeholder = SearchText.localized
            self.layOutSearchbar(searchBar: friendSearchBar)
        }
    }

    
    var groupRequests = [GroupJoinRequest](){
        didSet{
            self.friendRequestsTableView.reloadData()
        }
    }
    var groupRequestsSource = [GroupJoinRequest](){
        didSet{
            self.groupRequests = groupRequestsSource
        }
    }
    
    public typealias UserProfileCallBack = (_ index:Int) -> Void
    var callbacks : UserProfileCallBack?
    var friendRequests = [Friend ]()
    var friendRequestsSource = [Friend](){
        didSet{
            self.friendRequests = friendRequestsSource
        
        }
    }
    
    var chatById = ""
    var personName = ""
    var personProfession = ""
    var selectedRequest = DirectMessage()
//    weak var delegate : OthersViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.onlineUserList()
        self.checkSystemMembers()
        self.checkRequest()
        NotificationCenter.default.removeObserver(self)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadUpdatedData),
                                               name: ._FriendRequests,
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.loadFriendRequests()
        self.groupJoiningRequests()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        FirebaseManager.shared.removeChatObserver()
    }
    
    @objc func reloadUpdatedData(_ notification:Notification){
        
        if let requests = notification.userInfo?["requests"] as? [Friend]{
             self.friendRequestsSource = requests
        }
        self.friendRequestsTableView.reloadData()
    }
    
    func layOutSearchbar(searchBar: UISearchBar){
        searchBar.delegate = self
        searchBar.backgroundImage = UIImage()
        searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
        if let searchTextField = searchBar.textField{
//            searchTextField.borderStyle = .none
            searchTextField.textColor = .white
//            searchTextField.borderStyle = .none
//            searchTextField.setBottomBorder()
//            searchTextField.background = UIImage()
        }
    }
    
    func checkSystemMembers(){
        if !(AppConfig.shared.systemUsersList.count > 0) {
            self.systemMembers()
        }else{
            self.checkRequest()
        }
    }
    
    func gotoUserProfile(_ index:Int, isGroupRequest: Bool = false){
        if !isGroupRequest{
            let frndRequest = self.friendRequests[index]
            let user = AppConfig.shared.systemUsersList.filter{$0.id == frndRequest.IntId}.last
            if let firebase = user?.userFireBaseId{
                self.previewUser(profile: frndRequest.IntId, with: firebase)
            }
        }else{
            let grpRequest = self.groupRequests[index]
            if let id = grpRequest.userId{
                let user = AppConfig.shared.systemUsersList.filter{$0.id == id}.last
                if let firebase = user?.userFireBaseId{
                    self.previewUser(profile: id, with: firebase)
                }
            }
        }
    }
    
//MARK: - Firebase Requests
    
    func checkRequest(){
        if AppConfig.shared.requestListFirebase.count > 0  {
            self.friendRequestsSource = AppConfig.shared.requestListFirebase
            self.friendRequestsTableView.reloadData()
        } else{
            self.loadFriendRequests()
        }
    }
    
    func loadFriendRequests(){
        //TODO get
        guard let userFirebaseId = AppConfig.shared.userFirebaseId else {
            print("Unable to get the user's firebase Id")
            return
        }
        FirebaseManager.shared.userRequestsList(against: userFirebaseId)
    }
    
    func onlineUserList(){
          if !(AppConfig.shared.onlineUsersList.count > 0){
              self.getOnlineUserList()
          }
      }
      
      func getOnlineUserList(){
          FirebaseManager.shared.activeStatusList { (status, response) in
              if status{
                  if let result = response as? [OnlineStatus]{
                      AppConfig.shared.onlineUsersList = result
                  }
              }
          }
      }
    
    ///to get memberlist from firebase
    
    func firebaseMembersList() {
        FirebaseManager.shared.getUsersList { (status, response) in
            if status{
                if let data = response as? [CommentorProfile]{
                    if data.count > 0{
                        AppConfig.shared.firebaseUsersList = data
                    }
                }
            }
        }
    }
    
    //MARK: - Network Requests
    
    func systemMembers(){
        RequestManager.shared.systemMembers{ (status, response) in
            if status{
                if let data = response as? GroupMembersResponse{
                    if let members = data.result?.members
                    {
                        AppConfig.shared.systemUsersList = members
                         self.loadFriendRequests()
                    }
                }
            }
        }
    }
    
//    func groupJoiningRequests(){
//        RequestManager.shared.groupRequestsList { (status, response) in
//            if status{
//                if let res = response as? GroupRequestResponse{
//                    if let adminGroups = res.result?.data?.adminGroupUsers{
//                        self.groupRequestsSource = adminGroups
//                    }else if let joinedGroups = res.result?.data?.memberGroupUsers{
//                        self.groupRequestsSource.append(contentsOf: joinedGroups)
//                        self.groupRequests = self.groupRequestsSource
//                    }
//                    self.groupRequests = self.groupRequestsSource
//                }
//            }
//        }
//    }
    
    func handleGroupRequest(_ params : GroupRequestParam){
        RequestManager.shared.acceptGroupRequest(of: params) { (status, response) in
            if status{
                if let res = response as? GeneralSuccessResponse{
                    if res.result != nil{
                        self.showAlert(To: GroupRequestAccept.localized(), for: GrouopRequestAcceptMessage.localized()) { (ignor) in
                            self.groupJoiningRequests()
                        }
                    }
                }
            }
        }
    }
    
    func handleAcceptFriendRequest(of id: Int, at index: Int){
        RequestManager.shared.acceptFriendRequest(of: id) { (status, response) in
            if status{
                if let res = response as? GeneralSuccessResponse{
                    if res.result != nil{
                        self.showAlert(To: GroupRequestAccept.localized(), for: GrouopRequestAcceptMessage.localized()) { (ignor) in
                            if self.friendRequests.indices.contains(index){
//                                self.friendRequests.remove(at: index)
                                self.friendRequestsTableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func handleCancelFriendRequest(with params : FriendRequestParams, at index: Int) {
        RequestManager.shared.cancelFriendRequest(of: params) { (status, response) in
            if status{
                if let res = response as? GeneralSuccessResponse{
                    if res.result != nil{
                        self.showAlert(To: GroupRequestAccept.localized(), for: GrouopRequestAcceptMessage.localized()) { (ignor) in
                            if self.friendRequests.indices.contains(index){
//                                self.friendRequests.remove(at: index)
                                self.friendRequestsTableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueToRequestChat"{
            let dvc = segue.destination as? ChatVC
            dvc?.fireBaseId = self.chatById
            dvc?.request = self.selectedRequest
            self.selectedRequest = DirectMessage()
            dvc?.tab = "requests"
        }
    }
    
}

//MARK: -EXTENSIONS
extension OthersViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.friendRequests.count + self.groupRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let requestsCount = self.friendRequests.count
        if indexPath.row < requestsCount{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as? RequestViewCell else{
                    print("The dequeued cell is not an instance of Others View Controller.")
                    return UITableViewCell()
                }
                
                var recordId = 0
                /// record from firese can either be a string or can be int. In order to use the id for future use Conversion to int is performed accordingly
                if self.friendRequests[indexPath.row].id != nil{
                    if let id = self.friendRequests[indexPath.row].id as? Int{
                        recordId = id
                    }else  if let id = self.friendRequests[indexPath.row].id as? String{
                        recordId = Int(id)!
                    }else{
                        print("Id of Different Type")
                    }
                }
                self.friendRequests[indexPath.row].IntId = recordId
                
                let userStatus = AppConfig.shared.onlineUsersList.filter{$0.id == self.friendRequests[indexPath.row].IntId}.last
            
                
                if self.friendRequests[indexPath.row].onlineStatus == nil{
                    self.friendRequests[indexPath.row].onlineStatus = OnlineStatus()
                }
                self.friendRequests[indexPath.row].onlineStatus = userStatus
                
                if self.friendRequests[indexPath.row].messageBy == nil{
                    self.friendRequests[indexPath.row].messageBy = GroupMember()
                    self.friendRequestsSource[indexPath.row].messageBy = GroupMember()
                    let member = AppConfig.shared.systemUsersList.filter {$0.id == recordId}.first
                    
                    self.friendRequests[indexPath.row].messageBy = member
                    self.friendRequestsSource[indexPath.row].messageBy = member
                    
                }
                
                cell.setFriendRequestData(indexPath, data: friendRequests[indexPath.row])
                cell.delegate = self
                cell.profile { (index) in
                    self.gotoUserProfile(index)
                }
                return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as? RequestViewCell else{
                print("The dequeued cell is not an instance of Others View Controller.")
                return UITableViewCell()
            }
            let indxPth = IndexPath(row: indexPath.row - requestsCount, section: 0)
            cell.setGroupRequestData(indxPth, data: self.groupRequests[indexPath.row - requestsCount])
            cell.delegate = self
            cell.profile { (index) in
                self.gotoUserProfile(index, isGroupRequest: true)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == friendRequestsTableView{
            if self.friendRequests.indices.contains(indexPath.row){
                if let firebaseId = self.friendRequests[indexPath.row].firebase_coll_name{
                    self.chatById = firebaseId
                    
                    let userStatus = AppConfig.shared.onlineUsersList.filter{$0.id == self.friendRequests[indexPath.row].IntId}.last
                    if self.friendRequests[indexPath.row].onlineStatus == nil{
                        self.friendRequests[indexPath.row].onlineStatus = OnlineStatus()
                    }
                    self.friendRequests[indexPath.row].onlineStatus = userStatus
                    self.selectedRequest = self.friendRequests[indexPath.row]
                    self.performSegue(withIdentifier: "SegueToRequestChat", sender: self)
                }
            }
        }
    }
}

extension OthersViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            self.friendRequests = self.friendRequestsSource
            self.friendRequestsTableView.reloadData()
            return
        }
        self.friendRequests = self.friendRequestsSource.filter({ person -> Bool in
            (person.messageBy != nil ? (person.messageBy?.name)!.lowercased().contains(searchText.lowercased()) : false)
        })
        self.groupRequests = self.groupRequestsSource.filter({ request -> Bool in
            (request.name != nil ? (request.name)!.lowercased().contains(searchText.lowercased()) : false)
        })
        self.friendRequestsTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar == friendSearchBar{
            self.groupRequests = self.groupRequestsSource
        }else{
            self.friendRequests = self.friendRequestsSource
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar == friendSearchBar{
            self.groupRequests = self.groupRequestsSource
        }else{
            self.friendRequests = self.friendRequestsSource
        }
    }
}

extension OthersViewController  : RequestViewCellDelegate{
    
    func didTapRequestButton(index: Int, isFriendThing: Bool, isCancelRequest: Bool) {
        if !isCancelRequest{
            /// HANDLE THE ACCEPT REQUEST  HERE
            if isFriendThing{
                /// Handle Accept Friend Request Here
                self.handleAcceptFriendRequest(of: self.friendRequests[index].IntId, at: index)
                
            }else{
                /// Handle Accept GroupRequest Here
                var groupRequestParams = GroupRequestParam()
                groupRequestParams.user_id = self.groupRequests[index].userId
                groupRequestParams.clinic_id = self.groupRequests[index].clinicId
                groupRequestParams.type = "accept"
                groupRequestParams.clinic_user_id = self.groupRequests[index].clinicUserId
                self.handleGroupRequest(groupRequestParams)
            }
        }else{
            /// HANDLE THE CANCLE REQUEST  HERE
            if isFriendThing{
                /// Handle Friend Request Cancel Here
                var cancelRequestParams = FriendRequestParams()
                let data = friendRequests[index]
                if data.request_status == 0 {
                    //I have sent the request and now I want to cancel the request
                    cancelRequestParams.friend_id = String(self.friendRequests[index].intFrendId)
                    cancelRequestParams.conversation_id = self.friendRequests[index].conversation_id
                    cancelRequestParams.receiver_id = self.friendRequests[index].IntId
                    self.handleCancelFriendRequest(with: cancelRequestParams, at: index)
                }else{
                    //user wants to reject the request
                    cancelRequestParams.friend_id = String(self.friendRequests[index].intFrendId)
                    cancelRequestParams.conversation_id = self.friendRequests[index].conversation_id
                    self.handleCancelFriendRequest(with: cancelRequestParams, at: index)
                }
            }else{
                /// Handle Group Request Cancel Here
                var  cancelgroupRequestParams = GroupRequestParam()
                cancelgroupRequestParams.user_id = self.groupRequests[index].userId
                cancelgroupRequestParams.clinic_id = self.groupRequests[index].clinicId
                cancelgroupRequestParams.type = "reject"
                cancelgroupRequestParams.clinic_user_id = self.groupRequests[index].clinicUserId
                self.handleGroupRequest(cancelgroupRequestParams)
            }
        }
    }
    
    func didTapRequestFriendship(index: Int) {
//        guard let memberId = self.friendRequests[index].id as? Int else{print("Unable to get the member id to send request "); return}
        let memberId = self.friendRequests[index].IntId
        RequestManager.shared.sendRequest(userId: memberId) { (status, response) in
            if status{
                if (response as? ResetPasswordSuccessResponse) != nil{
                    self.showAlert(To: FriendRequestTitle.localized(), for: FriendRequestMessage.localized()) { (status) in
                    }
                }
            }
        }
    }
}
*/
