//
//  ChatPickRecipientsVV.swift
//  onlineDoc
//
//  Created by Piecyfer on 31/08/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class ChatPickRecipientsVC: BaseVC {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //    MARK: -  Structs
    
    //    MARK: - Variables and Instances
    var searchText = ""
    var searchPage = 0
//    var isSearch = false
//    var usersAll = [Member]()
//    var selectedMember = Member()
    var personName = ""
    var selectedIndex = -1
    
    // MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var friends = [MemberModel]()
    var otherUsers = [MemberModel]()
    var pageNumber = 1
    var loadMore = true
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.refreshUserList(_:)),
                                 for: .valueChanged)
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = .white
        return refreshControl
    }()
    
    @objc private func refreshUserList(_ sender: Any) {
        self.pageNumber = 1
        self.searchBar.text = ""
        self.loadMore = true
        self.loadUsersAndFriendList()
        self.searchBar.resignFirstResponder()
        refreshControl.endRefreshing()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.loadUsersAndFriendList()
        self.layOutSearchbar()
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationItem.title = "Pick a Recipient".localized()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if appDelegate.attachements.count > 0 {
            appDelegate.attachements.removeAll()
        }
    }
    
    func layOutSearchbar(){
        self.searchBar.placeholder = SearchText.localized
        self.searchBar.delegate = self
        self.searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
        if let searchTextField = self.searchBar.textField{
            searchTextField.textColor = .white
            searchTextField.keyboardAppearance = .dark
        }
    }
    
    func didTapSendMessage(toUser at: Int) {
//        if at == -1{
//            self.navigationItem.rightBarButtonItem?.title = ""
//            self.navigationItem.rightBarButtonItem?.isEnabled = false
//            return
//        }
//        var member = Member()
//        member = self.usersAll[at]
//        self.selectedMember = member
//        if member.userId == nil{
//            self.selectedMember.userId = member.id
//        }
//
//        self.sendMessage(userAt: at)
        //        The above statement is working fine both for friends and direct messages..... Dont Know Why !!!
        
        /**************************************************************************************************************/
        /*
         if member.status == nil {   // this check means to send friend a message
         self.sendMessage(userAt: at)
         }
         else { // for sending message first time to a user
         if (self.selectedMember.userId != nil) {
         self.checkDirectMessaging(self.selectedMember.userFirebaseId ?? "", userId: self.selectedMember .userId ?? 0, name: self.selectedMember.name ?? "", index: at)
         }
         else {
         print("userID is nil in the Model Member.")
         }
         }
         */
        /**************************************************************************************************************/
    }
    
    func sendMessage(userAt index :Int) {
//        if let firebaseId = self.selectedMember.userFirebaseId,  let userID = self.selectedMember.id, let name  = self.selectedMember.name?.capitalized{
//            FirebaseManager.shared.getLastMessageWithInt(for: AppConfig.shared.userFirebaseId, id: userID) { (status, response) in
//                if status{
//                    if response != nil{
//                        let res = response as? RecentMessage
//                        if let firebaseId = res?.firebase_coll_name{
//                            var friend = Friend()
//                            friend.userInfo = GroupMember()
//                            friend.userInfo?.name = name
//                            friend.userId = userID
//                            //                                friend.userFirebaseId = firebaseId
//                            friend.userInfo?.profilePicture = self.selectedMember.profilePicture
//                            //                                friend.userInfo?.userFirebaseId = self.selectedMember.userFirebaseId
//                            //                                if friend.recentMessage == nil{
//                            //                                    friend.recentMessage = RecentMessage()
//                            //                                }
//                            friend.friend_id = res?.friend_id
//                            //                                friend.recentMessage?.friend_id = res?.friend_id
//                            self.chat(for: firebaseId, name: name, userID: userID, isFriend: true, friend: friend, request: nil)
//                        }
//                    }else{
//                        self.checkDirectMessaging(AppConfig.shared.userFirebaseId, userId: userID, name: name, index: index)
//                    }
//                }else{
//                    FirebaseManager.shared.getLastMessageWithString(for: AppConfig.shared.userFirebaseId, id: String(userID)) { (status, response) in
//                        if status{
//                            if response != nil{
//                                let res = response as? RecentMessage
//                                var friend = Friend()
//                                friend.userInfo = GroupMember()
//                                friend.userInfo?.name = name
//                                friend.userId = userID
//                                //                                friend.userFirebaseId = firebaseId
//                                friend.userInfo?.profilePicture = self.selectedMember.profilePicture
//                                //                                friend.userInfo?.userFirebaseId = self.selectedMember.userFirebaseId
//                                //                                if friend.recentMessage == nil{
//                                //                                    friend.recentMessage = RecentMessage()
//                                //                                }
//                                friend.friend_id = res?.friend_id
//                                //                                friend.recentMessage?.friend_id = res?.friend_id
//                                if let firebaseId = res?.firebase_coll_name{
//                                    self.chat(for: firebaseId, name: name, userID: userID, isFriend: true, friend: friend, request: nil)
//                                }
//                            }else{
//                                self.checkDirectMessaging(AppConfig.shared.userFirebaseId, userId: userID, name: name, index: index)
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }
    
    //    MARK: - Selector Methods
    @objc func oKButtonTapped(){
//        didTapSendMessage(toUser: selectedIndex)
    }
    
    //    MARK: - Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    //MARK: - APIs
    func loadUsersAndFriendList(searchText: String = ""){
        if self.pageNumber == 1 {
            self.friends.removeAll()
            self.otherUsers.removeAll()
        }
        self.tableView.setEmptyMessage("")
        RequestManager.shared.getfriendsWithOtherUsers(page: self.pageNumber, searchText: searchText) { (status, response) in
            if status {
                if let res = response as? FriendListResponse {
                    if res.friends.isEmpty && res.other_users.isEmpty && self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                    }
                    if self.pageNumber == 1 {
                        if !searchText.isEmpty {
                            self.friends = res.friends.filter {
                                guard $0.phone != nil else {
                                    return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email)!.lowercased().contains(searchText.lowercased())
                                }
                                return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email)!.lowercased().contains(searchText.lowercased()) || ($0.phone)!.lowercased().contains(searchText.lowercased())
                                
                            }
                        } else {
                            self.friends = res.friends
                        }
                    }
                    self.otherUsers.append(contentsOf: res.other_users)
                    if res.other_users.count == 0 {
                        self.loadMore = false
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
}

//MARK: - EXTENSIONS
extension ChatPickRecipientsVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.resignFirstResponder()
            self.searchBar.textField?.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.pageNumber = 1
            self.loadMore = true
            self.loadUsersAndFriendList()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
        let txt = searchBar.text?.trim ?? ""
        self.loadUsersAndFriendList(searchText: txt)
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
    }
}

extension ChatPickRecipientsVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.friends.count
        case 1:
            return self.otherUsers.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else { return self.emptyTblCell() }
//        cell.index = indexPath
        if indexPath.section == 0 {
            if indexPath.row < friends.count {
                cell.configCell(indexPath: indexPath, data: friends[indexPath.row])
            }
        }
        else {
            if indexPath.row < otherUsers.count {
                cell.configCell(indexPath: indexPath, data: otherUsers[indexPath.row])
            }
            if self.loadMore && indexPath.row == (self.otherUsers.count - 2){
                self.pageNumber += 1
                self.loadUsersAndFriendList(searchText: searchBar.text?.trim ?? "")
            }
        }
        cell.cellCheckBox.isUserInteractionEnabled = false
        cell.cellCheckBox.boxType = .square
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        didTapSendMessage(toUser: indexPath.row)
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: GeneralViewCell.self), bundle: nil), forCellReuseIdentifier: "GeneralCell")
        
        self.tableView.separatorStyle = .none
        self.tableView.addSubview(refreshControl)
    }
}
//
//extension ChatPickRecipientsVC: GeneralViewCellAllUsersDelegate{
//
//    func didTapShowProfile(at index: Int, of section: Int) {
////        var member = Member()
////        member = self.usersAll[index]
////        member.userId = member.id
////        var memberID = 0
////        var firbaseId = ""
////        if let id = member.userId{
////            memberID = id
////        }
////        if let frbId = member.userFirebaseId{
////            firbaseId = frbId
////        }
////        self.previewUser(profile: memberID, with: firbaseId)
//    }
//
//
//    func didTapRow(at index: Int) {
//        // This condition will run when the very first time any row is tapped
////        if selectedIndex == index{
////            self.selectedIndex = -1
////            self.usersAll[index].isChecked = false
////            self.navigationItem.rightBarButtonItem?.title = ""
////            self.navigationItem.rightBarButtonItem?.isEnabled = false
////            self.tableView.reloadData()
////        }else{
////            if selectedIndex != -1{
////                self.usersAll[selectedIndex].isChecked = false
////            }
////            self.selectedIndex = index
////            self.usersAll[index].isChecked = true
////            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(oKButtonTapped))
////            self.navigationItem.rightBarButtonItem?.tintColor = .white
////            self.navigationItem.rightBarButtonItem?.isEnabled = true
////            self.tableView.reloadData()
////        }
//    }
//}
extension ChatPickRecipientsVC: GeneralViewCellDelegate{
    func didTapRow(at index: Int) {
//        self.recipients[index].isSelected = !self.recipients[index].isSelected
//        if let index = self.selectedRecipients.firstIndex(where: { (item) -> Bool in
//            item.email == self.recipients[index].email
//        }) {
//            self.selectedRecipients.remove(at: index)
//        }
//        else {
//            self.selectedRecipients.append(self.recipients[index])
//        }
//        self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
    }
}

