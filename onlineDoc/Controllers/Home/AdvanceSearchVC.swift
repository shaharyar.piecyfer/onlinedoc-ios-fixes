//
//  AdvanceSearchVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 12/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox
//import  ExpandableCell

protocol AdvanceSearchVCDelegate: AnyObject {
    func done(filterType: String)
}

class AdvanceSearchVC: BaseVC {
    //MARK: - IBOUTLETS
    @IBOutlet var checkBoxCollection : [BEMCheckBox]!
    @IBOutlet weak var topOptionsContainerView: UIView!
    
    @IBOutlet weak var topConstrintWhenText: NSLayoutConstraint!
    @IBOutlet weak var topConstraintWhenNoText: NSLayoutConstraint!
    
    @IBOutlet weak var groupSelectionTableView: UITableView!{
        didSet{
            groupSelectionTableView.register(UINib(nibName: "GeneralViewCell", bundle: Bundle.main), forCellReuseIdentifier: "GeneralCell")
        }
    }
    
    @IBOutlet weak var lblAllFilter: UILabel!{
        didSet{
            lblAllFilter.text = "All".localized()
        }
    }
    @IBOutlet weak var searchByAllView: UIView!{
        didSet{
            searchByAllView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(optionTap)))
            searchByAllView.backgroundColor = AppColors.evenRow
        }
    }
    
    @IBOutlet weak var searchByFilesView: UIView!{
        didSet{
            searchByFilesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(optionTap)))
            searchByFilesView.backgroundColor = AppColors.oddRow
        }
    }
    
    @IBOutlet weak var searchByCommentsView: UIView!{
        didSet{
            searchByCommentsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(optionTap)))
            searchByCommentsView.backgroundColor = AppColors.evenRow
        }
    }
    
    @IBOutlet weak var searchByThreadsView: UIView!{
        didSet{
            searchByThreadsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(optionTap)))
            searchByThreadsView.backgroundColor = AppColors.oddRow
        }
    }
    
    weak var delegate: AdvanceSearchVCDelegate?
    var searchParams: TimelineParams?
    var searchText: String = ""
    var filterType: String = SearchFilterTypes.NONE.rawValue
    var myGroups = [GroupModel]()
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.loadGroups()
        self.setTheme()
    }
    
    deinit {
        print("AdvanceSearchVC->deinit")
    }
    
    private func setTheme() {
        Themer.shared.register(
            target: self,
            action: AdvanceSearchVC.setTheme)
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        self.navigationController?.navigationBar.tintColor = .white
    }
    
    func setupView() {
        self.title = "Filter"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: SearchText.localized(), style: .plain, target: self, action: #selector(Done))
        self.layOutCheckBoxes()
        self.filterType = searchParams?.type ?? SearchFilterTypes.NONE.rawValue
        let tag = searchParams?.type == SearchFilterTypes.THREAD.rawValue ? 2 : (searchParams?.type == SearchFilterTypes.COMMENT.rawValue ? 1 : (searchParams?.type == SearchFilterTypes.FILE.rawValue ? 0 : (searchParams?.type == SearchFilterTypes.ALL.rawValue ? 3 : 100)))
        if tag != 100 {
            checkBoxCollection[tag].on = true
        }
        if #available(iOS 15.0, *) {
            self.groupSelectionTableView.sectionHeaderTopPadding = 0.0
        }
        
//        if self.searchText.isEmpty {
//            self.topConstrintWhenText.isActive = false
//            self.topOptionsContainerView.isHidden = true
//            self.topConstraintWhenNoText.isActive = true
//            self.topConstraintWhenNoText.constant = 8
//        }else{
//            self.topOptionsContainerView.isHidden = false
//            self.topConstrintWhenText.isActive = true
//            self.topConstraintWhenNoText.isActive = false
//            self.topConstrintWhenText.constant = 8
//            self.layOutCheckBoxes()
//        }
    }
    
    //MARK: - Class Methods
    func loadGroups(){
        if AppConfig.shared.myGroupsWithCategories.count > 0 {
            self.myGroups = AppConfig.shared.myGroupsWithCategories //.filter{($0.is_display_timeline ?? true)}
            for (_, item) in self.myGroups.enumerated() {
                if let params = self.searchParams {
                    if params.clinic_ids.isEmpty {
                        item.isSelected = true
                        item.clinic_categories = item.clinic_categories?.compactMap({ _item in
                            _item.isSelected = true
                            return _item
                        })
                    }
                    else {
                        params.clinic_ids.forEach { id in
                            if item.id == id {
                                item.isSelected = true
                            }
                            params.category_ids.forEach { id in
                                item.clinic_categories?.first(where: { $0.id == id })?.isSelected = true
                            }
                        }
                    }
                }
            }
            self.groupSelectionTableView.reloadData()
        }else{
            groupAndCategories()
        }
    }
    
    func groupAndCategories() {
        self.loadUserGroup { (status) in
            if status {
                let groups = AppConfig.shared.myGroupsWithCategories //.filter{($0.is_display_timeline ?? true)}
                self.myGroups = groups.reversed()
                if self.myGroups.count > 0 {
                    if self.searchParams == nil {
                        self.myGroups[0].isSelected = true
                        self.myGroups[0].clinic_categories?[0].isSelected = true
                    }
                    AppConfig.shared.myGroupsWithCategories = AppConfig.shared.myGroupsWithCategories.reversed()
                    self.groupSelectionTableView.reloadData()
                    self.loadGroups()
                }
            }
        }
    }
    
    func layOutCheckBoxes(){
        for box in self.checkBoxCollection {
            box.boxType = .square
            box.inputView?.cornerRadius = 5
        }
    }
    
    @objc func Done(){
        delegate?.done(filterType: self.filterType)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func optionTap(_ sender: UITapGestureRecognizer){
        checkBoxCollection[0].on = false
        checkBoxCollection[1].on = false
        checkBoxCollection[2].on = false
        checkBoxCollection[3].on = false
        if let tag = sender.view?.tag {
            checkBoxCollection[tag].on = !checkBoxCollection[tag].on
            switch tag {
            case 0:
                self.filterType = SearchFilterTypes.FILE.rawValue
                checkBoxCollection[tag].on = checkBoxCollection[tag].on ? true : false
                break
            case 1:
                self.filterType = SearchFilterTypes.COMMENT.rawValue
                checkBoxCollection[tag].on = checkBoxCollection[tag].on ? true : false
                break
            case 2:
                self.filterType = SearchFilterTypes.THREAD.rawValue
                checkBoxCollection[tag].on = checkBoxCollection[tag].on ? true : false
                break
            case 3:
                self.filterType = SearchFilterTypes.ALL.rawValue
                checkBoxCollection[tag].on = checkBoxCollection[tag].on ? true : false
                break
            default:
                break
            }
        }
    }
    
    @objc func sectionTap(_ sender:UITapGestureRecognizer){
        if self.searchParams == nil {
            for group in self.myGroups{
                group.isSelected = false
                for cat in group.clinic_categories!{
                    cat.isSelected = false
                }
            }
        }
        if let tag = sender.view?.tag{
            self.myGroups[tag].isSelected = !self.myGroups[tag].isSelected
            if self.searchParams == nil {
                if let categories = self.myGroups[tag].clinic_categories, categories.count > 0 {
                    self.myGroups[tag].clinic_categories![0].isSelected = self.myGroups[tag].isSelected
                }
            }
            else {
                if let categories = self.myGroups[tag].clinic_categories {
                    if self.myGroups[tag].isSelected {
                        _ = categories.compactMap({$0.isSelected = true})
                    }
                    else {
                        _ = categories.compactMap({$0.isSelected = false})
                    }
                }
            }
        }
        self.groupSelectionTableView.reloadData()
    }
}

//MARK: - Extensions
extension AdvanceSearchVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.myGroups[section].isSelected{
            return self.myGroups[section].clinic_categories!.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell", for: indexPath) as? GeneralViewCell else{
            return GeneralViewCell()
        }
        cell.setCellData(indexPath: indexPath, data: self.myGroups[indexPath.section].clinic_categories![indexPath.row])
        cell.contentView.sendSubviewToBack(cell.cellCheckBox)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.searchParams == nil {
            for (i,item) in self.myGroups[indexPath.section].clinic_categories!.enumerated(){
                if i != indexPath.row{
                    item.isSelected = false
                }
            }
        }
        if self.myGroups[indexPath.section].clinic_categories![indexPath.row].isSelected{
            self.myGroups[indexPath.section].clinic_categories![indexPath.row].isSelected = false
//            self.myGroups[indexPath.section].clinic_categories![0].isSelected = true
            if self.myGroups[indexPath.section].clinic_categories?.allSatisfy({$0.isSelected == false}) ?? false {
                self.myGroups[indexPath.section].isSelected = false
            }
        }else{
            self.myGroups[indexPath.section].clinic_categories![indexPath.row].isSelected = true
            self.myGroups[indexPath.section].isSelected = true
        }
        self.groupSelectionTableView.reloadData()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.myGroups.count > 0{
            tableView.setEmptyMessage("")
            return self.myGroups.count
        }else{
            tableView.setEmptyMessage(noGroupText.localized())
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.myGroups[indexPath.section].isSelected{
            return 50
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("HeaderViewCell", owner: self, options: nil)?.first as? HeaderViewCell
        if let title = self.myGroups[section].name, title != ""{
            headerView?.titleLabel.text = title.capitalized
        }
        headerView?.tag = section
        let imgUrl = self.myGroups[section].file?.medium_url ?? self.myGroups[section].file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            let url = URL(string: imageUrl)
            headerView?.groupImageView.imageFromServer(imageUrl: url!)
            headerView?.groupImageView.contentMode = .scaleAspectFill
        }else{
            headerView?.groupImageView.image = UIImage(named: "ic_group")
            headerView?.groupImageView.contentMode = .center
        }
        headerView?.checkBoxView.on = self.myGroups[section].isSelected
        if self.myGroups[section].isSelected{
            headerView?.iconImageView.image = #imageLiteral(resourceName: "carrotWhite").withRenderingMode(.alwaysTemplate)
        }else{
            headerView?.iconImageView.image = #imageLiteral(resourceName: "carrot").withRenderingMode(.alwaysTemplate)
        }
        headerView?.iconImageView.contentMode = .center
//        if section % 2 == 0{
//            headerView?.contentView.backgroundColor = AppColors.evenRow
//        }else{
//            headerView?.contentView.backgroundColor = AppColors.oddRow
//        }
        headerView?.gestureRecognizers?.forEach(headerView!.removeGestureRecognizer)
        headerView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sectionTap)))
        return headerView
    }
}

extension AdvanceSearchVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        searchByAllView.backgroundColor = theme.settings.highlightedBgColor
        searchByFilesView.backgroundColor = theme.settings.highlightedBgColor
        searchByCommentsView.backgroundColor = theme.settings.highlightedBgColor
        searchByThreadsView.backgroundColor = theme.settings.highlightedBgColor
    }
}

//MARK : - Custom DataTypes
struct AdvanceSearchOptions {
    var texttoSearch        = ""
    var byFile: String      = ""
    var byComment: String   = ""
    var byThread : String   = ""
    var folderIds: [Int]!    = [Int]()
    var groupIds : Int!
    var page = 0
    var type : String       = "group"
    var groups:[GroupModel]? = [GroupModel]()
    var blockSize = 10
    var inGroup : String = ""
}
