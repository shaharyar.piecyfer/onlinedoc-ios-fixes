
//
//  EditGroupInfoVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 27/02/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox

protocol EditGroupInfoViewControllerDelegate:class {
    func didUpdateGroup(groupInfo: GroupModel?)
    func didDeleteGroup()
}

class EditGroupInfoVC: BaseVC {
    var groupInfo = GroupModel()
    var groupCategories = [GroupCategoryModel](){
        didSet{
            self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
            self.groupCategoriesTableViewHeightConstraint.constant = self.tableView.contentSize.height
        }
    }
    var alreadyAddedCategories = [GroupCategoryModel]() {
        didSet{
            groupCategories = alreadyAddedCategories
        }
    }
    var groupData = NewGroupModel()
    weak var delegate : EditGroupInfoViewControllerDelegate?
    var privacyGroup = BEMCheckBoxGroup()
    var groupMembershipOptionsGroup = BEMCheckBoxGroup()
    var directUploadInfo: DirectUploadModel?
    var groupImageData: Data?
    
    // MARK: - IBOUTLETS
    @IBOutlet weak var groupTitleField: UITextField!
    @IBOutlet weak var groupImage :UIImageView!
    @IBOutlet weak var descriptionTextView:UITextView!{
        didSet{
            descriptionTextView.text = EditGroupDescriptionPlaceholder.localized()
        }
    }
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var groupCategoriesTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView:UIScrollView!
    
    @IBOutlet weak var publicCheckBox: BEMCheckBox!{
        didSet{
            publicCheckBox.boxType = .circle
        }
    }
    
    @IBOutlet weak var privateCheckBox: BEMCheckBox!{
        didSet{
            privateCheckBox.boxType = .circle
        }
    }
    
    @IBOutlet weak var membershipAdminCheckBox: BEMCheckBox!{
        didSet{
            membershipAdminCheckBox.boxType = .circle
        }
    }
    
    @IBOutlet weak var membershipMembersCheckBox: BEMCheckBox!{
        didSet{
            membershipMembersCheckBox.boxType = .circle
        }
    }
    
    @IBOutlet weak var publicCheckView:UIView!{
        didSet{
            publicCheckView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.publicViewTapped)))
        }
    }
    @IBOutlet weak var privateCheckView: UIView!{
        didSet{
            privateCheckView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.privateViewTapped)))
        }
    }
    
    @IBOutlet weak var membershipCheckView: UIView!
    
    @IBOutlet weak var membershipAdminLabel: UILabel!{
        didSet{
            membershipAdminLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.membershipAdminTapped)))
        }
    }
    
    @IBOutlet weak var membershipMemberLabel: UILabel!{
        didSet{
            membershipMemberLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.membershipMemberTapped)))
        }
    }
    
    @IBOutlet weak var btnAddCategory: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCheckBoxGroups()
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(title: SaveText.localized(), style: .plain, target: self, action: #selector(onClickUpdate))
        
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
//                UITextField.appearance().tintColor = .white
            } else {
//                UITextField.appearance().tintColor = .black
            }
        }
        self.setupTBL()
        self.setupView()
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: EditGroupInfoVC.setTheme)
        let labels: [UILabel] = AppConfig.shared.getSubviewsOf(view: self.view, tag: 222)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.setTheme)
        }
        
        let images: [UIImageView] = AppConfig.shared.getSubviewsOf(view: self.view, tag: 333)
        for image in images {
            image.image = image.image?.withRenderingMode(.alwaysTemplate)
            Themer.shared.register(
                target: image,
                action: UIImageView.setTheme)
        }
    }
    
    deinit {
        print("EditGroupInfoVC->deinit")
    }
    
    @objc func publicViewTapped(){
        self.privateCheckBox.on = false
        self.publicCheckBox.on = true
    }
    
    @objc func privateViewTapped(){
        self.publicCheckBox.on = false
        self.privateCheckBox.on = true
    }
    
    @objc func membershipAdminTapped(){
        self.membershipMembersCheckBox.on = false
        self.membershipAdminCheckBox.on = true
    }
    
    @objc func membershipMemberTapped(){
        self.membershipAdminCheckBox.on = false
        self.membershipMembersCheckBox.on = true
    }
    
    @objc func onClickUpdate(){
        self.view.endEditing(true)
        if let groupTitle = self.groupTitleField.text, groupTitle.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            self.groupData.name = groupTitle
        } else {
            self.showAlert(To: GroupTitleRequired.localized(), for: GroupTitleRequiredMessage.localized()) { [weak self] (status) in
                guard let `self` = self else {return}
                if status {
                    self.groupTitleField.text = self.groupInfo.name
                    self.groupTitleField.becomeFirstResponder()
                }
            }
            return
        }
//        AppConfig.shared.groupListNeedReload = true

        if let description = self.descriptionTextView.text, description != "" && description != EditGroupDescriptionPlaceholder.localized(){
            self.groupData.description = description
        }
        let privacySelection = self.privacyGroup.selectedCheckBox
        self.groupData.is_public = privacySelection == publicCheckBox ? true : false

//        let groupMailSelection = self.groupMailOptionsGroup.selectedCheckBox
//        self.groupData.groupMail = groupMailSelection == mailAdminCheckBox ? "A" : "E"

        let membershipApprovalSelction = self.groupMembershipOptionsGroup.selectedCheckBox
        self.groupData.can_approve = membershipApprovalSelction == membershipAdminCheckBox ? WhoCanUpdate.ADMIN.rawValue : WhoCanUpdate.EVERYONE.rawValue
        
        let preAddedCategories = self.groupCategories.filter { $0.isPreGroupCat && $0.name != "Standard (Default)" }
        var newCategories = self.groupCategories.filter { !$0.isPreGroupCat }
        newCategories = newCategories.filter { $0.isChecked }
        
        self.groupData.clinic_categories_attributes = (preAddedCategories + newCategories).map() {
            var category = ClinicCategoryModel()
            category.id = $0.id
            category.name = $0.name
            category.sort_order = $0.sort_order
            category.is_available = $0.isChecked
            return category
        }
        
        if let info = directUploadInfo, let data = groupImageData {
            var progressView: ProgressView?
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else {return}
                progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
                progressView!.lblTitle.text = "txt_progress_title".localized()
                progressView!.lblDetail.text = "txt_progress_detail".localized()
                progressView!.uiProgress.progress = 0
                progressView!.tag = 100
                progressView!.lblCount.text = "1/1"
                self.addView(view: progressView!)
            }
            
            RequestManager.shared.directUploadImage(data: data, params: info) { [weak self] (percent) in
                guard let _ = self else {return}
                progressView?.uiProgress.progress = Float(percent)
            } completion: { [weak self] (status, response) in
                guard let `self` = self else {return}
                if let progressView = progressView {
                    self.removeView(tag: progressView.tag)
                }
                if status {
                    self.groupData.file = info.signed_id
                    self.updateGroupInfo()
                }
                else {
                    self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                }
            }
        } else {
            self.updateGroupInfo()
        }
    }
    
    private func updateGroupInfo(){
        RequestManager.shared.updateGroup(id: self.groupInfo.id!, params: self.groupData) { (status, response) in
            if status {
                self.directUploadInfo = nil
                if let data = response as? CreateGroupReaponse, data.result != nil {
                    self.groupInfo = data.result!
                }
                self.getUserGroupWithUpdatedCategories()
            }
        }
    }
    
    // Get Logged In User Group along with checked Categories. This API call is same as loadUserGroup() API call in UIViewController Extension
    func getUserGroupWithUpdatedCategories(){
        self.loadUserGroup { (status) in
            if status {
                NotificationCenter.default.post(name: .notifyGroup, object: nil, userInfo: ["Group": self.groupInfo, "Notify": NotifyCRUD.update])
                self.delegate?.didUpdateGroup(groupInfo: self.groupInfo)
                self.showAlert(To: GroupInfo_Message.localized(), for: "") { [weak self] _ in
                    guard let `self` = self else {return}
                    self.dismissVC()
                }
            }
        }
    }
    
    private func setCheckBoxGroups(){
        self.privacyGroup = BEMCheckBoxGroup(checkBoxes: [publicCheckBox, privateCheckBox])
        self.privacyGroup.selectedCheckBox = publicCheckBox
        self.privacyGroup.mustHaveSelection = true
        
        self.groupMembershipOptionsGroup = BEMCheckBoxGroup(checkBoxes: [membershipAdminCheckBox, membershipMembersCheckBox])
        self.groupMembershipOptionsGroup.selectedCheckBox = membershipAdminCheckBox
        self.groupMembershipOptionsGroup.mustHaveSelection = true
    }
    
    func setupView(){
        if let cats = self.groupInfo.clinic_categories?.filter({ $0.is_active }) {
            self.alreadyAddedCategories = cats.map{ (category) in
                category.isPreGroupCat = true
                category.isChecked = category.is_available
                return category
            }
        }
//        groupTitleField.tintColor = .white
        if let groupTitle = self.groupInfo.name {
            self.groupTitleField.text = groupTitle
        }
        
        if let imgUrl = self.groupInfo.file?.src, imgUrl != "" {
            if let url = URL(string: imgUrl){
                self.groupImage.imageFromServer(imageUrl: url)
            }
        }else{
            self.groupImage.image = #imageLiteral(resourceName: "groupDefault")
        }
        
        if let desc = self.groupInfo.description{
            if desc.isEmpty {
                self.descriptionTextView.text = EditGroupDescriptionPlaceholder.localized()
            } else {
                self.descriptionTextView.text = desc
            }
        }
        
        if let isPublic = self.groupInfo.is_public, isPublic {
            self.publicViewTapped()
        }else{
            self.privateViewTapped()
        }
        
        if let requestApproval = self.groupInfo.can_approve, requestApproval == WhoCanUpdate.ADMIN.rawValue {
            self.membershipAdminTapped()
        }else{
            self.membershipMemberTapped()
        }
    }
    
    // MARK: - IBACTION METHODS
    @IBAction func changeGroupPicture(){
        self.showDialogAlert(options: .camera, .library, singleSelection: true)
    }
    
    @IBAction func addNewCategoryMethod(){
        let newCategry = GroupCategoryModel()
        let highestOrder = self.groupCategories.map{ ($0.sort_order ?? 0) }.max() ?? 0
        newCategry.sort_order = 1 + highestOrder
        self.groupCategories.append(newCategry)
        let lastCellIndex = IndexPath(row: groupCategories.count - 1, section: 0)
        self.tableView.scrollToRow(at: lastCellIndex, at: .bottom, animated: true)

        let cell = self.tableView.cellForRow(at: lastCellIndex) as? CategoryFieldCell
        _ = cell?.newCategoryField.becomeFirstResponder()
    }
    
    @IBAction func deleteGroup(){
        let superAadmin = self.groupInfo.clinic_admins?.first(where: { $0.role == GroupMemberRoles.SUPER_ADMIN.rawValue })
        if superAadmin?.id != AppConfig.shared.user.id {
            self.showAlert(To: warningText.localized(), for: deleteGroupAlertMessage.localized()) {_ in}
        }
//        if let memberCount = self.groupInfo.joined_members, memberCount > 1 {
//            self.showAlert(To: warningText.localized(), for: deleteGroupAlertMessage.localized().replacingOccurrences(of: "#GROUP#", with: self.groupInfo.name!)) { (status) in
//                return
//            }
//            return
//        }
        else{
            if let groupId = self.groupInfo.id {
                self.showAlert(To: DeleteGroupHeader.localized(), for: "", tfPlaceholder: "Enter Password".localized(), isSecure: true, buttons: 2,
                               firstBtnStyle: .default, secondBtnStyle: .cancel, firstBtnTitle: OKText.localized(), secondBtnTitle: declineText.localized()) { [weak self] (status, text) in
                    guard let `self` = self else {return}
                    if status {
                        if (text == nil || text!.isEmpty) {
                            self.showAlert(To: "", for: "Please enter password.".localized()) {_ in }
                        }
                        else {
                            let credentialsDic = UserDefaults.standard.object(forKey: CREDENTIALS_DICT) as? [[String:Any]] ?? [[String:Any]]()
                            if let index = (credentialsDic.firstIndex(where: { $0["userName"] as? String == AppConfig.shared.user.email })){
                                if text == credentialsDic[index]["passCode"] as? String {
                                    self.handleDeleteGroup(groupId)
                                }
                                else {
                                    self.showAlert(To: "", for: "Password does not match.".localized()) {_ in }
                                }
                            }
                            else {
                                self.showAlert(To: "", for: "Password does not match.".localized()) {_ in }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func handleDeleteGroup(_ id: Int){
        RequestManager.shared.deleteGroup(id: id) { (status, res) in
            if status{
                self.showAlert(To: DeleteGroupHeader.localized(), for: DeleteGroupMessage.localized()) { [weak self] _ in
                    guard let `self` = self else {return}
                    NotificationCenter.default.post(name: .notifyGroup, object: nil, userInfo: ["Group": self.groupInfo, "Notify": NotifyCRUD.delete])
                    self.delegate?.didDeleteGroup()
                    AppConfig.shared.didDeleteSelectedGroup = true
                    self.dismissVC()
                }
            }
        }
    }
    
    func EditRow(At indexPath: IndexPath){
        //TODO :
    }
    
    private func deleteCatagory(index: IndexPath) {
        if let id = self.groupCategories[index.row].id {
            RequestManager.shared.deleteCategory(id: id) { (status, res) in
                if status{
                    self.groupCategories.remove(at: index.row)
                    self.tableView.reloadData()
                }
            }
        }
    }
}

// MARK: - EXTENSIONs
extension EditGroupInfoVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < self.alreadyAddedCategories.count {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as? CategoryRowCell
            else {
                return UITableViewCell()
            }
            if groupCategories.indices.contains(indexPath.row) {
                cell.index = indexPath
                cell.configCell(indexPath: indexPath, data: groupCategories[indexPath.row])
                cell.delegate = self
                cell.categoryCheckBoxView.tag = indexPath.row
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CategoryFieldCell.self)) as? CategoryFieldCell
            else {
                return UITableViewCell()
            }
            cell.index = indexPath
            cell.checkBox.tag = indexPath.row
            cell.configCell(indexPath: indexPath, data: groupCategories[indexPath.row])
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0 {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: DeleteTitle.localized()) { _, _, _ in
            let superAadmin = self.groupInfo.clinic_admins?.first(where: { $0.role == GroupMemberRoles.SUPER_ADMIN.rawValue })
            if superAadmin?.id != AppConfig.shared.user.id {
                self.showAlert(To: warningText.localized(), for: deleteCategoryAlertMessage.localized()) {_ in}
            }
            else {
                self.showAlert(To: warningText.localized(), for: DeleteCategoryMessage.localized(), firstBtnStyle: .cancel) { [weak self] (completed) in
                    guard let `self` = self else {return}
                    if completed {
                        self.deleteCatagory(index: indexPath)
                    }
                }
            }
        }
        let editButton = UIContextualAction(style: .normal, title: EditTitle.localized()) { _, _, _ in
            self.groupCategories[indexPath.row].isSelectedToEdit = true
            self.tableView.reloadData()
        }
        editButton.backgroundColor = AppColors.choice
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editButton])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: CategoryRowCell.self), bundle: nil), forCellReuseIdentifier: "CategoryCell")
        self.tableView.register(UINib.init(nibName: String(describing: CategoryFieldCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CategoryFieldCell.self))
        
        self.tableView.separatorStyle = .none
    }
}

///Alternative for didSelectRowAt as it was not working on the cell

extension EditGroupInfoVC: CategoryRowCellDelegate {
    func didTapCheckMark(index: IndexPath, order: Int?) {
        if let sorting = order {
            self.groupCategories[index.row].sort_order = sorting
            if !self.groupCategories[index.row].isChecked{
                self.groupCategories[index.row].isChecked = true
            }
        }else{
            self.groupCategories[index.row].isChecked = !self.groupCategories[index.row].isChecked
        }
        self.groupCategories[index.row].isUpdated = true
        self.tableView.reloadRows(at: [index], with: .automatic)
    }
    
    func didEditCategory(at index: IndexPath, with Text: String) {
        self.groupCategories[index.row].name = Text
        self.tableView.reloadRows(at: [index], with: .automatic)
    }
}

extension EditGroupInfoVC: CategoryFieldCellDelegate {
    func didWriteNewCategory(index: IndexPath, isChecked: Bool, category: String?, sortOrder: Int?) {
        if isChecked{
            if let cat = category, cat != ""{
                let isValidIndex = groupCategories.indices.contains(index.row)
                if isValidIndex{
                    if !self.groupCategories[index.row].isChecked{
                        self.groupCategories[index.row].isChecked = !self.groupCategories[index.row].isChecked
                    }
                    self.groupCategories[index.row].name = cat
                }
            }else if let sorting = sortOrder{
                let isValidIndex = groupCategories.indices.contains(index.row)
                if isValidIndex{
                    self.groupCategories[index.row].sort_order = sorting
                }
            }
        }else{
            let isValidIndex = groupCategories.indices.contains(index.row)
            if isValidIndex{
                self.groupCategories[index.row].isChecked = false
                self.groupCategories[index.row].name = ""
            }
        }
    }
    
    func didTapCheckMark(index: IndexPath, isChecked: Bool) {
        self.groupCategories[index.row].isChecked = isChecked
//        self.tableView.reloadRows(at: [index], with: .automatic)
    }
}

extension EditGroupInfoVC {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var capturedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        capturedImage = self.imageOrientation(capturedImage)
        self.groupImage.image = capturedImage
        picker.dismiss(animated: true, completion: nil)
        
        var fileName = UUID().uuidString + ".jpeg"
        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
            fileName = url.lastPathComponent
//          fileType = url.pathExtension
        }
        
        groupImageData = capturedImage.jpegData(compressionQuality: capturedImage.size.width > 500 ? 0.5 : 1.0) ?? Data(capturedImage.sd_imageData()!)
        let imageSize: Int = groupImageData!.count
        
        var directUploadParams = DirectUploadParams()
        directUploadParams.filename = fileName
        directUploadParams.content_type = groupImageData!.fileExtension
        directUploadParams.byte_size = imageSize
        
        RequestManager.shared.directUpload(_params: directUploadParams, data: groupImageData!) { (status, result) in
            if status {
                if let data = result as? DirectUploadModel {
                    self.directUploadInfo = data
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension EditGroupInfoVC: UITextViewDelegate{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if (textView == descriptionTextView) {
            return true
        }
        return false
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == self.descriptionTextView{
            if let text = textView.text, text == EditGroupDescriptionPlaceholder.localized(){
                textView.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == self.descriptionTextView{
            if let text = textView.text{
                if text == ""{
                    textView.text = EditGroupDescriptionPlaceholder.localized()
                    textView.resignFirstResponder()
                }
            }
        }
        self.view.endEditing(true)
    }
}

extension EditGroupInfoVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        groupTitleField.textColor = theme.settings.titleColor
        groupTitleField.tintColor = theme.settings.titleColor
        publicCheckView.backgroundColor = theme.settings.highlightedBgColor
        privateCheckView.backgroundColor = theme.settings.highlightedBgColor
        membershipCheckView.backgroundColor = theme.settings.highlightedBgColor
        btnAddCategory.backgroundColor = theme.settings.highlightedBgColor
        btnAddCategory.setTitleColor(theme.settings.titleColor, for: .normal)
        descriptionTextView.textColor = theme.settings.subTitleColor
    }
}

extension UILabel {
    fileprivate func setTheme(_ theme: MyTheme) {
        self.textColor = theme.settings.subTitleColor
    }
}

extension UIImageView {
    fileprivate func setTheme(_ theme: MyTheme) {
        self.tintColor = theme.settings.titleColor
    }
}
