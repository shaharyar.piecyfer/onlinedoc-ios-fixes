//
//  GroupEmailVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 13/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import TagListView
import RAGTextField
import MobileCoreServices
import WeScan
import VisionKit
import PDFKit
import Photos
import OpalImagePicker

class GroupEmailVC: BaseVC {
    
    @IBOutlet weak var attachmentsCollectionView: UICollectionView!
    @IBOutlet weak var emailSubjectField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: emailSubjectField)
        }
    }
    @IBOutlet weak var emailContextTextView:UITextView!{
        didSet{
            emailContextTextView.showViewBorders(width: 1, color: AppColors.borderColor)
            emailContextTextView.text = GroupEmailDescription.localized()
            emailContextTextView.textColor = AppColors.grayColor
        }
    }
    @IBOutlet weak var keyboardHeightLayoutConstraint:NSLayoutConstraint!
    
    @IBOutlet weak var recipientsView: UIView!{
        didSet{
            recipientsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectRecipients)))
        }
    }
    
    @IBOutlet weak var chipsView:TagListView!{
        didSet{
            var selecteMemberNames = [String]()
            for member in self.groupMembers {
                if member.isSelected {
                    selecteMemberNames.append(member.full_name!)
                }
            }
            self.initUserTags(users: selecteMemberNames)
        }
    }
    var groupId = -1
    var groupMembers = [MemberModel]()
    let videoFileName = "/od_video.mov"
    var permittedMembers = "all"{
        didSet{
            print(permittedMembers)
        }
    }
    var directUploads = [Uploads](){
        didSet{
            DispatchQueue.main.async {
                self.attachmentsCollectionView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.keyboardSetting()
        self.emailContextTextView.text = GroupEmailDescription.localized()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .plain, target: self, action: #selector(sendEmail))
        
        self.setupTheme()
    }
    
    deinit {
        print("GroupEmailVC->deinit")
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: GroupEmailVC.setTheme)
    }
    
    func keyboardSetting(){
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillShowNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillHideNotification,object: nil)
    }
    
    @objc func keyboardNotification(_ notification: NSNotification) {
        let isShowing = notification.name == UIResponder.keyboardWillShowNotification
        
//        var tabbarHeight: CGFloat = 0
//        if self.tabBarController != nil {
//            tabbarHeight = self.tabBarController!.tabBar.frame.height
//        }
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.intValue ?? Int(UIView.AnimationOptions.curveEaseInOut.rawValue)
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: UInt(animationCurveRaw))
            self.keyboardHeightLayoutConstraint?.constant = isShowing ? (endFrame!.size.height) : 0
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    @objc func endEditing(){
        self.view.endEditing(true)
    }
    
    @objc func selectRecipients(){
        self.gotoSelectionController()
    }
    
    func gotoSelectionController() {
        if let vc = getVC(storyboard: .CREATE_THREAD, vcIdentifier: "permissionsScene") as? SelectPermissionsVC {
            vc.selection = .groupMail
            vc.groupId = self.groupId
            vc.delegate = self
//            vc.memberIds = self.memberIds
            vc.permittedMembers = self.permittedMembers
            self.navigateVC(vc)
        }
    }
    
    @objc func sendEmail(){
        sendGroupEmail()
    }
    
    private func sendGroupEmail() {
        guard let subject = self.emailSubjectField.text?.trimmingCharacters(in: .whitespacesAndNewlines), !subject.isEmpty else {
            self.showAlert(To: MailSubjectTitle.localized(), for: MailSubjectMessage.localized()) { [weak self] (status) in
                guard let `self` = self else {return}
                _ = self.emailSubjectField.becomeFirstResponder()
            }
            return
        }
        guard let body = self.emailContextTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines), !body.isEmpty else {
            self.showAlert(To: MailMessageTitle.localized(), for: MailMessageMessage.localized()) { [weak self] (status) in
                guard let `self` = self else {return}
                _ = self.emailContextTextView.becomeFirstResponder()
            }
            return
        }
        var params = GroupEmailParams()
        params.clinic_id = self.groupId
        params.title = subject
        params.body = body
        
        RequestManager.shared.sendGroupEmail(with: params) { (status, response) in
            if status{
                if let _ = response as? GeneralSuccessResult {
                    self.showAlert(To: "", for: GroupEmailMessage.localized()) { [weak self] (status) in
                        guard let `self` = self else {return}
                        if status {
                            self.dismissVC()
                        }
                    }
                }
            }
        }
    }
    
    func showScannedFileRenamePopUp(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: "Rename Scanned File".localized(), for: "Do you want to rename scanned file?".localized(), firstBtnStyle: .default, secondBtnStyle: .default, firstBtnTitle: yesText.localized(), secondBtnTitle: noText.localized()) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                self.showSaveDialog(params, data, thumbNail)
            } else {
                self.uploadForSignedId(params, data, thumbNail)
            }
        }
    }
    
    func showSaveDialog(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: saveDocuments.localized(), for: "Enter document name".localized(), tfPlaceholder: String(Date().getTime())) { [weak self] (status, text) in
            guard let `self` = self else {return}
            if (text == nil || text!.isEmpty) {
                self.uploadForSignedId(params, data, thumbNail)
            }
            else {
                var info = params
                info.filename = text!+".pdf"
                self.uploadForSignedId(info, data, thumbNail)
            }
        }
    }
    
    private func uploadForSignedId(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?){
        RequestManager.shared.directUpload(_params: params, data: data) { (status, result) in
            if status {
                if let info = result as? DirectUploadModel {
                    let upload = Uploads(directUploadInfo: info, data: data, image: thumbNail)
                    self.directUploads.append(upload)
                }
            }
        }
    }
    
    
    @IBAction func addAttachment() {
        self.showDialogAlert(options: .camera, .library, .video, .docs, .scan)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "SegueToReciupientsList"{
//            let dvc = segue.destination as? RecipientsVC
//            dvc?.selection = .groupEmail
//            dvc?.groupId = self.groupId
//            dvc?.delegate = self
//        }
    }
    
    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
        if let theError = error {
            print("error saving the video = \(theError)")
        } else {
            DispatchQueue.main.async(execute: { () -> Void in })
        }
    }
}

extension GroupEmailVC: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.directUploads.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? AttachmentCell else{ return AttachmentCell() }
        let attachment = directUploads[indexPath.item]
        if attachment.directUploadInfo.content_type!.contains("image") || attachment.directUploadInfo.content_type!.contains("video") {
            cell.cellImageView.image = attachment.image
        }else{
            cell.cellImageView.image = #imageLiteral(resourceName: "attached")
        }
        cell.index = indexPath
        cell.delegate = self
        return cell
    }
}

//UIDocumentBrowserViewController
extension GroupEmailVC {
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentsAt documentURLs: [URL]) {
        controller.dismiss(animated: true, completion: nil)
        guard documentURLs.count < maxAllowedAttachments else {
            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
            return
        }
        for url in documentURLs {
            let fileName = url.lastPathComponent
            var fileData = Data()
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                fileData = data
            } catch  {
                print("Unable to get data of the file")
            }
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = fileData.count
            
//            print("GroupEmailVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, fileData, nil)
        }
    }
    
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
//        guard urls.count < maxAllowedAttachments else {
//            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
//            return
//        }
//        for url in urls {
//            let fileName = url.lastPathComponent
//            var fileData = Data()
//            do {
//                let data = try Data(contentsOf: url, options: .mappedIfSafe)
//                fileData = data
//            } catch  {
//                print("Unable to get data of the file")
//            }
//            var directUploadParams = DirectUploadParams()
//            directUploadParams.filename = fileName
//            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//            directUploadParams.byte_size = fileData.count
//            
////            print("GroupEmailVC->directUploadParams", directUploadParams)
//            self.uploadForSignedId(directUploadParams, fileData, nil)
//        }
//    }
//    
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
//        let fileName = url.lastPathComponent
//        var fileData = Data()
//        do {
//            let data = try Data(contentsOf: url, options: .mappedIfSafe)
//            fileData = data
//        } catch  {
//            print("Unable to get data of the file")
//        }
//        var directUploadParams = DirectUploadParams()
//        directUploadParams.filename = fileName
//        directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//        directUploadParams.byte_size = fileData.count
//        
////        print("GroupEmailVC->directUploadParams", directUploadParams)
//        self.uploadForSignedId(directUploadParams, fileData, nil)
//    }
//    
//    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//        // Picker was cancelled! Duh 🤷🏻‍♀️
//        self.dismiss(animated: true, completion: nil)
//    }
}

extension GroupEmailVC {
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        print("GroupEmailVC->imagePickerDidCancel")
    }
    
    internal func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        for assetItem in assets {
            AppConfig.shared.getURLFromAsset(mPhasset: assetItem) { (url) in
                if url != nil {
                    var fileName = url!.lastPathComponent
                    var fileData = Data()
                    do {
                        let data = try Data(contentsOf: url!, options: .mappedIfSafe)
                        fileData = data
                        if fileName.fileExtension().uppercased() == "HEIC" {
                            if let dataToImage = UIImage(data: data) {
                                fileName = String(fileName.split(separator: ".").first!).appending(".jpeg")
                                fileData = dataToImage.jpegData(compressionQuality: dataToImage.size.width > 500 ? 0.5 : 1.0) ?? data
                            }
                        }
                    } catch  {
                        print("Unable to get data of the file")
                    }
                    var thumbNail: UIImage?
                    if assetItem.mediaType == PHAssetMediaType.image {
                        if let image = AppConfig.shared.getImageFromAsset(asset: assetItem) {
                            thumbNail = image
                        }
                    }
                    else {
                        thumbNail = url!.generateThumbnail()
                    }
                    var directUploadParams = DirectUploadParams()
                    directUploadParams.filename = fileName
                    directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                    directUploadParams.byte_size = fileData.count
                    
                    self.uploadForSignedId(directUploadParams, fileData, thumbNail)
                }
                else {
                    if let img = AppConfig.shared.getAssetThumbnail(asset: assetItem) {
                        let fileName = UUID().uuidString + ".jpeg"
                        let fileData = img.jpegData(compressionQuality: img.size.width > 500 ? 0.5 : 1.0)
                        if let fileData = fileData {
                            var directUploadParams = DirectUploadParams()
                            directUploadParams.filename = fileName
                            directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                            directUploadParams.byte_size = fileData.count
                            
                            self.uploadForSignedId(directUploadParams, fileData, img)
                        }
                    }
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 0
    }
    
    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
        return URL(string: "https://placeimg.com/500/500/nature")
    }
}

extension GroupEmailVC {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let _capturedImage = info[.originalImage] as? UIImage {
            picker.dismiss(animated: true, completion: nil)
            let capturedImage = self.imageOrientation(_capturedImage)
//            self.profileImageView.image = capturedImage
            
            var fileName = UUID().uuidString + ".jpeg"
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                fileName = url.lastPathComponent
    //          fileType = url.pathExtension
            }
            
            let _imageData = capturedImage.jpegData(compressionQuality: capturedImage.size.width > 500 ? 0.5 : 1.0) ?? Data(capturedImage.sd_imageData()!)
            let imageSize: Int = _imageData.count
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //_imageData.fileExtension
            directUploadParams.byte_size = imageSize
            
//            print("GroupEmailVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, _imageData, capturedImage)
        }
        else if let selectedVideo:URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) {
            // Save video to the main photo album
            let selectorToCall = #selector(self.videoSaved(_:didFinishSavingWithError:context:))
            
            UISaveVideoAtPathToSavedPhotosAlbum(selectedVideo.relativePath, self, selectorToCall, nil)
            // Save the video to the app directory
            var _videoData = Data()
            let videoData = try? Data(contentsOf: selectedVideo)
            let paths = NSSearchPathForDirectoriesInDomains(
                FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
            let dataPath = documentsDirectory.appendingPathComponent(videoFileName)
            
            try! videoData?.write(to: dataPath, options: [])
            
            do {
                let data = try Data(contentsOf: dataPath, options: .mappedIfSafe)
                _videoData = data
            } catch  {
                
            }
            let thumbNail = dataPath.generateThumbnail()
            let fileName = selectedVideo.lastPathComponent
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = _videoData.count
            
//            print("GroupEmailVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, _videoData, thumbNail)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension GroupEmailVC : AttachmentCellDelegate{
    func didSelectToRemove(index: IndexPath) {
//        for id in self.fileIds{
//            print(id)
//        }
//        if self.fileIds.indices.contains(at){
//            self.fileIds.remove(at: at)
//        }
//        print(self.attachements.description)
//
//        if self.attachements.indices.contains(at){
//            self.attachements.remove(at: at)
//        }
//        print(self.attachements.description)
    }
}

extension GroupEmailVC: TagListViewDelegate{
    func initUserTags(users: [String]) {
        chipsView.isHidden = false
        chipsView.textFont = .systemFont(ofSize: 20)
        chipsView.removeAllTags()
        chipsView.textFont = UIFont.systemFont(ofSize: 14)
        chipsView.alignment = .left
        for name in users{
            chipsView.addTag(name)
        }
        
    }
}

//extension GroupEmailVC: RecipientListViewControllerDelegate{
//    func didGroupSelection(data: [MyGroups], status: Bool) {}
//
//    func didSelectRecipients(status: Bool) {
//        if status{
//            var selecteMemberNames = [String]()
//            for member in AppConfig.shared.threadGroupMembers{
//                if member.isSelected{
//                    selecteMemberNames.append(member.name)
//                }
//            }
//            self.initUserTags(users: selecteMemberNames)
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .plain, target: self, action: #selector(sendEmail))
//        }
//    }
//}

extension GroupEmailVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.emailSubjectField{
            self.emailContextTextView.becomeFirstResponder()
        }
        return true
    }
}

extension GroupEmailVC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if let txt = textView.text, txt == GroupEmailDescription.localized(){
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
            textView.text = nil
        }else{
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if let txt = textView.text, txt == ""{
            textView.textColor = AppColors.grayColor
            textView.text = GroupEmailDescription.localized()
        }else{
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
            //                UIColor(hex: "#969696")
        }
    }
}

extension GroupEmailVC: SelectPermissionsViewControllerDelegate {
    func didSelectPermissions(status: Bool, permission: String, members: String, emailNotification: String, priority: Priority) {
////        self.priority = priority
////        self.emailNotify = emailNotification
//        self.permittedMembers = members
//        var selecteMemberNames = [String]()
////        self.addFilePermissions = permission
//        switch members{
//        case "Doctor":
//            let members = AppConfig.shared.threadGroupMembers.filter { $0.preName == "Doctor" }
//            var ids = [Int]()
//            selecteMemberNames.removeAll()
//            for member in members{
//                ids.append(member.userId!)
//                selecteMemberNames.append(member.name)
//            }
//            self.memberIds = ids
//            break
//        case "Health Care Personal":
//            let members = AppConfig.shared.threadGroupMembers.filter { $0.preName == "Health Care Personal" }
//            var ids = [Int]()
//            selecteMemberNames.removeAll()
//            for member in members{
//                ids.append(member.userId!)
//                selecteMemberNames.append(member.name)
//            }
//            self.memberIds = ids
//            break
//        case "Standard User":
//            let members = AppConfig.shared.threadGroupMembers.filter { $0.preName == "Standard User" }
//            var ids = [Int]()
//            selecteMemberNames.removeAll()
//            for member in members{
//                ids.append(member.userId!)
//                selecteMemberNames.append(member.name)
//            }
//            self.memberIds = ids
//            break
//        case "custom":
//            let members = AppConfig.shared.threadGroupMembers.filter { $0.isSelected == true }
//            var ids = [Int]()
//            selecteMemberNames.removeAll()
//            for member in members{
//                ids.append(member.userId!)
//                selecteMemberNames.append(member.name)
//            }
//            self.memberIds = ids
//            break
//        default:
//            var ids = [Int]()
//            selecteMemberNames.removeAll()
//            for (i,member) in AppConfig.shared.threadGroupMembers.enumerated(){
//                ids.append(member.userId!)
//                selecteMemberNames.append(member.name)
//            }
//            self.memberIds = ids
//            break
//        }
//
////        for member in AppConfig.shared.threadGroupMembers{
////            if member.isSelected{
////                selecteMemberNames.append(member.name)
////            }
////        }
//        if selecteMemberNames.count > 10 {
//            selecteMemberNames = selecteMemberNames[0...9].map{String($0)}
//            selecteMemberNames.append("...")
//        }
//
//        self.initUserTags(users: selecteMemberNames)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .plain, target: self, action: #selector(sendEmail))
    }
}

//VNDocumentCameraViewControllerDelegate
extension GroupEmailVC {
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        print(error)
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        guard scan.pageCount >= 1 else {
            controller.dismiss(animated: true)
            return
        }
        DispatchQueue.main.async {
            let pdfDocument = PDFDocument()
            for i in 0 ..< scan.pageCount {
                if  let image = scan.imageOfPage(at: i).resize(toWidth: 700) {
                    print(image)
                    // Create a PDF page instance from your image
                    let pdfPage = PDFPage(image: image)
                    // Insert the PDF page into your document
                    pdfDocument.insert(pdfPage!, at: i)
                }
            }
            // Get the raw data of your PDF document
            let data = pdfDocument.dataRepresentation()
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let docURL = documentDirectory.appendingPathComponent(Date().getTime()+".pdf")
            do{
                try data?.write(to: docURL)
                
                let fileName = docURL.lastPathComponent
                var directUploadParams = DirectUploadParams()
                directUploadParams.filename = fileName
                directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                directUploadParams.byte_size = data!.count
                
//                print("CreateThreadVC->directUploadParams", directUploadParams)
//                self.uploadForSignedId(directUploadParams, data!, nil)
                self.showScannedFileRenamePopUp(directUploadParams, data!, nil)
            }catch(let error)
            {
                print("error is \(error.localizedDescription)")
            }
        }
        controller.dismiss(animated: true)
    }
}

extension GroupEmailVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        emailSubjectField.tintColor = theme.settings.titleColor
        emailSubjectField.textColor = theme.settings.titleColor
        emailContextTextView.backgroundColor = theme.settings.highlightedBgColor
        emailContextTextView.tintColor = theme.settings.titleColor
        emailContextTextView.textColor = theme.settings.titleColor
    }
}

enum RecipientsFor{
    case invitation
    case groupEmail
}

struct GroupEmailParams {
    var clinic_id : Int!
    var title : String!
    var body : String!
}
enum SelectionType{
    case thread
    case groupMail
}
