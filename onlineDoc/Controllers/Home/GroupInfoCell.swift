//
//  GroupInfoCell.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 14/07/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

protocol GroupInfoCellDelegate: AnyObject {
    func didTapInvitation()
    func didTapMember()
    func didTapListChange(tag:Int)
    func didOnNotification(isOn:Bool)
    func didToggleDisplayTimeline(isOn:Bool)
    func didTapGroupEmail()
}

class GroupInfoCell: UITableViewCell {
    
    @IBOutlet weak var showThreadsOnHomeScreenToggleSwitch: UISwitch!
    @IBOutlet weak var onOffNotificationSwitch: UISwitch!
    @IBOutlet weak var showOnDashboardLabel: UILabel!
    @IBOutlet weak var lblNotificationActivation: UILabel!
    @IBOutlet weak var showThreadsButton: UIButton!
    @IBOutlet weak var showFoldersButton: UIButton!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var memberCountLabel: UILabel!
    @IBOutlet weak var privacyIcon: UIImageView! {
        didSet {
            let image = UIImage(named: "private")?.withRenderingMode(.alwaysTemplate)
            privacyIcon.image = image
        }
    }
    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var memberSearchBar: UISearchBar!
    @IBOutlet weak var groupImageView: UIImageView!
    @IBOutlet weak var uiUnread: UIView!
    @IBOutlet weak var uiNotification: UIView!
    @IBOutlet weak var uiShowOnDashboard: UIView!
    @IBOutlet weak var uiNotificationHeight: NSLayoutConstraint!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var groupMailImageView: UIImageView!{
        didSet{
            let image = UIImage(named: "invitation")?.withRenderingMode(.alwaysTemplate)
            groupMailImageView.image = image
            groupMailImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.groupMail)))
        }
    }
    
    weak var delegate:GroupInfoCellDelegate?
    var makefolderNameEmpty: ((Bool) -> ())?
    var groupID = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnInvite.setTitle(inviteButtonTitle.localized(), for: .normal)
        showThreadsButton.setTitle("txt_threads".localized().capitalized, for: .normal)
        showFoldersButton.setTitle("txt_Folders".localized().capitalized, for: .normal)
        //Generic theme
        Themer.shared.register(
            target: self,
            action: GroupInfoCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: GroupInfoCell.setTheme)
        
        showThreadsButton.underline()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
//        contentView.frame = contentView.frame.inset(by: margins)
//        self.layoutIfNeeded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - IBActions
    @objc func groupMail(){
        self.delegate?.didTapGroupEmail()
    }
    
    @IBAction func didTapShowThreadsOnHomeScreenSwitch(_ sender: UISwitch) {
        showOnTimeline(show: sender.isOn)
    }
    
    @IBAction func onClickNotificationSwitch(_ sender: UISwitch) {
        callGroupNotificationAPI(onOff: sender.isOn ? true : false)
    }
    
    private func callGroupNotificationAPI(onOff: Bool){
        RequestManager.shared.callNotificationAPI(groupID: groupID, notification: onOff) { (status, response) in
            print("callNotificationAPI -> \(String(describing: response))")
            self.delegate?.didOnNotification(isOn: onOff)
        }
    }
    
    private func showOnTimeline(show: Bool) {
        RequestManager.shared.toggleDisplayTimeline(groupID: groupID) { (status, message) in
            self.delegate?.didToggleDisplayTimeline(isOn: show)
        }
    }
    
    func setGroupInfo(data: GroupModel){
        if let id = data.id {
            self.groupID = id
        }
        if data.id == publicGroupID {
            uiNotification.isHidden = true
            uiNotificationHeight.constant = 0
        }
        
        if let groupImage = data.file?.src, groupImage != ""{
            if let url = URL(string: groupImage){
                self.groupImageView.contentMode = .scaleAspectFill
                self.groupImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "groupDefault"))
            }else{
                self.groupImageView.image = #imageLiteral(resourceName: "groupDefault")
                self.groupImageView.contentMode = .scaleAspectFill
            }
        }else{
            self.groupImageView.image = #imageLiteral(resourceName: "groupDefault")
            self.groupImageView.contentMode = .scaleAspectFill
        }
        
        if let membersCount = data.joined_members {
            if membersCount == 1 {
                self.memberCountLabel.text = String(membersCount) + " " + "Member".localized()
            }else{
                self.memberCountLabel.text = String(membersCount) + " " + "Members".localized()
            }
        }
        
        if let groupPrivacy = data.is_public {
            switch groupPrivacy{
            case true:
                self.privacyIcon.image = #imageLiteral(resourceName: "public").withRenderingMode(.alwaysTemplate)
            default:
                self.privacyIcon.image = #imageLiteral(resourceName: "private").withRenderingMode(.alwaysTemplate)
            }
        }
        
        if let desc = data.description {
            self.descriptionLabel.text = desc
        } else {
            self.descriptionLabel.text = ""
        }
        
        self.lblNotificationActivation.text = "Follow this group".localized()
        self.showOnDashboardLabel.text = "Show on dashboard".localized()
        
        if data.is_display_timeline ?? false {
            showThreadsOnHomeScreenToggleSwitch.setOn(true, animated: true)
        }
        else {
            showThreadsOnHomeScreenToggleSwitch.setOn(false, animated: true)
        }
        
        if let notification = data.is_notification_clinic, notification {
            onOffNotificationSwitch.setOn(true, animated: true)
        }
        else {
            onOffNotificationSwitch.setOn(false, animated: true)
        }
        
        if let count = data.clinic_notifications_count, count > 0 {
            self.uiUnread.isHidden = false
        }
        else {
            self.uiUnread.isHidden = true
        }
        
        if data.can_send_invite == WhoCanUpdate.EVERYONE.rawValue {
            self.btnInvite.isHidden = false
        } else {
            if let admins = data.clinic_admins, admins.contains(where: { $0.id == AppConfig.shared.user.id! }) {
                self.btnInvite.isHidden = false
            }
        }
        
        self.groupMailImageView.isHidden = true
        if let admins = data.clinic_admins, admins.contains(where: { $0.id == AppConfig.shared.user.id! }) {
            self.groupMailImageView.isHidden = false
        }
    }
    
    @IBAction func invitationTapped(){
        self.delegate?.didTapInvitation()
    }
    
    @IBAction func onClickMember(){
        self.delegate?.didTapMember()
    }
    
    @IBAction func listChangeTapped(_ sender : UIButton){
        if sender.tag == 2{
            showThreadsButton.underline()
            showFoldersButton.removeUnderline()
            (self.makefolderNameEmpty)?(true)  // this means make folderName variable in GroupInfoVC empty
        }else{
            showFoldersButton.underline()
            showThreadsButton.removeUnderline()
        }
        self.delegate?.didTapListChange(tag: sender.tag)
    }
    
}

extension GroupInfoCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        contentView.backgroundColor = theme.settings.cellColor
        uiNotification.backgroundColor = theme.settings.highlightedBgColor
        uiShowOnDashboard.backgroundColor = theme.settings.highlightedBgColor
        btnInvite.setTitleColor(theme.settings.titleColor, for: .normal)
        showThreadsButton.setTitleColor(theme.settings.titleColor, for: .normal)
        showFoldersButton.setTitleColor(theme.settings.titleColor, for: .normal)
        privacyIcon.tintColor = theme.settings.titleColor
        groupMailImageView.tintColor = theme.settings.titleColor
    }
}
