//
//  GroupInfoVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 15/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
protocol GroupInfoViewControllerDelegate: AnyObject {
    func didUpdateGroup(group: GroupModel)
    //    func didTapCategory(with id:Int)
}

class GroupInfoVC: BaseVC {
    var groupById = 0
    var loadMore = true
    var pageNumber = 1
    var threadsData = [ThreadModel]()
    var categoryThreadsData = [ThreadModel]()
    var categoryThreadPageNumber = 1
    
//    var folderName = "" // added by nosher
    var visibleSection = 0
    var navigatingFrom : VisitFrom = .none
    var memberIndex = 0
    var selectedIndexRow = 0
    var showGroupThreadsOnHomeScreenStatusStr = ""
//    var indexOfThreadToEdit = -1
//    var searchParams = AdvanceSearchOptions()
//    var mainDatSource = [TimeLineData]()
    
    var msgLabel = UILabel()
    var topLabel = UILabel()
    var newThreadWith : AttachmentOption = .none
    var commentWith : CommentWith = .none
//    var CreateOrEditThreadFrom : CreateOrEditThreadFrom  = .home
    var groupCategories = [GroupCategoryModel]()
    var selectedCategoryId = 0 // This is sent to Create VC during thread creation from group landing page (GroupInfoVC)
    var isLoadGroupInfoCall: Bool = true // By Default
    
    var groupMembers = [Member](){
        didSet{
            for (i,item) in groupMembers.enumerated(){
                if item.userId == AppConfig.shared.user.id{
                    groupMembers.remove(at: i)
                }
            }
        }
    }
    
    var groupMembersDataSource = [Member](){
        didSet{
            groupMembers = groupMembersDataSource
        }
    }
    var groupRequestedMembers = [Member]()
    
    var isUpdated = false
    var isThreadsList = true
    var isCategoriesList = false
    var groupInfo = GroupModel(){
        didSet{
            self.setView(with: groupInfo)
            if isUpdated {
                isUpdated = false
                self.delegate?.didUpdateGroup(group: groupInfo)
            }
        }
    }
    
//    lazy var refreshControl: UIRefreshControl = {
//        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action:
//                                    #selector(self.refreshTimeLineData(_:)),
//                                 for: .valueChanged)
//        refreshControl.backgroundColor = .clear
//        refreshControl.tintColor = .white
//        return refreshControl
//    }()
    @objc private func refreshData(_ sender: Any) {
        self.pageNumber = 1
        self.loadMore = true
        refreshControl.beginRefreshing()
        self.loadGroupData()
        //        refreshControl.endRefreshing()
    }
    
    //    var group = GroupModel()
    
    var isAdminUser = false
    weak var delegate : GroupInfoViewControllerDelegate?
    //MARK:- IBOUTLETs
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var memberCountLabel: UILabel!
    @IBOutlet weak var showThreadOnHomeScreenLbl: UILabel!
    @IBOutlet weak var privacyIcon: UIImageView!
    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var groupImageView: UIImageView!
    @IBOutlet weak var showThreadsOnHomeScreenToggleSwitch: UISwitch!
    @IBOutlet weak var tableViewTopWhenSearchBarHidden: NSLayoutConstraint!
    @IBOutlet weak var tableViewTopWhenSearchBarVisible: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnBecomeAdmin: UIButton!
    
    var hasUnread: Bool = false
    var hasUnreadBackup: Bool?
    var needToReload: ((Bool)->(Void))?
    var isNotifiedToHome = false
    var isVisited = false
    
    //MARK: -VIEW LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        print("GroupInfoVC->groupById", groupById)
        self.setupTBL()
        self.loadGroupData()
        
        btnBecomeAdmin.setTitle("Become Admin".localized(), for: .normal)
        
        self.setupNotificationObservers()
//        self.setSocket()
    }
    
//    private func setSocket() {
//        socketManager.delegate = self
//    }
    
    private func setupNotificationObservers(){
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifyThread, object: nil, queue: .main) { [weak self] (notification) in
            guard let strongSelf = self else { return }
            if let newThread = notification.userInfo?["Thread"] as? ThreadModel {
                if let notify = notification.userInfo?["Notify"] as? NotifyCRUD {
                    switch notify {
                    case .create:
                        strongSelf.threadsData.insert(newThread, at: 0)
                        if strongSelf.isThreadsList {
                            strongSelf.tableView.setEmptyMessage("")
                            strongSelf.tableView.insertRows(at: [IndexPath(row: 1, section: 1)], with: .automatic)
                        }
                    case .update:
                        strongSelf.threadsData[strongSelf.selectedIndexRow] = newThread
                        if strongSelf.isThreadsList {
                            strongSelf.tableView.reloadRows(at: [IndexPath(row: strongSelf.selectedIndexRow+1, section: 1)], with: .automatic)
                        }
                    case .delete:
                        strongSelf.threadsData.remove(at: strongSelf.selectedIndexRow)
                        if strongSelf.isThreadsList {
                            strongSelf.tableView.deleteRows(at: [IndexPath(row: strongSelf.selectedIndexRow+1, section: 1)], with: .automatic)
                        }
                    default:
                        break
                    }
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .notifyGroupMember, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else {return}
            if let _ = notification.userInfo?["Group"] as? GroupModel, let isAdded = notification.userInfo?["IsAdded"] as? Bool, let members = notification.userInfo?["Members"] as? Int {
                if isAdded {
                    self.groupInfo.joined_members = (self.groupInfo.joined_members ?? 0) + members
                }
                else {
                    self.groupInfo.joined_members = (self.groupInfo.joined_members ?? 1) - 1
                }
                self.tableView.reloadSections([0], with: .none)
            }
        }
        
        NotificationCenter.default.addObserver(forName: .notifySocket, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else {return}
            if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .CLINIC {
                if let notify = notification.userInfo?["Notify"] as? NotifySocket {
                    switch notify {
                    case .receiveComment:
                        if let data = notification.userInfo?["Data"] as? SocketCommentBaseModel {
                            if data.comment?.data?.clinic_id != self.groupById { return }
                            if let _data = data.comment?.data, let index = self.threadsData.firstIndex(where: { $0.id == _data.doc_id }) {
                                if data.type == "new" {
                                    if let _ = self.threadsData[index].clinic_doc_comments {
                                        self.threadsData[index].clinic_doc_comments?.insert(_data, at: 0)
                                    }
                                    else {
                                        self.threadsData[index].clinic_doc_comments = [_data]
                                    }
                                    let commentCount = (self.threadsData[index].clinic_doc_comments_count ?? 0) + 1
                                    self.threadsData[index].clinic_doc_comments_count = commentCount
                                }
                                else if let commentIndex = self.threadsData[index].clinic_doc_comments?.firstIndex(where: { $0.id == _data.id }) {
                                    self.threadsData[index].clinic_doc_comments?[commentIndex] = _data
                                }
                                
                                if self.isThreadsList {
                                    DispatchQueue.main.async {
                                        self.tableView.reloadRows(at: [IndexPath(row: index+1, section: 1)], with: .none)
                                    }
                                }
                            }
                        }
                    default:
                        break
                    }
                }
            }
        }
    }
    
    deinit {
        print("GroupInfoVC->deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    func resetScroll(){
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    //MARK: -CLASS METHODS
    @objc func syncThreadComments(_ notification:Notification){
    }
    
    func loadGroupData(){
        isLoadGroupInfoCall = true
        if self.groupById != 0 {
            DispatchQueue.global(qos: .background).async {
                RequestManager.shared.groupInfo(by: self.groupById) { (status, response) in
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        if let responseData = response as? GroupInfoResponse {
                            if let groupInfo = responseData.result {
                                if !self.isVisited {
                                    self.isVisited = true
                                    groupInfo.last_visit = Date().getTime()
                                    NotificationCenter.default.post(name: .notifyGroup, object: nil, userInfo: ["Group": groupInfo, "Notify": NotifyCRUD.update])
                                }
                                self.setupView(groupInfo)
                                self.loadTimeline()
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func setupView(_ groupInfo: GroupModel) {
        self.groupInfo = groupInfo
        self.tableView.reloadData()
        if let admins = self.groupInfo.clinic_admins, !admins.isEmpty {
            self.btnBecomeAdmin.isHidden = true
        } else {
            self.btnBecomeAdmin.isHidden = false
        }
    }
    
    @IBAction func onClickBecomeAdmin(_ sender: UIButton) {
        if let id = self.groupInfo.id {
            RequestManager.shared.becomeAdmin(clinicId: id) { (status, response) in
                if status{
                    self.btnBecomeAdmin.isHidden = true
                    self.isAdminUser = true
                    self.setAdminButton()
                    self.showAlert(To: "", for: "You are now admin.".localized()) {_ in }
                }
            }
        }
    }
    
    func loadTimeline() {
        print("GroupInfoVC->loadTimeline", self.pageNumber)
        if self.pageNumber == 1 {
            self.threadsData.removeAll()
        }
        self.tableView.setEmptyMessage("")
        var params = TimelineParams()
        params.clinic_ids = [self.groupById]
        params.page = self.pageNumber
        RequestManager.shared.timeline(params: params) { (status, response) in
            self.refreshControl.endRefreshing()
            if status {
                if let response = response as? TimeineResult {
                    if let data = response.data, data.isEmpty, self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                    }
                    if let dataSource = response.data {
                        self.threadsData.append(contentsOf: dataSource)
                        if dataSource.count == 0 {
                            self.loadMore = false
                        }
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func setView(with data: GroupModel) {
        if let groupCategories = data.clinic_categories {
            self.groupCategories = groupCategories.filter{ $0.is_available && $0.is_active }
        }
        
        if let title = data.name {
            self.navigationItem.title = title
        }
        
        if let admins = data.clinic_admins, let userId = AppConfig.shared.user.id, admins.contains(where: { $0.id == userId }) {
            self.isAdminUser = true
            self.setAdminButton()
        }
    }
    
    @objc func setAdminButton(){
        let button = UIButton(type: .custom)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 10);
        button.setImage(#imageLiteral(resourceName: "admin"), for: .normal)
        button.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
        button.addTarget(self, action: #selector(adminButtonTapped), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItems = [barButtonItem]
    }
    
    @objc func adminButtonTapped(){
        self.performSegue(withIdentifier: "EditGroupInfoSegue", sender: nil)
    }
    
    func gotoUserProfile(_ index: IndexPath) {
        let thread = self.threadsData[index.row - 1]
        if let id = thread.doc_creator?.id, id != AppConfig.shared.user.id {
            self.previewUser(id: id)
        }
    }
    
    func gotoCreateThreadScene(){
        if let vc = getVC(storyboard: .CREATE_THREAD, vcIdentifier: "CreateThreadScene") as? CreateThreadVC {
            vc.formMode = .new
            vc.createOrEditThreadFrom = .group
            vc.groupTitle = self.groupInfo.name ?? ""
            vc.groupId = self.groupInfo.id ?? 0
//            vc.isFromGroupInfoScreen = true // this check is used separately from the check in below line
//            vc.cameToCreateThreadVCFromGroupInfoScreen = true
            vc.createThreadWith = self.newThreadWith
//            vc.folderName = self.folderName
            if isCategoriesList {
                vc.categoryId = self.selectedCategoryId
            }
            self.navigateVC(vc)
        }
    }
    
    func update(thread at: IndexPath) {
        self.selectedIndexRow = at.row - 1
        let thread = self.threadsData[self.selectedIndexRow]
        let setNotification = !(thread.doc_notification_on ?? false)
        if let id = thread.id {
            RequestManager.shared.callNotificationAPI(groupID: self.groupById, notification: setNotification, docId: id) { (status, response) in
                if status {
                    self.threadsData[at.row - 1].doc_notification_on = setNotification
//                    self.tableView.reloadRows(at: [at], with: .automatic)
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadsData[self.selectedIndexRow], "Notify": NotifyCRUD.update])
                }
            }
        }
    }
    
    func showThreadEditOptions(for index:IndexPath, isEdit:Bool, isDelete: Bool){
        if isEdit || isDelete {
            var alert = AlertModel()
            if isEdit {
                var editBtn = AlertBtnModel()
                editBtn.title = "Edit"
                alert.btns.append(editBtn)
            }
            if isDelete {
                var deleteBtn = AlertBtnModel()
                deleteBtn.title = "Delete"
                alert.btns.append(deleteBtn)
            }
            var cancelBtn = AlertBtnModel()
            cancelBtn.title = "Cancel"
            cancelBtn.color = .red
            alert.btns.append(cancelBtn)
            self.showMultiBtnAlert(model: alert) { [weak self] (data) in
                guard let `self` = self else {return}
                if let btn = data as? String {
                    if btn == "Edit" {
                        self.editThread(at: index)
                    }
                    else if btn == "Delete" {
                        self.deleteThread(at: index)
                    }
                }
            }
        }
    }
    
    
    func editThread(at index:IndexPath){
        self.selectedIndexRow = index.row - 1
        if let vc = getVC(storyboard: .CREATE_THREAD, vcIdentifier: "CreateThreadScene") as? CreateThreadVC {
            vc.formMode = .edit
            vc.createOrEditThreadFrom = .group
            vc.groupTitle = self.groupInfo.name ?? ""
            vc.groupId = self.groupInfo.id ?? 0
            
//            vc.isFromGroupInfoScreen = true // this check is used separately from the check in below line
//            vc.cameToCreateThreadVCFromGroupInfoScreen = true
//            vc.createThreadWith = self.newThreadWith
//            vc.CreateOrEditThreadFrom = self.CreateOrEditThreadFrom
//            vc.folderName = self.folderName
//            vc.folderID = self.selectedCategoryId
//            vc.threadId = self.threadsData[self.selectedIndexRow].id
            vc.threadToEdit = self.threadsData[self.selectedIndexRow]
            self.navigateVC(vc)
        }
    }
    
    func deleteThread(at index:IndexPath){
        self.showAlert(To: "", for: deleteThreadMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                self.delete(at: index)
            }
        }
    }
    
    func delete(at index: IndexPath) {
        self.selectedIndexRow = index.row - 1
        let thread = self.threadsData[self.selectedIndexRow]
        if let id = thread.id {
            RequestManager.shared.deleteThread(id: id) { (status, response) in
                if status{
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": thread, "Notify": NotifyCRUD.delete])
                    self.showAlert(To: DeleteThreadHeading, for: DeleteThreadMessage.localized()) {_ in}
                }
            }
        }
    }
    
    func handleInvitation(){
        let groupData = self.groupInfo
        groupData.isSelected = true
        
        //        if AppConfig.shared.user.id == self.groupInfo.clinicAdmin{
        //            AppConfig.shared.didAdminSentInvitation = true
        //        }
        
        if let vc = getVC(storyboard: .INVITATION, vcIdentifier: String(describing: InviteVC.self)) as? InviteVC {
            vc.invitationType = .group
            vc.groups = [groupData]
            self.navigateVC(vc)
        }
    }
    
    // MARK: - NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditGroupInfoSegue"{
            let vc = segue.destination as? EditGroupInfoVC
            vc?.groupInfo = self.groupInfo
            vc?.delegate = self
        }
        else if segue.identifier == "threadDetailsSegue" {
            let vc = segue.destination as? ThreadDetailVC
            vc?.threadId = self.threadsData[self.selectedIndexRow].id!
            vc?.commentWith = self.commentWith
            vc?.delegate = self
            
            let thread = self.threadsData[self.selectedIndexRow]
            if let id = self.groupInfo.id, (thread.doc_notification_count ?? 0) > 0 {
                self.postNotifyToGroupUnread(groupId: id, notifications: thread.doc_notification_count!)
            }
        }
        else if segue.identifier == "GroupMembersVC" {
            if let vc = segue.destination as? GroupMembersVC {
                vc.groupInfo = self.groupInfo
                vc.onLeave = {
                    self.needToReload?(self.navigatingFrom == .groups)
                    self.dismissVC()
                }
            }
        }
        else if segue.identifier == "GroupEmailSegue"{
            let dvc = segue.destination as? GroupEmailVC
            dvc?.groupId = self.groupById
        }
    }
    
    func show(page number: Int, of section: Int){
        self.loadCategoryDocs(of: section, pageNumber: number)
    }
    
    @objc func sectionTap(_ sender:UITapGestureRecognizer){
        if let view = sender.view{
            self.visibleSection = view.tag+1
            for (i,_) in self.groupCategories.enumerated(){

                if self.groupCategories[i].isChecked{
                    if self.groupCategories[i].id == self.groupCategories[view.tag].id{
                        print("same section is called")
                        self.groupCategories[i].isChecked = false
//                        self.tableView.reloadSections([self.visibleSection], with: .none)
                        self.tableView.reloadData()
                        return
                    }
                    self.groupCategories[i].isChecked = false
                }
            }
            
//            var catId = 0

            if let id = self.groupCategories[view.tag].id {
//                self.folderName = self.groupCategories[view.tag].name ?? ""
                self.selectedCategoryId = id
//                catId = id
            }
            self.groupCategories[view.tag].isChecked = !self.groupCategories[view.tag].isChecked
//            self.groupCategories[view.tag].params.folderIds = [catId]
            self.loadCategoryDocs(of: self.visibleSection, pageNumber: 1)
        }
    }
    
    private func loadCategoryDocs(of section: Int = 0, pageNumber: Int) {
        self.categoryThreadPageNumber = pageNumber
        RequestManager.shared.loadCategoryDocs(catId: self.selectedCategoryId, page: pageNumber) { (status, response) in
            if status{
                if let data = response as? ClinicDocModel {
                    if let dataSource = data.result {
                        self.categoryThreadsData = dataSource
                        if self.isCategoriesList {
//                            self.tableView.reloadSections([section], with: .none)
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
}

// MARK: - EXTENSIONS
extension GroupInfoVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            if self.isCategoriesList {
                if self.groupCategories[section - 1].isChecked{
                    return self.categoryThreadsData.count
                }else{
                    return 0
                }
            }
            else if self.isThreadsList {
                return self.threadsData.count + 1
            }
            else{
                return 0
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isCategoriesList{
            return self.groupCategories.count + 1
        }
        else if self.isThreadsList {
            return 2
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GroupInfoCell.self), for: indexPath) as? GroupInfoCell else {
                print("Unable to Deqeue Thread Detail Cell")
                return self.emptyTblCell()
            }
            cell.makefolderNameEmpty = { [weak self] state in
                if (state) {
                    guard let strongSelf = self else { return }
//                    strongSelf.folderName = ""
                    strongSelf.selectedCategoryId = 0
                }
            }
            cell.setGroupInfo(data: self.groupInfo)
            cell.delegate = self
            return cell
        } else {
            if (self.isCategoriesList) {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FolderThreadCell.self)) as? FolderThreadCell else {
                    return self.emptyTblCell()
                }
                cell.configCell(indexPath, data: self.categoryThreadsData[indexPath.row])
                return cell
            }
            else if (self.isThreadsList) {
                if (indexPath.row == 0) {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CreateThreadViewCell.self)) as? CreateThreadViewCell else {
                        return self.emptyTblCell()
                    }
                    cell.delegate = self
                    let imgUrl = AppConfig.shared.user.file?.medium_url ?? AppConfig.shared.user.file?.src
                    if let imageUrl = imgUrl, imageUrl != "" {
                        let url = URL(string: imageUrl)
                        cell.myProfileImageView.imageFromServer(imageUrl: url!)
                    }else {
                        cell.myProfileImageView.image = #imageLiteral(resourceName: "ic_user")
                    }
                    return cell
                }
                else{
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GroupThreadCell.self), for: indexPath) as? GroupThreadCell else {
                        return self.emptyTblCell()
                    }
                    if indexPath.row < self.threadsData.count + 1 {
                        cell.index = indexPath
                        cell.setupData(indexPath, data: self.threadsData[indexPath.row - 1])
                        cell.delegate = self
                        cell.onClickProfile = { [weak self] index in
                            guard let strongSelf = self else { return }
                            strongSelf.gotoUserProfile(index)
                        }
                        
                        if self.loadMore && indexPath.row == (self.threadsData.count - 1) {
                            self.pageNumber += 1
                            self.loadTimeline()
                        }
                    }
                    self.view.layoutIfNeeded()
                    return cell
                }
            }
            else {
                preconditionFailure()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section != 0{
            if self.isCategoriesList{
                let headerView = Bundle.main.loadNibNamed(String(describing: FolderViewCell.self), owner: self, options: nil)?.first as? FolderViewCell
                headerView?.delegate = self
//                headerView?.backgroundColor = AppColors.evenRow
                headerView?.titleLabel.text = self.groupCategories[section - 1].name
                
                if let count = self.groupCategories[section - 1].total_doc_count{
                    headerView?.threadsCountLabel.text = "(\(count))"
                }
                headerView?.createButton.isHidden = !self.groupCategories[section - 1].isChecked
                
                if let notificationCount = self.groupCategories[section - 1].category_notification_count, notificationCount > 0{
                    headerView?.counterLabel.text = "\(notificationCount)"
                    headerView?.counterView.isHidden = false
                }else{
                    headerView?.counterView.isHidden = true
                }
                headerView?.groupImageView.image = #imageLiteral(resourceName: "folder").withRenderingMode(.alwaysTemplate)
                headerView?.tag = section - 1
                headerView?.groupImageView.contentMode = .center
                if self.groupCategories[section - 1].isChecked{
                    headerView?.iconImageView.image = #imageLiteral(resourceName: "carrotWhite").withRenderingMode(.alwaysTemplate)
                }else{
                    headerView?.iconImageView.image = #imageLiteral(resourceName: "carrot").withRenderingMode(.alwaysTemplate)
                }
                headerView?.iconImageView.contentMode = .center
//                headerView?.contentView.backgroundColor = AppColors.evenRow
                headerView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sectionTap)))
                return headerView
            }
            else {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section != 0 {
            if self.isCategoriesList {
                if self.groupCategories[section - 1].isChecked {
                    return 90
                }else{
                    return 50
                }
            }
            else {
                return 0
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
            if self.isCategoriesList {
                if indexPath.row < self.categoryThreadsData.count {
                    let count = self.categoryThreadsData[indexPath.row].doc_notification_count
                    if let count = count, count > 0 {
                        if let totalDocCount = self.groupCategories[indexPath.section - 1].category_notification_count {
                            if let _ = self.groupInfo.clinic_notifications_count {
                                self.groupInfo.clinic_notifications_count = self.groupInfo.clinic_notifications_count! - totalDocCount
                                self.groupInfo.group_notification_count = self.groupInfo.clinic_notifications_count
                            }
                            self.groupCategories[indexPath.section - 1].category_notification_count = totalDocCount < count ? 0 : (totalDocCount - count)
                        }
                        
                        self.categoryThreadsData[indexPath.row].doc_notification_count = 0
                        self.tableView.reloadData()
                    }
                    if let vc = getVC(storyboard: .TABS, vcIdentifier: "DetailView") as? ThreadDetailVC {
                        vc.threadId = self.categoryThreadsData[indexPath.row].id!
                        self.navigateVC(vc)
                        
                        if let id = self.groupInfo.id, (count ?? 0) > 0 {
                            self.postNotifyToGroupUnread(groupId: id, notifications: count!)
                        }
                    }
                }
            }
            else if (isThreadsList) {
                if indexPath.row == 0 {
                    self.newThreadWith = .none
                    self.gotoCreateThreadScene()
                }else{
                    self.selectedIndexRow = indexPath.row - 1
//                    self.indexOfThreadToEdit = indexPath.row - 1
                    performSegue(withIdentifier: "threadDetailsSegue", sender: self)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.isCategoriesList {
            if let count = self.groupCategories[section - 1].total_doc_count, count > 5 {
                let footerView = Bundle.main.loadNibNamed("PaginationFooter", owner: self, options: nil)?.first as? PaginationFooter
                footerView?.section = section - 1
                footerView?.pageNumber = self.categoryThreadPageNumber
                footerView?.totalThreads = count
                
                footerView?.delegate = self
                return footerView
            }else{
                return nil
            }
        }else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section != 0 {
            if isCategoriesList {
                if self.groupCategories[section - 1].isChecked, let count = self.groupCategories[section - 1].total_doc_count, count > 5 {
                    return 40
                }else{
                    return 0
                }
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName: String(describing: GroupInfoCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: GroupInfoCell.self))
        tableView.register(UINib(nibName: String(describing: CreateThreadViewCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: CreateThreadViewCell.self))
        tableView.register(UINib(nibName: String(describing: GroupThreadCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: GroupThreadCell.self))
        tableView.register(UINib(nibName: String(describing: FolderThreadCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: FolderThreadCell.self))
        
        self.tableView.separatorStyle = .none
        self.tableView.addSubview(refreshControl)
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.tableView.rowHeight = UITableView.automaticDimension
        if #available(iOS 15.0, *) {
            self.tableView.sectionHeaderTopPadding = 0.0
        }
        
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }
}

extension GroupInfoVC: EditGroupInfoViewControllerDelegate{
    func didDeleteGroup() {
        isLoadGroupInfoCall = false // Call LoadGroupInfo Call only when group is present and when group is deleted, GroupINfoCall should not be called. It is being checked in ViewDidLoad Method
        //        NotificationCenter.default.post(name: ._visitGroup, object: nil, userInfo: ["groupId": self.groupInfo.id, "isLeave": true])
        self.dismissVC()
    }
    
    func didUpdateGroup(groupInfo: GroupModel?) {
        self.isUpdated = true
        if groupInfo != nil {
            self.groupInfo = groupInfo!
            self.tableView.reloadData()
        } else {
            self.loadGroupData()
        }
    }
}

extension GroupInfoVC: PaginationFooterDelegate{
    func didTap(page number: Int, section sect: Int) {
        self.show(page: number, of: sect) //page start from one
    }
}

extension GroupInfoVC : SearchCellDelegate, UISearchBarDelegate{
    func didTapSearch(text: String) {
        guard !text.isEmpty else {
            self.groupMembers = self.groupMembersDataSource
            self.tableView.reloadData()
            return
        }
        self.groupMembers = self.groupMembersDataSource.filter{  ($0.name)!.lowercased().contains(text.lowercased())}
        self.tableView.reloadData()
    }
    
    func didEndEditing() {
        self.groupMembers = self.groupMembersDataSource
        self.tableView.reloadData()
    }
}

extension GroupInfoVC: GroupInfoCellDelegate{
    func didToggleDisplayTimeline(isOn: Bool) {
        self.groupInfo.is_display_timeline = isOn
    }
    
    func didOnNotification(isOn: Bool) {
        self.refreshData(UIButton())
        if self.groupInfo.is_display_timeline ?? false {
            NotificationCenter.default.post(name: .notifyGroupNotification, object: nil, userInfo: nil)
        }
    }
    
    func didTapListChange(tag: Int) {
        if tag == 0{
            self.isThreadsList = false
            self.isCategoriesList = true
            self.tableView.reloadData()
        }
        else if tag == 2 {
            isThreadsList = true
            isCategoriesList = false
            self.tableView.reloadData()
        }
    }
    
    func didTapInvitation() {
        self.handleInvitation()
    }
    
    func didTapMember() {
        performSegue(withIdentifier: "GroupMembersVC", sender: self)
    }
    
    func didTapGroupEmail() {
        self.performSegue(withIdentifier: "GroupEmailSegue", sender: self)
    }
}

extension GroupInfoVC: CreateThreadViewCellDelegate {
    
    func createThreadWith(type: Int) {
        switch type {
        case 1:
            self.newThreadWith = .library
            break
        case 2:
            self.newThreadWith = .camera
            break
        case 3:
            self.newThreadWith = .docs
            break
        case 4:
            self.newThreadWith = .video
            break
        case 5:
            self.newThreadWith = .scan
            break
            
        default:
            self.newThreadWith = .none
            break
        }
        self.gotoCreateThreadScene()
    }
}

extension GroupInfoVC: HomeTableViewCellDelegate {
    func didTapShowMore(forThread at: IndexPath) {
        self.threadsData[at.row-1].showAllComments = !self.threadsData[at.row-1].showAllComments
        self.tableView.reloadRows(at: [at], with: .automatic)
    }
    
    func didTapNotification(for threadAtIndex: IndexPath) {
        self.update(thread: threadAtIndex)
    }
    
    func didTapComment(with: Int, at index: IndexPath) {
        self.selectedIndexRow = index.row - 1
        switch with {
        case 0:
            self.commentWith = .cam
            break
        case 1:
            self.commentWith = .smiley
            break
        default:
            self.commentWith = .keyboard
            break
        }
        performSegue(withIdentifier: "threadDetailsSegue", sender: self)
    }
    
    func didTapMoreOptions(for threadAtIndex: IndexPath) {
        let thread = self.threadsData[threadAtIndex.row - 1]
        if let threadOwnerId = thread.doc_creator?.id {
            if AppConfig.shared.user.id != threadOwnerId {
                if let isAdmin = thread.is_admin, isAdmin {
                    self.showThreadEditOptions(for: threadAtIndex, isEdit: false, isDelete: true)
                }
            }
            else {
                self.showThreadEditOptions(for: threadAtIndex, isEdit: true, isDelete: true)
            }
        }
    }
}

extension GroupInfoVC: ThreadDetailVCDelegate{
    func didDeleteThread() {
//        self.threadsData.remove(at: self.selectedIndexRow)
//        self.tableView.deleteRows(at: [IndexPath(row: self.selectedIndexRow+1, section: 1)], with: .automatic)
//        let thread = self.threadsData[self.selectedIndexRow]
//        if let threadID = thread.id {
//            for (i,item) in self.mainDatSource.enumerated(){
//                if item.threadId == threadID{
//                    self.mainDatSource.remove(at: i)
//                }
//            }
//            for (i,item) in self.threadsData.enumerated(){
//                if item.threadId == threadID{
//                    self.threadsData.remove(at: i)
//                }
//            }
//        }
//        self.tableView.reloadData()
    }
    
    func didCommentOnthread() {
//        if self.threadsData.indices.contains(self.selectedIndexRow){
//            if let fireBaseID = self.threadsData[self.selectedIndexRow].fireBaseId{
//                self.tableView.reloadData()
//            }
//        }
    }
    
//    func didUpdateThreadFromDetail(with data: ThreadParams) {
        //Update Table Data Source
//        if self.threadsData[indexOfThreadToEdit].fireBaseId != nil{
//            self.threadsData[self.indexOfThreadToEdit].folderName = data.categoryTitle
//            self.threadsData[self.indexOfThreadToEdit].folder = data.categoryId
//            self.threadsData[self.indexOfThreadToEdit].journal = data.threadDescription
//            self.threadsData[self.indexOfThreadToEdit].title = data.threadTitle
//            self.threadsData[self.indexOfThreadToEdit].priority = data.threadPriority
//            //Update main Data Source
//            self.mainDatSource[self.indexOfThreadToEdit].folderName = data.categoryTitle
//            self.mainDatSource[self.indexOfThreadToEdit].folder = data.categoryId
//            self.mainDatSource[self.indexOfThreadToEdit].journal = data.threadDescription
//            self.mainDatSource[self.indexOfThreadToEdit].title = data.threadTitle
//            self.mainDatSource[self.indexOfThreadToEdit].priority = data.threadPriority
//            self.tableView.reloadData()
//        }
//    }
    
    func didUpdateNotificationStatus(with value: Bool) {
        self.threadsData[self.selectedIndexRow].doc_notification_on = value
        self.tableView.reloadRows(at: [IndexPath(row: self.selectedIndexRow+1, section: 1)], with: .none)
//        if self.indexOfThreadToEdit != -1, self.indexOfThreadToEdit < self.threadsData.count {
//            self.threadsData[self.indexOfThreadToEdit].notification = value
//            self.tableView.reloadData()
//        }
    }
}

extension GroupInfoVC: FolderViewCellDelegate {
    func createNew() {
        self.newThreadWith = .none
        self.gotoCreateThreadScene()
    }
}

//extension GroupInfoVC: SocketManagerDelegate {
//    func receiveComment(data: SocketCommentBaseModel) {
//        if data.comment?.data?.clinic_id != self.groupById { return }
//        if let _data = data.comment?.data, let index = self.threadsData.firstIndex(where: { $0.id == _data.doc_id }) {
//            if data.type == "new" {
//                if let _ = self.threadsData[index].clinic_doc_comments {
//                    self.threadsData[index].clinic_doc_comments?.insert(_data, at: 0)
//                }
//                else {
//                    self.threadsData[index].clinic_doc_comments = [_data]
//                }
//                let commentCount = (self.threadsData[index].clinic_doc_comments_count ?? 0) + 1
//                self.threadsData[index].clinic_doc_comments_count = commentCount
//            }
//            else if let commentIndex = self.threadsData[index].clinic_doc_comments?.firstIndex(where: { $0.id == _data.id }) {
//                self.threadsData[index].clinic_doc_comments?[commentIndex] = _data
//            }
//
//            DispatchQueue.main.async {
//                self.tableView.reloadRows(at: [IndexPath(row: index+1, section: 1)], with: .none)
//            }
//        }
//    }
//}

struct DeleteRequest {
    var record = "discard"
    var clinic_id : Int!
    var user_id : Int!
}

struct ResetNotification{
    var doc_id = 0
    var  clinic_id = 0
    var notification = ""
}
