//
//  HomeTableViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 11/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import AVKit
import TRMosaicLayout
import Atributika

@objc protocol HomeTableViewCellDelegate: AnyObject {
    @objc optional func didTapComment(with: Int, at index: IndexPath)
    func didTapMoreOptions(for threadAtIndex: IndexPath)
    func didTapNotification(for threadAtIndex: IndexPath)
    @objc optional func didTapShowMore(forThread at: IndexPath)
    @objc optional func didTapGroupName(index: IndexPath)
}

class GroupThreadCell: UITableViewCell {
    
    //MARK: - IBOutlets for the cell
    @IBOutlet weak var commentsContainer: UIView!{
        didSet{
            commentsContainer.sizeToFit()
        }
    }
    
    @IBOutlet weak var priorityView : UIView!{
        didSet{
            priorityView.roundedCorner([.topLeft], radius: 3)
        }
    }
    @IBOutlet weak var interactionBlockerView:UIView!
    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var priorotyWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var greaterNumberOfCommentsCommentsConstraint: NSLayoutConstraint!
    @IBOutlet weak var veryFewCommentsConstraint: NSLayoutConstraint!
    @IBOutlet weak var moreButton: UIButton! {
        didSet{
            let image = UIImage(named: "more")?.withRenderingMode(.alwaysTemplate)
            moreButton.setImage(image, for: .normal)
        }
    }
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var commentsStackView: UIStackView!
    @IBOutlet weak var countsView:UIView!
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var threadTitleLabel: UILabel!
    @IBOutlet weak var threadByNameLabel: UILabel!
    @IBOutlet weak var imagesVideosView :UIView!
    @IBOutlet weak var camerabutton :UIButton!
    @IBOutlet weak var showMoreButton :UIButton!
    @IBOutlet weak var filesCountView :UIView!
    @IBOutlet weak var filesCountLabel :UILabel!
    @IBOutlet weak var containerStack: UIStackView!
    @IBOutlet weak var docListStackView:UIStackView!
    @IBOutlet weak var filesListContainer:UIView!
    @IBOutlet weak var docListStackBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var showCommentsButtonView : UIView!
    @IBOutlet weak var threadGroupLabel: UILabel!{
        didSet{
            threadGroupLabel.isUserInteractionEnabled = true
            threadGroupLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapGroupName(_:))))
        }
    }
    @IBOutlet weak var threadByImageView: UIImageView!{
        didSet{
            threadByImageView.layer.cornerRadius = threadByImageView.layer.frame.size.height / 2
            threadByImageView.clipsToBounds = true
            threadByImageView.layer.borderWidth = 0.5
            threadByImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    
    @IBOutlet weak var typeCommentView: UIView!{
        didSet{
            typeCommentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.regulerComment)))
        }
    }
    
    @IBOutlet weak var userImageView: UIImageView!{
        didSet{
            userImageView.layer.cornerRadius = userImageView.frame.size.height / 2
            userImageView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var threadDescriptionLabel: UILabel!
    @IBOutlet weak var threadCommentsCountLabel: UILabel!
    @IBOutlet weak var threadDocumentsCountLabel: UILabel!
    
    @IBOutlet weak var iv1: UIImageView! //if one image
    @IBOutlet weak var ui22: UIView! //if two images
    @IBOutlet weak var iv21: UIImageView!
    @IBOutlet weak var iv22: UIImageView!
    
    @IBOutlet weak var ui33: UIView! //if three images
    @IBOutlet weak var iv31: UIImageView!
    @IBOutlet weak var iv32: UIImageView!
    @IBOutlet weak var iv33: UIImageView!
    
    @IBOutlet weak var ui44: UIView! //if three images
    @IBOutlet weak var iv41: UIImageView!
    @IBOutlet weak var iv42: UIImageView!
    @IBOutlet weak var iv43: UIImageView!
    @IBOutlet weak var iv44: UIImageView!
    
    private let lbl = AttributedLabel()
    
    var index: IndexPath?
    var onClickProfile: ((_ index: IndexPath) -> Void)?
    weak var delegate: HomeTableViewCellDelegate!
    
    var isShowMoreBtn = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblComment.text = CommentPlaceholder.localized()
        self.threadDescriptionLabel.text = nil
        lbl.onClick = { label, detection in
            switch detection.type {
            case .link(let url):
                var urlString = url.absoluteString
                if urlString.hasPrefix("https://") || urlString.hasPrefix("http://") {
                    UIApplication.shared.open(url)
                }
                else {
                    urlString = "https://\(urlString)"
                    if let _url = URL(string: urlString) {
                        UIApplication.shared.open(_url)
                    }
                }
            default:
                break
            }
        }
        self.threadDescriptionLabel.addSubview(lbl)
        self.threadDescriptionLabel.isUserInteractionEnabled = true
        let marginGuide = self.threadDescriptionLabel.layoutMarginsGuide
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lbl.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        lbl.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        lbl.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        lbl.numberOfLines = 5
        
        //Generic theme
        Themer.shared.register(
            target: self,
            action: GroupThreadCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: GroupThreadCell.setTheme)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        contentView.frame = contentView.frame.inset(by: margins)
        self.layoutIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupData(_ indexPath:IndexPath, data: ThreadModel){
        self.index = indexPath
        
        for subview in imagesVideosView.subviews{
            if subview is UIStackView{
                subview.removeFromSuperview()
            }
        }
        if self.docListStackView != nil{
            self.docListStackView.subviews.forEach{ $0.removeFromSuperview()}
        }
        self.commentsStackView.subviews.forEach{ $0.removeFromSuperview()}
        
        let title = data.showAllComments ? showLessTitle.localized() : showAllTitle.localized()
        self.showMoreButton.setTitle(title, for: .normal)
        
        if AppConfig.shared.user.id != data.doc_creator?.id {
            if let isAdmin = data.is_admin, isAdmin {
                self.moreButton.isHidden = false
            }
            else{
                self.moreButton.isHidden = true
            }
        }else{
            if AppConfig.shared.user.id == data.doc_creator?.id {
                self.moreButton.isHidden = false
            }else{
                self.moreButton.isHidden = true
            }
        }
        
        if let priority = data.priority {
            switch priority {
            case Priority.critical.rawValue:
                let newConstraint = priorotyWidthConstraint.constraintWithMultiplier(0.8)
                self.removeConstraint(priorotyWidthConstraint)
                self.addConstraint(newConstraint)
                self.layoutIfNeeded()
                self.priorotyWidthConstraint = newConstraint
                priorityView.backgroundColor = AppColors.critical
                self.priorityLabel.text = criticalPriorityText.localized()
                self.priorityLabel.isHidden = false
                break
            case Priority.important.rawValue:
                let newConstraint = priorotyWidthConstraint.constraintWithMultiplier(0.6)
                self.removeConstraint(priorotyWidthConstraint)
                self.addConstraint(newConstraint)
                self.layoutIfNeeded()
                self.priorotyWidthConstraint = newConstraint
                priorityView.backgroundColor = AppColors.important
                self.priorityLabel.text = importantPriorityText.localized()
                self.priorityLabel.isHidden = false
                break
            case Priority.notice.rawValue:
                let newConstraint = priorotyWidthConstraint.constraintWithMultiplier(0.4)
                self.removeConstraint(priorotyWidthConstraint)
                self.addConstraint(newConstraint)
                self.layoutIfNeeded()
                self.priorotyWidthConstraint = newConstraint
                priorityView.backgroundColor = AppColors.notice
                self.priorityLabel.text = noticePriorityText.localized()
                self.priorityLabel.isHidden = false
                break
            default:
                priorityView.backgroundColor = .clear
                self.priorityLabel.text = nil
                self.priorityLabel.isHidden = true
                break
            }
        }else{
            self.priorityView.backgroundColor = .clear
            self.priorityLabel.text = nil
            self.priorityLabel.isHidden = true
        }
        
        let imgUrl = AppConfig.shared.user.file?.medium_url ?? AppConfig.shared.user.file?.src
        if let imgPath = imgUrl, imgPath != ""{
            if let imgUrl = URL(string: imgPath){
                userImageView.imageFromServer(imageUrl: imgUrl, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                userImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            userImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let notfication = data.doc_notification_on {
            let image = (notfication ? #imageLiteral(resourceName: "bellOn") : #imageLiteral(resourceName: "bellOff")).withRenderingMode(.alwaysTemplate)
            self.notificationButton.setImage(image, for: .normal)
        }
        else {
            self.notificationButton.setImage(#imageLiteral(resourceName: "bellOff").withRenderingMode(.alwaysTemplate), for: .normal)
        }
        
        if data.doc_creator?.id == AppConfig.shared.user.id {
            self.moreButton.isHidden = false
        }else{
            threadByImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.viewProfile)))
        }
        
        if let name = data.doc_creator?.full_name {
            self.threadByNameLabel.text = name
        }else{
            self.threadByNameLabel.text = nil
        }
        
        let creatorImgUrl = data.doc_creator?.file?.medium_url ?? data.doc_creator?.file?.src
        if let img = creatorImgUrl, img != "" {
            if let url = URL(string: img){
                self.threadByImageView.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                self.threadByImageView.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.threadByImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let title = data.title {
            self.threadTitleLabel.attributedText = title.attributedText(fontSize: 16, allStyleFont: AppFont.kMuliSemiBold(16).font())?.attributedString
        }else{
            self.threadTitleLabel.text = nil
        }
        
        if let desc = data.description {
//            self.threadDescriptionLabel.attributedText = desc.style(tags: b, i).styleLinks(link).styleAll(all).attributedString
            lbl.attributedText = desc.attributedText(fontSize: 16, allStyleFont: AppFont.kMuliLight(16).font())

//            let attributedString = NSMutableAttributedString.init(string: desc)
//            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColors.blackColor, range: NSRange(location: 0, length: desc.count))
//            self.threadDescriptionLabel.attributedText = desc
            
            
//            self.threadDescriptionLabel.text = desc
//            self.threadDescriptionLabel.tintColor = .white
            
//            let descString = data.description!.htmlToString
//            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//            let matches = detector.matches(in: descString, options: [], range: NSRange(location: 0, length: descString.utf16.count))
//
//            for match in matches {
//                guard let range = Range(match.range, in: descString) else { continue }
//                let url = descString[range]
//
//                let _range = (descString as NSString).range(of: String(url))
//                let mutableAttributedString = NSMutableAttributedString.init(string: descString)
//                mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.systemBlue, range: _range)
//                self.threadDescriptionLabel.attributedText = mutableAttributedString
//
//                threadDescriptionLabel.isUserInteractionEnabled = true
//                threadDescriptionLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
//            }
        }else{
            self.threadDescriptionLabel.text = nil
        }
        
        if let groupTitle = data.clinic_name {
            self.threadGroupLabel.text = groupTitle
        }else{
            self.threadGroupLabel.text = nil
        }
        
        if let categoryTitle = data.clinic_category_name {
            if let title = self.threadGroupLabel.text, !title.isEmpty {
                self.threadGroupLabel.text = title + " / \(categoryTitle)"
            } else {
                self.threadGroupLabel.text = categoryTitle
            }
        }else{
            self.threadGroupLabel.text = nil
        }
        
        if let timeStamp  = data.created_at {
            self.timeStampLabel.text = (AppConfig.shared.convertToDate(from: timeStamp).getMoment() + AgoText.localized())
        }else{
            self.timeStampLabel.text = ""
        }
        
        if let files = data.doc_files {
            let imagesVideos = files.filter({$0.content_type!.contains("image") || $0.content_type!.contains("video")})
            self.setupImagesVideos(data: imagesVideos)
            
            let otherFiles = files.filter({!$0.content_type!.contains("image") && !$0.content_type!.contains("video")})
            self.loadFilesList(files: otherFiles)
        }
        else {
            self.imagesVideosView.isHidden = true
        }
        
        self.threadDocumentsCountLabel.text = "\((data.clinic_doc_file_count ?? 0) + (data.clinic_doc_comment_file_count ?? 0))"
        self.threadCommentsCountLabel.text = "\(data.clinic_doc_comments_count ?? 0)"
        if let count = data.clinic_doc_comments_count {
            if count > 1 {
                self.threadCommentsCountLabel.text = "\(count)" + "Comments".localized
            } else {
                self.threadCommentsCountLabel.text = "\(count)" + "Comment".localized
            }
        }
        
        var allComments = data.clinic_doc_comments
        allComments = allComments?.reversed()
        if !data.showAllComments {
//            allComments = Array(allComments?.prefix(3) ?? [])
            allComments = allComments?.suffix(3)
        }
        
        if let comments = allComments, !comments.isEmpty {
            for comment in comments {
                let commentView = Bundle.main.loadNibNamed(String(describing: CommentsView.self), owner: self, options: nil)?.first as? CommentsView
                commentView?.setTheme()
                commentView?.delegate = self

                if let commentByUser = comment.comment_creator {
                    commentView?.memberID = commentByUser.id!
                    if let name = commentByUser.full_name?.capitalized {
                        commentView?.profileNameLabel.text = name.capitalized
                    }else{
                        commentView?.profileNameLabel.text = nil
                    }

                    var imgUrl = commentByUser.file?.medium_url ?? commentByUser.file?.src
                    if imgUrl == nil {
                        imgUrl = commentByUser.comment_user_file?.medium_url ?? commentByUser.comment_user_file?.src
                    }
                    if let image = imgUrl, image != ""{
                        if let url = URL(string: image){
                            commentView?.profileImageView.imageFromServer(imageUrl: url)
                        }else{
                            commentView?.profileImageView.image = #imageLiteral(resourceName: "ic_user")
                        }
                    }else{
                        commentView?.profileImageView.image = #imageLiteral(resourceName: "ic_user")
                    }
                }

                if let commentTitle = comment.comment, commentTitle != "" {
                    commentView?.profileCommentLabel.text = nil
                    commentView?.profileCommentLabel.attributedText = commentTitle.attributedText(fontSize: 12, allStyleFont: AppFont.kMuliLight(12).font())?.attributedString
                }else{
                    if let files = comment.files, !files.isEmpty {
                        if files.count > 1 {
                            commentView?.profileCommentLabel.text = "Attachments".localized()
                        }
                        else {
                            commentView?.profileCommentLabel.text = files.first!.name
                        }
                    }
                }
                if comment.created_at != nil {
                    commentView?.profilemomentLabel.text = (AppConfig.shared.convertToDate(from: comment.created_at!).getMoment() + AgoText.localized())
                }
                else {
                    commentView?.profilemomentLabel.text = ""
                }
                self.commentsStackView.addArrangedSubview(commentView!)
            }
        }
        
        if let comments = data.clinic_doc_comments, comments.count <= 3 {
            self.showCommentsButtonView.isHidden = true
            self.greaterNumberOfCommentsCommentsConstraint.priority = .defaultLow
            self.veryFewCommentsConstraint.priority = .defaultHigh
        }else{
            self.showCommentsButtonView.isHidden = !isShowMoreBtn
            self.veryFewCommentsConstraint.priority = isShowMoreBtn ? .defaultLow : .defaultHigh
            self.greaterNumberOfCommentsCommentsConstraint.priority = isShowMoreBtn ? .defaultHigh : .defaultLow
        }
    }
    
    private func setupImagesVideos(data: [FileModel]) {
        if !data.isEmpty {
            //files.filter({$0.content_type!.contains("image") || $0.content_type!.contains("video")})
            self.imagesVideosView.isHidden = false
            if data.count == 1 {
                let isVideo = data.first?.content_type?.contains("video") ?? false
                if isVideo {
                    self.iv1.isHidden = false
                    self.iv1.image = UIImage(named: "ic_video")
                    self.iv1.contentMode = .center
                    self.iv1.backgroundColor = .black
//                    if let path = data.first?.image, let url = URL(string: path) {
//                        let view = UIView()
//                        view.frame = iv1!.frame
//                        let controller = AVPlayerViewController()
//                        let player = AVPlayer(url: url)
//                        controller.player = player
//                        player.rate = 0
//                        player.pause()
//                        controller.view.translatesAutoresizingMaskIntoConstraints = false
//                        controller.view.heightAnchor.constraint(equalToConstant: iv1!.frame.size.height).isActive = true
//                        controller.view.widthAnchor.constraint(equalToConstant: iv1!.frame.size.width).isActive = true
//                        view.clipsToBounds = true
//                        view.addSubview(controller.view)
//                        self.iv1.addSubview(view)
//                    }
                }
                else {
                    self.iv1.isHidden = false
                    let imgUrl = data.first?.medium_url ?? data.first?.src
                    if let img = imgUrl, img != "" {
                        if let url = URL(string: img){
                            self.iv1.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "defaultImage"))
                        }else{
                            self.iv1.image = #imageLiteral(resourceName: "defaultImage")
                        }
                    }else{
                        self.iv1.image = #imageLiteral(resourceName: "defaultImage")
                    }
                }
                
                self.ui22.isHidden = true
                self.ui33.isHidden = true
                self.ui44.isHidden = true
            }
            else if data.count == 2 {
                self.ui22.isHidden = false
                for (index, _img) in data.enumerated() {
                    let image = index == 0 ? self.iv21 : self.iv22
                    let isVideo = _img.content_type?.contains("video") ?? false
                    if isVideo {
                        image?.image = UIImage(named: "ic_video")
                        image?.contentMode = .center
                        image?.backgroundColor = .black
//                        if let path = _img.image, let url = URL(string: path) {
//                            let view = UIView()
//                            view.frame = image!.frame
//                            let controller = AVPlayerViewController()
//                            let player = AVPlayer(url: url)
//                            controller.player = player
//                            player.rate = 0
//                            player.pause()
//                            controller.view.translatesAutoresizingMaskIntoConstraints = false
//                            controller.view.heightAnchor.constraint(equalToConstant: image!.frame.size.height).isActive = true
//                            controller.view.widthAnchor.constraint(equalToConstant: image!.frame.size.width).isActive = true
//                            view.clipsToBounds = true
//                            view.addSubview(controller.view)
//                            image!.addSubview(view)
//                        }
                    }
                    else {
                        let imgUrl = _img.medium_url ?? _img.src
                        if let img = imgUrl, img != "" {
                            if let url = URL(string: img){
                                image?.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "defaultImage"))
                            }else{
                                image?.image = #imageLiteral(resourceName: "defaultImage")
                            }
                        }else{
                            image?.image = #imageLiteral(resourceName: "defaultImage")
                        }
                    }
                }
                self.iv1.isHidden = true
                self.ui33.isHidden = true
                self.ui44.isHidden = true
            }
            else if data.count == 3 {
                self.ui33.isHidden = false
                for (index, _img) in data.enumerated() {
                    let image = index == 0 ? self.iv31 : (index == 1 ? self.iv32 : self.iv33)
                    let isVideo = _img.content_type?.contains("video") ?? false
                    if isVideo {
                        image?.image = UIImage(named: "ic_video")
                        image?.contentMode = .center
                        image?.backgroundColor = .black
//                        if let path = _img.image, let url = URL(string: path) {
//                            let view = UIView()
//                            view.frame = image!.frame
//                            let controller = AVPlayerViewController()
//                            let player = AVPlayer(url: url)
//                            controller.player = player
//                            player.rate = 0
//                            player.pause()
//                            controller.view.translatesAutoresizingMaskIntoConstraints = false
//                            controller.view.heightAnchor.constraint(equalToConstant: image!.frame.size.height).isActive = true
//                            controller.view.widthAnchor.constraint(equalToConstant: image!.frame.size.width).isActive = true
//                            view.clipsToBounds = true
//                            view.addSubview(controller.view)
//                            image!.addSubview(view)
//                        }
                    }
                    else {
                        let imgUrl = _img.medium_url ?? _img.src
                        if let img = imgUrl, img != "" {
                            if let url = URL(string: img){
                                image?.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "defaultImage"))
                            }else{
                                image?.image = #imageLiteral(resourceName: "defaultImage")
                            }
                        }else{
                            image?.image = #imageLiteral(resourceName: "defaultImage")
                        }
                    }
                }
                self.iv1.isHidden = true
                self.ui22.isHidden = true
                self.ui44.isHidden = true
            }
            else {
                self.ui44.isHidden = false
                for (index, _img) in data.enumerated() {
                    let image = index == 0 ? self.iv41 : (index == 1 ? self.iv42 : (index == 2 ? self.iv43 : self.iv44))
                    let isVideo = _img.content_type?.contains("video") ?? false
                    if isVideo {
                        image?.image = UIImage(named: "ic_video")
                        image?.contentMode = .center
                        image?.backgroundColor = .black
//                        if let path = _img.image, let url = URL(string: path) {
                            
//                            let view = UIView()
//                            view.frame = image!.frame
//                            let controller = AVPlayerViewController()
//                            let player = AVPlayer(url: url)
//                            controller.player = player
//                            player.rate = 0
//                            player.pause()
//                            controller.view.translatesAutoresizingMaskIntoConstraints = false
//                            controller.view.heightAnchor.constraint(equalToConstant: image!.frame.size.height).isActive = true
//                            controller.view.widthAnchor.constraint(equalToConstant: image!.frame.size.width).isActive = true
//                            view.clipsToBounds = true
//                            view.addSubview(controller.view)
//                            image!.addSubview(view)
//                        }
                    }
                    else {
                        let imgUrl = _img.medium_url ?? _img.src
                        if let img = imgUrl, img != "" {
                            if let url = URL(string: img){
                                image?.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "defaultImage"))
                            }else{
                                image?.image = #imageLiteral(resourceName: "defaultImage")
                            }
                        }else{
                            image?.image = #imageLiteral(resourceName: "defaultImage")
                        }
                    }
                }
                self.iv1.isHidden = true
                self.ui22.isHidden = true
                self.ui33.isHidden = true
                if data.count > 4 {
                    self.filesCountView.isHidden = false
                    self.filesCountLabel.text = "+\(data.count - 4)"
                }
                else {
                    self.filesCountView.isHidden = true
                    self.filesCountLabel.text = ""
                }
            }
        }
        else {
            self.imagesVideosView.isHidden = true
        }
    }
    
    private func loadFilesList(files: [FileModel]) {
        for file in files{
            if let fileView  = Bundle.main.loadNibNamed("DocumentListView", owner: self, options: nil)?.first as? DocumentListView{
                fileView.setTheme()
                if let title = file.name {
                    fileView.documentTitle.text = title
                }
                if file.content_type?.contains("pdf") ?? false {
                    fileView.iconImageView.image = UIImage(named: "pdfIcon")
                }
                else if file.content_type?.contains("word") ?? false {
                    fileView.iconImageView.image = UIImage(named: "ic_word")
                }
                else if (file.content_type?.contains("sheet") ?? false) || (file.content_type?.contains("excel") ?? false) {
                    fileView.iconImageView.image = UIImage(named: "ic_excel")
                }
                else if file.content_type?.contains("text") ?? false {
                    fileView.iconImageView.image = UIImage(named: "ic_text")
                }
                else {
                    fileView.iconImageView.image = #imageLiteral(resourceName: "attached")
                }
                fileView.tapButton.isEnabled = false
                self.docListStackView.addArrangedSubview(fileView)
                self.docListStackView.isHidden = false
            }
        }
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = threadDescriptionLabel.attributedText?.string else {
            return
        }
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count))

        for match in matches {
            guard let range = Range(match.range, in: text) else { continue }
            let url = text[range]
            
            if let url = URL(string: String(url)) {
                UIApplication.shared.open(url)
            }
        }
    }
    
    @objc func didTapGroupName(_ sender: UITapGestureRecognizer) {
        delegate?.didTapGroupName?(index: self.index!)
    }
    @objc func viewProfile(){
        self.onClickProfile?(self.index!)
    }
    @objc func regulerComment(){
        self.delegate?.didTapComment?(with: -1, at: self.index!)
    }
    
    @IBAction func showMoreComments(){
        self.delegate?.didTapShowMore?(forThread: self.index!)
    }
    
    @IBAction func commentWithCameraOpen(){
        self.delegate?.didTapComment?(with: 0, at: self.index!)
    }
    
    @IBAction func commentWithsmilyBoardOpen(){
        self.delegate?.didTapComment?(with: 1, at:  self.index!)
    }
    
    @IBAction func moreOptions(){
        self.delegate?.didTapMoreOptions(for: self.index!)
    }
    
    @IBAction func changeNotification(){
        self.delegate?.didTapNotification(for: self.index!)
    }
}

extension GroupThreadCell: CommentsViewDelegate{
    func didTapProfileImage(with id: Int) {
//        self.memberCallbacks!(id)
    }
}

extension GroupThreadCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        contentView.backgroundColor = AppColors.grayColor
        containerView.backgroundColor = theme.settings.cellColor
        showMoreButton.backgroundColor = theme.settings.highlightedBgColor
        showMoreButton.setTitleColor(theme.settings.titleColor, for: .normal)
        moreButton.tintColor = theme.settings.titleColor
        notificationButton.tintColor = theme.settings.titleColor
        threadGroupLabel.textColor = theme.settings.subTitleColor
        timeStampLabel.textColor = theme.settings.subTitleColor
        priorityLabel.textColor = theme.settings.whiteColor
        filesCountLabel.textColor = theme.settings.whiteColor
    }
}
