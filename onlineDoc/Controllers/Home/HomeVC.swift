//
//  HomeVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 11/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField
import FirebaseDatabase

class HomeVC: BaseVC {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filtersButton:UIButton!
    
    let socketManager = SocketManager.sharedInstance
    var loadMore = true
    var pageNumber = 1
    var searchedText = ""
    var isSearchOn = false
    
    var threadsData = [ThreadModel]()
    
    var indexRow = 0
    var textToSearch:String!
    var searchBar = UISearchBar()
    var searchParams = TimelineParams()
    var mainDatSource = [TimeLineData]()
    
    var onlineStatusCollection = [OnlineStatus]()
    var msgLabel = UILabel()
    var newThreadWith : AttachmentOption = .none
    var commentWith : CommentWith = .none
    var CreateOrEditThreadFrom : CreateOrEditThreadFrom  = .home
    var indexOfThreadToEdit = -1
    var tappedGroupID = 0
    var userGroups = [GroupModel]()
    
    var overlay: UIView?
    var progressView: UIProgressView?
    var allAPIsProgress = 0 {
        didSet {
            if allAPIsProgress == 1 {
                progressView?.setProgress(0.111, animated: true)
            } else if allAPIsProgress == 3 {
                progressView?.setProgress(0.333, animated: true)
            } else if allAPIsProgress == 4 {
                progressView?.setProgress(0.555, animated: true)
                let loadingLabel = self.overlay?.subviews.compactMap { $0 as? UILabel }[0]
                loadingLabel?.text = "Almost ready...".localized()
            } else if allAPIsProgress == 5 {
                progressView?.setProgress(0.777, animated: true)
            } else if allAPIsProgress == 6 {
                progressView?.setProgress(1, animated: true)
            }
            
            if allAPIsProgress == maxProgress {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.overlay?.isHidden = true
                }
            }
        }
    }
    var maxProgress = 6
    
    @objc private func refreshData(_ sender: Any) {
        self.pageNumber = 1
        self.loadMore = true
        self.refreshControl.beginRefreshing()
        if self.searchedText.isEmpty {
            self.loadTimeline()
        }
        else {
            self.searchTimeline()
        }
        self.callMyGroupAPI()
    }
    
    //MARK: - Method OverRides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.delegate = self
        self.setupTBL()
        if appDelegate.attachements.count == 0 {
//            setupOverlay()
        }
        self.setUpUI()
        self.loadTimeline() //4

        self.callMyGroupAPI() //6
        self.tableView.addSubview(refreshControl)
        
        self.setupNotificationObservers()
        self.setSocket()
    }
    
    private func setSocket() {
        print("Home -> connectToSocket")
        socketManager.connectToSocket()
//        socketManager.delegate = self
    }
    
    deinit {
        print("HomeVC->deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupNotificationObservers(){
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifyThread, object: nil, queue: .main) { [weak self] (notification) in
            guard let strongSelf = self else { return }
            if let newThread = notification.userInfo?["Thread"] as? ThreadModel {
                if let notify = notification.userInfo?["Notify"] as? NotifyCRUD {
                    switch notify {
                    case .create:
                        if let showOnDashboard = notification.userInfo?["ShowOnDashboard"] as? Bool, showOnDashboard {
                            strongSelf.threadsData.insert(newThread, at: 0)
                            strongSelf.tableView.insertRows(at: [strongSelf.getTBLIndexPath(index: IndexPath(row: 0, section: 0))], with: .automatic)
                        }
                    case .update:
                        let indexToEdited = strongSelf.threadsData.firstIndex(where: {$0.id == newThread.id})
                        if let indexToEdited = indexToEdited {
                            strongSelf.threadsData[indexToEdited] = newThread
                            strongSelf.tableView.reloadRows(at: [strongSelf.getTBLIndexPath(index: IndexPath(row: indexToEdited, section: 0))], with: .automatic)
                        }
                    case .delete:
                        let indexToEdited = strongSelf.threadsData.firstIndex(where: {$0.id == newThread.id})
                        if let indexToEdited = indexToEdited {
                            strongSelf.threadsData.remove(at: indexToEdited)
                            strongSelf.tableView.deleteRows(at: [strongSelf.getTBLIndexPath(index: IndexPath(row: indexToEdited, section: 0))], with: .automatic)
                        }
                    default:
                        break
                    }
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .notifyGroup, object: nil, queue: .main) { [weak self] (notification) in
            guard let strongSelf = self else { return }
            if let group = notification.userInfo?["Group"] as? GroupModel {
                if let notify = notification.userInfo?["Notify"] as? NotifyCRUD {
                    switch notify {
                    case .create:
                        strongSelf.userGroups.insert(group, at: 0)
                        strongSelf.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                    case .update:
                        let indexToEdited = strongSelf.userGroups.firstIndex(where: {$0.id == group.id})
                        if let indexToEdited = indexToEdited {
//                            var updatedGroup = group
                            group.group_notification_count = strongSelf.userGroups[indexToEdited].group_notification_count ?? strongSelf.userGroups[indexToEdited].clinic_notifications_count
                            strongSelf.userGroups[indexToEdited] = group
                            strongSelf.userGroups = strongSelf.userGroups.sorted(by: { $0.last_visit ?? "" > $1.last_visit ?? "" })
                            strongSelf.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                        }
                    case .delete:
                        let indexToDeleted = strongSelf.userGroups.firstIndex(where: {$0.id == group.id})
                        if let indexToDeleted = indexToDeleted {
                            strongSelf.userGroups.remove(at: indexToDeleted)
                            strongSelf.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                        }
                        
                    case .read:
                        let indexToEdited = strongSelf.userGroups.firstIndex(where: {$0.id == group.id})
                        if let indexToEdited = indexToEdited {
                            let updatedGroup = strongSelf.userGroups[indexToEdited]
                            if (updatedGroup.group_notification_count ?? 0) >= (group.group_notification_count ?? 0) {
                                updatedGroup.group_notification_count = (updatedGroup.group_notification_count ?? 0) - (group.group_notification_count ?? 0)
                            }
                            else {
                                updatedGroup.group_notification_count = 0
                            }
                            strongSelf.userGroups[indexToEdited] = updatedGroup
                            strongSelf.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                        }
                    }
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .notifyGroupNotification, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            self.refreshData(UIButton())
        }
        
        NotificationCenter.default.addObserver(forName: .notifySocket, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            if let notify = notification.userInfo?["Notify"] as? NotifySocket {
                switch notify {
                case .receiveComment:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .CLINIC {
                        if let data = notification.userInfo?["Data"] as? SocketCommentBaseModel {
                            if let _data = data.comment?.data, let index = self.threadsData.firstIndex(where: { $0.id == _data.doc_id }) {
                                if data.type == "new" {
                                    if let _ = self.threadsData[index].clinic_doc_comments {
                                        self.threadsData[index].clinic_doc_comments?.insert(_data, at: 0)
                                    }
                                    else {
                                        self.threadsData[index].clinic_doc_comments = [_data]
                                    }
                                    let commentCount = (self.threadsData[index].clinic_doc_comments_count ?? 0) + 1
                                    self.threadsData[index].clinic_doc_comments_count = commentCount
                                }
                                else if let commentIndex = self.threadsData[index].clinic_doc_comments?.firstIndex(where: { $0.id == _data.id }) {
                                    self.threadsData[index].clinic_doc_comments?[commentIndex] = _data
                                }
                                
                                DispatchQueue.main.async {
                                    self.tableView.reloadRows(at: [self.getTBLIndexPath(index: IndexPath(row: index, section: 0))], with: .none)
                                }
                            }
                        }
                    }
                    
                case .chatRoomNotification:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let _ = notification.userInfo?["Data"] as? ChatRoomModel {
                            self.apiCallForChatCount()
                        }
                    }
                
                case .threadNotification:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let data = notification.userInfo?["Data"] as? SocketThreadNotificationBaseModel {
                            self.apiCallForNotifyCount()
                            let indexToEdited = self.userGroups.firstIndex(where: {$0.id == data.doc?.doc?.clinic_id})
                            if let indexToEdited = indexToEdited {
                                let updatedGroup = self.userGroups[indexToEdited]
                                let count = (updatedGroup.group_notification_count ?? 0)
                                updatedGroup.group_notification_count = count + 1
                                self.userGroups[indexToEdited] = updatedGroup
                                self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                            }
                        }
                    }
                    
                case .taskNotification:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let _ = notification.userInfo?["Data"] as? SocketThreadNotificationBaseModel {
                            self.apiCallForNotifyCount()
                        }
                    }
                    
                case .groupRequestNotification:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let _ = notification.userInfo?["Data"] as? SocketGroupRequestNotificationBaseModel {
                            self.apiCallForNotifyCount()
                        }
                    }
                    
                case .clinicJoined:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let data = notification.userInfo?["Data"] as? SocketClinicModel, let group = data.data {
                            self.userGroups.insert(group, at: 0)
                            self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                        }
                    }
                    
                default:
                    break
                }
            }
        }
    }
    
    private func setupOverlay(){
        let screenSize: CGRect = UIScreen.main.bounds
        overlay = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: self.tabBarController?.view.bounds.height ?? screenSize.height))
        overlay!.backgroundColor = AppColors.tabBarTint
        
        let logo = UIImageView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 50))
        logo.image = UIImage(named: "logo")
        logo.contentMode = .scaleAspectFit
//        logo.center = overlay!.center
        overlay!.addSubview(logo)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        label.textAlignment = .center
        label.text = "Loading...".localized()
        label.textColor = .white
        label.center = overlay!.center
        overlay!.addSubview(label)
        
        progressView = UIProgressView(progressViewStyle: .bar)
//        progressView!.center = overlay!.center
        progressView!.progress = 0
        progressView!.trackTintColor = UIColor.white
        progressView!.tintColor = UIColor.systemBlue
        overlay!.addSubview(progressView!)
        
        logo.translatesAutoresizingMaskIntoConstraints = false
        progressView!.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            logo.topAnchor.constraint(equalTo: overlay!.topAnchor, constant: 200),
            logo.leadingAnchor.constraint(equalTo: overlay!.leadingAnchor, constant: 40),
            logo.trailingAnchor.constraint(equalTo: overlay!.trailingAnchor, constant: -40),
            
            progressView!.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 40),
            progressView!.leadingAnchor.constraint(equalTo: overlay!.leadingAnchor, constant: 40),
            progressView!.trailingAnchor.constraint(equalTo: overlay!.trailingAnchor, constant: -40),
        ]
        NSLayoutConstraint.activate(constraints)
        
        self.tabBarController?.view.addSubview(overlay!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
//        super.viewWillAppear(animated)
//        if AppConfig.shared.isCreateThread{
//            AppConfig.shared.isCreateThread = false
//            self.pageNumber = 0
//            self.loadMore = true
//            self.loadTimeline()
//        }
//        self.newThreadWith = .none
//        self.commentWith = .none
//
        if appDelegate.attachements.count > 0 {
            self.showSharePopup()
        }
        
//        socketManager.delegate = self
    }
    
    private func showSharePopup() {
        if let vc = self.getVC(storyboard: .ALERT, vcIdentifier: String(describing: ShareAlertVC.self)) as? ShareAlertVC {
            vc.onClickThread = { [weak self] in
                guard let `self` = self else {return}
                self.gotoCreateThreadScene()
            }
            vc.onClickMessage = { [weak self] in
                guard let `self` = self else {return}
                self.sendFileViaMessage()
            }
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func onReceiveShareFile(_ notification: Notification){
        if appDelegate.attachements.count > 0 {
            self.showSharePopup()
        }
    }
    
    private func sendFileViaMessage() {
        self.tabBarController?.selectedIndex = 2
    }
    
    func loadTimeline() {
//        print("HomeVC->loadTimeline", self.pageNumber)
        if self.pageNumber == 1 {
            self.threadsData.removeAll()
            self.tableView.reloadData()
        }
        self.tableView.setEmptyMessage("")
        var params = TimelineParams()
        params.page = self.pageNumber
        params.home_enabled = true
        RequestManager.shared.timeline(params: params) { (status, response) in
            self.refreshControl.endRefreshing()
            if status {
                if let response = response as? TimeineResult {
                    if let data = response.data, data.isEmpty, self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                        self.tableView.reloadData()
                    }
                    if let dataSource = response.data {
                        self.threadsData.append(contentsOf: dataSource)
                        if dataSource.count < pageLimit5 {
                            self.loadMore = false
                        }
                        self.tableView.reloadData()
                    }
                }
            }
            
            if let payload = self.appDelegate.notificationPayload {
                if let type = payload["type"] as? NotificationTypes, let id = payload["id"] as? Int, let userId = payload["userId"] as? Int {
                    if userId == AppConfig.shared.user.id {
                        if type == NotificationTypes.CHAT {
                            if let vc = baseVC?.getVC(storyboard: .CONNECTIOS, vcIdentifier: "ChatScene") as? ChatVC {
                                vc.chatId = id
                                self.navigateVC(vc)
                            }
                        }
                        else if type == NotificationTypes.THREAD {
                            if let vc = baseVC?.getVC(storyboard: .TABS, vcIdentifier: "DetailView") as? ThreadDetailVC {
                                vc.threadId = id
                                self.navigateVC(vc)
                            }
                        }
                    }
                    else {
                        self.appDelegate.notificationPayload = nil
                    }
                }
                else if let type = payload["type"] as? NotificationTypes, type == .GROUP_REQUEST {
                    self.tabBarController?.selectedIndex = 1
                }
                self.appDelegate.notificationPayload = nil
            }
        }
    }
    
    func searchTimeline() {
//        print("HomeVC->searchTimeline", self.pageNumber)
//        if self.pageNumber == 1 {
//            self.threadsData.removeAll()
//            self.tableView.reloadData()
//        }
//        self.tableView.setEmptyMessage("")
//        searchParams.page = self.pageNumber
//        searchParams.search = self.searchedText
//        searchParams.home_enabled = true
//        RequestManager.shared.searchTimeline(params: searchParams) { (status, response) in
//            self.refreshControl.endRefreshing()
//            if status {
//                if let response = response as? SearchTimeineResult {
//                    if let data = response.data, data.isEmpty, self.pageNumber == 1 {
//                        self.tableView.setEmptyMessage(noRecordFoundHomeSearch.localized())
//                        self.tableView.reloadData()
//                    }
//                    if let dataSource = response.data {
//                        self.threadsData.append(contentsOf: dataSource)
//                        if dataSource.count < pageLimit5 {
//                            self.loadMore = false
//                        }
//                        self.tableView.reloadData()
//                    }
//                }
//            }
//        }
    }

    func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.searchBar.endEditing(true)
    }
    
    func layOutSearchbar(){
        self.navigationItem.leftBarButtonItems = nil
        self.searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: (UIScreen.main.bounds.width * 0.8), height: 50))
        self.searchBar.placeholder = SearchText.localized()
//        self.searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
        self.searchBar.delegate = self
//        self.searchBar.searchBarStyle = .minimal
//        self.searchBar.keyboardAppearance = .dark
        self.searchBar.textField?.rightViewMode = .always
        self.searchBar.textField?.clearButtonMode = .always
//        self.searchBar.textField?.textColor = .white
        let leftNavBarButton = UIBarButtonItem(customView: searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        self.rightNavBarButton(.advance)
        self.searchBar.text = ""
        self.filtersButton.isHidden = false
        self.searchBar.becomeFirstResponder()
        
        Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    func setUpUI() {
        self.navigationItem.leftBarButtonItems = nil
        let logoImage = UIImage.init(named: "logoHome")
        let logoImageView = UIImageView.init(image: logoImage)
        logoImageView.frame = CGRect(x: -40, y: 0, width: 45, height: 15)
        logoImageView.contentMode = .scaleAspectFit
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -25
        navigationItem.leftBarButtonItems = [negativeSpacer, imageItem]
        self.rightNavBarButton(.normal)
    }
    
    func rightNavBarButton(_ type : ButtonType) {
        self.isSearchOn = false
        let rightButton = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"),
                                          style: .plain,
                                              target: self,
                                              action: #selector(self.advanceSearch))
        self.navigationItem.rightBarButtonItem = rightButton
//        let view = UIView(frame: CGRect(x: 5, y: 5, width: 30, height: 30))
//        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//        if let imgBackArrow = UIImage(named: "search") {
//            imageView.image = imgBackArrow
//        }
//        view.addSubview(imageView)
//        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(advanceSearch)))
//        switch type {
//        case .normal:
//            if let imgBackArrow = UIImage(named: "search") {
//                imageView.image = imgBackArrow
//            }
//            view.addSubview(imageView)
//            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showSearchBar)))
//            break
//        case .advance:
//            if let imgBackArrow = UIImage(named: "advanceSearch") {
//                imageView.image = imgBackArrow
//            }
//            view.addSubview(imageView)
//            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(advanceSearch)))
//            break
//        }
//        let advanceSearchBarButtonItem = UIBarButtonItem(customView: view)
//        self.navigationItem.rightBarButtonItem = advanceSearchBarButtonItem
    }
    
    @objc func showSearchBar(){
        self.isSearchOn = !self.isSearchOn
        if self.isSearchOn {
            self.layOutSearchbar()
        }
        else {
            self.searchBtnClicked()
        }
    }
    
    @objc func advanceSearch(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.searchBar.textField?.resignFirstResponder()
        }
//        self.performSegue(withIdentifier: "searchFiltersSegue",sender: self)
        if let vc = getVC(storyboard: .SEARCH, vcIdentifier: String(describing: SearchVC.self)) as? SearchVC {
            self.navigateVC(vc)
        }
//        if let vc = getVC(storyboard: .CREATE_THREAD, vcIdentifier: "SelectGroupVC") as? SelectGroupVC {
//            vc.searchParams = self.searchParams
//            vc.delegate = self
//            self.navigateVC(vc)
//        }
    }
    
    @IBAction func clearSearchFilters(_ sender: UIButton) {
        self.setUpUI()
        self.pageNumber = 1
        self.loadMore = true
        self.filtersButton.isHidden = true
        
        self.searchParams = TimelineParams()
        sender.isHidden = true
        self.loadTimeline()
        self.searchBar.text = nil
        self.searchedText = ""
        for (_, item) in AppConfig.shared.myGroupsWithCategories.enumerated(){
            if item.isSelected{
                item.isSelected = !item.isSelected
                if let cats = item.clinic_categories {
                    for (_, folder) in cats.enumerated(){
                        if folder.isSelected{
                            folder.isSelected = !folder.isSelected
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier  == "threadDetailsSegue" {
            if let vc = segue.destination as? ThreadDetailVC, let id = self.threadsData[self.indexRow].id {
                vc.threadId = id
                vc.commentWith = self.commentWith
//              vc?.delegate = self
            }
        }
        else if segue.identifier == "searchFiltersSegue" {
            if let vc = segue.destination as? AdvanceSearchVC {
                vc.searchParams = self.searchParams
                vc.searchText = self.searchBar.text?.trim ?? ""
                vc.delegate = self
            }
        }
        else if segue.identifier == "GroupInfoVC" {
            let dvc = segue.destination as? GroupInfoVC
            dvc?.groupById = self.tappedGroupID
            dvc?.navigatingFrom = .home
        }
    }
    
    func gotoCreateThreadScene() {
        if let vc = getVC(storyboard: .CREATE_THREAD, vcIdentifier: "CreateThreadScene") as? CreateThreadVC {
            vc.formMode = .new
            vc.createOrEditThreadFrom = .home
            vc.groupTitle = ""
            vc.createThreadWith = self.newThreadWith
            self.navigateVC(vc)
        }
    }
    
    func gotoUserProfile(_ index: IndexPath) {
        let thread = self.threadsData[index.row]
        if let id = thread.doc_creator?.id, id != AppConfig.shared.user.id {
            self.previewUser(id: id)
        }
    }
    
    func showThreadEditOptions(for index:IndexPath, isEdit:Bool, isDelete: Bool){
        if isEdit || isDelete {
            var alert = AlertModel()
            if isEdit {
                var editBtn = AlertBtnModel()
                editBtn.title = "Edit"
                alert.btns.append(editBtn)
            }
            if isDelete {
                var deleteBtn = AlertBtnModel()
                deleteBtn.title = "Delete"
                alert.btns.append(deleteBtn)
            }
            var cancelBtn = AlertBtnModel()
            cancelBtn.title = "Cancel"
            cancelBtn.color = .red
            alert.btns.append(cancelBtn)
            self.showMultiBtnAlert(model: alert) { [weak self] (data) in
                guard let `self` = self else {return}
                if let btn = data as? String {
                    if btn == "Edit" {
                        self.editThread(at: index)
                    }
                    else if btn == "Delete" {
                        self.deleteThread(at: index)
                    }
                }
            }
        }
    }
    
    func editThread(at index:IndexPath){
        self.indexOfThreadToEdit = index.row
        let threadInfo = self.threadsData[indexOfThreadToEdit]
        if let vc = getVC(storyboard: .CREATE_THREAD, vcIdentifier: "CreateThreadScene") as? CreateThreadVC {
            vc.formMode = .edit
            vc.createOrEditThreadFrom = .home
            vc.groupTitle = threadInfo.clinic_name ?? ""
            vc.groupId = threadInfo.clinic_id ?? 0
//            vc.isFromGroupInfoScreen = true // this check is used separately from the check in below line
//            vc.cameToCreateThreadVCFromGroupInfoScreen = true
            vc.threadToEdit = threadInfo
            self.navigateVC(vc)
        }
    }
    
    func deleteThread(at index:IndexPath){
        self.showAlert(To: "", for: deleteThreadMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                self.delete(at: index)
            }
        }
    }
    
    func delete(at index: IndexPath) {
        let thread = self.threadsData[index.row]
        if let id = thread.id {
            RequestManager.shared.deleteThread(id: id) { (status, response) in
                if status{
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": thread, "Notify": NotifyCRUD.delete])
                    self.showAlert(To: DeleteThreadHeading, for: DeleteThreadMessage.localized()) {_ in}
                }
            }
        }
    }
    
    func update(thread at: IndexPath) {
        let thread = self.threadsData[at.row]
        let setNotification = !(thread.doc_notification_on ?? false)
        if let id = thread.id, let groupId = thread.clinic_id {
            RequestManager.shared.callNotificationAPI(groupID: groupId, notification: setNotification, docId: id) { (status, response) in
                if status {
                    self.threadsData[at.row].doc_notification_on = setNotification
                    self.tableView.reloadRows(at: [self.getTBLIndexPath(index: at)], with: .automatic)
                    
//                    NotificationCenter.default.post(name: ._UpdateThreadNotification,
//                                                    object: nil,
//                                                    userInfo: ["threadId": params.threadId!, "fireBaseId": thread.fireBaseId ?? "", "status": (status == "Y") ? "N" : "Y"]
//                    )
                }
            }
        }
    }
    
    func callMyGroupAPI() {
        RequestManager.shared.getVisitedGroups() { (status, response) in
            self.allAPIsProgress += 1
            print("callMyGroupAPI -> called -> allAPIsProgress -> \(self.allAPIsProgress)")
            if status {
                if let data = response as? GroupListModel {
                    if let userGroups = data.clinics {
                        self.userGroups = userGroups.sorted(by: { $0.last_visit ?? "" > $1.last_visit ?? "" })
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    private func onClickMyGroupCustomBtn(type: MyGroupCustomCellType) {
        if type == .CREATE_GROUP {
            if let vc = getVC(storyboard: .USER_AND_GROUPS, vcIdentifier: "CreateGroupScene") as? NewGroupVC {
                self.navigateVC(vc)
            }
        }
        else {
            NotificationCenter.default.post(name: .notifyGroup, object: nil, userInfo: ["SearchUpdate": true])
            self.tabBarController?.selectedIndex = 3
        }
    }
}

extension HomeVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
}

extension HomeVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.resetSearch()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBtnClicked()
    }
    
    func searchBtnClicked() {
        self.pageNumber = 1
        self.loadMore = true
        let text = searchBar.text?.trim ?? ""
        self.searchedText = text
        if text.isEmpty {
            self.setUpUI()
            self.loadTimeline()
        }
        else {
            self.searchTimeline()
        }
        self.filtersButton.isHidden = false
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.resetSearch()
    }
    
    func resetSearch(){
        self.searchBar.resignFirstResponder()
        self.searchBar.textField?.resignFirstResponder()
        self.searchBar.endEditing(true)
        self.view.endEditing(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.searchBar.textField?.resignFirstResponder()
        }
        self.filtersButton.isHidden = true
        self.pageNumber = 1
        self.loadMore = true
        self.searchedText = ""
        self.setUpUI()
        self.loadTimeline()
        self.searchParams = TimelineParams()
        for (_, item) in AppConfig.shared.myGroupsWithCategories.enumerated(){
            if item.isSelected{
                item.isSelected = !item.isSelected
                if let cats = item.clinic_categories {
                    for (_, folder) in cats.enumerated(){
                        if folder.isSelected{
                            folder.isSelected = !folder.isSelected
                        }
                    }
                }
            }
        }
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.threadsData.count + 1 + (userGroups.count > 0 ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CreateThreadViewCell") as? CreateThreadViewCell
            
            cell?.delegate = self
            let imgUrl = AppConfig.shared.user.file?.medium_url ?? AppConfig.shared.user.file?.src
            if let imageUrl = imgUrl, imageUrl != ""{
                let url = URL(string: imageUrl)
                cell?.myProfileImageView.imageFromServer(imageUrl: url!)
            }else{
                cell?.myProfileImageView.image = #imageLiteral(resourceName: "ic_user")
            }
            return cell!
        }
        else if indexPath.row == 1 && userGroups.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MyGroupHomeCell.self), for: indexPath) as! MyGroupHomeCell
            cell.userGroups = self.userGroups
            cell.collectionView.reloadData()
            cell.onSelect = { [weak self] group in
                guard let `self` = self else { return }
                if let groupID = group.id {
                    self.tappedGroupID = groupID
                    self.performSegue(withIdentifier: "GroupInfoVC", sender: self)
                }
            }
            cell.onSelectCustomBtn = { [weak self] type in
                guard let `self` = self else { return }
                self.onClickMyGroupCustomBtn(type: type)
            }
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GroupThreadCell.self), for: indexPath) as? GroupThreadCell else {
                return self.emptyTblCell()
            }
            let index = indexPath.row - 1 - (self.userGroups.count > 0 ? 1 : 0)
            if index < self.threadsData.count + 1 {
                let indexPath = IndexPath(row: index, section: indexPath.section)
                cell.index = indexPath
                cell.setupData(indexPath, data: self.threadsData[index])
                cell.delegate = self
                cell.onClickProfile = { [weak self] index in
                    guard let strongSelf = self else { return }
                    strongSelf.gotoUserProfile(index)
                }
                
                if self.loadMore && indexPath.row == (self.threadsData.count - 1) {
                    self.pageNumber += 1
                    if self.searchedText.isEmpty {
                        self.loadTimeline()
                    }
                    else {
                        self.searchTimeline()
                    }
                }
            }
            self.view.layoutIfNeeded()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            self.newThreadWith = .none
            self.gotoCreateThreadScene()
        }
        else if indexPath.row == 1 && userGroups.count > 0 {
        }else{
            let index = indexPath.row - 1 - (userGroups.count > 0 ? 1 : 0)
            self.indexRow = index
            self.indexOfThreadToEdit = index //indexPath.row - 1
            if self.indexRow < self.threadsData.count {
                performSegue(withIdentifier: "threadDetailsSegue", sender: self)
            }
        }
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName: String(describing: CreateThreadViewCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: CreateThreadViewCell.self))
        tableView.register(UINib(nibName: String(describing: GroupThreadCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: GroupThreadCell.self))
        tableView.register(UINib.init(nibName: String(describing: MyGroupHomeCell.self), bundle: nil), forCellReuseIdentifier: String(describing: MyGroupHomeCell.self))
        
        self.tableView.separatorStyle = .none
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 400
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }
    
    private func getTBLIndexPath(index: IndexPath) -> IndexPath { //index of thread data
        let row = index.row + 1 + (self.userGroups.count > 0 ? 1 : 0) //+1 for create group cell(1st cell) and +1 for group shortcuts(2nd cell)
        let indexPath = IndexPath(row: row, section: index.section)
        return indexPath
    }
}

//MARK: - HomeTableViewCellDelegate
extension HomeVC: HomeTableViewCellDelegate {
    func didTapShowMore(forThread at: IndexPath) {
        self.threadsData[at.row].showAllComments = !self.threadsData[at.row].showAllComments
        self.tableView.reloadRows(at: [self.getTBLIndexPath(index: at)], with: .automatic)
    }
    
    func didTapNotification(for threadAtIndex: IndexPath) {
        self.update(thread: threadAtIndex)
    }
    
    func didTapComment(with: Int, at index: IndexPath) {
        self.indexRow = index.row
        switch with {
        case 0:
            self.commentWith = .cam
            break
        case 1:
            self.commentWith = .smiley
            break
        default:
            self.commentWith = .keyboard
            break
        }
        if self.indexRow < self.threadsData.count {
            performSegue(withIdentifier: "threadDetailsSegue", sender: self)
        }
    }
    
    func didTapMoreOptions(for threadAtIndex: IndexPath) {
        let thread = self.threadsData[threadAtIndex.row]
        if let threadOwnerId = thread.doc_creator?.id {
            if AppConfig.shared.user.id != threadOwnerId {
                if let isAdmin = thread.is_admin, isAdmin {
                    self.showThreadEditOptions(for: threadAtIndex, isEdit: false, isDelete: true)
                }
            }
            else {
                self.showThreadEditOptions(for: threadAtIndex, isEdit: true, isDelete: true)
            }
        }
    }
    
    func didTapGroupName(index: IndexPath) {
        let thread = self.threadsData[index.row]
        if let groupID = thread.clinic_id {
            self.tappedGroupID = groupID
            performSegue(withIdentifier: "GroupInfoVC", sender: self)
        }
    }
}

extension HomeVC: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        
        if AppConfig.shared.lastVisitedTabIndex != tabBarIndex, AppConfig.shared.lastVisitedTabIndex != 0 {
            AppConfig.shared.lastVisitedTabIndex = 0
        }else{
            if tabBarIndex == 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
}

extension HomeVC: CreateThreadViewCellDelegate {
    func createThreadWith(type: Int) {
        switch type {
        case 1:
            self.newThreadWith = .library
            break
        case 2:
            self.newThreadWith = .camera
            break
        case 3:
            self.newThreadWith = .docs
            break
        case 4:
            self.newThreadWith = .video
            break
        case 5:
            self.newThreadWith = .scan
            break
        default:
            self.newThreadWith = .none
            break
        }
        self.gotoCreateThreadScene()
    }
}

extension HomeVC: AdvanceSearchVCDelegate {
    func done(filterType: String) {
        var searchOptions = TimelineParams()
        self.pageNumber = 1
        self.loadMore = true
        searchOptions.page = self.pageNumber
        if let txt = self.searchBar.text{
            let trimmedText = txt.trimmingCharacters(in: .whitespacesAndNewlines)
            self.searchedText = trimmedText
            searchOptions.search = trimmedText
        }
        
        var selectedClinicIds = [Int]()
        var selectedCatIds = [Int]()
        for (_, item) in AppConfig.shared.myGroupsWithCategories.enumerated(){
            if item.isSelected{
                if let id = item.id {
                    selectedClinicIds.append(id)
                }
                if let cats = item.clinic_categories {
                    for (_, folder) in cats.enumerated(){
                        if folder.isSelected{
                            if let id = folder.id{
                                selectedCatIds.append(id)
                            }
                        }
                    }
                }
            }
        }
        searchOptions.clinic_ids = selectedClinicIds
        searchOptions.category_ids = selectedCatIds
        searchOptions.type = filterType
        self.searchParams = searchOptions
        self.searchTimeline()
        
        self.filtersButton.isHidden = false
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}

//extension HomeVC: SocketManagerDelegate {
//    func receiveComment(data: SocketCommentBaseModel) {
//        if let _data = data.comment?.data, let index = self.threadsData.firstIndex(where: { $0.id == _data.doc_id }) {
//            if data.type == "new" {
//                if let _ = self.threadsData[index].clinic_doc_comments {
//                    self.threadsData[index].clinic_doc_comments?.insert(_data, at: 0)
//                }
//                else {
//                    self.threadsData[index].clinic_doc_comments = [_data]
//                }
//                let commentCount = (self.threadsData[index].clinic_doc_comments_count ?? 0) + 1
//                self.threadsData[index].clinic_doc_comments_count = commentCount
//            }
//            else if let commentIndex = self.threadsData[index].clinic_doc_comments?.firstIndex(where: { $0.id == _data.id }) {
//                self.threadsData[index].clinic_doc_comments?[commentIndex] = _data
//            }
//
//            DispatchQueue.main.async {
//                self.tableView.reloadRows(at: [self.getTBLIndexPath(index: IndexPath(row: index, section: 0))], with: .none)
//            }
//        }
//    }
//}

struct NotificationParam {
    var threadId : Int!
    var notificationStatus : String!
}


enum ButtonType {
    case normal, advance
}
