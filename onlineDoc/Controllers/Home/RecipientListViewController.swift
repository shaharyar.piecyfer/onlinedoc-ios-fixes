//
//  RecipientsVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 06/03/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
protocol RecipientListViewControllerDelegate : class {
    func selectedRecipients(recipients: [User])
}

class RecipientsVC: BaseVC {
    var selection:RecipientsFor = .invitation
    var recipients = [User]()
    var selectedRecipients = [User]()
    var delegate : RecipientListViewControllerDelegate!
    var pageNumber = 1
    var loadMore = true
    
    @IBOutlet weak var noUserRegisteredLabel: UILabel!
    @IBOutlet weak var invitationLabel: UILabel!
    @IBOutlet weak var searchBar:UISearchBar!
    
    @IBOutlet weak var inviteChoiceView:UIView!{
        didSet{
            inviteChoiceView.layer.cornerRadius = 12
            inviteChoiceView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var inviteAlertView:UIView!{
        didSet{
            inviteAlertView.layer.cornerRadius = 12
            inviteAlertView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var tableView:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.loadSystemMembers()
        if self.selection == .groupEmail{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: OKText.localized(), style: .plain, target: self, action: #selector(Done))
        }else{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: OKText.localized(), style: .plain, target: self, action: #selector(Done))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UITextField.appearance().tintColor = .white
        layOutSearchbar()
        searchBar.searchTextField.font = UIFont(name: "Helvetica", size: 12)
        searchBar.placeholder = searchBarplaceholderWithEmailNumName.localized()
    }

    @objc func Done() {
        delegate?.selectedRecipients(recipients: self.selectedRecipients)
        self.dismissVC()
    }
    
    func loadSystemMembers(searchText: String = ""){
        if self.pageNumber == 1 {
            self.recipients.removeAll()
        }
        self.tableView.setEmptyMessage("")
        self.systemMembers(page: self.pageNumber, searchText: searchText) { (status, users) in
            if status && users != nil {
                if users!.isEmpty && !searchText.isEmpty {
                    if self.recipients.count == 0 && searchText.isValidEmail{
                        self.inviteAlertView.isHidden = false
                        self.tableView.isHidden = true
                    }
                    else if self.recipients.count == 0 && searchText.isValidPhone() {
                        self.noUserRegisteredLabel.text = noRegisterationWithPhoneNum.localized()
                        self.invitationLabel.text = inviteThroughSMS.localized()
                        self.inviteAlertView.isHidden = false
                        self.tableView.isHidden = true
                        
                    }
                    else if self.recipients.count > 0 {
                        self.inviteAlertView.isHidden = true
                        self.tableView.isHidden = false
                    }
                    else {
                        self.recipients.removeAll()
                        self.tableView.reloadData()
                        self.pageNumber = 1
                        self.tableView.setEmptyMessage(noUserText.localized())
                    }
                }
                else {
                    var filteredUsers = users
                    if self.selectedRecipients.count != 0 {
                        filteredUsers = filteredUsers?.map{(user) in
                            var updatedUser = user
                            _ = self.selectedRecipients.allSatisfy { (item) -> Bool in
                                if item.email == user.email {
                                    updatedUser.isSelected = true
                                    return false
                                }
                                return true
                            }
                            return updatedUser
                        }
                    }
                    self.recipients.append(contentsOf: filteredUsers!)
                    if users!.count == 0 {
                        self.loadMore = false
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func layOutSearchbar(){
        self.searchBar.delegate = self
        self.searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
        if let searchTextField = self.searchBar.textField {
            searchTextField.textColor = .white
        }
    }
    
    @IBAction func yesButtonTapped(){
        var user = User()
        user.name = self.searchBar.text
        user.email = self.searchBar.text
        user.phone = self.searchBar.text
        user.isSelected = true
        self.selectedRecipients.append(user)
        self.inviteAlertView.isHidden = true
        self.searchBar.text = ""
        self.tableView.isHidden = false
        self.pageNumber = 1
        self.loadSystemMembers()
    }
    
    @IBAction func noButtonTapped(){
        self.searchBar.text = ""
        self.inviteAlertView.isHidden = true
        self.tableView.isHidden = false
        self.pageNumber = 1
        self.loadSystemMembers()
    }
}

extension RecipientsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.recipients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else {return GeneralViewCell()}
        cell.setupData(indexPath: indexPath, data: self.recipients[indexPath.row])
        cell.cellCheckBox.isUserInteractionEnabled = false
        cell.cellCheckBox.boxType = .square
        cell.delegate = self
        if self.loadMore && indexPath.row == (self.recipients.count - 2){
            self.pageNumber += 1
            self.loadSystemMembers()
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        recipients[indexPath.row].isSelected = !recipients[indexPath.row].isSelected
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: GeneralViewCell.self), bundle: nil), forCellReuseIdentifier: "GeneralCell")
        
        self.tableView.separatorStyle = .none
    }
}

extension RecipientsVC: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.resignFirstResponder()
            self.searchBar.textField?.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            if self.recipients.isEmpty {
                self.pageNumber = 1
                self.loadMore = true
                self.loadSystemMembers()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.pageNumber = 1
        self.loadMore = true
        let txt = searchBar.text?.trim ?? ""
        self.loadSystemMembers(searchText: txt)
        self.searchBar.resignFirstResponder()
    }
}

extension RecipientsVC: GeneralViewCellDelegate{
    
    func didTapRow(at index: Int) {
        self.recipients[index].isSelected = !self.recipients[index].isSelected
        if let index = self.selectedRecipients.firstIndex(where: { (item) -> Bool in
            item.email == self.recipients[index].email
        }) {
            self.selectedRecipients.remove(at: index)
        }
        else {
            self.selectedRecipients.append(self.recipients[index])
        }
        self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
    }
}
