//
//  TRCollectionViewController.swift
//  onlineDoc
//
//  Created by Piecyfer on 31/03/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.

/*
import UIKit
import TRMosaicLayout

class TRCollectionViewController: UICollectionViewController {
  override func viewDidLoad() {
    super.viewDidLoad()

      let mosaicLayout = TRMosaicLayout()
      self.collectionView?.collectionViewLayout = mosaicLayout
      mosaicLayout.delegate = self
  }
}

extension TRCollectionViewController: TRMosaicLayoutDelegate {
    func heightForSmallMosaicCell() -> CGFloat {
        return 100
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: TRMosaicLayout, insetAtSection: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, mosaicCellSizeTypeAtIndexPath indexPath: IndexPath) -> TRMosaicCellType {
        return indexPath.item % 3 == 0 ? .big : .small
    }
}
*/
