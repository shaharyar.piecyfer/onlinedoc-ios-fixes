//
//  ThreadDetailInfoCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 10/05/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import AVKit
import FSPagerView
import Atributika

protocol ThreadDetailsCellDelegate: AnyObject {
    func didTapPlay(videoAt index:Int)
    func didTapImagePreview(startAt index: Int, with imageView: UIImageView)
    func didTapGroupEmail()
    func didTapDeleteThread()
    func didTapShowList(isDocList:Bool)
    func didTapPreview(fileAt index:Int)
    func preview(fileWith url: String, name:String, contentType: String)
    func didTapChangeNotifications()
//    func didTapEditThread()
    func didTapMoreOptions()
}

class ThreadDetailInfoCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var bottomContainerView : UIView!
    @IBOutlet weak var userProfileImage:UIImageView!{
        didSet{
            userProfileImage.layer.cornerRadius = userProfileImage.frame.size.height / 2
            userProfileImage.clipsToBounds = true
            userProfileImage.layer.borderWidth = 0.5
            userProfileImage.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var counterView: UIView!{
        didSet{
            counterView.layer.cornerRadius = counterView.layer.frame.size.height / 2
            counterView.clipsToBounds = true
        }
    }
    @IBOutlet weak var counterLabel: UILabel!{
        didSet{
            counterLabel.textColor = .white
        }
    }
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var docListButton: UIButton!
    @IBOutlet weak var threadGroupLabel: UILabel!
    @IBOutlet weak var threadTitleLabel: UILabel!
    
    @IBOutlet weak var commentsCountLabel: UILabel!
    @IBOutlet weak var commentsListButton: UIButton!
    @IBOutlet weak var threadDescriptionLabel: UILabel!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var pagerViewAspectRatio:NSLayoutConstraint!
    @IBOutlet weak var doclistStackView: UIStackView!
    @IBOutlet weak var containerStack: UIStackView!
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    @IBOutlet weak var moreButton: UIButton!{
        didSet{
            let image = UIImage(named: "more")?.withRenderingMode(.alwaysTemplate)
            moreButton.setImage(image, for: .normal)
        }
    }
    @IBOutlet weak var deleteThreadImageView: UIImageView!{
        didSet{
            let image = UIImage(named: "delete")?.withRenderingMode(.alwaysTemplate)
            deleteThreadImageView.image = image
            deleteThreadImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.deleteThread)))
        }
    }
    @IBOutlet weak var groupMailImageView: UIImageView!{
        didSet{
            let image = UIImage(named: "invitation")?.withRenderingMode(.alwaysTemplate)
            groupMailImageView.image = image
            groupMailImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.groupMail)))
        }
    }
//    @IBOutlet weak var showDocsListImageView: UIImageView!{
//        didSet{
//            showDocsListImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(docList)))
//        }
//    }
    //MARK: - Properties
    weak var delegate : ThreadDetailsCellDelegate?
    var controller = AVPlayerViewController()
    var index: IndexPath?
    var isDocListVisibility: Bool = false
//    var recordIndex = 0
    
//    var commentsDataSource = [ThreadComment](){
//        didSet{
//            let files = self.commentsDataSource.filter{ $0.posted_from == "docs" }
//            let mediaFiles = files.filter{ $0.file_main_type != "application" }
//            self.threadAttachments = mediaFiles
//            let applicationFile = files.filter{ $0.file_main_type == "application" }
//            self.threadFiles = applicationFile
//        }
//    }
    
    var threadAttachments = [FileModel](){
        didSet{
            if threadAttachments.count > 0 {
                self.pagerView.isHidden = false
            }else{
                self.pagerView.isHidden = true
            }
            self.pagerView.reloadData()
        }
    }
//    var threadFiles = [ThreadComment](){
//        didSet{
//            if threadFiles.count > 0 {
//                self.doclistStackView.isHidden = false
//            }else{
//                self.doclistStackView.isHidden = true
//            }
//        }
//    }
    
    private let lbl = AttributedLabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.threadDescriptionLabel.text = nil
        lbl.onClick = { label, detection in
            switch detection.type {
            case .link(let url):
                var urlString = url.absoluteString
                if urlString.hasPrefix("https://") || urlString.hasPrefix("http://") {
                    UIApplication.shared.open(url)
                }
                else {
                    urlString = "https://\(urlString)"
                    if let _url = URL(string: urlString) {
                        UIApplication.shared.open(_url)
                    }
                }
            default:
                break
            }
        }
        self.threadDescriptionLabel.addSubview(lbl)
        self.threadDescriptionLabel.isUserInteractionEnabled = true
        let marginGuide = self.threadDescriptionLabel.layoutMarginsGuide
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lbl.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        lbl.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        lbl.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        lbl.numberOfLines = 0
        
        //Generic theme
        Themer.shared.register(
            target: self,
            action: ThreadDetailInfoCell.applyTheme)

        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: ThreadDetailInfoCell.setTheme)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(indexPath: IndexPath, data: ThreadModel) {
        if data.doc_type == PostTypes.TASK.rawValue {
            self.notificationButton.isHidden = true
        }
        self.index = indexPath
        
        if let timeStamp = data.created_at{
            self.timeStampLabel.text = (AppConfig.shared.convertToDate(from: timeStamp).getMoment() + AgoText.localized())
        }else{
            self.timeStampLabel.text = ""
        }
        
        if let notfication = data.doc_notification_on {
            let image = (notfication ? #imageLiteral(resourceName: "bellOn") : #imageLiteral(resourceName: "bellOff")).withRenderingMode(.alwaysTemplate)
            self.notificationButton.setImage(image, for: .normal)
        }
        else {
            self.notificationButton.setImage(#imageLiteral(resourceName: "bellOff").withRenderingMode(.alwaysTemplate), for: .normal)
        }
        
        if let threadTitle = data.title {
            self.threadTitleLabel.attributedText = threadTitle.attributedText(fontSize: 16, allStyleFont: AppFont.kMuliSemiBold(16).font())?.attributedString
        } else {
            self.threadTitleLabel.text = ""
        }
        
        if let groupTitle = data.clinic_name {
            self.threadGroupLabel.text = groupTitle
        }else{
            self.threadGroupLabel.text = nil
        }
        
        if let categoryTitle = data.clinic_category_name {
            if let title = self.threadGroupLabel.text, !title.isEmpty {
                self.threadGroupLabel.text = title + " / \(categoryTitle)"
            } else {
                self.threadGroupLabel.text = categoryTitle
            }
        }else{
            if data.doc_type == PostTypes.TASK.rawValue {
                self.threadGroupLabel.text = self.timeStampLabel.text
                self.timeStampLabel.text = " "
            }
            else {
                self.threadGroupLabel.text = nil
            }
        }
        
        if let desc = data.description {
            lbl.attributedText = desc.attributedText(fontSize: 16, allStyleFont: AppFont.kMuliLight(16).font())

//            let desc = desc.htmlToString
//            let attributedString = NSMutableAttributedString.init(string: desc)
//            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColors.blackColor, range: NSRange(location: 0, length: desc.count))
//            self.threadDescriptionLabel.attributedText = attributedString
////            self.threadDescriptionLabel.text = desc
////            self.threadDescriptionLabel.tintColor = .white
//
//            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//            let matches = detector.matches(in: desc, options: [], range: NSRange(location: 0, length: desc.utf16.count))
//
//            for match in matches {
//                guard let range = Range(match.range, in: desc) else { continue }
//                let url = desc[range]
//
//                let _range = (desc as NSString).range(of: String(url))
//                let mutableAttributedString = NSMutableAttributedString.init(string: desc)
//                mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.systemBlue, range: _range)
//                self.threadDescriptionLabel.attributedText = mutableAttributedString
//
//                threadDescriptionLabel.isUserInteractionEnabled = true
//                threadDescriptionLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
//            }
        }else{
            self.threadDescriptionLabel.text = nil
        }
        
        if AppConfig.shared.user.id != data.doc_creator?.id {
            if let isAdmin = data.is_admin, isAdmin {
                self.moreButton.isHidden = false
                self.deleteThreadImageView.isHidden = false
            }
            else if data.doc_type == PostTypes.TASK.rawValue {
                self.moreButton.isHidden = false
                self.deleteThreadImageView.isHidden = false
            }
            else{
                self.moreButton.isHidden = true
                self.deleteThreadImageView.isHidden = true
            }
        }else{
            self.moreButton.isHidden = false
            self.deleteThreadImageView.isHidden = false
        }
        
        if let name = data.doc_creator?.full_name {
            self.userNameLabel.text = name
        }else{
            self.userNameLabel.text = nil
        }
        
        let imgUrl = data.doc_creator?.file?.medium_url ?? data.doc_creator?.file?.src
        if let img = imgUrl, img != "" {
            if let url = URL(string: img) {
                self.userProfileImage.imageFromServer(imageUrl: url, placeholder: #imageLiteral(resourceName: "ic_user"))
            }else{
                self.userProfileImage.image = #imageLiteral(resourceName: "ic_user")
            }
        }else{
            self.userProfileImage.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let isAdmin = data.is_admin, isAdmin, data.doc_type == PostTypes.THREAD.rawValue {
            self.groupMailImageView.isHidden = false
        } else {
            self.groupMailImageView.isHidden = true
        }
        
        self.pagerView.isHidden = true
        if let files = data.doc_files {
            if files.count > 0 {
                self.containerStack.spacing = 8
                self.doclistStackView.subviews.forEach{ $0.removeFromSuperview()}
                let otherFiles = files.filter({!$0.content_type!.contains("image") && !$0.content_type!.contains("video")})
                self.loadFilesList(files: otherFiles)
                
                let imagesVideos = files.filter({$0.content_type!.contains("image") || $0.content_type!.contains("video")})
                self.threadAttachments = imagesVideos
            }
            let filesCount = files.filter({$0.content_type!.contains("image") || $0.content_type!.contains("video")}).count
            if filesCount > 1 {
                self.counterView.isHidden = false
                self.counterLabel.text = "1 / \(files.count)"
            }
            else {
                self.counterView.isHidden = true
            }
        }else{
            self.counterView.isHidden = true
        }
        
        if self.isDocListVisibility {
            self.docListButton.backgroundColor = UIColor.white.withAlphaComponent(0.1)
            self.docListButton.layer.cornerRadius = self.docListButton.frame.size.height / 2
            self.docListButton.clipsToBounds = true
            self.commentsListButton.backgroundColor = .clear
            let fileCount = (data.clinic_doc_file_count ?? 0) + (data.clinic_doc_comment_file_count ?? 0)
            self.commentsCountLabel.text = fileCount > 1 ? "\(fileCount) "+"Files".localized() : "\(fileCount) "+"File".localized()
        }
        else {
            self.commentsListButton.backgroundColor = UIColor.white.withAlphaComponent(0.1)
            self.commentsListButton.layer.cornerRadius = self.commentsListButton.frame.size.height / 2
            self.commentsListButton.clipsToBounds = true
            self.docListButton.backgroundColor = .clear
            let commentCount = data.clinic_doc_comments_count ?? 0
            self.commentsCountLabel.text = commentCount > 1 ? "\(commentCount)"+"Comments".localized() : "\(commentCount)"+"Comment".localized()
        }
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = threadDescriptionLabel.attributedText?.string else {
            return
        }
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count))

        for match in matches {
            guard let range = Range(match.range, in: text) else { continue }
            let url = text[range]
            
            if let url = URL(string: String(url)) {
                UIApplication.shared.open(url)
            }
        }
    }
    
    //MARK: - Methods
    @objc func deleteThread(){
        self.delegate?.didTapDeleteThread()
    }
    
    @objc func groupMail(){
        self.delegate?.didTapGroupEmail()
    }
    
    @IBAction func moreOptions() {
        self.delegate?.didTapMoreOptions()
    }
    
    @IBAction func showDocList(_ sender: UIButton){
        sender.backgroundColor = UIColor.white.withAlphaComponent(0.10)
        sender.layer.cornerRadius = sender.frame.size.height / 2
        sender.clipsToBounds = true
        self.commentsListButton.backgroundColor = .clear
        self.delegate?.didTapShowList(isDocList: true)
    }
    
    @IBAction func showCommentList(_ sender: UIButton){
        sender.backgroundColor = UIColor.white.withAlphaComponent(0.10)
        sender.layer.cornerRadius = sender.frame.size.height / 2
        sender.clipsToBounds = true
        self.docListButton.backgroundColor = .clear
        self.delegate?.didTapShowList(isDocList: false)
    }
    
    @IBAction func onClickNotification() {
        self.delegate?.didTapChangeNotifications()
    }
    
    func loadFilesList(files: [FileModel]) {
        for file in files {
            if let fileView  = Bundle.main.loadNibNamed("DocumentListView", owner: self, options: nil)?.first as? DocumentListView {
                fileView.setTheme()
                if let title = file.name {
                    fileView.documentTitle.text = title
                }
                if file.content_type?.contains("pdf") ?? false {
                    fileView.iconImageView.image = UIImage(named: "pdfIcon")
                }
                else if file.content_type?.contains("word") ?? false {
                    fileView.iconImageView.image = UIImage(named: "ic_word")
                }
                else if (file.content_type?.contains("sheet") ?? false) || (file.content_type?.contains("excel") ?? false) {
                    fileView.iconImageView.image = UIImage(named: "ic_excel")
                }
                else if file.content_type?.contains("text") ?? false {
                    fileView.iconImageView.image = UIImage(named: "ic_text")
                }
                else {
                    fileView.iconImageView.image = #imageLiteral(resourceName: "attached")
                }
//                fileView.tapButton.isEnabled = false
                if let img = file.src {
                    fileView.fileUrl = img
                    fileView.file { (url) in
                        self.delegate?.preview(fileWith: url, name: file.name ?? "", contentType: file.content_type!)
                    }
                }
                self.doclistStackView.addArrangedSubview(fileView)
                self.doclistStackView.isHidden = false
            }
        }
    }
}

extension ThreadDetailInfoCell: FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        self.threadAttachments.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        if self.threadAttachments[index].content_type!.contains("video") {
            cell.imageView?.isHidden = true
            if controller.view.isHidden{
                controller.view.isHidden = !controller.view.isHidden
            }
            controller = AVPlayerViewController()
            cell.imageView?.gestureRecognizers?.removeAll()
            if let path = self.threadAttachments[index].src, path != ""{
                let videoPath = path
                if let url = URL(string: videoPath){
                    let player = AVPlayer(url: url)
                    controller.player = player
                    controller.view.frame = cell.frame
                    //                    controller.delegate = self
                    cell.backgroundView = controller.view
                    cell.backgroundView?.isUserInteractionEnabled = true
                    cell.tag = index
                    cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.playVideo(_:))))
                }
            }
        }else if self.threadAttachments[index].content_type!.contains("image") {
            cell.imageView?.isHidden = false
            let imgUrl = self.threadAttachments[index].medium_url ?? self.threadAttachments[index].src
            if let path = imgUrl {
                let filePath = path
                if let url = URL(string: filePath){
                    cell.imageView?.imageFromServer(imageUrl: url, placeholder: UIImage())
                }else{
                    cell.imageView?.image = #imageLiteral(resourceName: "groupDefault")
                }
            }else{
                cell.imageView?.image = #imageLiteral(resourceName: "groupDefault")
            }
            cell.imageView?.tag = index
            cell.imageView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showAttachmentsSlide(_:))))
        }else{
            cell.imageView?.isHidden = false
            cell.imageView?.image = #imageLiteral(resourceName: "attached")
            cell.imageView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showFilePreview(_:))))
            cell.imageView?.tag = index
        }
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.isUserInteractionEnabled = true
        return cell
    }
    
    @objc func showAttachmentsSlide(_ sender:UITapGestureRecognizer){
        if let index = sender.view?.tag{
            self.delegate?.didTapImagePreview(startAt: index, with: sender.view as! UIImageView)
        }
    }
    
    @objc func showFilePreview(_ sender:UITapGestureRecognizer){
        if let index = sender.view?.tag{
            self.delegate?.didTapPreview(fileAt: index)
        }
    }
    
    @objc func playVideo(_ sender:UITapGestureRecognizer){
        if let index = sender.view?.tag {
            self.delegate?.didTapPlay(videoAt: index)
        }
    }
}

extension ThreadDetailInfoCell : FSPagerViewDelegate {
    func pagerView(_ pagerView: FSPagerView, didEndDisplaying cell: FSPagerViewCell, forItemAt index: Int) {
        //TODO -
//        print ("Pager is working fine \(index)")
//        print(index)
//        self.counterLabel.text = "\(index + 1) / \(self.commentsDataSource.count)"
    }
    
    func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
        self.counterLabel.text = "\(index + 1) / \(self.threadAttachments.count)"
    }
}

extension ThreadDetailInfoCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        containerView.backgroundColor = theme.settings.cellColor
        bottomContainerView.backgroundColor = theme.settings.highlightedBgColor
        moreButton.tintColor = theme.settings.titleColor
        notificationButton.tintColor = theme.settings.titleColor
        deleteThreadImageView.tintColor = theme.settings.subTitleColor
        groupMailImageView.tintColor = theme.settings.subTitleColor
        threadGroupLabel.textColor = theme.settings.subTitleColor
        timeStampLabel.textColor = theme.settings.subTitleColor
        counterLabel.textColor = theme.settings.whiteColor
    }
}
