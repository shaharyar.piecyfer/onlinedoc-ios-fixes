//
//  ThreadDetailVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 13/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import AVKit
import Alamofire
import FSPagerView
import MobileCoreServices
import SKPhotoBrowser
import WeScan
import VisionKit
import PDFKit
import OpalImagePicker
import Photos
import Lightbox

protocol ThreadDetailVCDelegate: AnyObject {
    func didDeleteThread()
    func didCommentOnthread()
    func didUpdateNotificationStatus(with value: Bool)
}

class ThreadDetailVC: BaseVC {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var newCommentTextView: UITextView!{
        didSet{
            newCommentTextView.layer.cornerRadius = 6
            newCommentTextView.clipsToBounds = true
            newCommentTextView.text = CommentPlaceholder.localized()
        }
    }
    @IBOutlet weak var attachFileButton: UIButton!{
        didSet{
            let image = UIImage(named: "attached-1")?.withRenderingMode(.alwaysTemplate)
            attachFileButton.setImage(image, for: .normal)
        }
    }
    @IBOutlet weak var postCommentButton: UIButton!{
        didSet{
            let image = UIImage(named: "send")?.withRenderingMode(.alwaysTemplate)
            postCommentButton.setImage(image, for: .normal)
        }
    }
    @IBOutlet weak var writeACommentView: UIView!
    @IBOutlet weak var keyboardHeightLayoutConstraint:NSLayoutConstraint!
    @IBOutlet weak var composerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var uiTopBtn: UIView!
    @IBOutlet weak var uiBottomBtn: UIView!
    
    let socketManager = SocketManager.sharedInstance
    var threadId: Int = 0
    var threadData = ThreadModel()
    var threadComments = [ThreadCommentModel]()
    var threadCommentFiles = [ThreadCommentModel]()
    var loadMore = true
    var pageNumber = 1
    var filesLoadMore = true
    var filesPageNumber = 0
    
    var CreateOrEditThreadFrom : CreateOrEditThreadFrom  = .home
    var isCommentEdit = false
    let videoFileName = "/od_video.mov"
    var isDocListVisibility = false
    
    var isAutoScroll = false
    var keyboardHeight : CGFloat = 0
    var commentWith: CommentWith = .none
    var commentToEdit = ThreadCommentModel()
    var indexToEdit: IndexPath?
    weak var delegate : ThreadDetailVCDelegate?
    var documentInteractionController: UIDocumentInteractionController!
    var controller = AVPlayerViewController()
    var pageCell = FSPagerViewCell()
    var cellIndex = -1
    var groupAdminId = 0
    var fileCount = 0
    var fromNotification = false
    var forceRefreshFileList = true
    var forceRefreshCommentList = false
//    var doAnyActivity = false {
//        didSet {
//            if let _ = self.threadData, doAnyActivity {
//                self.threadData?.doc_notification_on = true
//                self.tableView.reloadSections([0], with: .none)
//                NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
//            }
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.setupView()
        self.loadThread()
        self.setupNotificationObservers()
        
        self.setupTheme()
        self.keyboardSetting()
//        self.setupSocket()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: ThreadDetailVC.applyTheme)
        
        Themer.shared.register(
            target: self,
            action: ThreadDetailVC.setTheme)
    }
    
//    private func setupSocket() {
//        socketManager.delegate = self
//    }
    
    private func keyboardSetting(){
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillShowNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillHideNotification,object: nil)
    }
    
    @objc func keyboardNotification(_ notification: NSNotification) {
        let isShowing = notification.name == UIResponder.keyboardWillShowNotification

        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.intValue ?? Int(UIView.AnimationOptions.curveEaseInOut.rawValue)
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: UInt(animationCurveRaw))
            self.keyboardHeightLayoutConstraint?.constant = isShowing ? endFrame!.size.height : 0
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() }) { (completed) in
                if self.threadComments.count > 2 {
                    self.tableView.scrollToBottom()
                }
            }
        }
    }
    
    private func setupNotificationObservers(){
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifyThread, object: nil, queue: .main) { [weak self] (notification) in
            guard let strongSelf = self else { return }
            if let newThread = notification.userInfo?["Thread"] as? ThreadModel {
                if let notify = notification.userInfo?["Notify"] as? NotifyCRUD {
                    switch notify {
                    case .update:
                        strongSelf.threadData = newThread
                        self?.delay(1.0) { [weak self] in
                            guard let `self` = self else { return }
                            self.tableView.reloadSections([0], with: .automatic)
                        }

                    case .create:
                        break
                    case .delete:
                        break
                    default:
                        break
                    }
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .notifySocket, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .CLINIC {
                if let notify = notification.userInfo?["Notify"] as? NotifySocket {
                    switch notify {
                    case .receiveComment:
                        if let data = notification.userInfo?["Data"] as? SocketCommentBaseModel {
                            if self.threadId == data.comment?.data?.doc_id {
                                if data.type == "new", let data = data.comment?.data {
                                    if self.threadComments.isEmpty {
                                        DispatchQueue.main.async {
                                            if !self.isDocListVisibility {
                                                self.tableView.setEmptyMessage("")
                                            }
                                        }
                                    }
                                    self.threadComments.append(data)
                                    self.threadData?.clinic_doc_comments = self.threadComments.suffix(5).reversed()
                                    
                                    DispatchQueue.main.async {
                                        if !self.isDocListVisibility {
//                                            self.tableView.insertRows(at: [IndexPath(row: self.threadComments.count-1, section: 2)], with: .none)
                                            self.tableView.reloadSections([2], with: .none)
                                            self.tableView.scrollToBottom()
                                        }
                                        
                                        if data.comment_creator?.id != AppConfig.shared.user.id {
                                            let commentCount = (self.threadData?.clinic_doc_comments_count ?? 0) + 1
                                            self.threadData?.clinic_doc_comments_count = commentCount
                                        }
                                        if !self.isDocListVisibility {
                                            self.tableView.reloadSections([0], with: .none)
                                        }
                                        
                                        self.updateDocNotificationCount(id: self.threadId)
                                        NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
                                    }
                                }
                                else if let index = self.threadComments.firstIndex(where: { $0.id == data.comment?.data?.id }) {
                                    if let _data = data.comment?.data {
                                        self.threadComments[index] = _data
                                        self.threadData?.clinic_doc_comments = self.threadComments.suffix(5).reversed()
                                        
                                        DispatchQueue.main.async {
                                            if !self.isDocListVisibility {
                                                self.tableView.reloadRows(at: [IndexPath(row: index, section: 2)], with: .automatic)
                                            }
                                            NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
                                        }
                                    }
                                }
                            }
                        }
                        
                    default:
                        break
                    }
                }
            }
        }
    }
    
    deinit {
        print("ThreadDetailVC->deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.commentWith != .none{
            self.handleViewAppearence()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (self.fromNotification) {
            navigationController?.setNavigationBarHidden(true, animated: animated)
        }
    }
    
    func handleViewAppearence(){
        switch self.commentWith{
        case .cam:
            self.commentWith = .none
            break
        case .smiley:
            self.commentWith = .none
            self.newCommentTextView.becomeFirstResponder()
            break
        case .keyboard:
            self.commentWith = .none
            self.newCommentTextView.becomeFirstResponder()
            break
        case .none:
            break
        }
    }
    
    private func setupView() {
        self.rightBarButton()
        self.writeACommentView.layer.cornerRadius = writeACommentView.frame.size.height / 2
        self.writeACommentView.clipsToBounds = true
        self.writeACommentView.layer.borderColor = UIColor.darkGray.cgColor
        self.writeACommentView.layer.borderWidth = 0.5
        self.uiTopBtn.isHidden = true
        self.uiBottomBtn.isHidden = true
    }
    
    private func rightBarButton() {
        let groupInfoButton = UIBarButtonItem(image: #imageLiteral(resourceName: "groupInfo"),  style: .plain, target: self, action: #selector(self.groupInfo))
        self.navigationItem.rightBarButtonItem = groupInfoButton
    }
    
    private func setNavigationTitle() {
        if let groupTitle = self.threadData?.clinic_name {
            self.navigationItem.title = groupTitle.capitalized
            if self.threadData?.doc_type == PostTypes.TASK.rawValue {
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                self.navigationItem.rightBarButtonItem?.tintColor = .clear
            }
        }
    }
    
    private func loadThread() {
        if self.threadId != 0 {
            RequestManager.shared.getThread(id: self.threadId) { [weak self] (status, response) in
                guard let `self` = self else {return}
                if let thread = response as? ThreadModel {
                    self.threadData = thread
                    if let files = thread.doc_files, !files.isEmpty {
                        for item in files {
                            var comment = ThreadCommentModel()
                            comment?.created_at = thread.created_at
                            var creater = CommentCreatorModel()
                            creater?.full_name = thread.doc_creator?.full_name
                            comment?.comment_creator = creater
                            comment?.files = []
                            comment?.files?.append(item)
                            self.threadCommentFiles.append(comment!)
                        }
                    }
                    self.setNavigationTitle()
                    self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    self.loadComments()
                    self.apiCallForNotifyCount()
                }
                
                self.delay(1.0) { [weak self] in
                    guard let `self` = self else {return}
                    self.appDelegate.notificationPayload = nil
                }
            }
        }
    }
    
    private func loadComments() {
        if self.pageNumber == 1 {
            self.threadComments.removeAll()
        }
        self.tableView.setEmptyMessage("")
        RequestManager.shared.getComments(threadId: self.threadId, page: self.pageNumber) { [weak self] (status, response) in
            guard let `self` = self else {return}
            if status, let data = response as? ThreadCommentsModel {
                if let comments = data.clinic_doc_comments {
                    if !comments.isEmpty {
                        if comments.count < pageLimit10 {
                            self.loadMore = false
                        }
                        self.threadComments.insert(contentsOf: comments.reversed(), at: 0)
                        self.tableView.reloadData()
//                        var rows = [IndexPath]()
//                        for (index, _) in comments.enumerated() {
//                            rows.append(IndexPath(row: index, section: 1))
//                        }
//                        self.tableView.insertRows(at: rows, with: .none)
                        if self.pageNumber == 1 {
                            self.onClickBottomScroll(UIButton())
                        }
//                        else {
//                            let index = IndexPath(row: data.count, section: 0)
//                            self.tableView.scrollToRow(at: index, at: .bottom, animated: false)
//                        }
                    } else {
                        self.loadMore = false
                        self.tableView.reloadSections([1], with: .automatic)
                        if self.pageNumber == 1 {
                            self.tableView.setEmptyMessage(noRecordFound.localized())
                        }
                    }
                } else {
                    self.loadMore = false
                }
            }
        }
    }
    
    private func loadCommentFiles() {
        if self.filesPageNumber == 1 {
            self.threadCommentFiles.removeAll()
            if let files = self.threadData?.doc_files, !files.isEmpty {
                for item in files {
                    var comment = ThreadCommentModel()
                    comment?.created_at = self.threadData?.created_at
                    var creater = CommentCreatorModel()
                    creater?.full_name = self.threadData?.doc_creator?.full_name
                    comment?.comment_creator = creater
                    comment?.files = []
                    comment?.files?.append(item)
                    self.threadCommentFiles.append(comment!)
                }
            }
        }
        self.tableView.setEmptyMessage("")
        RequestManager.shared.getCommentFiles(threadId: self.threadId, page: self.filesPageNumber) { (status, response) in
            if status, let result = response as? CommentFileListModel, let data = result.data {
                if !data.isEmpty {
                    for item in data {
                        var comment = item
                        if let files = item.files, !files.isEmpty {
                            for file in files {
                                comment.files = []
                                comment.files?.append(file)
                                self.threadCommentFiles.append(comment)
                            }
                        }
                    }
                } else {
                    self.filesLoadMore = false
                }
                self.tableView.reloadData()
            }
        }
    }
    
    private func updateDocNotificationCount(id: Int) {
        RequestManager.shared.updateDocNotificationCount(id: id) { [weak self] (status, response) in
            guard let `self` = self else {return}
            if status {
                self.apiCallForNotifyCount()
            }
        }
    }
    
    private func setupScrollBtns(){
        self.uiTopBtn.isHidden = true
        self.uiBottomBtn.isHidden = true
    }
    
    @IBAction func onClickTopScroll(_ sender: UIButton) {
        self.tableView.scrollToTop()
    }
    
    @IBAction func onClickBottomScroll(_ sender: UIButton) {
        self.tableView.scrollToBottom()
    }
    
    @objc func groupInfo(){
        let count = self.navigationController?.viewControllers.count
        if ((self.navigationController?.viewControllers[count! - 2] as? GroupInfoVC) != nil){
            self.dismissVC()
        }else{
            segueToGroupInfo()
        }
    }
    
    func segueToGroupInfo() {
        guard let id = self.threadData?.clinic_id else { return }
        //        AppConfig.shared.needsResetGroupInfoScene = true
        if let vc = getVC(storyboard: .TABS, vcIdentifier: "GroupInfoScene") as? GroupInfoVC {
            //            self.removeLabel = false
            vc.groupById = id
            vc.navigatingFrom = .detailPage
            self.navigateVC(vc)
        }
    }
    
    //MARK: - Selector Methods
    func deleteThread() {
        if let id = self.threadData?.id {
            RequestManager.shared.deleteThread(id: id) { (status, response) in
                if status{
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.delete])
                    self.showAlert(To: "", for: DeleteThreadMessage.localized()) { [weak self] _ in
                        guard let `self` = self else {return}
                        self.dismissVC()
                    }
                }
            }
        }
    }
    
    func showScannedFileRenamePopUp(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: "Rename Scanned File".localized(), for: "Do you want to rename scanned file?".localized(), firstBtnStyle: .default, secondBtnStyle: .default, firstBtnTitle: yesText.localized(), secondBtnTitle: noText.localized()) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                self.showSaveDialog(params, data, thumbNail)
            } else {
                self.uploadForSignedId(params, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(params, data, thumbNail)
//                }
            }
        }
    }
    
    func showSaveDialog(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: saveDocuments.localized(), for: "Enter document name".localized(), tfPlaceholder: String(Date().getTime())) { [weak self] (status, text) in
            guard let `self` = self else {return}
            if (text == nil || text!.isEmpty) {
                self.uploadForSignedId(params, data, thumbNail)
            }
            else {
                var info = params
                info.filename = text!+".pdf"
                self.uploadForSignedId(info, data, thumbNail)
            }
        }
    }
    
    func showImageSlider(withStarting index : Int, from imageView:UIImageView){
        var images = [SKPhoto]()
        if let files = threadData?.doc_files {
            let _images = files.filter({$0.content_type!.contains("image")})
            for file in _images {
                if let imgPath = file.src, imgPath != "" {
                    let img = SKPhoto.photoWithImageURL(imgPath)
                    images.append(img)
                }
            }
            if !images.isEmpty {
                self.showSlider(with: images, from: imageView, at: index)
            }
        }
    }
    
    func showSlider(with images: [SKPhoto], from sender: UIImageView, at index:Int?){
        SKPhotoBrowserOptions.displayBackAndForwardButton = true
        SKPhotoBrowserOptions.backgroundColor = .black
        SKPhotoBrowserOptions.enableSingleTapDismiss = true
//        var img  = UIImage()
//
//        if let image  = sender.image{
//            img = image
//        }
        
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
//        let browser = SKPhotoBrowser(originImage: img, photos: images, animatedFromView: sender)
        //            SKPhotoBrowser(photos: images)
        //            SKPhotoBrowser(photos: img, initialPageIndex: index)
        
        
        //        if let i = index{
        //            browser.initializePageIndex(i)
        //        }
        browser.delegate = self
        present(browser, animated: true, completion: nil)
    }
    
    func preview(imageOf view:UIImageView){
        var image = [SKPhoto]()
        if let img = view.image{
            image.append(SKPhoto.photoWithImage(img))
        }
        self.showSlider(with: image, from: view, at: nil)
    }
    
    func play(video:String){
        if let url = URL(string: video){
            let player = AVPlayer(url: url)
            let controller = AVPlayerViewController()
            controller.player = player
            self.present(controller, animated: true, completion: nil)
            player.play()
        }
    }
    
    func preview(file:URL, fileName : String = ""){
        self.documentInteractionController = UIDocumentInteractionController.init(url: file)
        self.documentInteractionController?.delegate = self
//        UITextView.appearance().tintColor = UIColor.whiteOrBlack
//        UINavigationBar.appearance().tintColor = UIColor.whiteOrBlack
        if fileName != "" {
            self.documentInteractionController.name = fileName
        }
        documentInteractionController.presentPreview(animated: true)
    }
    
    @IBAction func selectAttachemnt(){
        self.view.endEditing(true)
        self.showDialogAlert(options: .camera, .library, .video, .docs, .scan)
    }
    
    @IBAction func sendMessage(){
        self.newCommentTextView.resignFirstResponder()
        self.commentWith = .none
        self.isAutoScroll = true
        if let messageText = self.newCommentTextView.text, messageText.trimmingCharacters(in: .whitespacesAndNewlines) != "" && messageText != CommentPlaceholder.localized() {
            if !isCommentEdit {
                var newComment = PostCommentParams()
                if let clinicId = self.threadData?.clinic_id {
                    newComment.clinic_id = clinicId
                }
                if let docId = self.threadData?.id {
                    newComment.doc_id = docId
                }
                newComment.comment = messageText.trimmingCharacters(in: .whitespacesAndNewlines)
                newComment.comment_type = .comment
                self.handleSendNewComment(with: newComment)
            }
            else {
                self.handleEditComment(with: messageText)
            }
        }else{
            return
        }
    }
    
    func handleSendNewComment(with params : PostCommentParams) {
        if let count = self.threadData?.clinic_doc_comments_count {
            self.threadData?.clinic_doc_comments_count = count + 1
        }
        else {
            self.threadData?.clinic_doc_comments_count = 1
        }
        
        if let fileCount = params.files?.count, fileCount > 0 {
            if let count = self.threadData?.clinic_doc_comment_file_count {
                self.threadData?.clinic_doc_comment_file_count = count + 1
            }
            else {
                self.threadData?.clinic_doc_comment_file_count = 1
            }
        }
        
        if let _ = self.threadData, self.threadData?.doc_notification_on == false {
            self.threadData?.doc_notification_on = true
        }
        
        if params.comment_type == CommentTypes.file {
            self.forceRefreshFileList = true
        }
        
//        self.tableView.reloadSections([0], with: .none)
        NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
        
        self.newCommentTextView.text = CommentPlaceholder.localized()
        self.newCommentTextView.textColor = .lightGray
        self.newCommentTextView.resignFirstResponder()

        RequestManager.shared.sendComment(_params: params) { (status, response) in
            if status, let response = response as? ThreadCommentResponse {
                if let comment = response.data, (self.socketManager.client.isConnected == false || (self.socketManager.channelClinic?.isSubscribed ?? false) == false) {
                    if self.threadComments.isEmpty {
                        if !self.isDocListVisibility {
                            self.tableView.setEmptyMessage("")
                        }
                    }
                    self.threadComments.append(comment)
                    self.threadData?.clinic_doc_comments = self.threadComments.suffix(5).reversed()
                    
                    if !self.isDocListVisibility {
//                        self.tableView.insertRows(at: [IndexPath(row: self.threadComments.count-1, section: 2)], with: .none)
                        self.tableView.reloadSections([2], with: .none)
                        self.tableView.scrollToBottom()
                    }
                    
                    if comment.comment_creator?.id != AppConfig.shared.user.id {
                        let commentCount = (self.threadData?.clinic_doc_comments_count ?? 0) + 1
                        self.threadData?.clinic_doc_comments_count = commentCount
                    }
                    if !self.isDocListVisibility {
                        self.tableView.reloadSections([0], with: .none)
                    }
                    
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
                }
            }
        }
    }
    
    func handleEditComment(with text: String) {
        self.isCommentEdit = false
        guard let _ = self.commentToEdit?.id else { return }
        var params = PostCommentParams()
        params.comment_type = .comment
        params.comment = text
        params.commentId = self.commentToEdit!.id

        RequestManager.shared.updateComment(_params: params) { (status, response) in
            if status, let response = response as? ThreadCommentResponse {
                self.view.endEditing(true)
                self.attachFileButton.isHidden = false
                self.newCommentTextView.resignFirstResponder()
                self.newCommentTextView.text = CommentPlaceholder.localized()
                self.newCommentTextView.textColor = .lightGray
                
                if let comment = response.data {
                    self.threadComments[self.indexToEdit!.row] = comment
                    self.tableView.reloadRows(at: [self.indexToEdit!], with: .automatic)
                }
                self.commentToEdit = ThreadCommentModel()
                self.indexToEdit = nil
                self.showAlert(To: UpdateMessageTitle.localized(), for: UpdateMessageSuccess.localized()) {_ in}
                self.threadData?.clinic_doc_comments = self.threadComments.suffix(5).reversed()
                NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
            }
        }
    }
    
    func updateNotificationStatus() {
        let setNotification = !(self.threadData?.doc_notification_on ?? false)
        if let id = self.threadData?.id, let groupId = self.threadData?.clinic_id {
            RequestManager.shared.callNotificationAPI(groupID: groupId, notification: setNotification, docId: id) { (status, response) in
                if status {
                    self.threadData?.doc_notification_on = setNotification
//                    self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
//                    self.delegate?.didUpdateNotificationStatus(with: setNotification)
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
                }
            }
        }
    }
    
    func deleteFile(index: IndexPath) {
        self.showAlert(To: "", for: deleteAlertMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                let item = self.threadCommentFiles[index.row]
                guard let signedId = item.files?.first?.signed_id else { return }
                RequestManager.shared.deleteFile(signedId: signedId) { (status, response) in
                    if status {
                        if let _ = response as? GeneralSuccessResult {
                            self.forceRefreshCommentList = true
                            self.threadCommentFiles.remove(at: index.row)
                            if let index = self.threadData?.doc_files?.firstIndex(where: {$0.signed_id == signedId}) {
                                self.threadData?.doc_files?.remove(at: index)
                            }
                            if let count = self.threadData?.clinic_doc_file_count, count > 0 {
                                self.threadData?.clinic_doc_file_count = count - 1
                            }
                            else {
                                if let count = self.threadData?.clinic_doc_comment_file_count, count > 0 {
                                    self.threadData?.clinic_doc_comment_file_count = count - 1
                                }
                            }
                            self.tableView.reloadData()
                            NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
                            if let id = item.id, item.files?.count == 1 {
                                self.deleteComment(id: id, index: nil)
                                let threadIndex = self.threadComments.firstIndex(where: {($0.files?.contains(where: {$0.signed_id == signedId}) ?? false)})
                                if let threadIndex = threadIndex {
                                    self.threadComments.remove(at: threadIndex)
                                }
                                self.threadData?.clinic_doc_comments = self.threadComments.suffix(5).reversed()
                                
                                let commentCount = (self.threadData?.clinic_doc_comments_count ?? 1) - 1
                                self.threadData?.clinic_doc_comments_count = commentCount
                                self.tableView.reloadSections([0], with: .none)
                                NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
                            }
                            self.showAlert(To: "", for: successfullyDeleted.localized()) { [weak self] _ in
                                guard let `self` = self else {return}
                                self.view.endEditing(true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func playVideo(at index: Int) {
        if let mediaFiles = self.threadData?.doc_files?[index], mediaFiles.content_type!.contains("video"), let viceoUrl = mediaFiles.src {
            if let url = URL(string: viceoUrl) {
                let player = AVPlayer(url: url)
                let cntrlr = AVPlayerViewController()
                cntrlr.player = player
                self.cellIndex = index
                self.present(cntrlr, animated: true) {
                    player.play()
                }
            }
        }
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GroupEmailSegue"{
            let dvc = segue.destination as? GroupEmailVC
            if let groupID = self.threadData?.clinic_id {
                dvc?.groupId = groupID
            }
        }
        if segue.identifier == "gotoEditAttachmentsController" {
            if let vc = segue.destination as? EditFileCommentVC {
                vc.commentToEdit = self.commentToEdit
                vc.updatedComment = { [weak self] data in
                    guard let strongSelf = self else { return }
                    if let data = data {
                        strongSelf.threadComments[strongSelf.indexToEdit!.row] = data
                        strongSelf.tableView.reloadRows(at: [strongSelf.indexToEdit!], with: .automatic)
                        strongSelf.threadData?.clinic_doc_comments = strongSelf.threadComments.suffix(5).reversed()
                        NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": strongSelf.threadData!, "Notify": NotifyCRUD.update])
                    }
                    else {
//                        strongSelf.threadComments.remove(at: strongSelf.indexToEdit!.row)
//                        strongSelf.tableView.deleteRows(at: [strongSelf.indexToEdit!], with: .automatic)
//                        strongSelf.threadData?.clinic_doc_comments = strongSelf.threadComments.suffix(5).reversed()
//
//                        let commentCount = (strongSelf.threadData?.clinic_doc_comments_count ?? 1) - 1
//                        strongSelf.threadData?.clinic_doc_comments_count = commentCount
//                        strongSelf.tableView.reloadSections([0], with: .none)
//                        NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": strongSelf.threadData!, "Notify": NotifyCRUD.update])
                        
                        if let id = strongSelf.threadComments[strongSelf.indexToEdit!.row].id {
                            strongSelf.deleteComment(id: id, index: strongSelf.indexToEdit!)
                        }
                    }
                }
            }
        }
    }
    
    func save(file image:UIImage){
        self.showLoader()
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        self.hideLoader()
        if let error = error {
            self.showAlert(To: FileSaveErrHeading.localized(), for: error.localizedDescription){_ in}
        } else {
            self.showAlert(To: FileSaveSuccess.localized(), for: FileSaveMessage.localized()){_ in}
        }
    }
    
    func downloadDocument(with url: String, name: String) {
        self.showLoader()
        let destination: DownloadRequest.Destination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .picturesDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent(name)
            
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }

        AF.download(url, to: destination)
            .downloadProgress { progress in
                print("Download Progress: \(progress.fractionCompleted)")
            }
            .responseData { response in
            self.hideLoader()
            if let _ = response.fileURL {
                self.showAlert(To: FileSaveSuccess.localized(), for: FileSaveMessage.localized()) {_ in}
            }
            else {
                print("Error in Downloading File")
            }
        }
    }
    
    func storeAndPreview(withURLString: String, name: String = "", contentType: String) {
        guard let url = URL(string: withURLString) else { return }
        self.showLoader()
        URLSession.shared.dataTask(with: url) { data, response, error in
            self.hideLoader()
            guard let data = data, error == nil else { return }
            var fileName = name != "" ? name : response?.suggestedFilename ?? "upload.pdf"
            let ext = mimeTypes.first { (_key, _value) -> Bool in
                if _value == contentType {
                    fileName.append("."+_key)
                    return true
                }
                return false
            }
            if ext == nil {
                fileName.append(".pdf")
            }
            
            let tmpURL = FileManager.default.temporaryDirectory.appendingPathComponent(fileName)
            do {
                try data.write(to: tmpURL)
                DispatchQueue.main.async {
                    self.preview(file: tmpURL, fileName: name)
                }
            } catch {
                print("storeAndPreview->error", error)
            }
        }.resume()
    }
    
    func handlePreview(index: IndexPath, isComments: Bool = true) {
        if isComments {
            guard let file = self.threadComments[index.row].files?.first else {
                return
            }
            if let type = file.content_type {
                if type.contains("image") {
                    let cell = self.tableView.cellForRow(at: index) as? CommentCell
                    if let imageView = cell?.iv1 {
                        self.preview(imageOf: imageView)
                    }
                }
                else if type.contains("video") {
                    if let path = file.src {
                        self.play(video: path)
                    }
                }
                else {
                    if let path = file.src, let name = file.name, let contentType = file.content_type {
                        self.showLoader()
                        self.storeAndPreview(withURLString: path, name: name, contentType: contentType)
                    }
                }
            }
        }
        else {
            let _file = self.threadCommentFiles[index.row]
            if let file = _file.files?.first, let type = file.content_type {
                if type.contains("image") {
                    let cell = self.tableView.cellForRow(at: index) as? DocumentsViewCell
                    if let imageView = cell?.fileContainerView.subviews.first as? UIImageView {
                        self.preview(imageOf: imageView)
                    }

                }
                else if type.contains("video") {
                    if let path = file.src {
                        self.play(video: path)
                    }
                }
                else {
                    if let path = file.src, let name = file.name, let contentType = file.content_type {
                        self.showLoader()
                        self.storeAndPreview(withURLString: path, name: name, contentType: contentType)
                    }
                }
            }
        }
    }
    
    private func navigateToCreateThread() {
        if let vc = getVC(storyboard: .CREATE_THREAD, vcIdentifier: "CreateThreadScene") as? CreateThreadVC {
            vc.formMode = .edit
            vc.createOrEditThreadFrom = .detail
            vc.groupTitle = self.threadData?.clinic_name ?? ""
            vc.groupId = self.threadData?.clinic_id ?? 0
            vc.threadToEdit = self.threadData
            self.navigateVC(vc)
        }
    }
    
    func gotoUserProfile(_ index: Int) {
        let comment = self.threadComments[index]
        if let id = comment.user_id {
            self.previewUser(id: id)
        }
    }
    
    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
        if let theError = error {
            print("error saving the video = \(theError)")
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
            })
        }
    }
    
    private func uploadForSignedId(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?){
        RequestManager.shared.directUpload(_params: params, data: data) { (status, result) in
            if status {
                if let info = result as? DirectUploadModel {
                    var progressView: ProgressView?
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else {return}
                        progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
                        progressView!.lblTitle.text = "txt_progress_title".localized()
                        progressView!.lblDetail.text = "txt_progress_detail".localized()
                        progressView!.uiProgress.progress = 0
                        progressView!.tag = 100
                        progressView!.lblCount.text = "1/1"
                        self.addView(view: progressView!)
                    }
                    
                    let upload = Uploads(directUploadInfo: info, data: data, image: thumbNail)
                    RequestManager.shared.directUploadImage(data: upload.data, params: upload.directUploadInfo) { [weak self] (percent) in
                        guard let _ = self else {return}
                        progressView?.uiProgress.progress = Float(percent)
                    } completion: { [weak self] (status, response) in
                        guard let `self` = self else {return}
                        if let progressView = progressView {
                            self.removeView(tag: progressView.tag)
                        }
                        if status {
                            var newComment = PostCommentParams()
                            if let clinicId = self.threadData?.clinic_id {
                                newComment.clinic_id = clinicId
                            }
                            if let docId = self.threadData?.id {
                                newComment.doc_id = docId
                            }
                            newComment.comment_type = .file
                            newComment.files = [upload.directUploadInfo.signed_id!]
                            self.handleSendNewComment(with: newComment)
                        }
                        else {
                            self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                        }
                    }
                }
            }
        }
    }
    
    private func uploadForSignedId(_ params: [UploadMultiFileParams]) {
        var newComment = PostCommentParams()
        if let clinicId = self.threadData?.clinic_id {
            newComment.clinic_id = clinicId
        }
        if let docId = self.threadData?.id {
            newComment.doc_id = docId
        }
        newComment.comment_type = .file
        newComment.files = []
        
        var progressView: ProgressView?
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else {return}
            progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
            progressView!.lblTitle.text = "txt_progress_title".localized()
            progressView!.lblDetail.text = "txt_progress_detail".localized()
            progressView!.uiProgress.progress = 0
            progressView!.tag = 100
            progressView!.lblCount.text = "1/\(params.count)"
            self.addView(view: progressView!)
        }
        let dispatchQueue = DispatchQueue(label: "FileUploadingQueue", qos: .background)
        let semaphore = DispatchSemaphore(value: 0)

        dispatchQueue.async {
            for (index, item) in params.enumerated() {
                RequestManager.shared.directUpload(_params: item.directUploadParams, data: item.data, isShowLoader: false) { (status, result) in
                    if status {
                        if let info = result as? DirectUploadModel {
                            let upload = Uploads(directUploadInfo: info, data: item.data, image: item.image)
                            RequestManager.shared.directUploadImage(data: upload.data, params: upload.directUploadInfo) { [weak self] (percent) in
                                guard let _ = self else {return}
                                progressView?.lblCount.text = "\(index+1)/\(params.count)"
                                progressView?.uiProgress.progress = Float(percent)
                            } completion: { [weak self] (status, response) in
                                guard let `self` = self else {return}
                                if status {
                                    newComment.files!.append(upload.directUploadInfo.signed_id!)
                                    if newComment.files?.count == params.count {
                                        if let progressView = progressView {
                                            self.removeView(tag: progressView.tag)
                                        }
                                        self.handleSendNewComment(with: newComment)
                                    }
                                }
                                else {
                                    if let progressView = progressView {
                                        self.removeView(tag: progressView.tag)
                                    }
                                    self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                                }
                                semaphore.signal()
                            }
                        }
                    }
                    else {
                        semaphore.signal()
                    }
                }
                semaphore.wait()
            }
        }
    }
    
    private func showMoreOptions(isEdit:Bool, isDelete: Bool, isComment: Bool = false, indexPath: IndexPath? = nil) {
        if isEdit || isDelete {
            var alert = AlertModel()
            if isEdit {
                var editBtn = AlertBtnModel()
                editBtn.title = "Edit"
                alert.btns.append(editBtn)
            }
            if isDelete {
                var deleteBtn = AlertBtnModel()
                deleteBtn.title = "Delete"
                alert.btns.append(deleteBtn)
            }
            var cancelBtn = AlertBtnModel()
            cancelBtn.title = "Cancel"
            cancelBtn.color = .red
            alert.btns.append(cancelBtn)
            self.showMultiBtnAlert(model: alert) { [weak self] (data) in
                guard let `self` = self else {return}
                if let btn = data as? String {
                    if btn == "Edit" {
                        if isComment {
                            self.editComment(index: indexPath!)
                        }
                        else {
                            if self.threadData?.doc_type == PostTypes.TASK.rawValue {
                                self.showAlert(To: "Alert".localized(), for: "Please go to the web portal if you want to edit the task.".localized()) {_ in}
                            } else {
                                self.navigateToCreateThread()
                            }
                        }
                    }
                    else if btn == "Delete" {
                        self.onClickDelete(isComment: isComment, indexPath: indexPath)
                    }
                }
            }
        }
    }
    
    private func onClickDelete(isComment: Bool = false, indexPath: IndexPath? = nil) {
        let message = isComment ? deleteAlertMessage.localized() : (self.threadData?.doc_type == PostTypes.TASK.rawValue ? deleteTaskMessage.localized() : deleteThreadMessage.localized())
        self.showAlert(To: "", for: message, firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                if isComment {
                    if let index = indexPath, let id = self.threadComments[index.row].id {
                        self.deleteComment(id: id, index: index)
                    }
                }
                else {
                    self.deleteThread()
                }
            }
        }
    }
    
    private func deleteComment(id: Int, index: IndexPath?) {
        RequestManager.shared.deleteComment(id: id) { (status, response) in
            if status {
                guard let index = index else {return}
                if self.threadComments[index.row].comment_type == CommentTypes.file.rawValue {
                    self.forceRefreshFileList = true
                    if let count = self.threadData?.clinic_doc_comment_file_count, count >= (self.threadComments[index.row].files?.count ?? 0) {
                        self.threadData?.clinic_doc_comment_file_count = count  - (self.threadComments[index.row].files?.count ?? 0)
                    }
                }
                self.threadComments.remove(at: index.row)
                self.tableView.reloadSections([2], with: .none)
//                self.tableView.deleteRows(at: [index], with: .automatic)
                self.threadData?.clinic_doc_comments = self.threadComments.suffix(5).reversed()
                
                let commentCount = (self.threadData?.clinic_doc_comments_count ?? 1) - 1
                self.threadData?.clinic_doc_comments_count = commentCount
                self.tableView.reloadSections([0], with: .none)
                NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
                self.showAlert(To: DeleteCommentTitle.localized(), for: DeleteCommentSuccess.localized()) {_ in}
            }
        }
    }
    
    func editComment(index: IndexPath) {
        let comment = self.threadComments[index.row]
        self.commentToEdit = comment
        self.indexToEdit = index
        if let type = comment.comment_type, type == CommentTypes.comment.rawValue {
            guard let text = comment.comment else { return }
            self.attachFileButton.isHidden = true
            self.isCommentEdit = true
            self.newCommentTextView.text = text
            self.newCommentTextView.becomeFirstResponder()
        }else {
            self.performSegue(withIdentifier: "gotoEditAttachmentsController", sender: self)
        }
    }
}

extension ThreadDetailVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1 {
            return self.isDocListVisibility ? 0 : (self.loadMore ? 1 : 0)
        }
        else {
            if !self.isDocListVisibility {
                return self.threadComments.count
            }else{
                return self.threadCommentFiles.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ThreadDetailInfoCell.self), for: indexPath) as? ThreadDetailInfoCell else {
                return self.emptyTblCell()
            }
            
            let image = UIImage(named: "showComments")?.withRenderingMode(.alwaysTemplate)
            cell.commentsListButton.setImage(image, for: .normal)
            cell.isDocListVisibility = self.isDocListVisibility
            cell.configCell(indexPath: indexPath, data: self.threadData!)
            cell.delegate = self
            return cell
        }
        else if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoadMoreCell.self), for: indexPath) as? LoadMoreCell else {
                return self.emptyTblCell()
            }
            cell.loadingIndicator.isHidden = true
            cell.loadingIndicator.stopAnimating()
            cell.btnLoadMore.setTitle(loadMoreText.localized(), for: .normal)
            cell.onClickLoadMore = { [weak self] in
                guard let `self` = self else {return}
                self.pageNumber += 1
                self.loadComments()
            }
            return cell
        }
        else {
            if !self.isDocListVisibility {
                guard indexPath.row < self.threadComments.count else {
                    return self.emptyTblCell()
                }
                let comment = self.threadComments[indexPath.row]
//                if indexPath.row == self.threadComments.count - 1 && self.pageNumber == 1 {
//                    self.loadMore = true
//                }
                if AppConfig.shared.user.id == comment.comment_creator?.id {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell2", for: indexPath) as? CommentCell else {
                        print("Unable to read the CommentCell2")
                        return self.emptyTblCell()
                    }
                    cell.configCell(index: indexPath, data: comment, isForMine: true, isAdmin: self.threadData?.is_admin ?? false)
                    cell.onClickProfile = { _ in }
//                    if self.loadMore && indexPath.row == 1 {
//                        self.pageNumber += 1
//                        self.loadComments()
//                    }
                    cell.delegate = self
                    return cell
                }
                else {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CommentCell.self), for: indexPath) as? CommentCell else {
                        print("Unable to read the CommentCell")
                        return self.emptyTblCell()
                    }
                    cell.configCell(index: indexPath, data: comment, isForMine: false, isAdmin: self.threadData?.is_admin ?? false)
                    cell.onClickProfile = { [weak self] (index) in
                        guard let strongSelf = self else { return }
                        strongSelf.gotoUserProfile(index.row)
                    }
//                    if self.loadMore && indexPath.row == 1 {
//                        self.pageNumber += 1
//                        self.loadComments()
//                    }
                    cell.delegate = self
                    return cell
                }
            }
            else {
                guard indexPath.row < self.threadCommentFiles.count else {
                    return self.emptyTblCell()
                }
                guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DocumentsViewCell.self), for: indexPath) as? DocumentsViewCell else {
                    return self.emptyTblCell()
                }
                cell.setDocument(with: self.threadCommentFiles[indexPath.row], at: indexPath)
                cell.delegate = self
                if self.filesLoadMore && indexPath.row == threadCommentFiles.count - 1 {
                    self.filesPageNumber += 1
                    self.loadCommentFiles()
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            if isDocListVisibility {
                self.handlePreview(index: indexPath, isComments: false)
            }
            else {
                if let files = self.threadComments[indexPath.row].files {
                    if files.count > 1 {
                        self.displayImagesVideos(index: indexPath.row)
                    }
                    else {
                        self.handlePreview(index: indexPath)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.row == self.selectedThreadComments.count{
//            if isAutoScroll{
//                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
//                isAutoScroll = false
//            }
//        }
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let totalRows = tableView.numberOfRows(inSection: indexPath.section)
//        if indexPath.row >= 5 {
//            self.uiTopBtn.isHidden = false
//        }
//        if indexPath.row < 5 {
//            self.uiTopBtn.isHidden = true
//        }
//        if indexPath.row < 5 && totalRows > 5 {
//            self.uiBottomBtn.isHidden = true
//        }
//        if indexPath.row == totalRows - 5 {
//            self.uiBottomBtn.isHidden = false
//        }
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if contentYoffset > height/2 {
            self.uiBottomBtn.isHidden = true
            self.uiTopBtn.isHidden = false
        }
        else if contentHeight < height {
            self.uiBottomBtn.isHidden = true
            self.uiTopBtn.isHidden = true
        }
        else if contentYoffset < height {
            self.uiTopBtn.isHidden = true
            let distanceFromBottom = contentHeight - contentYoffset
            if distanceFromBottom < height+20 {
                self.uiBottomBtn.isHidden = true
                self.uiTopBtn.isHidden = false
            } else {
                self.uiBottomBtn.isHidden = false
            }
        }
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName: String(describing: ThreadDetailInfoCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ThreadDetailInfoCell.self))
        tableView.register(UINib(nibName: String(describing: CommentCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CommentCell.self))
        tableView.register(UINib(nibName: "CommentCell2", bundle: nil), forCellReuseIdentifier: "CommentCell2")
        tableView.register(UINib(nibName: String(describing: LoadMoreCell.self), bundle: nil), forCellReuseIdentifier: String(describing: LoadMoreCell.self))
        tableView.register(UINib(nibName: String(describing: DocumentsViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: DocumentsViewCell.self))
        
        self.tableView.separatorStyle = .none
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
    }
}

extension ThreadDetailVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        //        textview.con
        //        textView.text = textView.text
        //        self.adjustUITextViewHeight(txtView: textView)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //        textView.keyboard
        
        if let txt = textView.text, txt == CommentPlaceholder.localized(){
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
            textView.text = nil
        }else{
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
        }
        
        //        var textInputMode: UITextInputMode? {
        //            for mode in UITextInputMode.activeInputModes {
        //                if mode.primaryLanguage == "emoji" {
        //                    return mode
        //                }
        //            }
        //            return nil
        //        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let text = textView.text, textView == self.newCommentTextView else {
            return
        }
        if text.isEmpty {
            textView.textColor = .lightGray
            textView.text = CommentPlaceholder.localized()
        }
    }
}

extension ThreadDetailVC {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let _capturedImage = info[.originalImage] as? UIImage {
            picker.dismiss(animated: true, completion: nil)
            let capturedImage = self.imageOrientation(_capturedImage)
//            self.profileImageView.image = capturedImage
            
            var fileName = UUID().uuidString + ".jpeg"
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                fileName = url.lastPathComponent
    //          fileType = url.pathExtension
            }
            
            let _imageData = capturedImage.jpegData(compressionQuality: capturedImage.size.width > 500 ? 0.5 : 1.0) ?? Data(capturedImage.sd_imageData()!)
            let imageSize: Int = _imageData.count
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //_imageData.fileExtension
            directUploadParams.byte_size = imageSize
            
//            print("ThreadDetailVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, _imageData, capturedImage)
        }
        else if let selectedVideo:URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) {
            // Save video to the main photo album
            let selectorToCall = #selector(self.videoSaved(_:didFinishSavingWithError:context:))
            
            UISaveVideoAtPathToSavedPhotosAlbum(selectedVideo.relativePath, self, selectorToCall, nil)
            // Save the video to the app directory
            var _videoData = Data()
            let videoData = try? Data(contentsOf: selectedVideo)
            let paths = NSSearchPathForDirectoriesInDomains(
                FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
            let dataPath = documentsDirectory.appendingPathComponent(videoFileName)
            
            try! videoData?.write(to: dataPath, options: [])
            
            do {
                let data = try Data(contentsOf: dataPath, options: .mappedIfSafe)
                _videoData = data
            } catch  {
                
            }
            let thumbNail = dataPath.generateThumbnail()
            let fileName = selectedVideo.lastPathComponent
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = _videoData.count
            
//            print("ThreadDetailVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, _videoData, thumbNail)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

//UIDocumentBrowserViewController
extension ThreadDetailVC {
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentsAt documentURLs: [URL]) {
        controller.dismiss(animated: true, completion: nil)
        guard documentURLs.count < maxAllowedAttachments else {
            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
            return
        }
        var multiFilesParams = [UploadMultiFileParams]()
        for url in documentURLs {
            let fileName = url.lastPathComponent
            var fileData = Data()
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                fileData = data
            } catch  {
                print("Unable to get data of the file")
            }
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = fileData.count
            
//            print("ThreadDetailVC->directUploadParams", directUploadParams)
            let param = UploadMultiFileParams(directUploadParams: directUploadParams, data: fileData, image: nil)
            multiFilesParams.append(param)
        }
        if !multiFilesParams.isEmpty {
            self.uploadForSignedId(multiFilesParams)
        }
    }
    
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
//        guard urls.count < maxAllowedAttachments else {
//            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
//            return
//        }
//        var multiFilesParams = [UploadMultiFileParams]()
//        for url in urls {
//            let fileName = url.lastPathComponent
//            var fileData = Data()
//            do {
//                let data = try Data(contentsOf: url, options: .mappedIfSafe)
//                fileData = data
//            } catch  {
//                print("Unable to get data of the file")
//            }
//            var directUploadParams = DirectUploadParams()
//            directUploadParams.filename = fileName
//            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//            directUploadParams.byte_size = fileData.count
//
////            print("ThreadDetailVC->directUploadParams", directUploadParams)
//            let param = UploadMultiFileParams(directUploadParams: directUploadParams, data: fileData, image: nil)
//            multiFilesParams.append(param)
//        }
//        if !multiFilesParams.isEmpty {
//            self.uploadForSignedId(multiFilesParams)
//        }
//    }
//
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
//        let fileName = url.lastPathComponent
//        var fileData = Data()
//        do {
//            let data = try Data(contentsOf: url, options: .mappedIfSafe)
//            fileData = data
//        } catch  {
//            print("Unable to get data of the file")
//        }
//        var directUploadParams = DirectUploadParams()
//        directUploadParams.filename = fileName
//        directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//        directUploadParams.byte_size = fileData.count
//
////        print("ThreadDetailVC->directUploadParams", directUploadParams)
//        self.uploadForSignedId(directUploadParams, fileData, nil)
//    }
//
//    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//        // Picker was cancelled! Duh 🤷🏻‍♀️
//        self.dismiss(animated: true, completion: nil)
//    }
}

//MARK: - CommentCellDelegate
extension ThreadDetailVC: CommentCellDelegate {
    func didTapMoreOptions(index: IndexPath) {
        guard self.threadComments.count > index.row else {return}
        if let ownerId = self.threadComments[index.row].comment_creator?.id {
            if AppConfig.shared.user.id != ownerId {
                if let isAdmin = self.threadData?.is_admin, isAdmin {
                    self.showMoreOptions(isEdit: false, isDelete: true, isComment: true, indexPath: index)
                }
            }
            else {
                self.showMoreOptions(isEdit: true, isDelete: true, isComment: true, indexPath: index)
            }
        }
    }
    
    func previewComment(fileWith url: String, name: String, contentType: String) {
        self.storeAndPreview(withURLString: url, name: name, contentType: contentType)
    }
}

//MARK: - ThreadCommentCellDelegate
extension ThreadDetailVC: ThreadDetailsCellDelegate {
    func preview(fileWith url: String, name: String, contentType: String) {
        self.storeAndPreview(withURLString: url, name: name, contentType: contentType)
    }
    
    func didTapMoreOptions() {
        if let threadOwnerId = self.threadData?.doc_creator?.id {
            if AppConfig.shared.user.id != threadOwnerId {
                if let isAdmin = self.threadData?.is_admin, isAdmin {
                    self.showMoreOptions(isEdit: (self.threadData?.doc_type == PostTypes.TASK.rawValue ? true : false), isDelete: true)
                }
                else if self.threadData?.doc_type == PostTypes.TASK.rawValue {
                    self.showMoreOptions(isEdit: true, isDelete: true)
                }
            }
            else {
                self.showMoreOptions(isEdit: true, isDelete: true)
            }
        }
    }
    
    func didTapChangeNotifications() {
        self.updateNotificationStatus()
    }
    
    func didTapPreview(fileAt index: Int) {
        if let file = self.threadData?.doc_files?[index], let path = file.src, let name = file.name, let contentType = file.content_type {
            self.storeAndPreview(withURLString: path, name: name, contentType: contentType)
        }
    }
    
    func didTapShowList(isDocList: Bool) {
        self.isDocListVisibility = isDocList
//        self.tableView.reloadSections([1], with: .automatic)
        self.tableView.reloadData() // need to reload section 1 because show file count
        if self.forceRefreshCommentList && !isDocList {
            self.forceRefreshCommentList = false
            self.loadMore = true
            self.pageNumber = 1
            self.loadComments()
        }
        else if self.forceRefreshFileList && isDocList {
            self.forceRefreshFileList = false
            self.filesLoadMore = true
            self.filesPageNumber = 1
            self.loadCommentFiles()
        }
    }
    
    func didTapGroupEmail() {
        self.performSegue(withIdentifier: "GroupEmailSegue", sender: self)
    }
    
    func didTapDeleteThread() {
        self.onClickDelete()
    }
    
    //    func didTapDocList() {
    //        self.performSegue(withIdentifier: "DocumentsListSegue", sender: nil)
    //    }
    
    func didTapPlay(videoAt index: Int) {
        self.playVideo(at: index)
    }
    func didTapImagePreview(startAt index: Int, with imageView: UIImageView) {
        self.showImageSlider(withStarting: index, from: imageView)
    }
    
}

extension ThreadDetailVC : DocumentsViewCellDelegate{
    func download(image: UIImage?, url: String?, fileName: String?) {
        if image != nil{
            self.save(file: image!)
        }else{
            if let path = url, let name = fileName {
                self.downloadDocument(with: path, name: name)
            }
        }
    }
    
    func deleteFile(at index: IndexPath) {
        self.deleteFile(index: index)
    }
}

extension ThreadDetailVC: UIDocumentInteractionControllerDelegate{
    private func documentInteractionControllerViewControllerForPreview(controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
        //        UINavigationBar.appearance().tintColor = .white
//        UITextView.appearance().tintColor = .white
        self.newCommentTextView.tintColor = .white
    }
}

//VNDocumentCameraViewControllerDelegate
extension ThreadDetailVC {
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        print(error)
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        guard scan.pageCount >= 1 else {
            controller.dismiss(animated: true)
            return
        }
        DispatchQueue.main.async {
            let pdfDocument = PDFDocument()
            for i in 0 ..< scan.pageCount {
                if  let image = scan.imageOfPage(at: i).resize(toWidth: 700) {
                    print(image)
                    // Create a PDF page instance from your image
                    let pdfPage = PDFPage(image: image)
                    // Insert the PDF page into your document
                    pdfDocument.insert(pdfPage!, at: i)
                }
            }
            // Get the raw data of your PDF document
            let data = pdfDocument.dataRepresentation()
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let docURL = documentDirectory.appendingPathComponent(Date().getTime()+".pdf")
            do{
                try data?.write(to: docURL)
                
                let fileName = docURL.lastPathComponent
                var directUploadParams = DirectUploadParams()
                directUploadParams.filename = fileName
                directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                directUploadParams.byte_size = data!.count
                
//                print("CreateThreadVC->directUploadParams", directUploadParams)
//                self.uploadForSignedId(directUploadParams, data!, nil)
                self.showScannedFileRenamePopUp(directUploadParams, data!, nil)
            }catch(let error)
            {
                print("error is \(error.localizedDescription)")
            }
        }
        controller.dismiss(animated: true)
    }
}

extension ThreadDetailVC {
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        print("ThreadDetailVC->imagePickerDidCancel")
    }
    
    internal func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        var multiFilesParams = [UploadMultiFileParams]()
        for assetItem in assets {
            AppConfig.shared.getURLFromAsset(mPhasset: assetItem) { (url) in
                if url != nil {
                    var fileName = url!.lastPathComponent
                    var fileData = Data()
                    do {
                        let data = try Data(contentsOf: url!, options: .mappedIfSafe)
                        fileData = data
                        if fileName.fileExtension().uppercased() == "HEIC" {
                            if let dataToImage = UIImage(data: data) {
                                fileName = String(fileName.split(separator: ".").first!).appending(".jpeg")
                                fileData = dataToImage.jpegData(compressionQuality: dataToImage.size.width > 500 ? 0.5 : 1.0) ?? data
                            }
                        }
                    } catch  {
                        print("Unable to get data of the file")
                    }
                    var directUploadParams = DirectUploadParams()
                    directUploadParams.filename = fileName
                    directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                    directUploadParams.byte_size = fileData.count
                    let param = UploadMultiFileParams(directUploadParams: directUploadParams, data: fileData, image: nil)
                    multiFilesParams.append(param)
                    if multiFilesParams.count == assets.count {
                        self.uploadForSignedId(multiFilesParams)
                    }
                }
                else {
                    if let img = AppConfig.shared.getAssetThumbnail(asset: assetItem) {
                        let fileName = UUID().uuidString + ".jpeg"
                        let fileData = img.jpegData(compressionQuality: img.size.width > 500 ? 0.5 : 1.0)
                        if let fileData = fileData {
                            var directUploadParams = DirectUploadParams()
                            directUploadParams.filename = fileName
                            directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                            directUploadParams.byte_size = fileData.count
                            let param = UploadMultiFileParams(directUploadParams: directUploadParams, data: fileData, image: nil)
                            multiFilesParams.append(param)
                            if multiFilesParams.count == assets.count {
                                self.uploadForSignedId(multiFilesParams)
                            }
                        }
                    }
                }
            }
        }
        //Dismiss Controller
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 0
    }
    
    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
        return URL(string: "https://placeimg.com/500/500/nature")
    }
}

extension ThreadDetailVC: LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func displayImagesVideos(index: Int) {
        guard let files = self.threadComments[index].files else { return }
        let imagesVides = files.filter{ $0.content_type!.contains("image") || $0.content_type!.contains("video") }
        var combineData = [LightboxImage]()
        for file in imagesVides {
            if file.content_type!.contains("image") {
                if let path = file.src, path != "" {
                    if let url = URL(string: path) {
                        let res = LightboxImage(imageURL: url)
                        combineData.append(res)
                    }
                }
            }else{
                if let path = file.src, path != "" {
                    if let url = URL(string: path) {
                        let res = LightboxImage(image: UIImage(named: "vidThumb")!, text: "", videoURL: url)
                        combineData.append(res)
                    }
                }
            }
        }
        
        // Configure the LightBox
        LightboxConfig.CloseButton.text = closeText.localized()
        // Create an instance of LightboxController.
        let controller = LightboxController(images: combineData)
        
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
        
    }
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print("lightboxController->page", page)
    }
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        print("lightboxController->dissmiss view")
    }
}

extension ThreadDetailVC:  SKPhotoBrowserDelegate {
    func willShowActionSheet(_ photoIndex: Int) {
//        UITextView.appearance().tintColor = UIColor.whiteOrBlack
    }
    
    func didDismissAtPageIndex(_ index: Int) {
//        UITextView.appearance().tintColor = .white
        self.newCommentTextView.tintColor = .white
    }
}

struct DeleteThread {
    var doc_id : Int!
    var clinic_id : Int!
}

struct MultiFilesDeleteParams {
    var clinic_id : Int!
    var doc_id : Int!
    var file_id = [Int]()
    var firebase_id: String!
}

struct PostComment {
    var clinic_id : Int!
    var doc_id : Int!
    var file_id : Int!
    var comment : String!
    var comment_id : Int?
    var docCommentFireBaseId: String!
    var posted_from = ""
}

struct DelThread {
    var doc_id : Int?
    var clinic_id : Int?
}

struct MultiFilesUploadParams{
    var clinicId: Int!
    var threadId: Int!
    var postedFrom: String!
    var firebaseCommentID: String!
}

struct FileUploadData {
    var fileData : Data!
    var attchmentType : String!
    var fileName : String = ""
    var mimeType : String = ""
}

struct editMultiFileCommentParams {
    var groupId : Int!
    var threadId: Int!
    var firebaseCommentID: String!
    var fileIDs = [Int]()
    var posted_from = "multiFile"
}

extension ThreadDetailVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        attachFileButton.tintColor = theme.settings.titleColor
        postCommentButton.tintColor = theme.settings.titleColor
    }
}

//extension ThreadDetailVC: SocketManagerDelegate {
//    func receiveComment(data: SocketCommentBaseModel) {
//        if self.threadId == data.comment?.data?.doc_id {
//            if data.type == "new", let data = data.comment?.data {
//                if self.threadComments.isEmpty {
//                    DispatchQueue.main.async {
//                        if !self.isDocListVisibility {
//                            self.tableView.setEmptyMessage("")
//                        }
//                    }
//                }
//                threadComments.append(data)
//                self.threadData?.clinic_doc_comments = self.threadComments.suffix(5).reversed()
//
//                DispatchQueue.main.async {
//                    if !self.isDocListVisibility {
//                        self.tableView.insertRows(at: [IndexPath(row: self.threadComments.count-1, section: 2)], with: .none)
//                        self.tableView.scrollToBottom()
//                    }
//
//                    let commentCount = (self.threadData?.clinic_doc_comments_count ?? 0) + 1
//                    self.threadData?.clinic_doc_comments_count = commentCount
//                    if !self.isDocListVisibility {
//                        self.tableView.reloadSections([0], with: .none)
//                    }
//
//                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
//                }
//            }
//            else if let index = self.threadComments.firstIndex(where: { $0.id == data.comment?.data?.id }) {
//                if let _data = data.comment?.data {
//                    self.threadComments[index] = _data
//                    self.threadData?.clinic_doc_comments = self.threadComments.suffix(5).reversed()
//
//                    DispatchQueue.main.async {
//                        if !self.isDocListVisibility {
//                            self.tableView.reloadRows(at: [IndexPath(row: index, section: 2)], with: .automatic)
//                        }
//                        NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadData!, "Notify": NotifyCRUD.update])
//                    }
//                }
//            }
//        }
//    }
//}
