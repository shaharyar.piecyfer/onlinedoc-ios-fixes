//
//  UsersListViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 14/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class UsersListViewCell: UITableViewCell {

    @IBOutlet weak var recipientProfileImage: UIImageView!
    @IBOutlet weak var recipientNameLabel: UILabel!
    @IBOutlet weak var recipientEmailLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
