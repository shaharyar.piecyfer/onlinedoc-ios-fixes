//
//  NotificationVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/11/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

class NotificationVC: BaseVC {
    //MARK: - IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar:UISearchBar!
    
    var searchedText = ""
    var pageNumber = 1
    var loadMore = true
    var threadNotificationData = [ThreadNotificationModel]()
    var groupRequests = [MemberModel]()
    
    @objc private func refreshData(_ sender: Any) {
        refreshControl.beginRefreshing()
        self.pageNumber = 1
        self.loadMore = true
        self.getThreadNotification(self.searchedText)
        self.apiCallForNotifyCount()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppConfig.shared.lastVisitedTabIndex = 1
        self.setupTBL()
        self.layOutSearchbar()
        self.getThreadNotification()

        self.setupTheme()
        self.setupNotificationObservers()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: NotificationVC.applyTheme)

        Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    private func setupNotificationObservers() {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifySocket, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            if let notify = notification.userInfo?["Notify"] as? NotifySocket {
                switch notify {
                case .threadNotification, .taskNotification:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let data = notification.userInfo?["Data"] as? SocketThreadNotificationBaseModel {
                            if let index = self.threadNotificationData.firstIndex(where: { $0.doc?.id == data.doc?.doc?.id }) {
                                if let _data = data.doc {
                                    _data.user_notification_count = data.user_notification_count
                                    _data.activity_type = data.activity_type
//                                    if (self.threadNotificationData[index].user_notification_count ?? 0) == 0 {
//                                        self.apiCallForNotifyCount()
//                                    }
                                    self.threadNotificationData[index] = _data
                                    DispatchQueue.main.async {
                                        let sortedList = self.sortedThreadNotificatioList(list: self.threadNotificationData)
                                        self.threadNotificationData = sortedList
                                        self.tableView.reloadData()
                                    }
                                }
                            }
                            else {
                                if let _data = data.doc {
                                    _data.user_notification_count = data.user_notification_count
                                    _data.activity_type = data.activity_type
                                    if self.threadNotificationData.count == 0 {
                                        self.threadNotificationData = []
                                    }
                                    self.threadNotificationData.insert(_data, at: 0)
                                    DispatchQueue.main.async {
                                        self.tableView.insertRows(at: [IndexPath(row: 0, section: 1)], with: .automatic)
                                    }
                                }
                            }
                        }
                    }
                    
                case .groupRequestNotification:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let data = notification.userInfo?["Data"] as? SocketGroupRequestNotificationBaseModel {
                            if let _data = data.notification?.notification {
                                if !self.groupRequests.isEmpty, let _ = self.groupRequests.first(where: { $0.user_id == _data.user_id && $0.clinic_id == _data.clinic_id }) {return}
                                self.groupRequests.insert(_data, at: 0)
                                DispatchQueue.main.async {
                                    self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                                }
                            }
                        }
                    }
                    
                case .readNotification:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let data = notification.userInfo?["Data"] as? SocketThreadNotificationBaseModel {
                            if let index = self.threadNotificationData.firstIndex(where: { $0.doc?.id == data.doc?.doc?.id }) {
                                if let _data = data.doc {
                                    _data.user_notification_count = data.user_notification_count
                                    _data.activity_type = data.activity_type
                                    self.threadNotificationData[index] = _data
                                    DispatchQueue.main.async {
                                        let sortedList = self.sortedThreadNotificatioList(list: self.threadNotificationData)
                                        self.threadNotificationData = sortedList
                                        self.tableView.reloadData()
                                    }
                                }
                                self.apiCallForNotifyCount()
                            }
                        }
                    }
                    
                default:
                    break
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .notifyThread, object: nil, queue: .main) { [weak self] (notification) in
            guard let strongSelf = self else { return }
            if let newThread = notification.userInfo?["Thread"] as? ThreadModel {
                if let notify = notification.userInfo?["Notify"] as? NotifyCRUD {
                    switch notify {
                    case .update:
                        let indexToEdited = strongSelf.threadNotificationData.firstIndex(where: {$0.doc?.id == newThread.id})
                        if let indexToEdited = indexToEdited {
                            strongSelf.threadNotificationData[indexToEdited].doc = newThread
                            strongSelf.tableView.reloadData()
                        }
                    case .delete:
                        let indexToEdited = strongSelf.threadNotificationData.firstIndex(where: {$0.doc?.id == newThread.id})
                        if let indexToEdited = indexToEdited {
                            strongSelf.threadNotificationData.remove(at: indexToEdited)
                            strongSelf.tableView.reloadData()
                        }
                    default:
                        break
                    }
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .notifyGroupRequest, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            self.getGroupRequests()
            self.apiCallForNotifyCount()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
    }
    
    deinit {
        print("NotificationVC->deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func layOutSearchbar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = SearchText.localized()
    }
    
    private func getThreadNotification(_ search: String = "") {
        if self.pageNumber == 1 {
            self.threadNotificationData.removeAll()
        }
        RequestManager.shared.getThreadNotifications(page: pageNumber, searchText: search) { (status, response) in
            self.refreshControl.endRefreshing()
            if status{
                if let result = response as? ThreadNotificationResult, let data = result.data {
                    if data.count < pageLimit50 {
                        self.loadMore = false
                    }
                    
                    let sortedList = self.sortedThreadNotificatioList(list: data)
                    self.threadNotificationData.append(contentsOf: sortedList)
                    self.tableView.reloadData()
                    if self.pageNumber == 1 {
                        self.getGroupRequests()
                    }
                }
            }
        }
    }
    
    func sortedThreadNotificatioList(list: [ThreadNotificationModel]) -> [ThreadNotificationModel] {
        let unreadThreadList = list.filter { ($0.user_notification_count ?? 0) > 0 }
        let readThreadList = list.filter { ($0.user_notification_count ?? 0) == 0 }
        let newList = unreadThreadList.sorted(by: { ($0.doc?.updated_at ?? Date().toString()) > ($1.doc?.updated_at ?? Date().toString()) }) + readThreadList
        return newList
    }
    
    private func getGroupRequests() {
        RequestManager.shared.getGroupRequests() { (status, response) in
            if status{
                if let result = response as? GroupMemberModel, let data = result.requested_users {
                    self.groupRequests = data.sorted{ ($0.join_date ?? Date().toString()) > ($1.join_date ?? Date().toString()) }
                    self.tableView.reloadSections([0], with: .automatic)
                }
            }
        }
    }
    
    private func gotoThreadDetail(index: IndexPath) {
        if let id = self.threadNotificationData[index.item].doc?.id {
            if let vc = getVC(storyboard: .TABS, vcIdentifier: "DetailView") as? ThreadDetailVC {
                vc.threadId = id
                self.navigateVC(vc)
            }
        }
    }
    
    func gotoUserProfile(_ index: IndexPath) {
        let userId = self.groupRequests[index.row].user_id
        if let id = userId, id != AppConfig.shared.user.id {
            self.previewUser(id: id)
        }
    }
    
    func handleGroupRequest(_ index: IndexPath, isAccept: Bool) {
        var requestData = MembershipRequest()
        requestData.clinic_id = self.groupRequests[index.row].clinic_id
        requestData.user_id = self.groupRequests[index.row].user_id
        requestData.request_status = isAccept ? GroupStatus.APPVOVED.rawValue : GroupStatus.REJECTED.rawValue
        
        RequestManager.shared.updateGroupStatus(request: requestData) { (status, data) in
            if status {
                self.groupRequests.remove(at: index.row)
                self.tableView.reloadData()
                self.apiCallForNotifyCount()
            }
            else {
                self.getGroupRequests()
                self.apiCallForNotifyCount()
            }
        }
    }
}

//MARK : - EXTENSIONS
extension NotificationVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchedText = ""
            self.searchBar.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.pageNumber = 1
            self.loadMore = true
            self.getThreadNotification()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.pageNumber = 1
        self.loadMore = true
        self.searchedText = searchBar.text ?? ""
        self.getThreadNotification(searchedText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchBar.resignFirstResponder()
    }
}

extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.groupRequests.count
        } else {
            return self.threadNotificationData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ConnectionViewCell.self), for: indexPath) as? ConnectionViewCell else {
                return self.emptyTblCell()
            }
            guard indexPath.row < self.groupRequests.count else { return self.emptyTblCell() }
            cell.index = indexPath
            cell.setupRequests(indexPath, data: self.groupRequests[indexPath.row])
            cell.action {  [weak self] (index, isProfile, isAccept) in
                guard let strongSelf = self else { return }
                if isProfile{
                    strongSelf.gotoUserProfile(index)
                }else{
                    strongSelf.handleGroupRequest(index, isAccept: isAccept)
                }
            }
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowCell", for: indexPath) as? FollowCell else{
                print("The dequeued cell is not an instance of ChatBaseVC.")
                return self.emptyTblCell()
            }
            if indexPath.item < self.threadNotificationData.count, let doc = self.threadNotificationData[indexPath.item].doc {
                let item = self.threadNotificationData[indexPath.item]
                if doc.doc_type == PostTypes.THREAD.rawValue {
                    cell.lblTitle.text = doc.clinic_name
                }
                else {
                    cell.lblTitle.attributedText = doc.title?.attributedText(fontSize: 16, allStyleFont: AppFont.kMuliSemiBold(16).font())?.attributedString ?? doc.description?.attributedText(fontSize: 16, allStyleFont: AppFont.kMuliSemiBold(16).font())?.attributedString ?? nil
                }
                
                if doc.doc_type == PostTypes.THREAD.rawValue {
                    cell.lblDetail.attributedText = (doc.title?.isEmpty ?? true) ? doc.description?.attributedText(fontSize: 16, allStyleFont: AppFont.kMuliLight(16).font())?.attributedString : (doc.title?.attributedText(fontSize: 16, allStyleFont: AppFont.kMuliLight(16).font())?.attributedString ?? doc.description?.attributedText(fontSize: 16, allStyleFont: AppFont.kMuliLight(16).font())?.attributedString ?? nil)
                }
                else if let activityType = self.threadNotificationData[indexPath.item].activity_type {
                    switch activityType {
                    case TaskActivityTypes.CREATED.value():
                        cell.lblDetail.text = TaskActivityTypes.CREATED.msg()
                    case TaskActivityTypes.COMMENT_ADDED.value():
                        cell.lblDetail.text = TaskActivityTypes.COMMENT_ADDED.msg()
                    case TaskActivityTypes.EDITED.value():
                        cell.lblDetail.text = TaskActivityTypes.EDITED.msg()
                    case TaskActivityTypes.FILE_ADDED.value():
                        cell.lblDetail.text = TaskActivityTypes.FILE_ADDED.msg()
                    default:
                        cell.lblDetail.text = ""
                    }
                }
                let unreadCount = item.user_notification_count ?? 0
                cell.lblUnreadCount.text = "\(unreadCount)"
                cell.viewUnreadCount.isHidden = (unreadCount == 0)
                let imgUrl = doc.groupImage?.medium_url ?? doc.groupImage?.src
                if let imageUrl = imgUrl, imageUrl != ""{
                    let url = URL(string: imageUrl)
                    cell.imgView.imageFromServer(imageUrl: url!, placeholder: UIImage(named: "ic_group")!)
                }
                else {// #imageLiteral(resourceName: "ic_group")
                    cell.imgView.image = UIImage(named: "ic_group")
                }
                if self.loadMore && indexPath.row == (self.threadNotificationData.count - 1) {
                    self.pageNumber += 1
                    self.getThreadNotification(self.searchedText)
                }
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let item = self.threadNotificationData[indexPath.item]
            if let id = item.doc?.clinic_id, (item.user_notification_count ?? 0) > 0 {
                self.postNotifyToGroupUnread(groupId: id, notifications: item.user_notification_count!)
            }
            item.user_notification_count = 0
            //            self.tableView.reloadRows(at: [indexPath], with: .fade)
            self.gotoThreadDetail(index: indexPath)
            
            //sorting
            let sortedList = self.sortedThreadNotificatioList(list: self.threadNotificationData)
            self.threadNotificationData = sortedList
            self.tableView.reloadData()
        }
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: String(describing: FollowCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: FollowCell.self))
        self.tableView.register(UINib(nibName: String(describing: ConnectionViewCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: ConnectionViewCell.self))
        
        self.tableView.separatorStyle = .none
        
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }
}
