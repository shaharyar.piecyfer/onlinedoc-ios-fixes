//
//  AccountSettingVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 22/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField

class AccountSettingVC: BaseVC {
    
    var inactivityTime = 0
    var isUserUpdated:(()->())?
    
    //MARK: - Outlets
    @IBOutlet weak var uiChangePassword: UIView!
    @IBOutlet weak var uiInactivitySetting: UIView!
    @IBOutlet weak var uiLanguage: UIView!
    @IBOutlet weak var currentPasswordTextField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: currentPasswordTextField)
        }
    }
    
    @IBOutlet weak var newPasswordTextfield: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: newPasswordTextfield)
        }
    }
    
    @IBOutlet weak var confirmPasswordTexrtField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: confirmPasswordTexrtField)
        }
    }
    
    @IBOutlet weak var inactivitySettingsField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: inactivitySettingsField)
        }
    }
    @IBOutlet weak var danishLanguageButton: UIButton!
    @IBOutlet weak var englishLanguageButton: UIButton!
    @IBOutlet var languageButtons: [UIButton]!
    
    @IBOutlet weak var btnUpdatePassword: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = AccountSettingTitle.localized()
        layoutSubViewTextFieldIcons()
        self.inactivitySettingsField.text = String(self.inactivityTime)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTap)))
        
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: AccountSettingVC.setTheme)
    }
    
    @objc func viewTap(){
        self.view.endEditing(true)
    }
    
    func layoutSubViewTextFieldIcons() {
        AppConfig.shared.setTextfieldIcon(for: self.currentPasswordTextField, addIcon: UIImage(named: "secureEntry")!)
        AppConfig.shared.setTextfieldIcon(for: self.newPasswordTextfield, addIcon: UIImage(named: "secureEntry")!)
        AppConfig.shared.setTextfieldIcon(for: self.confirmPasswordTexrtField, addIcon: UIImage(named: "secureEntry")!)
        self.layOutLanguageButtons()
    }
    
    func layOutLanguageButtons()  {
        self.danishLanguageButton.roundCorners(corners: [.topLeft, .bottomLeft], radius: 6.0)
        self.englishLanguageButton.roundCorners(corners: [.topRight, .bottomRight], radius: 6.0)
        
        switch AppConfig.shared.currentLanguage() {
        case .DANISH:
            self.setButtonSelection(danishLanguageButton)
        default:
            self.setButtonSelection(englishLanguageButton)
        }
    }
    
    func setButtonSelection(_ sender:UIButton) {
        for button in languageButtons{
            if button != sender{
                guard let title = button.currentTitle else { return }
                button.backgroundColor = AppColors.setInActive
                button.alpha = 0.5
                button.setImage(UIImage(named: "\(title)Inactive"), for: .normal)
            }else{
                guard let title = button.currentTitle else { return }
                button.backgroundColor = AppColors.setActive
                button.alpha = 1
                button.setImage(UIImage(named: title), for: .normal)
            }
        }
    }
    
    @IBAction func selectLanguage(_ sender:UIButton){
        self.setButtonSelection(sender)
        let currentLang = AppConfig.shared.currentLanguage()
        if sender == englishLanguageButton {
            if currentLang == .DANISH {
                AppConfig.shared.setLanguage(lang: .ENGLISH)
                AppConfig.shared.changeRootVC()
            }
        }else{
            if currentLang == .ENGLISH {
                AppConfig.shared.setLanguage(lang: .DANISH)
                AppConfig.shared.changeRootVC()
            }
        }
    }
    
    @IBAction func deleteUserProfile(_ sender: UIButton){
        
    }
    
    @IBAction func updateInactivityTime(){
        _ = self.inactivitySettingsField.resignFirstResponder()
        var time = 0
        if let time = self.inactivitySettingsField.text, time == "0" || time == "" {
            self.showAlert(To: InactivityValidation.localized(), for: InactivityValidationMessage.localized()) { [weak self] (status) in
                guard let `self` = self else {return}
                _ = self.inactivitySettingsField.text = String(self.inactivityTime)
                _ = self.inactivitySettingsField.becomeFirstResponder()
                return
            }
            return
        }
        
        if let value = self.inactivitySettingsField.text{
            time = Int(value)!
        }else {
            time = 5
        }
        
        RequestManager.shared.updateInactivityTime(timeValue: time) { (status,responseData) in
            if status{
                if let _ = responseData as? LoginSuccessResponse {
                    AppConfig.shared.user.inactivity_limit = Int(time)
                    self.isUserUpdated?()
                    self.showAlert(To: InactivityHeader.localized(), for: InactivityMessage.localized()) {_ in}
                }
            }
        }
    }
   
    @IBAction func updatePassword(){
        var passwordCreds = ResetPassword()
        if currentPasswordTextField.text!.isEmpty{
            self.showAlert(To: "Current Password".localized(), for: "current password is required and can't be empty".localized()) {_ in}
            return
        }else{
            passwordCreds.oldPassword = currentPasswordTextField.text!
        }
        if let newPassword = self.newPasswordTextfield.text, let confirmPassword = self.confirmPasswordTexrtField.text{
            
            if newPassword.isEmpty && confirmPassword.isEmpty{
                self.showAlert(To: "New Password".localized(), for: "New Password and Confirm New Password is required and can't be empty".localized()) {_ in}
                return
            }else if newPassword.isEmpty{
                self.showAlert(To: "New Password".localized(), for: "New Password is required and can't be empty".localized()) {_ in}
                return
            }else if confirmPassword.isEmpty{
                self.showAlert(To: "New Password".localized(), for: "Confirm New password is required".localized()) {_ in}
                return
            }else if newPassword.count < 6 || confirmPassword.count < 6{
                self.showAlert(To: "New Password".localized(), for: "Password be at least 6 character long".localized()) {_ in}
            }else{
                passwordCreds.newPassword = newPassword
                passwordCreds.newConfirmPassword = confirmPassword
            }
            
            RequestManager.shared.updatePassword(passwordCreds: passwordCreds) { (status,responseData) in
                if status {
                    if let data = responseData as? GeneralSuccessResult {
                        var credentialsDic = UserDefaults.standard.object(forKey: CREDENTIALS_DICT) as? [[String:Any]] ?? [[String:Any]]()
                        if let index = (credentialsDic.firstIndex(where: { $0["userName"] as? String == AppConfig.shared.user.email })){
                            credentialsDic[index]["passCode"] = passwordCreds.newPassword
                            UserDefaults.standard.set(credentialsDic, forKey: CREDENTIALS_DICT)
                        }
                        self.view.endEditing(true)
                        self.currentPasswordTextField.text = ""
                        self.newPasswordTextfield.text = ""
                        self.confirmPasswordTexrtField.text  = ""
                        self.showAlert(To: "Alert".localized(), for: data.message!.localized()) {_ in}
                    }else{
                        self.view.endEditing(true)
                    }
                }else{
                }
            }
        }
    }
    
    deinit {
        print("AccountSettingVC->deint")
    }
}

extension AccountSettingVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == currentPasswordTextField{
            _ = newPasswordTextfield.becomeFirstResponder()
        }else if textField == newPasswordTextfield{
            _ = confirmPasswordTexrtField.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == currentPasswordTextField{
            _ = newPasswordTextfield.becomeFirstResponder()
        }else if textField == newPasswordTextfield{
            _ = confirmPasswordTexrtField.becomeFirstResponder()
        }else if textField == inactivitySettingsField{
            if let text = textField.text {
                let number = Int(text)
                if number == 0{
                    self.showAlert(To: InactivityValidation.localized(), for: InactivityValidationMessage.localized()) { [weak self] (status) in
                        guard let `self` = self else {return}
                        _ = self.inactivitySettingsField.becomeFirstResponder()
                    }
                }else{
                    self.view.endEditing(true)
                }
            }else{
                self.view.endEditing(true)
            }
            
        }else{
            self.view.endEditing(true)
        }
    }
}

struct ResetPassword {
    var oldPassword: String!
    var newPassword: String!
    var newConfirmPassword: String!
}


extension AccountSettingVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        uiChangePassword.backgroundColor = theme.settings.highlightedBgColor
        uiInactivitySetting.backgroundColor = theme.settings.highlightedBgColor
        uiLanguage.backgroundColor = theme.settings.highlightedBgColor
        btnUpdatePassword.backgroundColor = theme.settings.highlightedBgColor
        btnUpdatePassword.setTitleColor(theme.settings.titleColor, for: .normal)
        btnUpdate.backgroundColor = theme.settings.highlightedBgColor
        btnUpdate.setTitleColor(theme.settings.titleColor, for: .normal)
        currentPasswordTextField.tintColor = theme.settings.titleColor
        currentPasswordTextField.textColor = theme.settings.titleColor
        newPasswordTextfield.tintColor = theme.settings.titleColor
        newPasswordTextfield.textColor = theme.settings.titleColor
        confirmPasswordTexrtField.tintColor = theme.settings.titleColor
        confirmPasswordTexrtField.textColor = theme.settings.titleColor
        inactivitySettingsField.tintColor = theme.settings.titleColor
        inactivitySettingsField.textColor = theme.settings.titleColor
    }
}
