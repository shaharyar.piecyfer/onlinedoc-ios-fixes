//
//  AddEmailTableViewCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 19/01/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField

class AddEmailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tfEmail: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: tfEmail)
        }
    }
    
    var onClickAddBtn : ((String)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tfEmail.placeholder = "Email".localized()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: AddEmailTableViewCell.applyTheme)
        
        Themer.shared.register(
            target: self,
            action: AddEmailTableViewCell.setTheme)
    }

    @IBAction func onClickAdd(_ sender: UIButton) {
        self.onClickAddBtn?(tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        self.tfEmail.text = ""
    }

}

extension AddEmailTableViewCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        tfEmail.textColor = theme.settings.titleColor
    }
}
