//
//  GroupThreadListVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 10/06/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class GroupThreadListVC: BaseVC {
    var groupsMember = [GroupModel]()
    var groupsAdmin = [GroupModel]()
    var groupsOther = [GroupModel]()
    
    var loadMore = true
    var pageNumber = 1
    var searchedText = ""
    var userThreads = [ThreadModel]()
    var userThreadsSource = [ThreadModel]()
    
    var isGroupList = true
    var CreateOrEditThreadFrom : CreateOrEditThreadFrom = .profile
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    @objc private func refreshData(_ sender: Any) {
        refreshControl.beginRefreshing()
        self.pageNumber = 1
        self.loadMore = true

        self.callAPI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isGroupList {
            self.navigationItem.title = Groups.localized()
            self.setupNotificationObservers()
        }
        self.setupTBL()
        self.layOutSearchbar()
        self.setUpRightButton()
        self.callAPI()
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    private func setupNotificationObservers(){
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifyGroup, object: nil, queue: .main) { [weak self] (notification) in
            guard let strongSelf = self else { return }
            if let group = notification.userInfo?["Group"] as? GroupModel {
                if let notify = notification.userInfo?["Notify"] as? NotifyCRUD {
                    switch notify {
                    case .create:
                        strongSelf.groupsAdmin.insert(group, at: 0)
                        strongSelf.tableView.reloadData()
                    case .update:
                        let indexToEdited = strongSelf.groupsAdmin.firstIndex(where: {$0.id == group.id})
                        if let indexToEdited = indexToEdited {
                            strongSelf.groupsAdmin[indexToEdited] = group
                            strongSelf.tableView.reloadData()
                        }
                        else if let indexToEdited = strongSelf.groupsMember.firstIndex(where: {$0.id == group.id}) {
                            strongSelf.groupsMember[indexToEdited] = group
                            strongSelf.tableView.reloadData()
                        }
                    case .delete:
                        let indexToDeleted = strongSelf.groupsAdmin.firstIndex(where: {$0.id == group.id})
                        if let indexToDeleted = indexToDeleted {
                            strongSelf.groupsAdmin.remove(at: indexToDeleted)
                            strongSelf.tableView.reloadData()
                        }
                        else if let indexToDeleted = strongSelf.groupsMember.firstIndex(where: {$0.id == group.id}) {
                            strongSelf.groupsMember.remove(at: indexToDeleted)
                            strongSelf.tableView.reloadData()
                        }
                    default:
                        break
                    }
                }
            }
            else if let searchUpdate = notification.userInfo?["SearchUpdate"] as? Bool, searchUpdate {
                strongSelf.searchBar.text = ""
                strongSelf.searchedText = ""
                strongSelf.pageNumber = 1
                strongSelf.loadMore = true
                strongSelf.callAPI()
            }
        }
        
        NotificationCenter.default.addObserver(forName: .notifySocket, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            if let notify = notification.userInfo?["Notify"] as? NotifySocket {
                switch notify {
                case .clinicJoined:
                    if let channel = notification.userInfo?["Channel"] as? SocketChannels, channel == .NOTIFICATION {
                        if let data = notification.userInfo?["Data"] as? SocketClinicModel, let group = data.data {
                            self.groupsMember.insert(group, at: 0)
                            self.tableView.reloadData()
                        }
                    }
                    
                default:
                    break
                }
            }
        }
    }
    
    deinit {
        print("GroupThreadListVC->deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    func callAPI(){
        if isGroupList {
            if self.pageNumber == 1 {
                self.groupsAdmin.removeAll()
                self.groupsMember.removeAll()
                self.groupsOther.removeAll()
                self.tableView.reloadData()
            }
            self.tableView.setEmptyMessage("")
            RequestManager.shared.getGroups(page: self.pageNumber, searchText: self.searchedText) { [weak self] (status, response) in
                guard let `self` = self else { return }
                self.loadMore = true
                self.refreshControl.endRefreshing()
                if status {
                    if let data = response as? ClinicListModel {
                        if let groups = data.groups, groups.isEmpty, self.pageNumber == 1 {
                            self.tableView.setEmptyMessage(noRecordFound.localized())
                            return
                        }
                        if let groups = data.groups, !groups.isEmpty {
                            self.groupsAdmin.append(contentsOf: groups.filter({ item in
                                item.role == GroupMemberRoles.SUPER_ADMIN.rawValue || item.role == GroupMemberRoles.ADMIN.rawValue
                            }))
                            self.groupsMember.append(contentsOf: groups.filter({ item in
                                item.role == GroupMemberRoles.USER.rawValue && item.request_status == GroupStatus.APPVOVED.rawValue
                            }))
                            self.groupsOther.append(contentsOf: groups.filter({ item in
                                 (item.role == GroupMemberRoles.USER.rawValue && item.request_status == GroupStatus.REQUESTED.rawValue) || item.role == nil
                            }))
                        }
                        if (data.groups?.count ?? 0) < pageLimit10 {
                            self.loadMore = false
                        }
                        self.tableView.reloadData()
                    }
                }
            }
        }
        else {
            self.getMyThreads()
        }
    }
    
    private func getMyThreads() {
        if self.pageNumber == 1 {
            self.userThreads.removeAll()
        }
        self.tableView.setEmptyMessage("")
        RequestManager.shared.getMyThreads(page: self.pageNumber, searchText: self.searchedText) { [weak self] (status, response) in
            guard let `self` = self else { return }
            self.refreshControl.endRefreshing()
            if status {
                if let response = response as? ClinicDocModel {
                    if let data = response.result, data.isEmpty, self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                    }
                    if let dataSource = response.result {
                        self.userThreads.append(contentsOf: dataSource)
                        if dataSource.count == 0 {
                            self.loadMore = false
                        }
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func layOutSearchbar(){
        self.searchBar.placeholder = SearchText.localized
        self.searchBar.delegate = self
    }
    
    private func setUpRightButton() {
        if self.isGroupList {
            let rightButton = UIBarButtonItem(image: UIImage(systemName: "plus"),
                                              style: .plain,
                                              target: self,
                                              action: #selector(self.tapRightBarButton))
            self.navigationItem.rightBarButtonItem = rightButton
        }
    }

    @objc func tapRightBarButton() {
        if let vc = getVC(storyboard: .USER_AND_GROUPS, vcIdentifier: "CreateGroupScene") as? NewGroupVC {
            self.navigateVC(vc)
        }
    }
    
    func groupInfo(with group: GroupModel){
        if let vc = getVC(storyboard: .TABS, vcIdentifier: "GroupInfoScene") as? GroupInfoVC {
            if let groupId = group.id{
                vc.groupById = groupId
                vc.needToReload = { [weak self] need in
                    guard let `self` = self else {return}
                    if need {
                        self.pageNumber = 1
                        self.loadMore = true
                        self.callAPI()
                    }
                }
                self.navigateVC(vc)
            }
        }
    }

    func navigate(thread: ThreadModel){
        if let vc = getVC(storyboard: .TABS, vcIdentifier: "DetailView") as? ThreadDetailVC {
            vc.threadId = thread.id!
            self.navigateVC(vc)
        }
    }
    
    private func showOptionsForOtherGroups(index: IndexPath) {
        let group = self.groupsOther[index.row]
        var alert = AlertModel()
        
        var requestBtn = AlertBtnModel()
        requestBtn.title = group.request_status == GroupStatus.REQUESTED.rawValue ? CancelRequest.localized() : RequestMembership.localized()
        alert.btns.append(requestBtn)
        
        var cancelBtn = AlertBtnModel()
        cancelBtn.title = "Cancel".localized()
        cancelBtn.color = .red
        alert.btns.append(cancelBtn)
        self.showMultiBtnAlert(model: alert) { [weak self] (data) in
            guard let `self` = self else {return}
            if let btn = data as? String {
                if btn == RequestMembership.localized() {
                    self.didTapGroupRequest(index: index, isSendRequest: true)
                }
                else if btn == CancelRequest.localized() {
                    self.didTapGroupRequest(index: index, isSendRequest: false)
                }
            }
        }
    }
    
    private func didTapGroupRequest(index: IndexPath, isSendRequest: Bool) {
        let group = self.groupsOther[index.row]
        if isSendRequest {
            if let _ = group.id {
                RequestManager.shared.joinGroup(id: group.id!) { (status, data) in
                    if status{
                        group.request_status = GroupStatus.REQUESTED.rawValue
                        self.groupsOther[index.row] = group
                        self.showAlert(To: RequestMembership.localized(), for: GroupRequestMessage.localized()) {_ in}
                    }
                }
            }
        }
        else {
            var requestData = MembershipRequest()
            if let _ = group.id {
                requestData.clinic_id = group.id
                requestData.user_id = AppConfig.shared.user.id
                requestData.request_status = GroupStatus.CANCELLED.rawValue
                
                RequestManager.shared.updateGroupStatus(request: requestData) { (status, data) in
                    if status {
                        group.request_status = GroupStatus.OTHER.rawValue
                        self.groupsOther[index.row] = group
                        self.showAlert(To: GroupRequestHeading.localized(), for: CancelGroupRequest.localized()) {_ in}
                    }
                }
            }
        }
    }
}

extension GroupThreadListVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.resignFirstResponder()
            self.searchBar.textField?.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.pageNumber = 1
            self.loadMore = true
            self.searchedText = ""
            self.callAPI()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
        let txt = searchBar.text?.trim ?? ""
        self.searchedText = txt
        self.callAPI()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchBar.resignFirstResponder()
        
    }
}

extension GroupThreadListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.isGroupList {
            if section == 0 {
                return self.groupsAdmin.isEmpty ? 0 : 50
            }
            else if section == 1 {
                return self.groupsMember.isEmpty ? 0 : 50
            }
            else {
                return self.groupsOther.isEmpty ? 0 : 50
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.isGroupList{
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
            headerView.backgroundColor = AppConfig.shared.isDarkMode ? AppColors.darkGrayColor : AppColors.mediumGrayColor
            let label = UILabel()
            label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
            if section == 0 {
                label.text = "Administrator of".localized().uppercased()
            } else if section == 1 {
                label.text = "Member of".localized().uppercased()
            } else {
                label.text = OtherGroupsHeaderText.localized().uppercased()
            }
            
    //        label.font = UIFont().futuraPTMediumFont(16)
            label.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
            headerView.addSubview(label)
            return headerView
        } else {
            return nil
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isGroupList{
            return 3
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isGroupList{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileGroupCell") as? ProfileGroupCell
                else {
                    return self.emptyTblCell()
            }
            cell.selectionStyle = .none
            
            if indexPath.section == 0 {
                if indexPath.row < self.groupsAdmin.count {
                    let group = self.groupsAdmin[indexPath.row]
                    let imgUrl = group.file?.medium_url ?? group.file?.src
                    if let imageUrl = imgUrl {
                        let url = URL(string: imageUrl)
                        cell.groupImageView?.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "groupDefault"))
                    }else{
                        cell.groupImageView?.image = #imageLiteral(resourceName: "groupDefault")
                    }
                    
                    if let groupTitle = group.name {
                        cell.groupTitle?.text = groupTitle
                    }
                    cell.lblUnread.text = "\(group.current_user_notification_count ?? group.clinic_notifications_count ?? group.group_notification_count ?? 0)"
                    cell.viewUnread.isHidden = (group.current_user_notification_count ?? group.clinic_notifications_count ?? group.group_notification_count ?? 0) == 0
                }
            }
            else if indexPath.section == 1 {
                if indexPath.row < self.groupsMember.count {
                    let group = self.groupsMember[indexPath.row]
                    let imgUrl = group.file?.medium_url ?? group.file?.src
                    if let imageUrl = imgUrl {
                        let url = URL(string: imageUrl)
                        cell.groupImageView?.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "groupDefault"))
                    }else{
                        cell.groupImageView?.image = #imageLiteral(resourceName: "groupDefault")
                    }

                    if let groupTitle = group.name {
                        cell.groupTitle?.text = groupTitle
                    }
                    cell.lblUnread.text = "\(group.current_user_notification_count ?? group.clinic_notifications_count ?? group.group_notification_count ?? 0)"
                    cell.viewUnread.isHidden = (group.current_user_notification_count ?? group.clinic_notifications_count ?? group.group_notification_count ?? 0) == 0
                }
            }
            else {
                if indexPath.row < self.groupsOther.count {
                    let group = self.groupsOther[indexPath.row]
                    let imgUrl = group.file?.medium_url ?? group.file?.src
                    if let imageUrl = imgUrl {
                        let url = URL(string: imageUrl)
                        cell.groupImageView?.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "groupDefault"))
                    }else{
                        cell.groupImageView?.image = #imageLiteral(resourceName: "groupDefault")
                    }

                    if let groupTitle = group.name {
                        cell.groupTitle?.text = groupTitle
                    }
                    cell.viewUnread.isHidden = true
                }
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSceneCell") as? MyGroupViewCell
                else {
                return self.emptyTblCell()
            }
            if indexPath.row < self.userThreads.count {
                cell.index = indexPath
                cell.setThreadData(indexPath: indexPath, data: self.userThreads[indexPath.row])
                if self.loadMore && indexPath.row == (self.userThreads.count - 1){
                    self.pageNumber += 1
                    self.getMyThreads()
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isGroupList{
            if section == 0 {
                return self.groupsAdmin.count
            }
            else if section == 1 {
                return self.groupsMember.count
            }
            else {
                return self.groupsOther.count
            }
        }else{
            return self.userThreads.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isGroupList {
            if indexPath.section == 0 {
                self.groupInfo(with: self.groupsAdmin[indexPath.row])
            } else if indexPath.section == 1 {
                self.groupInfo(with: self.groupsMember[indexPath.row])
            } else {
                self.showOptionsForOtherGroups(index: indexPath)
            }
        }else {
            self.navigate(thread: self.userThreads[indexPath.row])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + 1) >= (scrollView.contentSize.height - scrollView.frame.size.height) {
            if self.loadMore && self.isGroupList {
                self.loadMore = false
                self.pageNumber += 1
                self.callAPI()
            }
        }
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: MyGroupViewCell.self), bundle: nil), forCellReuseIdentifier: "ProfileSceneCell")
        self.tableView.register(UINib.init(nibName: String(describing: ProfileGroupCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ProfileGroupCell.self))
        
        self.tableView.separatorStyle = .none
        
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
        if #available(iOS 15.0, *) {
            self.tableView.sectionHeaderTopPadding = 0.0
        }
    }
}
