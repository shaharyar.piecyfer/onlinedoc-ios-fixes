//
//  MemberProfileVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 28/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class MemberProfileVC: BaseVC {
    
    //MARK: - Outlets
    @IBOutlet weak var profileImageView:UIImageView!{
        didSet{
            profileImageView.layer.cornerRadius = profileImageView.bounds.size.height / 2
            profileImageView.clipsToBounds = true
            profileImageView.layer.borderWidth = 0.5
            profileImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var userStatusView: UIView!
    @IBOutlet weak var connectionImgView: UIImageView!
    @IBOutlet weak var ivInvite: UIImageView!
    @IBOutlet weak var ivMessage: UIImageView!
    @IBOutlet weak var sendMessageButtonView: UIView!
    @IBOutlet weak var invitationButtonView: UIView!
    @IBOutlet weak var ConnectionButtonView: UIView!
    @IBOutlet weak var sendMessageLabel: UILabel!
    @IBOutlet weak var inviteToGroupLabel: UILabel!
    @IBOutlet weak var makeConnectionLabel: UILabel!
    @IBOutlet weak var userStatusLabel: UILabel!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var dateOfBirthLabel:UILabel!
    @IBOutlet weak var genderLabel:UILabel!
    @IBOutlet weak var sendRequestButton:UIButton!
    @IBOutlet weak var infoTextView: UITextView!
    @IBOutlet weak var professionLabel: UILabel!
    @IBOutlet weak var weblinksView:UIView!
//    MARK: - Instances
    var memberId = 0
    var friendshipStatus: FriendModel?
    var phoneNumber = ""
    var user = User()
    var onUserUpdate: ((User)-> Void)?
    var fromChat = false
    
    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadProfile()
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: MemberProfileVC.setTheme)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
        applyViewChanges()
        self.infoTextView.linkTextAttributes = [.foregroundColor: UIColor.systemBlue]
        self.infoTextView.isScrollEnabled = false
        self.infoTextView.sizeToFit()
    }
    
    func applyViewChanges() {
        userStatusView.layer.cornerRadius = 25
        userStatusView.clipsToBounds = true
        userStatusView.layer.masksToBounds = false
        userStatusView.layer.borderWidth = 1
        userStatusView.layer.borderColor = UIColor.gray.cgColor
        
        sendMessageButtonView.layer.cornerRadius = 10
        sendMessageButtonView.clipsToBounds = true
        sendMessageButtonView.layer.masksToBounds = false
        
        invitationButtonView.layer.cornerRadius = 10
        invitationButtonView.clipsToBounds = true
        invitationButtonView.layer.masksToBounds = false
        
        ConnectionButtonView.layer.cornerRadius = 10
        ConnectionButtonView.clipsToBounds = true
        ConnectionButtonView.layer.masksToBounds = false
    }
    
    deinit {
        print("MemberProfileVC->deinit")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func loadProfile(){
        RequestManager.shared.otherUserProfile(id: memberId){ (status, response) in
            if status {
                if  let data = response as? MemberProfileResult {
                    if let user = data.user {
                        self.user = user
                        self.layOutViewData(user: user)
                    }
                }
            }
        }
    }
    
    func layOutViewData(user: User){
        if let profileLink = user.file?.src, profileLink != ""{
            let imagePath = URL(string: profileLink)
            self.profileImageView.imageFromServer(imageUrl: imagePath!)
        }else{
            self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let name = user.name {
            self.nameLabel.text = name
        }else{
            self.nameLabel.text = nil
        }
        
        if let profession = user.pre_name {
            self.professionLabel.text = profession.replacingOccurrences(of: "_", with: " ").capitalized.localized()
        }else{
            self.professionLabel.text = nil
        }
        if let dob = user.dob {
            self.dateOfBirthLabel.text = AppConfig.shared.convertToDate(from: dob).formatDate(format: "dd, MMM YYYY")  //day + ", \(month.toName) " + year
        }else{
            self.dateOfBirthLabel.text = nil
        }
        
        if let sex = user.gender, sex != "" {
            self.genderLabel.text = sex.lowercased() == "m" ? MaleGender.localized() : FemaleGender.localized()
        }else{
            self.genderLabel.text = nil
        }
        
        if user.friend_status?.requestStatus == FriendStatus.ACCEPT.rawValue {
            if let phone = user.phone {
                self.phoneNumber = phone
            }
            self.userStatusView.isHidden = false
            self.userStatusLabel.text = connected.localized()
        }else{
            self.userStatusView.isHidden = true
        }
        
        var linksStr = [String]()
        if let webLinks = user.web_links, !webLinks.isEmpty{
            linksStr = self.loadLinks(links: webLinks.compactMap({$0.link}))
        }
        else {
            self.weblinksView.isHidden = true
        }
        if let info = user.bio {
            if (info == "" || info.isEmpty) {
                self.infoTextView.isHidden = true
            }
        }
        var link = ""
        for strItem in linksStr {
            link = link + strItem + "\n"
        }
        self.infoTextView.text = user.bio != nil ? user.bio! + "\n" + link : link
        
        if let friend = user.friend_status {
            self.friendshipStatus = friend
            switch friend.requestStatus {
            case FriendStatus.REQUEST.rawValue:
                self.connectionImgView.image = UIImage(named: "ConnectIcon")
                if AppConfig.shared.user.id == friend.senderId {
                    self.makeConnectionLabel.text = declineText.localized()
                    self.sendRequestButton.tag = 2
                } else {
                    self.makeConnectionLabel.text = acceptRequesText.localized()
                    self.sendRequestButton.tag = 3
                }
                break
            case FriendStatus.ACCEPT.rawValue:
                self.makeConnectionLabel.text = startCall.localized()
                self.connectionImgView.image = #imageLiteral(resourceName: "PhoneIcon")
                self.sendRequestButton.tag = 0
                break
            case FriendStatus.CANCEL.rawValue:
                self.connectionImgView.image = UIImage(named: "ConnectIcon")
                self.makeConnectionLabel.text = connect.localized()
                self.sendRequestButton.tag = 1
                break
            default:
                break
            }
        }
        else {
            self.connectionImgView.image = UIImage(named: "ConnectIcon")
            self.makeConnectionLabel.text = connect.localized()
            self.sendRequestButton.tag = 1
        }
    }
    
    func loadLinks(links: [String]) -> [String]{
        var labelsArray = [UITextView]()
        var linksArr = [String]()
        for item in links {
            labelsArray.append(self.getLinkRow(txt: String(item)))
        }
        
        for item in labelsArray {
            linksArr.append(item.text)
        }
        return linksArr
    }
    
    func getLinkRow(txt:String) -> UITextView {
        let label = UITextView(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        label.linkTextAttributes = [.foregroundColor: UIColor.systemBlue]
        label.textColor = .white
        label.textAlignment = .left
        label.text = txt
        return label
    }
    
//    MARK: - Actions
    @IBAction func invitationTapped(){
        if let vc = getVC(storyboard: .INVITATION, vcIdentifier: String(describing: InviteVC.self)) as? InviteVC {
//            vc.memberEmail = self.user.email!
            vc.invitationType = .member
//            vc.memberName = self.user.name!
            vc.selectedMembers = [self.user]
//            if let img = self.user.file?.image {
//                vc.memberImageString = img
//            }
            vc.isFromMemberProfileVC = true
            self.navigateVC(vc)
        }
    }

    @IBAction func sendRequestTapped(_ sender : UIButton){
        if let friend = self.friendshipStatus {
            if (friend.requestStatus == FriendStatus.ACCEPT.rawValue) {
                guard let number = URL(string: "tel://" + phoneNumber ) else {return}
                UIApplication.shared.open(number)
            }
            else if friend.requestStatus == FriendStatus.REQUEST.rawValue {
                var params = FriendRequestModel()
                params.receiver_id = memberId
                
                if AppConfig.shared.user.id == friend.senderId {
                    params.request_status = FriendStatus.CANCEL
                } else {
                    params.request_status = FriendStatus.ACCEPT
                }
                self.handleFirendRequest(params: params)
            }
            else { //if status cancel, can send request again
                var params = FriendRequestModel()
                params.receiver_id = memberId
                params.request_status = FriendStatus.REQUEST
                self.handleFirendRequest(params: params)
            }
        } else {
            var params = FriendRequestModel()
            params.receiver_id = memberId
            params.request_status = FriendStatus.REQUEST
            self.handleFirendRequest(params: params)
        }
    }
    
    func handleFirendRequest(params: FriendRequestModel){
        self.friendRequest(params: params) { [unowned self] (status,_) in
            if status {
                var friend = FriendModel()
                friend.id = self.user.friend_status?.id
                friend.senderId = AppConfig.shared.user.id
                friend.receiverId = params.receiver_id
                friend.requestStatus = params.request_status.rawValue
                self.user.friend_status = friend
                self.layOutViewData(user: self.user)
                self.onUserUpdate?(self.user)
            }
        }
    }
    
    @IBAction func sendMessageTapped(){
        if fromChat {
            self.dismissVC()
        }
        else {
            if let vc = getVC(storyboard: .CONNECTIOS, vcIdentifier: "ChatScene") as? ChatVC {
                if let roomId = self.user.chatRoomId {
                    vc.chatId = roomId
                }
                else {
                    var receiver = self.user
                    receiver.full_name = (self.user.first_name ?? "") + " " + (self.user.last_name ?? "")
                    vc.receiver = receiver
                }
                self.navigateVC(vc)
            }
        }
    }
}

extension MemberProfileVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        connectionImgView.tintColor = theme.settings.whiteColor
        ivInvite.tintColor = theme.settings.whiteColor
        ivMessage.tintColor = theme.settings.whiteColor
        sendMessageLabel.textColor = theme.settings.whiteColor
        inviteToGroupLabel.textColor = theme.settings.whiteColor
        makeConnectionLabel.textColor = theme.settings.whiteColor
        professionLabel.textColor = theme.settings.subTitleColor
        genderLabel.textColor = theme.settings.subTitleColor
        dateOfBirthLabel.textColor = theme.settings.subTitleColor
        userStatusLabel.textColor = theme.settings.titleColor
        nameLabel.textColor = theme.settings.titleColor
    }
}

struct FriendRequestModel {
    var receiver_id: Int?
    var request_status: FriendStatus = .REQUEST
}
