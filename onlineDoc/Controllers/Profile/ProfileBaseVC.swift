//
//  ProfileBaseVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 24/06/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class ProfileBaseVC: BaseVC {

    var user = User()
    var data = UserThreadAndCategories()
    let socketManager = SocketManager.sharedInstance
    @IBOutlet weak var lblVersion : UILabel!
    @IBOutlet weak var btnLogout : UIButton! {
        didSet {
            let image = UIImage(named: "logOut-1")?.withRenderingMode(.alwaysTemplate).withTintColor(AppColors.mediumGrayColor)
            btnLogout.setImage(image, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadProfile()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if let version = AppConfig.shared.appVersion() {
            lblVersion.text = "v\(version)"
        }
        
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: ProfileBaseVC.setTheme)
        
        let labels = AppConfig.shared.getLabelsInView(view: self.view, tag: 222)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.setTheme)
        }
    }
    
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2
            profileImageView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var imageContainer:UIView!{
        didSet{
            imageContainer.layer.cornerRadius = imageContainer.frame.size.height / 2
            imageContainer.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var profileView : UIView!{
        didSet{
            profileView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.profile)))
        }
    }
    
    @IBOutlet weak var gotoSettingsView: UIView!{
        didSet{
            gotoSettingsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.settings)))
        }
    }
    
    @IBOutlet weak var gotoExplore: UIView!{
        didSet{
            gotoExplore.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onClickExplore)))
        }
    }
    
    @IBOutlet weak var gotoThreadList: UIView!{
        didSet{
            gotoThreadList.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.listOF)))
        }
    }
    
    @IBOutlet weak var gotoNewGroup: UIView!{
        didSet{
            gotoNewGroup.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.createGroup)))
        }
    }
    
    @IBOutlet weak var gotoInvite: UIView!{
        didSet{
            gotoInvite.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.invite)))
        }
    }
    
    @objc func profile(){
        self.performSegue(withIdentifier: "SegueToProfile", sender: nil)
    }
    
    @objc func settings(){
        self.performSegue(withIdentifier: "SegueToSettings", sender: nil)
    }
    
    @objc func listOF(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "SegueToDetail", sender: nil)
    }
    
    @objc func onClickExplore(_ sender:UITapGestureRecognizer){
        if let vc = getVC(storyboard: .USER_AND_GROUPS, vcIdentifier: String(describing: UsersAndGroupsVC.self)) as? UsersAndGroupsVC {
            self.navigateVC(vc)
        }
    }

    @objc func createGroup(){
        if let vc = getVC(storyboard: .USER_AND_GROUPS, vcIdentifier: "CreateGroupScene") as? NewGroupVC {
            self.navigateVC(vc)
        }
    }
    
    @objc func invite(){
        if let vc = getVC(storyboard: .INVITATION, vcIdentifier: String(describing: InviteVC.self)) as? InviteVC {
            vc.invitationType = .all
            self.navigateVC(vc)
        }
    }
    
    @IBAction func logOut(){
        RequestManager.shared.logout() { [weak self] (status, response) in
            guard let `self` = self else {return}
            if status {
                let params: [String : Any] = ["online_status": OnlineMode.OFFLINE.rawValue]
                self.socketManager.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
                
                let defaults = UserDefaults.standard
                var appUsers = defaults.object(forKey: CREDENTIALS_DICT) as? [[String:Any]] ?? [[String:Any]]()
                if let index = (appUsers.firstIndex(where: { $0["userName"] as? String == AppConfig.shared.user.email })){
                    appUsers[index]["name"] = (self.user.first_name ?? "") + " " + (self.user.last_name ?? "")
                    appUsers[index]["image"]  = self.user.file?.src
                    appUsers[index]["chatUnreadCount"] = AppConfig.shared.chatUnreadCount
                    appUsers[index]["threadNotificationUnreadCount"] = AppConfig.shared.threadNotificationUnreadCount
                    UserDefaults.standard.set(appUsers, forKey: CREDENTIALS_DICT)
                }
                AppConfig.shared.logout = ""
                let mainView = self.getVC(storyboard: .MAIN, vcIdentifier: "MainNavView")
                UIApplication.shared.windows.first?.rootViewController = mainView
            }
        }
    }

    func loadProfile(){
        RequestManager.shared.myProfile(){ (status,response) in
            if status {
                if let data = response as? LoginSuccessResponse{
                    self.user = (data.result)!
                    AppConfig.shared.user = self.user
                    self.setupView()
                }
            }else{
            }
        }
    }
    
    private func setupView() {
        let imgUrl = self.user.file?.medium_url ?? self.user.file?.src
        if let url = imgUrl {
            let imageUrl = URL(string: url)
            self.profileImageView.imageFromServer(imageUrl: imageUrl!)
        }else{
            self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        if let firstName = self.user.first_name, let lastName = self.user.last_name{
            self.userNameLabel.text = firstName + " " + lastName
        }else{
            self.userNameLabel.text = nil
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueToProfile"{
            let vc = segue.destination as? ProfileVC
            vc?.user = self.user
            vc?.isUserUpdated = { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.user = AppConfig.shared.user
                strongSelf.setupView()
            }
        }else if segue.identifier == "SegueToSettings"{
            let vc = segue.destination as? AccountSettingVC
            if let inactivity_limit = self.user.inactivity_limit {
                vc?.inactivityTime = inactivity_limit
                vc?.isUserUpdated = { [weak self] in
                    guard let strongSelf = self else { return }
                    strongSelf.user = AppConfig.shared.user
                }
            }
        }else if segue.identifier == "SegueToDetail"{
            let dvc = segue.destination as? GroupThreadListVC
            dvc?.hidesBottomBarWhenPushed = true
            dvc?.isGroupList = false
        }
    }
}

extension ProfileBaseVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        lblVersion.textColor = theme.settings.subTitleColor
    }
}

extension UILabel {
    fileprivate func setTheme(_ theme: MyTheme) {
        self.textColor = theme.settings.subTitleColor
    }
}
