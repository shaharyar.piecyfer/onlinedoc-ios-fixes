//
//  ProfileVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 22/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField

class ProfileVC: BaseVC, UIPopoverPresentationControllerDelegate {
    var selectTag = 0
    var user = User()
    var sceneNeedsReload = true
    var profileUpdatedData = User()
    var datePicker : UIDatePicker!{
        didSet{
            datePicker.datePickerMode = .date
        }
    }
    var newGender = ""
    var CreateOrEditThreadFrom : CreateOrEditThreadFrom = .profile
    var isUserUpdated:(()->())?
    
    let socketManager = SocketManager.sharedInstance
    //MARK: - OUtlets
    @IBOutlet weak var cameraButton: UIButton! {
        didSet{
            cameraButton.backgroundColor = .white
            let image = UIImage(named: "p-cam")?.withRenderingMode(.alwaysTemplate)
            cameraButton.setImage(image, for: .normal)
            cameraButton.layer.cornerRadius = cameraButton.frame.size.height / 2
            cameraButton.layer.borderWidth = 0.5
            cameraButton.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet var profileButtons: [UIButton]!
    @IBOutlet weak var addWebLinkButton:UIButton!
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2
            profileImageView.clipsToBounds = true
            profileImageView.layer.borderWidth = 0.5
            profileImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var professionField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var lastNameWidthCons: NSLayoutConstraint!
    
    @IBOutlet weak var additionalInfoTextView: UITextView!
    @IBOutlet weak var scrollView : UIScrollView!
    
    //MARK: - Components
    @IBOutlet weak var BioInfoComponentView:UIView!
    @IBOutlet weak var ContactInfoComponentView:UIView!
    @IBOutlet weak var WebLinksComponentView:UIView!
    @IBOutlet weak var AdditonalInfoComponentView:UIView!
    
    
    //MARK: - Basic Info Outlets
    @IBOutlet weak var DateOfBirthField: UITextField!
    @IBOutlet weak var genderOptionsView: UIView!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet var genderButtons:[UIButton]!
    
    
    
    //MARK: - Contact Info Outlets
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var adressField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    //MARK: - WebLinks Outlets
    @IBOutlet weak var linksField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: linksField)
        }
    }
    @IBOutlet weak var webLinksTableView:UITableView!
    @IBOutlet weak var tblEmail: UITableView!
    @IBOutlet weak var heightTblEmail: NSLayoutConstraint!
    
    var visibleRecords = 10{
        didSet{
            if visibleRecords != 10{
                
            }
        }
    }
    var webLinks = [WebLinkModel](){
        didSet{
            self.webLinksTableView.reloadData()
        }
    }
    var directUploadInfo: DirectUploadModel?
    var profileImageData: Data?
    
    var emailData = [EmailModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.additionalInfoTextView.text = ProfilePlaceholder.localized()
//        self.tabBarController?.delegate = self
        self.laOutSubView()
        self.makeViewEditable()
        AppConfig.shared.lastVisitedTabIndex = 4
        self.setViewValues()
        self.getWebLinks()
        self.getEmails()
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(title: EditTitle.localized(), style: .plain, target: self, action: #selector(self.editProfile))
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: ProfileVC.setTheme)
    }
    
    @objc func viewTap(){
        self.view.endEditing(true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if sceneNeedsReload || AppConfig.shared.didDeleteSelectedGroup {
            AppConfig.shared.didDeleteSelectedGroup = false
            sceneNeedsReload = !sceneNeedsReload
        }
    }
    
    deinit {
        print("ProfileVC->deinit")
    }
    
    private func getWebLinks(){
        RequestManager.shared.webLinks(){ (status, response) in
            if status{
                if let data = response as? WebLinkResponseModel, let links = data.user_web_link {
                    self.webLinks = links
                }
            }
        }
    }
    
    private func getEmails() {
        self.tblEmail.separatorStyle = .none
        self.heightTblEmail.constant = 100
        RequestManager.shared.getEmails(){ (status, response) in
            if status{
                if let data = response as? EmailResponseModel, let emailData = data.data {
                    self.emailData = emailData
                    self.updateTblEmailHeight()
                    self.tblEmail.reloadData()
                }
            }
        }
    }
    
    private func updateTblEmailHeight() {
        self.heightTblEmail.constant = CGFloat(100 + (50 * self.emailData.count))
    }
    
    private func addSecondryEmail(email: String) {
        guard !email.isEmpty else {
            self.showAlert(To: "", for: "txt_email_required".localized()) {_ in}
            return
        }
        
        guard AppConfig.shared.isValidEmail(testStr: email) else {
            self.showAlert(To: "", for: "txt_email_valid".localized()) {_ in}
            return
        }
        
        RequestManager.shared.addSecondryEmail(email: email) { (status, response) in
            if status{
                self.showAlert(To: "", for: "txt_secondry_email_confirmation".localized()) {_ in}
            }
        }
    }
    
    private func showOptionsForSecondryEmail(index: IndexPath) {
        var alert = AlertModel()
        
        var editBtn = AlertBtnModel()
        editBtn.title = "txt_make_primary_email".localized()
        alert.btns.append(editBtn)
        
        var deleteBtn = AlertBtnModel()
        deleteBtn.title = "Delete".localized()
        alert.btns.append(deleteBtn)
        
        var cancelBtn = AlertBtnModel()
        cancelBtn.title = "Cancel".localized()
        cancelBtn.color = .red
        alert.btns.append(cancelBtn)
        self.showMultiBtnAlert(model: alert) { [weak self] (data) in
            guard let `self` = self else {return}
            if let btn = data as? String {
                if btn == "txt_make_primary_email".localized() {
                    self.makePrimaryEmail(index: index)
                }
                else if btn == "Delete".localized() {
                    self.showAlert(To: "", for: deleteAlertMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
                        guard let `self` = self else {return}
                        if status {
                            self.deleteEmail(index: index)
                        }
                    }
                }
            }
        }
    }
    
    private func makePrimaryEmail(index: IndexPath) {
        guard (index.row-1) < self.emailData.count else {return}
        var emailData = self.emailData[index.row-1]
        guard let email = emailData.email else {return}
        RequestManager.shared.makePrimaryEmail(email: email) { (status, response) in
            if status {
                guard let indexPrimaryEmail = self.emailData.firstIndex(where: {$0.is_primary ?? false}) else {return}
                var primaryEmail = self.emailData[indexPrimaryEmail]
                primaryEmail.is_primary = false
                emailData.is_primary = true
                self.emailData[indexPrimaryEmail] = primaryEmail
                self.emailData[index.row-1] = emailData
                self.tblEmail.reloadData()
                
                self.emailField.text = email
                self.updateEmail(email: email)
                self.showAlert(To: "", for: "txt_email_switched".localized()) {_ in}
            }
        }
    }
    
    private func updateEmail(email: String) {
        let defaults = UserDefaults.standard
        var appUsers = defaults.object(forKey: CREDENTIALS_DICT) as? [[String:Any]] ?? [[String:Any]]()
        if let index = (appUsers.firstIndex(where: { $0["userName"] as? String == AppConfig.shared.user.email })){
            appUsers[index]["userName"] = email
            AppConfig.shared.user.email = email
            UserDefaults.standard.set(appUsers, forKey: CREDENTIALS_DICT)
        }
    }
    
    private func deleteEmail(index: IndexPath) {
        guard (index.row-1) < self.emailData.count else {return}
        guard let id = self.emailData[index.row-1].id else {return}
        RequestManager.shared.deleteSecondryEmail(id: id) { (status, response) in
            if status {
                self.emailData.remove(at: index.row-1)
                self.updateTblEmailHeight()
                self.tblEmail.reloadData()
                self.showAlert(To: "", for: "txt_email_deleted".localized()) {_ in}
            }
        }
    }
    
    func laOutSubView() {
        self.ContactInfoComponentView.isHidden = false
        self.BioInfoComponentView.isHidden = true
        self.WebLinksComponentView.isHidden = true
        self.AdditonalInfoComponentView.isHidden = true
        self.layOutGenderButtons()
    }
    
    func layOutGenderButtons()  {
        self.maleButton.roundCorners(corners: [.topLeft, .bottomLeft], radius: 6.0)
        self.maleButton.backgroundColor = AppColors.setActive
        
        self.femaleButton.roundCorners(corners: [.topRight, .bottomRight], radius: 6.0)
        self.femaleButton.backgroundColor = AppColors.setInActive
    }
    
    @objc func makeViewEditable(_ flag:Bool = false){
        if flag{
            self.lastNameWidthCons.constant = 0.0
        }else{
            self.lastNameWidthCons.constant = -200.0
        }
        self.professionField.isEnabled = flag
        //self.emailField.isEnabled = flag
        self.adressField.isEnabled = flag
        self.phoneField.isEnabled = flag
        self.DateOfBirthField.isEnabled = flag
        self.nameField.isEnabled = flag
        self.lastNameField.isEnabled = flag
        self.lastNameField.isHidden = !flag
        
        self.additionalInfoTextView.isEditable = flag
        self.genderLabel.isHidden = flag
        self.genderOptionsView.isHidden = !flag
        if !flag{
            self.genderLabel.text = self.newGender == "M" ? MaleGender.localized() : FemaleGender.localized()
        }else{
            self.newGender == "M" ? setGenderSelection(maleButton) : setGenderSelection(femaleButton)
        }
        self.cameraButton.isEnabled = true
        self.webLinksTableView.isEditing = true
        self.addWebLinkButton.isEnabled = true
        self.linksField.isEnabled = true
    }
    
    @objc func editProfile(){
        let editButton = UIBarButtonItem(title: SaveTitle.localized(), style: .plain, target: self, action: #selector(UpdateProfileData))
        self.navigationItem.rightBarButtonItem  = editButton
        self.makeViewEditable(true)
        self.nameField.text = self.user.first_name
        self.lastNameField.text = self.user.last_name
        nameField.becomeFirstResponder()
        lastNameField.becomeFirstResponder()
    }
    
    @objc func UpdateProfileData(){
        let saveButton = UIBarButtonItem(title: EditTitle.localized(), style: .plain, target: self, action: #selector(editProfile))
        self.navigationItem.rightBarButtonItem  = saveButton
        self.makeViewEditable(false)
        if let info = directUploadInfo, let data = profileImageData {
            var progressView: ProgressView?
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else {return}
                progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
                progressView!.lblTitle.text = "txt_progress_title".localized()
                progressView!.lblDetail.text = "txt_progress_detail".localized()
                progressView!.uiProgress.progress = 0
                progressView!.tag = 100
                progressView!.lblCount.text = "1/1"
                self.addView(view: progressView!)
            }
            
            RequestManager.shared.directUploadImage(data: data, params: info) { [weak self] (percent) in
                guard let _ = self else {return}
                progressView?.uiProgress.progress = Float(percent)
            } completion: { [weak self] (status, response) in
                guard let `self` = self else {return}
                if let progressView = progressView {
                    self.removeView(tag: progressView.tag)
                }
                if status {
                    self.updateProfile()
                }
                else {
                    self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                }
            }
        } else {
            self.updateProfile()
        }
    }
    
    func showPopOver() {
        if let vc = getVC(storyboard: .MAIN, vcIdentifier: "popOverView") as? UserTypeVC {
            vc.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover: UIPopoverPresentationController = vc.popoverPresentationController!
            popover.delegate = self
            vc.underlyingViewController = self
            if let profession = self.user.pre_name{
                vc.userType = profession.replacingOccurrences(of: "_", with: " ").capitalized.localized()
            }
            vc.onCompletion = { (state, str) in
                if (state) {
                    self.professionField.text = str.replacingOccurrences(of: "_", with: " ").capitalized.localized() // update the Designation for the User
                    self.user = AppConfig.shared.user
                }
            }
            self.presentVC(vc)
        }
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func pickUpDate(_ textField : UITextField){
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
        }
        self.datePicker.calendar = Calendar(identifier: .gregorian)
        self.datePicker.locale = Locale(identifier: "en_US")
        self.datePicker!.datePickerMode = .date
        self.datePicker!.maximumDate = Date()
        
        if let txt = textField.text, txt != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            self.datePicker.setDate(dateFormatter.date(from: txt)!, animated: true)
        }
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .white
        toolBar.sizeToFit()
        
        // Adding Button to the ToolBar
        let cancelButton = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(self.cancelClick))
        cancelButton.setTitleTextAttributes([
            NSAttributedString.Key.font :UIFont.systemFont(ofSize: 19),
            NSAttributedString.Key.foregroundColor : AppColors.destructive,
        ], for: .normal)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Select".localized(), style: .plain, target: self, action: #selector(self.doneClick))
        doneButton.setTitleTextAttributes([
            NSAttributedString.Key.font :UIFont.systemFont(ofSize: 20),
            NSAttributedString.Key.foregroundColor : AppColors.choice,
        ], for: .normal)
        
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        textField.inputAccessoryView = toolBar
        textField.inputView = self.datePicker
    }
    
    @objc func doneClick(sender:UIButton) {
        DateOfBirthField.text = AppConfig.shared.formatPickerDate(date: self.datePicker.date)
        DateOfBirthField.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        DateOfBirthField.resignFirstResponder()
    }
    
    func setButtonSelection(_ sender:UIButton) {
        for button in genderButtons{
            if button == sender{
                button.backgroundColor = AppColors.setActive
                button.alpha = 1
            }else{
                button.backgroundColor = AppColors.setInActive
                button.alpha = 0.5
            }
        }
    }
    
    @IBAction func setGenderSelection(_ sender:UIButton){
        if sender == maleButton{
            self.newGender = "M"
            setButtonSelection(maleButton)
        }else{
            self.newGender = "F"
            setButtonSelection(femaleButton)
        }
    }
    
    @IBAction func gotoSettings(){
        self.performSegue(withIdentifier: "SegueToSettings", sender: self)
    }
    
    @IBAction func showRequestdInfo(_ sender: UIButton){
        for button in profileButtons{
            button.backgroundColor = AppColors.setInActive
        }
        sender.backgroundColor = AppColors.setActive
        self.setupComponents(tag: sender.tag)
    }
    
    @IBAction func logout(fromDeleteProdile: Bool = false){
        let defaults = UserDefaults.standard
        var appUsers = defaults.object(forKey: CREDENTIALS_DICT) as? [[String:Any]] ?? [[String:Any]]()
        
        if fromDeleteProdile {
            let params: [String : Any] = ["online_status": OnlineMode.OFFLINE.rawValue]
            socketManager.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
            
            if let index = (appUsers.firstIndex(where: { $0["userName"] as? String == AppConfig.shared.user.email })) {
                appUsers.remove(at: index)
                defaults.set(appUsers, forKey: CREDENTIALS_DICT)
                defaults.synchronize()
            }
            
            AppConfig.shared.logout = ""
            let mainView = getVC(storyboard: .MAIN, vcIdentifier: "MainNavView")
            UIApplication.shared.windows.first?.rootViewController = mainView
        } else {
            RequestManager.shared.logout() { [self] (status, response) in
                if status {
                    let params: [String : Any] = ["online_status": OnlineMode.OFFLINE.rawValue]
                    socketManager.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
                    
                    if let index = (appUsers.firstIndex(where: { $0["userName"] as? String == AppConfig.shared.user.email })){
                        appUsers[index]["name"] = (user.first_name ?? "") + " " + (user.last_name ?? "")
                        appUsers[index]["image"]  = user.file?.medium_url ?? user.file?.src
                        appUsers[index]["chatUnreadCount"] = AppConfig.shared.chatUnreadCount
                        appUsers[index]["threadNotificationUnreadCount"] = AppConfig.shared.threadNotificationUnreadCount
                        UserDefaults.standard.set(appUsers, forKey: CREDENTIALS_DICT)
                    }
                    AppConfig.shared.logout = ""
                    let mainView = getVC(storyboard: .MAIN, vcIdentifier: "MainNavView")
                    UIApplication.shared.windows.first?.rootViewController = mainView
                }
            }
        }
    }
    
    @IBAction func addNewWebLink(){
        if let newLink = linksField.text{
            if newLink.isValidUrl(), !self.webLinks.contains(where: { (item) -> Bool in
                return item.link == newLink
            }) {
                self.addNewLink(link: newLink)
            } else {
                self.showAlert(To: "Invalid Url", for: "Please enter a valid url.") { [weak self] (status) in
                    guard let `self` = self else {return}
                    self.linksField.text = ""
                }
            }
        }
    }
    
    private func addNewLink(link: String) {
        RequestManager.shared.webLinks(link: link){ (status, response) in
            if status{
                if let _ = response as? GeneralSuccessResult  {
                    self.linksField.text = ""
                    self.getWebLinks()
                }
            }else{
            }
        }
    }
    
    private func deleteWebLink(index: Int) {
        RequestManager.shared.deleteWebLinks(id: self.webLinks[index].id!){ (status, response) in
            if status{
                if let _ = response as? GeneralSuccessResult  {
                    self.webLinks.remove(at: index)
                }
            }else{
            }
        }
    }
    
    @IBAction func changeProfilePicture(){
        self.showDialogAlert(options: .camera, .library, singleSelection: true)
    }
    
    @IBAction func deleteProfile(){
        self.showOptions()
//        self.showAlert(To: "Delete Profile".localized(), for: "Are you sure to delete?".localized()) { [weak self] (status) in
//        guard let `self` = self else {return}
//            if status {
//                self.showOptions()
//            }
//        }
    }
    
    func showOptions(){
        UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).effect = UIBlurEffect(style: .dark)
        
        let imagePicker = UIImagePickerController()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let firstSubview = alert.view.subviews.first, let alertContentView = firstSubview.subviews.first {
            for view in alertContentView.subviews {
                view.backgroundColor = AppColors.destructive
            }
        }
        alert.modalPresentationStyle = .popover
        alert.view.tintColor = .white
        imagePicker.navigationController?.navigationBar.tintColor = AppColors.destructive
        
        let camera = UIAlertAction(title: NSLocalizedString("Disable".localized(), comment: ""), style: .default) { action in
            alert.dismiss(animated: true) { [weak self] in
                guard let `self` = self else {return}
                self.deleteMyProfile(isDelete: false)
            }
            
//            self.showAlert(To: "Disable Profile".localized(), for: "Are you sure you want to disable?".localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
//                guard let `self` = self else {return}
//                if status {
//                    self.deleteMyProfile(isDelete: false)
//                }
//            }
        }
        
        let photos = UIAlertAction(title: NSLocalizedString("Delete".localized(), comment: ""), style: .default)   { action in
            alert.dismiss(animated: true) { [weak self] in
                guard let `self` = self else {return}
                self.deleteMyProfile(isDelete: true)
            }
//            self.showAlert(To: "Delete Profile".localized(), for: "Are you sure to delete?".localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
//                guard let `self` = self else {return}
//                if status {
//                    self.deleteMyProfile(isDelete: true)
//                }
//            }
        }
        
        alert.addAction(camera)
        alert.addAction(photos)
        
        alert.addAction(UIAlertAction(title: "", style: .default, handler: nil))
        
        let cancelButtonViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CancelButtonViewController")
        let cancel = UIAlertAction(title: "", style: .cancel, handler: nil)
        cancel.setValue(cancelButtonViewController, forKey: "contentViewController")
        alert.addAction(cancel)
        
        if let presenter = alert.popoverPresentationController
        {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        present(alert, animated: true, completion: nil)
    }
    
    func deleteMyProfile(isDelete:Bool){
        self.showAlert(To: isDelete ? "Delete Profile".localized() : "Disable Profile".localized(), for: "", tfPlaceholder: "Enter Password".localized(), isSecure: true, buttons: 2,
                       firstBtnStyle: .default, secondBtnStyle: .cancel, firstBtnTitle: OKText.localized(), secondBtnTitle: declineText.localized()) { [weak self] (status, text) in
            guard let `self` = self else {return}
            if status {
                if (text == nil || text!.isEmpty) {
                    self.showAlert(To: "", for: "Please enter password.".localized()) {_ in }
                }
                else {
                    if isDelete{
                        RequestManager.shared.deleteMyProfile(isDelete: isDelete, password: text!) { (status, data) in
                            if status{
                                self.deleteFromLoggedInUserList()
                            }
                        }
                    }else{
                        RequestManager.shared.deleteMyProfile(isDelete: isDelete, password: text!) { (status, data) in
                            if status{
                                self.deleteFromLoggedInUserList()
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func deleteFromLoggedInUserList(){
        self.logout(fromDeleteProdile: true)
    }
    
    func setViewValues() {
        
        if let url = self.user.file?.src {
            let imageUrl = URL(string: url)
            self.profileImageView.imageFromServer(imageUrl: imageUrl!)
        }else{
            self.profileImageView.image = #imageLiteral(resourceName: "ic_user")
        }
        
        
        if let firstName = self.user.first_name, let lastName = self.user.last_name{
            self.nameField.text = firstName + " " + lastName
        }else{
            self.nameField.text = nil
        }
        
        if let profession = self.user.pre_name{
            self.professionField.text = profession.replacingOccurrences(of: "_", with: " ").capitalized.localized()
        }else{
            self.professionField.text = nil
        }
        
        if let phone = self.user.phone{
            self.phoneField.text = phone
        }else{
            self.phoneField.text = nil
        }
        
        if let email = self.user.email{
            self.emailField.text = email
        }else{
            self.emailField.text = nil
        }
        
        if let adress = self.user.address{
            self.adressField.text = adress
        }else{
            self.adressField.text = nil
        }
        
        if let dob = self.user.dob{
            
            self.DateOfBirthField.text = dob.formattedDate
        }
        
        if let gender = self.user.gender, gender != "" {
            self.newGender = gender
            self.genderLabel.text = gender.lowercased() == "m" ?  MaleGender.localized() : FemaleGender.localized()
        }else{
            self.genderLabel.text = nil
        }
        if let bio = self.user.bio{
            if bio != ""{
                self.additionalInfoTextView.text = bio
                self.additionalInfoTextView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
            }else{
                self.additionalInfoTextView.text = ProfilePlaceholder.localized()
                self.additionalInfoTextView.textColor = .lightGray
            }
        }
    }
    
    func setupComponents(tag:Int) {
        self.tblEmail.isHidden = true
        self.heightTblEmail.constant = CGFloat(0)
        switch tag {
        case 0:
            self.tblEmail.isHidden = false
            self.updateTblEmailHeight()
            self.ContactInfoComponentView.isHidden = false
            self.BioInfoComponentView.isHidden = true
            self.WebLinksComponentView.isHidden = true
            self.AdditonalInfoComponentView.isHidden = true
        case 1:
            self.ContactInfoComponentView.isHidden = true
            self.BioInfoComponentView.isHidden = false
            self.WebLinksComponentView.isHidden = true
            self.AdditonalInfoComponentView.isHidden = true
        case 2:
            self.ContactInfoComponentView.isHidden = true
            self.BioInfoComponentView.isHidden = true
            self.WebLinksComponentView.isHidden = false
            self.AdditonalInfoComponentView.isHidden = true
        case 3:
            self.ContactInfoComponentView.isHidden = true
            self.BioInfoComponentView.isHidden = true
            self.WebLinksComponentView.isHidden = true
            self.AdditonalInfoComponentView.isHidden = false
        default:
            break
        }
    }
    
    func updateProfile(){
        if let firstName = nameField.text, let lastName = lastNameField.text {
            self.profileUpdatedData.first_name = firstName
            self.profileUpdatedData.last_name = lastName
            self.profileUpdatedData.name = firstName + " " + lastName
        }
        
        self.nameField.text = self.profileUpdatedData.name
        
        if let bio = self.additionalInfoTextView.text{
            if bio == ProfilePlaceholder.localized(){
                self.profileUpdatedData.bio = nil
            }else{
                self.profileUpdatedData.bio = bio
                self.additionalInfoTextView.text = bio != "" ? bio : ProfilePlaceholder.localized()
                self.additionalInfoTextView.textColor = .lightGray
            }
        }else{
            self.profileUpdatedData.bio = nil
        }
        
        if (professionField.text == "Læge") {
            self.profileUpdatedData.pre_name = "Doctor" // update the Designation for the User as on Servers only English words are considered
        }
        else if (professionField.text == "Personlig sundhedspleje"){
            self.profileUpdatedData.pre_name = "Health Care Personal" // update the Designation for the User as on Servers only English words are considered
        }
        else if (professionField.text == "Standard Bruger"){
            self.profileUpdatedData.pre_name = "Standard User" // update the Designation for the User as on Servers only English words are considered
        }
        else {
            self.profileUpdatedData.pre_name = professionField.text
        }
        self.profileUpdatedData.phone = phoneField.text
        self.profileUpdatedData.address = adressField.text
        self.profileUpdatedData.email = emailField.text
        if let dob = DateOfBirthField.text, !dob.isEmpty {
            self.profileUpdatedData.dob = dob.dbFormat
        }
        
        if  self.newGender != ""{
            self.profileUpdatedData.gender = self.newGender
        }
        
        var file: String?
        if let info = directUploadInfo {
            file = info.signed_id!
        }
        
        RequestManager.shared.updateUser(user: self.profileUpdatedData, file: file){ (status, response) in
            if status{
                self.directUploadInfo = nil
                if let data = response as? LoginSuccessResponse, data.result != nil {
                    self.user = data.result!
                    AppConfig.shared.user = self.user
                    self.isUserUpdated?()
                    self.showAlert(To: UpdateProfileTitle.localized(), for: UpdateProfileMessage.localized()) {_ in}
                }
            }
        }
    }
    
    private func updateProfileImage(){
        var file = ""
        if let info = directUploadInfo {
            file = info.signed_id!
        }
        guard !file.isEmpty else {return}
        RequestManager.shared.updateUserProfile(file: file){ (status, response) in
            if status{
                self.directUploadInfo = nil
                if let data = response as? LoginSuccessResponse, data.result != nil {
                    self.user = data.result!
                    AppConfig.shared.user = self.user
                    self.isUserUpdated?()
                    self.showAlert(To: "", for: UpdateProfileImageMessage.localized()) {_ in}
                }
            }
        }
    }
    
    @IBAction func showListOf(_ sender: UIButton){
        self.selectTag = sender.tag
        self.performSegue(withIdentifier: "SegueToDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SegueToSettings"{
            let dvc = segue.destination as? AccountSettingVC
            if let time = self.user.inactivity_limit{
                dvc?.inactivityTime = time
            }
        }
    }
}

extension ProfileVC: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == DateOfBirthField{
            pickUpDate(DateOfBirthField)
        }
        else if textField == professionField  {
            nameField.resignFirstResponder()
            professionField.resignFirstResponder()
            dismissKeyboard()
            showPopOver()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == professionField{
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == nameField || textField == lastNameField) {
            let maxLength = 15
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else {
            return true
        }
    }
}

extension ProfileVC {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        var capturedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        capturedImage = self.imageOrientation(capturedImage)
        self.profileImageView.image = capturedImage
        
        var fileName = UUID().uuidString + ".jpeg"
        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
            fileName = url.lastPathComponent
            //          fileType = url.pathExtension
        }
        
        profileImageData = capturedImage.jpegData(compressionQuality: capturedImage.size.width > 500 ? 0.5 : 1.0) ?? Data(capturedImage.sd_imageData()!)
        let imageSize: Int = profileImageData!.count
        
        var directUploadParams = DirectUploadParams()
        directUploadParams.filename = fileName
        directUploadParams.content_type = profileImageData!.fileExtension
        directUploadParams.byte_size = imageSize
        
        //        print("ProfileVC->directUploadParams", directUploadParams)
        RequestManager.shared.directUpload(_params: directUploadParams, data: profileImageData!) { [weak self] (status, result) in
            guard let `self` = self else {return}
            if status {
                if let data = result as? DirectUploadModel {
                    self.directUploadInfo = data
                    if let info = self.directUploadInfo, let data = self.profileImageData {
                        var progressView: ProgressView?
                        DispatchQueue.main.async { [weak self] in
                            guard let `self` = self else {return}
                            progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
                            progressView!.lblTitle.text = "txt_progress_title".localized()
                            progressView!.lblDetail.text = "txt_progress_detail".localized()
                            progressView!.uiProgress.progress = 0
                            progressView!.tag = 100
                            progressView!.lblCount.text = "1/1"
                            self.addView(view: progressView!)
                        }
                        
                        RequestManager.shared.directUploadImage(data: data, params: info) { [weak self] (percent) in
                            guard let _ = self else {return}
                            progressView?.uiProgress.progress = Float(percent)
                        } completion: { [weak self] (status, response) in
                            guard let `self` = self else {return}
                            if let progressView = progressView {
                                self.removeView(tag: progressView.tag)
                            }
                            if status {
                                self.updateProfileImage()
                            }
                            else {
                                self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                            }
                        }
                    }
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (t) in
            self.sceneNeedsReload = true
        }
    }
}

//MARK : - UITextViewDelegate

extension ProfileVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text != "" && textView.text == ProfilePlaceholder.localized(){
            textView.text = ""
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
        }else{
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text != "" && (textView.text == ProfilePlaceholder.localized() || textView.text == ""){
            textView.text = ""
            textView.textColor = .lightGray
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
}

//MARK: - TABLEVIEW DELEGATES

extension ProfileVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == webLinksTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WebLinksTableViewCell") as! WebLinksTableViewCell
            cell.selectionStyle = .none
//            cell.backgroundColor = AppColors.tabBarTint
//            cell.backgroundView?.backgroundColor = AppColors.tabBarTint
//            cell.contentView.backgroundColor = AppColors.tabBarTint
            cell.linksTextView?.text = webLinks[indexPath.row].link
            cell.linksTextView.textColor = .systemBlue
            return cell
        }
        else if tableView == tblEmail {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddEmailTableViewCell") as! AddEmailTableViewCell
                cell.onClickAddBtn = { [weak self] email in
                    guard let `self` = self else {return}
                    self.addSecondryEmail(email: email)
                }
                cell.selectionStyle = .none
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SecondryEmailCell") as! SecondryEmailCell
                if (indexPath.row - 1) < self.emailData.count {
                    cell.indexPath = indexPath
                    cell.configCell(data: self.emailData[indexPath.row - 1])
                    cell.onClickMoreOptions = { [weak self] _index in
                        guard let _index = _index else {return}
                        guard let `self` = self else {return}
                        self.showOptionsForSecondryEmail(index: _index)
                    }
                }
                cell.selectionStyle = .none
                return cell
            }
        }
        else {
            preconditionFailure()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == webLinksTableView{
            if self.webLinks.count == 0{
//                webLinksTableView.backgroundView?.backgroundColor = .black
                return 0
            }else{
                return 1
            }
        }
        else if tableView == tblEmail {
            return 1
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == webLinksTableView{
            return self.webLinks.count
        }else if tableView == tblEmail {
            return self.emailData.count + 1 //1 for add an email
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if tableView == webLinksTableView{
            return true
        }else{
            return false
        }
    }
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete".localized()) { (rowAction, indexPath) in
//            if tableView == self.webLinksTableView {
//                self.deleteWebLink(index: indexPath.row)
//            }
//
//        }
//        deleteAction.backgroundColor = .red
//        return [deleteAction]
//    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: DeleteTitle.localized()) { _, _, _ in
            if tableView == self.webLinksTableView {
                self.deleteWebLink(index: indexPath.row)
            }
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
}

//extension ProfileVC : UITabBarControllerDelegate{
//    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//        let tabBarIndex = tabBarController.selectedIndex
//        AppConfig.shared.lastVisitedTabIndex = tabBarIndex
//        
//        if tabBarIndex == 3{
//            self.scrollView.scrollTo(direction: .top)
//        }
//    }
//}

extension ProfileVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        nameField.textColor = theme.settings.titleColor
        lastNameField.textColor = theme.settings.titleColor
        professionField.textColor = theme.settings.subTitleColor
        cameraButton.tintColor = theme.settings.titleColor
        DateOfBirthField.textColor = theme.settings.titleColor
        phoneField.textColor = theme.settings.titleColor
        adressField.textColor = theme.settings.titleColor
        emailField.textColor = theme.settings.titleColor
        linksField.textColor = theme.settings.titleColor
        additionalInfoTextView.textColor = theme.settings.titleColor
    }
}

//extension Notification.Name {
//    static let professionFieldNotificationName = Notification.init(name: Notification.Name(rawValue: updateProfessionField))
//}

struct UserThreadAndCategories {
    var groups: [MyGroups]!
    var totalThreads: [Threads]!
    var threads: [Threads]!
    var isGSVisible = true
    var isTSVisible = true
}
