//
//  SecondryEmailCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 19/01/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit

class SecondryEmailCell: UITableViewCell {
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var uiOptions: UIView!
    @IBOutlet weak var btnOptions: UIButton!
    
    var indexPath: IndexPath?
    var onClickMoreOptions: ((IndexPath?)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnOptions.setTitle("", for: .normal)
        //Generic theme
        Themer.shared.register(
            target: self,
            action: SecondryEmailCell.applyTheme)
    }
    
    @IBAction func onClickAdd(_ sender: UIButton) {
        self.onClickMoreOptions?(indexPath)
    }

    func configCell(data: EmailModel) {
        self.uiOptions.isHidden = false
        if data.is_primary ?? false {
            self.uiOptions.isHidden = true
        }
        self.lblEmail.text = data.email
    }
}
