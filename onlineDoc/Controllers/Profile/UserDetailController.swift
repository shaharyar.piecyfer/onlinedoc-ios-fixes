//
//  GroupThreadListVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 10/06/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class GroupThreadListVC: BaseVC {
    
    var userGroups = [GroupModel]()
    var userGroupsSource = [GroupModel]()
    
//    var groupsMember = [MyGroups]()
//    var groupsMemberSource = [MyGroups]()
//    var groupsAdmin = [MyGroups]()
//    var groupsAdminSource = [MyGroups]()
    
    var userThreads = [Threads]()
    var userThreadsSource = [Threads]()
    
    var isGroupList = false
    var editThreadFrom : EditThreadFrom = .profile
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.layOutSearchbar()
        self.callAPI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func callAPI(){
        if isGroupList {
            RequestManager.shared.getVisitedGroups() { (status, response) in
                if status {
                    if let data = response as? GroupListModel {
                        if let userGroups = data.clinics {
                            self.userGroupsSource = userGroups
                            self.userGroups = userGroups
                            self.sortData()
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    func sortData(){
        self.groupsAdmin.removeAll()
        self.groupsMember.removeAll()
        self.groupsAdmin = self.userGroups.filter({ (group) -> Bool in
            if group.clinicAdmins != nil {
                let array = group.clinicAdmins!.components(separatedBy: ",")
                if array.contains(String(AppConfig.shared.user.id!)) {
                    return true
                }
            }
            self.groupsMember.append(group)
            return false
        })
        self.groupsAdminSource = self.groupsAdmin
        self.groupsMemberSource = self.groupsMember
        
        self.tableView.reloadData()
    }
    
    func layOutSearchbar(){
        
        self.searchBar.placeholder = SearchText.localized
        self.searchBar.delegate = self
        self.searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
        if let searchTextField = self.searchBar.textField{
            //               searchTextField.borderStyle = .none
            //               searchTextField.setBottomBorder()
            searchTextField.textColor = .white
            searchTextField.keyboardAppearance = .dark
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func groupInfo(with group:MyGroups){
        if #available(iOS 13.0, *) {
            guard let groupInfoCtrlr = UIStoryboard(name: "Tabs", bundle: nil).instantiateViewController(identifier: "GroupInfoScene") as? GroupInfoVC else{ print("Unable to get the controller instance"); return}
            if let groupId = group.id{
                groupInfoCtrlr.groupById = groupId
            }
            self.navigationController?.pushViewController(groupInfoCtrlr, animated: true)
        } else {
            guard let groupInfoCtrlr = UIStoryboard(name: "Tabs", bundle: nil).instantiateViewController(withIdentifier: "GroupInfoScene") as? GroupInfoVC else{ print("Unable to get the controller instance"); return}
            if let groupId = group.id{
                groupInfoCtrlr.groupById = groupId
            }
            self.navigationController?.pushViewController(groupInfoCtrlr, animated: true)
        }
        
    }
    
    func threadInfo(with thread : Threads){
        var threadDetail = ThreadToEdit()
        if let id = thread.threadId{
            RequestManager.shared.thread(id: id){ (status, response) in
                if status{
                    if let data = response as? ThreadResponse{
                        if let threadData = data.result?.thread{
                            threadDetail = threadData
                            var threadInfo = TimeLineData()
                            threadInfo.clinicName = thread.clinicName
                            threadInfo.folderName = thread.folderName
                            threadInfo.clinicId = thread.clinicId
                            threadInfo.threadId = threadDetail.zValue?.threadId
                            threadInfo.adminsArray = threadDetail.adminsArray
                            threadInfo.folder = threadDetail.zValue?.folder
                            threadInfo.profilePicture = threadDetail.zValue?.profilePicture
                            threadInfo.fullName = threadDetail.zValue?.name.capitalized
                            threadInfo.fireBaseId = threadDetail.commentFirebaseId
                            threadInfo.journal = thread.journal
                            threadInfo.notification = threadDetail.zValue?.notification
                            threadInfo.title = threadDetail.zValue?.title
                            threadInfo.adminsArray = threadDetail.adminsArray
                            threadInfo.userId = threadDetail.zValue?.userId
                            if let firebaseId = threadDetail.commentFirebaseId{
                                FirebaseManager.shared.getThreadComments(id: firebaseId)
                            }
                            if threadInfo.createTime == nil{
                                threadInfo.createTime = CreateTime()
                            }
                            threadInfo.createTime?.date = threadDetail.zValue?.addedDate?.date
                            threadInfo.createTime?.timezone_type = threadDetail.zValue?.addedDate?.timezone_type
                            threadInfo.createTime?.timezone = threadDetail.zValue?.addedDate?.timezone
                            self.navigate(threadInfo)
                        }
                    }
                }
            }
        }
    }
    
    func navigate(_ data:TimeLineData){
        if #available(iOS 13.0, *){
            if let controller = UIStoryboard(name: "Tabs", bundle: nil).instantiateViewController(identifier: "DetailView") as? DetailsViewController{
                controller.threadData = data
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }else{
            if let controller = UIStoryboard(name: "Tabs", bundle: nil).instantiateViewController(withIdentifier: "DetailView") as? DetailsViewController{
                self.navigationController?.pushViewController(controller, animated: true)
                controller.threadData = data
            }
        }
    }
    
    /*{
     if #available(iOS 13.0, *) {
     if let controller = UIStoryboard(name: "CreateThread", bundle: nil).instantiateViewController(identifier: "CreateThreadScene") as? CreateThreadViewController{
     
     if let groupId = thread.clinicId{
     controller.groupId = groupId
     }
     
     if let group = thread.clinicName
     {
     controller.groupTitle = group
     }
     
     if let categoryId = thread.id{
     controller.categoryId = categoryId
     }
     if let category = thread.folderName{
     controller.CategoryTitle = category
     }
     
     if let threadId = thread.threadId{
     controller.threadId = threadId
     }
     controller.formMode = .edit
     controller.editThreadFrom = self.editThreadFrom
     self.navigationController?.pushViewController(controller, animated: true)
     }
     } else {
     if let controller = UIStoryboard(name: "CreateThread", bundle: nil).instantiateViewController(withIdentifier: "CreateThreadScene") as? CreateThreadViewController{
     
     if let groupId = thread.clinicId{
     controller.groupId = groupId
     }
     
     if let group = thread.clinicName
     {
     controller.groupTitle = group
     }
     
     if let categoryId = thread.id{
     controller.categoryId = categoryId
     }
     if let category = thread.folderName{
     controller.CategoryTitle = category
     }
     
     if let threadId = thread.threadId{
     controller.threadId = threadId
     }
     controller.formMode = .edit
     self.navigationController?.pushViewController(controller, animated: true)
     }
     }
     }*/
    
    private func getUnreadCount(){
        for group in self.userGroupsSource {
            for firebaseId in group.threadIds! {
                FirebaseManager.shared.getGroupUnreadCount(firebaseId, userId: AppConfig.shared.user.id != nil ? String(AppConfig.shared.user.id!) : "") { (status, threadId, unreadCount) in
                    if status {
                        group.unreadCount += unreadCount
                        self.userGroupsTableView.reloadData()
                    }
                }
            }
        }
    }
}

extension GroupThreadListVC:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            if self.isGroupList{
//                self.userGroups = self.userGroupsSource
                self.groupsMember = self.groupsMemberSource
                self.groupsAdmin = self.groupsAdminSource
                self.tableView.reloadData()
            }else{
                self.userThreads = self.userThreadsSource
                self.tableView.reloadData()
            }
            return
        }
        if self.isGroupList{
//            self.userGroups = self.userGroupsSource.filter{ ($0.clinicName != " " ? ($0.clinicName)!.lowercased().contains(searchText.lowercased()) : false)}
            self.groupsMember = self.groupsMemberSource.filter{ ($0.clinicName != " " ? ($0.clinicName)!.lowercased().contains(searchText.lowercased()) : false)}
            self.groupsAdmin = self.groupsAdminSource.filter{ ($0.clinicName != " " ? ($0.clinicName)!.lowercased().contains(searchText.lowercased()) : false)}
            self.tableView.reloadData()
        }else{
            self.userThreads = self.userThreadsSource.filter{ (($0.title != nil && $0.title != " ") ? ($0.title)!.lowercased().contains(searchText.lowercased()) : false)}
            self.tableView.reloadData()
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if self.isGroupList{
//            self.userGroups = self.userGroupsSource
//            self.groupsMember = self.groupsMemberSource
//            self.groupsAdmin = self.groupsAdminSource
//            self.tableView.reloadData()
        }else{
//            self.userThreads = self.userThreadsSource
//            self.tableView.reloadData()
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
}

extension GroupThreadListVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.isGroupList{
            return 50
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.isGroupList{
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
            headerView.backgroundColor = ColorPalette.light
            let label = UILabel()
            label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
            if section == 0 {
                label.text = !self.groupsAdmin.isEmpty ? "Administrator of".localized().uppercased() : "Member of".localized().uppercased()
            } else {
                label.text = "Member of".localized().uppercased()
            }
    //        label.font = UIFont().futuraPTMediumFont(16)
            label.textColor = UIColor.white
            headerView.addSubview(label)
            return headerView
        } else {
            return nil
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isGroupList{
            if self.groupsAdmin.isEmpty && self.groupsMember.isEmpty {
                return 0
            } else if self.groupsAdmin.isEmpty || self.groupsMember.isEmpty {
                return 1
            }
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isGroupList{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileGroupCell") as? ProfileGroupCell
                else {
                    return UITableViewCell()
            }
            cell.selectionStyle = .none
            
            if indexPath.section == 0 {
                let group = self.groupsAdmin.isEmpty ? self.groupsMember[indexPath.row] : self.groupsAdmin[indexPath.row]
                if let imageUrl = group.groupImage{
                    let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(imageUrl)
                    cell.groupImageView?.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "GroupsActive"))
                }else{
                    cell.groupImageView?.image = #imageLiteral(resourceName: "GrupperActive")
                }

                if let groupTitle = group.clinicName?.capitalized{
                    cell.groupTitle?.text = groupTitle
                }
                cell.lblUnread.text = "\(group.unreadCount)"
                cell.viewUnread.isHidden = group.unreadCount == 0
            }
            else {
                let group = self.groupsMember[indexPath.row]
                if let imageUrl = group.groupImage{
                    let url = URL(string: ApiConfig.shared.imageStagingUrl)?.appendingPathComponent(imageUrl)
                    cell.groupImageView?.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "GroupsActive"))
                }else{
                    cell.groupImageView?.image = #imageLiteral(resourceName: "GrupperActive")
                }

                if let groupTitle = group.clinicName?.capitalized{
                    cell.groupTitle?.text = groupTitle
                }
                cell.lblUnread.text = "\(group.unreadCount)"
                cell.viewUnread.isHidden = group.unreadCount == 0
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSceneCell") as? MyGroupViewCell
                else {
                    return UITableViewCell()
            }
            cell.setThreadData(indexPath: indexPath, data: self.userThreads[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isGroupList{
            if section == 0 {
                return self.groupsAdmin.isEmpty ? self.groupsMember.count : self.groupsAdmin.count
            }
            else {
                return self.groupsMember.count
            }
        }else{
            return self.userThreads.count
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isGroupList{
            if indexPath.section == 0 {
                let group = self.groupsAdmin.isEmpty ? self.groupsMember[indexPath.row] : self.groupsAdmin[indexPath.row]
                self.groupInfo(with: group)
            } else {
                self.groupInfo(with: self.groupsMember[indexPath.row])
            }
            if !(self.searchBar.text?.isEmpty ?? false) {
                self.searchBar.text = ""
                self.groupsMember = self.groupsMemberSource
                self.groupsAdmin = self.groupsAdminSource
                self.tableView.reloadData()
            }
        }else{
            self.threadInfo(with: self.userThreads[indexPath.row])
        }
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: "MyGroupViewCell", bundle: nil), forCellReuseIdentifier: "ProfileSceneCell")
        self.tableView.register(UINib.init(nibName: "ProfileGroupCell", bundle: nil), forCellReuseIdentifier: "ProfileSceneCell")
        
        self.tableView.separatorStyle = .none
    }
}
