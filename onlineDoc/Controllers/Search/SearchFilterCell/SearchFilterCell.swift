//
//  SearchFilterCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 02/02/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit

class SearchFilterCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var uiMain: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Themer.shared.register(
            target: self,
            action: SearchFilterCell.setTheme)
    }
}

extension SearchFilterCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        lblTitle.textColor = theme.settings.titleColor
    }
}
