//
//  SearchGroupVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 02/02/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit

class SearchGroupVC: BaseVC {
    //MARK: - IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    
    var searchedText = ""
    var loadMore = true
    var pageNumber = 1
    var groupsData = [GroupModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.setupTheme()
        
        self.searchGroups()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: SearchGroupVC.applyTheme)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
    }
    
    deinit {
        print("SearchGroupVC->deinit")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func searchGroups() {
        if self.pageNumber == 1 {
            self.groupsData.removeAll()
            self.tableView.reloadData()
        }
        self.tableView.setEmptyMessage("")
        var searchParams = TimelineParams()
        searchParams.search = self.searchedText
        searchParams.page = self.pageNumber
        searchParams.limit = pageLimit10
        searchParams.type = SearchFilterTypes.CLINIC.rawValue
        RequestManager.shared.searchTimeline(params: searchParams) { (status, response) in
            if status {
                if let response = response as? SearchTimeineResult {
                    if let data = response.groups, data.isEmpty, self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFoundHomeSearch.localized())
                        self.tableView.reloadData()
                        return
                    }
                    if (response.groups?.count ?? 0) == 0 {
                        self.loadMore = false
                    }
                    self.groupsData.append(contentsOf: response.groups ?? [])
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func showOptionsForOtherGroups(index: IndexPath) {
        let group = self.groupsData[index.row]
        var alert = AlertModel()
        
        var requestBtn = AlertBtnModel()
        requestBtn.title = group.request_status == GroupStatus.REQUESTED.rawValue ? CancelRequest.localized() : RequestMembership.localized()
        alert.btns.append(requestBtn)
        
        var cancelBtn = AlertBtnModel()
        cancelBtn.title = "Cancel".localized()
        cancelBtn.color = .red
        alert.btns.append(cancelBtn)
        self.showMultiBtnAlert(model: alert) { [weak self] (data) in
            guard let `self` = self else {return}
            if let btn = data as? String {
                if btn == RequestMembership.localized() {
                    self.didTapGroupRequest(index: index, isSendRequest: true)
                }
                else if btn == CancelRequest.localized() {
                    self.didTapGroupRequest(index: index, isSendRequest: false)
                }
            }
        }
    }
    
    private func didTapGroupRequest(index: IndexPath, isSendRequest: Bool) {
        let group = self.groupsData[index.row]
        if isSendRequest {
            if let _ = group.id {
                RequestManager.shared.joinGroup(id: group.id!) { (status, data) in
                    if status{
                        group.request_status = GroupStatus.REQUESTED.rawValue
                        self.groupsData[index.row] = group
                        self.showAlert(To: RequestMembership.localized(), for: GroupRequestMessage.localized()) {_ in}
                    }
                }
            }
        }
        else {
            var requestData = MembershipRequest()
            if let _ = group.id {
                requestData.clinic_id = group.id
                requestData.user_id = AppConfig.shared.user.id
                requestData.request_status = GroupStatus.CANCELLED.rawValue
                
                RequestManager.shared.updateGroupStatus(request: requestData) { (status, data) in
                    if status {
                        group.request_status = GroupStatus.OTHER.rawValue
                        self.groupsData[index.row] = group
                        self.showAlert(To: GroupRequestHeading.localized(), for: CancelGroupRequest.localized()) {_ in}
                    }
                }
            }
        }
    }
}

//MARK : - EXTENSIONS
extension SearchGroupVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileGroupCell") as? ProfileGroupCell else { return self.emptyTblCell() }
        if indexPath.row < self.groupsData.count {
            let group = self.groupsData[indexPath.row]
            let imgUrl = group.file?.medium_url ?? group.file?.src
            if let imageUrl = imgUrl {
                let url = URL(string: imageUrl)
                cell.groupImageView?.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "groupDefault"))
            }else{
                cell.groupImageView?.image = #imageLiteral(resourceName: "groupDefault")
            }
            
            if let groupTitle = group.name {
                cell.groupTitle?.text = groupTitle
            }
            cell.lblUnread.text = "\(group.group_notification_count ?? 0)"
            cell.viewUnread.isHidden = (group.group_notification_count ?? 0) == 0
            
            if self.loadMore && indexPath.row == (self.groupsData.count - 1) {
                self.pageNumber += 1
                self.searchGroups()
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let group = self.groupsData[indexPath.row]
        if group.request_status == GroupStatus.REQUESTED.rawValue || group.request_status == nil || group.request_status == GroupStatus.CANCELLED.rawValue {
            self.showOptionsForOtherGroups(index: indexPath)
        }
        else if let id = group.id, let vc = getVC(storyboard: .TABS, vcIdentifier: "GroupInfoScene") as? GroupInfoVC {
            vc.groupById = id
            self.navigateVC(vc)
        }
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: String(describing: ProfileGroupCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: ProfileGroupCell.self))
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        self.tableView.separatorStyle = .none
        if #available(iOS 15.0, *) {
            self.tableView.sectionHeaderTopPadding = 0.0
        }
    }
}
