//
//  SearchHistoryCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/02/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit

class SearchHistoryCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    
    var index: IndexPath?
    var onClickDelete: ((IndexPath?) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        deleteBtn.setTitle("", for: .normal)
        Themer.shared.register(
            target: self,
            action: SearchHistoryCell.setTheme)
    }

    @IBAction func onClickDelete(_ sender: UIButton) {
        self.onClickDelete?(index)
    }
}

extension SearchHistoryCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        lblTitle.textColor = theme.settings.titleColor
    }
}
