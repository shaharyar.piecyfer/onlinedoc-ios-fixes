//
//  SearchThreadVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/02/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit

class SearchThreadVC: BaseVC {
    //MARK: - IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    
    var searchedText = ""
    var loadMore = true
    var pageNumber = 1
    var threadsData = [ThreadModel]()
    var selectedThreadIndex: IndexPath?
    var type = SearchFilterTypes.THREAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.setupNotificationObservers()
        self.setupTheme()
        
        self.searchThreads()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: SearchThreadVC.applyTheme)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
    }
    
    private func setupNotificationObservers(){
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifyThread, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            guard let index = self.selectedThreadIndex else {return}
            if let newThread = notification.userInfo?["Thread"] as? ThreadModel {
                if let notify = notification.userInfo?["Notify"] as? NotifyCRUD {
                    switch notify {
                    case .update:
                        self.threadsData[index.row] = newThread
                        self.tableView.reloadRows(at: [index], with: .automatic)
                    case .delete:
                        self.threadsData.remove(at: index.row)
                        self.tableView.deleteRows(at: [index], with: .automatic)
                    default:
                        break
                    }
                }
            }
        }
    }
    
    deinit {
        print("SearchThreadVC->deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func searchThreads() {
        if self.pageNumber == 1 {
            self.threadsData.removeAll()
            self.tableView.reloadData()
        }
        self.tableView.setEmptyMessage("")
        var searchParams = TimelineParams()
        searchParams.search = self.searchedText
        searchParams.page = self.pageNumber
        searchParams.limit = pageLimit5
        searchParams.type = type.rawValue
        RequestManager.shared.searchTimeline(params: searchParams) { (status, response) in
            if status {
                if let response = response as? SearchTimeineResult {
                    if let data = response.threads, data.isEmpty, self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFoundHomeSearch.localized())
                        self.tableView.reloadData()
                        return
                    }
                    if (response.threads?.count ?? 0) == 0 {
                        self.loadMore = false
                    }
                    self.threadsData.append(contentsOf: response.threads ?? [])
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func gotoUserProfile(_ index: IndexPath) {
        let thread = self.threadsData[index.row]
        if let id = thread.doc_creator?.id, id != AppConfig.shared.user.id {
            self.previewUser(id: id)
        }
    }
    
    private func update(thread at: IndexPath) {
        self.selectedThreadIndex = at
        let thread = self.threadsData[self.selectedThreadIndex!.row]
        let setNotification = !(thread.doc_notification_on ?? false)
        if let id = thread.id, let groupId = thread.clinic_id {
            RequestManager.shared.callNotificationAPI(groupID: groupId, notification: setNotification, docId: id) { (status, response) in
                if status {
                    self.threadsData[at.row].doc_notification_on = setNotification
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadsData[at.row], "Notify": NotifyCRUD.update])
                }
            }
        }
    }
    
    private func showThreadEditOptions(for index:IndexPath, isEdit:Bool, isDelete: Bool){
        if isEdit || isDelete {
            var alert = AlertModel()
            if isEdit {
                var editBtn = AlertBtnModel()
                editBtn.title = "Edit"
                alert.btns.append(editBtn)
            }
            if isDelete {
                var deleteBtn = AlertBtnModel()
                deleteBtn.title = "Delete"
                alert.btns.append(deleteBtn)
            }
            var cancelBtn = AlertBtnModel()
            cancelBtn.title = "Cancel"
            cancelBtn.color = .red
            alert.btns.append(cancelBtn)
            self.showMultiBtnAlert(model: alert) { [weak self] (data) in
                guard let `self` = self else {return}
                if let btn = data as? String {
                    if btn == "Edit" {
                        self.editThread(at: index)
                    }
                    else if btn == "Delete" {
                        self.deleteThread(at: index)
                    }
                }
            }
        }
    }
    
    private func editThread(at index:IndexPath){
        self.selectedThreadIndex = index
        let thread = self.threadsData[self.selectedThreadIndex!.row]
        if let vc = getVC(storyboard: .CREATE_THREAD, vcIdentifier: "CreateThreadScene") as? CreateThreadVC {
            vc.formMode = .edit
            vc.createOrEditThreadFrom = .group
            vc.groupTitle = thread.clinic_name ?? ""
            vc.groupId = thread.clinic_id ?? 0
            vc.threadToEdit = self.threadsData[index.row]
            self.navigateVC(vc)
        }
    }
    
    private func deleteThread(at index:IndexPath){
        self.showAlert(To: "", for: deleteThreadMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                self.delete(at: index)
            }
        }
    }
    
    private func delete(at index: IndexPath) {
        self.selectedThreadIndex = index
        let thread = self.threadsData[self.selectedThreadIndex!.row]
        if let id = thread.id {
            RequestManager.shared.deleteThread(id: id) { (status, response) in
                if status{
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": thread, "Notify": NotifyCRUD.delete])
                    self.showAlert(To: DeleteThreadHeading, for: DeleteThreadMessage.localized()) {_ in}
                }
            }
        }
    }
}

//MARK : - EXTENSIONS
extension SearchThreadVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.threadsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GroupThreadCell.self), for: indexPath) as? GroupThreadCell else {
            return self.emptyTblCell()
        }
        if indexPath.row < self.threadsData.count {
            cell.index = indexPath
            cell.isShowMoreBtn = false
            cell.setupData(indexPath, data: self.threadsData[indexPath.row])
            cell.delegate = self
            cell.onClickProfile = { [weak self] index in
                guard let `self` = self else { return }
                self.gotoUserProfile(index)
            }
            
            if self.loadMore && indexPath.row == (self.threadsData.count - 1) {
                self.pageNumber += 1
                self.searchThreads()
            }
        }
        cell.selectionStyle = .none
        self.view.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let id = self.threadsData[indexPath.row].id, let vc = getVC(storyboard: .TABS, vcIdentifier: "DetailView") as? ThreadDetailVC {
            vc.threadId = id
            self.selectedThreadIndex = indexPath
            self.navigateVC(vc)
        }
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: String(describing: GroupThreadCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: GroupThreadCell.self))
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        self.tableView.separatorStyle = .none
        if #available(iOS 15.0, *) {
            self.tableView.sectionHeaderTopPadding = 0.0
        }
    }
}

extension SearchThreadVC: HomeTableViewCellDelegate {
    func didTapNotification(for threadAtIndex: IndexPath) {
        self.update(thread: threadAtIndex)
    }
    
    func didTapMoreOptions(for threadAtIndex: IndexPath) {
        let thread = self.threadsData[threadAtIndex.row]
        if let threadOwnerId = thread.doc_creator?.id {
            if AppConfig.shared.user.id != threadOwnerId {
                if let isAdmin = thread.is_admin, isAdmin {
                    self.showThreadEditOptions(for: threadAtIndex, isEdit: false, isDelete: true)
                }
            }
            else {
                self.showThreadEditOptions(for: threadAtIndex, isEdit: true, isDelete: true)
            }
        }
    }
}
