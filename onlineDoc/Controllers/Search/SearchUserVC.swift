//
//  SearchUserVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/02/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit

class SearchUserVC: BaseVC {
    //MARK: - IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    
    var searchedText = ""
    var loadMore = true
    var pageNumber = 1
    var usersData = [User]()
    var selectedThreadIndex: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.setupTheme()
        
        self.searchUsers()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: SearchUserVC.applyTheme)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
    }
    
    deinit {
        print("SearchUserVC->deinit")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func searchUsers() {
        if self.pageNumber == 1 {
            self.usersData.removeAll()
            self.tableView.reloadData()
        }
        self.tableView.setEmptyMessage("")
        var searchParams = TimelineParams()
        searchParams.search = self.searchedText
        searchParams.page = self.pageNumber
        searchParams.limit = pageLimit20
        searchParams.type = SearchFilterTypes.USER.rawValue
        RequestManager.shared.searchTimeline(params: searchParams) { (status, response) in
            if status {
                if let response = response as? SearchTimeineResult {
                    if let data = response.users, data.isEmpty, self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFoundHomeSearch.localized())
                        self.tableView.reloadData()
                        return
                    }
                    if (response.users?.count ?? 0) == 0 {
                        self.loadMore = false
                    }
                    self.usersData.append(contentsOf: response.users ?? [])
                    self.tableView.reloadData()
                }
            }
        }
    }
}

//MARK : - EXTENSIONS
extension SearchUserVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usersData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else { return self.emptyTblCell() }
        if indexPath.row < usersData.count {
            cell.setupData(indexPath: indexPath, data: usersData[indexPath.row])
            cell.delegate = self
            if self.loadMore && indexPath.row == (self.usersData.count - 1) {
                self.pageNumber += 1
                self.searchUsers()
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: GeneralViewCell.self), bundle: nil), forCellReuseIdentifier: "GeneralCell")
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        self.tableView.separatorStyle = .none
        if #available(iOS 15.0, *) {
            self.tableView.sectionHeaderTopPadding = 0.0
        }
    }
}

extension SearchUserVC: GeneralViewCellDelegate {
    func didTapRow(at index: Int) {
        if let id = self.usersData[index].id {
            self.previewUser(id: id)
        }
    }
}
