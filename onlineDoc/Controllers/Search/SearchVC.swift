//
//  SearchVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 31/01/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit
import TagListView

class SearchVC: BaseVC {
    //MARK: - IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var uiFilters: TagListView!
    
    var historyData = [SearchHistoryModel]()
    
    var searchedText = ""
    var loadMore = true
    
    var threadsData = [ThreadModel]()
    var usersData = [User]()
    var groupsData = [GroupModel]()
    
    var isSearching = false
    var selectedThreadIndex: IndexPath?
    
    var selectedFilterIndex = IndexPath(row: 0, section: 0)
    var filters: [SearchFilters] = [SearchFilters(index: 0, title: "txt_all", count: 0),
                                    SearchFilters(index: 1, title: "txt_users", count: 0),
                                    SearchFilters(index: 2, title: "txt_threads", count: 0),
                                    SearchFilters(index: 3, title: "txt_files", count: 0),
                                    SearchFilters(index: 4, title: "txt_groups", count: 0),
                                    SearchFilters(index: 5, title: "txt_comments", count: 0)]
    
//    var filterType = SearchFilterTypes.THREAD
    var searchParams = TimelineParams()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.layOutSearchbar()
        self.setupNotificationObservers()
        self.setupTheme()
        
        self.getSearchHistory()
    }
    
    private func setupView() {
        let rightButton = UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"),
                                          style: .plain,
                                              target: self,
                                              action: #selector(self.onClickFilter))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func onClickFilter() {
        if let vc = getVC(storyboard: .TABS, vcIdentifier: String(describing: AdvanceSearchVC.self)) as? AdvanceSearchVC {
            vc.searchParams = self.searchParams
            vc.searchText = self.searchedText
            vc.delegate = self
            self.navigateVC(vc)
        }
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: SearchVC.applyTheme)

        Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
    }
    
    private func setupNotificationObservers(){
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifyThread, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else { return }
            guard let index = self.selectedThreadIndex else {return}
            if let newThread = notification.userInfo?["Thread"] as? ThreadModel {
                if let notify = notification.userInfo?["Notify"] as? NotifyCRUD {
                    switch notify {
                    case .update:
                        self.threadsData[index.row] = newThread
                        self.tableView.reloadRows(at: [index], with: .automatic)
                    case .delete:
                        self.threadsData.remove(at: index.row)
                        self.tableView.deleteRows(at: [index], with: .automatic)
                    default:
                        break
                    }
                }
            }
        }
    }
    
    deinit {
        print("SearchVC->deinit")
        NotificationCenter.default.removeObserver(self)
        for (_, item) in AppConfig.shared.myGroupsWithCategories.enumerated(){
            if item.isSelected{
                item.isSelected = !item.isSelected
                if let cats = item.clinic_categories {
                    for (_, folder) in cats.enumerated(){
                        if folder.isSelected{
                            folder.isSelected = !folder.isSelected
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func layOutSearchbar(){
        navigationItem.titleView = self.searchBar
        self.searchBar.delegate = self
        self.searchBar.placeholder = SearchText.localized()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchThreadVC" {
            if let vc = segue.destination as? SearchThreadVC {
                vc.searchedText = self.searchedText
//                vc.type = self.filterType
            }
        }
        else if segue.identifier == "SearchUserVC" {
            if let vc = segue.destination as? SearchUserVC {
                vc.searchedText = self.searchedText
            }
        }
        else if segue.identifier == "SearchGroupVC" {
            if let vc = segue.destination as? SearchGroupVC {
                vc.searchedText = self.searchedText
            }
        }
    }
    
    private func getSearchHistory() {
        self.historyData.removeAll()
        self.tableView.setEmptyMessage("")
        RequestManager.shared.getSearchHistory() { [weak self] (status, response) in
            guard let `self` = self else { return }
            if status {
                if let response = response as? SearchHistoryListModel {
                    if let data = response.data, data.isEmpty {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                    }
                    self.historyData = response.data ?? []
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func deleteSearchHistory(index: IndexPath) {
        guard let id = self.historyData[index.row].id else {return}
        RequestManager.shared.deleteSearchHistory(id: id) { (status, response) in
            if status {
                self.historyData.remove(at: index.row)
                self.tableView.reloadData()
            }
        }
    }
    
    private func searchTimeline() {
        if self.uiFilters.isHidden == true {
            self.uiFilters.isHidden = false
            self.setupSearchFilter()
            self.searchCount()
        }
        if searchParams.page == 1 {
            self.threadsData.removeAll()
            self.usersData.removeAll()
            self.groupsData.removeAll()
            self.tableView.reloadData()
        }
        self.tableView.setEmptyMessage("")

        if self.searchParams.type == SearchFilterTypes.ALL.rawValue || self.searchParams.type == SearchFilterTypes.NONE.rawValue {
            self.searchParams.page = 1
            self.searchParams.limit = pageLimit3
        }
        else if self.searchParams.type == SearchFilterTypes.THREAD.rawValue || self.searchParams.type == SearchFilterTypes.COMMENT.rawValue || self.searchParams.type == SearchFilterTypes.FILE.rawValue {
            self.searchParams.limit = pageLimit3
        }
        else if self.searchParams.type == SearchFilterTypes.USER.rawValue {
            self.searchParams.limit = pageLimit20
        }
        else if self.searchParams.type == SearchFilterTypes.CLINIC.rawValue {
            self.searchParams.limit = pageLimit10
        }
        self.searchParams.search = self.searchedText
        RequestManager.shared.searchTimeline(params: searchParams) { [weak self] (status, response) in
            guard let `self` = self else { return }
            self.loadMore = true
            if status {
                if let response = response as? SearchTimeineResult {
                    if let threads = response.threads, threads.isEmpty, let groups = response.groups, groups.isEmpty, let users = response.users, users.isEmpty, self.searchParams.page == 1 {
                        self.tableView.setEmptyMessage(noRecordFoundHomeSearch.localized())
                        self.tableView.reloadData()
                    }
                    self.threadsData.append(contentsOf: response.threads ?? [])
                    self.usersData.append(contentsOf: response.users ?? [])
                    self.groupsData.append(contentsOf: response.groups ?? [])
                    if ((response.threads?.count ?? 0) < pageLimit3 && (self.searchParams.type == SearchFilterTypes.THREAD.rawValue || self.searchParams.type == SearchFilterTypes.COMMENT.rawValue || self.searchParams.type == SearchFilterTypes.FILE.rawValue)) ||
                        ((response.users?.count ?? 0) < pageLimit20 && self.searchParams.type == SearchFilterTypes.USER.rawValue) ||
                        ((response.groups?.count ?? 0) < pageLimit10 && self.searchParams.type == SearchFilterTypes.CLINIC.rawValue) {
                        self.loadMore = false
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func searchCount() {
        RequestManager.shared.searchCount(search: self.searchedText) { (status, response) in
            if status {
                if let response = response as? SearchCount {
                    self.filters = self.filters.map { item in
                        switch item.index {
                        case 0:
                            let fileCount = (response.comments_files_count ?? 0) + (response.files_count ?? 0)
                            let allCount = (response.threads_count ?? 0) + (response.users_count ?? 0) + (response.clinics_count ?? 0) + (response.comments_count ?? 0) + fileCount
                            item.count = allCount
                        case 1:
                            item.count = response.users_count ?? 0
                        case 2:
                            item.count = response.threads_count ?? 0
                        case 3:
                            item.count = (response.files_count ?? 0) + (response.comments_files_count ?? 0)
                        case 4:
                            item.count = response.clinics_count ?? 0
                        case 5:
                            item.count = (response.comments_count ?? 0)
                        default:
                            break
                        }
                        return item
                    }
                    self.setupSearchFilter()
                }
            }
        }
    }
    
    private func showOptionsForOtherGroups(index: IndexPath) {
        let group = self.groupsData[index.row]
        var alert = AlertModel()
        
        var requestBtn = AlertBtnModel()
        requestBtn.title = group.request_status == GroupStatus.REQUESTED.rawValue ? CancelRequest.localized() : RequestMembership.localized()
        alert.btns.append(requestBtn)
        
        var cancelBtn = AlertBtnModel()
        cancelBtn.title = "Cancel".localized()
        cancelBtn.color = .red
        alert.btns.append(cancelBtn)
        self.showMultiBtnAlert(model: alert) { [weak self] (data) in
            guard let `self` = self else {return}
            if let btn = data as? String {
                if btn == RequestMembership.localized() {
                    self.didTapGroupRequest(index: index, isSendRequest: true)
                }
                else if btn == CancelRequest.localized() {
                    self.didTapGroupRequest(index: index, isSendRequest: false)
                }
            }
        }
    }
    
    private func didTapGroupRequest(index: IndexPath, isSendRequest: Bool) {
        let group = self.groupsData[index.row]
        if isSendRequest {
            if let _ = group.id {
                RequestManager.shared.joinGroup(id: group.id!) { (status, data) in
                    if status{
                        group.request_status = GroupStatus.REQUESTED.rawValue
                        self.groupsData[index.row] = group
                        self.showAlert(To: RequestMembership.localized(), for: GroupRequestMessage.localized()) {_ in}
                    }
                }
            }
        }
        else {
            var requestData = MembershipRequest()
            if let _ = group.id {
                requestData.clinic_id = group.id
                requestData.user_id = AppConfig.shared.user.id
                requestData.request_status = GroupStatus.CANCELLED.rawValue
                
                RequestManager.shared.updateGroupStatus(request: requestData) { (status, data) in
                    if status {
                        group.request_status = GroupStatus.OTHER.rawValue
                        self.groupsData[index.row] = group
                        self.showAlert(To: GroupRequestHeading.localized(), for: CancelGroupRequest.localized()) {_ in}
                    }
                }
            }
        }
    }
    
    private func gotoUserProfile(_ index: IndexPath) {
        let thread = self.threadsData[index.row]
        if let id = thread.doc_creator?.id, id != AppConfig.shared.user.id {
            self.previewUser(id: id)
        }
    }
    
    private func update(thread at: IndexPath) {
        self.selectedThreadIndex = at
        let thread = self.threadsData[self.selectedThreadIndex!.row]
        let setNotification = !(thread.doc_notification_on ?? false)
        if let id = thread.id, let groupId = thread.clinic_id {
            RequestManager.shared.callNotificationAPI(groupID: groupId, notification: setNotification, docId: id) { (status, response) in
                if status {
                    self.threadsData[at.row].doc_notification_on = setNotification
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": self.threadsData[at.row], "Notify": NotifyCRUD.update])
                }
            }
        }
    }
    
    private func showThreadEditOptions(for index:IndexPath, isEdit:Bool, isDelete: Bool){
        if isEdit || isDelete {
            var alert = AlertModel()
            if isEdit {
                var editBtn = AlertBtnModel()
                editBtn.title = "Edit"
                alert.btns.append(editBtn)
            }
            if isDelete {
                var deleteBtn = AlertBtnModel()
                deleteBtn.title = "Delete"
                alert.btns.append(deleteBtn)
            }
            var cancelBtn = AlertBtnModel()
            cancelBtn.title = "Cancel"
            cancelBtn.color = .red
            alert.btns.append(cancelBtn)
            self.showMultiBtnAlert(model: alert) { [weak self] (data) in
                guard let `self` = self else {return}
                if let btn = data as? String {
                    if btn == "Edit" {
                        self.editThread(at: index)
                    }
                    else if btn == "Delete" {
                        self.deleteThread(at: index)
                    }
                }
            }
        }
    }
    
    private func editThread(at index:IndexPath){
        self.selectedThreadIndex = index
        let thread = self.threadsData[self.selectedThreadIndex!.row]
        if let vc = getVC(storyboard: .CREATE_THREAD, vcIdentifier: "CreateThreadScene") as? CreateThreadVC {
            vc.formMode = .edit
            vc.createOrEditThreadFrom = .group
            vc.groupTitle = thread.clinic_name ?? ""
            vc.groupId = thread.clinic_id ?? 0
            vc.threadToEdit = self.threadsData[index.row]
            self.navigateVC(vc)
        }
    }
    
    private func deleteThread(at index:IndexPath){
        self.showAlert(To: "", for: deleteThreadMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                self.delete(at: index)
            }
        }
    }
    
    private func delete(at index: IndexPath) {
        self.selectedThreadIndex = index
        let thread = self.threadsData[self.selectedThreadIndex!.row]
        if let id = thread.id {
            RequestManager.shared.deleteThread(id: id) { (status, response) in
                if status{
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": thread, "Notify": NotifyCRUD.delete])
                    self.showAlert(To: DeleteThreadHeading, for: DeleteThreadMessage.localized()) {_ in}
                }
            }
        }
    }
}

//MARK : - EXTENSIONS
extension SearchVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.isSearching = false
            self.clearSearchFilter()
            self.threadsData.removeAll()
            self.usersData.removeAll()
            self.groupsData.removeAll()
            self.tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.isSearching = true
        if let txt = self.searchBar.text{
            let trimmedText = txt.trimmingCharacters(in: .whitespacesAndNewlines)
            self.searchedText = trimmedText
        }
        else {
            self.searchedText = ""
        }
        self.searchCount()
        self.searchParams.page = 1
        self.searchTimeline()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchBar.resignFirstResponder()
    }
}

extension SearchVC: TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        let selectedTags = sender.selectedTags()
        _ = selectedTags.map { tag in
            tag.isSelected = false
        }
        tagView.isSelected = !tagView.isSelected
        
        let indexPath = IndexPath(row: tagView.tag, section: 0)
        guard self.selectedFilterIndex != indexPath else {return}
        self.selectedFilterIndex = indexPath
        if indexPath.row == 0 || indexPath.row == 4 || indexPath.row == 5 {
            self.navigationItem.rightBarButtonItem = nil
        }
        else {
            self.setupView()
        }
        self.searchParams.page = 1
        switch indexPath.row {
        case 1:
            self.searchParams.type = SearchFilterTypes.USER.rawValue
        case 2:
            self.searchParams.type = SearchFilterTypes.THREAD.rawValue
        case 3:
            self.searchParams.type = SearchFilterTypes.FILE.rawValue
        case 4:
            self.searchParams.type = SearchFilterTypes.CLINIC.rawValue
        case 5:
            self.searchParams.type = SearchFilterTypes.COMMENT.rawValue
        default:
            self.searchParams.type = SearchFilterTypes.NONE.rawValue
        }
        self.searchTimeline()
    }
    
    fileprivate func setupSearchFilter() {
        uiFilters.isHidden = false
        uiFilters.removeAllTags()
        uiFilters.textFont = AppFont.kMuliSemiBold(14).font()
        uiFilters.alignment = .left
        for item in self.filters {
            let tagView = uiFilters.addTag(item.title.localized().capitalized + " (\(item.count))")
            tagView.tag = item.index
        }
        uiFilters.delegate = self
        self.selectTag()
    }
    
    fileprivate func selectTag() {
        let selectedTags = self.uiFilters.selectedTags()
        _ = selectedTags.map { tag in
            tag.isSelected = false
        }
        self.uiFilters.tagViews[self.selectedFilterIndex.row].isSelected = true
    }
    
    fileprivate func clearSearchFilter() {
        uiFilters.isHidden = true
        uiFilters.removeAllTags()
    }
}

extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearching {
            switch self.selectedFilterIndex.row {
            case 0: return 6
            default: return 1
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard isSearching else {return self.historyData.count}
        guard self.selectedFilterIndex.row == 0 else {
            switch self.selectedFilterIndex.row {
            case 1: return self.usersData.count
            case 2,3,5: return self.threadsData.count
            case 4: return self.groupsData.count
            default: return 0
            }
        }
        if section == 0 {
            return self.threadsData.count > 3 ? 3 : self.threadsData.count
        } else if section == 2 {
            return self.usersData.count
        } else if section == 4 {
            return self.groupsData.count
        } else if section == 1 {
            return self.threadsData.count < 3 ? 0 : 1
        } else if section == 3 {
            return self.usersData.count < 3 ? 0 : 1
        } else if section == 5 {
            return self.groupsData.count < 3 ? 0 : 1
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard isSearching else {return 0}
        guard self.selectedFilterIndex.row == 0 else {return 0}
        if section == 0 {
            return self.threadsData.isEmpty ? 0 : 50
        } else if section == 2 {
            return self.usersData.isEmpty ? 0 : 50
        } else if section == 4 {
            return self.groupsData.isEmpty ? 0 : 50
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard isSearching else {return nil}
        guard self.selectedFilterIndex.row == 0 else {return nil}
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = AppConfig.shared.isDarkMode ? AppColors.darkGrayColor : AppColors.mediumGrayColor
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        if section == 0 {
            label.text = "Threads".localized().uppercased()
        } else if section == 2 {
            label.text = "Users".localized().uppercased()
        }else {
            label.text = "Groups".localized().uppercased()
        }
//        label.font = UIFont().futuraPTMediumFont(16)
        label.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard isSearching else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SearchHistoryCell.self), for: indexPath) as? SearchHistoryCell else {
                return self.emptyTblCell()
            }
            if indexPath.row < self.historyData.count {
                cell.index = indexPath
                cell.lblTitle.text = self.historyData[indexPath.row].text
                cell.lblTitle.font = AppFont.kMuliLight(14).font()
                cell.onClickDelete = { [weak self] _index in
                    guard let `self` = self, let _index = _index else {return}
                    self.deleteSearchHistory(index: _index)
                }
            }
            cell.selectionStyle = .none
            return cell
        }
        guard isSearching && self.selectedFilterIndex.row == 0 else {
            if self.searchParams.type == SearchFilterTypes.THREAD.rawValue || self.searchParams.type == SearchFilterTypes.COMMENT.rawValue || self.searchParams.type == SearchFilterTypes.FILE.rawValue {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GroupThreadCell.self), for: indexPath) as? GroupThreadCell else {
                    return self.emptyTblCell()
                }
                if indexPath.row < self.threadsData.count {
                    cell.index = indexPath
                    cell.isShowMoreBtn = false
                    cell.setupData(indexPath, data: self.threadsData[indexPath.row])
                    cell.delegate = self
                    cell.onClickProfile = { [weak self] index in
                        guard let `self` = self else { return }
                        self.gotoUserProfile(index)
                    }
                }
                cell.selectionStyle = .none
                return cell
            }
            else if self.searchParams.type == SearchFilterTypes.USER.rawValue {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else { return self.emptyTblCell() }
                if indexPath.row < usersData.count {
                    cell.setupData(indexPath: indexPath, data: usersData[indexPath.row])
                    cell.delegate = self
                }
                cell.selectionStyle = .none
                return cell
            }
            else if self.searchParams.type == SearchFilterTypes.CLINIC.rawValue {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileGroupCell") as? ProfileGroupCell else { return self.emptyTblCell() }
                if indexPath.row < self.groupsData.count {
                    let group = self.groupsData[indexPath.row]
                    let imgUrl = group.file?.medium_url ?? group.file?.src
                    if let imageUrl = imgUrl {
                        let url = URL(string: imageUrl)
                        cell.groupImageView?.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "groupDefault"))
                    }else{
                        cell.groupImageView?.image = #imageLiteral(resourceName: "groupDefault")
                    }
                    
                    if let groupTitle = group.name {
                        cell.groupTitle?.text = groupTitle
                    }
                    cell.lblUnread.text = "\(group.group_notification_count ?? 0)"
                    cell.viewUnread.isHidden = (group.group_notification_count ?? 0) == 0
                }
                cell.selectionStyle = .none
                return cell
            }
            else {
                return self.emptyTblCell()
            }
        }
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GroupThreadCell.self), for: indexPath) as? GroupThreadCell else {
                return self.emptyTblCell()
            }
            if indexPath.row < self.threadsData.count {
                cell.index = indexPath
                cell.isShowMoreBtn = false
                cell.setupData(indexPath, data: self.threadsData[indexPath.row])
                cell.delegate = self
                cell.onClickProfile = { [weak self] index in
                    guard let `self` = self else { return }
                    self.gotoUserProfile(index)
                }
            }
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoadMoreCell.self), for: indexPath) as? LoadMoreCell else {
                return self.emptyTblCell()
            }
            cell.loadingIndicator.isHidden = true
            cell.loadingIndicator.stopAnimating()
            cell.btnLoadMore.setTitle("txt_see_more".localized().capitalized, for: .normal)
            cell.onClickLoadMore = { [weak self] in
                guard let `self` = self else {return}
                cell.loadingIndicator.isHidden = true
                cell.loadingIndicator.stopAnimating()
                cell.btnLoadMore.setTitle("txt_see_more".localized().capitalized, for: .normal)
                self.searchParams.type = SearchFilterTypes.THREAD.rawValue
                self.selectedFilterIndex = IndexPath(row: 2, section: 0)
                self.selectTag()
                self.searchTimeline()
            }
            return cell
        }
        else if indexPath.section == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else { return self.emptyTblCell() }
            if indexPath.row < usersData.count {
                cell.setupData(indexPath: indexPath, data: usersData[indexPath.row])
                cell.delegate = self
            }
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.section == 3 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoadMoreCell.self), for: indexPath) as? LoadMoreCell else {
                return self.emptyTblCell()
            }
            cell.loadingIndicator.isHidden = true
            cell.loadingIndicator.stopAnimating()
            cell.btnLoadMore.setTitle("txt_see_more".localized().capitalized, for: .normal)
            cell.onClickLoadMore = { [weak self] in
                guard let `self` = self else {return}
                cell.loadingIndicator.isHidden = true
                cell.loadingIndicator.stopAnimating()
                cell.btnLoadMore.setTitle("txt_see_more".localized().capitalized, for: .normal)
                self.searchParams.type = SearchFilterTypes.USER.rawValue
                self.selectedFilterIndex = IndexPath(row: 1, section: 0)
                self.selectTag()
                self.searchTimeline()
            }
            return cell
        }
        else if indexPath.section == 4 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileGroupCell") as? ProfileGroupCell else { return self.emptyTblCell() }
            if indexPath.row < self.groupsData.count {
                let group = self.groupsData[indexPath.row]
                let imgUrl = group.file?.medium_url ?? group.file?.src
                if let imageUrl = imgUrl {
                    let url = URL(string: imageUrl)
                    cell.groupImageView?.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "groupDefault"))
                }else{
                    cell.groupImageView?.image = #imageLiteral(resourceName: "groupDefault")
                }
                
                if let groupTitle = group.name {
                    cell.groupTitle?.text = groupTitle
                }
                cell.lblUnread.text = "\(group.group_notification_count ?? 0)"
                cell.viewUnread.isHidden = (group.group_notification_count ?? 0) == 0
            }
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.section == 5 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoadMoreCell.self), for: indexPath) as? LoadMoreCell else {
                return self.emptyTblCell()
            }
            cell.loadingIndicator.isHidden = true
            cell.loadingIndicator.stopAnimating()
            cell.btnLoadMore.setTitle("txt_see_more".localized().capitalized, for: .normal)
            cell.onClickLoadMore = { [weak self] in
                guard let `self` = self else {return}
                cell.loadingIndicator.isHidden = true
                cell.loadingIndicator.stopAnimating()
                cell.btnLoadMore.setTitle("txt_see_more".localized().capitalized, for: .normal)
                self.searchParams.type = SearchFilterTypes.CLINIC.rawValue
                self.selectedFilterIndex = IndexPath(row: 4, section: 0)
                self.selectTag()
                self.searchTimeline()
            }
            return cell
        }
        else {
            return self.emptyTblCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard isSearching else {
            if let text = self.historyData[indexPath.row].text {
                self.isSearching = true
                self.searchedText = text
                self.searchBar.text = text
                self.searchTimeline()
            }
            return
        }
        guard isSearching && self.selectedFilterIndex.row == 0 else {
            if self.searchParams.type == SearchFilterTypes.THREAD.rawValue || self.searchParams.type == SearchFilterTypes.COMMENT.rawValue || self.searchParams.type == SearchFilterTypes.FILE.rawValue {
                if let id = self.threadsData[indexPath.row].id, let vc = getVC(storyboard: .TABS, vcIdentifier: "DetailView") as? ThreadDetailVC {
                    vc.threadId = id
                    self.selectedThreadIndex = indexPath
                    self.navigateVC(vc)
                }
            }
            else if self.searchParams.type == SearchFilterTypes.USER.rawValue {
                if let id = self.usersData[indexPath.row].id {
                    self.previewUser(id: id)
                }
            }
            else if self.searchParams.type == SearchFilterTypes.CLINIC.rawValue {
                let group = self.groupsData[indexPath.row]
                if group.request_status == GroupStatus.REQUESTED.rawValue || group.request_status == nil || group.request_status == GroupStatus.CANCELLED.rawValue {
                    self.showOptionsForOtherGroups(index: indexPath)
                }
                else if let id = group.id, let vc = getVC(storyboard: .TABS, vcIdentifier: "GroupInfoScene") as? GroupInfoVC {
                    vc.groupById = id
                    self.navigateVC(vc)
                }
            }
            return
        }
        if indexPath.section == 0 {
            if let id = self.threadsData[indexPath.row].id, let vc = getVC(storyboard: .TABS, vcIdentifier: "DetailView") as? ThreadDetailVC {
                vc.threadId = id
                self.selectedThreadIndex = indexPath
                self.navigateVC(vc)
            }
        }
        else if indexPath.section == 2 {
            if let id = self.usersData[indexPath.row].id {
                self.previewUser(id: id)
            }
        }
        else if indexPath.section == 4 {
            let group = self.groupsData[indexPath.row]
            if group.request_status == GroupStatus.REQUESTED.rawValue || group.request_status == nil || group.request_status == GroupStatus.CANCELLED.rawValue {
                self.showOptionsForOtherGroups(index: indexPath)
            }
            else if let id = group.id, let vc = getVC(storyboard: .TABS, vcIdentifier: "GroupInfoScene") as? GroupInfoVC {
                vc.groupById = id
                self.navigateVC(vc)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView && (self.searchParams.type != SearchFilterTypes.ALL.rawValue && self.searchParams.type != SearchFilterTypes.NONE.rawValue) && (scrollView.contentOffset.y + 1) >= (scrollView.contentSize.height - scrollView.frame.size.height) {
            if self.loadMore {
                self.loadMore = false
                self.searchParams.page += 1
                self.searchTimeline()
            }
        }
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: String(describing: GroupThreadCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: GroupThreadCell.self))
        self.tableView.register(UINib.init(nibName: String(describing: GeneralViewCell.self), bundle: nil), forCellReuseIdentifier: "GeneralCell")
        self.tableView.register(UINib(nibName: String(describing: ProfileGroupCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: ProfileGroupCell.self))
        self.tableView.register(UINib(nibName: String(describing: LoadMoreCell.self), bundle: nil), forCellReuseIdentifier: String(describing: LoadMoreCell.self))
        self.tableView.register(UINib(nibName: String(describing: SearchHistoryCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SearchHistoryCell.self))
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        self.tableView.separatorStyle = .none
        if #available(iOS 15.0, *) {
            self.tableView.sectionHeaderTopPadding = 0.0
        }
    }
}

extension SearchVC: HomeTableViewCellDelegate {
    func didTapNotification(for threadAtIndex: IndexPath) {
        self.update(thread: threadAtIndex)
    }
    
    func didTapMoreOptions(for threadAtIndex: IndexPath) {
        let thread = self.threadsData[threadAtIndex.row]
        if let threadOwnerId = thread.doc_creator?.id {
            if AppConfig.shared.user.id != threadOwnerId {
                if let isAdmin = thread.is_admin, isAdmin {
                    self.showThreadEditOptions(for: threadAtIndex, isEdit: false, isDelete: true)
                }
            }
            else {
                self.showThreadEditOptions(for: threadAtIndex, isEdit: true, isDelete: true)
            }
        }
    }
    
    func didTapComment(with: Int, at index: IndexPath) {
        if let id = self.threadsData[index.row].id, let vc = getVC(storyboard: .TABS, vcIdentifier: "DetailView") as? ThreadDetailVC {
            vc.threadId = id
            self.selectedThreadIndex = index
            self.navigateVC(vc)
        }
    }
}

extension SearchVC: GeneralViewCellDelegate {
    func didTapRow(at index: Int) {
        if let id = self.usersData[index].id {
            self.previewUser(id: id)
        }
    }
}

extension SearchVC: AdvanceSearchVCDelegate {
    func done(filterType: String) {
        switch filterType {
        case SearchFilterTypes.ALL.rawValue, SearchFilterTypes.NONE.rawValue:
            self.selectedFilterIndex = IndexPath(row: 0, section: 0)
        case SearchFilterTypes.THREAD.rawValue:
            self.selectedFilterIndex = IndexPath(row: 1, section: 0)
        case SearchFilterTypes.COMMENT.rawValue:
            self.selectedFilterIndex = IndexPath(row: 2, section: 0)
        case SearchFilterTypes.FILE.rawValue:
            self.selectedFilterIndex = IndexPath(row: 3, section: 0)
        default:
            self.selectedFilterIndex = IndexPath(row: 0, section: 0)
        }
        self.selectTag()
        self.searchParams.page = 1

        var selectedClinicIds = [Int]()
        var selectedCatIds = [Int]()
        for (_, item) in AppConfig.shared.myGroupsWithCategories.enumerated(){
            if item.isSelected{
                if let id = item.id {
                    selectedClinicIds.append(id)
                }
                if let cats = item.clinic_categories {
                    for (_, folder) in cats.enumerated(){
                        if folder.isSelected{
                            if let id = folder.id{
                                selectedCatIds.append(id)
                            }
                        }
                    }
                }
            }
        }
        self.searchParams.clinic_ids = selectedClinicIds
        self.searchParams.category_ids = selectedCatIds
        self.searchParams.type = filterType
        self.searchTimeline()
    }
}
