//
//  TabbarVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 03/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//
//
import UIKit

/// Default index value for priviousSelectedIndex
private let defaultIndexValue = 0

public class TabBarVC: UITabBarController {

    // MARK: - Overrides
    public override var selectedIndex: Int {
        didSet {
            guard let items = self.tabBar.items else { return }
            if items.indices.contains(selectedIndex) {
                let item = items[selectedIndex]
                self.tabBar(tabBar, didSelect: item)
            }
        }
    }
    static var isFromSignUp:Bool = false
    public override var viewControllers: [UIViewController]? {
        didSet {
            setup()
        }
    }

    /// Tabbar height
    @IBInspectable var barHeight: CGFloat {
        get{
            return self.kBarHeight ?? self.tabBar.frame.height
        }
        set{
            self.kBarHeight = newValue
        }
    }

    /// icon up animation point
    @IBInspectable var upAnimationPoint: CGFloat {
        get{
            return self.kUpAnimationPoint
        }
        set{
            self.kUpAnimationPoint = newValue
        }
    }

    private var kBarHeight: CGFloat?

    private var kUpAnimationPoint: CGFloat = 0//20

    private var priviousSelectedIndex: Int = defaultIndexValue

    private var priviousSelectedIndexBeforeCreate: Int = defaultIndexValue

//    var mDataLocationMaps:[RecommendationModel] = [RecommendationModel]()

    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.viewControllers.forEach({ (controllers) in
            if(controllers.isKind(of: TabBarVC.self) || controllers.isKind(of: SplashVC.self)){

            }else{
                controllers.removeFromParent()
            }
        })
        if(self.navigationController != nil){
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        }
        
        self.tabBar.barTintColor = AppConfig.shared.isDarkMode ? .darkGray : AppColors.extraLightGrayColor
        self.tabBar.tintColor = .systemBlue
        self.tabBar.unselectedItemTintColor = AppConfig.shared.isDarkMode ? AppColors.lightGrayColor : .darkGray
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.tabBar.layer.borderWidth = 0.0
        self.tabBar.layer.borderColor = UIColor.clear.cgColor
        self.tabBar.clipsToBounds = true
        
        self.setObserver()
    }

    /// Notifies the view controller that its view was added to a view hierarchy.
    ///
    /// - Parameter animated: variable for namiation
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setup()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func setup() {
        guard let count = tabBar.items?.count, count > 0 else { return }
        if self.priviousSelectedIndex == defaultIndexValue {
            if let item = self.tabBar.selectedItem {
                self.tabBar(self.tabBar, didSelect: item)
            }
        }
        self.applicationDidBecomeActive()
    }
    
    /// setObserver
    func setObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: .notifyNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(notifyCount), name: .chatCountCheck, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(notifyNotificationCount), name: .notificationCountCheck, object: nil)
    }

    @objc func notifyCount(){
        let totalCount = AppConfig.shared.getChatCount()
        self.tabBar.items?[2].badgeValue = totalCount > 0 ? "\(totalCount >= 100 ? "99+" : "\(totalCount)")" : nil
        updateAppBadgeCount()
    }
    
    @objc func notifyNotificationCount(){
        let totalCount = AppConfig.shared.getNotificationCount()
        self.tabBar.items?[1].badgeValue = totalCount > 0 ? "\(totalCount >= 100 ? "99+" : "\(totalCount)")" : nil
        updateAppBadgeCount()
    }
    
    private func updateAppBadgeCount() {
        let totalCount = AppConfig.shared.getChatCount() + AppConfig.shared.getNotificationCount()
        UIApplication.shared.applicationIconBadgeNumber = totalCount
    }

    /// removeObserver
    func removeObserver() {
        NotificationCenter.default.removeObserver(self)
    }

    /// call when application become active
    @objc func applicationDidBecomeActive() {
        self.apiCallForNotifyCount()
        DispatchQueue.main.async { [weak self] in
            guard let uSelf = self else { return }
            _ = uSelf.getUpView(index: uSelf.selectedIndex)
//            if view.frame.origin.y > 0 {
//                view.frame.origin.y -= uSelf.kUpAnimationPoint
//            }
        }
    }

    deinit {
        print("TabBarVC->deinit")
        self.removeObserver()
    }
}

// MARK: - set bar height
extension TabBarVC {
    override public func viewWillLayoutSubviews() {
        guard var height = kBarHeight else { return }
        height += self.view.safeAreaInsets.bottom
        var tabBarFrame = self.tabBar.frame
        tabBarFrame.size.height = height
        tabBarFrame.origin.y = UIScreen.main.bounds.height - height
        self.tabBar.frame = tabBarFrame
        self.tabBar.clipsToBounds = false
    }
    
    /// Sent to the delegate when the user selects a tab bar item.
    ///
    /// - Parameters:
    ///   - tabBar: The tab bar that is being customized.
    ///   - item: The tab bar item that was selected.
    override public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    }
    /// Get specific view from
    ///
    /// - Parameter index: view index
    /// - Returns: specific view
    func getUpView(index: Int) -> UIView {
        let orderedTabBarItemViews: [UIView] = {
            let interactionViews = tabBar.subviews.filter({ $0 is UIControl })
            return interactionViews.sorted(by: { $0.frame.minX < $1.frame.minX })
        }()
        return orderedTabBarItemViews[index]
    }
}
