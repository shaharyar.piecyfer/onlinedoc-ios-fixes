//
//  GroupsListViewController.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 23/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
protocol GroupsListViewControllerDelegate : class {
     func didGroupSelection(data: [MyGroups],status: Bool)
}

class GroupsListVC: BaseVC {
     var groups = [MyGroups](){
          didSet{
               self.listTableView.reloadData()
          }
     }
     var groupsSource = [MyGroups](){
          didSet{
               self.groups = groupsSource
          }
     }
     var delegate : GroupsListViewControllerDelegate!
     var selectedGroups = [Int]()
     @IBOutlet weak var searchBar:UISearchBar!
     @IBOutlet weak var listTableView:UITableView!
     
     override func viewDidLoad() {
          super.viewDidLoad()
          self.layOutSearchbar()
          listTableView.register(UINib(nibName: "CategoryRowCell", bundle: Bundle.main), forCellReuseIdentifier: "CategoryCell")
          self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: DoneText.localized(), style: .plain, target: self, action: #selector(doneTapped))
          self.setupTheme()
     }
     
     private func setupTheme() {
          Themer.shared.register(
               target: self.searchBar,
               action: UISearchBar.applyTheme)
     }
     
     private func setupNotificationObservers(){
     }
     
     func layOutSearchbar(){
          self.searchBar.placeholder = SearchText.localized
          self.searchBar.delegate = self
          if let searchTextField = self.searchBar.textField{
               searchTextField.borderStyle = .none
               searchTextField.setBottomBorder()
               searchTextField.textColor = .white
          }
     }
     
     @objc func doneTapped(){
          for group in groupsSource{
               if group.isChecked{
                    
               }
          }
          self.delegate?.didGroupSelection(data: self.groups, status: true)
          self.navigationController?.popViewController(animated: true)
     }
}

extension GroupsListVC: UITableViewDelegate, UITableViewDataSource{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          self.groups.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as? CategoryRowCell
          else {
               return GroupViewCell()
          }
          //        cell.setData(indexPath: indexPath, data: self.groups[indexPath.row])
          cell.delegate = self
          return cell
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          groupsSource[indexPath.row].isChecked = !groupsSource[indexPath.row].isChecked
     }
}

extension GroupsListVC: UISearchBarDelegate{
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
          guard !searchText.isEmpty else {
               self.groups = self.groupsSource
               
               self.searchBar.endEditing(true)
               return
          }
          
          groups = groupsSource.filter({ group -> Bool in
               (group.clinicName)!.lowercased().contains(searchText.lowercased())
          })
     }
     
     func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
          self.view.endEditing(true)
          self.groups = groupsSource
     }
}

extension GroupsListVC: CategoryRowCellDelegate{
     func didTapCheckMark(index: IndexPath, order: Int?) {
          let isValidIndex = groupsSource.indices.contains(index.row)
          if isValidIndex{
               self.groupsSource[index.row].isChecked = !groupsSource[index.row].isChecked
          }
     }
     
     func didEditCategory(at index: IndexPath, with Text: String) {
          //NO Use here
     }
}
