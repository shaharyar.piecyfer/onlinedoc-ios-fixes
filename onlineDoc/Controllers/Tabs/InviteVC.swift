//
//  InviteVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 20/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import TagListView

enum InvitationType {
    case member, group, all
}

class InviteVC: BaseVC {
    //    MARK: - Instances
    var isFromUserListVC = false
    var isFromMemberProfileVC = false
    var selectedMembers = [User]()
    var invitationType: InvitationType = .group
    var groups = [GroupModel]()
    var groupsSource = [GroupModel](){
        didSet{
            if groupsSource.count > 0 {
//                groupsSource[0].isSelected = true
                self.groups = groupsSource
            }
        }
    }
    
    //    MARK: - Actions
    @IBAction func didTapSelectRecipients(_ sender: UIButton) {
        self.performSegue(withIdentifier: String(describing: InvitePickRecipientVC.self), sender: nil)
    }
    
    //   MARK: - Outlets
    @IBOutlet weak var btnLabel: UILabel!
    @IBOutlet weak var selectRecipientsButtonView: UIView!
    @IBOutlet weak var selectedRecipientsTableViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var groupsTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var selectedRecipientsLabel: UILabel!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var recipientsTableView: UITableView!
    @IBOutlet weak var selectUserView: UIView!{
        didSet{
            selectUserView.showViewBorders(width: 1)
            selectUserView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectRecipients)))
            selectUserView.isUserInteractionEnabled = true
            
            if self.invitationType == .member{
                selectUserView.isUserInteractionEnabled = false
            }else{
                selectUserView.isUserInteractionEnabled = true
                
            }
        }
    }
    @IBOutlet weak var selectUserLabel: UILabel!{
        didSet{
            selectUserLabel.text = recipientPlaceholder.localized()
        }
    }
    @IBOutlet weak var tagsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var chipsView:TagListView!{
        didSet{
//            if invitationType == .member{
//                self.initUserTags(users: [memberName])
//            }else{
//                var selecteMemberNames = [String]()
//                for member in AppConfig.shared.threadGroupMembers{
//                    if member.isSelected{
//                        selecteMemberNames.append(member.name)
//                    }
//                }
//                if selecteMemberNames.count > 10 {
//                    selecteMemberNames = selecteMemberNames[0...9].map{String($0)}
//                    selecteMemberNames.append("...")
//                }
//                self.initUserTags(users: selecteMemberNames)
//            }
        }
    }
    @IBOutlet weak var selectOptionsView: UIView!{
        didSet{
            selectOptionsView.showViewBorders(width: 1)
        }
    }
    @IBOutlet weak var selectOptionsLabel: UILabel!{
        didSet{
            selectOptionsLabel.text = recipientsPlaceholder.localized()
        }
    }
    @IBOutlet weak var noteTextView: UITextView!{
        didSet{
            noteTextView.showViewBorders(width: 1, color: AppColors.borderColor)
            noteTextView.text = InvitePlaceholder.localized()
        }
    }
    
    //    MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.setupTBL()
        if invitationType == .member || invitationType == .all {
            self.loadGroups()
            self.tableView.reloadData()
        }
//        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send".localized(), style: .plain, target: self, action: #selector(sendInvitation))
        
        self.setupTheme()
        
        if invitationType == .group || invitationType == .all {
            self.selectRecipients()
        }
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: InviteVC.setTheme)
    }
    
    private func setupView(){
        applyViewChanges()
    }
    
    deinit {
        print("InviteVC->deinit")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
//        UITextView.appearance().tintColor = .white
        
        if (isFromUserListVC || isFromMemberProfileVC) {
//            selectedRecipientsLabel.isHidden = false
//            self.recipientsTableView.isHidden = false
//            selectedRecipientsTableViewHeightConstant.constant = 120
//            self.selectRecipientsButtonView.isUserInteractionEnabled = false
//            self.selectRecipientsButtonView.alpha = 0.3
//            self.recipientsTableView.reloadData()
        }
        
        if (self.selectedMembers.count > 0) {
            self.selectedRecipientsLabel.isHidden = false
            self.recipientsTableView.isHidden = false
        }
        else {
            self.selectedRecipientsLabel.isHidden = true
            self.recipientsTableView.isHidden = true
        }
        selectedRecipientsTableViewHeightConstant.constant = CGFloat((selectedMembers.count)) * 120
        
        if (isFromUserListVC || isFromMemberProfileVC) {
            self.recipientsTableView.reloadData()
        }
    }
    
    func applyViewChanges() {
        selectRecipientsButtonView.layer.cornerRadius = 5
        selectRecipientsButtonView.clipsToBounds = true
        selectRecipientsButtonView.layer.masksToBounds = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        if invitationType == .member{
//            self.initUserTags(users: [self.memberName])
//        }
    }
    
    func loadGroups(){
        if self.groups.count == 0 {
            RequestManager.shared.getVisitedGroups() { (status, response) in
                if status {
                    if let data = response as? GroupListModel {
                        if let userGroups = data.clinics {
                            self.groupsSource = userGroups
//                            self.groupsSource = userGroups.filter{ (group) in
//                                if group.can_send_invite == WhoCanUpdate.ADMIN.rawValue {
//                                    if let admins = group.clinic_admins, admins.contains(where: { (admin) -> Bool in
//                                        admin.id == AppConfig.shared.user.id
//                                    }) {
//                                        return true
//                                    }
//                                    return false
//                                }
//                                return true
//                            }
                            self.groupsTableViewHeightConstraint.constant = CGFloat((self.groups.count)) * 57
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectOptionsView"{
            if let vc = segue.destination as? GroupsListViewController {
                vc.delegate = self
            }
        }
        else if segue.identifier == String(describing: InvitePickRecipientVC.self) {
            if let vc = segue.destination as? InvitePickRecipientVC {
                vc.selectedRecipients = self.selectedMembers
                vc.delegate = self
            }
        }
    }
    
    //    MARK: - Selector Methods
    @objc func gotoGroupsList(){
        self.performSegue(withIdentifier: "selectOptionsView", sender: self)
    }
    
    @objc func selectRecipients(){
        self.performSegue(withIdentifier: String(describing: InvitePickRecipientVC.self), sender: nil)
    }
    
    @objc func sendInvitation(){
        self.view.endEditing(true)
        if self.selectedMembers.count == 0 {
            self.showAlert(To: noInviteeSelectedMessage.localized(), for: selectInvitees.localized()) { [weak self] _ in
                guard let `self` = self else {return}
                self.selectRecipients()
            }
            return
        }
        if !self.groups.contains(where: {$0.isSelected}) {
            self.showAlert(To: "", for: selectGroupFirst.localized()) {_ in}
            return
        }
        let users: [String] = selectedMembers.compactMap { user in
            if user.email != nil {
                return user.email
            }
            else if user.phone != nil {
                return user.phone
            }
            return nil
        }
//        let users = selectedMembers.compactMap{$0.email || $0.phone}
        let selectedClinicsIds = self.groups.filter({$0.isSelected}).compactMap({$0.id})
        
        var params = GroupsInvitation()
        params.clinic_ids = selectedClinicsIds
        params.users = users

        if let note = self.noteTextView.text, note != InvitePlaceholder.localized() {
            params.message = note
        }

        // Call for Group Invite
        RequestManager.shared.inviteToGroups(params: params) { (status, response) in
            if status{
                if let _ = response as? GeneralSuccessResult {
                    if let group = self.groups.first {
                        NotificationCenter.default.post(name: .notifyGroupMember, object: nil, userInfo: ["Group": group, "IsAdded": true, "Members": params.users.count])
                    }
                    self.showAlert(To: InvitationSentHeader.localized(), for: InvitationSentMessage.localized()) { [weak self] (status) in
                        guard let `self` = self else {return}
                        self.selectedMembers.removeAll()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.dismissVC()
                        }
                    }
                }
            }
        }
    }
}

//MARK: - Extensions
extension InviteVC : GroupsListViewControllerDelegate{
    func didGroupSelection(data: [MyGroups] , status: Bool) {
        if status{
        }
    }
}

extension InviteVC: TagListViewDelegate{
    func initUserTags(users: [String]) {
        chipsView.isHidden = false
        chipsView.textFont = .systemFont(ofSize: 20)
        chipsView.removeAllTags()
        chipsView.textFont = UIFont.systemFont(ofSize: 14)
        chipsView.alignment = .left
        for name in users{
            chipsView.addTag(name)
        }
    }
}

extension InviteVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == noteTextView{
            if let text = textView.text, text == InvitePlaceholder.localized(){
                textView.text = ""
            }
        }
    }
    
}

// MARK: - TableView Delegate
extension InviteVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == self.tableView) {
            return self.groups.count
        }
        else if (tableView == recipientsTableView) {
            if (isFromUserListVC || isFromMemberProfileVC) {
                return 1
            }
            else {
                return selectedMembers.count
            }
        }
        else {
            preconditionFailure()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == self.tableView) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as? CategoryRowCell
            else {
                return GroupViewCell()
            }
            cell.setData(indexPath: indexPath, data: self.groups[indexPath.row])
            cell.delegate = self
            return cell
        }
        else if (tableView == recipientsTableView) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoggedInUserInfoCell.self), for: indexPath) as? LoggedInUserInfoCell else {preconditionFailure()}
            if indexPath.row < selectedMembers.count {
                cell.setCellData(indexPath: indexPath, data: selectedMembers[indexPath.row])
                cell.deleteActionCompletion = { [weak self] index in
                    guard let strongSelf = self else { return }
                    strongSelf.selectedMembers.remove(at: index)
                    if (strongSelf.selectedMembers.count == 0) {
                        strongSelf.recipientsTableView.isHidden = true
                        strongSelf.selectedRecipientsLabel.isHidden = true
                    }
                    
                    strongSelf.selectedRecipientsTableViewHeightConstant.constant = CGFloat((strongSelf.selectedMembers.count)) * 120
                    strongSelf.recipientsTableView.reloadData()
                }
            }
            return cell
            
        }
        else {
            preconditionFailure()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == self.tableView) {
            self.groups[indexPath.row].isSelected = !groups[indexPath.row].isSelected
            self.tableView.reloadData()
        }
        else if (tableView == recipientsTableView) {}
        else {
            preconditionFailure()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (tableView == recipientsTableView) {
            cell.contentView.layer.masksToBounds = true
            let radius = cell.contentView.layer.cornerRadius
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: radius).cgPath
        } // End Function
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: CategoryRowCell.self), bundle: nil), forCellReuseIdentifier: "CategoryCell")
        
        self.tableView.separatorStyle = .none
        
        self.recipientsTableView.delegate = self
        self.recipientsTableView.dataSource = self
        self.recipientsTableView.register(UINib.init(nibName: String(describing: LoggedInUserInfoCell.self), bundle: nil), forCellReuseIdentifier: String(describing: LoggedInUserInfoCell.self))
        self.recipientsTableView.rowHeight = UITableView.automaticDimension
        self.recipientsTableView.separatorStyle = .none
    }
}

extension InviteVC: CategoryRowCellDelegate{
    
    func didTapCheckMark(index: IndexPath, order: Int?) {
        let isValidIndex = groupsSource.indices.contains(index.row)
        if isValidIndex{
            self.groups[index.row].isSelected = !groupsSource[index.row].isSelected
        }
    }
    func didEditCategory(at index: IndexPath, with Text: String) {
        //Optional
    }
}

extension InviteVC: RecipientListViewControllerDelegate{
    func selectedRecipients(recipients: [User]) {
        selectedMembers.removeAll()
        selectedMembers.append(contentsOf: recipients)
        self.recipientsTableView.reloadData()
    }
}

struct GroupsInvitation {
    var users : [String]!
    var clinic_ids : [Int]!
    var message : String?
}

extension InviteVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        btnLabel.textColor = theme.settings.whiteColor
        noteTextView.textColor = theme.settings.subTitleColor
    }
}
