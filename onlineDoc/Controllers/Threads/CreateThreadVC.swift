//
//  CreateThreadVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 04/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField
import MobileCoreServices
import WeScan
import VisionKit
import PDFKit
import Photos
import OpalImagePicker

class CreateThreadVC: BaseVC {
    
    //MARK: - IBOutlets
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var groupTitleLabel: UILabel!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var attachmentButton : UIButton!
    @IBOutlet weak var createThreadButton: UIButton!
    @IBOutlet weak var attachmentCollectionView: UICollectionView!
    @IBOutlet weak var keyboardHeightLayoutConstraint:NSLayoutConstraint!
    
    @IBOutlet weak var threadDescription: UITextView!{
        didSet{
            threadDescription.text = ThreadDetailPlaceholder.localized()
            threadDescription.textColor = AppColors.grayColor
        }
    }
    @IBOutlet weak var threadTitleField: RAGTextField!{
        didSet{
            AppConfig.shared.setunderLine(for: threadTitleField)
        }
    }
    
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2
            profileImageView.clipsToBounds = true
            profileImageView.layer.borderWidth = 0.5
            profileImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var groupsView: UIView!{
        didSet{
            groupsView.isUserInteractionEnabled = true
            groupsView.showViewBorders(width: 1)
            groupsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectGroup)))
        }
    }
    @IBOutlet weak var folderView: UIView!{
        didSet{
            folderView.isUserInteractionEnabled = true
            folderView.showViewBorders(width: 1)
            folderView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectGroup)))
        }
    }
    @IBOutlet weak var permissionsView: UIView!{
        didSet{
            permissionsView.showViewBorders(width: 1)
            permissionsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectPermissions)))
        }
    }
    
    //MARK: - Properties
    var categoryId = 0
    var groupTitle = ""
    var createOrEditThreadFrom: CreateOrEditThreadFrom = .none
    var files = [Attachment]()
    var threadId : Int!
    var threadToEdit: ThreadModel?
    var attachements = [FileModel]() {
        didSet{
            DispatchQueue.main.async {
                self.attachmentCollectionView.reloadData()
            }
        }
    }
    var didAnyUpdate = false //like, delete
//    var fileIds = [Int]()
//    var CategoryTitle = ""
    var emailNotify = ""
    var priority : Priority = .none
    var addFilePermissions = "all"
    var threadDescriptionHere = ""
    var permittedMembers = "all"
    var formMode: FormMode = .new
    var createThreadWith: AttachmentOption = .none
    let videoFileName = "/od_video.mov"
    
//    var isFromGroupInfoScreen = false
//    var cameToCreateThreadVCFromGroupInfoScreen = false
    var pdfView : PDFView!
//    var folderName = ""
//    var folderID = 0
    
//    var attachmentsOnEdit = [Attachment]()
    
    var groupId = 0
    var myGroups = [GroupModel]() {
        didSet{
            if createOrEditThreadFrom == .group || formMode == .edit {
                self.groupTitleLabel.text = groupTitle
                self.categoryTitleLabel.text = ""
                self.groupsView.isUserInteractionEnabled = false
                self.groupsView.alpha = 0.35
                
                if let threadToEdit = self.threadToEdit {
                    self.groupTitleLabel.text = threadToEdit.clinic_name
                    self.categoryTitleLabel.text = threadToEdit.clinic_category_name
                    self.categoryId = threadToEdit.clinic_category_id!
//                    self.folderView.isUserInteractionEnabled = false
//                    self.folderView.alpha = 0.35
                }
                else {
                    for item in self.myGroups {
                        if item.id == self.groupId{
                            for cat in item.clinic_categories! {
                                if self.categoryId == 0 && cat.name!.lowercased().contains("standard"){
                                    self.categoryId = cat.id!
                                    self.categoryTitleLabel.text = cat.name
                                }
                                else if cat.id == self.categoryId {
                                    self.categoryId = cat.id!
                                    self.categoryTitleLabel.text = cat.name
                                }
                            }
                            if self.categoryId == 0, let category = item.clinic_categories?.first {
                                self.categoryId = category.id!
                                self.categoryTitleLabel.text = category.name
                            }
                        }
                    }
                }
            }
            else{
                self.groupTitleLabel.text = ""
                self.categoryTitleLabel.text = ""
            }
        }
    }
    
    var showOnDashboard: Bool = false
//    var directUploadInfo = [DirectUploadModel]()
//    var imageData = [Data]()
    
    var directUploads = [Uploads](){
        didSet{
            DispatchQueue.main.async {
                self.attachmentCollectionView.reloadData()
            }
        }
    }
    
    //MARK: - Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUserGroup {_ in
//            if self.formMode != .edit {
                self.defaultSettings()
//            }
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTap)))
        self.keyboardSetting()
        
        if self.formMode == .edit {
            self.navigationItem.title = ThreadEditHeader.localized().uppercased()
            AppConfig.shared.resetGroupSelection()
            self.createThreadButton.setTitle(SaveTitle.localized(), for: .normal)
//            self.loadThread()
            if let thread = self.threadToEdit {
                self.setData(thread)
            }
        }else {
//            self.defaultSettings()
            self.navigationItem.title = ThreadCreateHeader.localized().uppercased()
            self.createThreadButton.setTitle(create.localized(), for: .normal)
        }
        if AppConfig.shared.myGroupsWithCategories.count > 0 {
            AppConfig.shared.resetGroupSelection()
        }
        
        self.setup()
        self.checkShareAttachment()
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: CreateThreadVC.setTheme)
    }
    
    private func checkShareAttachment() {
        if appDelegate.attachements.count > 0 {
            if appDelegate.attachements.count == 1 && appDelegate.attachements.first!.file_main_type == "web_url" {
                self.threadDescription.text = appDelegate.attachements.first!.FileName
            }
            else {
                for item in appDelegate.attachements {
                    var directUploadParams = DirectUploadParams()
                    directUploadParams.filename = item.FileName
                    directUploadParams.content_type = MimeType(ext: item.FileName.fileExtension())
                    directUploadParams.byte_size = item.base64String.count
                    
        //            print("CreateThreadVC->checkShareAttachment", directUploadParams)
                    var thumbnail: UIImage?
                    if item.file_main_type == "image" {
//                        if let decodedData = Data(base64Encoded: item.base64String, options: .ignoreUnknownCharacters) {
                            thumbnail = UIImage(data: item.base64String)
//                        }
                    }
                    self.uploadForSignedId(directUploadParams, item.base64String, thumbnail)
                }
            }
            appDelegate.attachements.removeAll()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
        self.createThreadWith = .none
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        self.viewDidAppear(animated)
        self.threadDescription.delegate = self
        self.createThreadWith = .none
    }
    
    deinit {
        print("CreateThreadVC->deinit")
    }
    
    func keyboardSetting(){
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillShowNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillHideNotification,object: nil)
    }
    
    @objc func keyboardNotification(_ notification: NSNotification) {
        let isShowing = notification.name == UIResponder.keyboardWillShowNotification

        var tabbarHeight: CGFloat = 0
        if self.tabBarController != nil, !self.tabBarController!.tabBar.isHidden {
            tabbarHeight = self.tabBarController!.tabBar.frame.height
        }
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.intValue ?? Int(UIView.AnimationOptions.curveEaseInOut.rawValue)
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: UInt(animationCurveRaw))
            self.keyboardHeightLayoutConstraint?.constant = isShowing ? (endFrame!.size.height - tabbarHeight) : 0
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    func defaultSettings(){
        if  (AppConfig.shared.myGroupsWithCategories.count > 0) {
            self.myGroups = AppConfig.shared.myGroupsWithCategories.reversed()
        }
    }
    
    //MARK: - Methods
    func setup(){
        if let name = AppConfig.shared.user.name {
            userNameLabel.text = name
        }
        let imgUrl = AppConfig.shared.user.file?.medium_url ?? AppConfig.shared.user.file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            let url = URL(string: imageUrl)
            profileImageView.imageFromServer(imageUrl: url!)
        }
        
        if self.createThreadWith != .none {
            self.showPreSelection()
        }
    }
    
    func showPreSelection(){
        self.didTapSelection(option: self.createThreadWith)
    }
    
    func resetView(){
        self.groupId = 0
        self.categoryId = 0
        self.threadId = nil
        self.permittedMembers = "all"
        self.addFilePermissions = "all"
        self.createThreadWith = .none
        self.priority = .none
        self.emailNotify = "N"
        self.threadTitleField.text = ""
        self.threadDescription.delegate = self
        self.threadDescription.textColor = .gray
        self.threadDescription.text = ThreadDetailPlaceholder.localized()
        self.showOnDashboard = false
        
        AppConfig.shared.resetGroupSelection()
        self.myGroups[0].isSelected = true
        self.myGroups[0].clinic_categories![0].isSelected = true
        
        self.defaultSettings()
    }
    
    func setData(_ data: ThreadModel) {
        self.threadTitleField.text = nil
        self.threadTitleField.placeholder = nil
        self.threadTitleField.attributedText = data.title?.attributedText(fontSize: 14, allStyleFont: AppFont.kMuliLight(14).font())?.attributedString
        self.threadDescription.attributedText = data.description?.attributedText(fontSize: 14, allStyleFont: AppFont.kMuliLight(14).font())?.attributedString
        if let description = data.description, let files = data.doc_files, !files.isEmpty, description.contains(files[0].name ?? "") {
            self.threadDescription.textColor = .gray
            self.threadDescription.text = ThreadDetailPlaceholder.localized()
        }
        if let priority = data.priority {
            switch priority {
            case "normal":
                self.priority = .normal
                break
            case "critical":
                self.priority = .critical
                break
            case "important":
                self.priority = .important
                break
            case "notice":
                self.priority = .notice
                break
            default:
                self.priority = .none
            }
        }
        self.attachements = data.doc_files ?? []
    }
    
    func showScannedFileRenamePopUp(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: "Rename Scanned File".localized(), for: "Do you want to rename scanned file?".localized(), firstBtnStyle: .default, secondBtnStyle: .default, firstBtnTitle: yesText.localized(), secondBtnTitle: noText.localized()) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                self.showSaveDialog(params, data, thumbNail)
            } else {
                self.uploadForSignedId(params, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(params, data, thumbNail)
//                }
            }
        }
    }
    
    func showSaveDialog(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?) {
        self.showAlert(To: saveDocuments.localized(), for: "Enter document name".localized(), tfPlaceholder: String(Date().getTime())) { [weak self] (status, text) in
            guard let `self` = self else {return}
            if (text == nil || text!.isEmpty) {
                self.uploadForSignedId(params, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(file)
//                }
            }
            else {
                var info = params
                info.filename = text!+".pdf"
                self.uploadForSignedId(info, data, thumbNail)
//                self.attachements.append(file)
//                if self.formMode == .edit{
//                    self.attachmentsOnEdit.append(file)
//                }
            }
        }
    }
    
    //MARK: - Selector Methods
    @objc func Done() {
        self.view.endEditing(true)
        self.validate()
    }
    
    @objc func viewTap(){
        self.view.endEditing(true)
    }
    
    @objc func selectGroup(){
        self.priority = .none
        self.performSegue(withIdentifier: "SegueToSelectGroupCategory", sender: self)
    }
    
    @objc func selectPermissions(){
        if (groupTitleLabel.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? true || categoryTitleLabel.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? true) {
            self.showAlert(To: "", for: selectGroupFirst) {_ in}
        }
        else if self.groupId == publicGroupID {
            self.showAlert(To: "", for: priorityNotAllowedForCommunity) {_ in}
        }
        else {
            self.performSegue(withIdentifier: "ThreadSettingVC", sender: self)
        }
    }
    
    //MARK: - IBActions
    @IBAction func addAttachment(){
        self.showDialogAlert(options: .camera, .library, .video, .docs, .scan)
    }
    
    @IBAction func didTapCreateThreadButton(_ sender: UIButton) {
        self.view.endEditing(true)
        self.validate()
    }
    
    func validate() {
        if (groupTitleLabel.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? true || categoryTitleLabel.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? true) {
            self.showAlert(To: groupAndCategoryAlert, for: groupAndCategorySelectionMessage) { [weak self] _ in
                guard let `self` = self else {return}
                self.selectGroup()
            }
        }else if ((self.formMode != .edit) && (threadDescription.text.trimmingCharacters(in: .whitespaces).isEmpty || threadDescription.text.localized() == ThreadDetailPlaceholder.localized()) && (self.directUploads.count == 0)){
            self.showAlert(To: ThreadCreateTitle, for: ThreadCreateMessage) {_ in}
        }else if ((self.formMode == .edit) && (threadDescription.text.trimmingCharacters(in: .whitespaces).isEmpty || threadDescription.text.localized() == ThreadDetailPlaceholder.localized()) && self.directUploads.count == 0 && self.attachements.count == 0){
            self.showAlert(To: ThreadCreateTitle, for: ThreadCreateMessage) {_ in}
        }
        else {
            self.createOrEditThread()
        }
    }
    
    func createOrEditThread() {
        var params = ThreadParamsModel()
        if let title = self.threadTitleField.text {
            params.title = title.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if let desc = self.threadDescription.text, desc != ThreadDetailPlaceholder.localized(){
            params.description  = desc.trimmingCharacters(in: .whitespacesAndNewlines)
        }else{
            params.description = ""
        }
        params.clinic_id = self.groupId
        
        if params.description == "" {
            if self.formMode == .edit && !self.attachements.isEmpty {
                let fileCount = self.attachements.count + self.directUploads.count
                if fileCount > 1 {
                    params.description = "\(self.attachements[0].name!) + \(fileCount-1) Files"
                }else{
                    params.description = self.attachements[0].name!
                }
            }
            else if self.directUploads.count > 0 {
                if self.directUploads.count > 1 {
                    params.description = "\(self.directUploads[0].directUploadInfo.filename!) + \(self.directUploads.count-1) Files"
                }else{
                    params.description = self.directUploads[0].directUploadInfo.filename!
                }
            }
        }
        
        params.clinic_category_id = self.categoryId
        params.priority = self.priority.rawValue
        params.posted_from = "mobile"
        params.doc_type = "thread"
        
        if !directUploads.isEmpty {
            var progressView: ProgressView?
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else {return}
                progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
                progressView!.lblTitle.text = "txt_progress_title".localized()
                progressView!.lblDetail.text = "txt_progress_detail".localized()
                progressView!.uiProgress.progress = 0
                progressView!.tag = 100
                progressView!.lblCount.text = "1/\(self.directUploads.count)"
                self.addView(view: progressView!)
            }

            let dispatchQueue = DispatchQueue(label: "FileUploadingQueue", qos: .background)
            let semaphore = DispatchSemaphore(value: 0)

            dispatchQueue.async {
                for (index, item) in self.directUploads.enumerated() {
                    RequestManager.shared.directUploadImage(data: item.data, params: item.directUploadInfo) { [weak self] (percent) in
                        guard let `self` = self else {return}
                        progressView?.lblCount.text = "\(index+1)/\(self.directUploads.count)"
                        progressView?.uiProgress.progress = Float(percent)
                    } completion: { [weak self] (status, response) in
                        guard let `self` = self else {return}
                        if status {
                            params.files.append(item.directUploadInfo.signed_id!)
                            if params.files.count == self.directUploads.count {
                                if let progressView = progressView {
                                    self.removeView(tag: progressView.tag)
                                }
                                self.proceedToCreateThread(&params)
                            }
                        }
                        else {
                            if let progressView = progressView {
                                self.removeView(tag: progressView.tag)
                            }
                            self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                        }
                        semaphore.signal()
                    }
                    semaphore.wait()
                }
            }
        } else {
            self.proceedToCreateThread(&params)
        }
    }
    
    private func proceedToCreateThread(_ params: inout ThreadParamsModel){
        if self.formMode == .edit {
            params.threadId = self.threadToEdit?.id
            params.files = self.attachements.compactMap({ $0.signed_id }) + params.files
        }
        RequestManager.shared.createOrEditThread(params, isEdit: self.formMode == .edit) { [weak self] (status, response) in
            guard let `self` = self else {return}
            if status{
                self.directUploads.removeAll()
                if let data = response as? ThreadModel {
                    NotificationCenter.default.post(name: .notifyThread, object: nil, userInfo: ["Thread": data, "ShowOnDashboard": self.showOnDashboard, "Notify": self.formMode == .edit ? NotifyCRUD.update : NotifyCRUD.create])
                    
                    
                    if self.formMode == .new, self.createOrEditThreadFrom == .home {
                        if let vc = self.getVC(storyboard: .TABS, vcIdentifier: "GroupInfoScene") as? GroupInfoVC {
                            vc.groupById = self.groupId
                            self.resetView()
                            self.dismissVC()
                            self.navigateVC(vc)
                        }
                    }
                    else {
                        self.resetView()
                        self.dismissVC()
                    }
                }
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier ==  "SegueToSelectGroupCategory"{
            let dvc = segue.destination as? SelectGroupVC
            if self.formMode == .edit{
                dvc?.groupId = self.groupId
                dvc?.catId = self.categoryId
            }
            if self.groupId != 0{
                dvc?.groupId = self.groupId
                dvc?.catId = self.categoryId
            }
            
            if (createOrEditThreadFrom == .group) {
                dvc?.groupId = self.groupId
                dvc?.catId = self.categoryId
            }
            dvc?.formMode = self.formMode
            dvc?.fromScreen = self.createOrEditThreadFrom
            dvc?.delegate = self
            dvc?.createThreadVCFolderViewTag = folderView.tag
            if self.formMode != .edit{
                self.threadId = nil
            }
            self.view.endEditing(true)
        }
        else if segue.identifier == "ThreadSettingVC"{
            let vc = segue.destination as? ThreadSettingVC
            vc?.delegate = self
            vc?.priority = self.priority
            vc?.emailNotify = self.emailNotify
            vc?.addFilePermissions = self.addFilePermissions
        }
    }
    
    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
        if let theError = error {
            print("error saving the video = \(theError)")
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
            })
        }
    }
    
    private func uploadForSignedId(_ params: DirectUploadParams, _ data: Data, _ thumbNail: UIImage?){
        RequestManager.shared.directUpload(_params: params, data: data) { (status, result) in
            if status {
                if let info = result as? DirectUploadModel {
                    let upload = Uploads(directUploadInfo: info, data: data, image: thumbNail)
                    self.directUploads.append(upload)
                }
            }
        }
    }
    
    private func deleteFile(index: IndexPath) {
        self.showAlert(To: "", for: deleteAlertMessage.localized(), firstBtnStyle: .destructive, secondBtnStyle: .default) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                let signedId = self.attachements[index.row].signed_id
                guard let id = signedId else { return }
                RequestManager.shared.deleteFile(signedId: id) { (status, response) in
                    if status {
                        if let _ = response as? GeneralSuccessResult {
                            self.attachements.remove(at: index.row)
                            self.attachmentCollectionView.deleteItems(at: [index])
                            self.didAnyUpdate = true
                            self.showAlert(To: "", for: successfullyDeleted.localized()) { [weak self] _ in
                                guard let `self` = self else {return}
                                self.view.endEditing(true)
                            }
                        }
                        else {
                            print("Error in Deleting File")
                        }
                    }
                }
            }
        }
    }
}

extension CreateThreadVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.attachements.count + self.directUploads.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? AttachmentCell else{ return emptyCVCell() }
        if indexPath.row < self.attachements.count {
            let item = self.attachements[indexPath.item]
            if item.content_type!.contains("image"), let imgPath = item.src, imgPath != "" {
                if let imgUrl = URL(string: imgPath){
                    cell.cellImageView.imageFromServer(imageUrl: imgUrl, placeholder: #imageLiteral(resourceName: "attached"))
                }else {
                    cell.cellImageView.image = #imageLiteral(resourceName: "attached")
                }
            }else {
                cell.cellImageView.image = #imageLiteral(resourceName: "attached")
            }
        }
        else {
            let attachment = directUploads[indexPath.item - self.attachements.count]
            if attachment.directUploadInfo.content_type!.contains("image") || attachment.directUploadInfo.content_type!.contains("video") {
                cell.cellImageView.image = attachment.image ?? (attachment.directUploadInfo.content_type!.contains("image") ? #imageLiteral(resourceName: "defaultImage") : #imageLiteral(resourceName: "ic_video"))
            }else{
                cell.cellImageView.image = #imageLiteral(resourceName: "attached")
            }
        }
        cell.index = indexPath
        cell.delegate = self
        return cell
    }
}

//MARK: - Extensions
extension CreateThreadVC {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let _capturedImage = info[.originalImage] as? UIImage {
            picker.dismiss(animated: true, completion: nil)
            let capturedImage = self.imageOrientation(_capturedImage)
//            self.profileImageView.image = capturedImage
            
            var fileName = UUID().uuidString + ".jpeg"
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                fileName = url.lastPathComponent
    //          fileType = url.pathExtension
            }
            
            let _imageData = capturedImage.jpegData(compressionQuality: capturedImage.size.width > 500 ? 0.5 : 1.0) ?? Data(capturedImage.sd_imageData()!)
            let imageSize: Int = _imageData.count
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //_imageData.fileExtension
            directUploadParams.byte_size = imageSize
            
//            print("CreateThreadVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, _imageData, capturedImage)
        }
        else if let selectedVideo:URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) {
            // Save video to the main photo album
            let selectorToCall = #selector(self.videoSaved(_:didFinishSavingWithError:context:))
            
            UISaveVideoAtPathToSavedPhotosAlbum(selectedVideo.relativePath, self, selectorToCall, nil)
            // Save the video to the app directory
            var _videoData = Data()
            let videoData = try? Data(contentsOf: selectedVideo)
            let paths = NSSearchPathForDirectoriesInDomains(
                FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
            let dataPath = documentsDirectory.appendingPathComponent(videoFileName)
            
            try! videoData?.write(to: dataPath, options: [])
            
            do {
                let data = try Data(contentsOf: dataPath, options: .mappedIfSafe)
                _videoData = data
            } catch  {
                
            }
            let thumbNail = dataPath.generateThumbnail()
            let fileName = selectedVideo.lastPathComponent
            
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = _videoData.count
            
//            print("CreateThreadVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, _videoData, thumbNail)
        }
        self.createThreadWith = .none
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.createThreadWith = .none
        self.dismiss(animated: true, completion: nil)
    }
}

extension CreateThreadVC {
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        print("CreateThreadVC->imagePickerDidCancel")
    }
    
    internal func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        for assetItem in assets {
            AppConfig.shared.getURLFromAsset(mPhasset: assetItem) { (url) in
                if url != nil {
                    var fileName = url!.lastPathComponent
                    var fileData = Data()
                    do {
                        let data = try Data(contentsOf: url!, options: .mappedIfSafe)
                        fileData = data
                        if fileName.fileExtension().uppercased() == "HEIC" {
                            if let dataToImage = UIImage(data: data) {
                                fileName = String(fileName.split(separator: ".").first!).appending(".jpeg")
                                fileData = self.imageOrientation(dataToImage).jpegData(compressionQuality: dataToImage.size.width > 500 ? 0.5 : 1.0) ?? data
                            }
                        }
                    } catch {
                        print("Unable to get data of the file")
                    }
                    var thumbNail: UIImage?
                    if assetItem.mediaType == PHAssetMediaType.image {
                        if let image = AppConfig.shared.getImageFromAsset(asset: assetItem) {
                            thumbNail = image
                        }
                    }
                    else {
                        thumbNail = url!.generateThumbnail()
                    }
                    var directUploadParams = DirectUploadParams()
                    directUploadParams.filename = fileName
                    directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                    directUploadParams.byte_size = fileData.count
                    
                    self.uploadForSignedId(directUploadParams, fileData, thumbNail)
                }
                else {
                    if let img = AppConfig.shared.getAssetThumbnail(asset: assetItem) {
                        let fileName = UUID().uuidString + ".jpeg"
                        let fileData = img.jpegData(compressionQuality: img.size.width > 500 ? 0.5 : 1.0)
                        if let fileData = fileData {
                            var directUploadParams = DirectUploadParams()
                            directUploadParams.filename = fileName
                            directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                            directUploadParams.byte_size = fileData.count
                            
                            self.uploadForSignedId(directUploadParams, fileData, img)
                        }
                    }
                }
            }
        }
        self.createThreadWith = .none
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 0
    }
    
    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
        return URL(string: "https://placeimg.com/500/500/nature")
    }
}

//UIDocumentBrowserViewController
extension CreateThreadVC {
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentsAt documentURLs: [URL]) {
        controller.dismiss(animated: true, completion: nil)
        guard documentURLs.count < maxAllowedAttachments else {
            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
            return
        }
        for url in documentURLs {
            let fileName = url.lastPathComponent
            var fileData = Data()
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                fileData = data
            } catch  {
                print("Unable to get data of the file")
            }
            var directUploadParams = DirectUploadParams()
            directUploadParams.filename = fileName
            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
            directUploadParams.byte_size = fileData.count
            
//            print("CreateThreadVC->directUploadParams", directUploadParams)
            self.uploadForSignedId(directUploadParams, fileData, nil)
        }
        self.createThreadWith = .none
    }
    
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
//        guard urls.count < maxAllowedAttachments else {
//            self.showAlert(To: "Alert".localized(), for: MaxAttachmentMessage.localized()){_ in}
//            return
//        }
//        for url in urls {
//            let fileName = url.lastPathComponent
//            var fileData = Data()
//            do {
//                let data = try Data(contentsOf: url, options: .mappedIfSafe)
//                fileData = data
//            } catch  {
//                print("Unable to get data of the file")
//            }
//            var directUploadParams = DirectUploadParams()
//            directUploadParams.filename = fileName
//            directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//            directUploadParams.byte_size = fileData.count
//
////            print("CreateThreadVC->directUploadParams", directUploadParams)
//            self.uploadForSignedId(directUploadParams, fileData, nil)
//        }
//        self.createThreadWith = .none
//    }
//
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
//        let fileName = url.lastPathComponent
//        var fileData = Data()
//        do {
//            let data = try Data(contentsOf: url, options: .mappedIfSafe)
//            fileData = data
//        } catch  {
//            print("Unable to get data of the file")
//        }
//        var directUploadParams = DirectUploadParams()
//        directUploadParams.filename = fileName
//        directUploadParams.content_type = MimeType(ext: fileName.fileExtension()) //"video/" + String(selectedVideo.lastPathComponent.split(separator: ".").last!)
//        directUploadParams.byte_size = fileData.count
//
////        print("CreateThreadVC->directUploadParams", directUploadParams)
//        self.uploadForSignedId(directUploadParams, fileData, nil)
//        self.createThreadWith = .none
//    }
//
//    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//        // Picker was cancelled! Duh 🤷🏻‍♀️
//        self.createThreadWith = .none
//        self.dismiss(animated: true, completion: nil)
//    }
}

extension CreateThreadVC: SelectGroupViewControllerDelegate{
    func didSelectGroup(status: Bool) {
        self.permissionsView.isUserInteractionEnabled = true
        self.attachmentButton.isEnabled = true
        let group = AppConfig.shared.myGroupsWithCategories.filter{$0.isSelected == true}
        if group.count > 0 {
            let category = group.first!.clinic_categories!.filter{$0.isSelected}
            if let title = group.first!.name{
                self.groupTitleLabel.text = title
            }
            if let categoryTitle = category.first!.name, let catId = category.first!.id{
                self.categoryTitleLabel.text = categoryTitle
                self.categoryId = catId
            }
            if let id = group.first!.id{
                self.groupId = id
                print("CreateThreadVC->didSelectGroup", self.groupId)
            }
            if let showOnDashboard = group.first!.is_display_timeline {
                self.showOnDashboard = showOnDashboard
            }
        }
    }
}

extension CreateThreadVC: SelectPermissionsViewControllerDelegate{
    func didSelectPermissions(status: Bool, permission: String, members: String, emailNotification: String, priority: Priority) {
        self.priority = priority
        self.emailNotify = emailNotification
        self.permittedMembers = members
        self.addFilePermissions = permission
    }
}

extension CreateThreadVC: ThreadSettingVCDelegate {
    func didSelect(permission: String, emailNotification: String, priority: Priority) {
        self.priority = priority
        self.emailNotify = emailNotification
        self.addFilePermissions = permission
    }
}

//MARK: - UITextFieldDelegate
extension CreateThreadVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // get the current text, or use an empty string if that failed
        var currentText = textField.text
        if (textField == threadTitleField) {
            currentText = textField.text ?? ""
        }
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText ?? "") else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText?.replacingCharacters(in: stringRange, with: string)
        
        // make sure the result is under 30 characters
        return updatedText?.count ?? 0 <= 30
    }
}

//MARK: - UITextViewDelegate
extension CreateThreadVC : UITextViewDelegate{
    func textViewShouldReturn(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
//        textView.text = textView.text.lowercased() == ThreadDetailPlaceholder.localized().lowercased() ? nil : textView.text
//        textView.textColor = textView.textColor == UIColor.lightGray ? UIColor.white : UIColor.lightGray
//        textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
        if let txt = textView.text, txt == ThreadDetailPlaceholder.localized(){
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
            textView.text = nil
        }else{
            textView.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        if textView.text.isEmpty {
            textView.text = ThreadDetailPlaceholder.localized()
            textView.textColor = AppColors.grayColor
        }
        return true
    }
}

extension CreateThreadVC : AttachmentCellDelegate {
    func didSelectToRemove(index: IndexPath) {
        if index.row < self.attachements.count {
            self.deleteFile(index: index)
        }
        else {
            let row = index.item - self.attachements.count
            if row < directUploads.count {
                self.directUploads.remove(at: row)
                self.attachmentCollectionView.deleteItems(at: [index])
            }
        }
//        if self.formMode == .edit{
//            if self.attachmentsOnEdit.indices.contains(at){
//                if self.attachements.count > 0 {
//                    let index = self.attachements.count - at
//                    if self.attachmentsOnEdit.indices.contains(index){
//                        self.attachmentsOnEdit.remove(at: index)
//                    }else{
//                        self.attachmentsOnEdit.remove(at: 0)
//                    }
//                }else{
//                    self.attachmentsOnEdit.remove(at: at)
//                }
//            }
//        }
    }
}

//VNDocumentCameraViewControllerDelegate
extension CreateThreadVC {
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        print(error)
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        self.createThreadWith = .none
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        guard scan.pageCount >= 1 else {
            controller.dismiss(animated: true)
            return
        }
        DispatchQueue.main.async {
            let pdfDocument = PDFDocument()
            for i in 0 ..< scan.pageCount {
                if  let image = scan.imageOfPage(at: i).resize(toWidth: 700) {
                    print(image)
                    // Create a PDF page instance from your image
                    let pdfPage = PDFPage(image: image)
                    // Insert the PDF page into your document
                    pdfDocument.insert(pdfPage!, at: i)
                }
            }
            // Get the raw data of your PDF document
            let data = pdfDocument.dataRepresentation()
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let docURL = documentDirectory.appendingPathComponent(Date().getTime()+".pdf")
            do{
                try data?.write(to: docURL)
                
                let fileName = docURL.lastPathComponent
                var directUploadParams = DirectUploadParams()
                directUploadParams.filename = fileName
                directUploadParams.content_type = MimeType(ext: fileName.fileExtension())
                directUploadParams.byte_size = data!.count
                
//                print("CreateThreadVC->directUploadParams", directUploadParams)
//                self.uploadForSignedId(directUploadParams, data!, nil)
                self.showScannedFileRenamePopUp(directUploadParams, data!, nil)
            }catch(let error)
            {
                print("error is \(error.localizedDescription)")
            }
        }
        controller.dismiss(animated: true)
    }
}

//MARK: - Custom Data Types
struct ThreadParamsModel {
    var threadId: Int! // need when edit thread
    var clinic_id: Int!
    var clinic_category_id: Int!
    var doc_type: String! = "thread"
    var priority: String!
    var title: String! = ""
    var description: String! = ""
    var posted_from: String = "mobile"
    var files = [String]()
}

extension CreateThreadVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        threadTitleField.textColor = theme.settings.titleColor
        threadDescription.textColor = theme.settings.titleColor
        threadDescription.tintColor = theme.settings.titleColor
    }
}
