//
//  SelectGroupVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox

protocol SelectGroupViewControllerDelegate: AnyObject {
    func didSelectGroup(status: Bool)
}

class SelectGroupVC: BaseVC {
    
    @IBOutlet weak var selectionListTableView: UITableView!{
        didSet{
            selectionListTableView.register(UINib(nibName: "GeneralViewCell", bundle: Bundle.main), forCellReuseIdentifier: "GeneralCell")
        }
    }
    weak var delegate: SelectGroupViewControllerDelegate!
    var myGroups = [GroupModel]()
    private var selectedSection: Int?
    
    var groupId = 0
    var catId = 0
    var formMode: FormMode = .new
    var fromScreen: CreateOrEditThreadFrom = .detail
    var editingGroupIsMarked = false
//    var cameToCreateThreadVCFromGroupInfoScreen = false
    var createThreadVCFolderViewTag = 0
    
    //for advance search at home
    var searchParams: TimelineParams?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.loadGroups()
        self.layOutNavBar()
    }
    
    deinit {
        print("SelectGroupVC->deinit")
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        if (cameToCreateThreadVCFromGroupInfoScreen) {
//            self.selectionListTableView.isUserInteractionEnabled = false
//        }
        
        if (createThreadVCFolderViewTag == 5) {
            self.selectionListTableView.isUserInteractionEnabled = true
        }
    }
    
    func layOutNavBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: SaveText.localized(), style: .plain, target: self, action: #selector(Done))
    }
    
    func loadGroups(){
        if AppConfig.shared.myGroupsWithCategories.count > 0 {
            self.myGroups = AppConfig.shared.myGroupsWithCategories //.filter{($0.is_display_timeline ?? true)}
            if fromScreen == .group {
                self.myGroups = self.myGroups.filter{$0.id == self.groupId}
                self.selectedSection = 0
            }
            for (index, item) in self.myGroups.enumerated() {
                if let params = self.searchParams {
                    params.clinic_ids.forEach { id in
                        if item.id == id {
                            item.isSelected = true
                        }
                        params.category_ids.forEach { id in
                            item.clinic_categories?.first(where: { $0.id == id })?.isSelected = true
                        }
                    }
                }
                else {
                    if item.id == self.groupId{
                        self.selectedSection = index
                        self.myGroups[index].isSelected = true
                        for cat in item.clinic_categories!{
                            if cat.id == self.catId{
                                cat.isSelected = true
                            }
                        }
                    }
                }
            }
            self.selectionListTableView.reloadData()
        }else{
            groupAndCategories()
        }
        if self.formMode == .edit{
            self.selectionListTableView.reloadData()
        }
    }
    
    func groupAndCategories() {
        self.loadUserGroup { (status) in
            if status {
                let groups = AppConfig.shared.myGroupsWithCategories //.filter{($0.is_display_timeline ?? true)}
                self.myGroups = groups.reversed()
                if self.formMode == .new {
                    if self.myGroups.count > 0 {
                        if self.searchParams == nil {
                            self.myGroups[0].isSelected = true
                            self.myGroups[0].clinic_categories?[0].isSelected = true
                        }
                        AppConfig.shared.myGroupsWithCategories = AppConfig.shared.myGroupsWithCategories.reversed()
                        self.selectionListTableView.reloadData()
                    }
                }else{
                    for (i,item) in self.myGroups.enumerated(){
                        if item.id == self.groupId{
                            self.selectedSection = i
                            self.myGroups[i].isSelected = true
                            for cat in item.clinic_categories! {
                                if cat.id == self.catId {
                                    cat.isSelected = true
                                }
                            }
                        }
                    }
                    self.selectionListTableView.reloadData()
                }
            }
        }
    }
    
    @objc func Done(){
        delegate?.didSelectGroup(status: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func sectionTap(_ sender:UITapGestureRecognizer){
        if self.searchParams == nil {
            for group in self.myGroups{
                group.isSelected = false
                for cat in group.clinic_categories!{
                    cat.isSelected = false
                }
            }
        }
        if let tag = sender.view?.tag{
            self.selectedSection = tag
            self.myGroups[tag].isSelected = !self.myGroups[tag].isSelected
            if self.searchParams == nil {
                if let categories = self.myGroups[tag].clinic_categories, categories.count > 0 {
                    self.myGroups[tag].clinic_categories![0].isSelected = self.myGroups[tag].isSelected
                }
            }
            else {
                if let categories = self.myGroups[tag].clinic_categories {
                    if self.myGroups[tag].isSelected {
                        _ = categories.compactMap({$0.isSelected = true})
                    }
                    else {
                        _ = categories.compactMap({$0.isSelected = false})
                    }
                }
            }
        }
        self.selectionListTableView.reloadData()
    }
    
    private func showNewFolderDialog() {
        self.showAlert(To: "", for: "txt_enter_folder_name".localized(), tfPlaceholder: "txt_folder_name".localized(), isSecure: false, buttons: 2,
                       firstBtnStyle: .default, secondBtnStyle: .cancel,
                       firstBtnTitle: create.localized(), secondBtnTitle: cancelText.localized()) { [weak self] (status, text) in
            guard let `self` = self else {return}
            if status {
                if (text == nil || text!.isEmpty) {}
                else {
                    self.addCategory(catName: text!.trimmingCharacters(in: .whitespacesAndNewlines))
                }
            }
        }
    }
    
    private func addCategory(catName: String) {
        if let _selectedSection = self.selectedSection, let clinicId = self.myGroups[_selectedSection].id {
            let sortOrder = (self.myGroups[_selectedSection].clinic_categories?.map{ ($0.sort_order ?? 0) }.max() ?? 0) + 1
            RequestManager.shared.addCategory(clinicId: clinicId, categoryName: catName, sortOrder: sortOrder) { [weak self] (status, response) in
                guard let `self` = self else {return}
                if status, let response = response as? GroupCategoryBaseModel, let data = response.data {
                    if let _ = self.myGroups[_selectedSection].clinic_categories {
                        self.myGroups[_selectedSection].clinic_categories!.append(data)
                    }
                    else {
                        self.myGroups[_selectedSection].clinic_categories = [data]
                    }
                    self.selectionListTableView.reloadData()
                }
            }
        }
    }
}

extension SelectGroupVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.myGroups[section].isSelected{
            return self.myGroups[section].clinic_categories!.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell", for: indexPath) as? GeneralViewCell else{
            return GeneralViewCell()
        }
        cell.setCellData(indexPath: indexPath, data: self.myGroups[indexPath.section].clinic_categories![indexPath.row])
        cell.cellImageView.image = nil
        cell.cellImageView.layer.borderColor = AppColors.clear.cgColor
        cell.contentView.sendSubviewToBack(cell.cellCheckBox)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.searchParams == nil {
            for (i,item) in self.myGroups[indexPath.section].clinic_categories!.enumerated(){
                if i != indexPath.row{
                    item.isSelected = false
                }
            }
        }
        if self.myGroups[indexPath.section].clinic_categories![indexPath.row].isSelected{
            self.myGroups[indexPath.section].clinic_categories![indexPath.row].isSelected = false
            self.myGroups[indexPath.section].clinic_categories![0].isSelected = true
        }else{
            self.myGroups[indexPath.section].clinic_categories![indexPath.row].isSelected = true
        }
        self.selectionListTableView.reloadData()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.myGroups.count > 0{
            tableView.setEmptyMessage("")
            return self.myGroups.count
        }else{
            tableView.setEmptyMessage(noGroupText.localized())
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let admins = self.myGroups[section].clinic_admins, let id = AppConfig.shared.user.id, admins.contains(where: { $0.id == id }),
            self.myGroups[section].isSelected {
            return 100
        }
        return 55
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.myGroups[indexPath.section].isSelected{
            return 50
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed(String(describing: FolderViewCell.self), owner: self, options: nil)?.first as? FolderViewCell
        if let title = self.myGroups[section].name, title != ""{
            headerView?.titleLabel.text = title.capitalized
        }
        headerView?.delegate = self
        headerView?.tag = section
        let imgUrl = self.myGroups[section].file?.medium_url ?? self.myGroups[section].file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            let url = URL(string: imageUrl)
            headerView?.groupImageView.imageFromServer(imageUrl: url!)
            headerView?.groupImageView.contentMode = .scaleAspectFill
        }else{
            headerView?.groupImageView.image = UIImage(named: "ic_group")
            headerView?.groupImageView.contentMode = .center
        }
        headerView?.groupImageView.layer.cornerRadius = (headerView?.groupImageView.bounds.size.height ?? 40) / 2
        headerView?.groupImageView.clipsToBounds = true
        headerView?.createButton.isHidden = true
        if let admins = self.myGroups[section].clinic_admins, let id = AppConfig.shared.user.id, admins.contains(where: { $0.id == id }),
            self.myGroups[section].isSelected {
            headerView?.createButton.isHidden = false
        }
        headerView?.counterView.isHidden = true
        headerView?.threadsCountLabel.isHidden = true
        headerView?.checkBoxView.isHidden = false
        headerView?.checkBoxView.on = self.myGroups[section].isSelected
        headerView?.createButton.setTitle("txt_create_folder".localized(), for: .normal)
        if (fromScreen == .group) {
            if !(headerView?.checkBoxView.on)! {
                headerView?.contentView.alpha = 0.3
            }
        }
        
        if (formMode == .edit) {
            if !(headerView?.checkBoxView.on)! {
                headerView?.contentView.alpha = 0.3
            }
        }
        
        if self.myGroups[section].isSelected{
            headerView?.iconImageView.image = #imageLiteral(resourceName: "carrotWhite").withRenderingMode(.alwaysTemplate)
        }else{
            headerView?.iconImageView.image = #imageLiteral(resourceName: "carrot").withRenderingMode(.alwaysTemplate)
        }
        headerView?.iconImageView.contentMode = .center
        headerView?.gestureRecognizers?.forEach(headerView!.removeGestureRecognizer)
        if fromScreen == .group {
            // Do Nothing
        }
        else {
            if self.formMode == .new{
                headerView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sectionTap)))
            }else{
                headerView?.gestureRecognizers?.forEach(headerView!.removeGestureRecognizer)
            }
        }
        return headerView
    }
    
    private func setupTBL(){
        if #available(iOS 15.0, *) {
            self.selectionListTableView.sectionHeaderTopPadding = 0.0
        }
    }
}

extension SelectGroupVC: FolderViewCellDelegate {
    func createNew() {
        self.showNewFolderDialog()
    }
}
