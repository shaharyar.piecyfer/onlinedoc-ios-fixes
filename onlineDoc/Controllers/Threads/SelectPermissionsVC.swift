//
//  SelectePermissionsViewController.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
import BEMCheckBox

protocol SelectPermissionsViewControllerDelegate: class {
    func didSelectPermissions(status: Bool, permission: String, members:String, emailNotification: String,priority: Priority)
}

class SelectPermissionsVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            if self.permittedMembers == "custom"{
                scrollView.scrollTo(direction: .bottom, animated: true)
            }
        }
    }
    @IBOutlet weak var notificationsView: UIView!{
        didSet{
            notificationsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didTapEmailNotification)))
        }
    }
    
    @IBOutlet weak var allUserView: UIView!{
        didSet{
            allUserView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectThreadPermissions)))
        }
    }
    @IBOutlet weak var onlyUserRollView: UIView!{
        didSet{
            onlyUserRollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectThreadPermissions)))
        }
    }
    @IBOutlet weak var userProfessionLabel : UILabel!
    @IBOutlet weak var container : UIView!
    @IBOutlet weak var whenHidden:NSLayoutConstraint!
    @IBOutlet weak var whenShown:NSLayoutConstraint!

    @IBOutlet weak var onlyStaffView: UIView!{
        didSet{
            onlyStaffView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectThreadPermissions)))
        }
    }
    @IBOutlet weak var onlyCitizenView: UIView!{
        didSet{
            onlyCitizenView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectThreadPermissions)))
        }
    }
    @IBOutlet weak var customUsersView: UIView!{
        didSet{
            customUsersView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectThreadPermissions)))
        }
    }
    @IBOutlet weak var emailHeader : UIView!
    
    @IBOutlet var checkBoxCollection: [BEMCheckBox]!{
        didSet{
            for check in checkBoxCollection{
                check.boxType = .square
            }
        }
    }
    @IBOutlet weak var OnlyICanButton: UIButton!
    @IBOutlet weak var allReadersCanButton: UIButton!
    @IBOutlet weak var whenMailPermission:NSLayoutConstraint!
    @IBOutlet weak var whenThreadPermission:NSLayoutConstraint!
    @IBOutlet weak var TopViewBottomToEmail: NSLayoutConstraint!
    @IBOutlet weak var TopViewBottomToPriority: NSLayoutConstraint!
    @IBOutlet weak var criticalButton: UIButton!
    @IBOutlet weak var importantButton: UIButton!
    @IBOutlet weak var noticeButton: UIButton!
    @IBOutlet weak var topView: UIView!
//    @IBOutlet var priorityButtons:
    
    
    var addFilePermissions = "only_authors"
    var isNavigating = false
    var permittedMembers = "all"
    var emailNotify = "N"
    var priority:Priority = .none {
        didSet{
            print(priority.rawValue)
        }
    }
    var groupId = 0
    var adminIds = [String]()
    var selection: SelectionType = .thread
    
    var groupMembers = [MemberModel]()
    var selectedMembers = [MemberModel]()
    var groupMembersSource = [MemberModel]()
    
    weak var delegate : SelectPermissionsViewControllerDelegate!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.allReadersCanButton.frame
        rectShape.position = self.allReadersCanButton.center
        rectShape.path = UIBezierPath(roundedRect: self.allReadersCanButton.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 6, height: 6)).cgPath
        if self.addFilePermissions == "all"{
            self.allReadersCanButton.layer.backgroundColor = AppColors.setActive.cgColor
            
        }else{
            self.allReadersCanButton.layer.backgroundColor = AppColors.setInActive.cgColor
        }
         self.allReadersCanButton.layer.mask = rectShape
        let rectBtnShape = CAShapeLayer()
        rectBtnShape.bounds = self.allReadersCanButton.frame
        rectBtnShape.position = self.allReadersCanButton.center
        rectBtnShape.path = UIBezierPath(roundedRect: self.noticeButton.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 6, height: 6)).cgPath
        self.noticeButton.layer.mask = rectBtnShape
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.layOutSearchbar()
        self.layOutRoleButtons()
        self.setViewParams(type: permittedMembers)
       
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTap)))
        self.layoutPriorityButtons()
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(title: SaveText.localized(), style: .plain, target: self, action: #selector(Done))
        self.layOUtConstraints()
        if self.selection != .groupMail{
//            self.userProfessionLabel.text = AppConfig.shared.user.preName?.capitalized
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         if self.selection == .groupMail{
            self.container.isHidden = false
            self.whenHidden.priority = .defaultLow
            self.whenShown.priority = .defaultHigh
            if self.groupMembersSource.count == 0 {
                self.getMembers(groupId: groupId)
            }else{
                self.setPreSelection()
            }
        }else{
//            self.container.isHidden = true
//            self.whenHidden.priority = .defaultHigh
//            self.whenShown.priority = .defaultLow
//            if self.memberIds.count == 0 {
//                self.members(of: groupId)
//            }else{
//                self.groupMembersSource = AppConfig.shared.threadGroupMembers
//                self.setPreSelection()
//            }
//            if self.groupId == publicGroupID{
//                self.adminIds = AppConfig.shared.communityGroupAdmins
//                if let userId = AppConfig.shared.user.id{
//                    if adminIds.contains(String(userId)){
//                        self.TopViewBottomToPriority.priority = .defaultLow
//                        self.TopViewBottomToEmail.priority = .defaultHigh
//                        self.emailHeader.isHidden = false
//                        self.notificationsView.isHidden = false
//                    }else{
//                        self.TopViewBottomToEmail.priority = .defaultLow
//                        self.TopViewBottomToPriority.priority = .defaultHigh
//                        self.emailHeader.isHidden = true
//                        self.notificationsView.isHidden = true
//                    }
//                }else{
//                    self.TopViewBottomToEmail.priority = .defaultLow
//                    self.TopViewBottomToPriority.priority = .defaultHigh
//                    self.emailHeader.isHidden = true
//                    self.notificationsView.isHidden = true
//                }
//            }
        }
    }
    
    func setPreSelection() {
        if self.selectedMembers.count != 0 {
            self.groupMembersSource = self.groupMembersSource.map{(user) in
                var updatedUser = user
                _ = self.selectedMembers.allSatisfy { (item) -> Bool in
                    if item.user_id == user.user_id {
                        updatedUser.isSelected = true
                        return false
                    }
                    return true
                }
                return updatedUser
            }
        }
        tableView.reloadData()
    }
    
    func layOUtConstraints(){
        switch self.selection {
        case .thread:
            topView.isHidden = false
            self.whenMailPermission.priority = .defaultLow
            self.whenThreadPermission.priority = .defaultHigh
            break
        case .groupMail:
            topView.isHidden = true
          //  topView.subviews.forEach { $0.isHidden = true }
            self.whenThreadPermission.priority = .defaultLow
            self.whenMailPermission.priority = .defaultHigh
            break
        }
    }
    
    func layOutRoleButtons() {
        self.OnlyICanButton.roundedCorner([.topLeft, .bottomLeft], radius: 6.0)
        self.OnlyICanButton.layer.masksToBounds = true
        self.allReadersCanButton.roundedCorner([.topRight, .bottomRight], radius: 6.0)
        self.allReadersCanButton.layer.masksToBounds = true
         self.addFilePermissions == "all" ? self.setPermissionRights(allReadersCanButton)  : self.setPermissionRights(OnlyICanButton)
    }
    
    func layoutPriorityButtons(){
        self.criticalButton.roundedCorner([.topLeft,.bottomLeft], radius: 6.0)
        self.criticalButton.layer.masksToBounds = true
        self.noticeButton.roundedCorner([.topRight,.bottomRight], radius: 6.0)
        self.noticeButton.layer.masksToBounds = true
        self.criticalButton.backgroundColor = AppColors.setInActive
        self.importantButton.backgroundColor = AppColors.setInActive
        self.noticeButton.backgroundColor = AppColors.setInActive
        switch self.priority {
        case .normal:
            break
        case .critical:
            self.setPriority(criticalButton)
            break
        case .important:
            self.setPriority(importantButton)
            break
        case .notice:
            self.setPriority(noticeButton)
            break
        case .none:
            break
        }
        
        if self.emailNotify == "N"{
            //Shiow checked
            if let view = notificationsView.subviews.last as? BEMCheckBox{
                view.on = false
            }
        }else{
            if let view = notificationsView.subviews.last as? BEMCheckBox{
                view.on = true
            }
        }
    }
    
    
    @objc func viewTap(){
        self.view.endEditing(true)
    }
    
    func setViewParams(type: String)  {
        if self.selection == .groupMail{
            switch type.lowercased() {
            case "doctor":
                checkBoxCollection[1].on = true
//                self.userProfessionLabel.text = type.capitalized
                setVisibilitty()
                break
            case "health care personal":
                checkBoxCollection[4].on = true
                setVisibilitty()
                break
            case "standarduser":
//                didself.userProfessionLabel.text = type.capitalized
                checkBoxCollection[5].on = true
                setVisibilitty()
                break
            case "custom":
                checkBoxCollection[2].on = true
                setVisibilitty(false)
                break
            default:
                checkBoxCollection[0].on = true
                setVisibilitty()
                break
            }
        }else{
            switch type.lowercased() {
            case "doctor", "health care personal","standarduser":
                checkBoxCollection[1].on = true
                self.userProfessionLabel.text = type.capitalized
                setVisibilitty()
                break
                //        case "health care personal":
                //            checkBoxCollection[1].on = true
                //            setVisibilitty()
                //            break
                //        case "citizen":
                //            self.userProfessionLabel.text = type.capitalized
                //            checkBoxCollection[1].on = true
                //            setVisibilitty()
            //            break
            case "custom":
                checkBoxCollection[2].on = true
                setVisibilitty(false)
                break
            default:
                checkBoxCollection[0].on = true
                setVisibilitty()
                break
            }
        }
        
    }
    
    private func getMembers(groupId: Int) {
        //TODO
//        RequestManager.shared.getGroupUsers(groupID: groupId) { (status, response) in
//            if status {
//                if let res = response as? GroupMemberModel {
//                    guard let id = AppConfig.shared.user.id else { return }
//                    let members = (res.clinic_admins ?? []) + (res.joined_users ?? [])
//                    self.groupMembersSource = members.filter {($0.user_id != id)}
//                    self.groupMembers = self.groupMembersSource
//                    self.tableView.reloadData()
//                }
//            }
//        }
    }
    
    func layOutSearchbar(){
        self.searchBar.placeholder = SearchText.localized
        self.searchBar.delegate = self
        if let searchTextField = self.searchBar.textField{
            searchTextField.textColor = .white
//            searchTextField.keyboardAppearance = .dark
        }
        searchBar.setImage(UIImage(named: "searchIcon"), for: .search, state: .normal)
    }
    
    @objc func Done(){
        delegate?.didSelectPermissions(status: true, permission: self.addFilePermissions, members: self.permittedMembers,emailNotification: self.emailNotify, priority: self.priority)
        self.dismissVC()
    }
    
    @objc func didTapEmailNotification(_ sender:UITapGestureRecognizer){
        if let view = sender.view?.subviews.last as? BEMCheckBox{

            view.on = !view.on
            if view.on{
                self.emailNotify = "Y"
            }else{
                self.emailNotify = "N"
            }
            print(self.emailNotify)
        }
    }
    
    @objc func selectThreadPermissions(_ sender : UIGestureRecognizer){
        for item in checkBoxCollection{
            item.on = false
        }
        if let view = sender.view?.subviews.last as? BEMCheckBox{
            view.on = !view.on
        }
        let tag = sender.view!.tag
        switch tag {
        case 1:
            if let preName = userProfessionLabel.text{
                self.permittedMembers = preName
            }
            setVisibilitty()
            break
        case 2:
            self.permittedMembers = "Health Care Personal"
            setVisibilitty()
            break
        case 3:
            self.permittedMembers = "Standard User"
            setVisibilitty()
            break
        case 4:
            self.permittedMembers = "custom"
//            AppConfig.shared.resetThreadGroupMembers()
//            self.groupMembersSource = AppConfig.shared.threadGroupMembers
            self.searchBar.isHidden = false
            setVisibilitty(false)
            self.scrollView.scrollTo(direction: .bottom, animated: true)
            self.tableView.reloadData()
            break
        default:
            self.permittedMembers = "all"
            setVisibilitty()
            break
        }
    }
    
    func setVisibilitty(_ flag:Bool = true){
        self.searchBar.isHidden = flag
        self.tableView.isHidden = flag
        self.tableView.isUserInteractionEnabled = true
        if flag{
            self.searchBar.resignFirstResponder()
            self.scrollView.scrollTo(direction: .top, animated: true)
        }else{
            self.scrollView.scrollTo(direction: .bottom, animated: true)
        }
    }
    
    @IBAction func setPermissionRights(_ sender: UIButton){
        if sender == OnlyICanButton{
            self.allReadersCanButton.backgroundColor = AppColors.setInActive
            sender.backgroundColor = AppColors.setActive
            addFilePermissions = "only_authors"
        }else{
            self.OnlyICanButton.backgroundColor = AppColors.setInActive
            sender.backgroundColor = AppColors.setActive
            addFilePermissions = "all"
        }
    }
    
    @IBAction func  setPriority(_ sender: UIButton){
        if sender == criticalButton{
            self.importantButton.backgroundColor = AppColors.setInActive
            self.importantButton.setTitleColor(.white, for: .normal)
            self.noticeButton.backgroundColor = AppColors.setInActive
            self.noticeButton.setTitleColor(.white, for: .normal)
            if !isNavigating{
                self.priority =  self.priority != .critical ? .critical :.none
            }else{
                self.isNavigating = false
                
            }
            sender.backgroundColor = self.priority == .critical ? AppColors.setActive : AppColors.setInActive
            self.priority == .critical ? sender.setTitleColor(AppColors.critical, for: .normal) : sender.setTitleColor(.white, for: .normal)
            
        
        }else if sender == importantButton{
            self.criticalButton.backgroundColor = AppColors.setInActive
            self.criticalButton.setTitleColor(.white, for: .normal)
            self.noticeButton.backgroundColor = AppColors.setInActive
            self.noticeButton.setTitleColor(.white, for: .normal)
            if !isNavigating{
                self.priority = self.priority != .important ? .important :.none
            }else{
                self.isNavigating = false
                
            }
            sender.backgroundColor = self.priority == .important ? AppColors.setActive : AppColors.setInActive
            self.priority == .important ? sender.setTitleColor(AppColors.important, for: .normal) : sender.setTitleColor(.white, for: .normal)
            
            
        }else{
            self.criticalButton.backgroundColor = AppColors.setInActive
            self.criticalButton.setTitleColor(.white, for: .normal)
            self.importantButton.backgroundColor = AppColors.setInActive
            self.importantButton.setTitleColor(.white, for: .normal)
            if !isNavigating{
                self.priority =  self.priority != .notice ? .notice :.none
            }else{
                self.isNavigating = false
                
            }
            sender.backgroundColor = self.priority == .notice ? AppColors.setActive : AppColors.setInActive
            self.priority == .notice ? sender.setTitleColor(AppColors.notice, for: .normal) : sender.setTitleColor(.white, for: .normal)
            
        }
    }
}

extension SelectPermissionsVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            self.groupMembers = groupMembersSource
            self.tableView.reloadData()
            self.searchBar.endEditing(true)
            return
        }

        self.groupMembers = groupMembersSource.filter({ members -> Bool in
            if members.phone != nil && members.email != nil {
                return members.full_name!.lowercased().contains(searchText.lowercased()) || members.phone!.contains(searchText.lowercased()) || members.email!.lowercased().contains(searchText.lowercased())
            }
            else {
                return members.full_name!.lowercased().contains(searchText.lowercased())
            }
        })
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension SelectPermissionsVC:  UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as? GeneralViewCell else {return self.emptyTblCell()}
        cell.delegate = self
        cell.setupData(indexPath: indexPath, data: self.groupMembers[indexPath.row])
        return cell
    }
    
    private func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: GeneralViewCell.self), bundle: nil), forCellReuseIdentifier: "GeneralCell")
        
        self.tableView.separatorStyle = .none
    }
}

extension SelectPermissionsVC: GeneralViewCellDelegate{
    func didTapRow(at index: Int) {
        self.groupMembers[index].isSelected = !self.groupMembers[index].isSelected
        self.groupMembersSource = self.groupMembersSource.map{(user) in
            var updatedUser = user
            _ = self.groupMembers.allSatisfy { (item) -> Bool in
                if item.user_id == user.user_id {
                    updatedUser.isSelected = item.isSelected
                    return false
                }
                return true
            }
            return updatedUser
        }
        
        //NOTE: add member to selected member list or remove from selected member list
        if let index = self.selectedMembers.firstIndex(where: { (item) -> Bool in
            item.user_id == self.groupMembers[index].user_id
        }) {
            self.selectedMembers.remove(at: index)
        }
        else {
            self.selectedMembers.append(self.groupMembers[index])
        }
        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
}
