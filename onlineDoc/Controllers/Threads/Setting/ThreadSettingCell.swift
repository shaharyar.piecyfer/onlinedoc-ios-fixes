//
//  ThreadSettingCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 25/02/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

class ThreadSettingCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var swBtn: UISwitch!
    var indexPath: IndexPath?
    var onUpdate:((Bool, IndexPath) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Generic theme
        Themer.shared.register(
            target: self,
            action: ThreadSettingCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(title: String, hasIcon: Bool, color: UIColor?, switchStatus: Bool = false){
        lblTitle.text = title.localized()
        swBtn.setOn(switchStatus, animated: false)
        ivPhoto.image = hasIcon ? UIImage(named: "ic_bell_filled_circle") : nil
        if hasIcon {
            ivPhoto.image = ivPhoto.image?.withRenderingMode(.alwaysTemplate)
            ivPhoto.tintColor = color
        }
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        onUpdate?(sender.isOn, self.indexPath!)
    }
}
