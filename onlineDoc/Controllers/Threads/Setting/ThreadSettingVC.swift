//
//  ThreadSettingVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 25/02/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

protocol ThreadSettingVCDelegate: class {
    func didSelect(permission: String, emailNotification: String, priority: Priority)
}

class ThreadSettingVC: BaseVC {

    @IBOutlet weak var tableView:UITableView!
    var secHeader:[String] = ["Priority", "Who can add files?", "Email notification"]
    var sec1Titles:[String] = ["Notifications active:  Normal", "Notifications active:  Notice", "Notifications active:  Important", "Notifications active:  Critical"]
    var sec1SwitchStatus:[Bool] = [false, false, false, false]
    var sec1TintColor:[UIColor] = [.gray, .systemBlue, .systemYellow, .systemRed]
    var sec2Titles:[String] = ["Only I Can", "All Readers Can"]
    var sec2SwitchStatus:[Bool] = [true, false]
    var sec3Titles:[String] = ["Send also posting some email notifications to members"]
    var sec3SwitchStatus:[Bool] = [false]
    
    weak var delegate : ThreadSettingVCDelegate!
    var addFilePermissions = "all"
    var emailNotify = "N"
    var priority: Priority = .none {
        didSet{
            print(priority.rawValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(title: SaveText.localized(), style: .plain, target: self, action: #selector(onClickSave))
        self.setupView()
    }
    
    private func setupView(){
        self.navigationItem.title = "Notifications".localized().uppercased()
        if addFilePermissions == "all" {
            sec2SwitchStatus[0] = false
            sec2SwitchStatus[1] = true
        } else {
            sec2SwitchStatus[0] = true
            sec2SwitchStatus[1] = false
        }
        
        emailNotify == "N" ? (sec3SwitchStatus[0] = false) : (sec3SwitchStatus[0] = true)
        switch priority {
        case .normal:
            sec1SwitchStatus[0] = true
            break
        case .critical:
            sec1SwitchStatus[3] = true
            break
        case .important:
            sec1SwitchStatus[2] = true
            break
        case .notice:
            sec1SwitchStatus[1] = true
            break
        case .none:
            break
        }
    }
    
    @objc func onClickSave(){
//        AppConfig.shared.threadGroupMembers = self.groupMembersSource
        delegate?.didSelect(permission: self.addFilePermissions, emailNotification: self.emailNotify, priority: self.priority)
        self.navigationController?.popViewController(animated: true)
    }
}

extension ThreadSettingVC: UITableViewDelegate, UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
//        headerView.backgroundColor = ColorPalette.light
//        let label = UILabel()
//        label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
//        label.text = secHeader[section].localized().uppercased()
////        label.font = UIFont().futuraPTMediumFont(16)
//        label.textColor = UIColor.white
//        headerView.addSubview(label)
//        return headerView
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return sec1Titles.count
        }
        else if section == 1 {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ThreadSettingCell.self), for: indexPath) as! ThreadSettingCell
        if indexPath.section == 0 {
            cell.indexPath = indexPath
            cell.configCell(title: sec1Titles[indexPath.row], hasIcon: true, color: sec1TintColor[indexPath.row], switchStatus: self.sec1SwitchStatus[indexPath.row])
            cell.onUpdate = { (val, index) in
                self.sec1SwitchStatus = self.sec1SwitchStatus.map({ (item) -> Bool in
                    return false
                })
                self.sec1SwitchStatus[index.row] = val
                if val {
                    self.priority = index.row == 0 ? .normal : index.row == 1 ? .notice : index.row == 2 ? .important : .critical
                    self.tableView.reloadSections([indexPath.section], with: .none)
                } else {
                    self.priority = .none
                }
            }
        }
        else if indexPath.section == 1 {
            cell.indexPath = indexPath
            cell.configCell(title: sec2Titles[indexPath.row], hasIcon: false, color: nil, switchStatus: self.sec2SwitchStatus[indexPath.row])
            cell.onUpdate = { (val, index) in
                self.sec2SwitchStatus = self.sec2SwitchStatus.map({ (item) -> Bool in
                    return false
                })
                self.sec2SwitchStatus[index.row] = val
                if !val {
                    self.sec2SwitchStatus[index.row == 0 ? 1 : 0] = true
                }
                if index.row == 0 {
                    self.addFilePermissions = val ? "only_authors" : "all"
                } else if index.row == 1 {
                    self.addFilePermissions = val ? "all" : "only_authors"
                }
                self.tableView.reloadSections([index.section], with: .none)
            }
        }
        else {
            cell.indexPath = indexPath
            cell.configCell(title: sec3Titles[indexPath.row], hasIcon: false, color: nil, switchStatus: self.sec3SwitchStatus[indexPath.row])
            cell.onUpdate = { (val, index) in
                self.sec3SwitchStatus = self.sec3SwitchStatus.map({ (item) -> Bool in
                    return false
                })
                self.sec3SwitchStatus[index.row] = val
                if val {
                    self.emailNotify = "Y"
                    self.tableView.reloadSections([index.section], with: .none)
                } else {
                    self.emailNotify = "N"
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: ThreadSettingCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ThreadSettingCell.self))

        self.tableView.separatorStyle = .none
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }
}
