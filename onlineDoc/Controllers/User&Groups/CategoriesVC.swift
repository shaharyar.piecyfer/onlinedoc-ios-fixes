//
//  CategoriesVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 18/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class CategoriesVC: BaseVC {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var groupData = NewGroupModel()
    var groupImageData: Data?
    var directUploadParams: DirectUploadParams?
    
    var categoriesData = [GroupCategoryModel](){
        didSet{
            tableView.reloadData()
        }
    }
    
    var alreadyAddedCategories = [GroupCategoryModel]() { //in case of editing
        didSet{
            categoriesData = alreadyAddedCategories
        }
    }
    var fromExplore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layOutNavBar()
        self.setupTBL()
        self.setupView()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTap)))
        
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: CategoriesVC.setTheme)
    }
    
    deinit {
        print("CategoriesVC->deinit")
    }
    
    @objc func viewTap(){
        self.view.endEditing(true)
    }
    
    func layOutNavBar()  {
        self.title = "SELECT FOLDERS".localized()
        let nextButton = UIBarButtonItem(title: DoneText.localized(), style: .plain, target: self, action: #selector(onClinkCreate))
        self.navigationItem.rightBarButtonItem  = nextButton
    }
    
    private func setupView() {
        if alreadyAddedCategories.isEmpty {
            let defaultCategory = GroupCategoryModel()
            defaultCategory.name = "Standard (Default)"
            defaultCategory.sort_order = 0
            defaultCategory.isChecked = true
            alreadyAddedCategories.append(defaultCategory)
        }
    }
    
    @objc func onClinkCreate(){
        self.view.endEditing(true)
        let selectedCategories = categoriesData.filter{ $0.isChecked }
        var checkedCategories = selectedCategories
        checkedCategories.removeFirst()
        
        for item in checkedCategories {
            let newCategory = ClinicCategoryModel(name: item.name, sort_order: item.sort_order)
            self.groupData.clinic_categories_attributes.append(newCategory)
        }
        
        print("CategoriesVC->groupData", self.groupData)
        
        let crossReference = Dictionary(grouping: selectedCategories, by: { $0.sort_order })
        let duplicates = crossReference.filter { $1.count > 1 }
        
        if !duplicates.isEmpty {
            self.showAlert(To: "Alert", for: "You cannot add a duplicate sort order number.".localized()) {_ in}
            return
        }
        
        if let params = directUploadParams, let imageData = groupImageData {
            RequestManager.shared.directUpload(_params: params, data: imageData) { [weak self] (status, result) in
                guard let `self` = self else {return}
                if status {
                    if let data = result as? DirectUploadModel {
                        var progressView: ProgressView?
                        DispatchQueue.main.async { [weak self] in
                            guard let `self` = self else {return}
                            progressView = ProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.screenHeight))
                            progressView!.lblTitle.text = "txt_progress_title".localized()
                            progressView!.lblDetail.text = "txt_progress_detail".localized()
                            progressView!.uiProgress.progress = 0
                            progressView!.tag = 100
                            progressView!.lblCount.text = "1/1"
                            self.addView(view: progressView!)
                        }
                        
                        RequestManager.shared.directUploadImage(data: imageData, params: data) { [weak self] (percent) in
                            guard let _ = self else {return}
                            progressView?.uiProgress.progress = Float(percent)
                        } completion: { [weak self] (status, response) in
                            guard let `self` = self else {return}
                            if let progressView = progressView {
                                self.removeView(tag: progressView.tag)
                            }
                            if status {
                                self.groupData.file = data.signed_id
                                self.createGroup()
                            }
                            else {
                                self.showAlert(To: "Alert".localized(), for: "Failed to upload the image.".localized()) {_ in}
                            }
                        }
                    }
                }
            }
        }
        else {
            self.createGroup()
        }
    }
    
    private func createGroup(){
        RequestManager.shared.createGroup(params: self.groupData) { (status, response) in
            if status {
                if let group = response as? CreateGroupReaponse {
                    if let groupInfo = group.result {
                        NotificationCenter.default.post(name: .notifyGroup, object: nil, userInfo: ["Group": groupInfo, "Notify": NotifyCRUD.create])
                    }
                    self.showAlert(To: "", for: GroupCreateMessage.localized()) { [weak self] (status) in
                        guard let `self` = self else {return}
                        if self.fromExplore && self.navigationController != nil {
                            self.popTo(from: self, to: UsersAndGroupsVC.self)
                        }
                        else{
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func addNewCategory(){
        let newCategry = GroupCategoryModel()
        let highestOrder = self.categoriesData.map{ ($0.sort_order ?? 0) }.max() ?? 0
        newCategry.sort_order = 1 + highestOrder
        self.categoriesData.append(newCategry)
        let lastCellIndex = IndexPath(row: categoriesData.count - 1, section: 0)
        self.tableView.scrollToRow(at: lastCellIndex, at: .bottom, animated: true)

        let cell = self.tableView.cellForRow(at: lastCellIndex) as? CategoryFieldCell
        _ = cell?.newCategoryField.becomeFirstResponder()
    }
}

extension CategoriesVC : UITableViewDelegate, UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         self.categoriesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < self.alreadyAddedCategories.count{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as? CategoryRowCell
                else {
                    return UITableViewCell()
            }
            
            if categoriesData.indices.contains(indexPath.row) {
                cell.configCell(indexPath: indexPath, data: categoriesData[indexPath.row])
                cell.delegate = self
                cell.categoryCheckBoxView.tag = indexPath.row
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CategoryFieldCell.self)) as? CategoryFieldCell
                else {
                    return UITableViewCell()
            }
            cell.index = indexPath
            cell.configCell(indexPath: indexPath, data: categoriesData[indexPath.row])
            cell.checkBox.tag = indexPath.row
            cell.delegate = self
            return cell
        }
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: CategoryRowCell.self), bundle: nil), forCellReuseIdentifier: "CategoryCell")
        self.tableView.register(UINib.init(nibName: String(describing: CategoryFieldCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CategoryFieldCell.self))
        
        self.tableView.separatorStyle = .none
    }
}

extension CategoriesVC: CategoryRowCellDelegate{
   
    func didTapCheckMark(index: IndexPath, order: Int?) {
        if let sorting = order {
            categoriesData[index.row].sort_order = sorting
        }else{
            categoriesData[index.row].isChecked = !categoriesData[index.row].isChecked
//            self.categoriesListTableView.reloadData()
        }
    }
    
    func didEditCategory(at index: IndexPath, with Text: String) {
        //Optional
    }
    
}

extension CategoriesVC: CategoryFieldCellDelegate{
    
    func didWriteNewCategory(index: IndexPath, isChecked: Bool, category: String?, sortOrder: Int?) {
        if isChecked{
            if let cat = category, cat != ""{
                let isValidIndex = categoriesData.indices.contains(index.row)
                if isValidIndex{
                    if !self.categoriesData[index.row].isChecked{
                        self.categoriesData[index.row].isChecked = !self.categoriesData[index.row].isChecked
                    }
                    self.categoriesData[index.row].name = cat
                }
            }else if let sorting = sortOrder{
                let isValidIndex = categoriesData.indices.contains(index.row)
                if isValidIndex{
                    self.categoriesData[index.row].sort_order = sorting
                }
            }
        }else{
            let isValidIndex = categoriesData.indices.contains(index.row)
            if isValidIndex{
                self.categoriesData[index.row].isChecked = false
                self.categoriesData[index.row].name = ""
            }
        }
    }
    
    func didTapCheckMark(index: IndexPath, isChecked: Bool) {
        self.categoriesData[index.row].isChecked = isChecked
    }
}

extension CategoriesVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        topView.backgroundColor = theme.settings.highlightedBgColor
    }
}
