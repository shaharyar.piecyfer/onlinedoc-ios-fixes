//
//  GroupMemberCell.swift
//  onlineDoc
//
//  Created by Piecyfer on 08/03/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

class GroupMemberCell: UITableViewCell {
    
    @IBOutlet weak var ivProfile: UIImageView! {
        didSet {
            ivProfile.layer.borderWidth = 0.5
            ivProfile.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var viewLeaveBtn: UIView!
    @IBOutlet weak var btnLeave: UIButton!
    @IBOutlet weak var ivDelete: UIImageView!
    @IBOutlet weak var viewRequestBtns: UIStackView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var uiMakeAdminBtn: UIView!
    
    var onDeleteMember:((IndexPath)->(Void))?
    var onClickRequestedUserAction:((IndexPath, Bool)->(Void))?
    var makeAdmin:((IndexPath)->(Void))?
    var indexPath: IndexPath?
    var groupInfo: GroupModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ivDelete.image = ivDelete.image?.withRenderingMode(.alwaysTemplate)
        ivDelete.tintColor = .white
        btnAccept.setTitle("Approve".localized(), for: .normal)
        btnReject.setTitle("Reject".localized(), for: .normal)
        
        //Generic theme
        Themer.shared.register(
            target: self,
            action: GroupMemberCell.applyTheme)
        let labels = AppConfig.shared.getLabelsInView(view: self.contentView)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.applyTheme)
        }
        //End Generic theme
        
        Themer.shared.register(
            target: self,
            action: GroupMemberCell.setTheme)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func onClickLeave(){
        self.onDeleteMember?(indexPath!)
    }
    
    @IBAction func onClickAccept(_ sender: UIButton){
        self.onClickRequestedUserAction?(indexPath!, true)
    }
    
    @IBAction func onClickReject(_ sender: UIButton){
        self.onClickRequestedUserAction?(indexPath!, false)
    }
    
    @IBAction func onClickMakeAdmin(_ sender: UIButton){
        self.makeAdmin?(indexPath!)
    }
    
    func configCell(me: MemberModel,_ data: MemberModel, hasAdmins:Bool = false, amIAdmin:Bool = false, canLeave:Bool = true) {
        let imgUrl = data.file?.medium_url ?? data.file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.ivProfile.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "ic_user"))
        }else{
            self.ivProfile.image = UIImage(named: "ic_user")
        }
        
        if let name  = data.full_name {
            self.lblName.text = name
        }
        
        if let profession = data.pre_name{
            self.lblProfession.text = profession.replacingOccurrences(of: "_", with: " ").capitalized.localized()
        }
        
        self.viewRequestBtns.isHidden = true
        self.viewLeaveBtn.isHidden = hasAdmins ? true : !amIAdmin
        if me.role == GroupMemberRoles.SUPER_ADMIN.rawValue {
            self.viewLeaveBtn.isHidden = me.user_id == data.user_id
            self.uiMakeAdminBtn.isHidden = data.role == GroupMemberRoles.SUPER_ADMIN.rawValue
        }
        else {
            self.uiMakeAdminBtn.isHidden = true
        }
        if data.request_status == GroupStatus.REQUESTED.rawValue {
            self.viewLeaveBtn.isHidden = true
            if ((groupInfo?.can_approve == WhoCanUpdate.ADMIN.rawValue || groupInfo?.can_approve == WhoCanUpdate.SUPER_ADMIN.rawValue) && amIAdmin) {
                self.viewRequestBtns.isHidden = false
            }
            if groupInfo?.can_approve == WhoCanUpdate.EVERYONE.rawValue {
                self.viewRequestBtns.isHidden = false
            }
            self.btnReject.isHidden = !amIAdmin
        }
        if !canLeave {
            self.viewLeaveBtn.isHidden = true
        }
        
    }
}

extension GroupMemberCell {
    fileprivate func setTheme(_ theme: MyTheme) {
        lblName.textColor = theme.settings.titleColor
        lblProfession.textColor = theme.settings.subTitleColor
    }
}
