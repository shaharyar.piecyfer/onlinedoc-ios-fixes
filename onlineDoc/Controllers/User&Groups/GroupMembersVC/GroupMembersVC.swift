//
//  GroupMembersVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 08/03/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

class GroupMembersVC: BaseVC {
    @IBOutlet weak var uiHeader: UIView!
    @IBOutlet weak var ivProfile: UIImageView! {
        didSet {
            ivProfile.layer.borderWidth = 0.5
            ivProfile.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMemberSince: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topViewHight: NSLayoutConstraint!
    
    var onLeave: (()->(Void))?
    
    var me: MemberModel?
    var groupInfo = GroupModel()
    var groupMembers = [MemberModel]()
    var groupAdmins = [MemberModel]()
    var groupRequestedMembers = [MemberModel]()
    var groupMembersDataSource = GroupMemberModel()
    
    var amIAdmin = false
    var canApproveRequests = false
    var lastContentOffset = CGPoint(x: 0, y: 0)
    var pageNumber = 1
    var loadMore = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Members".localized().uppercased()
        
        self.setupMe()
//        self.setupTBL()
        self.getGroupUsers()
        
        self.setupTheme()
    }
    
    private func setupMe() {
        let currentUser = AppConfig.shared.user
        var me = MemberModel()
        if let _ = currentUser.id {
            me.user_id = currentUser.id
            me.full_name = currentUser.full_name ?? ((currentUser.first_name ?? "") + " " + (currentUser.last_name ?? ""))
            me.pre_name = currentUser.pre_name
            me.email = currentUser.email
            me.file = currentUser.file
            me.request_status = GroupStatus.APPVOVED.rawValue
            me.role = GroupMemberRoles.USER.rawValue
        }
        
        if let admins = groupInfo.clinic_admins, let userId = AppConfig.shared.user.id,
           let admin = admins.first(where: { $0.id == userId }) {
            me.role = admin.role
        }
        
        me.join_date = self.groupInfo.join_date
        self.me = me
        
        self.setupView()
        self.setupTBL()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: GroupMembersVC.setTheme)
        Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    deinit {
        print("GroupMembersVC->deinit")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.navigationController != nil){
            baseVC = self
        }
        layOutSearchbar()
    }
    
    private func getGroupUsers(searchText: String = "") {
        guard let id = groupInfo.id else {return}
        if self.pageNumber == 1 {
            self.groupAdmins.removeAll()
            self.groupRequestedMembers.removeAll()
            self.groupMembers.removeAll()
        }
        RequestManager.shared.getGroupUsers(groupID: id, page: self.pageNumber, searchText: searchText) { (status, response) in
            if status {
                if let res = response as? GroupMemberModel {
                    if self.pageNumber == 1 {
                        if !searchText.isEmpty {
                            self.groupAdmins = self.groupMembersDataSource.clinic_admins?.filter {
                                guard $0.phone != nil else {return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email != nil && $0.email!.lowercased().contains(searchText.lowercased())) }

                                return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.phone)!.contains(searchText.lowercased()) || ($0.email != nil && $0.email!.lowercased().contains(searchText.lowercased()))
                            } ?? []
                            self.groupRequestedMembers = self.groupMembersDataSource.requested_users?.filter {
                                guard $0.phone != nil else {return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email != nil && $0.email!.lowercased().contains(searchText.lowercased())) }

                                return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.phone)!.contains(searchText.lowercased()) || ($0.email != nil && $0.email!.lowercased().contains(searchText.lowercased()))
                            } ?? []
                        }
                        else {
                            self.groupMembersDataSource.clinic_admins = res.clinic_admins
                            self.groupMembersDataSource.requested_users = res.requested_users
                            self.groupMembersDataSource.joined_users = []
                            self.groupAdmins = self.groupMembersDataSource.clinic_admins ?? []
                            self.groupRequestedMembers = self.groupMembersDataSource.requested_users ?? []
                            self.groupMembers = []
                            
                            if let userId = AppConfig.shared.user.id, let admins = self.groupMembersDataSource.clinic_admins,  admins.contains(where: { (member) -> Bool in
                                member.user_id == userId
                            }) {
                                self.amIAdmin = true
                            }
                        }
                    }
                    
                    if let joinedUsers = res.joined_users {
                        self.groupMembersDataSource.joined_users?.append(contentsOf: joinedUsers)
                        self.groupMembers.append(contentsOf: joinedUsers)
                        
                        if joinedUsers.count == 0 || joinedUsers.count < pageLimit10 {
                            self.loadMore = false
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func setupView() {
        lblHeader.text = "Your Membership".localized().uppercased()
        lblHeader.font = AppFont.kMuliSemiBold(14).font()
        btnDelete.setTitle("Delete Membership".localized(), for: .normal)
        let imgUrl = me?.file?.medium_url ?? me?.file?.src
        if let imageUrl = imgUrl, imageUrl != ""{
            let url = URL(string: imageUrl)
            self.ivProfile.imageFromServer(imageUrl: url!, placeholder: #imageLiteral(resourceName: "ic_user"))
        }else{
            self.ivProfile.image = UIImage(named: "ic_user")
        }
        
        if let name  = me?.full_name {
            self.lblName.text = name
        }
        
        var dateSince = "20/3 -2020"
        if let addedDate = me?.join_date {
            dateSince = AppConfig.shared.convertToDate(from: addedDate).formatDate(format: "dd/MM -YYYY") //addedDate.formattedDate
        }
        self.lblMemberSince.text = "Member since".localized() + " \(dateSince)"
        if let canLeave = self.groupInfo.can_leave_clinic {
            self.btnDelete.isHidden = !canLeave
        }
        
        self.canApproveRequests = amIAdmin
        if let approveBy = self.groupInfo.can_approve {
            self.canApproveRequests = approveBy == "Everyone" ? true : amIAdmin
        }
        
        self.lastContentOffset = self.tableView.contentOffset
    }
    
    func layOutSearchbar(){
        self.searchBar.placeholder = SearchText.localized
        self.searchBar.delegate = self
//        self.searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
//        self.searchBar.tintColor = .white
//        if let searchTextField = self.searchBar.textField{
//            searchTextField.textColor = .white
//            searchTextField.keyboardAppearance = .dark
//        }
    }
    
    @IBAction func onClickLeave(_ sender: UIButton){
        self.showAlert(To: "", for: "Are you sure you want to leave group?".localized(), firstBtnStyle: .cancel, secondBtnStyle: .destructive, firstBtnTitle: "Delete".localized(), secondBtnTitle: "Cancel".localized()) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                if let clinicId = self.groupInfo.id {
                    RequestManager.shared.leaveGroup(clinicId: clinicId) { (status, data) in
                        if status{
                            NotificationCenter.default.post(name: .notifyGroup, object: nil, userInfo: ["Group": self.groupInfo, "Notify": NotifyCRUD.delete])
                            self.navigationController?.popViewController(animated: true)
                            self.onLeave?()
                        }
                    }
                }
            }
        }
    }
    
//    func reloadData(members: GroupMemberModel) {
//        self.groupAdmins = members.clinic_admins?.filter({ (member) -> Bool in
//            if member.user_id == AppConfig.shared.user.id {
//                self.me = member
//            }
//            return true
//        }) ?? [MemberModel]()
//        self.groupMembers = members.joined_users?.filter({ (member) -> Bool in
//            if member.user_id == AppConfig.shared.user.id {
//                self.me = member
//                return false
//            }
//            return true
//        }) ?? [MemberModel]()
//
//        self.groupRequestedMembers = members.requested_users ?? [MemberModel]()
//        self.setupView()
//        self.tableView.reloadData()
//    }
    
    func deleteGroupMember(at index: IndexPath){
        let member = index.section == 0 ? self.groupAdmins[index.row] : self.groupMembers[index.row]
        var memberParams = DeleteRequest()
        memberParams.clinic_id = groupInfo.id
        memberParams.user_id = member.user_id
        let deleteMemberAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if let firstSubview = deleteMemberAlert.view.subviews.first, let alertContentView = firstSubview.subviews.first {
            for view in alertContentView.subviews {
                view.backgroundColor = .black
            }
        }
        deleteMemberAlert.modalPresentationStyle = .popover
        deleteMemberAlert.view.tintColor = .white
        deleteMemberAlert.addAction(UIAlertAction(title: option1.localized(), style: .default, handler: { (action) in
            memberParams.record = "discard"
            self.handleDeleteMember(memberParams, at: index)
        }))
        deleteMemberAlert.addAction(UIAlertAction(title:  option2.localized(), style: .default, handler: { (action) in
            memberParams.record = "keep"
            self.handleDeleteMember(memberParams, at: index)
        }))
        deleteMemberAlert.addAction(UIAlertAction(title: "", style: .default, handler: nil))
        let cancelButtonViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CancelButtonViewController")
        let cancelAction = UIAlertAction(title: "", style: .cancel, handler: nil)
        cancelAction.setValue(cancelButtonViewController, forKey: "contentViewController")
        deleteMemberAlert.addAction(cancelAction)
        deleteMemberAlert.view.tintColor = .white
        present(deleteMemberAlert, animated: true, completion: nil)
    }
    
    func handleDeleteMember(_ memberParams:DeleteRequest, at index:IndexPath){
        RequestManager.shared.removeGroupMember(params: memberParams) { (status, data) in
            if status{
                let group = self.groupInfo
                if let _ = group.id {
                    NotificationCenter.default.post(name: .notifyGroupMember, object: nil, userInfo: ["Group": group, "IsAdded": false, "Members": 1])
                }
                self.showAlert(To: RemoveMemberHeading.localized(), for: RemoveMemberMessage.localized()) { [weak self] _ in
                    guard let `self` = self else {return}
                    if index.section == 0 {
                        self.groupAdmins.remove(at: index.row)
                    } else {
                        self.groupMembers.remove(at: index.row)
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func onClickRequestedUserAction(_ index: IndexPath, isAccept: Bool){
        var member = self.groupRequestedMembers[index.row]
        var requestData = MembershipRequest()
        requestData.clinic_id = self.groupInfo.id
        requestData.user_id = member.user_id
        requestData.request_status = isAccept ? GroupStatus.APPVOVED.rawValue : GroupStatus.REJECTED.rawValue
        
        RequestManager.shared.updateGroupStatus(request: requestData) { (status, data) in
            if status {
                if isAccept {
                    self.groupRequestedMembers.remove(at: index.row)
                    member.request_status = GroupStatus.APPVOVED.rawValue
                    self.groupMembers.append(member)
                    self.tableView.reloadData()
                } else {
                    self.groupRequestedMembers.remove(at: index.row)
                    self.tableView.reloadData()
                }
                NotificationCenter.default.post(name: .notifyGroupRequest, object: nil, userInfo: nil)
            }
            else {
                self.pageNumber = 1
                self.loadMore = true
                self.getGroupUsers()
            }
            NotificationCenter.default.post(name: .notifyGroupRequest, object: nil, userInfo: nil)
        }
    }
    
    func makeAdmin(at index: IndexPath, role: GroupMemberRoles){
        let alertText = role == .ADMIN ? "txt_user_to_admin".localized() : "txt_admin_to_user".localized()
        self.showAlert(To: "", for: alertText, firstBtnStyle: .cancel, secondBtnStyle: .destructive, firstBtnTitle: "Yes".localized(), secondBtnTitle: "Cancel".localized()) { [weak self] (status) in
            guard let `self` = self else {return}
            if status {
                let member = index.section == 0 ? self.groupAdmins[index.row] : self.groupMembers[index.row]
                if let clinicId = self.groupInfo.id, let userId = member.user_id {
                    var requestData = MembershipRequest()
                    requestData.clinic_id = clinicId
                    requestData.user_id = userId
                    requestData.role = role
                    
                    RequestManager.shared.updateGroupStatus(request: requestData) { (status, data) in
                        if status {
                            if role == .ADMIN {
                                self.groupMembers.remove(at: index.row)
                                self.groupAdmins.append(member)
                                self.tableView.reloadData()
                            } else {
                                self.groupAdmins.remove(at: index.row)
                                self.groupMembers.append(member)
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
}

extension GroupMembersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 && self.groupAdmins.count == 0 {
            return 0
        }
        else if section == 1 && self.groupRequestedMembers.count == 0 {
            return 0
        }
        else if section == 2 && self.groupMembers.count == 0 {
            return 0
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = AppConfig.shared.isDarkMode ? AppColors.darkGrayColor : AppColors.mediumGrayColor
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        if section == 0 {
            label.text = "Administrators".localized().uppercased()
        } else if section == 1 {
            label.text = "Requests".localized().uppercased()
        } else {
            label.text = "Other Members".localized().uppercased()
        }
//        label.font = UIFont().futuraPTMediumFont(16)
        label.textColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
        label.font = AppFont.kMuliSemiBold(14).font()
        headerView.addSubview(label)
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
//        var sections = 0
//        if !self.groupAdmins.isEmpty {
//            sections += 1
//        }
//        if !self.groupMembers.isEmpty {
//            sections += 1
//        }
//        if !self.groupRequestedMembers.isEmpty, canApproveRequests {
//            sections += 1
//        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.groupAdmins.count
        }
        else if section == 1 {
            return self.groupRequestedMembers.count
        }
        else {
            return self.groupMembers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GroupMemberCell.self), for: indexPath) as! GroupMemberCell
        cell.groupInfo = self.groupInfo
        cell.indexPath = indexPath
        
        if indexPath.section == 0 {
            let member = self.groupAdmins[indexPath.row]
            cell.configCell(me: me!, member, hasAdmins: !self.groupAdmins.isEmpty, amIAdmin: self.amIAdmin, canLeave: self.groupInfo.can_leave_clinic ?? true)
        }
        else if indexPath.section == 1 {
            let member = self.groupRequestedMembers[indexPath.row]
            cell.configCell(me: me!, member, hasAdmins: false, amIAdmin: self.amIAdmin, canLeave: self.groupInfo.can_leave_clinic ?? true)
        }
        else {
            cell.configCell(me: me!, self.groupMembers[indexPath.row], hasAdmins: false, amIAdmin: self.amIAdmin, canLeave: self.groupInfo.can_leave_clinic ?? true)
            if self.loadMore && indexPath.row == (self.groupMembers.count - 1) {
                self.pageNumber += 1
                self.getGroupUsers(searchText: self.searchBar.text ?? "")
            }
        }
        
        cell.onDeleteMember = { [weak self] indexPath in
            guard let strongSelf = self else { return }
            strongSelf.deleteGroupMember(at: indexPath)
        }
        cell.onClickRequestedUserAction = { [weak self] (index, isAccept) in
            guard let strongSelf = self else { return }
            strongSelf.onClickRequestedUserAction(indexPath, isAccept: isAccept)
        }
        cell.makeAdmin = { [weak self] indexPath in
            guard let strongSelf = self else { return }
            
            strongSelf.makeAdmin(at: indexPath, role: indexPath.section == 0 ? .USER : .ADMIN)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var member = MemberModel()
        switch indexPath.section {
        case 0:
            member = self.groupAdmins[indexPath.row]
        case 1:
            member = self.groupRequestedMembers[indexPath.row]
        case 2:
            member = self.groupMembers[indexPath.row]
        default:
            break
        }
        
        if let memberId = member.user_id, memberId != AppConfig.shared.user.id {
            self.previewUser(id: memberId)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: GroupMemberCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GroupMemberCell.self))
        
        self.tableView.separatorStyle = .none
        if #available(iOS 15.0, *) {
            self.tableView.sectionHeaderTopPadding = 0.0
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let y = 170 - (scrollView.contentOffset.y)
//        let h = max(0, y)
//        self.topViewHight.constant = h
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = topViewHight.constant - y
        
        if newHeaderViewHeight > 170 {
            topViewHight.constant = 170
        } else if newHeaderViewHeight < 0 {
            topViewHight.constant = 0
        } else {
            topViewHight.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block scroll view
        }
    }
}

// MARK: - UISearchBar Delegate
extension GroupMembersVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.resignFirstResponder()
            self.searchBar.textField?.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.pageNumber = 1
            self.loadMore = true
            self.getGroupUsers()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
        let txt = searchBar.text?.trim ?? ""
        self.getGroupUsers(searchText: txt)
        self.searchBar.resignFirstResponder()
        
//        if let searchText = searchBar.textField?.text, searchText != ""{
//            var searchedMembers = GroupMemberModel()
//            searchedMembers.clinic_admins = self.groupAdmins.filter {
//                guard $0.phone != nil else {return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email != nil && $0.email!.lowercased().contains(searchText.lowercased())) }
//
//                return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.phone)!.contains(searchText.lowercased()) || ($0.email != nil && $0.email!.lowercased().contains(searchText.lowercased()))
//            }
//            searchedMembers.joined_users = self.groupMembers.filter {
//                guard $0.phone != nil else {return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email != nil && $0.email!.lowercased().contains(searchText.lowercased())) }
//
//                return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.phone)!.contains(searchText.lowercased()) || ( $0.email != nil && $0.email!.lowercased().contains(searchText.lowercased()))
//            }
//            searchedMembers.requested_users = self.groupRequestedMembers.filter {
//                guard $0.phone != nil else {return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.email != nil && $0.email!.lowercased().contains(searchText.lowercased())) }
//
//                return ($0.full_name)!.lowercased().contains(searchText.lowercased()) || ($0.phone)!.contains(searchText.lowercased()) || ($0.email != nil && $0.email!.lowercased().contains(searchText.lowercased()))
//            }
//            self.reloadData(members: searchedMembers)
//        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    }
}

extension GroupMembersVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        uiHeader.backgroundColor = theme.settings.cellColor
        uiHeader.subviews.first?.backgroundColor = theme.settings.highlightedBgColor
        lblHeader.textColor = theme.settings.titleColor
        lblMemberSince.textColor = theme.settings.subTitleColor
        btnDelete.setTitleColor(theme.settings.titleColor, for: .normal)
    }
}
