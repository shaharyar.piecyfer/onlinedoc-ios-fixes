//
//  GroupsVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 18/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class GroupsVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar:UISearchBar!
    
//    var groups = GroupData(){
//        didSet{
//            self.tableView.reloadData()
//        }
//    }
    var groupsData = [GroupModel]()
    var selectedIndex: IndexPath?
//    var responseGroups = GroupData()
    
    var pageNumber = 1
    var loadMore = true
    
    @objc private func refreshData(_ sender: Any) {
        self.pageNumber = 1
        self.loadMore = true
        self.searchBar.text = ""
        self.getGroupList()
        self.searchBar.resignFirstResponder()
//        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.getGroupList()
        self.layOutSearchbar()
        self.tableView.addSubview(refreshControl)
        self.navigationController?.navigationBar.backgroundColor = AppColors.tabBarTint
        self.setupNotificationObservers()
        self.setupTheme()
    }
    
    private func setupTheme() {
         Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    private func setupNotificationObservers(){
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(forName: .notifyGroup, object: nil, queue: .main) { [weak self] (notification) in
            guard let strongSelf = self else { return }
            if let group = notification.userInfo?["Group"] as? GroupModel {
                group.request_status = GroupStatus.APPVOVED.rawValue
                if let notify = notification.userInfo?["Notify"] as? NotifyCRUD {
                    switch notify {
                    case .create:
                        group.member_count = 1
                        strongSelf.groupsData.insert(group, at: 0)
                        strongSelf.tableView.reloadData()
                    case .update:
                        let indexToEdited = strongSelf.groupsData.firstIndex(where: {$0.id == group.id})
                        if let indexToEdited = indexToEdited {
                            group.member_count = group.joined_members
                            strongSelf.groupsData[indexToEdited] = group
                            strongSelf.tableView.reloadData()
                        }
                    case .delete:
                        let indexToDeleted = strongSelf.groupsData.firstIndex(where: {$0.id == group.id})
                        if let indexToDeleted = indexToDeleted {
                            strongSelf.groupsData.remove(at: indexToDeleted)
                            strongSelf.tableView.reloadData()
                        }
                    default:
                        break
                    }
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .notifyGroupMember, object: nil, queue: .main) { [weak self] (notification) in
            guard let `self` = self else {return}
            if let group = notification.userInfo?["Group"] as? GroupModel, let isAdded = notification.userInfo?["IsAdded"] as? Bool, let members = notification.userInfo?["Members"] as? Int {
                let indexToEdited = self.groupsData.firstIndex(where: {$0.id == group.id})
                if let indexToEdited = indexToEdited {
                    let desiredGroup = self.groupsData[indexToEdited]
                    if isAdded {
                        let count = (desiredGroup.joined_members ?? 0) + members
                        self.groupsData[indexToEdited].member_count = count
                    }
                    else {
                        let count = (desiredGroup.joined_members ?? 1) - 1
                        self.groupsData[indexToEdited].member_count = count
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    deinit {
        print("GroupsVC->deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    func layOutSearchbar(){
        self.searchBar.placeholder = SearchText.localized
        self.searchBar.delegate = self
//        self.searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
//        if let searchTextField = self.searchBar.textField{
//            searchTextField.textColor = .white
//            searchTextField.keyboardAppearance = .dark
//        }
    }
    
    func getGroupList(searchText: String = "") {
        if self.pageNumber == 1 {
            self.groupsData.removeAll()
            self.tableView.reloadData()
        }
        self.tableView.setEmptyMessage("")
        RequestManager.shared.getGroupList(page: self.pageNumber, searchText: searchText) { (status, response) in
            self.refreshControl.endRefreshing()
            if status {
                if let response = response as? GroupListResult {
                    if let data = response.data, data.isEmpty, self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                        self.tableView.reloadData()
                    }
                    if let dataSource = response.data {
                        self.groupsData.append(contentsOf: dataSource)
                        if dataSource.count == 0 {
                            self.loadMore = false
                        }
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func groupInfo(_ id:Int){
        AppConfig.shared.NewGroupId = 0
        if let vc = getVC(storyboard: .TABS, vcIdentifier: "GroupInfoScene") as? GroupInfoVC {
            vc.groupById = id
            self.navigateVC(vc)
        }
    }
    
    @IBAction func createGroup(){
        self.performSegue(withIdentifier: "SegueToCreateNewGroup", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueToCreateNewGroup"{
            if let vc = segue.destination as? NewGroupVC {
                vc.fromExplore = true
            }
        }
    }
}

extension GroupsVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.resignFirstResponder()
            self.searchBar.textField?.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.pageNumber = 1
            self.loadMore = true
            self.getGroupList()
        }
    }
 
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
        let txt = searchBar.text?.trim ?? ""
        self.getGroupList(searchText: txt)
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
    }
}

extension GroupsVC: GroupViewCellDelegate {
    func didTapGroupInfo(id: Int, index: IndexPath) {
        self.selectedIndex = index
        if let vc = getVC(storyboard: .TABS, vcIdentifier: "GroupInfoScene") as? GroupInfoVC {
            vc.groupById = id
//            vc.delegate = self
            vc.navigatingFrom = .groups
            vc.needToReload = { [weak self] need in
                guard let `self` = self else {return}
                if need {
                    self.getGroupList()
                }
            }
            self.navigateVC(vc)
        }
    }
    
    func didTapCancel(id: Int, index: IndexPath) {
        var requestData = MembershipRequest()
        let record = self.groupsData[index.row]
        if let _ = record.id {
            requestData.clinic_id = record.id
            requestData.user_id = AppConfig.shared.user.id
            requestData.request_status = GroupStatus.CANCELLED.rawValue
            
            RequestManager.shared.updateGroupStatus(request: requestData) { (status, data) in
                if status {
                    let group = self.groupsData[index.row]
                    group.request_status = GroupStatus.OTHER.rawValue
                    self.groupsData[index.row] = group
                    self.tableView.reloadData()
                    self.showAlert(To: GroupRequestHeading.localized(), for: CancelGroupRequest.localized()) {_ in}
                }
            }
        }
    }
    
    func didTapRequestMembership(id: Int, index: IndexPath) {
        let record = self.groupsData[index.row]
        if let _ = record.id {
            RequestManager.shared.joinGroup(id: record.id!) { (status, data) in
                if status{
                    let group = self.groupsData[index.row]
                    group.request_status = GroupStatus.REQUESTED.rawValue
                    self.groupsData[index.row] = group
                    self.tableView.reloadData()
                    self.showAlert(To: RequestMembership.localized(), for: GroupRequestMessage.localized()) {_ in}
                }
            }
        }
    }
}

extension GroupsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCell") as? GroupViewCell
            else {
                return GroupViewCell()
        }
        if indexPath.row < self.groupsData.count {
            cell.index = indexPath
            cell.groupId = self.groupsData[indexPath.row].id
            cell.setCell(self.groupsData[indexPath.row])
            if self.loadMore && indexPath.row == (self.groupsData.count - 1){
                self.pageNumber += 1
                self.getGroupList(searchText: searchBar.text?.trim ?? "")
            }
            
            cell.delegate = self
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 55
//    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func setupTBL() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: GroupViewCell.self), bundle: nil), forCellReuseIdentifier: "GroupCell")
        
        self.tableView.separatorStyle = .none
        
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }
}

struct MembershipRequest {
    var clinic_id : Int!
    var user_id : Int!
    var request_status = GroupStatus.CANCELLED.rawValue
    var role : GroupMemberRoles?
}

struct GroupData {
    var userGroups: [GroupModel] = []
    var requestGroups: [GroupModel] = []
    var otherGroups: [GroupModel] = []
    var userGroupsVisiblity = true
    var requestedGroupsVisiblity  = true
    var otherGroupsVisiblity  = true
}
