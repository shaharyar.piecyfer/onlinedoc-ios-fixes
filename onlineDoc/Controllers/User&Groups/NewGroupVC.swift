//
//  NewGroupVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 18/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import RAGTextField
import BEMCheckBox

class NewGroupVC: BaseVC {
    @IBOutlet weak var groupImageView: UIImageView!{
        didSet{
            groupImageView.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.selectImage))
            groupImageView.addGestureRecognizer(tapGesture)
            groupImageView.layer.cornerRadius = groupImageView.bounds.size.height / 2
            groupImageView.clipsToBounds = true
            groupImageView.layer.borderWidth = 0.5
            groupImageView.layer.borderColor = AppColors.mediumGrayColor.cgColor
        }
    }
    
    @IBOutlet weak var publicCheckView:UIView!{
        didSet{
            publicCheckView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.publicViewTapped)))
        }
    }
    @IBOutlet weak var privateCheckView: UIView!{
        didSet{
            privateCheckView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.privateViewTapped)))
        }
    }
    
//    @IBOutlet weak var uiMail: UIView!
    @IBOutlet weak var uiMembership: UIView!
    
//    @IBOutlet weak var mailAdminLabel: UILabel!{
//        didSet{
//            mailAdminLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.mailAdminTapped)))
//        }
//    }
//
//    @IBOutlet weak var mailMemberLabel: UILabel!{
//        didSet{
//            mailMemberLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.mailMemberTapped)))
//        }
//    }
    
    @IBOutlet weak var membershipAdminLabel: UILabel!{
        didSet{
            membershipAdminLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.membershipAdminTapped)))
        }
    }
    
    @IBOutlet weak var membershipMemberLabel: UILabel!{
        didSet{
            membershipMemberLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.membershipMemberTapped)))
        }
    }
    
    @IBOutlet weak var groupTitleField:RAGTextField!{
        didSet{
            groupTitleField.setUnderlineField()
        }
    }
    
    @IBOutlet weak var publicCheckBox: BEMCheckBox!{
        didSet{
            publicCheckBox.boxType = .circle
        }
    }
    
    @IBOutlet weak var privateCheckBox: BEMCheckBox!{
        didSet{
            privateCheckBox.boxType = .circle
        }
    }
    
//    @IBOutlet weak var mailAdminCheckBox: BEMCheckBox!{
//        didSet{
//            mailAdminCheckBox.boxType = .circle
//        }
//    }
//
//    @IBOutlet weak var mailMembersCheckBox: BEMCheckBox!{
//        didSet{
//            mailMembersCheckBox.boxType = .circle
//        }
//    }
    
    @IBOutlet weak var membershipAdminCheckBox: BEMCheckBox!{
        didSet{
            membershipAdminCheckBox.boxType = .circle
        }
    }
    
    @IBOutlet weak var membershipMembersCheckBox: BEMCheckBox!{
        didSet{
            membershipMembersCheckBox.boxType = .circle
        }
    }
    
    @IBOutlet weak var groupDescriptionTextView: UITextView!{
        didSet{
            groupDescriptionTextView.text = GroupDescriptionPlaceholder.localized()
        }
    }
    
    var privacyGroup = BEMCheckBoxGroup()
    var groupMailOptionsGroup = BEMCheckBoxGroup()
    var groupMembershipOptionsGroup = BEMCheckBoxGroup()
    var groupImageData: Data?
    var newGroupData = NewGroupModel()
    var directUploadParams = DirectUploadParams()
    var fromExplore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.layoutNavBar()
        self.layoutCheckBoxGroups()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTap)))
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: NewGroupVC.setTheme)
        
        let labels: [UILabel] = AppConfig.shared.getSubviewsOf(view: self.view, tag: 222)
        for label in labels {
            Themer.shared.register(
                target: label,
                action: UILabel.setTheme)
        }
        
        let images: [UIImageView] = AppConfig.shared.getSubviewsOf(view: self.view, tag: 333)
        for image in images {
            image.image = image.image?.withRenderingMode(.alwaysTemplate)
            Themer.shared.register(
                target: image,
                action: UIImageView.setTheme)
        }
    }
    
    deinit {
        print("NewGroupVC->deinit")
    }
    
    @objc func viewTap(){
        self.view.endEditing(true)
    }
    
    func layoutNavBar(){
        self.title = "CREATE GROUP".localized()
        let nextButton = UIBarButtonItem(title: NextText.localized, style: .plain, target: self, action: #selector(gotoCatogotiresList))
        self.navigationItem.rightBarButtonItem  = nextButton
    }
    
    func layoutCheckBoxGroups() {
        self.privacyGroup = BEMCheckBoxGroup(checkBoxes: [publicCheckBox, privateCheckBox])
        self.privacyGroup.selectedCheckBox = publicCheckBox
        self.privacyGroup.mustHaveSelection = true
        
//        self.groupMailOptionsGroup = BEMCheckBoxGroup(checkBoxes: [mailAdminCheckBox, mailMembersCheckBox])
//        self.groupMailOptionsGroup.selectedCheckBox = mailAdminCheckBox
        self.groupMailOptionsGroup.mustHaveSelection = true
        
        self.groupMembershipOptionsGroup = BEMCheckBoxGroup(checkBoxes: [membershipAdminCheckBox, membershipMembersCheckBox])
        self.groupMembershipOptionsGroup.selectedCheckBox = membershipAdminCheckBox
        self.groupMembershipOptionsGroup.mustHaveSelection = true
        
    }
    
    @objc func publicViewTapped(){
        self.privateCheckBox.on = false
        self.publicCheckBox.on = true
    }
    
    @objc func privateViewTapped(){
        self.publicCheckBox.on = false
        self.privateCheckBox.on = true
    }
    
//    @objc func mailAdminTapped(){
//        self.mailMembersCheckBox.on = false
//        self.mailAdminCheckBox.on = true
//    }
    
//    @objc func mailMemberTapped(){
//        self.mailAdminCheckBox.on = false
//        self.mailMembersCheckBox.on = true
//    }
    
    @objc func membershipAdminTapped(){
        self.membershipMembersCheckBox.on = false
        self.membershipAdminCheckBox.on = true
    }
    
    @objc func membershipMemberTapped(){
        self.membershipAdminCheckBox.on = false
        self.membershipMembersCheckBox.on = true
    }
    
    @objc func gotoCatogotiresList(){
        if let title = self.groupTitleField.text, title != ""{
            self.newGroupData.name = title
        }else{
            self.showAlert(To: "", for: "Group Title is required to proceed with new group creations".localized()) {_ in}
            return
        }
        
        if let description = self.groupDescriptionTextView.text, description != "" && description != GroupDescriptionPlaceholder.localized(){
            self.newGroupData.description = description
        }
        
        let privacySelection = self.privacyGroup.selectedCheckBox
        self.newGroupData.is_public = privacySelection == publicCheckBox ? true : false
        
//        let groupMailSelection = self.groupMailOptionsGroup.selectedCheckBox
//        self.newGroupData.groupMail = groupMailSelection == mailAdminCheckBox ? "A" : "E"
        
        let membershipApprovalSelction = self.groupMembershipOptionsGroup.selectedCheckBox
        self.newGroupData.can_approve = membershipApprovalSelction == membershipAdminCheckBox ? WhoCanUpdate.ADMIN.rawValue : WhoCanUpdate.EVERYONE.rawValue
        
        self.performSegue(withIdentifier: "SegueToNewGroupCategories", sender: self)
    }
    
    @objc func selectImage(){
        self.showDialogAlert(options: .camera, .library, singleSelection: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueToNewGroupCategories"{
            let vc = segue.destination as? CategoriesVC
            vc?.fromExplore = self.fromExplore
            vc?.groupData = self.newGroupData
            vc?.groupImageData = self.groupImageData
            vc?.directUploadParams = self.directUploadParams
        }
    }
}

extension NewGroupVC {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        var capturedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        capturedImage = self.imageOrientation(capturedImage)
        self.groupImageView.image = capturedImage
        
        var fileName = UUID().uuidString + ".jpeg"
        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
            fileName = url.lastPathComponent
//          fileType = url.pathExtension
        }
        
        groupImageData = capturedImage.jpegData(compressionQuality: capturedImage.size.width > 500 ? 0.5 : 1.0) ?? Data(capturedImage.sd_imageData()!)
        let imageSize: Int = groupImageData!.count
        
        directUploadParams.filename = fileName
        directUploadParams.content_type = groupImageData!.fileExtension
        directUploadParams.byte_size = imageSize
        
//        print("ProfileVC->directUploadParams", directUploadParams)
//        RequestManager.shared.directUpload(_params: directUploadParams, data: groupImageData!) { (status, result) in
//            if status {
//                if let data = result as? DirectUploadModel {
//                    self.directUploadInfo = data
//                }
//            }
//        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension NewGroupVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == groupTitleField {
            self.groupDescriptionTextView.becomeFirstResponder()
        }
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        true
    }
}

extension NewGroupVC: UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if (textView == groupDescriptionTextView) {
            return true
        }
        return false
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == self.groupDescriptionTextView{
            if let text = textView.text, text == GroupDescriptionPlaceholder.localized(){
                textView.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == self.groupDescriptionTextView{
            if let text = textView.text{
                if text == ""{
                    textView.text = GroupDescriptionPlaceholder.localized()
                    textView.resignFirstResponder()
                }
            }
        }
        self.view.endEditing(true)
    }
}

extension NewGroupVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        publicCheckView.backgroundColor = theme.settings.highlightedBgColor
        privateCheckView.backgroundColor = theme.settings.highlightedBgColor
        groupTitleField.textColor = theme.settings.titleColor
        groupDescriptionTextView.textColor = theme.settings.subTitleColor
//        uiMail.backgroundColor = theme.settings.highlightedBgColor
        uiMembership.backgroundColor = theme.settings.highlightedBgColor
    }
}

extension UILabel {
    fileprivate func setTheme(_ theme: MyTheme) {
        self.textColor = theme.settings.subTitleColor
    }
}

extension UIImageView {
    fileprivate func setTheme(_ theme: MyTheme) {
        self.tintColor = theme.settings.titleColor
    }
}
