//
//  UserListVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 13/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class UserListVC: BaseVC {
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchBar:UISearchBar!
    var friends = [MemberModel]()
    var otherUsers = [MemberModel]()
    
    //MARK: - Pagination Variables
    var pageNumber = 1
    var loadMore = true
    
    var activityIndicator = UIActivityIndicatorView()
    var memmberAtIndex = -1
//    var selectedIndex = 0
//    var selectedSection = 0
    var memberIndex = 0
    var selectedMember = Member()
    
    @objc private func refreshData(_ sender: Any) {
        self.pageNumber = 1
        self.searchBar.text = ""
        self.loadMore = true
        self.loadFriendList()
        self.searchBar.resignFirstResponder()
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        self.loadFriendList()
        self.layOutSearchbar()
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self.searchBar,
            action: UISearchBar.applyTheme)
    }
    
    deinit {
        print("UserListVC->deinit")
    }
    
    func layOutSearchbar(){
        self.searchBar.placeholder = SearchText.localized
        self.searchBar.delegate = self
//        self.searchBar.setImage(#imageLiteral(resourceName: "searchMini"), for: .search, state: .normal)
//        if let searchTextField = self.searchBar.textField{
//            searchTextField.textColor = .white
//            searchTextField.keyboardAppearance = .dark
//        }
    }
    
    func loadFriendList(searchText: String = ""){
        if self.pageNumber == 1 {
            self.friends.removeAll()
            self.otherUsers.removeAll()
        }
        self.tableView.setEmptyMessage("")
        RequestManager.shared.getfriendsWithOtherUsers(page: self.pageNumber, searchText: searchText) { (status, response) in
            if status {
                if let res = response as? FriendListResponse {
                    if res.friends.isEmpty && res.other_users.isEmpty && self.pageNumber == 1 {
                        self.tableView.setEmptyMessage(noRecordFound.localized())
                    }
                    if self.pageNumber == 1 {
                        if !searchText.isEmpty {
                            self.friends = res.friends.filter {
                                guard $0.phone != nil else {
                                    return ($0.full_name?.lowercased().contains(searchText.lowercased()) ?? false) || ($0.email?.lowercased().contains(searchText.lowercased()) ?? false)
                                }
                                return ($0.full_name?.lowercased().contains(searchText.lowercased()) ?? false) || ($0.email?.lowercased().contains(searchText.lowercased()) ?? false) || ($0.phone?.lowercased().contains(searchText.lowercased()) ?? false)
                                
                            }
                        } else {
                            self.friends = res.friends
                        }
                    }
                    self.otherUsers.append(contentsOf: res.other_users)
                    if res.other_users.count == 0 {
                        self.loadMore = false
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func navigateToChat(user: MemberModel){
        if let vc = getVC(storyboard: .CONNECTIOS, vcIdentifier: "ChatScene") as? ChatVC {
            if let roomId = user.chatRoomId {
                vc.chatId = roomId
            }
            else {
                var receiver = User()
                receiver.id = user.user_id
                receiver.full_name = user.full_name
                vc.receiver = receiver
            }
            self.navigateVC(vc)
        }
    }
}

extension UserListVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.friends.count
        case 1:
            return self.otherUsers.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell") as? MemberInfoTableViewCell else {
            return self.emptyTblCell()
        }
        cell.index = indexPath
        switch indexPath.section {
        case 0:
            if self.friends.count > indexPath.row {
                cell.setCell(self.friends[indexPath.row], indexPath: indexPath)
            }
            break
        case 1:
            if self.otherUsers.count > indexPath.row {
                cell.setCell(self.otherUsers[indexPath.row], indexPath: indexPath)
                if self.loadMore && indexPath.row == (self.otherUsers.count - 2){
                    self.pageNumber += 1
                    self.activityIndicator.startAnimating()
                    self.loadFriendList(searchText: searchBar.text?.trim ?? "")
                }
            }
            break
        default:
            break
        }
        
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: String(describing: MemberInfoTableViewCell.self), bundle: nil), forCellReuseIdentifier: "MemberCell")
        
        self.tableView.separatorStyle = .none
        
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }
}

extension UserListVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.resignFirstResponder()
            self.searchBar.textField?.resignFirstResponder()
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.textField?.resignFirstResponder()
            }
            self.pageNumber = 1
            self.loadMore = true
            self.loadFriendList()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
        let txt = searchBar.text?.trim ?? ""
        self.loadFriendList(searchText: txt)
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.pageNumber = 1
        self.loadMore = true
    }
}

extension UserListVC: MemberInfoTableViewCellDelegate {
    func sendRequest(index: IndexPath) {
        var member = MemberModel()
        var request_status = FriendStatus.REQUEST
        switch index.section {
        case 0:
            member = self.friends[index.row]
            request_status = FriendStatus.CANCEL
            break
        case 1:
            member = self.otherUsers[index.row]
            request_status = FriendStatus.REQUEST
            break
        default:
            break
        }
        
        var params = FriendRequestModel()
        params.receiver_id = member.user_id!
        params.request_status = request_status
        self.friendRequest(params: params) { (status, response) in
            if status {
                switch index.section {
                case 0:
                    self.friends.remove(at: index.row)
                    member.request_status = nil
                    self.otherUsers.append(member)
                    break
                case 1:
                    self.otherUsers.remove(at: index.row)
                    member.request_status = FriendStatus.REQUEST.rawValue
                    self.friends.append(member)
                    break
                default:
                    break
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func didRequestInvitation(index: IndexPath) {
        var member = MemberModel()
        switch index.section {
        case 0:
            member = self.friends[index.row]
            break
        case 1:
            member = self.otherUsers[index.row]
            break
        default:
            break
        }
        
        var user = User()
        user.id = member.user_id
        user.email = member.email
        user.name = member.full_name
        
        if let vc = getVC(storyboard: .INVITATION, vcIdentifier: String(describing: InviteVC.self)) as? InviteVC {
            vc.invitationType = .member
            vc.isFromUserListVC = true
            vc.selectedMembers = [user]
            self.navigateVC(vc)
        }
    }
    
    func didTapShowProfile(index: IndexPath) {
        var userId = 0
        switch index.section {
        case 0:
            userId = self.friends[index.row].user_id!
            break
        case 1:
            userId = self.otherUsers[index.row].user_id!
            break
        default:
            break
        }
        if let vc = getVC(storyboard: .PROFILE, vcIdentifier: "MemberProfileScene") as? MemberProfileVC {
            vc.memberId = userId
            vc.onUserUpdate = { [weak self] user in
                guard let strongSelf = self else { return }
                if index.section == 0 {
                    strongSelf.friends[index.row].request_status = user.friend_status?.requestStatus
                } else {
                    strongSelf.otherUsers[index.row].request_status = user.friend_status?.requestStatus
                }
                strongSelf.tableView.reloadRows(at: [index], with: .automatic)
            }
            self.navigateVC(vc)
        }
    }
    
    func didTapSendMessage(index: IndexPath) {
        let user = index.section == 0 ? friends[index.row] : otherUsers[index.row]
        self.navigateToChat(user: user)
    }
}
