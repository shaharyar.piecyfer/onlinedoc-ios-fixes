//
//  UsersAndGroupsVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 14/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

class UsersAndGroupsVC: BaseVC {
    @IBOutlet weak var userTabButton: UIButton! {
        didSet {
            let image = UIImage(named: "UsersActive")?.withRenderingMode(.alwaysTemplate)
            userTabButton.setImage(image, for: .normal)
        }
    }
    @IBOutlet weak var groupsTabButton: UIButton! {
        didSet {
            let image = UIImage(named: "Groups")?.withRenderingMode(.alwaysTemplate)
            groupsTabButton.setImage(image, for: .normal)
        }
    }
    @IBOutlet var tabButtons: [UIButton]!
    
    @IBOutlet weak var usersTabComponent: UIView!
    @IBOutlet weak var groupTabComponent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.navigationBar.barStyle = .black
        self.layOutTabButtons()
        self.manageTabView()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.setupTheme()
    }
    
    deinit {
        print("UsersAndGroupsVC->deinit")
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: UsersAndGroupsVC.setTheme)
    }
    
    func layOutTabButtons()  {
        self.userTabButton.roundCorners(corners: [.topRight, .bottomRight], radius: 5.0)
        self.userTabButton.backgroundColor = AppColors.setInActive
        
        self.groupsTabButton.roundCorners(corners: [.topLeft, .bottomLeft], radius: 5.0)
        self.groupsTabButton.backgroundColor = AppColors.setActive
    }
    
    func setTabSelection(_ sender:UIButton) {
        for button in tabButtons{
            if button == sender{
                button.alpha = 1
            }else{
                button.alpha = 0.5
            }
        }
    }
    
    func manageTabView(tabNumer: Int = 1){
        switch tabNumer {
        case 1:
            self.usersTabComponent.alpha = 0
            self.groupTabComponent.alpha = 1
        default:
            self.groupTabComponent.alpha = 0
            self.usersTabComponent.alpha = 1
        }
    }
    
    //MARK: - Action Methodss
    @IBAction func setTabButton(_ sender:UIButton){
        self.setTabSelection(sender)
        if  sender.tag == 1{
            self.manageTabView()
        }else{
            self.manageTabView(tabNumer: sender.tag)
        }
    }
}

extension UsersAndGroupsVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        userTabButton.backgroundColor = theme.settings.highlightedBgColor
        userTabButton.tintColor = theme.settings.subTitleColor
        userTabButton.setTitleColor(theme.settings.titleColor, for: .normal)
        groupsTabButton.backgroundColor = theme.settings.highlightedBgColor
        groupsTabButton.tintColor = theme.settings.subTitleColor
        groupsTabButton.setTitleColor(theme.settings.titleColor, for: .normal)
    }
}
