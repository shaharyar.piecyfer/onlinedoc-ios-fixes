//
//  SelfSizingTableView.swift
//  onlineDoc
//
//  Created by Piecyfer on 27/02/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit

class SelfSizingTableView: UITableView {

    var maxHeight: CGFloat = UIScreen.main.bounds.size.height
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
        self.layoutIfNeeded()
    }
    
    override var intrinsicContentSize: CGSize {
        let height = min(contentSize.height, maxHeight)
        return CGSize(width: contentSize.width, height: height)
    }

}
