//
//  AppColors.swift
//  onlineDoc
//
//  Created by Piecyfer on 07/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

enum AppColors {
    static let blackColor =  UIColor(hex: "#000000")
    static let whiteColor =  UIColor(hex: "#ffffff")
    static let extraLightGrayColor =  UIColor(hex: "#fafafa")
    static let lightGrayColor =  UIColor(hex: "#838388")
    static let mediumGrayColor =  UIColor(hex: "#eeeef0")
    static let darkGrayColor =  UIColor.darkGray
    static let grayColor =  UIColor(hex: "#b2b2b2")
    static let borderColor =  UIColor(hex: "#adadad")
    static let blueColor =  UIColor(hex: "#039be5")
    static let lightblueColor =  UIColor(hex: "#6ec3ec")
    static let redColor =  UIColor(hex: "#FF3B30")
    
    
    static let barTint =  UIColor(hex: "#19222AFF")
    static let gray = UIColor(hex: "#808080")
    static let tabBarTint = UIColor(hex: "#19222AFF")
    static let chalk = UIColor(hex: "#ECF0F1FF")
    static let underline = UIColor(hex: "#9E9E9FFF")
    static let underBar = UIColor(hex: "#9E9E9FFF")
    static let sky = UIColor(hex: "#3498DBFF")
    static let meadow = UIColor(hex: "#2ECC71FF")
    static let savanna = UIColor(hex: "#E67E22FF")
    static let bramble = UIColor(hex: "#9B59B6FF")
    static let midnight = UIColor(hex: "#d7d9dbFF")
    static let stone = UIColor(hex: "#95A5A6FF")
    static let star = UIColor(hex: "#F1C40FFF")
    static let setActive = UIColor(hex: "#033256FF")
    static let setInActive = UIColor(hex: "#0B141CFF")
    static let tabInActive = UIColor(hex: "#C3C3C3FF")
    static let clear = UIColor.clear
    static let destructive = UIColor(hex: "#A8452DFF")
    static let choice = UIColor(hex: "#1B7BE1FF")
    static let rowActive = UIColor(hex: "#FFFFFF")
    static let rowInactive = UIColor(hex: "#000F1AFF")
    static let oddChoice = UIColor(hex: "#FFFFFF05")
    static let evenChoice  = UIColor(hex: "#FFFFFF1F")
    static let grayish = UIColor(hex: "#1D1D1DFF")
    static let appGreen = UIColor(hex: "#058D02FF")
    static let critical = UIColor(hex: "#fb5b5bFF")
    static let important = UIColor(hex: "#ffcc00FF")
    static let notice = UIColor(hex: "#64CEF7FF")
    static let light = UIColor(hex: "#2D353FFF")
    //MARK: - COLORS WITH OPACITY
    static let defaultBlue = UIColor(red: 0/255, green: 138/255, blue: 236/255, alpha: 1)
    static let evenRow = UIColor(hex: "#19222AFF")
//       static let evenRow = UIColor(red: 13/255, green: 124/255, blue: 213/255, alpha: 0.63 )
     static let evenRowWithAlpha = UIColor(red: 13/255, green: 124/255, blue: 213/255, alpha: 1 )
    static let oddRow = UIColor(hex: "#19222AFF")
//    static let oddRow = UIColor(red: 27/255, green: 123/255, blue: 225/255, alpha: 0.86)
    static let oddRowWithAlpha = UIColor(red: 27/255, green: 123/255, blue: 225/255, alpha: 1)
    
}
