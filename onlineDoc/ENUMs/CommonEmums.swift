//
//  CommonEmums.swift
//  onlineDoc
//
//  Created by Piecyfer on 25/06/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation

enum Storyboard: String {
    case MAIN = "Main"
    case TABS = "Tabs"
    case PROFILE = "Profile"
    case USER_AND_GROUPS = "userAndGroups"
    case CONNECTIOS = "Connections"
    case NOTIFICATIONS = "Notifications"
    case HOME_TAB = "HomeTab"
    case CREATE_THREAD = "CreateThread"
    case INVITATION = "Invitation"
    case ALERT = "Alert"
    case SEARCH = "Search"
    
    func board()->String{
        return self.rawValue
    }
}

enum AttachmentOption {
    case library
    case camera
    case docs
    case video
    case scan
    case none
}

enum CommentTypes: String {
    case comment
    case file
}

enum MessageTypes: String {
    case message
    case file
}

enum CommentWith{
    case cam
    case smiley
    case keyboard
    case none
}
enum CreateOrEditThreadFrom{
    case home
    case profile
    case detail
    case group
    case none
}

enum NotifyCRUD {
    case create
    case update
    case delete
    case read
}

enum SupportedLanguages: String {
    case ENGLISH = "en"
    case DANISH = "da"
}

enum NewChatSelections {
    case CONTACTS
    case PHONE
    case EMAIL
}

enum PostTypes: String {
    case THREAD = "thread"
    case TASK = "task"
}

enum TaskActivityTypes: String {
    case CREATED = "doc_created"
    case EDITED = "doc_edited"
    case COMMENT_ADDED = "comment_added"
    case FILE_ADDED = "file_added"
    
    func value() -> String{
        return self.rawValue
    }
    
    func msg() -> String {
        switch self {
        case .CREATED:
            return "Task created".localized()
        case .EDITED:
            return "Task was edited".localized()
        case .COMMENT_ADDED:
            return "New comment".localized()
        case .FILE_ADDED:
            return "New file added".localized()
        }
    }
}
