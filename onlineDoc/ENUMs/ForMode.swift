//
//  ForMode.swift
//  onlineDoc
//
//  Created by Saqlain on 17/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
enum FormMode{
    case New
    case Update
}
