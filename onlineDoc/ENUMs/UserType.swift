//
//  UserType.swift
//  onlineDoc
//
//  Created by Piecyfer on 26/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import Foundation

enum UserType:String {
    
    case Doctor         = "doctor"
    case HealthStaff    = "healthcare_professional"
    case StandardUser   = "standard_user"

}

enum FormMode {
    case new
    case edit
}


enum OnlineMode : String {
     case ONLINE = "online", AWAY = "away", OFFLINE = "offline"
}

enum Priority: String{
    case normal = "normal"
    case critical = "critical"
    case important = "important"
    case notice = "notice"
    case none = ""
}

enum MessageType{
    case message, directmessage, friendRequest, thread, none
}

enum RecordType: String{
    case friend, request, group
}

enum VisitFrom{
    case profile, detailPage, home, groups, none
}
enum RequestType
{
    case send, cancel, accept, none
}
enum FriendRequestType{
    case sent, recieved, none
}

enum GroupStatus: String {
    case REQUESTED = "requested"
    case APPVOVED = "approved"
    case REJECTED = "rejected"
    case DIALOG = "dialog"
    case CANCELLED = "cancelled"
    case DELETED = "user_deleted"
    case DEACTIVATED = "user_deactivated"
    case LEAVE = "leave"
    case OTHER = "other"
}

enum GroupMemberRoles: String {
    case SUPER_ADMIN = "SuperAdmin"
    case ADMIN = "Admin"
    case USER = "User"
}

enum WhoCanUpdate: String {
    case SUPER_ADMIN = "SuperAdmin"
    case ADMIN = "Admin"
    case EVERYONE = "Everyone"
}

enum FriendStatus: String {
    case REQUEST = "request"
    case ACCEPT = "accept"
    case CANCEL = "cancel"
}

enum NotificationTypes: String {
    case THREAD = "thread"
    case CHAT = "message"
    case TASK = "task"
    case GROUP_REQUEST = "group_request"
}
