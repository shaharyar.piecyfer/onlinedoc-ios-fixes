//
//  Data.swift
//  onlineDoc
//
//  Created by Piecyfer on 07/07/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation

extension Data {
    var fileExtension: String {
        var values = [UInt8](repeating:0, count:1)
        self.copyBytes(to: &values, count: 1)
        
        let ext: String
        switch (values[0]) {
        case 0xFF:
            ext = "image/jpeg";
        case 0x89:
            ext = "image/png";
        case 0x47:
            ext = "image/gif";
        case 0x49, 0x4D :
            ext = "image/tiff";
        default:
            ext = "image/png";
        }
        return ext
    }
}
