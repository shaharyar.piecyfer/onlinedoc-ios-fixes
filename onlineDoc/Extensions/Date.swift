//
//  Date.swift
//  onlineDoc
//
//  Created by Piecyfer on 18/02/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
extension Date {
     
     func getTime() -> String {
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd_HH-mm-ss-SSSS"
        let dateString = formatter.string(from: now)
        return dateString
    }
    
    func getMoment() -> String {
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: Date(), to: self)
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + YearText.localized() : "\(year)" + " " + YearText.localized()
        } else if let year = interval.year, year < 0 {
            var yearStr = String(year)
            yearStr = String(yearStr.dropFirst())
        return yearStr == "1" ? "\(yearStr)" + " " + YearText.localized() : "\(yearStr)" + " " + YearText.localized()
        }
        else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + MonthText.localized() : "\(month)" + " " + MonthsText.localized()
        }
        else if let month = interval.month, month < 0 {
            var monthStr = String(month)
            monthStr = String(monthStr.dropFirst())
            return monthStr == "1" ? "\(monthStr)" + " " + MonthText.localized() : "\(monthStr)" + " " + MonthsText.localized()
        }
            
            
        else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + DayText.localized() : "\(day)" + " " + DaysText.localized()
        } else if let day = interval.day, day < 0 {
            var dayStr = String(day)
            dayStr = String(dayStr.dropFirst())
            return dayStr == "1" ? "\(dayStr)" + " " + DayText.localized() : "\(dayStr)" + " " + DaysText.localized()
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + HourText.localized() : "\(hour)" + " " + HoursText.localized()
        } else if let hour = interval.hour, hour < 0 {
            var hrStr = String(hour)
            hrStr = String(hrStr.dropFirst())
            return hrStr == "1" ? "\(hrStr)" + " " + HourText.localized() : "\(hrStr)" + " " + HoursText.localized()
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + MinuteText.localized() : "\(minute)" + " " + MinutesText.localized()
        } else if let minute = interval.minute, minute < 0 {
            var minStr = String(minute)
                       minStr = String(minStr.dropFirst())
        return minStr == "1" ? "\(minStr)" + " " + MinuteText.localized() : "\(minStr)" + " " + MinutesText.localized()
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + SecondText.localized() : "\(second)" + " " + SecondsText.localized()
        } else {
            return MomentText.localized()
        }
    }
    
    func formatDate(format: String?) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = format ?? "dd/MM/yyyy"
        let stringFromDate = dateFormater.string(from: self)
        return stringFromDate
    }
    
    public func toString(_ dateFormatter: DateFormatter, secondsFromGMT: Int?=nil) -> String {
        if let secondsFromGMT = secondsFromGMT {
            dateFormatter.timeZone = TimeZone(secondsFromGMT: secondsFromGMT)
        } else {
            dateFormatter.timeZone = TimeZone.current
        }
        
        return dateFormatter.string(from: self)
    }
    
    public func toString(dateFormat: String="dd-MM-yyyy HH:mm:ss", secondsFromGMT: Int?=nil) -> String {
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        return toString(dateFormatter, secondsFromGMT: secondsFromGMT)
    }
    
    public func stringWithTimeZone(_ dateFormatter: DateFormatter, secondsFromGMT: Int) -> String {
        dateFormatter.timeZone   = TimeZone(secondsFromGMT: secondsFromGMT)
        
        return dateFormatter.string(from: self)
    }
    
    public func stringWithTimeZone(dateFormat: String="dd-MM-yyyy HH:mm:ss", secondsFromGMT: Int) -> String {
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        return stringWithTimeZone(dateFormatter, secondsFromGMT: secondsFromGMT)
    }
}
