//
//  Dictionary.swift
//  onlineDoc
//
//  Created by Piecyfer on 17/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
extension Dictionary {
    var queryString: String {
        var output: String = ""
        for (key,value) in self {
            output +=  "\(key)=\(value)&"
        }
        output = String(output.dropLast(1))
        print(output)
        return output
    }
    
    public func toJSON(options: JSONSerialization.WritingOptions = []) throws -> String {
        let data = try JSONSerialization.data(withJSONObject: self, options: options)
        guard let string = String(data: data, encoding: .utf8) else { fatalError("Can't convert data to string") }
        
        return string
    }
}
