//
//  RAGTextField.swift
//  onlineDoc
//
//  Created by Piecyfer on 16/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import RAGTextField
extension RAGTextField{
    func setUnderlineField(){
        let bgView = UnderlineView(frame: .zero)
        bgView.textField = self
        bgView.backgroundLineColor = AppColors.underline
        bgView.backgroundLineColor = AppColors.underBar
        bgView.foregroundLineWidth = 2.0
        bgView.expandDuration = 0.1
        bgView.backgroundColor = AppColors.clear
        
        if #available(iOS 11, *) {
            bgView.layer.cornerRadius = 4.0
            bgView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
        
        self.tintColor = AppColors.underBar
        self.textBackgroundView = bgView
        self.textPadding = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
        self.textPaddingMode = .textAndPlaceholder
        self.scaledPlaceholderOffset = 0.0
        self.placeholderMode = .scalesWhenEditing
        self.placeholderScaleWhenEditing = 0.8
        self.placeholderColor = AppColors.underBar.withAlphaComponent(0.66)
        self.transformedPlaceholderColor = self.tintColor
        self.hint = nil
    }
}

