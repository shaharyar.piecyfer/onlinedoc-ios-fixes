//
//  String.swift
//  onlineDoc
//
//  Created by Piecyfer on 19/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//
import UIKit
import Atributika

extension String{
     
     func isValidUrl() -> Bool {
          let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
          //     let regEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
          //     let regEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
          let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
          return predicate.evaluate(with: self)
     }
     
     var unescaped: String {
          return self.replacingOccurrences(of: "\\", with: "", options: [], range: nil)
     }
     
     var trim: String{
          self.trimmingCharacters(in: CharacterSet.whitespaces)
     }
     
     var formattedDate: String{
          let inputFormatter = DateFormatter()
          inputFormatter.dateFormat = "yyyy-MM-dd"
          let showDate = inputFormatter.date(from: self)
          inputFormatter.dateFormat = "dd/MM/yyyy"
          return inputFormatter.string(from: showDate!)
     }
     /// extension to format picker date to presentable format
     var pickerDate: String{
          let inputFormatter = DateFormatter()
          inputFormatter.dateFormat = "MMM dd, yyyy"
          let showDate = inputFormatter.date(from: self)
          inputFormatter.dateFormat = "dd/MM/yyyy"
          return inputFormatter.string(from: showDate!)
     }
     /// extension to convert date to databases storeable format as there is major difference between display and database format for date
     var dbFormat:String{
          let inputFormatter = DateFormatter()
          inputFormatter.dateFormat = "dd/MM/yyyy"
          let showDate = inputFormatter.date(from: self)
          inputFormatter.dateFormat = "yyyy-MM-dd"
          return inputFormatter.string(from: showDate!)
     }
     
     func formatDate(from inputFormat: String, to outputFormat: String) -> String{
          let inputFormatter = DateFormatter()
          inputFormatter.dateFormat = inputFormat
          let showDate = inputFormatter.date(from: self)
          inputFormatter.dateFormat = outputFormat
          return inputFormatter.string(from: showDate!)
     }
     
     
     
     var toName: String{
          switch self {
          case "1","01":
               return "January"
               
          case "2","02":
               return "February"
               
          case "3","03":
               return "March"
               
          case "4","04":
               return "April"
               
          case "5","05":
               return "May"
               
          case "6","06":
               return "June"
               
          case "7","07":
               return "July"
               
          case "8","08":
               return "August"
               
          case "9","09":
               return "September"
               
          case "10":
               return "October"
               
          case "11":
               return "November"
               
          case "12":
               return "December"
               
          default:
               return ""
          }
     }
     
     var ToArray:[String]{
          var linksArr = [String]()
          let data = self.data(using: .utf8)!
          do {
               if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Dictionary<String,Any>]
               {
                    for item in json{
                         if let link = item["link"] as? String{
                              linksArr.append(link)
                         }
                    }
               } else {
                    print("bad json")
               }
          } catch let error as NSError {
               print(error)
          }
          return linksArr
     }
     
     var commaStringtoArray: [String]{
          return self.dropFirst().dropLast().split(separator: ",").map { String($0)}
     }
     
     var commaStrtoArray: [String]{
          return self.split(separator: ",").map { String($0)}
     }
     
     var UTCToLocal: String{
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
          let dt = dateFormatter.date(from: self)
          dateFormatter.timeZone = TimeZone.current
          dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          return dateFormatter.string(from: dt!)
     }
     
     var UTCToLocal0: String{
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
          dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
          let dt = dateFormatter.date(from: self)
          dateFormatter.timeZone = TimeZone.current
          dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          return dateFormatter.string(from: dt!)
     }
     
     func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
          return NSLocalizedString(self, tableName: tableName, value: "\(self)", comment: "")
     }
     
     var localized: String {
          return NSLocalizedString(self, comment: "")
     }
     
     var isValidEmail:Bool{
          let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
          let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
          return emailTest.evaluate(with: self)
     }
     
     func isValidPhoneNumber(value: String) -> Bool {
          let PHONE_REGEX = "^[7-9][0-9]{9}$";
          let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
          let result =  phoneTest.evaluate(with: self)
          return result
     }
     
     func isValidPhone() -> Bool {
          do {
               let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
               let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
               if let res = matches.first {
                    return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == 8
               } else {
                    return false
               }
          } catch {
               return false
          }
     }
     
     var isDigits: Bool {
          guard !self.isEmpty else { return false }
          return !self.contains { Int(String($0)) == nil }
     }
     
     var subString : String{
          let idx = self.firstIndex(of: "_")
          if idx != nil {
               return self.substring(to: idx!)
          }else{
               return ""
          }
     }
     
     //HTML string
     func attributedText(fontSize: CGFloat, allStyleFont: UIFont) -> AttributedText? {
          let b = Style("strong").font(AppFont.kMuliBold(fontSize).font())
          let i = Style("i").font(AppFont.kMuliItalic(fontSize).font())
          let link = Style("a").foregroundColor(.systemBlue, .normal).foregroundColor(.systemBlue, .highlighted)
          let all = Style.font(allStyleFont)
          
//          let boldItalic = Style("i", style: Style("strong")).font(AppFont.kMuliSemiBoldItalic(fontSize).font())
          
          return self.replacingOccurrences(of: "&Nbsp;", with: " ").style(tags: b, i).styleLinks(link).styleAll(all)
     }
}
