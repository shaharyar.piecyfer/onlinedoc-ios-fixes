//
//  UIImageView.swift
//  onlineDoc
//
//  Created by Piecyfer on 02/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import SDWebImage

typealias imageResponse = ((_ status:Bool,_ image: UIImage?) -> Void)
let imageCache = NSCache<NSString, UIImage>()

extension UIImageView{
    
    func imageFromServer(imageUrl: URL, placeholder: UIImage = #imageLiteral(resourceName: "ic_user")){
        self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.sd_imageIndicator?.indicatorView.transform = CGAffineTransform(scaleX: 1, y: 1)
        self.backgroundColor = .white
        self.sd_internalSetImage(with: imageUrl,
                                 placeholderImage: placeholder,
                                 options: .highPriority,
                                 context: nil,
                                 setImageBlock: nil,
                                 progress: nil)
        { image, data, errorException, cache, flag, url in
//            self.backgroundColor = .clear
            if let exception = errorException{
                print("Error in downloading Image: \(exception.localizedDescription), with url: \(url?.absoluteString ?? "nil")")
            }else{
//                print("Successful Download: \(url!.absoluteString)")
            }
        }
    }
    
    func imageFromServer(imageUrl: URL, placeholder: UIImage = #imageLiteral(resourceName: "ic_user"), completion: @escaping imageResponse){
        self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.sd_imageIndicator?.indicatorView.transform = CGAffineTransform(scaleX: 1, y: 1)
        self.backgroundColor = .white
        self.sd_internalSetImage(with: imageUrl,
                                 placeholderImage: placeholder,
                                 options: .highPriority,
                                 context: nil,
                                 setImageBlock: nil,
                                 progress: nil)
        { image, data, errorException, cache, flag, url in
//            self.backgroundColor = .clear
            if let exception = errorException{
                print("Error in downloading Image: \(exception.localizedDescription), with url: \(url?.absoluteString ?? "nil")")
                completion(false, nil)
            }else if let img = image{
                completion(true, img)
            }else{
                //                print("Successful Download: \(url!.absoluteString)")
            }
        }
    }
    
    func downloadImage(from imgURL: String) -> URLSessionDataTask? {
        self.backgroundColor = .white
        guard let url = URL(string: imgURL) else { return nil }
        
        // set initial image to nil so it doesn't use the image from a reused cell
        image = #imageLiteral(resourceName: "ic_user")
        
        // check if the image is already in the cache
        if let imageToCache = imageCache.object(forKey: imgURL as NSString) {
            self.image = imageToCache
            return nil
        }
        
        // download the image asynchronously
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let err = error {
                print("downloadImage \(err)")
                return
            }
            
            DispatchQueue.main.async {
                // create UIImage
                let imageToCache = UIImage(data: data!)
                // add image to cache
                imageCache.setObject(imageToCache!, forKey: imgURL as NSString)
                self.image = imageToCache
            }
        }
        task.resume()
        return task
    }
    
    fileprivate var activityIndicator: UIActivityIndicatorView {
        get {
            let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
            activityIndicator.color = UIColor.gray
            activityIndicator.hidesWhenStopped = true
            activityIndicator.center = CGPoint(x:self.frame.width/2,
                                               y: self.frame.height/2)
            activityIndicator.stopAnimating()
            self.addSubview(activityIndicator)
            return activityIndicator
        }
    }
    
//    func doStuff(stuff: String, completion: Action?) {
//        print(stuff)
//        action = completion
//        completion?()
//    }
}
