//
//  UIScrollView.swift
//  onlineDoc
//
//  Created by Piecyfer on 17/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import UIKit
extension UIScrollView {
    func scrollTo(direction: ScrollDirection, animated: Bool = true) {
        self.setContentOffset(direction.contentOffsetWith(self), animated: animated)
    }
}
