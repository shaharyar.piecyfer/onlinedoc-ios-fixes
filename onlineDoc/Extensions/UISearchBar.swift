//
//  UISearchBar.swift
//  onlineDoc
//
//  Created by Piecyfer on 11/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
extension UISearchBar {
    
    var textField : UITextField? {
        if #available(iOS 13.0, *) {
            return self.searchTextField
        } else {
            let field = UITextField()
            for view : UIView in (self.subviews[0]).subviews {
                
                if let textField = view as? UITextField {
                    return textField
                }
            }
        }
        return nil
    }
}
/*
extension UISearchBar{

    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: DismissText.localized(), style: .done, target: self, action: #selector(self.doneButtonAction))
//        done.tintColor = .white

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()

        self.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}
*/
