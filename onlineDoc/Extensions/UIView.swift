//
//  UIView.swift
//  onlineDoc
//
//  Created by Piecyfer on 21/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
extension UIView{
    func roundCorners(corners:UIRectCorner, radius: CGFloat)
    {
        superview?.layoutIfNeeded()
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        self.layer.mask = maskLayer
    }
    
    func showViewBorders(width:Int = 2, radius: Int = 6, color: UIColor = .gray){
        self.layer.borderWidth = CGFloat(width)
        self.layer.cornerRadius = CGFloat(radius)
        self.layer.borderColor = color.cgColor
    }
    
    func roundedCorner(_ corners: UIRectCorner, radius: CGFloat){
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.frame = self.bounds
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    //MARK: Set multo Color text
    func setAttributedTextForLabelAll(mainString : String , attributedStringsArray : [String] , color : UIColor, attFont:UIFont) {
        let attributedString1    = NSMutableAttributedString(string: mainString)
        for objStr in attributedStringsArray {
            let range1 = (mainString as NSString).range(of: objStr)
            let attribute_font = [NSAttributedString.Key.font: attFont]
            attributedString1.addAttributes(attribute_font, range:  range1)
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range1)
        }
        
        if(self.isKind(of: UILabel.self)) {
            (self as! UILabel).attributedText = attributedString1
        }
        if(self.isKind(of: UITextView.self)) {
            (self as! UITextView).attributedText = attributedString1
        }
        if(self.isKind(of: UITextField.self)) {
            (self as! UITextField).attributedText = attributedString1
        }
    }
    
    //MARK: Set multi Color text
    func  setAttributedTextForLabelAll(mainString : String , attributedStringsArray : [String]  , color : [UIColor], attFont:[UIFont]) {
        let attributedString1    = NSMutableAttributedString(string: mainString)
        for (index,objStr) in attributedStringsArray.enumerated() {
            let range1 = (mainString as NSString).range(of: objStr)
            let attribute_font = [NSAttributedString.Key.font: attFont[index]]
            attributedString1.addAttributes(attribute_font, range:  range1)
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color[index], range: range1)
        }
        if(self.isKind(of: UILabel.self)) {
            (self as! UILabel).attributedText = attributedString1
        }
        if(self.isKind(of: UITextView.self)) {
            (self as! UITextView).attributedText = attributedString1
        }
        if(self.isKind(of: UITextField.self)) {
            (self as! UITextField).attributedText = attributedString1
        }
        if(self.isKind(of: UIButton.self)) {
            (self as! UIButton).setAttributedTitle(attributedString1, for: .normal)
        }
    }
    
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = 1 //Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}
/*

 */

@IBDesignable
extension UIView {
    
    @IBInspectable
    public var cornerRadius: CGFloat {
        set (radius) {
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = radius > 0
        }
        
        get {
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat {
        set (borderWidth) {
            self.layer.borderWidth = borderWidth
        }
        
        get {
            return self.layer.borderWidth
        }
    }
    
    @IBInspectable var borderAlpha: CGFloat {
        set {
            let borderCGColor = layer.borderColor
            var color = UIColor(cgColor: borderCGColor!)
            
            if (newValue < 0) {
                color = color.withAlphaComponent(0)
                layer.borderColor = color.cgColor
            } else if (newValue > 1) {
                color = color.withAlphaComponent(1)
                layer.borderColor = color.cgColor
            } else {
                color = color.withAlphaComponent(newValue)
                layer.borderColor = color.cgColor
            }
        }
        get {
            return layer.borderColor!.alpha
        }
    }
    
    
    @IBInspectable
    public var borderColor:UIColor? {
        set (color) {
            self.layer.borderColor = color?.cgColor
        }
        
        get {
            if let color = self.layer.borderColor {
                return UIColor(cgColor: color)
            } else {
                return nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
