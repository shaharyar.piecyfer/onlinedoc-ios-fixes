//
//  UIViewController.swift
//  onlineDoc
//
//  Created by Piecyfer on 10/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit

extension UIViewController {
    var window : UIWindow {
        return UIApplication.shared.windows.first!
    }
    
    var screenWidth:CGFloat {
        get {
            return UIScreen.main.bounds.width
        }
    }
    
    var screenHeight:CGFloat {
        get {
            return UIScreen.main.bounds.height
        }
    }
    
    var currentUser : User? {
        get{
            AppConfig.shared.user
        }
    }
    
    var currentUserId : String? {
        get{
            AppConfig.shared.userId
        }
    }
    
    func navigateVC(_ vc: UIViewController) {
        if let _ = self.navigationController {
            self.pushView(vc)
        }else{
            self.presentVC(vc)
        }
    }
    
    func pushView(_ vc: UIViewController,_ isAnimated:Bool = true){
        self.navigationController?.pushViewController(vc, animated: isAnimated)
    }
    
    func presentVC(_ vc:UIViewController, _ isAnimated:Bool = true){
        self.present(vc, animated: isAnimated, completion: nil)
    }
    
    func apiCallForNotifyCount(){
//        DispatchQueue.global(qos:.background).async {
            RequestManager.shared.getNotificationCount{ status, response in
                if status, let data = response as? CountModel {
                    if let count = data.notificationCount {
                        AppConfig.shared.saveNotificationCount(count: count)
                        NotificationCenter.default.post(name: .notificationCountCheck, object: nil)
                    }
                }
                
                self.apiCallForChatCount()
            }
//        }
    }
    
    func apiCallForChatCount(){
//        DispatchQueue.global(qos:.background).async {
            RequestManager.shared.getUnreadChatCount{ status, response in
                if status, let data = response as? CountModel {
                    if let count = data.chatUnreadCount {
                        AppConfig.shared.saveChatCount(count: count)
                        NotificationCenter.default.post(name: .chatCountCheck, object: nil)
                    }
                }
            }
//        }
    }
    
    func updateTitleView(title: String, subtitle: String?, baseColor: UIColor = .white) {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: -2, width: 0, height: 0))
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = baseColor
        titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
        titleLabel.text = title
        titleLabel.textAlignment = .center
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.sizeToFit()
        
        let subtitleLabel = UILabel(frame: CGRect(x: 0, y: 18, width: 0, height: 0))
        subtitleLabel.textColor = baseColor.withAlphaComponent(0.95)
        subtitleLabel.font = UIFont.systemFont(ofSize: 12)
        subtitleLabel.text = subtitle
        subtitleLabel.textAlignment = .center
        subtitleLabel.adjustsFontSizeToFitWidth = true
        subtitleLabel.sizeToFit()
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), height: 30))
        titleView.addSubview(titleLabel)
        if subtitle != nil {
            titleView.addSubview(subtitleLabel)
        } else {
            titleLabel.frame = titleView.frame
        }
        let widthDiff = subtitleLabel.frame.size.width - titleLabel.frame.size.width
        if widthDiff < 0 {
            let newX = widthDiff / 2
            subtitleLabel.frame.origin.x = abs(newX)
        } else {
            let newX = widthDiff / 2
            titleLabel.frame.origin.x = newX
        }
        
        navigationItem.titleView = titleView
    }
    
    func addView(view: UIView) {
        UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.addSubview(view)
    }
    
    func removeView(tag: Int) {
        if let viewWithTag = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.viewWithTag(tag) {
            viewWithTag.removeFromSuperview()
        }
    }
}
