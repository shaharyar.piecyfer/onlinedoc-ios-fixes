//
//  URL.swift
//  onlineDoc
//
//  Created by Piecyfer on 17/01/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//import AVFoundation

import Foundation

import UIKit
import PDFKit
import AVKit
import WebKit
extension URL{
    
    func generateThumbnail() -> UIImage? {
        do {
            let asset = AVURLAsset(url: self)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imageGenerator.copyCGImage(at: .zero, actualTime: nil)
            return UIImage(cgImage: cgImage)
        } catch {
            print("generateThumbnail", error.localizedDescription)
            return nil
        }
    }
    
    func getQueryStringParameter(_ param: String) -> String? {
        //          guard let url = URLComponents(string: self) else { return nil }
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.filter{$0.name == param}.first?.value
        //      let a = url.queryItems?.first(where: { $0.name == param })?.value
//        return url.queryItems?.filter{$0.name == param}.first?.value
    }
    
   /*

    func createThumb() -> UIImage? {
        let url = URL(string: (self.file?.path() ?? ))
        switch file?.type {
        case FileType.image.rawValue:
            let image = UIImage(contentsOfFile: (url?.path)!)
            _finalImage = self.createScaledImage(image: image!)
            break
        case FileType.office.rawValue:
            //Loading.......
            break
        case FileType.Pdf.rawValue:
            guard let doc = PDFDocument(url: url!) else {return}
            guard let page = doc.page(at: 0) else {return}
            _finalImage = page.thumbnail(of: CGSize(width: 768, height: 1024), for: .cropBox)
            break
        case: FileType.video.rawValue:
        let asset = AVAsset(url: url!)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let time = CMTime(seconds: 2, preferredTimescale: 1)
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            _finalImage = UIImage(cgImage: imageRef)
        } catch let error{
            print("Error: \(error)")
        }
            break
            
        }
    }

    func createScaledImage(image: UIImage) {
        
        let THUMB_WIDTH = 150.0 - 40.0
        let THUMB_HEIGHT = THUMB_WIDTH - 23.0
        var itemThumb = resizeImage(image: image, constraintSize: CGSize(width: THUMB_WIDTH, height: THUMB_HEIGHT))
        let thumbRect = CGRect(x: 0, y: 0, width: 10, height: 10)
        UIGraphicsBeginImageContextWithOptions(thumbRect.size, true, 0.0)
        let context = UIGraphicsGetCurrentContext()
        
        // Fill a white rect
        context?.setFillColor(gray: 1.0, alpha: 1.0)
        context?.fill(thumbRect)
        
        // Stroke a gray rect
        let comps : [CGFloat] = [0.8, 0.8, 0.8, 1]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let strokeColor = CGColor(colorSpace: colorSpace, components: comps)
        context?.setStrokeColor(strokeColor!)
        UIRectFrame(thumbRect)
        //CGColorRelease(strokeColor!)
        
        itemThumb.draw(in: thumbRect.insetBy(dx: 1, dy: 1))
        
        itemThumb = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.finishThumCreation(image: image)
    }*/
}



