//
//  URL_Ext.swift
//  onlineDoc
//
//  Created by Piecyfer on 02/06/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
extension URL{
    var uti: String {
        return (try? self.resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier ?? "public.data"
    }


}
