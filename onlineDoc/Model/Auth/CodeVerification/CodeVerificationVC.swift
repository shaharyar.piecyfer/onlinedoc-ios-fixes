//
//  CodeVerificationVC.swift
//  onlineDoc
//
//  Created by Piecyfer on 29/01/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit
import PinCodeTextField

class CodeVerificationVC: BaseVC {
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var fldCode: PinCodeTextField!
    @IBOutlet weak var lblCodeWhereSent: UILabel!
    @IBOutlet weak var btnUserDiffEmail: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    
    private lazy var viewModel = { return CodeVerificationVM() }()
    var onVerifiedCallBack:(() -> Void)?
    var email = ""
    var phone = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
        lblHeader.text = "Enter 6-digit code".localized()
        fldCode.becomeFirstResponder()
        fldCode.keyboardType = .emailAddress
        fldCode.delegate = self
        lblCodeWhereSent.text = "Code has been send to".localized() + " \(email)" + (!phone.isEmpty ? "\n"+"Code has been send to".localized() + " \(phone)" : "")
        btnResend.setTitle("Resend".localized(), for: .normal)
        btnUserDiffEmail.setTitle("Use a different email?".localized(), for: .normal)
        
        let toolbar = UIToolbar()
        let leftButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let nextButtonItem = UIBarButtonItem(title: NSLocalizedString("NEXT",
                                                                      comment: ""),
                                             style: .done,
                                             target: self,
                                             action: #selector(onClickNext))
        toolbar.items = [leftButton, nextButtonItem]
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        fldCode.inputAccessoryView = toolbar
        
        self.setupTheme()
    }
    
    private func setupTheme() {
        Themer.shared.register(
            target: self,
            action: CodeVerificationVC.setTheme)
    }
    
    private func initViewModel() {
        viewModel.email = self.email
//        viewModel.callSendCodeApi()
        
        viewModel.updateVerifyStatus = {
            if self.viewModel.isVerified {
                self.dismiss()
                self.onVerifiedCallBack?()
            }
        }
        
        viewModel.updateErrorMessage = { msg in
            self.showAlert(To: "Alert".localized(), for: msg) { _ in }
        }
    }
    
    @IBAction func onClickUseDiffEmail(_ sender: Any) {
        dismiss()
    }
    
    private func dismiss(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onClickNext() {
        viewModel.onVerifyCode()
    }
    
    @IBAction func onClickResend() {
        viewModel.callSendCodeApi()
    }
}

extension CodeVerificationVC: PinCodeTextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        
    }
    
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        let value = textField.text ?? ""
        print("value changed: \(value)")
        viewModel.code = value
        if value.count == 6 {
            viewModel.onVerifyCode()
        }
    }
    
    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: PinCodeTextField) -> Bool {
        return true
    }
}

extension CodeVerificationVC {
    fileprivate func setTheme(_ theme: MyTheme) {
        lblCodeWhereSent.textColor = theme.settings.subTitleColor
        fldCode.textColor = theme.settings.titleColor
        fldCode.updatedUnderlineColor = theme.settings.titleColor
    }
}
