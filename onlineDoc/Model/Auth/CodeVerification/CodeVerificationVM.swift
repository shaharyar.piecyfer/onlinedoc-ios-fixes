//
//  CodeVerificationVM.swift
//  onlineDoc
//
//  Created by Piecyfer on 29/01/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation

class CodeVerificationVM {
    
    var errorMessage: String = "" {
        didSet{
            if !errorMessage.isEmpty {
                self.updateErrorMessage?(errorMessage)
            }
        }
    }
    var updateErrorMessage: ((_ msg: String)->())?
    
    var isVerified = false {
        didSet {
            self.updateVerifyStatus?()
        }
    }
    var updateVerifyStatus: (()->())?
    
    var code: String = ""
    var email: String = ""
    
    func callSendCodeApi() {
        RequestManager.shared.resendConfirmationLink(email: email) { (status, response) in
            if status{
                if let data = response as? GeneralSuccessResponse{
                    print("callSendCodeApi -> success -> \(status) -> data \(data)")
                    self.errorMessage = "Code has been sent to".localized() + " \(self.email)."
                }
            }else{
                print("callSendCodeApi -> failure -> \(status) -> response \(String(describing: response))")
            }
        }
    }
    
    func onVerifyCode() {
        guard validateCode() else { return }
        RequestManager.shared.codeVerification(email: email, code: code) { (status, response) in
            if status{
                if let data = response as? LoginSuccessResponse{
                    print("onVerifyCode -> success -> \(status) -> data \(data)")
                    self.isVerified = true
                }
            }else{
                print("onVerifyCode -> failure -> \(status) -> response \(String(describing: response))")
                self.errorMessage = "Please enter a valid code.".localized()
                self.isVerified = false
            }
        }
    }

    private func validateCode() -> Bool {
        if code.count < 6 {
            errorMessage = "Please enter 6-digit code.".localized()
            return false
        }
        return true
    }
}
