//
//  LogInCredentials.swift
//  onlineDoc
//
//  Created by Piecyfer on 28/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import Foundation

struct LogInCredentials {
    var userName : String!
    var passCode: String!
    init() {}
    var dictionary: [String: String] {
        return ["userName": userName,
                "passCode": passCode]
    }
    var nsDictionary: NSDictionary {
        return dictionary as NSDictionary
    }
}
