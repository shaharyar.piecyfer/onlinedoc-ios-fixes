//
//  LoggedInUserCountModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 11/03/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct LoggedInUserCountResultModel : Mappable {
    var users : [LoggedInUsersModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        users <- map["users"]
    }
}

struct LoggedInUsersModel : Mappable {
    var id : Int?
    var notification_count : Int?
    var message_count: Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        id <- map["id"]
        notification_count <- map["notification_count"]
        message_count <- map["message_count"]
    }
}
