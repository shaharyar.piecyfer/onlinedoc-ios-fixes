/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct User : Mappable, Codable {
    
    var id : Int? {
        didSet{
            if let iD = id{
                stringId = String(iD)
            }
        }
    }
    var stringId : String?
    var email : String?
    var pre_name : String?
    var first_name : String?
    var last_name : String?
    var name : String?
    var _name : String?
    var gender : String?
    var dob : String?
    var phone : String?
    var address : String?
    var bio : String?
    var is_active : Bool?
    var inactivity_limit : Int?
    var is_confirmed : Bool?
    var old_first_name : String?
    var old_last_name : String?
    var status : String?
    var created_at : String?
    var updated_at : String?
    var file : FileModel?
    var token : String?
    
    var isSelected: Bool = false
    var full_name: String?
    var friend_status: FriendModel?
    var web_links: [WebLinkModel]?
    var online_status: String?
    var chatRoomId : Int?
    var device_token: String?
    var reset_temp_password: Bool?
    var virtual_user: Bool?
    
    init(){}
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        email <- map["email"]
        pre_name <- map["pre_name"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        if let fName = first_name, let lName = last_name{
            name = fName + " " + lName
        }
        _name <- map["name"]
        gender <- map["gender"]
        dob <- map["dob"]
        phone <- map["phone"]
        address <- map["address"]
        bio <- map["bio"]
        is_active <- map["is_active"]
        inactivity_limit <- map["inactivity_limit"]
        is_confirmed <- map["is_confirmed"]
        old_first_name <- map["old_first_name"]
        old_last_name <- map["old_last_name"]
        status <- map["status"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        file <- map["file"]
        token <- map["token"]
        
        full_name <- map["full_name"]
        friend_status <- map["friend_status"]
        web_links <- map["web_links"]
        online_status <- map["online_status"]
        chatRoomId <- map["chat_room_id"]
        device_token <- map["device_token"]
        reset_temp_password <- map["reset_temp_password"]
        virtual_user <- map["virtual_user"]
    }
}

struct FriendModel : Mappable, Codable {
    var id : Int?
    var senderId : Int?
    var receiverId : Int?
    var requestStatus : String?
    var created_at : String?
    var updated_at : String?
    
    init(){}
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        senderId <- map["sender_id"]
        receiverId <- map["receiver_id"]
        requestStatus <- map["request_status"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }
}

struct RegisterUserModel {
    var email : String?
    var pre_name : String?
    var first_name : String?
    var last_name : String?
    var gender : String?
    var profilePicture : String?
    var phone : String?
    var address : String?
    var password : String?
    var password_confirmation : String?
    var dob : String?
    var bio : String?
    init() {}
}
