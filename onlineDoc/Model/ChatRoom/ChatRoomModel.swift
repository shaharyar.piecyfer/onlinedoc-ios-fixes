//
//  ChatRoomModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 29/07/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct ChatRoomResult: Mappable {
    var result: [ChatRoomModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        result <- map["chat_rooms"]
    }
}

struct SingleChatRoomResult: Mappable {
    var result: ChatRoomModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        result <- map["chat_room"]
    }
}

struct ChatRoomModel: Mappable {
    var chat: ChatRoomDetailModel?
    var lastMessage: String?
    var last_message_created_at: String?
    var user: User?
    var friend_status: FriendModel?

    init() {}
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        chat <- map["chat_room"]
        lastMessage <- map["last_message"]
        last_message_created_at <- map["last_message_created_at"]
        user <- map["user"]
        friend_status <- map["friend_status"]
    }
}

struct ChatRoomDetailModel: Mappable {
    var id: Int?
    var chat_room_id: Int?
    var user_id: Int?
    var chat_room_active: Bool?
    var message_count: Int?
    var created_at: String?
    var updated_at: String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id <- map["id"]
        chat_room_id <- map["chat_room_id"]
        user_id <- map["user_id"]
        chat_room_active <- map["chat_room_active"]
        message_count <- map["message_count"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }
}

struct ChatMessagesResult: Mappable {
    var data: [ChatModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        data <- map["message"]
    }
}

struct ChatMessageResult: Mappable {
    var data: ChatModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        data <- map["message"]
    }
}

struct ChatModel: Mappable {
    var id: Int?
    var chat_room_id: Int?
    var sender_id: Int?
    var receiver_id: Int?
    var message: String?
    var message_type: String?
    var sender_is_read: Bool?
    var receiver_is_read: Bool?
    var created_at: String?
    var updated_at: String?
    var files: [FileModel]?

    init() {}
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id <- map["id"]
        chat_room_id <- map["chat_room_id"]
        sender_id <- map["sender_id"]
        receiver_id <- map["receiver_id"]
        message <- map["message"]
        message_type <- map["message_type"]
        sender_is_read <- map["sender_is_read"]
        receiver_is_read <- map["receiver_is_read"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        files <- map["files"]
    }
}


struct ChatNotificationPayload: Mappable {
    var chat_room_id : String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        chat_room_id <- map["chat_room_id"]
    }

}

struct InvitedChatRoomResult: Mappable {
    var result: InvitedChatRoomModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        result <- map["chat_room"]
    }
}

struct InvitedChatRoomModel: Mappable {
    var user_id: Int?
    var email: String?
    var phone: String?
    var chat_room_id: Int?
    var virtual_user: Bool?

    init() {}
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        user_id <- map["user_id"]
        email <- map["email"]
        phone <- map["phone"]
        chat_room_id <- map["chat_room_id"]
        virtual_user <- map["virtual_user"]
    }
}

struct CheckChatRoomResult: Mappable {
    var system_user: Bool?
    var chat_room_exists: Bool?
    var user_name: String?
    var user_id: Int?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        system_user <- map["system_user"]
        chat_room_exists <- map["chat_room_exists"]
        user_name <- map["user_name"]
        user_id <- map["user_id"]
    }
}

struct ContactListResponse : Mappable {
    var contacts = [ContactModel]()

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        contacts <- map["clinic_contacts"]
    }
}

struct NewChatSearchResponse : Mappable {
    var friends = [MemberModel]()
    var contacts = [ContactModel]()
    var other_users = [MemberModel]()
    var contacts_imported: Bool?

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        friends <- map["friends"]
        contacts <- map["contacts"]
        other_users <- map["other_users"]
        contacts_imported <- map["contacts_imported?"]
    }
}
