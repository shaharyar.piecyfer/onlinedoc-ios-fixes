//
//  CommonModels.swift
//  onlineDoc
//
//  Created by Piecyfer on 12/07/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

struct AlertModel {
    var title: String?
    var description: String?
    var style = UIAlertController.Style.actionSheet
    var presentationStyle = UIModalPresentationStyle.popover
    var backgroundColor = AppConfig.shared.isDarkMode ? AppColors.blackColor : AppColors.extraLightGrayColor
    var tintColor = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
    var btns = [AlertBtnModel]()
}

struct AlertBtnModel {
    var title: String?
    var style: UIAlertAction.Style = .default
    var color = AppConfig.shared.isDarkMode ? AppColors.whiteColor : AppColors.blackColor
}
