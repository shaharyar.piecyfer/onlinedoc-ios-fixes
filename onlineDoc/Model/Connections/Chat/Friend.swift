//
//  Friend.swift
//  onlineDoc
//
//  Created by Piecyfer on 17/03/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//


import Foundation
import ObjectMapper

struct Friend : Mappable {
    var active_thread : Int?
    var conversation_id : String?
    var firebase_coll_name : String?
    var friend_id : Any?{
        didSet{
            if let friendID = friend_id as? Int{
                friendStringId = String(friendID)
            }else if let friendID = friend_id as? String{
                friendStringId = friendID
            }
        }
    }
    var id : Any?{
        didSet{
            if let userID = id as? Int{
                self.userId = userID
            }else if let userID = id as? String{
                self.userId = Int(userID)
            }
        }
    }
    var userId: Int?
    var last_message : String?
    var message_id : Any?
    var unread : Int?
    var friendStringId = ""
    var messageStringId = ""
    var request_status : Int?
    var created_at : String?
//    lazy var created_date = Date()
    var created_date : Date!
    
    
    var clinicUserId : Int?
    var email : String?
    var preName : String?
    var firstName : String?
    var lastName : String?
    var profilePicture : String?
    var usersId : Int?
    var clinicId : Int?
    var requestStatus : String?
    var clinicName : String?
    var approvedBy : String?
    var clinicAdmin : Int?
    var name : String?
    var addedDate : AddedDate?
    var isAdminRecord = false
    
    var userInfo : GroupMember?
    var onlineStatus : OnlineStatus?
    var recordType :RecordType = .friend
    var chat_delete_status: String?
    var userFirebaseId: String?
    
    init?(map: Map) {

    }
    init() {
    }
    
    mutating func mapping(map: Map) {

        active_thread <- map["active_thread"]
        conversation_id <- map["conversation_id"]
        firebase_coll_name <- map["firebase_coll_name"]
        friend_id <- map["friend_id"]
        id <- map["id"]
        last_message <- map["last_message"]
        message_id <- map["message_id"]
        unread <- map["unread"]
        request_status <- map["request_status"]
        created_at <- map["created_at"]
        
        
        clinicUserId <- map["clinicUserId"]
        email <- map["email"]
        preName <- map["preName"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        profilePicture <- map["profilePicture"]
        usersId <- map["userId"]
        clinicId <- map["clinicId"]
        requestStatus <- map["requestStatus"]
        clinicName <- map["clinicName"]
        approvedBy <- map["approvedBy"]
        clinicAdmin <- map["clinicAdmin"]
        addedDate <- map["addedDate"]
        name = (firstName ?? "") + " " + (lastName ?? "")
        chat_delete_status <- map["chat_delete_status"]
        userFirebaseId <- map["userFirebaseId"]
    }

}

import Foundation
import ObjectMapper

struct FriendPayLoad : Mappable {
    var conversation_id : String?
    var receiver_id : String?
    var onlineStatus : String?
    var created_at : String?
    var message_id : String?
    var id : String?
    var message : String?
    var sender_id : Int?
    var friend_id: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        conversation_id <- map["conversation_id"]
        receiver_id <- map["receiver_id"]
        onlineStatus <- map["onlineStatus"]
        created_at <- map["created_at"]
        message_id <- map["message_id"]
        id <- map["id"]
        message <- map["message"]
        sender_id <- map["sender_id"]
        friend_id <- map["friend_id"]
    }

}
