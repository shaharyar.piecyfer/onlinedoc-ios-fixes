/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct Message : Mappable {
	var attachment_id : String?
	var conversation_id : String?
	var created_at : String?
	var file_ext : String?
	var file_name : String?
    var real_name: String?
	var file_path : String?
    var id : String?
	var message_id : Any?
	var receiver_id : Any?{
        didSet{
            if let receiverId = receiver_id as? Int{
                receiverIntId = receiverId
            }else if let receiverId = receiver_id as? String{
                receiverIntId = Int(receiverId)
            }
        }
    }
    var message : String?
    var sender_id : Any?{
        didSet{
            if let senderId = sender_id as? Int{
                senderIntId = senderId
            }else if let  senderId = sender_id as? String{
                senderIntId = Int(senderId)
            }
        }
    }
    
    var image : UIImage?
    var mimeType : String?
    var isServerFile = true
    var messageIdString : String?
    var messageIdInt : Int?
    var senderIntId : Int?
    var receiverIntId : Int?
    var commentBy : GroupMember?
    /// Message ID  can be either int or string to query data need to check that
//    var isOfTypeInt = false
    
    var comment_type:String?
    var firebaseCommentID:String?
    var multiFiles:[Message]?
    var file_main_type : String?
    var AttachmentBase64String : Data!
    
    var receiver_status : String?
    var sender_status : String?
    var isRead : Bool = false

	init?(map: Map) {

	}
    init() {}

	mutating func mapping(map: Map) {
        comment_type <- map["comment_type"]
        multiFiles <- map["files"]
		attachment_id <- map["attachment_id"]
		conversation_id <- map["conversation_id"]
		created_at <- map["created_at"]
		file_ext <- map["file_ext"]
		file_name <- map["file_name"]
		file_path <- map["file_path"]
        id <- map["id"]
		message_id <- map["message_id"]
        message <- map["message"]
		receiver_id <- map["receiver_id"]
		sender_id <- map["sender_id"]
        real_name <- map["real_name"]
        firebaseCommentID <- map["firebaseChatKey"]
        file_main_type <- map["file_main_type"]
        receiver_status <- map["receiver_status"]
        sender_status <- map["sender_status"]
        isRead <- map["is_read"]
	} 
}
