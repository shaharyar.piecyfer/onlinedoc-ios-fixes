/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct DrctMsgData : Mappable {
	var conversation_id : String?
	var firebase_coll_name : String?
	var friend_id : Int?
	var last_message : String?
	var request_status : Int?
	var message_id : String?
	var id : Int?
	var unread : Int?
	var active_thread : Int?
    var created_at : String?
    var firebaseMessageId : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		conversation_id <- map["conversation_id"]
		firebase_coll_name <- map["firebase_coll_name"]
		friend_id <- map["friend_id"]
		last_message <- map["last_message"]
		request_status <- map["request_status"]
		message_id <- map["message_id"]
		id <- map["id"]
		unread <- map["unread"]
		active_thread <- map["active_thread"]
        created_at <- map["created_at"]
        firebaseMessageId <- map["firebaseMessageId"]
	}

}
