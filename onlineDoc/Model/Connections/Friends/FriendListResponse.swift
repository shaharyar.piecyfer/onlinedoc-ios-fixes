
import Foundation
import ObjectMapper

struct FriendListResponse : Mappable {
	var friends = [MemberModel]()
    var other_users = [MemberModel]()

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {
        friends <- map["friends"]
        other_users <- map["other_users"]
	}
}
