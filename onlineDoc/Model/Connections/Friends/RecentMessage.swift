//
//  RecentMessage.swift
//  onlineDoc
//
//  Created by Piecyfer on 07/03/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper
struct RecentMessage: Mappable {
    
    var id: Int?
    var unread: Int?
    var message_id: Int?
    var friend_id: Any?{
        didSet{
            if let id = friend_id as? Int{
                friend_IntId = id
            }else{
                if let id = friend_id as? String{
                    friend_StringId = id
                }
            }
        }
    }
    var active_thread: Int?
    var last_message: String?
    var conversation_id: String?
    var firebase_coll_name: String?
    var friend_StringId: String?{
        didSet{
            if friend_IntId == nil{
                if let iD = friend_StringId{
                     friend_IntId = Int(iD)
                }
            }
        }
    }
    var friend_IntId : Int?{
        didSet{
            if friend_StringId == nil{
                if let ID = friend_IntId {
                        friend_StringId = String(ID)
                }
            }
        }
    }
    
    init?(map: Map) {

    }
    init() {
        
    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        unread <- map["unread"]
        friend_id <- map["friend_id"]
        message_id <- map["message_id"]
        last_message <- map["last_message"]
        active_thread <- map["active_thread"]
        conversation_id <- map["conversation_id"]
        firebase_coll_name <- map["firebase_coll_name"]
    }
}



struct OnlineStatus: Mappable {
    
    var id : Int?
    var status : String?
    
    init?(map: Map) {
        
    }
    init() {}
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        status <- map["status"]
    }

}
