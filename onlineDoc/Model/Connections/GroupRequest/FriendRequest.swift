//
//  FriendRequest.swift
//  onlineDoc
//
//  Created by Piecyfer on 08/03/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct FriendRequest : Mappable {
        
    var active_thread : Int?
    var conversation_id : String?
    var firebase_coll_name : String?
    var friend_id : String?
    var id : String?
    var last_message : String?
    var message_id : Int?
    var unread : Int?
    
    init?(map: Map) {

    }
    init() {}

    mutating func mapping(map: Map) {

        active_thread <- map["active_thread"]
        conversation_id <- map["conversation_id"]
        firebase_coll_name <- map["firebase_coll_name"]
        friend_id <- map["friend_id"]
        id <- map["id"]
        last_message <- map["last_message"]
        message_id <- map["message_id"]
        unread <- map["unread"]
    }

}
