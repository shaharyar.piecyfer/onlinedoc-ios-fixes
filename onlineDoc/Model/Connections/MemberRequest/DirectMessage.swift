//
//  DirectMessage.swift
//  onlineDoc
//
//  Created by Piecyfer on 09/03/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct DirectMessage: Mappable{
    var conversation_id : String?
    var firebase_coll_name : String?
    var friend_id : Any?{
        didSet{
            if let ID = friend_id as? Int{
                intFrendId = ID
            }else if let ID = friend_id as? String{
                if let id = Int(ID){
                    intFrendId = id
                }
            }
        }
    }
    var id : Any?{
        didSet{
            if let ID = id as? Int{
                IntId = ID
            }else if let ID = id as? String{
                if let id = Int(ID){
                    IntId = id
                }
            }
        }
    }
    
    var last_message : String?
    var message_id : Int?
    var request_status : Int?
    var unread : Int?
    var IntId = 0
    var messageBy : GroupMember?
    var onlineStatus : OnlineStatus?
    var StringId: String?
    var intFrendId = 0
    
    init?(map: Map) {

    }

    init() {}
    
    mutating func mapping(map: Map) {

        conversation_id <- map["conversation_id"]
        firebase_coll_name <- map["firebase_coll_name"]
        friend_id <- map["friend_id"]
        id <- map["id"]
        last_message <- map["last_message"]
        message_id <- map["message_id"]
        request_status <- map["request_status"]
        unread <- map["unread"]
    }

}

import Foundation
import ObjectMapper

struct DirctMessagePayload : Mappable {
    var conversation_id : String?
    var receiver_id : String?
    var onlineStatus : String?
    var created_at : String?
    var message_id : String?
    var id : String?
    var message : String?
    var sender_id : Int?
    var firebaseColKey: String?
    var friend_id: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        conversation_id <- map["conversation_id"]
        receiver_id <- map["id"]
        onlineStatus <- map["onlineStatus"]
        created_at <- map["created_at"]
        message_id <- map["message_id"]
        id <- map["id"]
        message <- map["last_message"]
        sender_id <- map["sender_id"]
        firebaseColKey <- map["firebase_coll_name"]
        friend_id <- map["friend_id"]
    }

}

import Foundation
import ObjectMapper

struct FriendRequestPayload : Mappable {
    var conversation_id : String?
    var receiver_id : String?
    var attachment_id : String?
    var created_at : String?
    var message_id : String?
    var message : String?
    var sender_id : Int?
    var friend_id: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        conversation_id <- map["conversation_id"]
        receiver_id <- map["receiver_id"]
        attachment_id <- map["attachment_id"]
        created_at <- map["created_at"]
        message_id <- map["message_id"]
        message <- map["message"]
        sender_id <- map["sender_id"]
        friend_id <- map["friend_id"]
    }

}

