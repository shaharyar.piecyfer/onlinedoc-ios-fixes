//
//  ContactModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 29/11/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct ContactModel : Mappable, Codable {
    var id : Int?
    var clinic_id : Int?
    var user_id : Int?
    var contact_name : String?
    var contact_email : String?
    var title : String?
    var date_of_birth : String?
    var notes : String?
    var company : String?
    var group : String?
    var number : String?
    var mobile_no : String?
    var address : String?
    var cpr_no : String?
    var created_at : String?
    var updated_at : String?
    var file : FileModel?
    var isSelected = false

    init?() {}
    init?(map: Map) {}

    mutating func mapping(map: Map) {

        id <- map["id"]
        clinic_id <- map["clinic_id"]
        user_id <- map["user_id"]
        contact_name <- map["contact_name"]
        contact_email <- map["contact_email"]
        title <- map["title"]
        date_of_birth <- map["date_of_birth"]
        notes <- map["notes"]
        company <- map["company"]
        group <- map["group"]
        number <- map["number"]
        mobile_no <- map["mobile_no"]
        address <- map["address"]
        cpr_no <- map["cpr_no"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        file <- map["file"]
    }
}
