//
//  CountModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 04/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct CountModel : Mappable {
    var notificationCount : Int?
    var chatUnreadCount : Int?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        notificationCount <- map["notification_count"]
        chatUnreadCount <- map["message_count"]
    }
}
