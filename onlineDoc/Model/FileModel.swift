//
//  FileModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 02/07/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct FileModel : Mappable, Codable {
    var src : String?
    var thumb_url: String?
    var medium_url: String?
    var content_type : String?
    var name : String?
    var size : Int?
    var signed_id : String?

    init(){}
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        src <- map["src"]
        thumb_url <- map["thumb_url"]
        medium_url <- map["medium_url"]
        content_type <- map["content_type"]
        name <- map["name"]
        size <- map["size"]
        signed_id <- map["signed_id"]
    }
}

struct Uploads {
    var directUploadInfo: DirectUploadModel
    var data: Data
    var image: UIImage?
}

struct UploadMultiFileParams {
    var directUploadParams: DirectUploadParams
    var data: Data
    var image: UIImage?
}
