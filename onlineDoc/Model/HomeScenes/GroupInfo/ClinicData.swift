/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct ClinicData : Mappable {
	var id : Int?
	var clinicDomain : String?
	var clinicType : String?
	var clinicDomainAlias : String?
	var clinicName : String?
	var clinicImage : String?
	var clinicDescription : String?
	var clinicEmail : String?
	var clinicAddress : String?
	var clinicPhone : String?
	var invitationSendBy : String?
	var approvedBy : String?
	var isPublic : String?
	var clinicMessage : String?
	var clinicAdmin : Int?
	var isActive : String?
	var clinicMembership : Int?
	var clinicModules : String?
	var createdAt : CreatedAt?
    var canLeaveGroup : Bool?
	var role : String?
    var userGroupNotification: String?
    var groupStatus: String?
	var memberCount : String?
	var folder : [Category]?
    var admins : String?{
        didSet{
            adminsArray = [String]()
            if let res = admins?.commaStrtoArray{
                adminsArray = res
            }
        }
    }
    var adminsArray = [String]()
	init?(map: Map) {

	}
    init(){}

	mutating func mapping(map: Map) {
		id <- map["id"]
		clinicDomain <- map["clinicDomain"]
		clinicType <- map["clinicType"]
		clinicDomainAlias <- map["clinicDomainAlias"]
		clinicName <- map["clinicName"]
		clinicImage <- map["clinicImage"]
		clinicDescription <- map["clinicDescription"]
		clinicEmail <- map["clinicEmail"]
		clinicAddress <- map["clinicAddress"]
		clinicPhone <- map["clinicPhone"]
		invitationSendBy <- map["invitationSendBy"]
		approvedBy <- map["approvedBy"]
		isPublic <- map["isPublic"]
		clinicMessage <- map["clinicMessage"]
		clinicAdmin <- map["clinicAdmin"]
		isActive <- map["isActive"]
		clinicMembership <- map["clinicMembership"]
		clinicModules <- map["clinicModules"]
		createdAt <- map["createdAt"]
        canLeaveGroup <- map["canLeaveGroup"]
		role <- map["role"]
        userGroupNotification <- map["userGroupNotification"]
		memberCount <- map["memberCount"]
		folder <- map["folder"]
        admins <- map["admins"]
        groupStatus <- map["groupStatus"]
	}

}
