//
//  ThreadComments.swift
//  onlineDoc
//
//  Created by Piecyfer on 12/02/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//


import Foundation
import ObjectMapper

struct ThreadComment : Mappable{
    var comment_type:String?
    var firebaseCommentID:String?
    var multiFiles:[ThreadComment]?
    var added_date : String?
    var attachment_id : Any?{
        didSet{
            if let ID = attachment_id as? Int{
                attachmentIntId = ID
            }else if let ID = attachment_id as? String{
                attachmentStringId = ID
            }
        }
    }
    var clinic_id : Any?
    var comment : String?
    var doc_id : String?
    var file_ext : String?
    var file_main_type : String?
    var file_name : String?
    var file_path : String?
    var file_size : String?
    var posted_from : String?
    var real_name : String?
    var posted_param : String = ""
    var commentId: Any?{
        didSet{
            if let ID = commentId as? Int{
                commentIntId = ID
            }else if let ID = commentId as? String{
                commentStringId = ID
            }
        }
    }
    var profile : CommentorProfile?   
    var commentBy : GroupMember?
    var idInt : Int?
    var idStirng : String?
    var id : Any?{
        didSet{
            if let ID = id as? Int{
                idInt = ID
            }else if let ID = id as? String{
                idStirng = ID
            }
        }
    }
        /*
        didSet{
            if id != nil{
                let intID = Int(id!)
                if let iD = intID{
                    self.idInt = iD
                }
            }
            
        }
        */
    
    var user_id : Any?{
        didSet{
            if let ID = user_id as? Int{
                user_idInt = ID
            }else if let ID = user_id as? String{
                user_idString = ID
            }
        }
    }
    var user_idInt : Int?{
        didSet{
            if user_idString == nil{
                if let id = user_idInt{
                    user_idString = String(id)
                }
            }
        }
    }
    var user_idString: String?{
        didSet{
            if user_idInt == nil{
                if let id = user_idString{
                    user_idInt = Int(id)
                }
            }
        }
    }
    
    var commentStringId: String?
    var commentIntId : Int?
    var attachmentStringId : String?
    var attachmentIntId : Int?
    var onlineStatus:OnlineStatus?
    
    init?(map: Map) {

    }

    init() {}
    mutating func mapping(map: Map) {

        comment_type <- map["comment_type"]
        multiFiles <- map["files"]
        added_date <- map["added_date"]
        attachment_id <- map["attachment_id"]
        clinic_id <- map["clinic_id"]
        comment <- map["comment"]
        
        doc_id <- map["doc_id"]
        file_ext <- map["file_ext"]
        file_main_type <- map["file_main_type"]
        file_name <- map["file_name"]
        file_path <- map["file_path"]
        file_size <- map["file_size"]
        id <- map["id"]
        posted_from <- map["posted_from"]
        real_name <- map["real_name"]
        commentId <- map["commentId"]
        user_id <- map["user_id"]
        firebaseCommentID <- map["firebaseCommentId"]
    }

}

