/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct TimeLineData : Mappable{
    var activityId : Int?
    var activityRefId : Int?
    var threadId : Int?
    var activityType : String?
    var clinicId : Int?
    var clinicName : String?
    var clinicType : String?
    var createTime : CreateTime?
    var userId : Int?
    var profilePicture : String?
    var fullName : String?
    var docUsersType : String?
    var writeUsersType : String?
    var writeUsers : Bool?
    var allowEdit : Bool?
    var allowDel : Bool?
    var allowDelete: Bool?{
        didSet{
            if let delete = allowDelete{
                allowDel = delete
            }
        }
    }
    var folderName : String?
    var folder : Int?
    var loginUserRole : String?
    var isAttachment : String?
    var realName : String?
    var title : String?
    var journal : String?
    var priority : String?
    var notification : String?
    var sendEmail : String?
    var fireBaseId : String?
    var clinicAdmins : String?{
        didSet{
            adminsArray = [String]()
            if let res = clinicAdmins?.commaStrtoArray{
                adminsArray = res
                
            }
        }
    }
    var fileUploadRights : String?
    var invitationSendBy : String?
    var comments : [ThreadComment]?
    var adminsArray = [String]()
    var showAll = false
    var isFromServer = false
    var attachments : [Attachment]?
    init?(map: Map) {

    }
    init(){
        
    }

    mutating func mapping(map: Map) {

        activityId <- map["activityId"]
        activityRefId <- map["activityRefId"]
        threadId <- map["threadId"]
        activityType <- map["activityType"]
        clinicId <- map["clinicId"]
        clinicName <- map["clinicName"]
        clinicType <- map["clinicType"]
        createTime <- map["createTime"]
        userId <- map["userId"]
        profilePicture <- map["profilePicture"]
        fullName <- map["fullName"]
        docUsersType <- map["docUsersType"]
        writeUsersType <- map["writeUsersType"]
        writeUsers <- map["writeUsers"]
        allowEdit <- map["allowEdit"]
        allowDel <- map["allowDel"]
        allowDelete <- map["allowDelete"]
        folderName <- map["folderName"]
        folder <- map["folder"]
        loginUserRole <- map["loginUserRole"]
        isAttachment <- map["isAttachment"]
        realName <- map["realName"]
        title <- map["title"]
        journal <- map["journal"]
        priority <- map["priority"]
        notification <- map["notification"]
        sendEmail <- map["sendEmail"]
        fireBaseId <- map["fireBaseId"]
        clinicAdmins <- map["clinicAdmins"]
        fileUploadRights <- map["fileUploadRights"]
        invitationSendBy <- map["invitationSendBy"]
    }

}



/*
{
	var activityId : Int?
	var activityRefId : Int?
	var threadId : Int?
	var activityType : String?
	var clinicId : Int?
	var clinicName : String?
	var clinicType : String?
	var createTime : CreateTime?
	var userId : Int?
	var profilePicture : String?
	var fullName : String?
	var docUsersType : String?
	var writeUsersType : String?
	var writeUsers : Bool?
	var allowEdit : Bool?{
        didSet{
            if let edit = allowEdit{
                isEdit = edit
            }
        }
    }
    var allowDelete : Bool?{
        didSet{
            if let del = allowDelete{
                isDelete = del
            }
        }
    }
    var allowDel: Bool?{
        didSet{
            if allowDelete == nil{
                allowDelete = allowDel
            }
        }
    }
	var folderName : String?
	var folder : Int?
	var loginUserRole : String?
	var isAttachment : String?
	var realName : String?
	var title : String?
	var journal : String?
    var priority : String?
    var notification : String?
    var sendEmail : String?
    var clinicAdmins: String?{
        didSet{
            adminsArray = [String]()
            if let res = clinicAdmins?.commaStrtoArray{
                adminsArray = res
                
            }
        }
    }
    var fileUploadRights: String?
    var comments : [ThreadComment]?
    var isEdit = false
    var isDelete = false
    var isModified = false
    var fireBaseId : String?
    var adminsArray = [String]()

    required init?(map: Map) {
        
    }
    
    func setComments(_ data : [ThreadComment]?){
        self.comments = [ThreadComment]()
        self.comments = data
    }
    
    init(){}

    func mapping(map: Map) {
		activityId <- map["activityId"]
		activityRefId <- map["activityRefId"]
		threadId <- map["threadId"]
		activityType <- map["activityType"]
		clinicId <- map["clinicId"]
		clinicName <- map["clinicName"]
		clinicType <- map["clinicType"]
		createTime <- map["createTime"]
		userId <- map["userId"]
		profilePicture <- map["profilePicture"]
		fullName <- map["fullName"]
		docUsersType <- map["docUsersType"]
		writeUsersType <- map["writeUsersType"]
		writeUsers <- map["writeUsers"]
		allowEdit <- map["allowEdit"]
		allowDelete <- map["allowDelete"]
        allowDel <- map["allowDel"]
		folderName <- map["folderName"]
		folder <- map["folder"]
		loginUserRole <- map["loginUserRole"]
		isAttachment <- map["isAttachment"]
		realName <- map["realName"]
		title <- map["title"]
		journal <- map["journal"]
        priority <- map["priority"]
        notification <- map["notification"]
        sendEmail <- map["sendEmail"]
		fireBaseId <- map["fireBaseId"]
        clinicAdmins <- map["clinicAdmins"]
        fileUploadRights <- map["fileUploadRights"]
	}
    
//    mutating func testFirebaseData(id : String) {
//        var values : [ThreadComment]? = [ThreadComment]()
//        FirebaseManager.shared.threadComments(id: "4571_647c5366be0462fc93c4ec358a56fd"){ (status, data) in
//            if status{
//                values =  data as? [ThreadComment]
//            }
//        }
//        comments = values
//    }
}
*/
