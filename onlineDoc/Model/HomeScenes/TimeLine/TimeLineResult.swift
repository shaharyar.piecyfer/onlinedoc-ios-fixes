
import Foundation
import ObjectMapper

struct TimeLineResult : Mappable {
	var data : [TimeLineData]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		data <- map["data"]
	}

}


struct TimeineResult : Mappable {
    var data : [ThreadModel]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        data <- map["clinic_docs"]
    }

}


struct SearchTimeineResult : Mappable {
    var threads : [ThreadModel]?
    var users : [User]?
    var groups : [GroupModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        threads <- map["threads"]
        users <- map["users"]
        groups <- map["clinics"]
    }
}

class SearchFilters {
    var index: Int = 0
    var title: String = ""
    var count: Int = 0
    
    init(index: Int, title: String, count: Int) {
        self.index = index
        self.title = title
        self.count = count
    }
}

struct SearchCount: Mappable {
    var clinics_count: Int?
    var users_count: Int?
    var threads_count: Int?
    var files_count: Int?
    var comments_count: Int?
    var comments_files_count: Int?
    
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        clinics_count <- map["clinics_count"]
        users_count <- map["users_count"]
        threads_count <- map["threads_count"]
        files_count <- map["files_count"]
        comments_count <- map["comments_count"]
        comments_files_count <- map["comments_files_count"]
    }
}
