/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct TimelineComments : Mappable {
	var threadId : Int?
	var loginUserRole : String?
	var clinicName : String?
	var folderName : String?
	var folderId : Int?
	var title : String?
	var isAttachment : String?
	var isDraft : String?
	var journal : String?
	var docUsersType : String?
	var writeUsersType : String?
	var writeUsers : String?
	var docUsers : String?
	var firstName : String?
	var lastName : String?
	var profilePicture : String?
	var fileName : String?
	var realName : String?
	var filePath : String?
	var fileId : String?
	var fileSubType : String?
	var fileExt : String?
	var fileMainType : String?
	var fullName : String?
	var uploadFrom : String?
	var comment : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		0 <- map["0"]
		threadId <- map["threadId"]
		loginUserRole <- map["loginUserRole"]
		clinicName <- map["clinicName"]
		folderName <- map["folderName"]
		folderId <- map["folderId"]
		title <- map["title"]
		isAttachment <- map["isAttachment"]
		isDraft <- map["isDraft"]
		journal <- map["journal"]
		docUsersType <- map["docUsersType"]
		writeUsersType <- map["writeUsersType"]
		writeUsers <- map["writeUsers"]
		docUsers <- map["docUsers"]
		firstName <- map["firstName"]
		lastName <- map["lastName"]
		profilePicture <- map["profilePicture"]
		fileName <- map["fileName"]
		realName <- map["realName"]
		filePath <- map["filePath"]
		fileId <- map["fileId"]
		fileSubType <- map["fileSubType"]
		fileExt <- map["fileExt"]
		fileMainType <- map["fileMainType"]
		fullName <- map["fullName"]
		uploadFrom <- map["uploadFrom"]
		comment <- map["comment"]
	}

}
