/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct TimelineData : Mappable {
	var activityId : Int?
	var activityRefId : Int?
	var threadId : Int?
	var activityType : String?
	var clinicId : Int?
	var clinicName : String?
	var createTime : CreateTime?
	var userId : Int?
	var profilePicture : String?
	var fullName : String?
	var docUsersType : String?
	var writeUsersType : String?
	var writeUsers : Bool?
	var allowEdit : Bool?
	var allowDel : Bool?
	var folderName : String?
	var folder : Int?
	var loginUserRole : String?
	var isAttachment : String?
	var realName : String?
	var title : String?
	var journal : String?
	var fireBaseId : String?
	var comments : [Comments]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		activityId <- map["activityId"]
		activityRefId <- map["activityRefId"]
		threadId <- map["threadId"]
		activityType <- map["activityType"]
		clinicId <- map["clinicId"]
		clinicName <- map["clinicName"]
		createTime <- map["createTime"]
		userId <- map["userId"]
		profilePicture <- map["profilePicture"]
		fullName <- map["fullName"]
		docUsersType <- map["docUsersType"]
		writeUsersType <- map["writeUsersType"]
		writeUsers <- map["writeUsers"]
		allowEdit <- map["allowEdit"]
		allowDel <- map["allowDel"]
		folderName <- map["folderName"]
		folder <- map["folder"]
		loginUserRole <- map["loginUserRole"]
		isAttachment <- map["isAttachment"]
		realName <- map["realName"]
		title <- map["title"]
		journal <- map["journal"]
		fireBaseId <- map["fireBaseId"]
		comments <- map["comments"]
	}

}
