//
//  ParamsModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 15/07/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation

struct PostCommentParams {
    var clinic_id: Int!
    var doc_id: Int!
    var comment_type: CommentTypes!
    var commentId: Int?
    var comment: String?
    var files: [String]?
}

struct PostMessageParams {
    var message_type: MessageTypes!
    var messageId: Int?
    var receiver_id: Int?
    var message: String?
    var files: [String]?
}
