//
//  CommentorProfile.swift
//  onlineDoc
//
//  Created by Piecyfer on 18/02/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct CommentorProfile : Mappable {
    var f_name : String?
    var id : Int?
    var img : String?
    var l_name : String?
    var name : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        f_name <- map["f_name"]
        id <- map["id"]
        img <- map["img"]
        l_name <- map["l_name"]
        name = (f_name ?? "") + " " + (l_name ?? "")
    }
}
