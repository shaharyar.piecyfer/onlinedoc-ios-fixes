//
//  EmailModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 19/01/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct EmailResponseModel : Mappable {
    var data : [EmailModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        data <- map["user_emails"]
    }
}

struct EmailModel : Mappable {
    var id : Int?
    var email : String?
    var is_primary : Bool?
    var confirmation_token : String?
    var is_confirm : Bool?
    var user_id : Int?
    var created_at : String?
    var updated_at : String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id <- map["id"]
        email <- map["email"]
        is_primary <- map["is_primary"]
        confirmation_token <- map["confirmation_token"]
        is_confirm <- map["is_confirm"]
        user_id <- map["user_id"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }
}
