//
//  ODMember.swift
//  onlineDoc
//
//  Created by  Piecyfer  on 18/05/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct ODMember : Mappable {
    var id : Int?
    var email : String?
    var preName : String?
    var firstName : String?
    var lastName : String?
    var profilePicture : String?
    var userFireBaseId : String?
    var name: String!
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        email <- map["email"]
        preName <- map["preName"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        profilePicture <- map["profilePicture"]
        userFireBaseId <- map["userFireBaseId"]
        name = (firstName ?? "") + " " + (lastName ?? "")
    }

}

