//
//  WebLinkModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 18/06/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct WebLinkResponseModel : Mappable {
    var user_web_link : [WebLinkModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        user_web_link <- map["user_web_links"]
    }
}

struct WebLinkModel : Mappable, Codable {
    var id : Int?
    var link : String?
    var user_id : Int?
    var created_at : String?
    var updated_at : String?

    init(){}
    init?(map: Map) {}

    mutating func mapping(map: Map) {

        id <- map["id"]
        link <- map["link"]
        user_id <- map["user_id"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }

}
