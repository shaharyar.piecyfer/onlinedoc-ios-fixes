//
//  SearchHistoryModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 01/02/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct SearchHistoryListModel: Mappable {
    var data : [SearchHistoryModel]?
    
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        data <- map["search_histories"]
    }

}

struct SearchHistoryModel: Mappable {
    var id : Int?
    var text : String?
    var is_active : Bool?
    var created_at : String?
    var updated_at : String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id <- map["id"]
        text <- map["text"]
        is_active <- map["is_active"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }

}
