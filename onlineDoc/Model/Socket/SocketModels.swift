//
//  SocketModels.swift
//  onlineDoc
//
//  Created by Piecyfer on 12/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct SocketAppearanceModel : Mappable {
    var type: String?
    var data: SocketAppearanceDataModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        type <- map["type"]
        data <- map["user"]
    }
}

struct SocketAppearanceDataModel : Mappable {
    var user: User?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        user <- map["user"]
    }
}

struct SocketMessageModel : Mappable {
    var type: String?
    var data: ChatModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        type <- map["type"]
        data <- map["message"]
    }
}

struct SocketChatNotificationModel : Mappable {
    var type: String?
    var data: ChatRoomModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        type <- map["type"]
        data <- map["chat_room"]
    }
}

struct SocketCommentBaseModel : Mappable {
    var type: String?
    var comment : SocketCommentModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        type <- map["type"]
        comment <- map["comment"]
    }
}

struct SocketCommentModel : Mappable {
    var data : ThreadCommentModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        data <- map["doc_comment"]
    }
}

struct SocketThreadNotificationBaseModel : Mappable {
    var type: String?
    var user_notification_count: Int?
    var all_notification_count: Int?
    var doc: ThreadNotificationModel?
    var activity_type: String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        type <- map["type"]
        user_notification_count <- map["user_notification_count"]
        all_notification_count <- map["all_notification_count"]
        doc <- map["doc"]
        activity_type <- map["activity_type"]
    }
}

struct SocketGroupRequestNotificationBaseModel : Mappable {
    var type: String?
    var user_notification_count: Int?
    var all_notification_count: Int?
    var notification: SocketGroupRequestNotificationModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        type <- map["type"]
        user_notification_count <- map["user_notification_count"]
        all_notification_count <- map["all_notification_count"]
        notification <- map["notification"]
    }
}

struct SocketGroupRequestNotificationModel : Mappable {
    var notification : MemberModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        notification <- map["notification"]
    }
}

struct SocketThreadNotificationModel : Mappable {
    var data : ThreadNotificationModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        data <- map["doc"]
    }
}

struct SocketReadFromOtherDeviceModel : Mappable {
    var type: String?
    var chat_room_id: Int?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        type <- map["type"]
        chat_room_id <- map["chat_room_id"]
    }
}

struct SocketClinicModel : Mappable {
    var type: String?
    var data: GroupModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        type <- map["type"]
        data <- map["clinic"]
    }
}
