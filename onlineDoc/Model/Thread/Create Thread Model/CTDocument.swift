/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct CTDocument : Mappable {
    var id : Int?
    var addedDate : AddedDate?
    var title : String?
    var clinicId : Int?
    var userId : Int?
    var folder : Int?
    var docUsers : String?
    var docUsersType : String?
    var writeUsersType : String?
    var writeUsers : String?
    var fileUploadRights : String?
    var journal : String?
    var isDraft : String?
    var isSound : String?
    var isImage : String?
    var isVideo : String?
    var isFile : String?
    var isAttachment : String?
    var firstName : String?
    var lastName : String?
    var profilePicture : String?
    var draft : String?
    var allow_edit : Bool?
    var allow_del : Bool?
    var files : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        addedDate <- map["addedDate"]
        title <- map["title"]
        clinicId <- map["clinicId"]
        userId <- map["userId"]
        folder <- map["folder"]
        docUsers <- map["docUsers"]
        docUsersType <- map["docUsersType"]
        writeUsersType <- map["writeUsersType"]
        writeUsers <- map["writeUsers"]
        fileUploadRights <- map["fileUploadRights"]
        journal <- map["journal"]
        isDraft <- map["isDraft"]
        isSound <- map["isSound"]
        isImage <- map["isImage"]
        isVideo <- map["isVideo"]
        isFile <- map["isFile"]
        isAttachment <- map["isAttachment"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        profilePicture <- map["profilePicture"]
        draft <- map["draft"]
        allow_edit <- map["allow_edit"]
        allow_del <- map["allow_del"]
        files <- map["files"]
    }

}
