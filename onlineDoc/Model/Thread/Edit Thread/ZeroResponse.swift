/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct ZeroResponse : Mappable {
    var threadId : Int?
    var userId : Int?
    var addedDate : AddedDate?
    var folderName : String?
    var clinicName : String?
    var title : String?
    var clinicId : Int?
    var journal : String?
    var isDraft : String?
    var isSound : String?
    var isImage : String?
    var isVideo : String?
    var isFile : String?
    var docUsers : String?
    var docUsersType : String?
    var fileUploadRights : String?
    var writeUsers : String?
    var writeUsersType : String?
    var folder : Int?
    var priority : String?
    var sendEmail : String?
    var notification : String?
    var email : String?
    var preName : String?
    var firstName : String?
    var lastName : String?
    var profilePicture : String?
    var name = ""
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        threadId <- map["threadId"]
        userId <- map["userId"]
        addedDate <- map["addedDate"]
        folderName <- map["folderName"]
        clinicName <- map["clinicName"]
        title <- map["title"]
        clinicId <- map["clinicId"]
        journal <- map["journal"]
        isDraft <- map["isDraft"]
        isSound <- map["isSound"]
        isImage <- map["isImage"]
        isVideo <- map["isVideo"]
        isFile <- map["isFile"]
        docUsers <- map["docUsers"]
        docUsersType <- map["docUsersType"]
        fileUploadRights <- map["fileUploadRights"]
        writeUsers <- map["writeUsers"]
        writeUsersType <- map["writeUsersType"]
        folder <- map["folder"]
        priority <- map["priority"]
        sendEmail <- map["sendEmail"]
        notification <- map["notification"]
        email <- map["email"]
        preName <- map["preName"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        profilePicture <- map["profilePicture"]
        name = (firstName ??  "") + " " + (lastName ?? "")
    }

}
