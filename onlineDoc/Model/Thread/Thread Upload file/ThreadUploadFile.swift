/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct ThreadUploadFile : Mappable {
	var doc_file_id : Int?
	var id : Int?
	var real_name : String?
	var file_name : String?
	var file_ext : String?
	var file_size : Int?
	var file_display_size : String?
	var file_main_type : String?
	var file_sub_type : String?
	var file_path : String?
	var file_added_date : String?
	var user_id : String?
	var clinic_id : String?
	var posted_from : String?
	var added_date : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		doc_file_id <- map["doc_file_id"]
		id <- map["id"]
		real_name <- map["real_name"]
		file_name <- map["file_name"]
		file_ext <- map["file_ext"]
		file_size <- map["file_size"]
		file_display_size <- map["file_display_size"]
		file_main_type <- map["file_main_type"]
		file_sub_type <- map["file_sub_type"]
		file_path <- map["file_path"]
		file_added_date <- map["file_added_date"]
		user_id <- map["user_id"]
		clinic_id <- map["clinic_id"]
		posted_from <- map["posted_from"]
		added_date <- map["added_date"]
	}

}
