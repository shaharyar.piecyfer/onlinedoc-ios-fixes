//
//  ThreadModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 25/06/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct DocsResponseModel: Mappable {
    var result : ThreadModel?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        result <- map["doc"]
    }
}

struct ThreadListModel: Mappable {
    var result : [ThreadModel]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        result <- map["doc"]
    }
}

struct ClinicDocModel: Mappable {
    var result : [ThreadModel]?

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        result <- map["clinic_docs"]
    }
}

struct ThreadModel : Mappable {
    var id : Int?
    var clinic_id : Int?
    var clinic_category_id : Int?
    var title : String?
    var description : String?
    var doc_type : String?
    var priority : String?
    var created_at : String?
    var updated_at : String?
    var clinic_name: String?
    var clinic_category_name: String?
    var doc_creator: User?
    var doc_notification_on: Bool?
    var is_admin: Bool?
    var doc_files: [FileModel]?
    var clinic_doc_comments: [ThreadCommentModel]?
    var clinic_doc_file_count: Int?
    var clinic_doc_comments_count: Int?
    var clinic_doc_comment_file_count: Int?
    var doc_notification_count: Int?
    
    var showAllComments = false
    var groupImage: FileModel?
    
    //for my thread
    var clinicImage: FileModel?

    init?(){}
    init?(map: Map) {
    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        clinic_id <- map["clinic_id"]
        clinic_category_id <- map["clinic_category_id"]
        title <- map["title"]
        description <- map["description"]
        doc_type <- map["doc_type"]
        priority <- map["priority"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        clinic_name <- map["clinic_name"]
        clinic_category_name <- map["clinic_category_name"]
        doc_creator <- map["doc_creator"]
        doc_notification_on <- map["doc_notification_on"]
        is_admin <- map["is_admin"]
        doc_files <- map["doc_files"]
        clinic_doc_comments <- map["clinic_doc_comments"]
        clinic_doc_file_count <- map["clinic_doc_file_count"]
        clinic_doc_comments_count <- map["clinic_doc_comments_count"]
        clinic_doc_comment_file_count <- map["clinic_doc_comment_file_count"]
        doc_notification_count <- map["doc_notification_count"]
        groupImage <- map["file"]
        clinicImage <- map["clinic_file"]
    }
    
}

struct ThreadCommentsModel : Mappable {
    var clinic_doc_comments: [ThreadCommentModel]?
    
    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        clinic_doc_comments <- map["clinic_doc_comments"]
    }
}

struct ThreadCommentResponse : Mappable {
    var data: ThreadCommentModel?
    
    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        data <- map["doc_comment"]
    }
}

struct CommentFileListModel : Mappable {
    var data: [ThreadCommentModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        data <- map["clinic_doc_files_comments"]
    }
}

struct ThreadCommentModel : Mappable {
    var id : Int?
    var doc_id : Int?
    var clinic_id : Int?
    var user_id : Int?
    var comment : String?
    var comment_type : String?
    var is_active : Bool?
    var created_at : String?
    var updated_at : String?
    var comment_file_count : Int?
    var files: [FileModel]?
    var comment_creator: CommentCreatorModel?

    init?(){}
    init?(map: Map) {
    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        doc_id <- map["doc_id"]
        clinic_id <- map["clinic_id"]
        user_id <- map["user_id"]
        comment <- map["comment"]
        comment_type <- map["comment_type"]
        is_active <- map["is_active"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        comment_file_count <- map["comment_file_count"]
        files <- map["files"]
        comment_creator <- map["comment_creator"]
    }
    
}

struct CommentCreatorModel : Mappable {
    var id : Int?
    var full_name : String?
    var file: FileModel?
    var comment_user_file: FileModel?
    
    init?(){}
    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        id <- map["id"]
        full_name <- map["full_name"]
        file <- map["file"]
        comment_user_file <- map["comment_user_file"]
    }
}

struct TimelineParams {
    var clinic_ids = [Int]()
    var category_ids = [Int]()
    var page = 1
    var limit = pageLimit3
    var search: String?
    var home_enabled: Bool?
    var type: String = SearchFilterTypes.NONE.rawValue //it can be thread, file, comment
}

enum SearchFilterTypes: String, CaseIterable {
    case ALL = "all"
    case THREAD = "thread"
    case USER = "user"
    case CLINIC = "clinic"
    case COMMENT = "comment"
    case FILE = "file"
    case NONE = "none"
}
