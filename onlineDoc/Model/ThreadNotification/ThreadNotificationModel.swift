//
//  ThreadNotificationModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 11/01/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

class ThreadNotificationModel : Mappable { //it should be class because need to update notification count
    var user_notification_count : Int?
    var activity_type: String?
    var doc : ThreadModel?
    
    init(){}
    required init?(map: Map) {}
    func mapping(map: Map) {
        user_notification_count <- map["user_notification_count"]
        activity_type <- map["activity_type"]
        doc <- map["doc"]
    }
}

struct ThreadNotificationPayload : Mappable {
    var thread_id : String?
    var group_id : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        thread_id <- map["thread_id"]
        group_id <- map["group_id"]
    }

}
