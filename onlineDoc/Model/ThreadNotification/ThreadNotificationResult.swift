//
//  ThreadNotificationResult.swift
//  onlineDoc
//
//  Created by Piecyfer on 11/01/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct ThreadNotificationResult : Mappable {
    var data : [ThreadNotificationModel]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        data <- map["doc_notifications"]
    }
}
