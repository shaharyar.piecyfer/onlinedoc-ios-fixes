//
//  DirectUploadModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 07/07/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct DirectUploadModel : Mappable {
    var id : Int?
    var key : String?
    var filename : String?
    var content_type : String?
    var metadata : DirectUploadMetadataModel?
    var service_name : String?
    var byte_size : Int?
    var checksum : String?
    var created_at : String?
    var signed_id : String?
    var attachable_sgid : String?
    var direct_upload : DirectUploadInfoModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {

        id <- map["id"]
        key <- map["key"]
        filename <- map["filename"]
        content_type <- map["content_type"]
        metadata <- map["metadata"]
        service_name <- map["service_name"]
        byte_size <- map["byte_size"]
        checksum <- map["checksum"]
        created_at <- map["created_at"]
        signed_id <- map["signed_id"]
        attachable_sgid <- map["attachable_sgid"]
        direct_upload <- map["direct_upload"]
    }
}

struct DirectUploadHeaderModel : Mappable {
    var contentType : String?
    var contentMD5 : String?
    var x_ms_blob_content_disposition : String?
    var x_ms_blob_type : String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {

        contentType <- map["Content-Type"]
        contentMD5 <- map["Content-MD5"]
        x_ms_blob_content_disposition <- map["x-ms-blob-content-disposition"]
        x_ms_blob_type <- map["x-ms-blob-type"]
    }
}

struct DirectUploadInfoModel : Mappable {
    var url : String?
    var headers : DirectUploadHeaderModel?

    init?(map: Map) {}

    mutating func mapping(map: Map) {

        url <- map["url"]
        headers <- map["headers"]
    }
}


struct DirectUploadMetadataModel : Mappable {

    init?(map: Map) {}
    mutating func mapping(map: Map) {
    }
}


struct DirectUploadParams {
    var filename: String?
    var content_type: String?
    var byte_size: Int?
//    var checksum: String?
}
