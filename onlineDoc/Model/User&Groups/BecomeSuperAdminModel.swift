//
//  BecomeSuperAdminModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 12/03/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct BecomeSuperAdminModel : Mappable {
    var result : BecomeSuperAdminResultModel?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        result <- map["result"]
    }
}

struct BecomeSuperAdminResultModel : Mappable {
    var message : String?
    var superAdmin : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        message <- map["message"]
        superAdmin <- map["SuperAdmin"]
    }
}
