//
//  File.swift
//  onlineDoc
//
//  Created by Piecyfer on 16/12/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct Group : Mappable {
    var id : Int?
    var memberCount : String?
    var clinicDescription : String?
    var isPublic : String?
    var adminRole : String?
    var requestStatus : String?
    var userId : Int?
    var groupImage : String?
    var dashboardUrl : String?
    var clinicType : String?
    var modules : String?
    var clinicName : String?
    var isMember: String?

   
    init?(map: Map) {

    }
    init() {}

    mutating func mapping(map: Map) {

        id <- map["id"]
        memberCount <- map["memberCount"]
        clinicDescription <- map["clinicDescription"]
        isPublic <- map["isPublic"]
        adminRole <- map["adminRole"]
        requestStatus <- map["requestStatus"]
        userId <- map["userId"]
        groupImage <- map["groupImage"]
        dashboardUrl <- map["dashboardUrl"]
        clinicType <- map["clinicType"]
        modules <- map["modules"]
        clinicName <- map["clinicName"]
        isMember = "N/A"
    }

}
