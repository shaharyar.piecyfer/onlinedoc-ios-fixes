//
//  GroupCategoryModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 18/06/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct GroupCategoryBaseModel: Mappable {
    var data: GroupCategoryModel?
    
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        data <- map["clinic_category"]
    }
}

class GroupCategoryModel : Mappable {
    var id : Int?
    var clinic_id : Int?
    var name : String?
    var sort_order : Int?
    var is_active : Bool = true
    var created_at : String?
    var updated_at : String?
    var is_available : Bool = false
    var total_doc_count : Int?
    var category_notification_count : Int?
    var isChecked = false
    var isSelectedToEdit = false
    var isSelected = false
    //for pre categories
    var isPreGroupCat = false
    var isUpdated = false

    required init?(map: Map) {
    }
    
    init() {
    }

    func mapping(map: Map) {

        id <- map["id"]
        clinic_id <- map["clinic_id"]
        name <- map["name"]
        sort_order <- map["sort_order"]
        is_active <- map["is_active"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        is_available <- map["is_available"]
        total_doc_count <- map["total_doc_count"]
        category_notification_count <- map["category_notification_count"]
    }

}
