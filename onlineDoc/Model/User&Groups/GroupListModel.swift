//
//  GroupListModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 02/07/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct GroupListModel : Mappable {
    var clinics : [GroupModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        clinics <- map["clinics"]
    }
}

struct GroupListParentResult : Mappable {
    var admin_clinics: [GroupModel]?
    var other_clinics: [GroupModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        admin_clinics <- map["admin_clinics"]
        other_clinics <- map["other_clinics"]
    }
}

struct ClinicListModel : Mappable {
    var groups: [GroupModel]?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        groups <- map["groups"]
    }
}
