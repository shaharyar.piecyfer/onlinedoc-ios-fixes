//
//  GroupMemberModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 30/06/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct GroupMemberModel : Mappable {
    var joined_users_count: Int?
    var requested_users_count: Int?
    var clinic_admins_count: Int?
    var joined_users: [MemberModel]?
    var requested_users: [MemberModel]?
    var clinic_admins: [MemberModel]?

    init() {}
    init?(map: Map) {
    }

    mutating func mapping(map: Map) {

        joined_users_count <- map["joined_users_count"]
        requested_users_count <- map["requested_users_count"]
        clinic_admins_count <- map["clinic_admins_count"]
        joined_users <- map["joined_users"]
        requested_users <- map["requested_users"]
        clinic_admins <- map["clinic_admins"]
    }

}


struct MemberModel : Mappable {
    var id : Int?
    var user_id : Int?
    var clinic_role : String?
    var role : String?
    var request_status : String?
    var full_name : String?
    var pre_name : String?
    var file : FileModel?
    var join_date : String?
    var phone: String?
    var email: String?
    
    var clinic_id: Int?
    var clinic_name: String?
    var date: String?
    
    var isSelected = false
    var chatRoomId : Int?

    init() {}
    init?(map: Map) {
    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        user_id <- map["user_id"]
        clinic_role <- map["clinic_role"]
        role <- map["role"]
        request_status <- map["request_status"]
        full_name <- map["full_name"]
        pre_name <- map["pre_name"]
        file <- map["file"]
        join_date <- map["join_date"]
        phone <- map["phone"]
        email <- map["email"]
        
        clinic_id <- map["clinic_id"]
        clinic_name <- map["clinic_name"]
        date <- map["date"]
        chatRoomId <- map["chat_room_id"]
    }

}
