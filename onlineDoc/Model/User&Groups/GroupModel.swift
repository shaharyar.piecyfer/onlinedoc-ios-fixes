//
//  GroupModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 21/06/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

class GroupModel : Mappable {
    var id : Int?
    var name : String?
    var description : String?
    var email : String?
    var address : String?
    var phone : String?
    var additional_phone : String?
    var fax : String?
    var can_send_invite : String?
    var can_approve : String?
    var is_public : Bool?
    var is_active : Bool?
    var created_at : String?
    var updated_at : String?
    var clinic_admins : [GroupAdminModel]?
    var joined_members : Int?
    var requested_members : Int?
    var clinic_categories : [GroupCategoryModel]?
    var clinic_notifications_count : Int?
    var is_notification_clinic : Bool?
    var is_display_timeline : Bool?
    var can_leave_clinic : Bool?
    var last_visit : String?
    var file : FileModel?
    var group_notification_count : Int?
    var join_date: String?
    var request_status: String?
    var role: String?
    
    var member_count: Int?
    
    var current_user_notification_count: Int?
    var group_pinned : Bool?
    
    var memberStatus: String = "others"
    var isSelected = false

    required init?(map: Map) {

    }
    
    init() {}

    func mapping(map: Map) {

        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        email <- map["email"]
        address <- map["address"]
        phone <- map["phone"]
        additional_phone <- map["additional_phone"]
        fax <- map["fax"]
        can_send_invite <- map["can_send_invite"]
        can_approve <- map["can_approve"]
        is_public <- map["is_public"]
        is_active <- map["is_active"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        clinic_admins <- map["clinic_admins"]
        joined_members <- map["joined_members"]
        requested_members <- map["requested_members"]
        clinic_categories <- map["clinic_categories"]
        clinic_notifications_count <- map["clinic_notifications_count"]
        is_notification_clinic <- map["is_notification_clinic"]
        is_display_timeline <- map["is_display_timeline"]
        can_leave_clinic <- map["can_leave_clinic"]
        last_visit <- map["last_visit"]
        file <- map["file"]
        group_notification_count <- map["group_notification_count"]
        join_date <- map["join_date"]
        request_status <- map["request_status"]
        role <- map["role"]
        
        member_count <- map["member_count"]
        
        current_user_notification_count <- map["current_user_notification_count"]
        group_pinned <- map["group_pinned"]
    }
}

class GroupAdminModel : Mappable {
    var id : Int?
    var first_name : String?
    var last_name : String?
    var role : String?
    var file : FileModel?
    
    required init?(map: Map) {
    }
    
    init() {}

    func mapping(map: Map) {
        id <- map["id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        role <- map["role"]
        file <- map["file"]
    }
}
//
//struct Group : Mappable {
//    var id : Int?
//    var memberCount : String?
//    var clinicDescription : String?
//    var isPublic : String?
//    var adminRole : String?
//    var requestStatus : String?
//    var userId : Int?
//    var groupImage : String?
//    var dashboardUrl : String?
//    var clinicType : String?
//    var modules : String?
//    var clinicName : String?
//    var isMember: String?
