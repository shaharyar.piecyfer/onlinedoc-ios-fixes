//
//  NewGroupModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 21/06/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation

struct NewGroupModel {
    var name: String?
    var description: String?
    var can_send_invite: String?
    var can_approve: String?
    var is_public: Bool?
    var clinic_categories_attributes = [ClinicCategoryModel]()
    var file: String?
    
    init(){}
}

struct ClinicCategoryModel {
    var name: String?
    var sort_order: Int?
    //additional properties in case of edit group
    var id: Int?
    var is_available: Bool?
}
