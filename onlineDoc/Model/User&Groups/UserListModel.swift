//
//  UserListModel.swift
//  onlineDoc
//
//  Created by Piecyfer on 05/07/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserListModel : Mappable {
    var users : [User]?

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        users <- map["users"]
    }
}
