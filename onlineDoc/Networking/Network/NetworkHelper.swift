//
//  NetworkHelper.swift
//  Alpha_Pulse
//
//  Created by Piecyfer on 28/10/2019.
//  Copyright © 2019 Piecyfer. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}

class NetworkHelper{
    
    static let shared = NetworkHelper()
//    let controller = UIApplication.shared.windows.first?.rootViewController
    fileprivate init() {}
    
    typealias webRequestResponse = ([String:Any]?)-> Void
    typealias boolCallBack = (Bool)-> Void
    
    func executeUserRequest(path: String, params: Parameters? , method: HTTPMethod, requiredToken:Bool = true, isShowLoader: Bool = true ,completion: @escaping webRequestResponse) {
    
        if isShowLoader{
            baseVC?.showLoader()
        }
        
        if Connectivity.isConnectedToInternet {
            print("Internet Connected")
        } else {
            print("No Internet")
            if isShowLoader{
                baseVC?.hideLoader()
            }
            DispatchQueue.main.async {
                if !path.contains("app_versions") && !path.contains("get_user_all_notification_count") && !path.contains("total_chat_room_count") {
                    baseVC?.showAlert(To: NoInternetHeader.localized(), for: NoInternetMessage.localized()) {_ in}
                }
            }
            completion(nil)
        }
        
        var headers: HTTPHeaders = HTTPHeaders()
        var requestEncoding = URLEncoding.default
        if method == .get{
            requestEncoding = .default
        }else{
            requestEncoding = .httpBody
        }
        if requiredToken {
            guard let authenticationCode = AppConfig.shared.authToken else{return}
            guard authenticationCode != "" else{
                if let vc = baseVC?.getVC(storyboard: .MAIN, vcIdentifier: "MainNavView") {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = vc
                    appDelegate.window?.makeKeyAndVisible()
                }
                completion(nil)
                return
            }
            headers = [
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "\(authenticationCode)"
            ]
        }else{
            headers = [
                "Content-Type": "application/x-www-form-urlencoded",
            ]
        }
        
        AF.request(path,
                          method: method,
                          parameters: params,
                          encoding: requestEncoding,
                          headers: headers)
            .responseJSON(completionHandler: { response in
                    let responsStatusCode = response.response?.statusCode
//                    print(response)
                    
                    switch response.result {
                    case .success(let jsonResponse):
//                        print("NetworkHelper->success", jsonResponse)
//                        print("NetworkHelper->success->URL \(path)")
//                        print("NetworkHelper->success->statusCode \(String(describing: responsStatusCode))")
//                        print("NetworkHelper->success->Param \(String(describing: params))")
//                        print("NetworkHelper->success->headers \(String(describing: headers))")
                        if responsStatusCode == 200 || responsStatusCode == 201 {
                            if isShowLoader {
                                baseVC?.hideLoader()
                            }
                            if let jsonDict = jsonResponse as? [String:Any]{
                                completion(jsonDict)
                            }else{
                                if !path.contains("user/status/update"){
                                    self.ShowGeneralError(error: path, with: params)
                                }
                                completion(nil)
                            }
                        }else if responsStatusCode == 422 || responsStatusCode == 401 || responsStatusCode == 403{
                            baseVC?.hideLoader()
                            if let result = response.value {
                                if let JSON = result as? NSDictionary {
                                    if let message = JSON["errors"] as? [String] {
                                        if responsStatusCode == 401 && message.allSatisfy({$0.lowercased().contains("invalid request")}) {
                                            baseVC?.showAlert(To: "Alert".localized(), for: AuthError.localized()){_ in}
                                            AppConfig.shared.logout = ""
                                            baseVC?.navigationController?.popToRootViewController(animated: true)
                                            completion(nil)
                                        }
                                        else if responsStatusCode == 401 && path.contains("users/login") {
                                            if let errorResponse = jsonResponse as? [String:Any]{
                                                completion(errorResponse)
                                            }else{
                                                self.showErrors(message: message)
                                                completion(nil)
                                                return
                                            }
                                        }
                                        else if responsStatusCode == 401 && AppConfig.shared.authToken != "" {
                                            AppConfig.shared.logout = ""
                                            baseVC?.navigationController?.popToRootViewController(animated: true)
                                            completion(nil)
                                        }
                                        else{
                                            self.showErrors(message: message)
                                            completion(nil)
                                            return
                                        }
                                    }else{
                                        if !path.contains("user/status/update"){
                                            if responsStatusCode == 422, let obj = JSON["errors"] as? [String:Any], let message = obj["message"] as? String {
                                                self.showErrors(message: [message])
                                            }
                                            else if let message = JSON["message"] as? String {
                                                self.showErrors(message: [message])
                                            }
                                            else {
                                                self.ShowGeneralError(error: path, with: params)
                                            }
                                        }
                                        
                                        completion(nil)
                                    }
                                }else{
                                    if !path.contains("user/status/update"){
                                        self.ShowGeneralError(error: path, with: params)
                                        
                                    }
                                    completion(nil)
                                }
                            }else{
                                if !path.contains("user/status/update"){
                                    self.ShowGeneralError(error: path, with: params)
                                    
                                }
                                completion(nil)
                            }
                        }else{
                            if !path.contains("user/status/update"){
                                self.ShowGeneralError(error: path, with: params)
                                
                            }
                            completion(nil)
                        }
                    case .failure(let error):
                        print("\n\n===========Error===========")
                        print("Error Code: \(error._code) -> status code \(responsStatusCode ?? 0)")
                        print("URL \(path)")
                        print("Param \(String(describing: params))")
                        print("Error Messsage: \(error.localizedDescription)")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Server Error: " + str)
                        }
                        debugPrint(error as Any)
                        print("===========================\n\n")
                        
                        if !path.contains(ApiConfig.forceUpdate) && !path.contains(ApiConfig.docNotificationCount) && !path.contains(ApiConfig.chatUnreadCount) && !path.contains(ApiConfig.updateDocNotification) {
                            self.ShowGeneralError(error: path, with: params)
                        }
                        completion(nil)
                    }
            })
    }
    
    func ShowGeneralError(error : String = "",with params:Parameters? = Parameters() )  {
        baseVC?.hideLoader()
        if let params = params {
            print("ShowGeneralError -> params", params)
        }
        DispatchQueue.main.async {
            baseVC?.showAlert(To: "Error".localized(), for: "There is some issue with server, Please try again!".localized()) {_ in}
        }
        
//        AppConfig.shared.popAlert(To: "Request Failure".localized(), for: "There is some issue with server, Please try again! \nURL ::\(error) +\nPARAMS :: \(String(describing: params))", callingView: controller!)
    }
    
    func showErrors(message: [String])  {
        baseVC?.hideLoader()
        var msg = ""
        
        if message.count == 1 {
            msg = message[0]
        }else{
            for data in message {
                msg.append("\(data) \n")
            }
        }
        if msg.contains("associated record") {
            msg = "No associated record with this email exists"
        }
        else if msg.contains("valid email") {
            msg = "Enter valid email address"
        }
        else if msg.contains("Invalid username or password") {
            msg = "Invalid current password"
        }
        else if msg.contains("Your account is blocked.") {
            msg = "Your account is blocked. Please contact admin."
        }
        else if msg.contains("Your account is permanent deleted. Please contact admin.") {
            msg = "Your account is permanent deleted. Please contact admin."
        }
        else if msg.contains("Password does not match.") {
            msg = "Password does not match."
        }
        else if msg.contains("current password is required and can't be empty") {
            msg = "current password is required and can't be empty"
        }
        else if msg.contains("If there is an account associated with") {
            msg = "current password is required and can't be empty"
        }
        else if msg.contains("Email already exists") {
            msg = "Email already exists"
        }
        
//        let rootController = UIApplication.shared.keyWindow?.rootViewController
        DispatchQueue.main.async {
            baseVC?.showAlert(To: "Alert".localized(), for: msg.localized()) {_ in}
        }
    }
    
    func uploadWith(url: String, imageData: Data, headers: HTTPHeaders, progressBlock: ((_ percent: Double) -> Void)? = nil, completion: @escaping boolCallBack) {
//        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (t) in
//            baseVC?.showLoader()
            
            AF.upload(imageData, to: url, method: .put, headers: headers) { $0.timeoutInterval = Double.infinity }
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                    progressBlock?(progress.fractionCompleted)
                })
                .response { (response) in
                if let _ = response.error {
//                    baseVC?.hideLoader()
                    completion(false)
                }
                else {
                    completion(true)
                }
            }
//            .uploadProgress { (progress) in
//                print("NetworkHelper->uploadWith->progress", progress)
//                if progress.isFinished {
////                    baseVC?.hideLoader()
////                    completion(true)
//                    print("NetworkHelper->uploadWith->response->isFinished")
//                }
//            }
            
            
//            Alamofire.upload(multipartFormData: { (multipartFormData) in
//                multipartFormData.append(imageData, withName: "file")
//            }, usingThreshold: UInt64.init(), to: url, method: .put, headers: headers) { (result) in
//                switch result {
//                case .success(let upload, _, _):
//                    print("NetworkHelper->uploadWith", upload)
//                    upload.response(completionHandler: { (response) in
//                        baseVC?.hideLoader()
//                        completion(true)
//                        print("NetworkHelper->uploadWith->response", response)
//                    })
////                    .responseJSON{ response in
////                        if let jsonDict = response.result.value as? [String:Any]{
////                            baseVC?.hideLoader()
////                            completion(jsonDict)
////                        }else{
////                            if let err = response.error{
////                                self.showErrors(message: [err.localizedDescription])
////                                baseVC?.hideLoader()
////                                completion(nil)
////                            }else{
////                                self.ShowGeneralError()
////                                baseVC?.hideLoader()
////                                completion(nil)
////                            }
////                        }
////                        baseVC?.hideLoader()
////                        print("NetworkHelper->uploadWith->response", response)
////                    }
//                case .failure(let error):
//                    baseVC?.hideLoader()
//                    print("NetworkHelper->uploadWith->Error: \(error)")
//                    completion(false)
//                }
//            }
//        }
    }
}
