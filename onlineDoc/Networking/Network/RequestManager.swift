//
//  RequestManager.swift
//  Alpha_Pulse
//
//  Created by Piecyfer on 28/10/2019.
//  Copyright © 2019 Piecyfer. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class RequestManager {
    static let shared = RequestManager()
    private init() {}
    typealias imageUploadResponse = ((_ object: AnyObject)-> Void)
    typealias rrequestsResponse = ((_ status:Bool,_ object: AnyObject?) -> Void)
    
    func register(data:RegisterUserModel, completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.registerRoute
        
        var userParams : Dictionary<String,Any> = Dictionary()
//        userParams["dob"]               = data.dob
        userParams["phone"]             = data.phone
        userParams["email"]             = data.email
//        userParams["gender"]            = data.gender
        userParams["last_name"]         = data.last_name
        userParams["first_name"]        = data.first_name
        userParams["password"]          = data.password
        userParams["password_confirmation"]  = data.password_confirmation
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": userParams], method: .post, requiredToken : false) { (resultData) in
            if resultData != nil{
                print("OD - Register User -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: resultData!)
                completion(true,data as AnyObject)
            }
            else{
                completion(false, nil)
            }
        }
    }
    
    func login(credentials: LogInCredentials, completion: @escaping rrequestsResponse){
        let apiPath = ApiConfig.BASE_URL + ApiConfig.authRoute
        var authParams: Dictionary<String, Any> = Dictionary()
        authParams["email"] = credentials.userName
        authParams["password"] = credentials.passCode
        authParams["device_token"] = AppConfig.shared.pushToken
        authParams["device_type"] = "ios"
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": authParams], method: .post, requiredToken : false) { (resultData) in
            if resultData != nil{
                print("OD - Login -: Success")
                if let data = Mapper<LoginSuccessResponse>().map(JSON: resultData!), data.result != nil{
                    print("data->login->", data)
                    completion(true,data as AnyObject)
                }else{
                    let errorData = Mapper<Login401Response>().map(JSON: resultData!)
                    completion(true, errorData as AnyObject)
                }
            }
        }
    }
    
    func logout(completion: @escaping rrequestsResponse){
        let apiPath = ApiConfig.BASE_URL + ApiConfig.logout
        var authParams: Dictionary<String, Any> = Dictionary()
        authParams["device_token"] = AppConfig.shared.deviceToken
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": authParams], method: .post) { (resultData) in
            if resultData != nil {
                print("OD - logout -: Success")
                if let data = Mapper<GeneralSuccessResult>().map(JSON: resultData!){
                    print("data->logout->", data)
                    completion(true, data as AnyObject)
                }else{
                    completion(false, nil)
                }
            }
        }
    }
    
    func updateUser(user:User, file: String?, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.updateUser
        var params: Dictionary<String, Any> = Dictionary()
        params["bio"]               = user.bio
        params["dob"]               = user.dob
        params["phone"]             = user.phone
        params["email"]             = user.email
        params["gender"]            = user.gender
        params["last_name"]         = user.last_name
        params["first_name"]        = user.first_name
//        params["pre_name"]          = user.pre_name
        params["address"]           = user.address
        if let file = file {
            params["file"]          = file
        }
//        updateParams["link_list"]         = profileUpdateData.webLinks
        NetworkHelper.shared.executeUserRequest(path: path, params: ["user": params], method: .put) { (resultData) in
            if resultData != nil{
                print("OD - updateUser -: Success")
                let data = Mapper<LoginSuccessResponse>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func updateUserProfile(file: String, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.updateUser
        var params: Dictionary<String, Any> = Dictionary()
        params["file"] = file
        NetworkHelper.shared.executeUserRequest(path: path, params: ["user": params], method: .put) { (resultData) in
            if resultData != nil{
                print("OD - setOnlineStatus -: Success")
                let data = Mapper<LoginSuccessResponse>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func resetPassword(email:String, completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.forgotPasswordRoute
        var resetParams: Dictionary<String,Any> = Dictionary()
        resetParams["email"] = email
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": resetParams], method: .post, requiredToken: false) { (jsonResponse) in
            if jsonResponse != nil{
                print("OD -ResetPassword- Success")
                let data = Mapper<ResetPasswordSuccessResponse>().map(JSON: jsonResponse!)
                completion(true,data as AnyObject)
            }else{
                completion(false,nil)
            }
        }
    }
    
    func myProfile(isLoader: Bool = true, completion: @escaping rrequestsResponse){
        let apiPath = ApiConfig.BASE_URL + ApiConfig.me
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: nil, method: .get, isShowLoader: isLoader) { (resultData) in
            if resultData != nil{
                print("OD - myProfile GET -: Success")
                let data = Mapper<LoginSuccessResponse>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }else{
               completion(false, nil)
            }
        }
    }
    
    func deleteMyProfile(isDelete:Bool, password: String, completion: @escaping rrequestsResponse){
        var path = ApiConfig.BASE_URL + ApiConfig.deleteProfileRoute
        if !isDelete {
            path = ApiConfig.BASE_URL + ApiConfig.deactivateProfile
        }
        var params: Dictionary<String, Any> = Dictionary()
        params["password"] = password
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["user": params], method: .post) { (responseResult) in
            if responseResult != nil{
                print("OD -: Delete/Disable :- Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: responseResult!)
                completion(true,data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getEmails(completion: @escaping rrequestsResponse){
        let path = ApiConfig.BASE_URL + ApiConfig.userEmails
        
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: false) { (resultData) in
            if resultData != nil{
                print("OD - getEmails -: Success")
                let data = Mapper<EmailResponseModel>().map(JSONObject: resultData!)
                completion(true, data as AnyObject)
            }else{
               completion(false, nil)
            }
        }
    }
    
    func addSecondryEmail(email: String, completion: @escaping rrequestsResponse){
        let path = ApiConfig.BASE_URL + ApiConfig.userEmails
        var params: Dictionary<String,Any> = Dictionary()
        params["email"] = email
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["user_emails": params], method: .post) { (resultData) in
            if resultData != nil{
                print("OD - addSecondryEmail -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSONObject: resultData!)
                completion(true, data as AnyObject)
            }else{
               completion(false, nil)
            }
        }
    }
    
    func makePrimaryEmail(email: String, completion: @escaping rrequestsResponse){
        let path = ApiConfig.BASE_URL + ApiConfig.switchPrimaryEmail
        var params: Dictionary<String,Any> = Dictionary()
        params["email"] = email
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["user_emails": params], method: .post) { (resultData) in
            if resultData != nil{
                print("OD - makePrimaryEmail -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSONObject: resultData!)
                completion(true, data as AnyObject)
            }else{
               completion(false, nil)
            }
        }
    }
    
    func deleteSecondryEmail(id: Int, completion: @escaping rrequestsResponse){
        let path = ApiConfig.BASE_URL + ApiConfig.userEmails + "/\(id)"
        
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .delete) { (resultData) in
            if resultData != nil{
                print("OD - deleteSecondryEmail -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSONObject: resultData!)
                completion(true, data as AnyObject)
            }else{
               completion(false, nil)
            }
        }
    }
    
    func webLinks(link: String = "", isLoader: Bool = true, completion: @escaping rrequestsResponse){
        let apiPath = ApiConfig.BASE_URL + ApiConfig.weblinks
        var method = HTTPMethod.get
        var params: Dictionary<String, Any> = Dictionary()
        if !link.isEmpty {
            method = HTTPMethod.post
            params["link"] = link
            params = ["user_web_links": params]
        }
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: params, method: method, isShowLoader: isLoader) { (resultData) in
            if resultData != nil{
                print("OD - webLinks GET/POST -: Success")
                if method == .get {
                    let data = Mapper<WebLinkResponseModel>().map(JSONObject: resultData!)
                    completion(true, data as AnyObject)
                }
                else {
                    let data = Mapper<GeneralSuccessResult>().map(JSON: resultData!)
                    completion(true, data as AnyObject)
                }
            }else{
               completion(false, nil)
            }
        }
    }
    
    func deleteWebLinks(id: Int, isLoader: Bool = true, completion: @escaping rrequestsResponse){
        let apiPath = ApiConfig.BASE_URL + ApiConfig.weblinks + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: nil, method: .delete, isShowLoader: isLoader) { (resultData) in
            if resultData != nil{
                print("OD - deleteWebLinks -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }else{
               completion(false, nil)
            }
        }
    }
    
    func updateInactivityTime(timeValue: Int, completion: @escaping rrequestsResponse){
        let urlPath = ApiConfig.BASE_URL + ApiConfig.inactivityTimeRoute
        var params: Dictionary<String, Any> = Dictionary()
        params["inactivity_limit"] = timeValue
        
        NetworkHelper.shared.executeUserRequest(path: urlPath, params: ["user": params], method: .put) { (responseResult) in
            if responseResult != nil{
                print("OD - updateInactivityTime -: Success")
                let data = Mapper<LoginSuccessResponse>().map(JSON: responseResult!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func updatePassword(passwordCreds: ResetPassword, completion: @escaping rrequestsResponse) {
        
        let urlPath = ApiConfig.BASE_URL + ApiConfig.changePassword
        var params: Dictionary<String, Any> = Dictionary()
        params["old_password"] = passwordCreds.oldPassword
        params["new_password"] = passwordCreds.newPassword
        params["new_password_confirmation"] = passwordCreds.newConfirmPassword
        
        NetworkHelper.shared.executeUserRequest(path: urlPath, params: ["user": params], method: .post) { (responseResult) in
            if responseResult != nil{
                print("OD - updatePassword -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: responseResult!)
                completion(true,data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func createGroup(params: NewGroupModel, completion:@escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.groupRoute
        
        var _params: Dictionary<String, Any> = Dictionary()
        _params["name"]                              = params.name
        _params["description"]                       = params.description
        _params["is_public"]                         = params.is_public
        _params["can_send_invite"]                   = params.can_send_invite
        _params["can_approve"]                       = params.can_approve
        
        var categoryDicArray = Array<Dictionary<String, Any>>()
        for item in params.clinic_categories_attributes {
            var dic = Dictionary<String, Any>()
            dic["name"] = item.name
            dic["sort_order"] = item.sort_order
            categoryDicArray.append(dic)
        }
        _params["clinic_categories_attributes"]      = categoryDicArray
        
        if let file = params.file {
            _params["file"] = file
        }
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["clinic": _params], method: .post) { (result) in
            if result != nil{
                print("OD - createGroup -: Success")
                let data = Mapper<CreateGroupReaponse>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    //deprecated
    func groupList(page: Int, searchText: String = "", completion: @escaping rrequestsResponse) {
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = pageLimit10
        
        var path = ApiConfig.BASE_URL + ApiConfig.groupsListRouteDeprecated + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: false) { (result) in
            if result != nil{
                print("OD - Get Groups List -: Success")
                let data = Mapper<GroupsResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getGroupList(page: Int, searchText: String = "", completion: @escaping rrequestsResponse) {
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = pageLimit10
        
        var path = ApiConfig.BASE_URL + ApiConfig.groupsListRoute + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: false) { (result) in
            if result != nil{
                print("OD - Get Groups List -: Success")
                let data = Mapper<GroupListResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func groupInfo(by id : Int, showLoader : Bool = true, completion: @escaping rrequestsResponse){
        let url = ApiConfig.BASE_URL + ApiConfig.groupRoute + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .get, isShowLoader: showLoader) { (result) in
            if result != nil{
                print("OD - groupInfo-: Success")
                let data = Mapper<GroupInfoResponse>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func updateGroupStatus(request: MembershipRequest, completion: @escaping rrequestsResponse){
        let url = ApiConfig.BASE_URL + ApiConfig.updateGroupStatus
        var params : Dictionary<String, Any> = Dictionary()
        params["clinic_id"] = request.clinic_id
        params["user_id"] = request.user_id
        if let role = request.role {
            params["role"] = role.rawValue
        }
        else {
            params["request_status"] = request.request_status
        }
        
        NetworkHelper.shared.executeUserRequest(path: url, params: ["clinic_users": params], method: .put) { (result) in
            if result != nil{
                print("OD - updateGroupStatus -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func joinGroup(id: Int, completion: @escaping rrequestsResponse){
        let url = ApiConfig.BASE_URL + ApiConfig.joinGroup
        var params : Dictionary<String, Any> = Dictionary()
        params["clinic_id"] = id
        NetworkHelper.shared.executeUserRequest(path: url, params: ["clinic_users": params], method: .post) { (result) in
            if result != nil{
                print("OD - Group Membership -: Success")
                let data = Mapper<ResetPasswordSuccessResponse>().map(JSON: result!)
                completion(true,data as AnyObject)
            }else{
                completion(false,nil)
            }
        }
    }
    
    func userGroupsWithCategpries(completion: @escaping rrequestsResponse){
        let url = ApiConfig.BASE_URL + ApiConfig.userGroupsWithCategpries
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .get) { (result) in
            if result != nil{
                print("OD - Groups and Respect Categores List -: Success")
                let data = Mapper<GroupsWithCategoriesResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func createOrEditThread(_ params: ThreadParamsModel, isEdit: Bool = false, completion: @escaping rrequestsResponse){
        var url = ApiConfig.BASE_URL + ApiConfig.docs
        if isEdit {
            url = url + "/\(params.threadId!)"
        }
        
        var _params: Dictionary<String, Any> = Dictionary()
        _params["title"] = params.title
        _params["description"] = params.description
        _params["clinic_id"] = params.clinic_id
        _params["clinic_category_id"] = params.clinic_category_id
        _params["doc_type"] = params.doc_type
        _params["priority"] = params.priority
        _params["posted_from"] = params.posted_from
        if !params.files.isEmpty {
            _params["files"] = params.files
        }
        
        NetworkHelper.shared.executeUserRequest(path: url, params: ["doc": _params], method: isEdit ? .put : .post) { (result) in
            if result != nil{
                print("OD - createOrEditThread -: Success")
                let data = Mapper<ThreadModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func callNotificationAPI(groupID: Int, notification: Bool, docId: Int? = nil, completion: @escaping rrequestsResponse) {
        var path = ApiConfig.BASE_URL + ApiConfig.docNotification
        var method = HTTPMethod.put
        var params: Dictionary<String, Any> = Dictionary()
        params["clinic_id"] = groupID
        if let threadId = docId {
            params["doc_id"] = threadId
            params["notification"] = notification
            
            method = .post
        }
        else {
            if notification {
                path = path + "/group_notification_status_on"
            } else {
                path = path + "/group_notification_status_off"
            }
        }
        
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["doc_notification": params], method: method) { (resultData) in
            if resultData != nil{
                if let data = Mapper<GeneralSuccessResult>().map(JSON: resultData!), data.message != nil {
                    print("OD - callNotificationAPI -: Success")
                    completion(true, data as AnyObject)
                }else{
                    completion(false, nil)
                }
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getGroupUsers(groupID: Int, page: Int, searchText: String = "", completion: @escaping rrequestsResponse) {
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = pageLimit10
        
        var path = ApiConfig.BASE_URL + ApiConfig.groupRoute + "/\(groupID)/clinic_users" + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (resultData) in
            if resultData != nil{
                if let data = Mapper<GroupMemberModel>().map(JSON: resultData!) {
                    print("OD - getGroupUsers -: Success")
                    completion(true, data as AnyObject)
                }else{
                    completion(false, nil)
                }
            }else{
                completion(false, nil)
            }
        }
    }
    
    func leaveGroup(clinicId:Int, completion: @escaping rrequestsResponse) {
        let  url = ApiConfig.BASE_URL + ApiConfig.leaveGroup
        
        var params : Dictionary<String, Any> = Dictionary()
        params["clinic_id"] = clinicId
        
        NetworkHelper.shared.executeUserRequest(path: url, params: ["clinic": params], method: .post) { (result) in
            if result != nil{
                print("OD - leaveGroup -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func removeGroupMember(params: DeleteRequest, completion: @escaping rrequestsResponse) {
        let  url = ApiConfig.BASE_URL + ApiConfig.removeGroupMember
        
        var _params : Dictionary<String, Any> = Dictionary()
        _params["record"] = params.record
        _params["clinic_id"] = params.clinic_id
        _params["user_id"] = params.user_id
        
        NetworkHelper.shared.executeUserRequest(path: url, params: ["clinic_user_remove": _params], method: .post) { (result) in
            if result != nil{
                print("OD - Delete Member -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true,data as AnyObject)
            }else{
                completion(false,nil)
            }
        }
    }
    
    func deleteGroup(id: Int, completion: @escaping rrequestsResponse) {
        let url = ApiConfig.BASE_URL + ApiConfig.groupRoute + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .delete) { (result) in
            if result != nil{
                print("OD - deleteGroup -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func updateGroup(id: Int, params: NewGroupModel, completion:@escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.groupRoute + "/\(id)"
        
        var _params: Dictionary<String, Any> = Dictionary()
        _params["name"]                              = params.name
        _params["description"]                       = params.description
        _params["is_public"]                         = params.is_public
//        _params["can_send_invite"]                   = params.can_send_invite
        _params["can_approve"]                       = params.can_approve
        
        var categoryDicArray = Array<Dictionary<String, Any>>()
        for item in params.clinic_categories_attributes {
            var dic = Dictionary<String, Any>()
            dic["name"] = item.name
            dic["sort_order"] = item.sort_order
            if let id = item.id {
                dic["id"] = id
            }
            if let available = item.is_available {
                dic["is_available"] = available
            }
            categoryDicArray.append(dic)
        }
        _params["clinic_categories_attributes"]      = categoryDicArray
        
        if let file = params.file {
            _params["file"] = file
        }
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["clinic": _params], method: .put) { (result) in
            if result != nil{
                print("OD - updateGroup -: Success")
                let data = Mapper<CreateGroupReaponse>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func addCategory(clinicId: Int, categoryName: String, sortOrder: Int, completion:@escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.clinic_categories
        var _params : Dictionary<String, Any> = Dictionary()
        _params["clinic_id"] = clinicId
        _params["name"] = categoryName
        _params["sort_order"] = sortOrder
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["clinic_category": _params], method: .post) { (result) in
            if result != nil{
                print("OD - addCategory -: Success")
                let data = Mapper<GroupCategoryBaseModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func deleteCategory(id: Int, completion:@escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.clinic_categories + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .delete) { (result) in
            if result != nil{
                print("OD - deleteCategory -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func becomeAdmin(clinicId:Int, completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.becomeAdmin
        
        var params : Dictionary<String,Any> = Dictionary()
        params["clinic_id"] = clinicId
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["clinic": params], method: .post) { (resultData) in
            if resultData != nil{
                print("OD - becomeAdmin -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }
            else{
                completion(false, nil)
            }
        }
    }
    
    func getVisitedGroups(completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.getVisitedGroups
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get) { (result) in
            if result != nil{
                print("OD - getVisitedGroups -: Success")
                let data = Mapper<GroupListModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getGroups(page: Int, searchText: String = "", completion: @escaping rrequestsResponse) {
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = pageLimit10
        
        var path = ApiConfig.BASE_URL + ApiConfig.getAllClinics + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - getVisitedGroups -: Success")
                let data = Mapper<ClinicListModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func otherUserProfile(id: Int, isLoader: Bool = true, completion: @escaping rrequestsResponse){
        let apiPath = ApiConfig.BASE_URL + ApiConfig.registerRoute + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: nil, method: .get, isShowLoader: isLoader) { (resultData) in
            if resultData != nil{
                print("OD - otherUserProfile -: Success")
                let data = Mapper<MemberProfileResult>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }else{
               completion(false, nil)
            }
        }
    }
    
    func friendRequest(params: FriendRequestModel, completion: @escaping rrequestsResponse ){
        var url = ApiConfig.BASE_URL + ApiConfig.friendsRequest
        switch params.request_status {
        case .ACCEPT, .CANCEL:
            url = url + "/\(ApiConfig.updateFriendsRequest)"
        default:
            break
        }
        
        var _params: Dictionary<String, Any> = Dictionary()
        _params["receiver_id"] = params.receiver_id
        _params["request_status"] = params.request_status.rawValue
        
        NetworkHelper.shared.executeUserRequest(path: url, params: ["friend": _params], method: .post) { (result) in
            if result != nil{
                print("OD - friendRequest -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getSystemMembers(page: Int, searchText: String = "", completion: @escaping rrequestsResponse) {
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = pageLimit20
        
        var path = ApiConfig.BASE_URL + ApiConfig.systemUsersList + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - systemMembers -: Success")
                let data = Mapper<UserListModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func inviteToGroups(params: GroupsInvitation, completion: @escaping rrequestsResponse){
        let url = ApiConfig.BASE_URL + ApiConfig.invitationRoute
        
        var _params: Dictionary< String, Any> = Dictionary()
        _params["users"] = params.users
        _params["clinic_ids"] = params.clinic_ids
        if params.message != nil {
            _params["message"] = params.message
        }
        
        NetworkHelper.shared.executeUserRequest(path: url, params: _params, method: .post) { (result) in
            if result != nil{
                print("OD - inviteToGroups -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func toggleDisplayTimeline(groupID: Int, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.toggleDisplayTimeline
        var params: Dictionary<String, Any> = Dictionary()
        params["clinic_id"] = groupID
        
        NetworkHelper.shared.executeUserRequest(path: path, params: params, method: .put) { (result) in
            if result != nil{
                print("OD - toggleDisplayTimeline -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getfriendsWithOtherUsers(page: Int, searchText: String = "", completion: @escaping rrequestsResponse){
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = pageLimit20
        
        var path = ApiConfig.BASE_URL + "users/" + ApiConfig.friendsRequest + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - getfriendsWithOtherUsers -: Success")
                let data = Mapper<FriendListResponse>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func directUpload(_params: DirectUploadParams, data: Data, isShowLoader: Bool = true, completion: @escaping rrequestsResponse){
        let path = ApiConfig.BASE_URL + ApiConfig.directUpload
        
        var params: Dictionary<String, Any> = Dictionary()
        params["filename"] = _params.filename
        params["content_type"] = _params.content_type
        params["byte_size"] = _params.byte_size
        params["checksum"] = Checksum.hash(data: data, using: .sha256)
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["blob": params], method: .post, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                if let data = Mapper<DirectUploadModel>().map(JSON: result!) {
                    print("OD - directUpload -: Success")
                    completion(true, data as AnyObject)
                }else{
                    print("OD - directUpload -: Failure")
                    completion(false, nil)
                }
            }else{
                completion(false, nil)
            }
        }
    }
    
    func directUploadImage(data: Data, params: DirectUploadModel, progressBlock: ((_ percent: Double) -> Void)? = nil, completion: @escaping rrequestsResponse){
//        let _url = URL(string: params.direct_upload!.url!)!
//        let queryItems = URLComponents(
//            url: _url,
//            resolvingAgainstBaseURL: false
//        )?.queryItems
//        let version = queryItems?.filter({$0.name == "sv"}).first
        let headers: HTTPHeaders = [
            "Content-Type": params.direct_upload?.headers?.contentType ?? "application/octet-stream",
            "x-ms-blob-type": params.direct_upload?.headers?.x_ms_blob_type ?? "BlockBlob",
            "x-ms-blob-content-type": params.direct_upload?.headers?.contentType ?? "application/octet-stream",
            "x-ms-version": "2021-02-12",
            "X-File-Name": params.key!
        ]
        //"x-ms-version": version?.value ?? "2018-11-09",
//        print("directUploadImage->headers", headers)

        NetworkHelper.shared.uploadWith(url: params.direct_upload!.url!, imageData: data, headers: headers) { percent in
            print("Upload Progress: -> \(percent)")
            progressBlock?(percent)
        } completion: { (status) in
            if status {
                print("OD - directUploadImage -: Success")
                completion(true, nil)
            }
        }
    }
    
    func timeline(params: TimelineParams, completion: @escaping rrequestsResponse) {
        var _params: Dictionary<String, Any> = Dictionary()
        _params["clinic_ids"]       = params.clinic_ids
        _params["category_ids"]     = params.category_ids
        _params["page"]             = params.page
        _params["limit"]            = pageLimit5
        
        var isShowLoader = params.page == 1 ? true : false
        if params.home_enabled ?? false {
            _params["home_enabled"] = true
        }
        else {
            isShowLoader = false
        }
        
        let path = ApiConfig.BASE_URL + ApiConfig.timelineRoute
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["clinic_filter": _params], method: .post, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                if let data = Mapper<TimeineResult>().map(JSON: result!) {
                    print("OD - timeline -: Success")
                    completion(true, data as AnyObject)
                }else{
                    print("OD - timeline -: Failure")
                    completion(false, nil)
                }
            }else{
                completion(false, nil)
            }
        }
    }
    
    func searchTimeline(params: TimelineParams, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.searchTimeline
        var _params: Dictionary<String, Any> = Dictionary()
        if let search = params.search, !search.isEmpty {
            _params["search"]       = search
        }
        _params["clinic_ids"]       = params.clinic_ids
        _params["category_ids"]     = params.category_ids
        _params["page"]             = params.page
        _params["limit"]            = params.limit
        if params.type != SearchFilterTypes.NONE.rawValue {
            _params["type"]         = params.type
        }
        
        if params.home_enabled ?? false {
            _params["home_enabled"] = true
        }
        
        let isShowLoader = params.page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: _params, method: .post, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                if let data = Mapper<SearchTimeineResult>().map(JSON: result!) {
                    print("OD - searchTimeline -: Success")
                    completion(true, data as AnyObject)
                }else{
                    print("OD - searchTimeline -: Failure")
                    completion(false, nil)
                }
            }else{
                completion(false, nil)
            }
        }
    }
    
    func searchCount(search: String, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.searchCount
        let params: [String: Any] = [
            "search": search
        ]
            
        NetworkHelper.shared.executeUserRequest(path: path, params: params, method: .post, isShowLoader: false) { (result) in
            if result != nil{
                if let data = Mapper<SearchCount>().map(JSON: result!) {
                    print("OD - searchCount -: Success")
                    completion(true, data as AnyObject)
                }else{
                    print("OD - searchCount -: Failure")
                    completion(false, nil)
                }
            }else{
                completion(false, nil)
            }
        }
    }
    
    func deleteThread(id: Int, completion: @escaping rrequestsResponse) {
        let url = ApiConfig.BASE_URL + ApiConfig.docs + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .delete) { (result) in
            if result != nil{
                print("OD - deleteThread -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getThread(id: Int, completion: @escaping rrequestsResponse) {
        let url = ApiConfig.BASE_URL + ApiConfig.docs + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .get) { (result) in
            if result != nil{
                print("OD - getThread -: Success")
                let data = Mapper<ThreadModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getComments(threadId: Int, page: Int, completion: @escaping rrequestsResponse){
        var params: Dictionary<String, Any> = Dictionary()
        params["page"] = page
        params["limit"] = pageLimit10
        
        var path = ApiConfig.BASE_URL + ApiConfig.docComments + "/\(threadId)?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
//        let isShowLoader = page == 0 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: false) { (result) in
            if result != nil{
                print("OD - getComments -: Success")
                let data = Mapper<ThreadCommentsModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func sendComment(_params: PostCommentParams, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.docComments
        
        var params: Dictionary<String, Any> = Dictionary()
        params["clinic_id"] = _params.clinic_id
        params["doc_id"] = _params.doc_id
        params["comment"] = _params.comment
        params["comment_type"] = _params.comment_type
        if let files = _params.files {
            params["files"] = files
        }
        
        let isShowLoader = _params.comment_type == .file
        NetworkHelper.shared.executeUserRequest(path: path, params: ["doc_comment": params], method: .post, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - sendComment -: Success")
                let data = Mapper<ThreadCommentResponse>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func deleteComment(id: Int, completion: @escaping rrequestsResponse) {
        let url = ApiConfig.BASE_URL + ApiConfig.docComments + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .delete) { (result) in
            if result != nil{
                print("OD - deleteComment -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func updateComment(_params: PostCommentParams, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.docComments + "/\(_params.commentId!)"
        
        var params: Dictionary<String, Any> = Dictionary()
        if _params.comment_type == .comment {
            params["comment"] = _params.comment
        }
        else {
            params["files"] = _params.files
        }
        
        let isShowLoader = _params.comment_type == .file
        NetworkHelper.shared.executeUserRequest(path: path, params: ["doc_comment": params], method: .put, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - updateComment -: Success")
                let data = Mapper<ThreadCommentResponse>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func sendGroupEmail(with _params: GroupEmailParams, completion: @escaping rrequestsResponse) {
        let url = ApiConfig.BASE_URL + ApiConfig.groupEmailRoute
        var params: Dictionary< String, Any> = Dictionary()
        params["clinic_id"] = _params.clinic_id
        params["title"] = _params.title
        params["body"] = _params.body
        
        NetworkHelper.shared.executeUserRequest(path: url, params: ["clinic": params], method: .post) { (result) in
            if result != nil{
                print("OD - sendGroupEmail -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func loadCategoryDocs(catId: Int, page: Int, completion: @escaping rrequestsResponse) {
        var _params: Dictionary<String, Any> = Dictionary()
        _params["category_id"]      = catId
        _params["page"]             = page
        _params["limit"]            = pageLimit5
        
        let path = ApiConfig.BASE_URL + ApiConfig.categoryDoc
        
//        let isShowLoader = page == 0 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: ["clinic_category_docs": _params], method: .post) { (result) in
            if result != nil{
                if let data = Mapper<ClinicDocModel>().map(JSON: result!) {
                    print("OD - loadCategoryDocs -: Success")
                    completion(true, data as AnyObject)
                }else{
                    print("OD - loadCategoryDocs -: Failure")
                    completion(false, nil)
                }
            }else{
                completion(false, nil)
            }
        }
    }
    
    func deleteFile(signedId: String, isChatMessageFile: Bool = false, completion: @escaping rrequestsResponse){
        var url = ApiConfig.BASE_URL + ApiConfig.deleteFileRoute + "/\(signedId)"
        if isChatMessageFile {
            url = ApiConfig.BASE_URL + ApiConfig.deleteFileMessageRoute + "/\(signedId)"
        }
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .delete) { (result) in
            if result != nil{
                print("OD - deleteFile -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getThreadNotifications(page: Int, searchText: String = "", completion: @escaping rrequestsResponse){
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = pageLimit50
        
        var path = ApiConfig.BASE_URL + ApiConfig.docNotification + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - getThreadNotifications -: Success")
                let data = Mapper<ThreadNotificationResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getCommentFiles(threadId: Int, page: Int, completion: @escaping rrequestsResponse){
        var params: Dictionary<String, Any> = Dictionary()
        params["page"] = page
        params["limit"] = pageLimit10
        
        var path = ApiConfig.BASE_URL + ApiConfig.docCommentFiles + "/\(threadId)?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
//        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: false) { (result) in
            if result != nil{
                print("OD - getCommentFiles -: Success")
                let data = Mapper<CommentFileListModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getChatRooms(page: Int, searchText: String = "", completion: @escaping rrequestsResponse){
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = searchText.isEmpty ? pageLimit50 : 1000
        
        var path = ApiConfig.BASE_URL + ApiConfig.chatRoomRoute + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!.replacingOccurrences(of: "+", with: "%2B")
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - getChatRooms -: Success")
                let data = Mapper<ChatRoomResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getContacts(page: Int, completion: @escaping rrequestsResponse){
        var params: Dictionary<String, Any> = Dictionary()
        params["page"] = page
        params["limit"] = pageLimit20
        
        var path = ApiConfig.BASE_URL + ApiConfig.getClinicContacts + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - searchNewChatUsers -: Success")
                let data = Mapper<ContactListResponse>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func importContacts(contacts: [ContactModel], completion: @escaping rrequestsResponse){
        let path = ApiConfig.BASE_URL + ApiConfig.clinicContactRoute
        let params = contacts.toJSON()
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["data": params], method: .post) { (result) in
            if result != nil{
                print("OD - importContacts -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func searchNewChatUsers(page: Int, searchText: String = "", completion: @escaping rrequestsResponse){
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = pageLimit20
        
        var path = ApiConfig.BASE_URL + ApiConfig.searchClinicContactRoute + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - searchNewChatUsers -: Success")
                let data = Mapper<NewChatSearchResponse>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getGroupRequests(completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.groupRequests
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: false) { (result) in
            if result != nil{
                print("OD - getGroupRequests -: Success")
                let data = Mapper<GroupMemberModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func deleteChatRoom(id: Int, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.deleteChatRoute + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .delete) { (result) in
            if result != nil{
                print("OD - deleteChatRoom -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getChat(chatId: Int, page: Int, completion: @escaping rrequestsResponse){
        var params: Dictionary<String, Any> = Dictionary()
        params["page"] = page
        params["limit"] = pageLimit10
        
        var path = ApiConfig.BASE_URL + ApiConfig.chatMessagesRoute + "/\(chatId)?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - getChat -: Success")
                let data = Mapper<ChatMessagesResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getChatRoom(chatId: Int, completion: @escaping rrequestsResponse) {
        var path = ApiConfig.BASE_URL + ApiConfig.chatRoomRoute + "/show/\(chatId)"
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get) { (result) in
            if result != nil{
                print("OD - getChatRoom -: Success")
                let data = Mapper<ChatRoomModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func checkChatRoom(by virtualUser: String, completion: @escaping rrequestsResponse) {
        var _params: Dictionary<String, Any> = Dictionary()
        _params["virtual_user"] = virtualUser
        
        let path = ApiConfig.BASE_URL + ApiConfig.checkChatRoomByVirtual
        
        NetworkHelper.shared.executeUserRequest(path: path, params: ["user": _params], method: .post) { (result) in
            if let result = result {//CheckChatRoomResult
                let data = Mapper<CheckChatRoomResult>().map(JSON: result)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func sendMessage(_params: PostMessageParams, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.chatMessagesRoute
        
        var params: Dictionary<String, Any> = Dictionary()
        params["receiver_id"] = _params.receiver_id
        params["message"] = _params.message
        params["message_type"] = _params.message_type.rawValue
        if let files = _params.files {
            params["files"] = files
        }
        
        let isShowLoader = _params.message_type == .file
        NetworkHelper.shared.executeUserRequest(path: path, params: ["message": params], method: .post, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - sendMessage -: Success")
                let data = Mapper<ChatMessageResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func deleteMessage(id: Int, completion: @escaping rrequestsResponse) {
        let url = ApiConfig.BASE_URL + ApiConfig.chatMessagesRoute + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .delete) { (result) in
            if result != nil{
                print("OD - deleteMessage -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func updateMessage(_params: PostMessageParams, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.chatMessagesRoute + "/\(_params.messageId!)"
        
        var params: Dictionary<String, Any> = Dictionary()
        if _params.message_type == .message {
            params["message"] = _params.message
        }
        else {
            params["files"] = _params.files
        }
        
        let isShowLoader = _params.message_type == .file
        NetworkHelper.shared.executeUserRequest(path: path, params: ["message": params], method: .put, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - updateMessage -: Success")
                let data = Mapper<ChatMessageResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getNotificationCount(completion: @escaping rrequestsResponse){
        let url = ApiConfig.BASE_URL + ApiConfig.docNotificationCount
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .get, isShowLoader: false) { (result) in
            if result != nil{
                print("OD - getNotificationCount -: Success")
                let data = Mapper<CountModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func updateDocNotificationCount(id: Int, completion: @escaping rrequestsResponse){
        let url = ApiConfig.BASE_URL + ApiConfig.updateDocNotification
        var params: Dictionary<String, Any> = Dictionary()
        params["doc_id"] = id
        NetworkHelper.shared.executeUserRequest(path: url, params: ["doc_notification": params], method: .put, isShowLoader: false) { (result) in
            if result != nil{
                print("OD - updateDocNotificationCount -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getUnreadChatCount(completion: @escaping rrequestsResponse){
        let url = ApiConfig.BASE_URL + ApiConfig.chatUnreadCount
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .get, isShowLoader: false) { (result) in
            if result != nil{
                print("OD - getUnreadChatCount -: Success")
                let data = Mapper<CountModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getMyThreads(page: Int, searchText: String = "", completion: @escaping rrequestsResponse){
        var params: Dictionary<String, Any> = Dictionary()
        if !searchText.isEmpty {
            params["search"] = searchText.trim
        }
        params["page"] = page
        params["limit"] = pageLimit20
        
        var path = ApiConfig.BASE_URL + ApiConfig.myDocs + "?" + params.queryString
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let isShowLoader = page == 1 ? true : false
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, isShowLoader: isShowLoader) { (result) in
            if result != nil{
                print("OD - getMyThread -: Success")
                let data = Mapper<ClinicDocModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func setOnlineStatus(_ status: OnlineMode, completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.updateUser
        var params: Dictionary<String, Any> = Dictionary()
        params["online_status"] = status.rawValue
        NetworkHelper.shared.executeUserRequest(path: path, params: ["user": params], method: .put) { (resultData) in
            if resultData != nil{
                print("OD - setOnlineStatus -: Success")
                let data = Mapper<LoginSuccessResponse>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getLoggedInUsersCount(data:[Int], completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.loggedInUsersCount
        
        var params : Dictionary<String,Any> = Dictionary()
        params["user_ids"] = data
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": params], method: .post, requiredToken : false) { (resultData) in
            if resultData != nil{
                print("OD - getLoggedInUsersCount -: Success")
                let data = Mapper<LoggedInUserCountResultModel>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }
        }
    }
    
    func resetPassword(_ params: NewPassword, completion: @escaping rrequestsResponse ){
        let apiPath = ApiConfig.BASE_URL + ApiConfig.resetPassword
        var  resetParams: Dictionary<String,Any> = Dictionary()
        resetParams["email"] = params.email
        resetParams["token"] = params.token
        resetParams["password"] = params.newPassword
        resetParams["confirm_password"] = params.confirmPassword
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": resetParams], method: .post, requiredToken: false) { (jsonResponse) in
            if jsonResponse != nil{
                print("OD -resetPassword- Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: jsonResponse!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   

    
//    func removeNoficationToken(completion: @escaping rrequestsResponse) {
//        let url = ApiConfig.BASE_URL + ApiConfig.removeNotificationToken
//        var params: Dictionary< String, Any> = Dictionary()
//
//        params["platform"] = "ios"
//
//        NetworkHelper.shared.executeUserRequest(path: url, params: params, method: .post,isShowLoader: false) { (responseResult) in
//            if responseResult != nil{
//                print("OD - Remove Device Token Status -: Success")
//                let data = Mapper<GeneralSuccessResponse>().map(JSON: responseResult!)
//                completion(true, data as AnyObject)
//            }else{
//                print("ERROR OD - Remove Device Token Status -: Success")
//                completion(false,nil)
//            }
//        }
//    }
    
    func resendConfirmationLink(email:String, completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.resendConfirmationRoute
        var params : Dictionary<String,Any> = Dictionary()
        params["email"] = email
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": params], method: .post, requiredToken: false) { (responseResult) in
            if responseResult != nil {
                print("OD - resendConfirmationLink -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: responseResult!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func reactivateUser(email:String, completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.reactivateUser
        var params : Dictionary<String, Any> = Dictionary()
        params["email"] = email
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": params], method: .post, requiredToken: false) { (responseResult) in
            if responseResult != nil{
                print("OD - reactivateUser -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: responseResult!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func createVirtualUser(by virtualUser: String, name: String, isEmail: Bool, do_not_send_message: Bool = false, completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.createVirtualUser
        var params : Dictionary<String, Any> = Dictionary()
        if !name.isEmpty {
            var nameArray = name.split(separator: " ")
            params["first_name"] = nameArray.first
            nameArray.removeFirst()
            params["last_name"] = nameArray.count > 0 ? nameArray.joined(separator: " ") : ""
        }
        params["virtual_user"] = virtualUser
        params["is_email"] = "\(isEmail)"
        params["do_not_send_message"] = "\(do_not_send_message)"
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": params], method: .post) { (responseResult) in
            if responseResult != nil{
                print("OD - createVirtualUser -: Success")
                let data = Mapper<InvitedChatRoomResult>().map(JSON: responseResult!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func codeVerification(email: String, code: String, completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.codeVerification
        var params : Dictionary<String,Any> = Dictionary()
        params["email"] = email
        params["code"] = code
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": params], method: .post, requiredToken: false) { (responseResult) in
            if responseResult != nil{
                if let data = Mapper<LoginSuccessResponse>().map(JSON: responseResult!), data.result != nil {
                    print("Success in Posting codeVerification to server ")
                    completion(true, data as AnyObject)
                }else{
                    print("Error in Posting codeVerification to server ")
                    completion(false, nil)
                }
            }else{
                print("Error in Posting codeVerification to server ")
                completion(false,nil)
            }
        }
    }

    func forceUpdate(completion: @escaping rrequestsResponse) {
        let path = ApiConfig.BASE_URL + ApiConfig.forceUpdate
        NetworkHelper.shared.executeUserRequest(path: path, params: nil, method: .get, requiredToken: false, isShowLoader: false) { (result) in
            if result != nil{
                print("OD - Force Update Call -: Success")
                let data = Mapper<ForceUpdateModelResult>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func userType(preName: String, completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.updateUser
        var params: Dictionary<String, Any> = Dictionary()
        params["pre_name"] = preName
        
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: ["user": params], method: .put) { (resultData) in
            if resultData != nil{
                print("OD - userType -: Success")
                let data = Mapper<LoginSuccessResponse>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func getSearchHistory(completion: @escaping rrequestsResponse) {
        let url = ApiConfig.BASE_URL + ApiConfig.searchHistoryRoute
        NetworkHelper.shared.executeUserRequest(path: url, params: nil, method: .get) { (result) in
            if result != nil{
                print("OD - getSearchHistory -: Success")
                let data = Mapper<SearchHistoryListModel>().map(JSON: result!)
                completion(true, data as AnyObject)
            }else{
                completion(false, nil)
            }
        }
    }
    
    func deleteSearchHistory(id: Int, completion: @escaping rrequestsResponse) {
        let apiPath = ApiConfig.BASE_URL + ApiConfig.searchHistoryRoute + "/\(id)"
        NetworkHelper.shared.executeUserRequest(path: apiPath, params: nil, method: .delete) { (resultData) in
            if resultData != nil{
                print("OD - deleteSearchHistory -: Success")
                let data = Mapper<GeneralSuccessResult>().map(JSON: resultData!)
                completion(true, data as AnyObject)
            }else{
               completion(false, nil)
            }
        }
    }
}

