//
//  SocketClient.swift
//  onlineDoc
//
//  Created by Piecyfer on 05/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit
import ActionCableClient
import ObjectMapper

enum SocketChannels: String {
    case APPEARANCE = "AppearanceChannel"
    case CHAT = "ChatChannel"
    case NOTIFICATION = "NotificationChannel"
    case CLINIC = "ClinicChannel"
}

enum SocketActions: String {
    case UPDATE = "update"
    case TYPING = "typing"
    case SEND = "send_message"
    case MESSAGE_READ = "message_read"
}

enum ResponseTypes: String {
    case STATUS = "online_status"
    case CHAT_ROOM = "chat_room"
    case THREAD_NOTIFICAION = "thread_notification"
    case TASK_NOTIFICATION = "task_notification"
    case GROUP_JOIN_NOTIFICATION = "group_join_notification"
    case READ_MESSAGE = "read_message"
    case TYPING = "typing"
    case READ_NOTIFICATION = "notification_read"
    case READ_FROM_OTHER_DEVICE = "read_from_other_device"
    case CLINIC_JOINED = "clinic_joined"
}


enum NotifySocket {
    case appearance
    case message
    case typing
    case readMessage
    case chatRoomNotification
    case threadNotification
    case taskNotification
    case groupRequestNotification
    case receiveComment
    case readNotification
    case readFromOtherDevice
    case clinicJoined
}


//protocol SocketManagerDelegate: class {
//    func userAppearance(data: SocketAppearanceModel)
//    func receiveMessage(data: SocketMessageModel)
//    func typing()
//    func chatRoomNotification(data: ChatRoomModel)
//    func receiveComment(data: SocketCommentBaseModel)
//    func threadNotification(data: SocketThreadNotificationBaseModel)
//    func readChatMessages(userId: Int)
//}

class SocketManager: NSObject {
    private static var _sharedInstance: SocketManager? = nil
//    static let sharedInstance = SocketManager()
    static var sharedInstance: SocketManager {
        if _sharedInstance == nil {
            _sharedInstance = SocketManager()
        }
        return _sharedInstance!
    }
    static func resetSharedInstance() {
        _sharedInstance = nil
    }
    
    // Variables of instance
    var client: ActionCableClient!
    var channelAppearance: Channel!
    var channelChat: Channel!
    var channelNotification: Channel!
    var channelClinic: Channel!
    
//    weak var delegate : SocketManagerDelegate?
    
    private override init() {
        let url = ApiConfig.SOCKET_URL + AppConfig.shared.authToken.dropFirst(7)
        print("SocketManager->url-> \(url)")
        
        self.client = ActionCableClient(url: URL(string: url)!)
    }
    
    func connectToSocket() -> () {
        if client?.isConnected == false {
            client.connect()
            
            client.onConnected = {
                print("SocketManager->onConnected")
                print("SocketManager->IsConnected-> \(self.client.isConnected)")
                if !(self.channelAppearance?.isSubscribed ?? false) {
                    self.subscribeChannel(channel: .APPEARANCE)
                }
                if !(self.channelNotification?.isSubscribed ?? false) {
                    self.subscribeChannel(channel: .NOTIFICATION)
                }
                if !(self.channelClinic?.isSubscribed ?? false) {
                    self.subscribeChannel(channel: .CLINIC)
                }
            }
            
            client.onPing = {
                print("SocketManager-> ping")
            }
            
            client.onDisconnected = { (reason) in
                print("SocketManager->onDisconnected-> \(reason ?? ConnectionError.none)")
            }
            
            client.onRejected = {
                print("SocketManager->onRejected")
            }
        }
    }
    
    func disconnectToSocket() {
        if channelAppearance != nil && channelAppearance.isSubscribed {
            channelAppearance.unsubscribe()
            channelAppearance = nil
        }
        if channelChat != nil && channelChat.isSubscribed {
            channelChat.unsubscribe()
            channelChat = nil
        }
        if channelNotification != nil && channelNotification.isSubscribed {
            channelNotification.unsubscribe()
            channelNotification = nil
        }
        if channelClinic != nil && channelClinic.isSubscribed {
            channelClinic.unsubscribe()
            channelClinic = nil
        }
        
        client?.disconnect()
        client = nil
        SocketManager.resetSharedInstance()
    }
    
    func subscribeChannel(channel: SocketChannels, params: [String: String]? = nil) {
        switch channel {
        case .APPEARANCE:
            channelAppearance = client.create(channel.rawValue)
            channelAppearance.onSubscribed = {
//                print("APPEARANCE addOnSubscribe!", channel.identifier)
                if self.channelAppearance.isSubscribed {
                    print("APPEARANCE Channel Subscribed Successfuly!")
                    let params: [String : Any] = ["online_status": OnlineMode.ONLINE.rawValue]
                    self.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
                }
            }
            channelAppearance.onReceive = { (result : Any?, error : Error?) in
                guard error == nil else {
                    print("APPEARANCE Channel Error onReceive is: \(error!.localizedDescription)")
                    return
                }
//                print("APPEARANCE addOnMessage!", result ?? "")
                if let result = result as? [String : Any] {
                    let dataResult = Mapper<SocketAppearanceModel>().map(JSON: result)
                    if let type = dataResult?.type {
                        if type == ResponseTypes.STATUS.rawValue {
                            if let data = dataResult?.data?.user {
                                if data.id != AppConfig.shared.user.id {
//                                    guard let delegate = self.delegate else {return}
//                                    delegate.userAppearance(data: dataResult!)
                                    
                                    NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": dataResult!, "Channel": SocketChannels.APPEARANCE, "Notify": NotifySocket.appearance])
                                }
                            }
                        }
                    }
                }
            }
            channelAppearance.onUnsubscribed = {
//                print("APPEARANCE addOnUnsubscribe", channel)
            }
            channelAppearance.onRejected = {
//                print("APPEARANCE addOnRejectSubscription", channel)
            }
        case .CHAT:
            channelChat = client.create(channel.rawValue, identifier: params, autoSubscribe: true, bufferActions: true)
            channelChat.onSubscribed = {
                print("CHAT Channel Subscribed Successfuly!")
            }
            channelChat.onReceive = { (result : Any?, error : Error?) in
                guard error == nil else {
                    print("CHAT Channel Error onReceive is: \(error!.localizedDescription)")
                    return
                }
                if let result = result as? [String : Any] {
                    let dataResult = Mapper<SocketMessageModel>().map(JSON: result)
                    guard let type = dataResult?.type else {return}
                    if let _ = dataResult?.data {
                        print("CHAT addOnMessage!", result)
//                            guard let delegate = self.delegate else {return}
//                            delegate.receiveMessage(data: dataResult!)
                        NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": dataResult!, "Channel": SocketChannels.CHAT, "Notify": NotifySocket.message])
                    }
                    else if type == ResponseTypes.TYPING.rawValue {
                        let dataResult = Mapper<SocketAppearanceModel>().map(JSON: result)
                        if let data = dataResult?.data?.user {
                            if data.id != AppConfig.shared.user.id {
//                                guard let delegate = self.delegate else {return}
//                                delegate.typing()
                                NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Channel": SocketChannels.CHAT, "Notify": NotifySocket.typing])
                            }
                        }
                    }
                    else if type == ResponseTypes.READ_MESSAGE.rawValue {
//                        guard let delegate = self.delegate else {return}
                        if let id = result["user_id"] as? Int {
//                            delegate.readChatMessages(userId: id)
                            NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": id, "Channel": SocketChannels.CHAT, "Notify": NotifySocket.readMessage])
                        }
                    }
                }
            }
            channelChat.onUnsubscribed = {
                print("CHAT addOnUnsubscribe -> \(channel)")
            }
            channelChat.onRejected = {
                print("CHAT addOnRejectSubscription -> \(channel)")
            }
        case .NOTIFICATION:
            channelNotification = client.create(channel.rawValue)
            channelNotification.onSubscribed = {
                print("NOTIFICATION Channel Subscribed Successfuly!")
            }
            channelNotification.onReceive = { (result : Any?, error : Error?) in
//                print("NOTIFICATION addOnMessage!", result ?? "")
                if let result = result as? [String : Any] {
                    let dataResult = Mapper<SocketChatNotificationModel>().map(JSON: result)
                    if let type = dataResult?.type {
                        if type == ResponseTypes.CHAT_ROOM.rawValue {
                            if let data = dataResult?.data {
//                                    guard let delegate = self.delegate else {return}
//                                    delegate.chatRoomNotification(data: data)
                                NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": data, "Channel": SocketChannels.NOTIFICATION, "Notify": NotifySocket.chatRoomNotification])
                            }
                        }
                        else if type == ResponseTypes.THREAD_NOTIFICAION.rawValue || type == ResponseTypes.TASK_NOTIFICATION.rawValue {
                            let dataResult = Mapper<SocketThreadNotificationBaseModel>().map(JSON: result)
                            if let _ = dataResult?.doc?.doc {
//                                guard let delegate = self.delegate else {return}
//                                delegate.threadNotification(data: dataResult!)
                                NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": dataResult!, "Channel": SocketChannels.NOTIFICATION, "Notify": type == ResponseTypes.THREAD_NOTIFICAION.rawValue ? NotifySocket.threadNotification : NotifySocket.taskNotification])
                            }
                        }
                        else if type == ResponseTypes.GROUP_JOIN_NOTIFICATION.rawValue {
                            let dataResult = Mapper<SocketGroupRequestNotificationBaseModel>().map(JSON: result)
                            if let _ = dataResult?.notification?.notification {
                                NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": dataResult!, "Channel": SocketChannels.NOTIFICATION, "Notify": NotifySocket.groupRequestNotification])
                            }
                        }
                        else if type == ResponseTypes.READ_NOTIFICATION.rawValue {
                            let dataResult = Mapper<SocketThreadNotificationBaseModel>().map(JSON: result)
                            if let _ = dataResult?.doc?.doc {
                                NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": dataResult!, "Channel": SocketChannels.NOTIFICATION, "Notify": NotifySocket.readNotification])
                            }
                        }
                        else if type == ResponseTypes.READ_FROM_OTHER_DEVICE.rawValue {
                            let dataResult = Mapper<SocketReadFromOtherDeviceModel>().map(JSON: result)
                            if let _ = dataResult?.chat_room_id {
                                NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": dataResult!, "Channel": SocketChannels.NOTIFICATION, "Notify": NotifySocket.readFromOtherDevice])
                            }
                        }
                        else if type == ResponseTypes.CLINIC_JOINED.rawValue {
                            let dataResult = Mapper<SocketClinicModel>().map(JSON: result)
                            if let _ = dataResult?.data?.id {
                                NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": dataResult!, "Channel": SocketChannels.NOTIFICATION, "Notify": NotifySocket.clinicJoined])
                            }
                        }
                    }
                }
            }
            channelNotification.onUnsubscribed = {
//                print("NOTIFICATION addOnUnsubscribe", channel)
            }
            channelNotification.onRejected = {
//                print("NOTIFICATION addOnRejectSubscription", channel)
            }
        case .CLINIC:
            channelClinic = client.create(channel.rawValue)
            channelClinic.onSubscribed = {
//                print("CLINIC addOnSubscribe!", channel.identifier)
            }
            channelClinic.onReceive = { (result : Any?, error : Error?) in
//                print("CLINIC addOnMessage!", result ?? "")
                if let result = result as? [String : Any] {
                    let dataResult = Mapper<SocketCommentBaseModel>().map(JSON: result)
                    if let _ = dataResult?.comment?.data {
//                        guard let delegate = self.delegate else {return}
//                        delegate.receiveComment(data: dataResult!)
                        NotificationCenter.default.post(name: .notifySocket, object: nil, userInfo: ["Data": dataResult!, "Channel": SocketChannels.CLINIC, "Notify": NotifySocket.receiveComment])
                    }
                }
            }
            channelClinic.onUnsubscribed = {
//                print("CLINIC addOnUnsubscribe", channel)
            }
            channelClinic.onRejected = {
//                print("CLINIC addOnRejectSubscription", channel)
            }
        }
    }
    
    func emit(channel: SocketChannels, action: SocketActions, params: [String: Any]) -> () {
        switch channel {
        case .APPEARANCE:
            if channelAppearance != nil {
                channelAppearance.action(action.rawValue, with: params)
            }
        case .CHAT, .NOTIFICATION, .CLINIC:
            break
        }
    }
}

//extension SocketManagerDelegate {
//    func userAppearance(data: SocketAppearanceModel) {}
//    func receiveMessage(data: SocketMessageModel) {}
//    func typing() {}
//    func chatRoomNotification(data: ChatRoomModel) {}
//    func receiveComment(data: SocketCommentBaseModel) {}
//    func threadNotification(data: SocketThreadNotificationBaseModel) {}
//    func readChatMessages(userId: Int) {}
//}


