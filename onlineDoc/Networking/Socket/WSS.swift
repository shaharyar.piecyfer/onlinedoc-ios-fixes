////
////  WSS.swift
////  onlineDoc
////
////  Created by Piecyfer on 16/08/2021.
////  Copyright © 2021 onlineDoc. All rights reserved.
////
//
//import Foundation
//import Starscream
//import ActionCableSwift
//
//class WSS: ACWebSocketProtocol, WebSocketDelegate {
//
//    var url: URL
//    var ws: WebSocket
//
//    init(stringURL: String) {
//        url = URL(string: stringURL)!
//        ws = WebSocket(request: URLRequest(url: url))
//        ws.delegate = self
//    }
//
//    var onConnected: ((_ headers: [String : String]?) -> Void)?
//    var onDisconnected: ((_ reason: String?) -> Void)?
//    var onCancelled: (() -> Void)?
//    var onText: ((_ text: String) -> Void)?
//    var onBinary: ((_ data: Data) -> Void)?
//    var onPing: (() -> Void)?
//    var onPong: (() -> Void)?
//
//    func connect(headers: [String : String]?) {
//        ws.request.allHTTPHeaderFields = headers
//        ws.connect()
//    }
//
//    func disconnect() {
//        ws.disconnect()
//    }
//
//    func send(data: Data) {
//        ws.write(data: data)
//    }
//
//    func send(data: Data, _ completion: (() -> Void)?) {
//        ws.write(data: data, completion: completion)
//    }
//
//    func send(text: String) {
//        ws.write(string: text)
//    }
//
//    func send(text: String, _ completion: (() -> Void)?) {
//        ws.write(string: text, completion: completion)
//    }
//
//    func didReceive(event: WebSocketEvent, client: WebSocket) {
//        switch event {
//        case .connected(let headers):
//            onConnected?(headers)
//        case .disconnected(let reason, let code):
//            onDisconnected?(reason)
//        case .text(let string):
//            onText?(string)
//        case .binary(let data):
//            onBinary?(data)
//        case .ping(_):
//            onPing?()
//        case .pong(_):
//            onPong?()
//        case .cancelled:
//            onCancelled?()
//        default: break
//        }
//    }
//}
