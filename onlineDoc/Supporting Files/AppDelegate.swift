//
//  AppDelegate.swift
//  onlineDoc
//
//  Created by Piecyfer  on 10/10/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseDatabase
import UserNotifications
import FirebaseMessaging
import ObjectMapper

//#if DEBUG
//    import CocoaDebug
//#endif

//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var backgroundTasks: UIBackgroundTaskIdentifier?
//    var notificationType: NotificationTypes?
    var notificationPayload: AnyObject?
    var firebaseChat = ""
    var senderName   = ""
    var chatTab = ""
    var attachements = [Attachment]()
    var isUniversalLinkClick: Bool = false
    var isPushNotificationClicked: Bool = false
//    var friendID = ""
    override init() {
        super.init()
        UIFont.overrideInitialize()
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //setup network connectivity
        self.setupNetworkConnectivity()
//        #if DEBUG
//            customCocoaDebug()
//        #endif
        //force reset login if needed
//        let appVersion = AppConfig.shared.appVersion()
//        if let appVersion = appVersion, appVersion == "2.0.0", AppConfig.shared.getResetUserLogin() == false {
//            AppConfig.shared.logout = ""
//        }
        
        AppConfig.shared.isDarkMode = false

//        if #available(iOS 11.0, *) {
//            UINavigationBar.appearance(whenContainedInInstancesOf: [UIDocumentBrowserViewController.self]).tintColor = .black
//        } else {
//            // Fallback on earlier versions
//        }
        
//        if #available (iOS 11.0, *) {
//            UINavigationBar.appearance(whenContainedInInstancesOf: [UIImagePickerController.self]).tintColor = .black
//        }
        // Reset Language Based on Current User Prefrences
//        LanguageManager.setupCurrentLanguage()
//        let pre = Locale.preferredLanguages[0]
//        print(pre)
//        application.statusBarStyle = .lightContent
//        UINavigationBar.appearance().barStyle = .blackOpaque
        IQKeyboardManager.shared.enable = true
//        IQKeyboardManager.shared.enableDebugging = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = DismissText.localized()
//        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Dismiss".localized()
        IQKeyboardManager.shared.toolbarDoneBarButtonItemAccessibilityLabel = DismissText.localized()
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
//        FirebaseConfiguration.shared.setLoggerLevel(.max)
        self.registerForPushNotifications()
//        var ref: DatabaseReference!
//        ref = Database.database().reference()

//        //MARK: - APP Level Controls Settings using Appearence
        
        //MARK: - TextFields
        
//        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
//        UISearchBar.appearance().setImage(UIImage(named: "searchMini"), for: UISearchBar.Icon.search, state: .normal)
        
        //MARK: - Tab Bar
//        UITabBar.appearance().badgeColor = .green
//        if #available(iOS 11.0, *) {
//            UINavigationBar.appearance(whenContainedInInstancesOf: [UIDocumentBrowserViewController.self]).tintColor = nil
//        }
//        UITabBar.appearance().barTintColor = AppColors.tabBarTint
//        UITabBar.appearance().tintColor = AppColors.choice
//        UITabBar.appearance().unselectedItemTintColor = UIColor.white
//        let sharedApplication = UIApplication.shared
//        sharedApplication.delegate?.window??.tintColor = .black
        
        //Auto Logout on Inactivity
        
        NotificationCenter.default.addObserver( self,
                                                selector: #selector(AppDelegate.applicationDidTimeout(notification:)),
                                                name: .appTimeout,
                                                object: nil
        )
//        AppConfig.shared.requestMessagesCount = 0
//        AppConfig.shared.friendMessagesCount = 0
        
        //MARK: -   AVOID FIELD HIDING
        print("OnLaunch : Controll is in didFinishLaunchingWithOptions")
        
        if (launchOptions?[UIApplication.LaunchOptionsKey.userActivityDictionary] != nil) {
            let activityDictionary = launchOptions?[UIApplication.LaunchOptionsKey.userActivityDictionary] as? [AnyHashable: Any] ?? [AnyHashable: Any]()
            let activity = activityDictionary["UIApplicationLaunchOptionsUserActivityKey"] as? NSUserActivity
            if activity != nil {
                isUniversalLinkClick = true
            }
        }
        if isUniversalLinkClick {
            // app opened via clicking a universal link.
            print("isUniversalLinkClick", isUniversalLinkClick)
        } else {
            // set the initial viewcontroller
            print("isUniversalLinkClick", isUniversalLinkClick)
        }
        return true
    }
    
    func setupNetworkConnectivity() {
        do {
            try Network.reachability = Reachability() //(hostname: "www.google.com")
        }
        catch {
            switch error as? Network.Error {
            case let .failedToCreateWith(hostname)?:
                print("Network error:\nFailed to create reachability object With host named:", hostname)
            case let .failedToInitializeWith(address)?:
                print("Network error:\nFailed to initialize reachability object With address:", address)
            case .failedToSetCallout?:
                print("Network error:\nFailed to set callout")
            case .failedToSetDispatchQueue?:
                print("Network error:\nFailed to set DispatchQueue")
            case .none:
                print("Network error:", error)
            }
        }
    }
    
//    func customCocoaDebug() {
//        //--- If want to manual enable App logs (Take effect the next time when app starts) ---
//        #if DEBUG
//            CocoaDebugSettings.shared.enableLogMonitoring = true
//        #endif
//    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        //TODO Do dep linking here
        print("Continue User Activity called: ")
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            let url = userActivity.webpageURL!
            print("userActivity", url.absoluteString)
            let urlToken = url.getQueryStringParameter("token")
            let urlEmail = url.getQueryStringParameter("email")
            
//            print(urlToken)
            if let token =  AppConfig.shared.authToken, token == ""{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let rootView = storyboard.instantiateViewController(withIdentifier: "MainNavView") as? UINavigationController
                if let initialViewController = storyboard.instantiateViewController(withIdentifier: "ResetPaswordScene") as? ResetPasswordVC{
                    if let uToken = urlToken{
                        initialViewController.queryToken = uToken
                        
                    }
                    if let uEmail = urlEmail{
                        initialViewController.email = uEmail
                    }
                    rootView?.setViewControllers([initialViewController], animated: true)
                }
                self.window?.rootViewController = rootView
                self.window?.makeKeyAndVisible()
            }
            //            let token = self.getQueryStringParameter(url: url.absoluteString, param: "token")
            //            let email = self.getQueryStringParameter(url: url.absoluteString, param: "email")
            //handle url and open whatever page you want to open.
        }
        return true
    }
    
    func cancelBgTasks() -> () {
        if self.backgroundTasks != nil{
            UIApplication.shared.endBackgroundTask(self.backgroundTasks!)
            self.backgroundTasks = UIBackgroundTaskIdentifier.invalid
            self.backgroundTasks = nil
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        self.cancelBgTasks()
        if let token = AppConfig.shared.authToken, token != ""{
//            RequestManager.shared.setOnlineStatus(.ONLINE) { _,_ in }
            let params: [String : Any] = ["online_status": OnlineMode.ONLINE.rawValue]
            SocketManager.sharedInstance.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
//            if SocketManager.sharedInstance.client.isConnected == true && (SocketManager.sharedInstance.channelChat?.isSubscribed ?? false) == true {
//                SocketManager.sharedInstance.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
//            }
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if let token = AppConfig.shared.authToken, token != ""{
            backgroundTasks = UIApplication.shared.beginBackgroundTask(withName: "com.piecyfer.onlinedoc.status.refresh") {
                // Cleanup code should be written here so that you won't see the loader
                self.cancelBgTasks()
            }
            let params: [String : Any] = ["online_status": OnlineMode.AWAY.rawValue]
            SocketManager.sharedInstance.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
//            if SocketManager.sharedInstance.client.isConnected == true && (SocketManager.sharedInstance.channelChat?.isSubscribed ?? false) == true {
//                SocketManager.sharedInstance.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
//            }
//            RequestManager.shared.setOnlineStatus(.AWAY) { (status, data) in
//                self.cancelBgTasks()
//            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if let token = AppConfig.shared.authToken, token != ""{
//            RequestManager.shared.setOnlineStatus(.OFFLINE) { _,_ in }
            let params: [String : Any] = ["online_status": OnlineMode.OFFLINE.rawValue]
            SocketManager.sharedInstance.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
        }
    }
    
    
    // MARK: UISceneSession Lifecycle
    /*
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        print()
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    */
    @objc func applicationDidTimeout(notification: NSNotification) {
        if let token =  AppConfig.shared.authToken, token != "" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                RequestManager.shared.logout() { (status, response) in
                    if status {
                        let params: [String : Any] = ["online_status": OnlineMode.OFFLINE.rawValue]
                        SocketManager.sharedInstance.emit(channel: .APPEARANCE, action: .UPDATE, params: params)
                        
                        AppConfig.shared.logout = ""
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainView = storyboard.instantiateViewController(withIdentifier: "MainNavView")
                        UIApplication.shared.windows.first?.rootViewController = mainView
                        baseVC?.showAlert(To: LogoutHeader.localized(), for: AutoLogoutText.localized()) {_ in}
                        print("Response AppDelegate: Timer Has Exceeded in timeHasExceeded")
                    }
                }
            }
        }else{
            print("Token is Empty")
        }
    }
}

extension AppDelegate : MessagingDelegate, UNUserNotificationCenterDelegate {
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }

        UIApplication.shared.registerForRemoteNotifications()
//        updatePushToken()
    }
    
    func updatePushToken(tokens:String="") {
        Messaging.messaging().token { token, error in
            if let token = token {
                print("we have to update this token: \(token)")
                AppConfig.shared.pushToken = token
            }
        }
//        if let token = Messaging.messaging().fcmToken {
//            AppConfig.shared.pushToken = token
//            print("we have to update this token: \(token)")
//        }else{
//            print("we have to save this token: \(tokens)")
//            AppConfig.shared.pushToken = tokens
//        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        if fcmToken != nil {
            updatePushToken(tokens: fcmToken!)
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let application = UIApplication.shared
        print("OnLaunch : Controll is in didReceive response")
        
        if(application.applicationState == .active){
            print("Tap : user tapped the notification bar when the app is in foreground")
        }
        
        if(application.applicationState == .inactive || application.applicationState == .background) {
            print("Tap : user tapped the notification bar when the app is in background")
            if let token = AppConfig.shared.authToken, token == "" {return}
            if AppConfig.shared.authToken == nil {return}
            
            let userInfo = response.notification.request.content.userInfo
            guard let type = userInfo["type"] as? String else { return }
            switch type {
            case NotificationTypes.THREAD.rawValue, NotificationTypes.TASK.rawValue:
                var id = userInfo["thread_id"] as? String
                if type == NotificationTypes.TASK.rawValue {
                    id = userInfo["task_id"] as? String
                }
                guard let id = id else {return}
                if let userId = userInfo["user_id"] as? String {
                    self.notificationPayload = ["type": NotificationTypes.THREAD, "id": Int(id)!, "userId": Int(userId)!] as AnyObject
                    if baseVC != nil {
                        if let vc = UIApplication.topViewController() as? ThreadDetailVC {
                            vc.dismissVC()
                        }
                        if let vc = baseVC?.getVC(storyboard: .TABS, vcIdentifier: "DetailView") as? ThreadDetailVC {
                            vc.threadId = Int(id)!
                            if let navi = UIApplication.topViewController() {
                                navi.navigateVC(vc)
                            }
                        }
                    }
                    else {
                        print("AppDelegate-> connectToSocket")
                        SocketManager.sharedInstance.connectToSocket() //application is in inactive state, so performed connect to socket
                        let splash = UIStoryboard(name: Storyboard.MAIN.rawValue, bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
                        let nextMain = UIStoryboard(name: Storyboard.HOME_TAB.rawValue, bundle: nil).instantiateViewController(withIdentifier: "TabsHomeScene") as! TabBarVC
                        nextMain.selectedIndex = 1
                        nextMain.title = ""
                        let nextVC = UIStoryboard(name: Storyboard.TABS.rawValue, bundle: nil).instantiateViewController(withIdentifier: "DetailView") as! ThreadDetailVC
                        nextVC.threadId = Int(id)!
                        nextVC.fromNotification = true
                        isPushNotificationClicked = true
                        let nav1 = UINavigationController()
    //                    nav1.navigationBar.topItem?.backButtonTitle = ""
    //                    nav1.navigationBar.isHidden = true
    //                    nav1.setNavigationBarHidden(true, animated: false)
                        nav1.viewControllers = [splash,nextMain,nextVC]
                        self.window?.rootViewController = nav1
                    }
                }
                
            case NotificationTypes.CHAT.rawValue:
                if let chatRoomId = userInfo["chat_room_id"] as? String, let userId = userInfo["user_id"] as? String {
                    self.notificationPayload = ["type": NotificationTypes.CHAT, "id": Int(chatRoomId)!, "userId": Int(userId)!] as AnyObject
                    if baseVC != nil {
                        if let vc = UIApplication.topViewController() as? ChatVC {
                            vc.dismissVC()
                        }
                        if let vc = baseVC?.getVC(storyboard: .CONNECTIOS, vcIdentifier: "ChatScene") as? ChatVC {
                            vc.chatId = Int(chatRoomId)
                            if let navi = UIApplication.topViewController() {
                                navi.navigateVC(vc)
                            }
                        }
                    }
                    else {
                        print("AppDelegate-> connectToSocket")
                        SocketManager.sharedInstance.connectToSocket() //application is in inactive state, so performed connect to socket
                        let splash = UIStoryboard(name: Storyboard.MAIN.rawValue, bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
                        let nextMain = UIStoryboard(name: Storyboard.HOME_TAB.rawValue, bundle: nil).instantiateViewController(withIdentifier: "TabsHomeScene") as! TabBarVC
                        nextMain.selectedIndex = 2
                        nextMain.title = ""
                        let nextVC = UIStoryboard(name: Storyboard.CONNECTIOS.rawValue, bundle: nil).instantiateViewController(withIdentifier: "ChatScene") as! ChatVC
                        nextVC.chatId = Int(chatRoomId)
                        nextVC.fromNotification = true
                        isPushNotificationClicked = true
                        let nav1 = UINavigationController()
    //                    nav1.navigationBar.topItem?.backButtonTitle = ""
    //                    nav1.navigationBar.isHidden = true
    //                    nav1.setNavigationBarHidden(true, animated: false)
                        nav1.viewControllers = [splash,nextMain,nextVC]
                        self.window?.rootViewController = nav1
                    }
                }
            
            case NotificationTypes.GROUP_REQUEST.rawValue:
                self.notificationPayload = ["type": NotificationTypes.GROUP_REQUEST] as AnyObject
                if baseVC != nil {
                    if let tabbar = self.window?.rootViewController as? TabBarVC {
                        tabbar.selectedIndex = 1
                        tabbar.navigationController?.popToRootViewController(animated: true)
                    }
                }
//                else {
//                    SocketManager.sharedInstance.connectToSocket() //application is in inactive state, so performed connect to socket
//                    let splash = UIStoryboard(name: Storyboard.MAIN.rawValue, bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
//                    let nextMain = UIStoryboard(name: Storyboard.HOME_TAB.rawValue, bundle: nil).instantiateViewController(withIdentifier: "TabsHomeScene") as! TabBarVC
//                    nextMain.selectedIndex = 1
//                    nextMain.title = ""
//                    let nav = UINavigationController()
//                    nav.viewControllers = [splash,nextMain]
//                    self.window?.rootViewController = nav
//                }
                
            default:
                return
            }
        }
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification
       userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        let state = application.applicationState
        switch state {
        case .inactive:
            print("Inactive")
            application.applicationIconBadgeNumber = application.applicationIconBadgeNumber + 1
        case .background:
            print("Background")
            application.applicationIconBadgeNumber = application.applicationIconBadgeNumber + 1
        case .active:
            print("Active")
        default:
            break
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")

        // With swizzling disabled you must set the APNs token here.
         Messaging.messaging().apnsToken = deviceToken
      }
}

extension AppDelegate {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let scheme = url.scheme,
           scheme.caseInsensitiveCompare("onlineDoc") == .orderedSame {
            print("url ----> \(url)")
            self.attachements.removeAll()
            let fileList = getSharedFiles()
            print("getSharedFiles() =----> \(fileList.count)")
            for item in fileList {
                print("item =--> \(item.fileExt)")
                let file = Attachment()
                if item.fileType != "web_url" {
//                    file.FileName = item.name
//                    file.file_main_type = item.fileType
                    if item.base64String == nil, let fileURL = item.fileURL {
                        do {
                            let rawData = try Data(contentsOf: fileURL, options: .mappedIfSafe)
                            item.base64String = rawData
                        }
                        catch let exp {
                            print("AppDelegate -> GETTING EXCEPTION =====> \(exp.localizedDescription)  ===> url: \(fileURL)")
                            break
//                            self.showErrorPopup(exp)
                        }
                    }
                    file.image = item.fileType == "image" ? UIImage(data: item.base64String)! : nil
                    file.base64String = item.base64String
                    file.FileType = item.fileExt.lowercased()
                    file.MimeType = item.mimeType.lowercased()
                }
                file.file_main_type = item.fileType
                file.FileName = item.name
                self.attachements.append(file)
            }
            
            let userDefaults = UserDefaults(suiteName: groupName)
            userDefaults?.removeObject(forKey: KeyShareAttachment)
            NotificationCenter.default.post(name: ._fileShare, object: nil)
            if let token = AppConfig.shared.authToken, token != ""{
                let storyboard = UIStoryboard(name: "HomeTab", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabsHomeScene") as? UITabBarController

                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }
            
            return true
        }
        return false
    }
    
    func getSharedFiles() -> [ShareFileModel] {
        let defaults = UserDefaults(suiteName: groupName)
//        print("defaults-> \(defaults)")
        let _data = defaults?.object(forKey: KeyShareAttachment)
        print("datadata-> \(_data ?? "nil")")
        guard let data = _data as? Data else {
            print("data error")
            return []
        }
        
        guard let fileList = try? PropertyListDecoder().decode([ShareFileModel].self, from: data) else {
            print("fileList error")
            return []
        }
        print("fileList success -> \(fileList)")
        return fileList
    }
}

//MARK: - override Swift `print` method
//public func print<T>(file: String = #file, function: String = #function, line: Int = #line, _ message: T, color: UIColor = .white) {
//#if DEBUG
//    Swift.print(message)
//    _SwiftLogHelper.shared.handleLog(file: file, function: function, line: line, message: message, color: color)
//#endif
//}
