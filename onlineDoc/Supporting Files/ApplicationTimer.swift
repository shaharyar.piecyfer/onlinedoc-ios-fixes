//
//  TimerApplication.swift
//  SafeStart
//
//  Created by  Piecyfer  on 2/25/19.
//  Copyright © 2019 Piecyfer. All rights reserved.
//
import UIKit

class ApplicationTimer: UIApplication {

    // the timeout in seconds, after which should perform custom actions
    // such as disconnecting the user
    
    private var timeoutInSeconds: TimeInterval {
        if let time = AppConfig.shared.user.inactivity_limit {
            return TimeInterval(time * 60)
        }else{
            return TimeInterval(5 * 60)
        }
    }

    private var idleTimer: Timer?

    // resent the timer because there was user interaction
    private func resetIdleTimer() {
        if let idleTimer = idleTimer {
            idleTimer.invalidate()
        }

        idleTimer = Timer.scheduledTimer(timeInterval: timeoutInSeconds,
                                         target: self,
                                         selector: #selector(ApplicationTimer.timeHasExceeded),
                                         userInfo: nil,
                                         repeats: false
        )
    }
    
    func checkTimerStatus() {
//        if (AppConfig.shared.rememberMe) {
//            if let idleTimer = idleTimer {
//                idleTimer.invalidate()
//            }
//            else {
//                idleTimer = Timer()
//                idleTimer?.invalidate()
//            }
//        }
//        else {
            resetIdleTimer()
//        }
    }
    
    // if the timer reaches the limit as defined in timeoutInSeconds, post this notification
    
    @objc private func timeHasExceeded() {
        print("Timer Has Exceeded in timeHasExceeded")
        NotificationCenter.default.post(name: .appTimeout, object: nil)
    }

    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)

        if idleTimer != nil {
            self.checkTimerStatus()
        }

        if let touches = event.allTouches {
            for touch in touches where touch.phase == UITouch.Phase.began {
                self.checkTimerStatus()
            }
        }
    }
}
