//
//  NotificationName+Extension.swift
//  SafeStart
//
//  Created by  Piecyfer  on 2/25/19.
//  Copyright © 2019 Piecyfer. All rights reserved.
//
import Foundation

extension Notification.Name {
    
    static let appTimeout = Notification.Name("appTimeout")
    static let notifyThread = Notification.Name("notifyThread")
    static let notifyGroup = Notification.Name("notifyGroup")
    static let notifyGroupMember = Notification.Name("notifyGroupMember")
    static let notifyGroupNotification = Notification.Name("notifyGroupNotification")
    static let notifySocket = Notification.Name("notifySocket")
//    static let notifyGroupVisited = Notification.Name("notifyGroupVisited")
    static let notifyNotification = Notification.Name("notifyNotification")
    static let chatCountCheck = Notification.Name("chatCountCheck")
    static let notificationCountCheck = Notification.Name("notificationCountCheck")
    static let nertworkFlagsChanged = Notification.Name("nertworkFlagsChanged") //to check network connectivity
    static let notifyGroupRequest = Notification.Name("notifyGroupRequest")
    
    static let _ThreadComments = Notification.Name("OD.threadsComments")
    static let _FriendRequests  = Notification.Name("OD.Requests")
    static let _Connections   = Notification.Name("OD.connections")
    static let _ConnectionDataChange = Notification.Name("OD.connectionsDataChange")
    static let _RequestsDataChange = Notification.Name("OD.requestsDataChange")
    static let _catSelection = Notification.Name("OD.selectCategoryAtGroupInfoScene")
    static let _dummyThread = Notification.Name("OD.creatDummyThread")
    static let _uploadComplete = Notification.Name("OD.filesUploadDone")
    static let _onlineListUpdate = Notification.Name("OD.Online")
    static let _fileShare = Notification.Name("OD.FileShare")
    static let _UpdateThreadUnreadCount  = Notification.Name("OD.UpdateThreadUnreadCount")
    static let _ThreadUnreadCount  = Notification.Name("OD.ThreadUnreadCount")
    static let _UpdateThreadNotification = Notification.Name("OD.UpdateThreadNotification")
    static let _visitGroup  = Notification.Name("OD.VisitGroup")
    static let _loadTimeline  = Notification.Name("OD.LoadTimeline")
    static let _reloadThreadNotification = Notification.Name("OD.ReloadThreadNotification")
}
