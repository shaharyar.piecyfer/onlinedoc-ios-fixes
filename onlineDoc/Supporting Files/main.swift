//
//  Main.swift
//  SafeStart
//
//  Created by  Piecyfer  on 2/25/19.
//  Copyright © 2019 Piecyfer. All rights reserved.
//

import UIKit

if #available(iOS 13.0, *) {
    UIApplicationMain(
        CommandLine.argc,
        CommandLine.unsafeArgv,
        NSStringFromClass(ApplicationTimer.self),
        NSStringFromClass(AppDelegate.self)
    )
//    UIApplicationMain(
//        CommandLine.argc,
//        UnsafeMutableRawPointer(CommandLine.unsafeArgv)
//            .bindMemory(
//                to: UnsafeMutablePointer<Int8>.self,
//                capacity: Int(CommandLine.argc)),
//        NSStringFromClass(ApplicationTimer.self),
//        NSStringFromClass(AppDelegate.self)
//    )
} else {
    _ = UIApplicationMain(
        CommandLine.argc,
        UnsafeMutableRawPointer(CommandLine.unsafeArgv)
            .bindMemory(
                to: UnsafeMutablePointer<Int8>.self,
                capacity: Int(CommandLine.argc)),
        NSStringFromClass(ApplicationTimer.self),
        NSStringFromClass(AppDelegate.self)
    )
}
