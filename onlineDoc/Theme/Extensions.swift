//
//  Extensions.swift
//  onlineDoc
//
//  Created by Piecyfer on 06/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

extension UIViewController {
    func applyTheme(_ theme: MyTheme) {
        view.backgroundColor = theme.settings.appBgColor
        if let nv = self as? UINavigationController {
            nv.navigationBar.backgroundColor = theme.settings.whiteColor
            nv.navigationBar.tintColor = theme.settings.titleColor
            nv.navigationBar.barTintColor = theme.settings.nvBarColor
            nv.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.settings.titleColor]
        }
        //MARK: - set status bar color
        guard let nv = self.navigationController else { return }
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let window = app.windows.filter {$0.isKeyWindow}.first
            let statusBarHeight: CGFloat = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = theme.settings.whiteColor
            nv.view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: nv.view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: nv.view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: nv.view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = theme.settings.whiteColor
        }
    }
}

extension UILabel {
    func applyTheme(_ theme: MyTheme) {
//        self.backgroundColor = theme.settings.appBgColor
        self.textColor = theme.settings.titleColor
    }
}

extension UITableViewCell {
    func applyTheme(_ theme: MyTheme) {
        self.contentView.backgroundColor = theme.settings.cellColor
    }
}

extension UISearchBar {
    func applyTheme(_ theme: MyTheme) {
        self.barTintColor = theme.settings.searchBarColor
        self.backgroundImage = UIImage()
//        self.backgroundColor = theme.settings.searchBarColor
//        for view in self.subviews {
//            for subview in view.subviews {
//                if subview.isKind(of: UITextField.self) {
//                    let tf: UITextField = subview as! UITextField
////                    tf.backgroundColor = theme.settings.titleColor
//                    tf.textColor = theme.settings.titleColor
//                }
//            }
//        }
        if let tf = self.value(forKey: "searchField") as? UITextField {
//            tf.backgroundColor = theme.settings.searchBarColor
            tf.textColor = theme.settings.titleColor

            if let tfPlaceholder = tf.value(forKey: "placeholderLabel") as? UILabel {
                tfPlaceholder.textColor = theme.settings.subTitleColor
            }
            
            let glassIconView = tf.leftView as? UIImageView
            glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
            glassIconView?.tintColor = theme.settings.subTitleColor
        }
    }
}
