//
//  MyTheme.swift
//  onlineDoc
//
//  Created by Piecyfer on 06/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

// A theme enum, that describes available themes
enum MyTheme: ThemeProtocol {
    case light
    case dark
    var settings: MyThemeSettings {
        switch self {
        case .light: return MyThemeSettings.lightTheme
        case .dark: return MyThemeSettings.darkTheme
        }
    }
}
