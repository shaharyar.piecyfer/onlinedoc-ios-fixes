//
//  MyThemeSettings.swift
//  onlineDoc
//
//  Created by Piecyfer on 06/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

/// A theme model.
struct MyThemeSettings: ThemeModelProtocol {
    let appBgColor: UIColor
    let nvBarColor: UIColor
    let highlightedBgColor: UIColor
    let titleColor: UIColor
    let subTitleColor: UIColor
    let cellColor: UIColor
    let searchBarColor: UIColor
    
    let whiteColor: UIColor
}
