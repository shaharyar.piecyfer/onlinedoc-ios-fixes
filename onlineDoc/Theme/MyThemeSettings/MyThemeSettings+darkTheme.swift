//
//  MyThemeSettings+darkTheme.swift
//  onlineDoc
//
//  Created by Piecyfer on 06/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

// A concrete theme instance
extension MyThemeSettings {
    static let darkTheme = MyThemeSettings(
        appBgColor: AppColors.blackColor,
        nvBarColor: AppColors.extraLightGrayColor,
        highlightedBgColor: .darkGray,
        titleColor: AppColors.whiteColor,
        subTitleColor: AppColors.lightGrayColor,
        cellColor: AppColors.extraLightGrayColor,
        searchBarColor: AppColors.mediumGrayColor,
        
        whiteColor: AppColors.whiteColor
    )
}
