//
//  MyThemeSettings+lightTheme.swift
//  onlineDoc
//
//  Created by Piecyfer on 06/08/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import UIKit

// A concrete theme instance
extension MyThemeSettings {
    static let lightTheme = MyThemeSettings(
        appBgColor: AppColors.whiteColor,
        nvBarColor: AppColors.extraLightGrayColor,
        highlightedBgColor: AppColors.mediumGrayColor,
        titleColor: AppColors.blackColor,
        subTitleColor: AppColors.lightGrayColor,
        cellColor: AppColors.extraLightGrayColor,
        searchBarColor: AppColors.extraLightGrayColor,
        
        whiteColor: AppColors.whiteColor
    )
}
