////
////  ThemeManager.swift
////  onlineDoc
////
////  Created by Piecyfer on 05/08/2021.
////  Copyright © 2021 onlineDoc. All rights reserved.
////
//
//import UIKit
//
//enum Theme {
//    case light
//    case dark
//}
//
//extension Theme {
//    var appTheme: ThemeProtocol {
//        switch self {
//            case .light: return LightTheme()
//            case .dark: return LightTheme()
//        }
//    }
//}
//
//protocol ThemeProtocol {
//    var assets: Themeable { get }
//    var `extension`: (() -> Void)? { get }
//}
//
//protocol Themeable {
//    var labelAssets: AppLabelAssets { get }
////    var buttonAssets: ButtonAssets { get }
////    var switchAssets: SwitchAssets { get }
//}
//
//class AppButton: UIButton {
//    @objc dynamic override var cornerRadius: CGFloat {
//        get { return layer.cornerRadius }
//        set (newValue) { layer.cornerRadius = newValue }
//    }
//}
//class AppLabel: UILabel {}
//
//struct AppLabelAssets {
//    var color: UIColor { get{} }
//    var font: UIFont { get{} }
//}
//
//struct ThemeAssets {
//    var labelAssets = AppLabelAssets()
//}
//
//struct ThemeManager {
//
//    static func apply(_ theme: Theme, application: UIApplication) {
//        // 1
//        let appTheme = theme.appTheme
//        // 2
//        updateLabel(using: appTheme.assets.labelAssets)
//        // 3
//        appTheme.extension?()
//        // 4
//        application.windows.first?.reload()
//    }
//}
//
//func updateLabel(using themeAssets: AppLabelAssets) {
//    AppLabel.appearance().textColor = themeAssets.color
//}
//
//class LightTheme: ThemeProtocol {
//    
//    var assets: Themeable {
//        return ThemeAssets(
//            labelAssets: AppLabelAssets(
//                color: UIColor.blue.primary,
//                font: .systemFont(ofSize: 18)
//            )
////            buttonAssets: AppButtonAssets(
////                normalBackgroundColor: UIColor.blue.primary,
////                selectedBackgroundColor: UIColor.blue.secondary,
////                disabledBackgroundColor: UIColor.blue.tertiary
////            ),
////            switchAssets: AppSwitchAssets(
////                isOnColor: UIColor.blue.primary,
////                isOnDefault: true
////            ),
////            cellAssets: AppTableViewCellAssets(selectedColor: UIColor.blue.tertiary),
////            segmentedControlAssets: AppSegmentControllAssets(activeColor: UIColor.blue.primary),
////            pinAssets: AppAnnotationViewAssets(color: UIColor.blue.primary)
//        )
//    }
//
//    override var `extension`: (() -> Void)? {
//        return {
//            let proxy = AppButton.appearance(whenContainedInInstancesOf: [AppView.self])
//            proxy.cornerRadius = 12.0
//            proxy.setBackgroundColor(color: blue, forState: .normal)
//        }
//    }
//}
//
//// application.keyWindow?.reload()
//// Implementation
//public extension UIWindow {
//
//    /// Unload all views and add them back
//    /// Used for applying `UIAppearance` changes to existing views
//    func reload() {
//        subviews.forEach { view in
//            view.removeFromSuperview()
//            addSubview(view)
//        }
//    }
//}
//
////ThemeManager.apply(.red)
