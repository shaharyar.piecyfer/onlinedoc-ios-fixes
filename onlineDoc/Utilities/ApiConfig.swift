//
//  ApiConfigswift
//  onlineDoc
//
//  Created by Piecyfer on 27/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import Foundation

internal struct ApiConfig {
//    Live URL's
//    static let DOMAIN                              = "apiv2.onlinedoc.dk"

//    Staging URL's
    static let DOMAIN                              = "devapi.journalcare.dk"
    
//    Local URL's
//    static let DOMAIN                              = "192.168.1.38:3000"
//    static let BASE_URL                            = "http://\(DOMAIN)/api/v1/"

    static let BASE_URL                            = "https://\(DOMAIN)/api/v1/"
    static let SOCKET_URL                          = "wss://\(DOMAIN)/cable?token="
    
    static let USERS                               = "users"
    static let authRoute                           = "\(USERS)/login"
    static let logout                              = "\(USERS)/logout"
    static let updateUser                          = "\(USERS)/update"
    static let registerRoute                       = "users"
    static let codeVerification                    = "\(USERS)/verify_user_phone_code"
    static let resendConfirmationRoute             = "\(USERS)/resend_confirmation_email"
    static let forgotPasswordRoute                 = "\(USERS)/forgot_password"
    static let forceUpdate                         = "app_versions"
    static let systemUsersList                     = "\(USERS)/all_system_users?"
    static let me                                  = "\(USERS)/me"
    static let deleteProfileRoute                  = "\(USERS)/delete_user"
    static let deactivateProfile                   = "\(USERS)/deactivate_user"
    static let weblinks                            = "user_web_links"
    static let userEmails                          = "user_emails"
    static let switchPrimaryEmail                  = "\(userEmails)/switch_primary_email"
    static let inactivityTimeRoute                 = "\(USERS)/set_inactivity_limit"
    static let changePassword                      = "\(USERS)/change_password"
    static let loggedInUsersCount                  = "\(USERS)/logged_user_list"
    static let resetPassword                       = "\(USERS)/reset_password"
    static let reactivateUser                      = "\(USERS)/reactivate_user"
    static let checkChatRoomByVirtual              = "\(USERS)/check_chat_room"
    static let createVirtualUser                   = "\(USERS)/create_virtual_user"
    
    static let groupRoute                          = "clinics"
    static let groupsListRouteDeprecated           = "\(groupRoute)/get_all_clinic_listing"
    static let groupsListRoute                     = "\(groupRoute)/get_all_clinic_listing_web"
    static let updateGroupStatus                   = "clinic_users/update_clinic_user_status"
    static let joinGroup                           = "clinic_users/join_clinic"
    static let userGroupsWithCategpries            = "\(groupRoute)/user_clinics_with_categories"
    static let docs                                = "docs"
    static let myDocs                              = "\(docs)/my_docs"
    static let docNotification                     = "doc_notifications"
    static let docNotificationCount                = "\(docNotification)/get_user_all_notification_count"
    static let updateDocNotification               = "\(docNotification)/update_notification_unread_count"
    static let leaveGroup                          = "\(groupRoute)/leave_clinic"
    static let removeGroupMember                   = "clinic_users/remove_users_from_group"
    static let clinic_categories                   = "clinic_categories"
    static let becomeAdmin                         = "\(groupRoute)/become_admin"
    static let getVisitedGroups                    = "\(groupRoute)/visited_clinic_listing"
    static let getGroups                           = "\(groupRoute)/visited_clinic_listing_mobile"
    static let getAllClinics                       = "\(groupRoute)/get_all_clinic_listing_mobile"
    static let invitationRoute                     = "clinic_users/invite_user_to_clinic"
    static let toggleDisplayTimeline               = "clinic_users/toggle_display_timeline"
    static let groupEmailRoute                     = "\(groupRoute)/send_group_email"
    static let categoryDoc                         = "clinic_categories/category_docs"
    
    static let friendsRequest                      = "friends"
    static let updateFriendsRequest                = "update_request_status"
    
    static let directUpload                        = "direct_uploads"
    static let timelineRoute                       = "\(groupRoute)/index"
    static let searchTimeline                      = "\(groupRoute)/search"
    static let searchCount                         = "\(groupRoute)/search_count"
    static let docComments                         = "doc_comments"
    static let deleteFileRoute                     = "\(docs)/delete_attachment"
    static let docCommentFiles                     = "doc_comments/show_files"
    
    static let chatRoomRoute                       = "chat_rooms"
    static let chatUnreadCount                     = "\(chatRoomRoute)/total_chat_room_count"
    static let groupRequests                       = "clinic_users/show_all_group_request_users"
    static let deleteChatRoute                     = "\(chatRoomRoute)/delete_chat_room"
    static let chatMessagesRoute                   = "messages"
    static let deleteFileMessageRoute              = "\(chatMessagesRoute)/delete_attachment"
    
    static let clinicContactRoute                  = "clinic_contacts"
    static let getClinicContacts                   = "\(clinicContactRoute)/showv2"
    static let searchClinicContactRoute            = "\(clinicContactRoute)/search_contact"
    
    static let searchHistoryRoute                  = "search_histories"
}
