//
//  AppConfig.swift
//  onlineDoc
//
//  Created by Piecyfer on 05/11/2019.
//  Copyright © 2019 onlineDoc. All rights reserved.
//

import Foundation
import UIKit
import RAGTextField
import Photos

class AppConfig {
    
    static let shared = AppConfig()
    fileprivate init(){}
    fileprivate let tokenKey = "ODRememberedToken"
    fileprivate let userIDKey = "RememberedUserId"
    fileprivate let userObjectKey = "rememberUserKey"
    fileprivate let IdentifierKey = "com.example.github-token"
    fileprivate let PushtokenKey = "ODPushtoken"
    fileprivate let kUserPrefKey = "user"
    fileprivate let kForceResetLogin = "kForceResetLogin"
    fileprivate let kDeviceToken = "DeviceToken"
    fileprivate let kCurrentLanguage = "kCurrentLanguage"
    fileprivate let kIsContactPermissionShows = "kIsContactPermissionShows"
    
    var isDarkMode: Bool = false {
        didSet {
            Themer.shared.theme = isDarkMode ? .dark : .light
        }
    }
    
    let supportedVideoFormats = ["mp4","mov", "avi","mkv","MOV"]
    let supportedImageFormats = ["jpeg", "gif", "bmp", "tiff", "png","JPG","jpg","HEIC"]
    let defaultImage = UIImage(named: "defaultImage")
    var rememberMe = true
    var lastVisitedTabIndex = 0
    var friendMessagesCount = 0
    var requestMessagesCount = 0
    var groupJoiningRequestCount = 0
    var chatUnreadCount = 0
    var threadNotificationUnreadCount = 0
    var myGroupsWithCategories = [GroupModel]()
    var threadGroupMembers = [GroupMember](){
        didSet{
            var ind = -1
            
            for (i,member) in threadGroupMembers.enumerated(){
                if member.userId == self.user.id{
                    ind = i
//                    threadGroupMembers.remove(at: i)
                }
            }
            if ind != -1{
                threadGroupMembers.remove(at: ind)
            }
        }
    }
    
    var isCreateThread = false
    var listNeedReload = false
    var communityGroupAdmins = [String]()
    var actionDoneOnProfileScene:RequestAction = .none
//    var searchFilters = AdvanceSearchOptions()
    var firebaseUsersList = [CommentorProfile]()
    var needsRequestUpdate = false
    var requestType: RequestType = .none
    var systemUsersList = [GroupMember]()
    var onlineUsersList = [OnlineStatus]()
    var friendListFirebase = [Friend]()
    var requestListFirebase = [Friend]()
    fileprivate var _userAuthToken  = ""
    
    fileprivate var _userId         = ""
    fileprivate var _pushToken  = ""
    var groupListNeedReload = false
    var didDeleteSelectedGroup = false
    var didAdminSentInvitation = false
    var needsResetGroupInfoScene = false
    var needResetEditGroupInfoScene = false
    var timelineNeedsReload  = false
    var NewGroupId = 0
    typealias alertResponse = (Bool?)-> Void

    var authToken:String! {
        get{
            if _userAuthToken == ""{
                if let token  = UserDefaults.standard.string(forKey: tokenKey){
                _userAuthToken = token
                }
            }
            return _userAuthToken
        }
        
        set{
            if rememberMe{
                self._userAuthToken = newValue
                UserDefaults.standard.set(_userAuthToken, forKey: tokenKey)
            }else{
                self._userAuthToken = newValue
            }
        }
    }
    
    var pushToken:String {
        get{
            if _pushToken == ""{
                if let token  = UserDefaults.standard.string(forKey: PushtokenKey){
                _pushToken = token
                }
            }
            return _pushToken
        }
        
        set{
            if _pushToken != newValue {
                _pushToken = newValue
                UserDefaults.standard.set(_pushToken, forKey: PushtokenKey)
            }
        }
    }
    
    var deviceToken: String {
        get{
            if let token  = UserDefaults.standard.string(forKey: kDeviceToken){
                return token
            }
            return ""
        }
        set{
            UserDefaults.standard.set(newValue, forKey: kDeviceToken)
        }
    }
    
    var userId:String!{
        get{
            if _userId == ""{
                if let id  = UserDefaults.standard.string(forKey: userIDKey){
                    _userId = id
                }
            }
            return _userId
        }
        set{
            self._userId = newValue
            if rememberMe{
                UserDefaults.standard.set(newValue, forKey: userIDKey)
            }
        }
    }
    
    var user : User {
        get{
            self.getUser()
        }
        set{
            self.saveUser(newValue)
        }
    }
    
    private func saveUser(_ item: User) {
        let encodedData = try? JSONEncoder().encode(item)
        UserDefaults.standard.set(encodedData, forKey: kUserPrefKey)
    }

    private func getUser() -> User {
        if let data = UserDefaults.standard.data(forKey: kUserPrefKey), let userData = try? JSONDecoder().decode(User.self, from: data) {
            return userData
        } else {
            return User()
        }
    }
    
    func saveChatCount(count:Int){
        UserDefaults.standard.set(count, forKey: Notification.Name.chatCountCheck.rawValue)
    }
    
    func getChatCount() -> Int{
        return UserDefaults.standard.integer(forKey: Notification.Name.chatCountCheck.rawValue)
    }
    
    func saveNotificationCount(count:Int){
        UserDefaults.standard.set(count, forKey: Notification.Name.notificationCountCheck.rawValue)
    }
    
    func getNotificationCount() -> Int{
        return UserDefaults.standard.integer(forKey: Notification.Name.notificationCountCheck.rawValue)
    }
    
    func saveResetUserLogin(){
        UserDefaults.standard.set(true, forKey: kForceResetLogin)
    }
    
    func getResetUserLogin() -> Bool {
        return UserDefaults.standard.bool(forKey: kForceResetLogin)
    }
    
    func saveContactPermissionShow() {
        UserDefaults.standard.set(true, forKey: kIsContactPermissionShows)
    }
    
    func getIsContactPermissionShows() -> Bool {
        return UserDefaults.standard.bool(forKey: kIsContactPermissionShows)
    }
    
    var logout: String!{
        get{
            return _userId
        }
        
        set{
            SocketManager.sharedInstance.disconnectToSocket()
            UIApplication.shared.applicationIconBadgeNumber = 0
            actionDoneOnProfileScene = .none
//            rememberMe      = false
            self.userId = ""
            self.authToken = ""
            
            self.user       = User()
            self.myGroupsWithCategories = [GroupModel]()
            self.threadGroupMembers = [GroupMember]()
//            self.searchFilters = AdvanceSearchOptions()
            self.firebaseUsersList = [CommentorProfile]()
            self.requestListFirebase = [Friend]()
            self.friendListFirebase = [Friend]()
            self.systemUsersList    = [GroupMember]()
            self.needsRequestUpdate = false
            self.requestType = .none
//            self.chatUnreadCount = 0
            self.threadNotificationUnreadCount = 0
            UserDefaults.standard.set("", forKey: userIDKey)
            UserDefaults.standard.set("", forKey: tokenKey)
            UserDefaults.standard.set("", forKey: userObjectKey)
            UserDefaults.standard.set(newValue, forKey: tokenKey)
            UserDefaults.standard.removeObject(forKey: kUserPrefKey)
            self.resetDefaults()
        }
    }
    
//    func logoutUser() {
//        user = nil
//        self.resetDefaults()
//        UserDefaults.standard.removeObject(forKey: "user")
//    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if key == CREDENTIALS_DICT && AppConfig.shared.getResetUserLogin() == false {
                defaults.removeObject(forKey: key)
            }
            else if key == kForceResetLogin || key == kCurrentLanguage {
                // no need to reset it
            }
            else {
                if key != CREDENTIALS_DICT {
                    defaults.removeObject(forKey: key)
                }
            }
        }
    }
    
    func resetThreadGroupMembers(){
        for (i, _) in threadGroupMembers.enumerated(){
            if i < threadGroupMembers.count{
                threadGroupMembers[i].isSelected = false
                
            }
        }
    }
    
    func resetGroupSelection(){
        for (i,_) in myGroupsWithCategories.enumerated(){
            myGroupsWithCategories[i].isSelected = false
            if let categories = myGroupsWithCategories[i].clinic_categories{
                for item in categories {
                    item.isSelected = false
                }
            }
        }
    }
    
    func getSystemRegion() -> String {
        return Locale.current.regionCode ?? "US"
    }
    
    func appVersion() -> String? {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    var threadgroups = [GroupsList]()
    
    func setTextfieldIcon(for textField: UITextField,addIcon iconImage:UIImage,location: IconLocation = .left){
        
        let iconView = UIImageView(frame: CGRect(x: 5, y: 5, width: 30, height: 30))
        iconView.image = iconImage
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
        iconContainerView.addSubview(iconView)
        if location == .right{
            textField.rightView = iconContainerView
            textField.rightView?.tag = textField.tag
            textField.rightViewMode = .always
        }else{
            textField.leftView = iconContainerView
            textField.leftView?.tag = textField.tag
            textField.leftViewMode = .always
        }
    }
    
    func setunderLine(for textField:RAGTextField){
        let bgView = UnderlineView(frame: .zero)
        bgView.textField = textField
        bgView.backgroundLineColor = AppColors.underline
        bgView.backgroundLineColor = AppColors.underBar
        bgView.foregroundLineWidth = 2.0
        bgView.expandDuration = 0.1
        bgView.backgroundColor = AppColors.clear
        
        if #available(iOS 11, *) {
            bgView.layer.cornerRadius = 4.0
            bgView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
        
        textField.tintColor = AppColors.underBar
        textField.textBackgroundView = bgView
        textField.textPadding = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
        textField.textPaddingMode = .textAndPlaceholder
        textField.scaledPlaceholderOffset = 0.0
        textField.placeholderMode = .scalesWhenEditing
        textField.placeholderScaleWhenEditing = 0.8
        textField.placeholderColor = AppColors.underBar.withAlphaComponent(0.66)
        textField.transformedPlaceholderColor = textField.tintColor
        textField.hint = nil
    }
    
    func convertToDate(from dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale(identifier: "en_US")

//        dateFormatter.locale = Locale(identifier: "en_US")
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        return(dateFormatter.date(from: dateString)!) // Jan 2, 2001
        if let date = dateFormatter.date(from: dateString) {
            return date
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
            if let date = dateFormatter.date(from: dateString) {
                return date
            } else {
                return Date.init()
            }
        }
    }
    
     func convertToDateZero(from dateString: String) -> Date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
    //        dateFormatter.locale = Locale(identifier: "en_US")
    //        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            return(dateFormatter.date(from: dateString)!) // Jan 2, 2001
        }
    
    func formatPickerDate(date : Date) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd/MM/yyyy"
        dateFormater.calendar = Calendar(identifier: .gregorian)
        dateFormater.locale = Locale(identifier: "en_US")
        let stringFromDate = dateFormater.string(from: date)
        return stringFromDate
    }
    
    func setLanguage(lang: SupportedLanguages) {
        UserDefaults.standard.set(lang.rawValue, forKey: kCurrentLanguage)
        UserDefaults.standard.set([lang.rawValue], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        Bundle.setLanguage(lang.rawValue)
    }
    
    func currentLanguage() -> SupportedLanguages {
//        print("localizations -> \(Bundle.main.localizations)")
//        print("preferredLocalizations -> \(Bundle.main.preferredLocalizations.first)")
//        print("deviceLanguage -> \(Locale.current.languageCode)")
        if let currentLang = UserDefaults.standard.string(forKey: kCurrentLanguage) {
            switch currentLang {
            case SupportedLanguages.DANISH.rawValue:
                return .DANISH
            default:
                return .ENGLISH
            }
        }
        else if (Locale.current.languageCode == SupportedLanguages.DANISH.rawValue){
            return .DANISH
        }
        return .ENGLISH
    }
    
    func changeRootVC() -> (){
        let storyboard: UIStoryboard = UIStoryboard(name: "HomeTab", bundle: nil)
        let mainView = storyboard.instantiateInitialViewController()
        UIApplication.shared.windows.first?.rootViewController = mainView
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func changeRootVCToLoginVC() -> (){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainView = storyboard.instantiateInitialViewController()
        UIApplication.shared.windows.first?.rootViewController = mainView
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func getImageFromAsset(asset: PHAsset) -> UIImage? {
        var img: UIImage?
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        options.isNetworkAccessAllowed = true
        manager.requestImageData(for: asset, options: options) { data, _, _, _ in
            if let data = data {
                img = UIImage(data: data)
            }
        }
        return img
    }
    
    func getImageFromURL(url:URL) -> UIImage? {
        var image: UIImage?
        var imgData = Data()
        do {
            let data = try Data(contentsOf: url, options: .mappedIfSafe)
            imgData = data
        } catch  {
            print("No image data to show")
        }
        image = UIImage(data: imgData)
        return image
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage? {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail: UIImage?
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 500, height: 500), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            if let result = result {
                thumbnail = result
            }
        })
        return thumbnail
    }
    
    func getURLFromAsset(mPhasset: PHAsset, completionHandler : @escaping ((_ responseURL : URL?) -> Void)) {
        if mPhasset.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            mPhasset.requestContentEditingInput(with: options, completionHandler: { (contentEditingInput, info) in
                if let input = contentEditingInput, let imgURL = input.fullSizeImageURL {
                    completionHandler(imgURL)
                }
                else {
                    completionHandler(nil)
                }
            })
        } else if mPhasset.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            options.isNetworkAccessAllowed = true
            PHImageManager.default().requestAVAsset(forVideo: mPhasset, options: options, resultHandler: { (asset, audioMix, info) in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl = urlAsset.url
                    completionHandler(localVideoUrl)
                } else {
                    completionHandler(nil)
                }
            })
        }
    }
    
    func getSubviewsInView(view: UIView) -> [UIView] {
        var results = [UIView]()
        for subview in view.subviews as [UIView] {
            results += [subview]
            results += getSubviewsInView(view: subview)
        }
        return results
    }
    
    func getLabelsInView(view: UIView, tag: Int? = nil) -> [UILabel] {
        var results = [UILabel]()
        for subview in view.subviews as [UIView] {
            if let tag = tag {
                if let labelView = subview as? UILabel, labelView.tag == tag {
                    results += [labelView]
                } else {
                    results += getLabelsInView(view: subview, tag: tag)
                }
            }
            else if let labelView = subview as? UILabel {
                results += [labelView]
            }
            else {
                results += getLabelsInView(view: subview)
            }
        }
        return results
    }
    
//    private func getSubviewsOf<T: UIView>(view: UIView) -> [T] {
//        var subviews = [T]()
//
//        for subview in view.subviews {
//            subviews += getSubviewsOf(view: subview) as [T]
//
//            if let subview = subview as? T {
//                subviews.append(subview)
//            }
//        }
//
//        return subviews
//    }
    
    func getSubviewsOf<T: UIView>(view: UIView, tag: Int? = nil) -> [T] {
        var results = [T]()
        for subview in view.subviews as [UIView] {
            if let tag = tag {
                if let view = subview as? T, view.tag == tag {
                    results += [view]
                } else {
                    results += getSubviewsOf(view: subview, tag: tag)
                }
            }
            else if let view = subview as? T {
                results += [view]
            }
            else {
                results += getSubviewsOf(view: subview)
            }
        }
        return results
    }
}

enum IconLocation {
    case left
    case right
}
enum RequestAction{
    case sent
    case cancelled
    case accept
    case none
}
