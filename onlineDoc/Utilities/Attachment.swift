//
//  Attachment.swift
//  DoT
//
//  Created by Zeeshan Ahmad Butt on 22/03/2018.
//  Copyright © 2018 Zeeshan Ahmad Butt. All rights reserved.
//

import UIKit
import ObjectMapper

class Attachment {
 
    var FileName : String = ""
    var base64String : Data!
    var FileType : String = ""
    var MimeType : String = ""
    var imagePath : String = ""
    var file_main_type : String = ""
    var image : UIImage!
    var isServerRecord: Bool = false
    var postedFrom: String!
    var id: Any = ""
    var msgId: Any = ""
    
}
