//
//  FirebaseKeys.swift
//  onlineDoc
//
//  Created by Piecyfer on 13/02/2020.
//  Copyright © 2020 onlineDoc. All rights reserved.
//

import Foundation

let FirebaseKeyToThreadComments         = "documentComments"

let FirbaseUsersKey                     = "users"

let FirebaseKeyToFriendList             = "friendList"

let FirebaseOnlineStatusKey             = "online"

let FirebaseKeyToDirectMessages         = "directMessages"

let FirebaseKeyToChat                   = "chat"

let FirebaseKeyToDocs                   = "docs"

let FirebaseKeyToDocUsers               = "docUsers"
