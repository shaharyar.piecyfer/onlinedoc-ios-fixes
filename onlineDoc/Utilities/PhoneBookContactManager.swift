//
//  PhoneBookContactManager.swift
//  onlineDoc
//
//  Created by Piecyfer on 08/11/2021.
//  Copyright © 2021 onlineDoc. All rights reserved.
//

import Foundation
import Contacts

class PhoneBookContactManager {
    
    static let shared = PhoneBookContactManager()
    func getContactFromCNContact() -> [CNContact] {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactGivenNameKey,
            CNContactMiddleNameKey,
            CNContactFamilyNameKey,
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey,
            CNContactImageDataKey,
            CNContactIdentifierKey
            ] as [Any]
        //Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor]).filter { contact in
                    !contact.phoneNumbers.isEmpty
                }
                results.append(contentsOf: containerResults)
                
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return results
    }
    
    
    func getAllPhoneContactJson() ->  String {
        let contacts =  PhoneBookContactManager.shared.getContactFromCNContact()
        var contactsList = [PhoneBookContacts]()
        for obj in contacts{
            var cnt_obj = PhoneBookContacts()
            cnt_obj.name = obj.givenName + " " + obj.familyName
            cnt_obj.email = obj.emailAddresses.first?.value as String? ?? ""
            cnt_obj.phone = obj.phoneNumbers.first?.value.stringValue ?? ""
            contactsList.append(cnt_obj)
        }
        return self.JSONStringify(value: contactsList)
    }
    
    func getAllPhoneContactList() -> [ContactModel] {
        let contacts =  PhoneBookContactManager.shared.getContactFromCNContact()
        var contactsList = [ContactModel]()
        for obj in contacts{
            var cnt_obj = ContactModel()
            cnt_obj?.user_id = AppConfig.shared.user.id
            cnt_obj?.contact_name = obj.givenName + " " + obj.familyName
            cnt_obj?.contact_email = obj.emailAddresses.first?.value as String? ?? ""
            if let phone = obj.phoneNumbers.first?.value.stringValue {
                var desiredNumber = ""
                if phone.starts(with: "+") {
                    desiredNumber = "+" + phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
                }
                else {
                    desiredNumber = "+45" + phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression).suffix(8)
                }
                cnt_obj?.mobile_no = desiredNumber
            }
            else {
                cnt_obj?.mobile_no = ""
            }
//            if(obj.imageDataAvailable){
//                cnt_obj.photo = obj.thumbnailImageData ?? obj.imageData!
//            }else{
//                cnt_obj.photo = nil
//            }
            if !(cnt_obj?.mobile_no?.isEmpty ?? false) {
                contactsList.append(cnt_obj!)
            }
        }
        return contactsList
    }
    
    func JSONStringify(value: [PhoneBookContacts], prettyPrinted:Bool = false) -> String{
        do {
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(value)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            print(json!)
            return json!
        }
        catch {
            print(error)
        }
        return ""
    }
}

struct PhoneBookContacts : Codable{
    var name  : String?
    var phone  : String?
    var email  : String?
    var photo : Data?
    var display :String {
        if let na = name ,na.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
            return na
        }else if let na = email ,na.count > 0{
            return na
        }else if let na = phone ,na.count > 0{
            return na
        }else{
            return ""
        }
    }
}
