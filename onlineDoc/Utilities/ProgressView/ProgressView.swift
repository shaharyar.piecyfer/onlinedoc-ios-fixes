//
//  ProgressView.swift
//  onlineDoc
//
//  Created by Piecyfer on 14/02/2022.
//  Copyright © 2022 onlineDoc. All rights reserved.
//

import UIKit

class ProgressView: UIView {
    @IBOutlet var uiContent: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var uiProgress: UIProgressView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed(String(describing: ProgressView.self), owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        viewFromXib.backgroundColor = AppColors.lightGrayColor.withAlphaComponent(0.5)
        addSubview(viewFromXib)
        // corner radius
        uiContent.layer.cornerRadius = 10

        // shadow
        uiContent.layer.shadowColor = UIColor.black.cgColor
        uiContent.layer.shadowOffset = CGSize(width: 0, height: 0)
        uiContent.layer.shadowOpacity = 0.7
        uiContent.layer.shadowRadius = 4.0
    }
}
