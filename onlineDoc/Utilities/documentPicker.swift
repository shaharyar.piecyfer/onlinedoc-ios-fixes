//
//  documentPicker.swift
//  DoT
//
//  Created by Zeeshan Ahmad Butt on 29/03/2018.
//  Copyright © 2018 Zeeshan Ahmad Butt. All rights reserved.
//
/*
import UIKit
import DBAttachmentPickerController

typealias PickerResponseBlock = ((_ success : Bool, _ attachment: Attachment) -> Void)

private let _singletonSharedInstance = documentPicker()

class documentPicker: NSObject {

    var picker = DBAttachmentPickerController()
    
    static func sharedInstance() -> documentPicker {
        return _singletonSharedInstance
    }
    
    func getAttachment(_ VC: UIViewController, responseBlock : @escaping PickerResponseBlock) {
        UINavigationBar.appearance().tintColor = UIColor.black
        let attachmentObj = Attachment()
        
        picker = DBAttachmentPickerController(finishPicking: { (attachmentArray) in
//            UINavigationBar.appearance().tintColor = AppTheme.navigationBarColor()
            let myAttachment = attachmentArray[0]
            
//            switch myAttachment.sourceType {
//            case .documentURL:
//                break
//            case.phAsset:
//                break
//            case .image:
//                break
//            case .unknown:
//                break
//            }
            
            if myAttachment.mediaType == .image {
                myAttachment.loadOriginalImage(completion: { (image) in
                    var fileName = myAttachment.fileName
                    let imageData = image!.jpegData(compressionQuality: 0.5)
                    let base64String = imageData?.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
                    if fileName! == "capturedImage.HEIC" {
                        fileName = "capturedImage\(arc4random()).HEIC"
                    }
                    attachmentObj.FileName = fileName!
                    attachmentObj.base64String = base64String!
                    responseBlock(true, attachmentObj)
                })
            }
//            else if myAttachment.mediaType == .video {
//                print("\(myAttachment.fileName)")
//                print("\(myAttachment.creationDate)")
//                print("\(myAttachment.fileSize)")
//                print("\(myAttachment.mediaType)")
//                print("\(myAttachment.fileSizeStr)")
//                print("\(myAttachment.originalFileResource())")
//                responseBlock(false, attachmentObj)
//                AppConfig.shared.popAlert(To: "Document Reading Error", for: "Selected file type not supported", callingView: nil)
////                Utility.showAlertWithTitle(Alert.localized(), message: fileTypeErr.localized(), sender: VC)
//            }
            else {
                let fileType = myAttachment.originalFileResource() as AnyObject
                
                if fileType.isKind(of: NSString.self) {
                    let fileResource : String = myAttachment.originalFileResource() as! String
                    let getName = fileResource.components(separatedBy: "/")
                    if getName.last != nil {
                        attachmentObj.FileName = getName.last!
                        let file = URL(fileURLWithPath: fileResource , isDirectory: true)
                        do {
                            let fileData = try NSData(contentsOf: file, options: NSData.ReadingOptions())
                            let base64String = fileData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
                            attachmentObj.AttachmentBase64String = base64String
                        } catch {
                        }
                        print(attachmentObj.FileName)
                        print(attachmentObj.FileType)
                        print(attachmentObj.AttachmentBase64String)
                        responseBlock(true, attachmentObj)
                    }else{
                        responseBlock(false, attachmentObj)
                    }
                }else{
                    responseBlock(false, attachmentObj)
                     AppConfig.shared.popAlert(To: "Document Reading Error", for: "Selected file type not supported", callingView: nil)
//                    Utility.showAlertWithTitle(Alert.localized(), message: fileTypeErr.localized(), sender: VC)
                }
            }
        }, cancel: {
//            UINavigationBar.appearance().tintColor = AppTheme.navigationBarColor()
            print("Cancel Block Calling")
            responseBlock(false, attachmentObj)
        })
        
        picker.allowsSelectionFromOtherApps = true
        picker.allowsMultipleSelection = false
        picker.present(on: VC)
    }
}


extension documentPicker : UIDocumentPickerDelegate {
    
    func openDocumentPicker(_ vc : UIViewController) -> () {
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text"], in: .import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        vc.present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("cancel")
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print(urls[0])
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print(url)
    }
}


*/

